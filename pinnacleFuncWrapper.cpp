#include "pinnacleFuncWrapper.h"
#include "someHelpers.h"
#include "GuiException.h"

// Pinnacle file
#include "libClif/PinnacleFiles.h"

// Uncertainty models
// Model Data
#include "MotionModels/ModelData.h"

// Robustness analyzer
#include "Algorithm/RobustnessAnalyzer.h"

#include <iomanip>      // std::setw
#include <vector>
#include <string>
#include <algorithm>
pinnacleFuncWrapper::pinnacleFuncWrapper() //:debug_buffer(debug_capture), logstream(&debug_buffer)
{
    isPatientFolderSelected = false;
    setInitialParamsForApplication();
    verbose = false;
}

void pinnacleFuncWrapper::robustnessAnalyzerPinnacle(QString patientPath,
                                            QString planDotRoiPath,
                                            const std::vector<std::string>& desiredRoiList,
                                            QString totalDoseHeaderFilePath,
                                            QString totalDoseImageFilePath,
                                            QString totalDoseImageHeaderFilePath ,
                                            QString resultFolder,
                                            float sysTranSigmaX,
                                            float sysTranSigmaY,
                                            float sysTranSigmaZ,
                                            float randomTranSigmaX,
                                            float randomTranSigmaY,
                                            float randomTranSigmaZ,
                                            float sysRotSigmaX,
                                            float sysRotSigmaY,
                                            float sysRotSigmaZ,
                                            float randomRotSigmaX,
                                            float randomRotSigmaY,
                                            float randomRotSigmaZ,
                                            int numFrac,
                                            int numSimulation,
                                            bool debug)
{

    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setVebose(debug); // if debug is on verbisty will be on as well
    // 1) read a patinet filea and its imagesSets
    myPinnacleFile.setPatientFilePath(patientPath.toStdString());

    if (myPinnacleFile.readPatientFileAndImageSets()!=0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
        return;
    }
    if (debug)
    {
        for (int iImageSet = 0; iImageSet < myPinnacleFile.Patient.ImageSetList.size(); ++iImageSet)

        {
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/ImageSetXGridData_"+ std::to_string(myPinnacleFile.Patient.PatientID)+
                                      "ImageSet"+myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".bin",      myPinnacleFile.Patient.ImageSetList[iImageSet].volumeXGridDataVectorInCm);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/ImageSetYGridData_"+ std::to_string(myPinnacleFile.Patient.PatientID)+
                                      "ImageSet"+myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".bin",      myPinnacleFile.Patient.ImageSetList[iImageSet].volumeYGridDataVectorInCm);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/ImageSetZGridData_"+ std::to_string(myPinnacleFile.Patient.PatientID)+
                                      "ImageSet"+myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".bin",      myPinnacleFile.Patient.ImageSetList[iImageSet].volumeZGridDataVectorInCm);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/ImageSetPixelData_"+ std::to_string(myPinnacleFile.Patient.PatientID)+
                                      "ImageSet"+myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".bin",          myPinnacleFile.Patient.ImageSetList[iImageSet].volumeDataUint16);
        }

    }

    // 2) read ROIs for plan.roi
    myPinnacleFile.roisPropVector.clear();
    myPinnacleFile.setRoiFilePath(planDotRoiPath.toStdString());
    if (OKFlag != myPinnacleFile.readRoisProperties())
    {
        std::cout << "ERROR: in reading Roi properties" << std::endl;
        return;
    }

    std::cout << "\nroiPropVector has "  << myPinnacleFile.roisPropVector.size() << " members" << std::endl;

    // Loading data for an specific ROI with a given name
    std::vector<int> allroiIndices = generateIntRange(0,1,myPinnacleFile.roisPropVector.size()-1);


    std::vector<int> desiredRoiIndicesInRoisPropVector;
    for (int iRoi = 0; iRoi < desiredRoiList.size(); ++iRoi)
    {
        std::string roiNameToLoadData = desiredRoiList[iRoi];
        auto itRoi = std::find_if(myPinnacleFile.roisPropVector.begin(), myPinnacleFile.roisPropVector.end(),
                                  [&roiNameToLoadData] (const roi_prop& rp) { return 0 ==rp.name.compare(roiNameToLoadData); }) ;
        int roiIndexInRoisPropVector=std::distance( myPinnacleFile.roisPropVector.begin(),itRoi);
        desiredRoiIndicesInRoisPropVector.push_back(roiIndexInRoisPropVector);
        if (roiIndexInRoisPropVector >= myPinnacleFile.roisPropVector.size())
        {
            std::cout << "No ROI in plan.roi file with name= " << roiNameToLoadData  << ",  ignoring this ROI." << std::endl;
            continue;
        }
    }

    std::vector<int> roiIndicesToBeExcluded;
    // delete desired desiredRoiIndicesInRoisPropVector elements from allroiIndices and put the result in roiIndicesToBeExcluded
    std::set_difference(allroiIndices.begin(), allroiIndices.end(), desiredRoiIndicesInRoisPropVector.begin(), desiredRoiIndicesInRoisPropVector.end(),std::inserter(roiIndicesToBeExcluded, roiIndicesToBeExcluded.begin()));

    //  now erase  roiIndicesToBeExcluded from myPinnacleFile.roisPropVector
    myPinnacleFile.roisPropVector.erase(ToggleIndices(myPinnacleFile.roisPropVector,std::begin(roiIndicesToBeExcluded), std::end(roiIndicesToBeExcluded)),myPinnacleFile.roisPropVector.end());


    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {

        if (OKFlag != myPinnacleFile.readRoiDataForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.createBitmapForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.calculateCenterOfMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (debug)
        {
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointIndicesX_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesX);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointIndicesY_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesY);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointIndicesZ_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesZ);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointCoordinatesX_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointCoordinatesY_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY);
            writeVectorToBinaryFileQT(resultFolder.toStdString() + "/binaryBitmapPointCoordinatesZ_" + myPinnacleFile.roisPropVector[iRoi].name+ '_' + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ);
        }

    }


    // 2.5) calculated voxel mass --> TODO: DMH will be optinal in next version for this version I need them for QA purpose therefore mass needs to be calculated
    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if   (OKFlag !=  myPinnacleFile.createBitmapVoxelMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            std::cout << "ERROR:  in  running createBitmapVoxelMassForGivenRoiName for ROI: " << myPinnacleFile.roisPropVector[iRoi].name << std::endl;

        }
        if (verbose)
        {
            std::cout << "Roi[" << myPinnacleFile.roisPropVector[iRoi].name << "].mass = " << myPinnacleFile.roisPropVector[iRoi].mass<< std::endl;
        }
    }

    //2.75) identify target volume and normal tissues automatically

    if (OKFlag != myPinnacleFile.deduceRoiTypeFromName())
    {
        std::cout << "ERROR: This is not what we expected" << std::endl;
        return;
    }


    // 3) Setting up uncertainty model
    std::cout  << "... Instantiating ModelData ... " <<std::endl;
    ModelData* modelData = new ModelData(std::cout,myPinnacleFile.roisPropVector); // // It automatically creates GUMMODELMap
    std::string ROINameForCenterOfRotation ;
    bool atLeastOneTargetBool = false;

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
           if (myPinnacleFile.roisPropVector[iRoi].IsTargetVolume)
           {
               ROINameForCenterOfRotation = myPinnacleFile.roisPropVector[iRoi].name;
               atLeastOneTargetBool= true;
           }
    }

    // if there is no target we choose the first ROI name for COM
    if(!atLeastOneTargetBool)
    {
        ROINameForCenterOfRotation = myPinnacleFile.roisPropVector[0].name;
    }


    std::string RoiToRotateAroundCOMString =ROINameForCenterOfRotation;
    // 4) setting up rigidbody motion
    modelData->rigidBodyMotionModel =new RigidBodyMotionModel(std::cout);
    modelData->rigidBodyMotionModel->DoseConvFlag= false;
    modelData->rigidBodyMotionModel->rotCenter.RotationCenterType=RotationCenter::ROICenterOfMass;
    modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM =RoiToRotateAroundCOMString;
    modelData->rigidBodyMotionExist = true;

    modelData->rigidBodyMotionModel->sysTranErrorSigma.x =sysTranSigmaX ;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.y =sysTranSigmaY ;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.z =sysTranSigmaZ ;

    modelData->rigidBodyMotionModel->randTranErrorSigma.x =randomTranSigmaX;
    modelData->rigidBodyMotionModel->randTranErrorSigma.y =randomTranSigmaY;
    modelData->rigidBodyMotionModel->randTranErrorSigma.z =randomTranSigmaZ ;

    modelData->rigidBodyMotionModel->sysRotErrorSigma.x =sysRotSigmaX;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.y =sysRotSigmaY;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.z = sysRotSigmaZ;

    modelData->rigidBodyMotionModel->randRotErrorSigma.x =randomRotSigmaX;
    modelData->rigidBodyMotionModel->randRotErrorSigma.y =randomRotSigmaY;
    modelData->rigidBodyMotionModel->randRotErrorSigma.z = randomRotSigmaZ;

    // 5) setting up Rotation and transformation of indiviual ROI
    RotationAndTranslationModel rotTransModel = RotationAndTranslationModel(std::cout);
    rotTransModel.sysRotErrorSigma.x = 0;
    rotTransModel.sysRotErrorSigma.y = 0;
    rotTransModel.sysRotErrorSigma.z = 0;
    rotTransModel.sysTranErrorSigma.x = 0;
    rotTransModel.sysTranErrorSigma.y = 0;
    rotTransModel.sysTranErrorSigma.z = 0;
    rotTransModel.rotCenter.RoiIDForRotationAroundCOM = ROINameForCenterOfRotation;
    rotTransModel.rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator  iter =modelData->gumMap.find(ROINameForCenterOfRotation);
    if (iter != modelData->gumMap.end())
    {
        boost::get<0>(iter->second) = rotTransModel;
    }
    else
    {
        std::cout << "ERROR/Warning rotTranModel for " << ROINameForCenterOfRotation << " is not created, since it was not in the gumap"  << std::endl;

    }

    // 6) Reading a dose file
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    rtDosePinnTotalDose.setDoseHeaderFilePath(totalDoseHeaderFilePath.toStdString());
    rtDosePinnTotalDose.setDoseImageFilePath(totalDoseImageFilePath.toStdString());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(totalDoseImageHeaderFilePath.toStdString());

    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }

    if (debug)
    {
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseArrayCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  rtDosePinnTotalDose.doseArrayCoordinatesX );
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseArrayCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  rtDosePinnTotalDose.doseArrayCoordinatesY );
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseArrayCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin", rtDosePinnTotalDose.doseArrayCoordinatesZ );

        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseGridCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  rtDosePinnTotalDose.doseGridXvalues);
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseGridCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  rtDosePinnTotalDose.doseGridYvalues );
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseGridCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin", rtDosePinnTotalDose.doseGridZvalues );

        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/TotalDoseArray_"+ std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + ".bin",  rtDosePinnTotalDose.doseArray );
    }
    // 7) assign the dose to the pinnacle patient
    myPinnacleFile.setRtDosePinn(&rtDosePinnTotalDose);

    // 8) Initialze RobustAnalyzer
    // Calculation engine is instantiated here !
    RobustnessAnalyzer * robustnessAnalyzer = new RobustnessAnalyzer(std::cout);
    robustnessAnalyzer->setPinnacleFlag(true);
    robustnessAnalyzer->setModelData(modelData);
    robustnessAnalyzer->setPinnacleFiles(&myPinnacleFile);
    robustnessAnalyzer->setNumFractions(numFrac);
    robustnessAnalyzer->setNumDoseBins(350);
    robustnessAnalyzer->setNumTrials(numSimulation);
    robustnessAnalyzer->setGPUUsage(true);

    if (!robustnessAnalyzer->generateTransformationsParamsPinnacle())
    {
        std::cout  <<   " Error in generateTransformationsParams ." << std::endl;

        return;
    }
    //     std::cout << "Patient" << std::to_string(patientNum) << " transformations has been created in robustnessAnalyzer  for dose  " <<  rtDose.doseCalcImageSet <<  " plan and Scan_" + std::to_string(scanFolderNumber) + " at " + nowInString() << std::endl;
    //if gpu device is chosen for computation
    if (!robustnessAnalyzer->calculateAllDVHSonGpuForPinnaclePatient()) // in this version it calculated both DVHs and DMHs both
    {

        std::cout  <<  " Error in Calculation PDVHs on GPU(s)." << std::endl;
        return;
    }

    if (!robustnessAnalyzer->calculateDchs())
    {
        std::cout  <<  " Error in Calculation Dchs." << std::endl;
        return;
    }

    std::cout << " all Dchs has been calculated in robustnessAnalyzer "<< std::endl;
    std::cout  <<  "Initialize Confidence Level for PDVHs based on the ROI's' type " <<std::endl;
    if (!robustnessAnalyzer->calculatePDVHsConfLevelInitialValues())
    {
        return ; // no further comment is needed
    }

    std::cout  <<  "Writing DCHs to file... " <<std::endl;
    if (!robustnessAnalyzer->writeAllDchsToFile())
    {
        std::cout <<  "ERORR --> in Writing DCHs to file" << std::endl;
        return;
    }

    std::cout  <<  "calculateDVHsMeanAndVarianceMedianPercentiles... " <<std::endl;
    if (!robustnessAnalyzer->calculateDVHsMeanAndVarianceMedianPercentiles())
    {
        std::cout <<  "ERORR --> in calculateDVHsMeanAndVarianceMedianPercentiles  " << std::endl;
        return;
    }

    std::cout  <<  "Writing Planed Dvhs to file... " <<std::endl;
    if (!robustnessAnalyzer->WritePlannedDVHS())
    {
        std::cout <<  "ERORR --> in Writing DCHs to file" << std::endl;
        return;
    }
    writeVectorToBinaryFileQT(resultFolder.toStdString() + "/DoseBinCenters_Patient_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + myPinnacleFile.rtDosePinn->doseCalcImageSet  + ".bin", robustnessAnalyzer->doseBinCenters);

    for (int iRoi = 0; iRoi < robustnessAnalyzer->pinnacleFiles->roisPropVector.size(); ++iRoi)
    {
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/cDMH_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + myPinnacleFile.rtDosePinn->doseCalcImageSet  +"_roi_"+ robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name +".bin", robustnessAnalyzer->cDMHForTrial[iRoi]);
        writeVectorToBinaryFileQT(resultFolder.toStdString() + "/diffDMH_" + std::to_string(myPinnacleFile.Patient.PatientID)+ "_Plan_" + myPinnacleFile.rtDosePinn->doseCalcImageSet  +"_roi_"+ robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name +".bin", robustnessAnalyzer->diffDMHForTrial[iRoi]);

    }
    delete(modelData);
    delete(robustnessAnalyzer);
    std::cout<< "Done !" << std::endl;

}

void pinnacleFuncWrapper::setInitialParamsForApplication()
{
    //1) initial all pointers

    //2) initial application default paramaters from setting
    QSettings settings("UVA","ًًُRTRA");
    patientCurrentFolderQString = settings.value("patientCurrentFolderQString",QDir::currentPath()).toString();
}

void pinnacleFuncWrapper::listRoisForGivenPatient(QString patientPath, QString planDotRoiPath)
{
    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setPatientFilePath(patientPath.toStdString());

    //myPinnacleFile.setVebose(true);
    if (myPinnacleFile.readPatientFileAndImageSets()!=0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }


    myPinnacleFile.setRoiFilePath(planDotRoiPath.toStdString());
    myPinnacleFile.readRoisProperties();

    std::cout << "\nroiPropVector has "  << myPinnacleFile.roisPropVector.size() << " members" << std::endl;
    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        std::cout <<std::setw(10)<< "Roi[" << std::to_string(iRoi) << "] = " << myPinnacleFile.roisPropVector[iRoi].name  <<std::setw(20) << "ImageSet ->  " <<   myPinnacleFile.roisPropVector[iRoi].volume_name << std::endl;
    }

}
