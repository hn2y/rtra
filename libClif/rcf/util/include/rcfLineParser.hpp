#ifndef __rcfLineParser
#define __rcfLineParser

/**
    \class rcfLineParser
    @ingroup UTIL
*/

#include <string>
#include <vector>
#include "cfGenDefines.h"

class rcfLineParser
{
   protected:

      int ntokens;
      int curtokens;
      std::string delim;
      bool delim_set;
      std::vector<std::string> tkn_head;
      bool head_set;
      bool ready;
      bool full;
      char *func;
      bool keepSpacesFlag;

   public:

      rcfLineParser();
      rcfLineParser(unsigned int ntokens);
      rcfLineParser(char *del_str);
      rcfLineParser(unsigned int ntokens,char *del_str);
      ~rcfLineParser();

      int setDelimiter(char *del_str);
      int setTokens(unsigned int utokens);
      int parseLine(char *line);
      int parseLine(std::string line);
      int getToken(int indx, char** token);
      std::string getToken(int indx, int *valid);
      int getNumberOfTokens(int *ntokens);
      int isReady();
      void keepSpaces();

  protected:

      void reportError(const char *str);
      int tokenizeLine(std::string line);

  private:

      void clearLine();
      void removeSpaces(std::string &);
      void basicInit();
      
};

#endif
