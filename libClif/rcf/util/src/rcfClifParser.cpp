#include "rcfClifParser.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <ctype.h> // for isspace

void rcfClifParser::basicInit()
{
   func = (char*)"rcfClifParser::basicInit";

   memset(&myclif,0,sizeof(myclif));
   clif_ready = 0;
   debug_flag = 0;
   myerrno = 0;
   fid_out = NULL;
   sel_level = 0;
   wrk_clif = NULL;
   object_found = 0;
   search_level = 0;
   open_for_write = 0;
   iterator = false;
   iterators.clear();
   block_iterators.clear();
   unhandled_block_iterators.clear();
   top_clif = &myclif;
}

rcfClifParser::rcfClifParser()
{
   func = (char*)"rcfClifParser::rcfClifParser";
   basicInit();
}

void rcfClifParser::buildParser(char *file, char *handle)
{
   func = (char*)"rcfClifParser::rcfClifParser_char*";
   int rtn;

   basicInit();
   if(NULL == file)
   {
      reportError("User passed NULL pointer in lieu of file name");
      return;
   }
   if(0 == strlen(file) )
   {
      reportError("User passed Empty String in lieu of file name");
      return;
   }
   // open the file
   FILE *lfid = NULL;
   lfid = fopen(file,"r");
   if(NULL == lfid)
   {
      fprintf(stderr," ERROR: %s : Failed to open clif file %s \n", func, file);
      //reportError("ERROR: Failure to open Clif file specified by user ",file);
      return;
   }
   rtn = readClifFile(lfid,&myclif,handle);
   if( CF_OK != rtn )
   {
      reportError(" ERROR: buildParser: Failure to process Clif file specified by user",file);
   }
   else 
   {
      clif_ready = 1;
      wrk_clif = &myclif;
   }

   fclose(lfid);
   
}

rcfClifParser::rcfClifParser(char *file, char *handle)
{
   buildParser(file,handle);
}
rcfClifParser::rcfClifParser(char *file)
{
   buildParser(file,NULL);
}

rcfClifParser::~rcfClifParser()
{
   func = (char*)"rcfClifParser::~rcfClifParser";
   if(false == iterator)
   {
       freeClifStructure(&myclif);
   }
   if(NULL != fid_out)
   {
       fclose(fid_out);
   }
   for(unsigned int i=0; i<iterators.size(); i++) 
   {
       if(NULL != iterators[i])
       {
           delete iterators[i];
           iterators[i] = NULL;
       }
   }
   for(unsigned int i=0; i<block_iterators.size(); i++) 
   {
       if(NULL != block_iterators[i])
       {
           delete block_iterators[i];
           block_iterators[i] = NULL;
       }
   }
}

int rcfClifParser::isClifReady()
{
   return(clif_ready);
}

int rcfClifParser::selectTopClifToWrite(char *usrHandle)
{
   func = (char*)"rcfClifParser::selectTopClifToWrite";

   if(1 == clif_ready)
   {
      reportError("You can not build clif programmatically if the clif was read from file");
      return(CF_FAIL);
   }
   if(true == iterator)
   {
      reportError("You can not build clif programmatically if the clif is an iterator");
      return(CF_FAIL);
   }
   open_for_write = 1;

   int length;
   wrk_clif = top_clif;
   if(NULL == usrHandle)return(CF_OK);
   if(0 == (length = strlen(usrHandle)) )return(CF_OK);
   // note: use C style memory allocation, as legacy code is
   // used to deallocate clif.
   wrk_clif->Handle = (char*)calloc(length+1,sizeof(char));
   if(NULL == wrk_clif->Handle)
   {
      reportError("Failure to allocate memory");
      return(CF_FAIL);
   }
   strcpy(wrk_clif->Handle,usrHandle);
   return(CF_OK);
}
int rcfClifParser::selectTopClifToWrite()
{
   return(selectTopClifToWrite(NULL));
}

int rcfClifParser::addObjectArrayToSelectedClif(int nobjs,int select)
{
   func = (char*)"rcfClifParser::addClifArrayToSelectedClif";
   if(0 == open_for_write)
   {
      reportError("You can not modify clif unless it has been opened for writing");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("You can not modify clif unless a clif has been selected");
      return(CF_FAIL);
   }
   if(nobjs <= 0)
   {
      reportError("A request to add zero objects. Check your code ...");
      return(CF_FAIL);
   }
   switch(select)
   {
      case 0:
         if( (wrk_clif->nObjects > 0) || (NULL != wrk_clif->Object) )
         {
            reportError("You can not overwrite existing array in this function");
            return(CF_FAIL);
         }
         wrk_clif->Object = (clifObjectType*)calloc(nobjs,sizeof(clifObjectType));
         if(NULL == wrk_clif->Object)
         {
            reportError("Failure to allocate memory for object array");
            return(CF_FAIL);
         }
         wrk_clif->nObjects = nobjs;
         break;
      case 1:
         if( (wrk_clif->nDataObjects > 0) || (NULL != wrk_clif->Data) )
         {
            reportError("You can not overwrite existing array in this function");
            return(CF_FAIL);
         }
         wrk_clif->Data = (clifDataType*)calloc(nobjs,sizeof(clifDataType));
         if(NULL == wrk_clif->Data)
         {
            reportError("Failure to allocate memory for object array");
            return(CF_FAIL);
         }
         wrk_clif->nDataObjects = nobjs;
         break;
      case 2:
         if( (wrk_clif->nArrayObjects > 0) || (NULL != wrk_clif->Array) )
         {
            reportError("You can not overwrite existing array in this function");
            return(CF_FAIL);
         }
         wrk_clif->Array = (clifArrayType*)calloc(nobjs,sizeof(clifArrayType));
         if(NULL == wrk_clif->Array)
         {
            reportError("Failure to allocate memory for object array");
            return(CF_FAIL);
         }
         wrk_clif->nArrayObjects = nobjs;
         break;
      default:
         reportError("Illegal selector value (must be 0,1,2)");
         return(CF_FAIL);
         break;
    
   }
   return(CF_OK);
}

int rcfClifParser::addClifArrayToSelectedClif(int nobjs)
{
   return(addObjectArrayToSelectedClif(nobjs,0));
}
int rcfClifParser::addDataObjectArrayToSelectedClif(int nobjs)
{
   return(addObjectArrayToSelectedClif(nobjs,1));
}
int rcfClifParser::addArrayObjectArrayToSelectedClif(int nobjs)
{
   return(addObjectArrayToSelectedClif(nobjs,2));
}

int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,bool Data)
{
    char lbuffer[6]; //local buffer
    if( Data )
    {
        sprintf(lbuffer,"true");
    }
    else
    {
        sprintf(lbuffer,"false");
    }
   
    return(setDataObjectInSelectedClifSelect(nobj,usrHandle,lbuffer,1,NULL)); 
}

int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,unsigned long Data)
{
    char lbuffer[500] = {0}; //local buffer, 500 digit buffer should be enough
    sprintf(lbuffer,"%ld",Data);
    return(setDataObjectInSelectedClifSelect(nobj,usrHandle,lbuffer,1,NULL));
}

int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,int Data)
{
    char lbuffer[500] = {0}; //local buffer, 500 digit buffer should be enough
    memset(&lbuffer, 0, 500);
    sprintf(lbuffer,"%d",Data);
    return(setDataObjectInSelectedClifSelect(nobj,usrHandle,lbuffer,1,NULL));
}
int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,float Data, int precision)
{
    char lbuffer[500] = {0}; //local buffer, 500 digit buffer should be enough

   // set floating point precision
   if( precision > 0 )
   {
        sprintf(lbuffer,"%.*f", precision, Data);        
   }
   // default to no precision formating
   else
   {
       sprintf(lbuffer,"%f",Data);
   }
   return(setDataObjectInSelectedClifSelect(nobj,usrHandle,lbuffer,1,NULL)); 
}
int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,char *Data)
{
   return(setDataObjectInSelectedClifSelect(nobj,usrHandle,Data,0,NULL)); 
}
int rcfClifParser::setDataObjectInSelectedClif(int nobj,char *usrHandle,char *Data,char *usr_delim)
{
   return(setDataObjectInSelectedClifSelect(nobj,usrHandle,Data,2,usr_delim)); 
}

// set data field in a DataObject. This is a private work function.
// Above, are public interface wrapper function.
int rcfClifParser::setDataObjectInSelectedClifSelect(int nobj,char *usrHandle,char *Data,int select,char *usr_delim)
{
   func = (char*)"rcfClifParser::setDataObjectInSelectedClif";
   int hlength,dlength;

   // sanity checks

   if(0 == open_for_write)
   {
      reportError("You can not modify clif unless it has been opened for writing");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("You can not modify clif unless a clif has been selected");
      return(CF_FAIL);
   }
   if( (nobj < 0) || (nobj >= wrk_clif->nDataObjects) )
   {
      reportError("Object index Out of bounds");
      fprintf(stderr,"index passed: %d  limits: 0 - %d\n",nobj,wrk_clif->nDataObjects-1);
      return(CF_FAIL);
   }
   // check user handle. User Handle is mandatory for this function
   if(NULL == usrHandle)
   {
      reportError("NULL pointer instead of Handle");
      return(CF_FAIL);
   }
   if(0 == (hlength = strlen(usrHandle)) )
   {
      reportError("empty string passed instead of Handle");
      return(CF_FAIL);
   }
   // check selector validity
   switch(select)
   {
      case 0:
      case 1:
      case 2:
         break;
      default:
         reportError("Invalid Selector Value");
         fprintf(stderr,"selector: %d\n",select);
         break;
   }
   // allocate handle and copy
   clifDataType *dptr = NULL;
   dptr = (wrk_clif->Data) + nobj;
   if(NULL != dptr->Handle)
   {
      free(dptr->Handle);
      dptr->Handle = NULL;
   }
   if(NULL != dptr->Value)
   {
      free(dptr->Value);
      dptr->Value = NULL;
   }
   memset(dptr->Delim,0,sizeof(dptr->Delim));
   switch(select)
   {
      case 0:
         dptr->isquote = 1;
         break;
      case 1:
         dptr->isquote = 0;
         break;
      case 2:
         if(NULL == usr_delim)
         {
            reportError("Selector Value indicates delimiter must be set, but no delimiter is specified (NULL pointer)");
            return(CF_FAIL);
         }
         if( 0 == strlen(usr_delim) )
         {
            reportError("Selector Value indicates delimiter must be set, but no delimiter is specified");
            return(CF_FAIL);
         }
         if( strlen(usr_delim) > MAXDELIM-1)
         {
            reportError("Delimiter specification too long");
            fprintf(stderr,"string seen: %s  length: %ld  maximum allowed: %d\n",usr_delim,strlen(usr_delim),MAXDELIM-1);
            return(CF_FAIL);
         }
         strcpy(dptr->Delim,usr_delim);
         break;
      default:
         reportError("Invalid Selector Value");
         fprintf(stderr,"selector: %d\n",select);
         break;
   }
   dptr->Handle = (char*)calloc(hlength+1,sizeof(char));
   if(NULL == dptr->Handle)
   {
      reportError("Failure to allocate memory for Handle");
      return(CF_FAIL);
   }
   strcpy(dptr->Handle,usrHandle);

   if(NULL == Data)
   {
      dptr->Value = (char*)calloc(1,sizeof(char));
      if(NULL == dptr->Value)
      {
         reportError("Failure to allocate memory for value (one byte) ");
         return(CF_FAIL);
      }
      return(CF_OK); // OK, data may be empty on purpose
   }
   // there is data
   if(0 == (dlength = strlen(Data)) )
   {
      // nothing in there, its ok
      return(CF_OK);
   }
   dptr->Value = (char*)calloc(dlength+1,sizeof(char));
   if(NULL == dptr->Value)
   {
      reportError("Failure to allocate memory for Value");
      return(CF_FAIL);
   }
   strcpy(dptr->Value,Data);

   return(CF_OK); 
}

int rcfClifParser::setArrayObjectInSelectedClif(int nobj,char *usrHandle,float *buffer, int length)
{
   func = (char*)"rcfClifParser::setArrayObjectInSelectedClif";
   int hlength;

   // sanity checks

   if(0 == open_for_write)
   {
      reportError("You can not modify clif unless it has been opened for writing");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("You can not modify clif unless a clif has been selected");
      return(CF_FAIL);
   }
   if( (nobj < 0) || (nobj >= wrk_clif->nArrayObjects) )
   {
      reportError("Object index Out of bounds");
      fprintf(stderr,"index passed: %d  limits: 0 - %d\n",nobj,wrk_clif->nArrayObjects-1);
      return(CF_FAIL);
   }
   // check user handle. User Handle is mandatory for this function
   if(NULL == usrHandle)
   {
      reportError("NULL pointer instead of Handle");
      return(CF_FAIL);
   }
   if(0 == (hlength = strlen(usrHandle)) )
   {
      reportError("empty string passed instead of Handle");
      return(CF_FAIL);
   }
   // allocate handle and copy
   clifArrayType *dptr = NULL;
   dptr = (wrk_clif->Array) + nobj;
   if(NULL != dptr->Handle)
   {
      free(dptr->Handle);
      dptr->Handle = NULL;
   }
   if(NULL != dptr->Value)
   {
      free(dptr->Value);
      dptr->Value = NULL;
   }
   dptr->Handle = (char*)calloc(hlength+1,sizeof(char));
   if(NULL == dptr->Handle)
   {
      reportError("Failure to allocate memory for Handle");
      return(CF_FAIL);
   }
   strcpy(dptr->Handle,usrHandle);

   if(NULL == buffer)return(CF_OK); // OK, data may be empty on purpose
   // there is data
   if(length <= 0 )
   {
      reportError("Illegal buffer length, must be greater than 0");
      return(CF_FAIL);
   }
   dptr->Value = (float*)calloc(length,sizeof(float));
   if(NULL == dptr->Value)
   {
      reportError("Failure to allocate memory for Value");
      return(CF_FAIL);
   }
   memcpy(dptr->Value,buffer,length*sizeof(float));
   dptr->nValues = length;

   return(CF_OK); 
}

int rcfClifParser::setHandleInSelectedClif(char *usrHandle)
{
   func = (char*)"rcfClifParser::setHandleInSelectedClif";
   int hlength;

   // sanity checks

   if(0 == open_for_write)
   {
      reportError("You can not modify clif unless it has been opened for writing");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("You can not modify clif unless a clif has been selected");
      return(CF_FAIL);
   }
   if(NULL == usrHandle)
   {
      reportError("NULL pointer passed instead of Handle");
      return(CF_FAIL);
   }
   if(0 == (hlength = strlen(usrHandle)) )
   {
      reportError("empty string passed instead of Handle");
      return(CF_FAIL);
   }
   if(NULL != wrk_clif->Handle )
   {
      free(wrk_clif->Handle);
      wrk_clif->Handle = NULL;
   }
   wrk_clif->Handle = (char*)calloc(hlength+1,sizeof(char));
   if(NULL == wrk_clif->Handle)
   {
      reportError("Failure to allocate memory for a new Handle");
      return(CF_FAIL);
   }
   strcpy(wrk_clif->Handle,usrHandle);
   return(CF_OK);
}

int rcfClifParser::readClifFromFile(char *file, char *handle)
{
   func = (char*)"rcfClifParser::readClif";
   int rtn;

   basicInit();
   if(NULL == file)
   {
      reportError("User passed NULL pointer in lieu of file name");
      return(CF_FAIL);
   }
   if(0 == strlen(file) )
   {
      reportError("User passed Empty String in lieu of file name");
      return(CF_FAIL);
   }
   // open the file
   FILE *lfid = NULL;
   lfid = fopen(file,"r");
   if(NULL == lfid)
   {
      reportError("Failure to open Clif file specified by user",file);
      return(CF_FAIL);
   }
   rtn = readClifFile(lfid,&myclif,handle);
   if( CF_OK != rtn )
   {
      reportError("ERROR: Failure to process Clif file specified by user",file);
      fclose(lfid);
      return(rtn);
   }
   fclose(lfid);
   clif_ready = 1;
   wrk_clif = &myclif;
   return(CF_OK);
   
}

void rcfClifParser::setDebug()
{
   debug_flag = 1;
}
void rcfClifParser::setDebugSearch()
{
   debug_flag = 2;
}

int rcfClifParser::getClifLevel(int *nlevel)
{
   func = (char*)"rcfClifParser::getClifLevel";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   *nlevel = top_clif->clifLevel;
   return(CF_OK);
}
int rcfClifParser::getNumberOfClifObjects(int *nobjs)
{
   func = (char*)"rcfClifParser::getNumberOfClifObjects";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   *nobjs = top_clif->nObjects;
   return(CF_OK);
}
int rcfClifParser::getNumberOfArrayObjects(int *narrays)
{
   func = (char*)"rcfClifParser::getNumberOfArrayObjects";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   *narrays = top_clif->nArrayObjects;
   return(CF_OK);
}
int rcfClifParser::getNumberOfDataObjects(int *ndata)
{
   func = (char*)"rcfClifParser::getNumberOfDataObjects";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   *ndata = top_clif->nDataObjects;
   return(CF_OK);
}

int rcfClifParser::dumpClif()
{
   func = (char*)"rcfClifParser::dumpClif";
   if( (1 != clif_ready) && (1 != open_for_write) )
   {
      reportError("Request to dump clif, but clif is not ready");
      return(CF_FAIL);
   }
   else
   {
      return(dumpClifStructure(top_clif));
   }
   return(CF_OK);
}
int rcfClifParser::dumpSelectedClif()
{
   func = (char*)"rcfClifParser::dumpSelectedClif";
   if( (1 != clif_ready) && (1 != open_for_write) )
   {
      reportError("Request to dump clif, but clif is not ready");
      return(CF_FAIL);
   }
   else
   {
      if(NULL != wrk_clif)
      {
         return(dumpClifStructure(wrk_clif));
      }
      else
      {
         return(dumpClifStructure(top_clif));
      }
   }
   return(CF_OK);
}
char* rcfClifParser::getHandleOfSelectedClif(int *verify)
{
   func = (char*)"rcfClifParser::getHandleOfSelectedClif";
   *verify = 0;
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   if(NULL != wrk_clif->Handle)*verify = 1;
   return(wrk_clif->Handle);
}

int rcfClifParser::getNumberOfClifObjectsInSelectedClif(int *nobjs)
{
   func = (char*)"rcfClifParser::getNumberOfClifObjectsInSelectedClif";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(CF_FAIL);
   }
   *nobjs = wrk_clif->nObjects;
   return(CF_OK);
}
int rcfClifParser::getNumberOfArrayObjectsInSelectedClif(int *narrays)
{
   func = (char*)"rcfClifParser::getNumberOfArrayObjectsInSelectedClif";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(CF_FAIL);
   }
   *narrays = wrk_clif->nArrayObjects;
   return(CF_OK);
}
int rcfClifParser::getNumberOfDataObjectsInSelectedClif(int *ndata)
{
   func = (char*)"rcfClifParser::getNumberOfDataObjectsInSelectedClif";
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(CF_FAIL);
   }
   *ndata = wrk_clif->nDataObjects;
   return(CF_OK);
}
char* rcfClifParser::getHandleOfArrayInSelectedClif(int narray,int *verify)
{
   func = (char*)"rcfClifParser::getHandleOfArrayInSelectedClif";
   *verify = 0;
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   if( narray < 0)
   {
      reportError("Array index can not be negative");
      return(NULL);
   }
   if( narray >= wrk_clif->nArrayObjects)
   {
      reportError("Array index too large");
      return(NULL);
   }
   *verify = 1;
   return(wrk_clif->Array[narray].Handle);
}
float* rcfClifParser::getArrayBufferInSelectedClif(int narray,int *length,int *verify)
{
   func = (char*)"rcfClifParser::getArrayBufferInSelectedClif";
   *verify = 0;
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   if( narray < 0)
   {
      reportError("Array index can not be negative");
      return(NULL);
   }
   if( narray >= wrk_clif->nArrayObjects)
   {
      reportError("Array index too large");
      return(NULL);
   }
   *verify = 1;
   *length = wrk_clif->Array[narray].nValues;
   return(wrk_clif->Array[narray].Value);
}

float* rcfClifParser::getArrayBufferInSelectedClif(const char *usrHandle,int *length,int *verify)
{
   func = (char*)"rcfClifParser::getArrayBufferInSelectedClif_char";
   *verify = 0;
   int rtn;

   if(NULL == usrHandle)
   {
      reportError("NULL pointer instead of user handle");
      return(NULL);
   }
   if(0 == strlen(usrHandle))
   {
      reportError("Empty string instead of user handle");
      return(NULL);
   }
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   clifArrayType *lptr = NULL;
   int found = 0;
   rtn = searchForArrayObjectInClif(wrk_clif,usrHandle,&found,&lptr);
   if(CF_OK != rtn)return(NULL);
   else
   {
      if(1 == found)
      {
         if(NULL == lptr)
         {
            reportError("Object apparently found, but data pointer is empty");
            return(NULL);
         }
         else
         {
            *length = lptr->nValues;
            if(*length <= 0)
            {
               reportError("Object apparently found, but data pointer is empty");
               return(NULL);
            }
            if(NULL == lptr->Value)
            {
               reportError("Object apparently found, but data buffer is NULL");
               return(NULL);
            }
            *verify = 1;
            
            return(lptr->Value);
         }
      }
   }
   return(CF_OK);
}

int rcfClifParser::getDataBufferInSelectedClif(const char *usrHandle,int *out,int *verify)
{
   int lverify=0;
   char *rtn = NULL;

   *verify = 0;

   rtn = getDataBufferInSelectedClif(usrHandle,&lverify);
   if( (NULL == rtn) || (1 != lverify) )
   {
      // prior function call reported, this is just a wrapper
      return(CF_FAIL);
   }
   *out = atoi(rtn);
   *verify = 1;
   return(CF_OK);
}
int rcfClifParser::getDataBufferInSelectedClif(const char *usrHandle,unsigned long *out,int *verify)
{
   int lverify=0;
   char *rtn = NULL;

   *verify = 0;

   rtn = getDataBufferInSelectedClif(usrHandle,&lverify);
   if( (NULL == rtn) || (1 != lverify) )
   {
      // prior function call reported, this is just a wrapper
      return(CF_FAIL);
   }
   *out = atol(rtn);
   *verify = 1;
   return(CF_OK);
}

int rcfClifParser::getDataBufferInSelectedClif(const char *usrHandle,float *out,int *verify)
{
   int lverify=0;
   char *rtn = NULL;

   *verify = 0;

   rtn = getDataBufferInSelectedClif(usrHandle,&lverify);
   if( (NULL == rtn) || (1 != lverify) )
   {
      // prior function call reported, this is just a wrapper
      return(CF_FAIL);
   }
   *out = atof(rtn);
   *verify = 1;
   return(CF_OK);
}

char* rcfClifParser::getDataBufferInSelectedClif(const char *usrHandle,int *verify)
{
   func = (char*)"rcfClifParser::getDataBufferInSelectedClif_char";
   *verify = 0;
   int rtn;

   if(NULL == usrHandle)
   {
      reportError("NULL pointer instead of user handle");
      return(NULL);
   }
   if(0 == strlen(usrHandle))
   {
      reportError("Empty string instead of user handle");
      return(NULL);
   }
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   clifDataType *lptr = NULL;
   int found = 0;
   rtn = searchForDataObjectInClif(wrk_clif,usrHandle,&found,&lptr);
   if(CF_OK != rtn)return(NULL);
   else
   {
      if(1 == found)
      {
         if(NULL == lptr)
         {
            reportError("Object apparently found, but data pointer is empty");
            return(NULL);
         }
         else
         {
            if(NULL == lptr->Value)
            {
               reportError("Object apparently found, but data buffer is NULL");
               return(NULL);
            }
            *verify = 1;
            return(lptr->Value);
         }
      }
   }
   return(CF_OK);
}

bool rcfClifParser::downstringCompareDataBufferInSelectedClif(const char* usrHandle, const char* ucomp, bool *verify)
{
   func = (char*)"rcfClifParser::downstringCompareDataBufferInSelectedClif";
   *verify = false;
   if(NULL == usrHandle)
   {
      reportError("NULL pointer passed instead of user handle");
      return(false);
   }
   if(strlen(usrHandle) <= 0 )
   {
      reportError("empty string passed instead of user handle");
      return(false);
   }
   if(NULL == ucomp)
   {
      reportError("NULL pointer passed instead of comparison template");
      return(false);
   }
   if(strlen(ucomp) <= 0 )
   {
      reportError("empty string passed instead of user template");
      return(false);
   }
   int lverify;
   char *tbuf = NULL;
   tbuf = getDataBufferInSelectedClif(usrHandle,&lverify);
   if( (NULL == tbuf) || (0 == lverify) )
   {
      // assume not found, but data apparently valid
      *verify = true;
      return(false);
   }
   int length = strlen(tbuf);
   if(length <= 0)
   {
      // quit quietly, no way to judge what is happening here
      *verify = true;
      return(false);
   }
   char *cbuf = NULL;
   cbuf = new char[length+1];
   if(NULL == cbuf)
   {
      reportError("Failure to allocate temporary buffer");
      fprintf(stderr,"length of request: %d\n",length+1);
      return(false);
   }
   strcpy(cbuf,tbuf);
   for(int i=0; i<length; i++)cbuf[i] = tolower(cbuf[i]);
   bool result;
   if(0 == strcmp(cbuf,ucomp))result = true;
   else result = false;
   delete [] cbuf;
   *verify = true;
   return(result);
}
bool rcfClifParser::downstringCompare(std::string usr1, std::string usr2)
{
   int length1 = usr1.size();
   int length2 = usr2.size();
   if(length1 != length2)return(false);
   char *buf1 = NULL;
   char *buf2 = NULL;
   buf1 = new char[length1+1];
   buf2 = new char[length2+1];
   if( (NULL == buf1) || (NULL == buf2) )
   {
      fprintf(stderr,"ERROR:rcfClifParser::downstringCompare failure to allocate memory with combined length: %d\n",length1+length2+2);
      return(false);
   }
   strcpy(buf1,usr1.c_str());
   strcpy(buf2,usr2.c_str());
   for(int i=0; i<length1; i++)
   {
      buf1[i] = tolower(buf1[i]);   
      buf2[i] = tolower(buf2[i]);   
   }
   bool lret = strcmp(buf1,buf2);
   delete [] buf1;
   delete [] buf2;
   return(lret);
}

char* rcfClifParser::getHandleOfDataObjectInSelectedClif(int ndata,int *verify)
{
   func = (char*)"rcfClifParser::getHandleOfDataObjectInSelectedClif";
   *verify = 0;
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   if( ndata < 0)
   {
      reportError("Array index can not be negative");
      return(NULL);
   }
   if( ndata >= wrk_clif->nDataObjects)
   {
      reportError("Array index too large");
      return(NULL);
   }
   *verify = 1;
   return(wrk_clif->Data[ndata].Handle);
}

int rcfClifParser::getDataBufferInSelectedClif(int ndata,int *out,int *verify)
{
   int lverify=0;
   char *rtn = NULL;

   *verify = 0;

   rtn = getDataBufferInSelectedClif(ndata,&lverify);
   if( (NULL == rtn) || (1 != lverify) )
   {
      // prior function call reported, this is just a wrapper
      return(CF_FAIL);
   }
   *out = atoi(rtn);
   *verify = 1;
   return(CF_OK);
}
int rcfClifParser::getDataBufferInSelectedClif(int ndata,float *out,int *verify)
{
   int lverify=0;
   char *rtn = NULL;

   *verify = 0;

   rtn = getDataBufferInSelectedClif(ndata,&lverify);
   if( (NULL == rtn) || (1 != lverify) )
   {
      // prior function call reported, this is just a wrapper
      return(CF_FAIL);
   }
   *out = atof(rtn);
   *verify = 1;
   return(CF_OK);
}
char* rcfClifParser::getDataBufferInSelectedClif(int ndata,int *verify)
{
   func = (char*)"rcfClifParser::getDataBufferInSelectedClif";
   *verify = 0;
   if(0 == clif_ready)
   {
      reportError("clif is not ready");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("No clif has been selected");
      return(NULL);
   }
   if( ndata < 0)
   {
      reportError("Data index can not be negative");
      return(NULL);
   }
   if( ndata >= wrk_clif->nDataObjects)
   {
      reportError("Array index too large");
      return(NULL);
   }
   *verify = 1;
   return(wrk_clif->Data[ndata].Value);
}

int rcfClifParser::setFileOutput(char *file)
{
   func = (char*)"rcfClifParser::setFileOutput";
   if(NULL != fid_out)fclose(fid_out);
   if(NULL == file)
   {
      reportError("NULL pointer passed in lieu of file name");
      return(CF_FAIL);
   }
   if( 0 == strlen(file) )
   {
      reportError("NULL pointer passed in lieu of file name");
      return(CF_FAIL);
   }
   if(NULL != fid_out)fclose(fid_out);

   fid_out = fopen(file,"w");
   if(NULL == fid_out)
   {
      reportError("Failure to open specified output file",file);
      return(CF_FAIL);
   }
   return(CF_OK);
}
void rcfClifParser::closeFileOutput()
{
   if(NULL != fid_out)fclose(fid_out);
}
int rcfClifParser::selectClifObject(int nObject)
{
   func = (char*)"rcfClifParser::selectClifObject";
   clifObjectType *lptr = NULL;
   if(NULL != wrk_clif)lptr = wrk_clif;
   else lptr = top_clif;

   if(nObject < 0)
   {
      reportError("Object level must not be less than zero");
      return(CF_FAIL);
   }
   if(nObject >= lptr->nObjects)
   {
      reportError("Illegal clif selection request");
      if(NULL != lptr->Handle)fprintf(stderr,"currently selected clif handle: %s\n",lptr->Handle);
      else fprintf(stderr,"Apparently currently selected clif has null handle? \n");
      fprintf(stderr,"Requested selection level: %d\n",nObject);
      fprintf(stderr,"Available number of objects: %d\n",lptr->nObjects);
      fprintf(stderr,"Maximum selection level: %d\n",(lptr->nObjects)-1);
      return(CF_FAIL);
   }
   // all is well, check the pointer and assign
   wrk_clif = (lptr->Object) + nObject;
   if(NULL == wrk_clif)
   {
      reportError("Obtained NULL pointer. Serious object error");
      return(CF_FAIL);
   }
   return(CF_OK);
}

void rcfClifParser::resetClifObjectSelection()
{
   if(1 == clif_ready) wrk_clif = top_clif;
   else wrk_clif = NULL;
   object_found = 0;
   search_level = 0;
}

rcfClifParser* rcfClifParser::getIteratorOnSelectedClif(int *verify)
{
   func = (char*)"rcfClifParser::getIteratorOnSelectedClif";
   rcfClifParser *lparser = NULL;
   *verify = 0;
   // sanity checks
   if( 0 == clif_ready )
   {
      reportError("Can not obtain an iterator on invalid clif parser");
      return(NULL);
   }
   if(NULL == wrk_clif)
   {
      reportError("Can not obtain an iterator on invalid clif parser working clif pointer apparently not set");
      return(NULL);
   }
   // all is well, get the job done
   lparser = new rcfClifParser;
   if(NULL == lparser)
   {
      reportError("Failure to allocate memory for the iterator");
      return(NULL);
   }
   lparser->top_clif = wrk_clif;
   lparser->wrk_clif = wrk_clif;
   lparser->iterator = true;
   lparser->clif_ready = 1;
   iterators.push_back(lparser);
   *verify = 1;
   return(lparser);
}

rcfClifParser* rcfClifParser::getIteratorFromBlock(unsigned int index)
{
   func = (char*)"rcfClifParser::getIteratorFromBlock";
   // sanity check
   if( index >= block_iterators.size() )
   {
      reportError("Illegal index value, exceeds size of the block");
      fprintf(stderr,"Index value: %d  maximum allowed value: %lu\n",index,(block_iterators.size()-1));
      return(NULL);
   }
   return(block_iterators[index]);
}

int rcfClifParser::createSubordinateIteratorBlockOnSelectedClif(int *nobjects)
{
   func = (char*)"rcfClifParser::createSubordinateIteratorBlockOnSelectedClif";
   // sanity checks
   if( 0 == clif_ready )
   {
      reportError("Can not create subordinate iterator block on invalid clif parser");
      return(CF_FAIL);
   }
   if(NULL == wrk_clif)
   {
      reportError("Can not obtain an iterator on invalid clif parser working clif pointer apparently not set");
      return(CF_FAIL);
   }
   //check if there is something stored
   if(block_iterators.size() > 0)
   {
      for(unsigned int i = 0; i < block_iterators.size(); i++) 
      {
         if(NULL != block_iterators[i])
         {
           delete block_iterators[i];
           block_iterators[i] = NULL;
         }
      }
      block_iterators.clear();
   }

   rcfClifParser *lparser = NULL;
   clifObjectType *lptr = NULL;
   for(int i=0; i<wrk_clif->nObjects; i++)
   {
      lptr = (wrk_clif->Object) + i;
      if(NULL == lptr)
      {
         reportWarning("Unexpected NULL pointer associated with subordinate clif. This clif will be skipped");
         continue; 
      }
      lparser = NULL;
      lparser = new rcfClifParser;
      if(NULL == lparser)
      {
         reportError("Failure to allocate memory for the iterator");
         return(CF_FAIL);
      }
      lparser->top_clif = lptr;
      lparser->wrk_clif = lptr;
      lparser->iterator = true;
      lparser->clif_ready = 1;
      block_iterators.push_back(lparser);
   }
   // all is well, get the job done
   *nobjects = block_iterators.size();
   return(CF_OK);
}
int rcfClifParser::createUnhandledSubordinateIteratorBlockOnSelectedClif(int *nobjects)
{
   func = (char*)"rcfClifParser::createUnhandledSubordinateIteratorBlockOnSelectedClif";
   unhandled_block_iterators.clear();
   int rtn, lclif;
   rtn = createSubordinateIteratorBlockOnSelectedClif(&lclif);
   if( (CF_OK != rtn) )
   {
      reportError("Failure to create subordinate iterator block");
      return(CF_FAIL);
   }
   if((unsigned int)lclif != block_iterators.size())
   {
      //serious application error
      reportError("subordinate block apparently created, but size of block inconsistent with function return");
      return(CF_FAIL);
   }
   for(int i = 0; i < lclif; i++)
   {
        unhandled_block_iterators.push_back(block_iterators[i]);
   }
   *nobjects = unhandled_block_iterators.size();
   block_iterators.clear();
   return(CF_OK);
}

rcfClifParser* rcfClifParser::getUnhandledIteratorFromBlock(unsigned int index)
{
   func = (char*)"rcfClifParser::getUnhandledIteratorFromBlock";
   // sanity check
   if( index >= unhandled_block_iterators.size() )
   {
      reportError("Illegal index value, exceeds size of the block");
      fprintf(stderr,"Index value: %d  maximum allowed value: %lu\n",index,(unhandled_block_iterators.size()-1));
      return(NULL);
   }
   return(unhandled_block_iterators[index]);
}

bool rcfClifParser::isThisAnIterator()
{
   return(iterator);
}

void rcfClifParser::reportError(const char* usr)
{
   if(NULL == usr)
   {
      fprintf(stderr,"rcfClifParser::reportError_char* called with NULL pointer in lieu of a message\n");
      return;
   }
   fprintf(stderr,"function %s reports ERROR: %s \n",func,usr);
}

void rcfClifParser::reportWarning(const char* usr)
{
   if(NULL == usr)
   {
      fprintf(stderr,"rcfClifParser::reportWarning_char* called with NULL pointer in lieu of a message\n");
      return;
   }
   fprintf(stderr,"function %s reports WARNING: %s \n",func,usr);
}

void rcfClifParser::reportError(const char* usr, char * msg)
{
   if(NULL == usr)
   {
      fprintf(stderr,"rcfClifParser::reportError_char* called with NULL pointer in lieu of a message\n");
      return;
   }
   if(NULL == msg) fprintf(stderr,"function %s reports ERROR: %s \n",func,usr);
   else fprintf(stderr,"function %s reports ERROR: %s with additional message: %s \n",func,usr,msg);
   
}

int rcfClifParser::findAndSelectClif(const char *usrHandle, int *found)
{
   func = (char*)"rcfClifParser::findAndSelectClif";

   object_found = 0;
   search_level = 0;
   resetClifObjectSelection();

   return(searchForClif(top_clif,usrHandle,found,0));
}

int rcfClifParser::findAndSelectClifWithSkip(const char *usrHandle, int *found, int skip)
{
   func = (char*)"rcfClifParser::findAndSelectClifWithSkip";

   object_found = 0;
   search_level = 0;
   resetClifObjectSelection();
   if(skip < 0)
   {
      reportWarning("skip level less than zero, resetting to zero");
      skip = 0;
   }

   return(searchForClif(top_clif,usrHandle,found,skip));
}

int rcfClifParser::iterateAndSelectClif(const char *usrHandle, int *found)
{
   func = (char*)"rcfClifParser::iterateAndSelectClif";
 
   object_found = 0;

   if(NULL == wrk_clif)wrk_clif = top_clif;

   return(searchForClif(wrk_clif,usrHandle,found,0));
}

int rcfClifParser::searchForDataObjectInClif(struct clifObjectType *clif, const char *usrHandle, int *found,clifDataType **out)
{
   func = (char*)"rcfClifParser::searchForDataObjectInClif";

   *found = 0;
   // sanity checks
   if(NULL == usrHandle)
   {
      // this is misuse. Report error
      reportError("NULL pointer passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(0 == strlen(usrHandle))
   {
      // this is misuse. Report error
      reportError("empty string passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(NULL == clif)return(CF_OK); // conceivably, this could be legal

   if(clif->nDataObjects <= 0)return(CF_OK); // Nothing to do

   if(NULL == clif->Data)
   {
      // now this is self-contradictory. Something is supposed to be there
      reportError("NULL pointer seen where data array is expected. Object, or underlying Clif error");
      return(CF_FAIL);
   }

   clifDataType *dptr = NULL;
   for(int i=0; i<clif->nDataObjects; i++)
   {
     dptr = (clif->Data) + i;
     if(NULL != dptr)
     {
        if(0 == strcmp(usrHandle,dptr->Handle))
        {
           // found it!
           *found = 1;
           *out = dptr;
           object_found = 1;
        }
     }
   }

   return(CF_OK);
}

int rcfClifParser::searchForArrayObjectInClif(struct clifObjectType *clif, const char *usrHandle, int *found,clifArrayType **out)
{
   func = (char*)"rcfClifParser::searchForArrayObjectInClif";

   *found = 0;
   // sanity checks
   if(NULL == usrHandle)
   {
      // this is misuse. Report error
      reportError("NULL pointer passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(0 == strlen(usrHandle))
   {
      // this is misuse. Report error
      reportError("empty string passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(NULL == clif)return(CF_OK); // conceivably, this could be legal

   if(clif->nArrayObjects <= 0)return(CF_OK); // Nothing to do

   if(NULL == clif->Array)
   {
      // now this is self-contradictory. Something is supposed to be there
      reportError("NULL pointer seen where data array is expected. Object, or underlying Clif error");
      return(CF_FAIL);
   }

   clifArrayType *dptr = NULL;
   for(int i=0; i<clif->nArrayObjects; i++)
   {
     dptr = (clif->Array) + i;
     if(NULL != dptr)
     {
        if(0 == strcmp(usrHandle,dptr->Handle))
        {
           // found it!
           *found = 1;
           *out = dptr;
           object_found = 1;
        }
     }
   }

   return(CF_OK);
}


int rcfClifParser::searchForClif(struct clifObjectType *clif, const char *usrHandle, int *found,int skip)
{
   func = (char*)"rcfClifParser::searchForClif";
   int rtn;

   *found = 0;
   search_level++;
   if(2 == debug_flag)
   {
      fprintf(stderr,"Entering searchForClif with Search Level: \t %d \n",search_level);
      if(NULL != clif->Handle)fprintf(stderr,"With clif handle %s\n",clif->Handle);
      else fprintf(stderr,"With NULL Clif handle\n");
      fprintf(stderr,"Number of clifs in this clif: %d\n",clif->nObjects);
   }

   if(NULL == usrHandle)
   {
      // this is misuse. Report error
      reportError("NULL pointer passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(0 == strlen(usrHandle))
   {
      // this is misuse. Report error
      reportError("empty string passed in lieu of User Handle");
      return(CF_FAIL);
   }
   if(NULL == clif)return(CF_OK); // conceivably, this could be legal

   // check if this may be the object we are looking for
   if(NULL != clif->Handle)
   {
      if(2 == debug_flag)
      {
         fprintf(stderr,"Examining object with handle: %s against: %s\n",clif->Handle,usrHandle);
      }
      if(0 == strcmp(usrHandle,clif->Handle) )
      {
         // found it !
         // but check the skip level
         if(skip > 0)
         {
            skip--;
            if(2 == debug_flag)
            {
               fprintf(stderr,"Decrementing skip to: \t %d \n",skip);
            }
         }
         else
         {
            *found = 1;
            object_found = 1;
            wrk_clif = clif;
            return(CF_OK);
         }
       }
   }

   if(clif->nObjects <= 0)
   {
      if(2 == debug_flag)fprintf(stderr,"No additional objects found\n");
      return(CF_OK); // not here, not found
   }
    // consistency check
   if(NULL == clif->Object)
   {
      reportError("Inconsistent state of Clif Object. Empty pointer to Object array, but the number of objects greater than zero");
      return(CF_FAIL);
   }

   // there is a list,try to find requested object on this list first 
   clifObjectType *lptr = NULL;
   for(int i=0; i<clif->nObjects; i++)
   {
      lptr = (clif->Object) + i;
      if(NULL == lptr)
      {
         reportError("Unexpected NULL pointer during clif object array search");
         return(CF_FAIL);
      }
      if(NULL != lptr->Handle)
      {
         if(2 == debug_flag)
         {
            fprintf(stderr,"Examining object with handle: %s against: %s\n",lptr->Handle,usrHandle);
         }
         if(0 == strcmp(usrHandle,lptr->Handle) )
         {
            // found it !
            // but check the skip level
            if(skip > 0)
            {
               skip--;
               if(2 == debug_flag)
               {
                  fprintf(stderr,"Decrementing skip to: \t %d \n",skip);
               }
            }
            else
            {
               *found = 1;
               object_found = 1;
               wrk_clif = lptr;
               return(CF_OK);
            }
         }
      }
   }

   // object not found on the list. Attempt recursive search below
   lptr = NULL;
 
   if(2 == debug_flag)
   {
      fprintf(stderr,"Attempting recursive search with nobjects: %d\n",clif->nObjects);
   }

   for(int i=0; i<clif->nObjects; i++)
   {
      lptr = (clif->Object) + i;
      if(NULL == lptr)
      {
         reportError("Unexpected NULL pointer during clif object array search");
         return(CF_FAIL);
      }
      if(lptr->nObjects > 0)
      {
         // looks like a viable branch
         if(NULL == lptr->Object)
         {
            //internal contradiction
            reportError("Unexpected NULL pointer during clif object array search");
            return(CF_FAIL);
         }
         // search this branch
         rtn = searchForClif(lptr,usrHandle,found,0);
         if(CF_OK != rtn)
         {
            reportError("Failure return searching through a branch");
            return(rtn);
         }
         else
         {
            // verify the result of search
            if(1 == object_found)return(CF_OK);
         }
      }
   }


   return(CF_OK);
}

void rcfClifParser::markCurrentClif()
{
   if(NULL != wrk_clif)mark_clif = wrk_clif;
   else mark_clif = NULL;
}

int rcfClifParser::selectMarkedClif()
{
   if(NULL == mark_clif)return(CF_FAIL);
   wrk_clif = mark_clif;
   return(CF_OK);
} 


clifObjectType* rcfClifParser::getClifStructure()
{    
    return top_clif;
}

// The functions below represent legacy code, imported from CVS library directory tree

// #define DEBUG
/* ******************************************************************** */
int rcfClifParser::dumpClifStructure(clifObjectType *clif)
{   
    // outputs the clif structure to screen or file 
    func = (char*)"rcfClifParser::dumpClifStructure";
    char spaceString[MAX_STR_LEN];
    for(int i=0; i<clif->clifLevel;i++)
       spaceString[i]=' ';
    spaceString[clif->clifLevel]=0;
    int stdalone_clif = 0;
    if( (0 == clif->clifLevel) && (NULL == clif->Handle) )stdalone_clif = 1;
  
    FILE *lfid = NULL;
    if(NULL == fid_out)lfid = stderr;
    else lfid = fid_out; 

    if(0 == stdalone_clif) fprintf(lfid,"\n%s%s ={",spaceString,clif->Handle);
    for(int iData=0; iData<clif->nDataObjects; iData++)
    {
       if(NULL == clif->Data[iData].Handle)
       {
          //report warning and continue to the next object 
          reportWarning("Data Object with NULL Handle encountered");
          continue; // go to the end of data object loop
       }
       if(0 == strcmp(clif->Data[iData].Delim,":") )
       {
          // special case of non-conventional string
          if(NULL != clif->Data[iData].Value)
          fprintf(lfid,"\n%s  %s : %s", spaceString,clif->Data[iData].Handle, clif->Data[iData].Value);
          else 
          fprintf(lfid,"\n%s  %s : ", spaceString,clif->Data[iData].Handle);
       }
       else
       {
          // assume a conventional string
          if(1 == clif->Data[iData].isquote)
          {
             if(NULL != clif->Data[iData].Value)
             fprintf(lfid,"\n%s  %s = \"%s\";", spaceString,clif->Data[iData].Handle, clif->Data[iData].Value);
             else 
             fprintf(lfid,"\n%s  %s = \"\";", spaceString,clif->Data[iData].Handle);
          }
          else
          {
             if(NULL != clif->Data[iData].Value)
             fprintf(lfid,"\n%s  %s = %s;", spaceString,clif->Data[iData].Handle, clif->Data[iData].Value);
             else 
             fprintf(lfid,"\n%s  %s = ;", spaceString,clif->Data[iData].Handle);
          }
       }
    } //end of data object loop
    // Output the Arrays
    for(int iArray=0; iArray<clif->nArrayObjects; iArray++)
    {
       fprintf(lfid,"\n%s  %s[] ={",spaceString,clif->Array[iArray].Handle);
       for(int itmp=0; itmp<clif->Array[iArray].nValues; itmp++)
       {
          fprintf(lfid,"\n%s    ",spaceString);
          fprintf(lfid,"%f", clif->Array[iArray].Value[itmp]);
          if(itmp<clif->Array[iArray].nValues-1) fprintf(lfid,",");
       }
       fprintf(lfid,"\n%s  };",spaceString);
    }
    // Output the clifObjects
    for(int iObjects=0; iObjects<clif->nObjects;iObjects++)
        dumpClifStructure(&clif->Object[iObjects]);
    if(0 == stdalone_clif)fprintf(lfid,"\n%s};",spaceString);
    return(CF_OK);
}
/* ******************************************************************** */
int rcfClifParser::checkIfObjectList(clifObjectType *List)
{
   for(int iObj=1; iObj < List->nObjects; iObj++)
   {
     if(strcmp( List->Object[0].Handle, List->Object[iObj].Handle) )
     {
        printf("\n ERROR: Expected Object list, object names do not match (%s,%s)",
               List->Object[0].Handle,List->Object[iObj].Handle);
        return(CF_FAIL);
     }
   }
   return(CF_OK);
} 
/* ******************************************************************** */
int rcfClifParser::checkIfObjectList(clifObjectType *Object, int nObjects)
{
   for(int iObj=1; iObj < nObjects; iObj++)
   {
     if(strcmp( Object[0].Handle, Object[iObj].Handle) )
     {
        printf("\n ERROR: Expected Object list, object names do not match (%s,%s)",
               Object[0].Handle,Object[iObj].Handle);
        return(CF_FAIL);
     }
   }
   return(CF_OK);
} 
/* ********************************************************************* */
int rcfClifParser::parseClifObjectNumber(int nObjects, clifObjectType *Object, char *objectHandle, int *iObject)
{
   /* check to make sure all of the objects in this list have the
      same name (ie, that it is a list of similar objects */
    /*   printf("\n Old Check\n");
   for(int iObj=1; iObj < nObjects; iObj++)
   {
     printf("\n Comparing %s and %s for object list", Object[0].Handle, Object[iObj].Handle);
     if(strcmp( Object[0].Handle, Object[iObj].Handle) )
     {
        printf("\n ERROR: Expected Object list, object names do not match (%s,%s)",
               Object[0].Handle,Object[iObj].Handle);
        return(CF_FAIL);
     }
   }
   printf("\n Old Check Passed, Do New Check\n"); */
   if(checkIfObjectList(Object, nObjects) != CF_OK)
      return(CF_FAIL);
   
   /* Parse out the object number */
   int objectLen=strlen(objectHandle);
   if(objectLen < 5 ||  // make sure has #,",#, at least 1 digit, and "
      objectHandle[1] != '"' ||  // make sure has beginning quote"
      objectHandle[objectLen-1] != '"' || // make sure has ending quote"        
      objectHandle[2] != '#' )   // make sure has # before number
   {
      printf("\n ERROR: # syntax is improper in %s",objectHandle);return(CF_FAIL);
   }
   // parse the value out of the string
   objectHandle[objectLen-1] = 0;
   if(sscanf(objectHandle+3,"%d", iObject) != 1) 
   {
      printf("\n ERROR: Scanning Object Number from %s", objectHandle); return(CF_FAIL);
   }
   if(*iObject < 0 || *iObject >= nObjects)
   {
      printf("\n ERROR: Invalid Object Number Scanned from %s", objectHandle); return(CF_FAIL);
   }
   return(CF_OK);
}
/* ************************************************************************************* */
int rcfClifParser::getClifObjectNumber(int nObjects,  clifObjectType *Object, char *objectHandle, int *iObject)
{
  /* Given an Object Name, determine the Object Number, return error if not found */
  if( findClifObjectNumber(nObjects, Object, objectHandle, iObject) != CF_OK)
  {
      printf("\n ERROR: getClifObjectNumber: Finding Object %s in %s", objectHandle , Object->Handle); 
      return(CF_FAIL);
  }
  return(CF_OK);
}
/* *************************************************************************************** */
int rcfClifParser::findClifObjectNumber(int nObjects,  clifObjectType *Object, char *objectHandle, int *iObject)
{
  /* Given an Object Name, determine the Object Number */
    // printf("\n getClifObjectNumber");
  int iObj = 0;
  while( iObj < nObjects &&                             // Not Exceed number of objects
         strcmp( Object[iObj].Handle, objectHandle) )   // Check for Match of Handle (Name)
  {
      //      printf("\n Compared %s and %s", Object[iObj].Handle, objectHandle);
     iObj++;
  }
  if(iObj >= nObjects)  // Object Not Found, check in "Name" of the data sub-objects
  {
     iObj = 0; 
     int found=0;
     while(iObj < nObjects && !found)
     {
        int isubObjects = 0;
        while( isubObjects < Object[iObj].nDataObjects  &&
              !( strcmp( Object[iObj].Data[isubObjects].Handle,"Name") == 0  &&   
                 strcmp( Object[iObj].Data[isubObjects].Value, objectHandle)==0 ) )
	{
	    /*  printf("\n Compared %s with %s and %s with %s", 
                    Object[iObj].Data[isubObjects].Handle,"Name",
                    Object[iObj].Data[isubObjects].Value, objectHandle); */
           isubObjects++;
        }
        if(isubObjects < Object[iObj].nDataObjects)
        {
           found++;
        }
        else
	{
           iObj++;
        }
     }
     if(!found)
     { 
        return(CF_FAIL);  
     }
   }
   *iObject = iObj;
   //   printf("\n Found Match of %s and %s", Object[iObj].Handle, objectHandle);
   return(CF_OK);
}
/* ******************************************************************** */
int rcfClifParser::checkForClifObject(clifObjectType *clif, char *messageHandle)
{   /* returns OK if clifObject with the messageHandle exists, else, returns FAIL
       needed so can use read routines with various versions of Pinnacle */
   int nameLen=strlen(messageHandle);
   int imsgHandle=0;
   // look for a period
   while( messageHandle[imsgHandle] != '.' && imsgHandle < nameLen) imsgHandle++;
   if(messageHandle[imsgHandle] == '.' ) // period found, now must find this name or number
   {                                     // in the structure list and call recursive 
      char objectHandle[MAX_STR_LEN];
      strncpy(objectHandle, messageHandle,imsgHandle);
      objectHandle[imsgHandle] = 0; // NULL terminate the string
      // check if given a "#" syntax
      int iObjects=0;
      if( objectHandle[0]=='#')
      {
         if( parseClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK )
	 {
            printf("\n ERROR: checkForClifObject for %s", messageHandle);
            printf("\n ERROR: Parsing Clif Object Number from %s", objectHandle); return(CF_FAIL);
         }
      }
      else
      {     // check through the objects (when not given in # format)
         if( getClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK )
	 {
            printf("\n ERROR: checkForClifObject for %s", messageHandle);
            printf("\n ERROR: Determining Clif Object Number for %s", objectHandle); return(CF_FAIL);
         }
      }
      // if found, read next level of the structure
      if( checkForClifObject( &clif->Object[iObjects], messageHandle+imsgHandle+1)!=CF_OK )
      {
         printf("\n ERROR: checkForClifObject for %s", messageHandle);
	 printf("\n ERROR: checkForClifObject with Message %s", messageHandle+imsgHandle+1); return(CF_FAIL);
      }
   }
   else /* Look for the name in the Objects*/
   {
       // printf("\n Looking for %s in the Objects", messageHandle);
      int iObject=0; /*
      while(  iObject < clif->nObjects && strcmp( clif->Object[iObject].Handle, messageHandle) )
      iObject++; */

      // check if object found, if not, look in list of names
      /* if( iObject >= clif->nObjects && */
       if(  findClifObjectNumber(clif->nObjects, clif->Object, messageHandle, &iObject) != CF_OK )
       {
         return(CF_FAIL);
      }
   }
  
   return(CF_OK);
}
/* ********************************************************************************** */
/* This routine tries to match the "value" for the object "handleToMatch" with the 
   value contained in "valueToMatch"for the clifObject clif, it returns the address of 
   the machingObject...Note: This routine does NOT climb down clif levels currently, thus,
   it is typcially used with getClifAddress to get the address of the parent clifs.
*/ 
int rcfClifParser::getMatchingClifAddress(clifObjectType *clif, char *handleToMatch, char *valueToMatch, clifObjectType **matchingObject)
{
#ifdef DEBUG
  printf("\n getMatchingClifAddress: looking for %s %s",handleToMatch, valueToMatch);
  printf("\t nObjects = %d, nDataObjects = %d", clif->nObjects, clif->nDataObjects);
#endif
  int iObject=0;
  char testValue[MAX_STR_LEN];
  do{
     if( readStringClifStructure(&clif->Object[iObject], handleToMatch, testValue) != CF_OK )
     {
        printf("\n ERROR: Reading %s for object %d",handleToMatch,  iObject);return(CF_FAIL);
     }
  }while( strcmp(valueToMatch,testValue) && ++iObject < clif->nObjects );

  if(iObject >= clif->nObjects)
  {
     printf("\n ERROR: Finding Object %s = %s;", handleToMatch, valueToMatch); return(CF_FAIL);
  }
  // printf("\n Found PlanList at Object %d", iObject);
  *matchingObject = &clif->Object[iObject];
  return(CF_OK);
}
/* ******************************************************************** */
int rcfClifParser::getClifAddress(clifObjectType *clif, char *messageHandle, clifObjectType **Object)
{
   int nameLen=strlen(messageHandle);
   int imsgHandle=0;
   // look for a period
   while( messageHandle[imsgHandle] != '.' && imsgHandle < nameLen) imsgHandle++;
   if(messageHandle[imsgHandle] == '.' ) // period found, now must find this name or number
   {                                     // in the structure list and call recursive 
      char objectHandle[MAX_STR_LEN];
      strncpy(objectHandle, messageHandle,imsgHandle);
      objectHandle[imsgHandle] = 0; // NULL terminate the string
      // check if given a "#" syntax
      int iObjects=0;
      if( objectHandle[0]=='#')
      {
         if( parseClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK )
	 {
            printf("\n ERROR: getClifAddress for %s", messageHandle);
            printf("\n ERROR: Parsing Clif Object Number from %s", objectHandle); return(CF_FAIL);
         }
      }
      else
      {     // check through the objects (when not given in # format)
         if( getClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK )
	 {
            printf("\n ERROR: getClifAddress for %s", messageHandle);
            printf("\n ERROR: Determining Clif Object Number for %s", objectHandle); return(CF_FAIL);
         }
      }
      // if found, read next level of the structure
      if( getClifAddress( &clif->Object[iObjects], messageHandle+imsgHandle+1, Object)!=CF_OK )
      {
         printf("\n ERROR: getClifAddress for %s", messageHandle);
	 printf("\n ERROR: readClifStructure with Message %s", messageHandle+imsgHandle+1); return(CF_FAIL);
      }
   }
   else /* Look for the name in the Objects*/
   {
       // printf("\n Looking for %s in the Objects", messageHandle);
     /* JVS: .... getClifObjectNumber does this.....*/
      int iObject=0;
      /*
      while(  iObject < clif->nObjects && strcmp( clif->Object[iObject].Handle, messageHandle) )
	 iObject++;
     */

      // check if object found, if not, look in list of names
     /*      if( iObject >= clif->nObjects && */
       if(  getClifObjectNumber(clif->nObjects, clif->Object, messageHandle, &iObject) != CF_OK )
       {
         printf("\n ERROR: getClifAddress for %s", messageHandle);
         printf("\n ERROR: Finding Object Named %s", messageHandle); 
         printf("\n\tObject List is");
         for(int iObj=0; iObj<clif->nObjects; iObj++)
            printf("\n\t\t%s", clif->Object[iObj].Handle);
         return(CF_FAIL);
      }
      *Object = &clif->Object[iObject];
   }
   return(CF_OK);
}
/* *********************************************************************************** */
int rcfClifParser::readStringClifStructure(clifObjectType *clif, char *messageHandle, char *Value)
{  /* Reads a pre-allocated string from a clif structure */
#ifdef DEBUG
   printf("\n readStringClifStructure %s", messageHandle);
#endif
   char *tString;
   if(readClifStructure(clif, messageHandle, &tString)!= CF_OK )
   {
      printf("\n ERROR: Reading %s from clif %s", messageHandle, clif->Handle); return(CF_FAIL);
   }
   // printf(" readStringClifStructure:len=(%d):%s = %s\n", strlen(tString),messageHandle, tString );
   strcpy(Value,tString);
   return(CF_OK);
}
int rcfClifParser::readFloatClifStructure(clifObjectType *clif, char *messageHandle, float *Value)
{  /* Reads a pre-allocated string from a clif structure */
#ifdef DEBUG
  printf("\n readFloatClifStructure %s", messageHandle);
#endif
   char *tString;
   if(readClifStructure(clif, messageHandle, &tString)!= CF_OK )
   {
      printf("\n ERROR: Reading %s from clif %s", messageHandle, clif->Handle); return(CF_FAIL);
   }
   if(sscanf(tString,"%f",Value) != 1)
   {
      printf("\n ERROR: Reading %s from clif %s, cannot read float from %s", 
             messageHandle, clif->Handle, tString); return(CF_FAIL);
   }
   return(CF_OK);
}
int rcfClifParser::readIntClifStructure(clifObjectType *clif, char *messageHandle, int *Value)
{  /* Reads a pre-allocated string from a clif structure */
   char *tString;
   if(readClifStructure(clif, messageHandle, &tString) != CF_OK )
   {
      printf("\n ERROR: Reading %s from clif %s", messageHandle, clif->Handle); return(CF_FAIL);
   }
   if(sscanf(tString,"%d",Value) != 1)
   {
      printf("\n ERROR: Reading %s from clif %s, cannot read float from %s", 
             messageHandle, clif->Handle, tString); return(CF_FAIL);
   }
   return(CF_OK);
}
/* *********************************************************************************** */
int rcfClifParser::readClifStructure(clifObjectType *clif, char *messageHandle, char **Value)
{
   /* if find a . in the object, then must find upper clif level and then
      go to next lower level
      if no . exists, then check in the clif Data objects (clifData and
      ClifArray)

      Also note: Can use by specifying a "Name" or by a #"#0" type syntax
   */
#ifdef DEBUG
   printf("\n readClifStructure reports %d Objects", clif->nObjects);
   printf("\n readClifStructure reports %d DataObjects", clif->nDataObjects);
#endif
   int nameLen=strlen(messageHandle);
   int imsgHandle=0;
   // look for a period
   while( messageHandle[imsgHandle] != '.' && imsgHandle < nameLen) imsgHandle++;
   if(messageHandle[imsgHandle] == '.' ) // period found, now must find this name or number
   {                                     // in the structure list and call recursive 
#ifdef DEBUG
     printf("\n Found period (.)in message %s", messageHandle);
#endif
      char objectHandle[MAX_STR_LEN];
      strncpy(objectHandle, messageHandle,imsgHandle);
      objectHandle[imsgHandle] = 0; // NULL terminate the string
      // check if given a "#" syntax
      int iObjects=0;
      if( objectHandle[0]=='#')
      {
         if(parseClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK)
	 {
            printf("\n ERROR: Parsing Clif Object Number from %s", objectHandle); return(CF_FAIL);
         }
      }
      else
      {     // check through the objects (when not given in # format)
         if( getClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK)
	 {
            printf("\n ERROR: Determining Clif Object Number for %s", objectHandle); return(CF_FAIL);
         }
      }
      // if found, read next level of the structure
      if(readClifStructure( &clif->Object[iObjects], messageHandle+imsgHandle+1, Value)!=CF_OK)
      {
	printf("\n ERROR: readClifStructure with Message %s", messageHandle+imsgHandle+1); return(CF_FAIL);
      }
   }
   else /* Look for the name in the Data Objects*/
   {
      int iObject=0;
      //      printf("\n nDataObjects = %d (%s)", clif->nDataObjects, messageHandle);
      while(  iObject < clif->nDataObjects && strcmp( clif->Data[iObject].Handle, messageHandle) )
	 iObject++;

      if(iObject >= clif->nDataObjects)
      {
         printf("\n ERROR: Finding Data Object Named %s", messageHandle); return(CF_FAIL);
      }
      // printf("\n Found %s with value %s", clif->Data[iObject].Handle, clif->Data[iObject].Value);
      *Value = clif->Data[iObject].Value;
   }
   return(CF_OK);
}
/* ************************************************************************************* */
int rcfClifParser::readClifArrayStructure(clifObjectType *clif, char *messageHandle, int *nArray, float **Array)
{
   /* if find a . in the object, then must find upper clif level and then
      go to next lower level
      if no . exists, then check in the clif Data objects (clifData and
      ClifArray)

      Also note: Can use by specifying a "Name" or by a #"#0" type syntax
   */
   int nameLen=strlen(messageHandle);
   int imsgHandle=0;
   // look for a period
   while( messageHandle[imsgHandle] != '.' && imsgHandle < nameLen) imsgHandle++;
   if(messageHandle[imsgHandle] == '.' ) // period found, now must find this name or number
   {                                     // in the structure list and call recursive 
      char objectHandle[MAX_STR_LEN];
      strncpy(objectHandle, messageHandle,imsgHandle);
      objectHandle[imsgHandle] = 0; // NULL terminate the string
      // check if given a "#" syntax
      int iObjects=0;
      if( objectHandle[0]=='#')
      {
         if(parseClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK)
	 {
            printf("\n ERROR: Parsing Clif Object Number from %s", objectHandle); return(CF_FAIL);
         }
      }
      else
      {     // check through the objects (when not given in # format)
         if( getClifObjectNumber(clif->nObjects, clif->Object, objectHandle, &iObjects) != CF_OK)
	 {
            printf("\n ERROR: Determining Clif Object Number for %s", objectHandle); return(CF_FAIL);
         }
      }
      // if found, read next level of the structure
      if(readClifArrayStructure( &clif->Object[iObjects], messageHandle+imsgHandle+1, nArray, Array)!=CF_OK)
      {
	printf("\n ERROR: readClifArrayStructure with Message %s", messageHandle+imsgHandle+1); return(CF_FAIL);
      }
   }
   else /* Look for the name in the Array Objects*/
   {
      int iObject=0;
      while(  iObject < clif->nArrayObjects && strcmp( clif->Array[iObject].Handle, messageHandle) )
      {
         printf("\n Array %s not match %s",  clif->Array[iObject].Handle, messageHandle);
	 iObject++;
      }
      if(iObject >= clif->nArrayObjects)
      {
         printf("\n ERROR: Finding Array Object Named %s", messageHandle); return(CF_FAIL);
      }
      *nArray = clif->Array[iObject].nValues;
      /* Copy Array into local memory */
      float *localArray = (float *) calloc(*nArray, sizeof(float));
      if(localArray == NULL) 
      {
         printf("\n ERROR: Allocating ARRAY memory"); return(CF_FAIL);
      }
      for(int iArray=0; iArray<*nArray; iArray++) localArray[iArray]= clif->Array[iObject].Value[iArray];
      *Array  = localArray;
   }
   return(CF_OK);
}
/* ------------------------------------------------------------------------------------------ */
/* ********************************************************************* */
int rcfClifParser::freeClifStructure(clifObjectType *clif)
{  // free's all of the memory allocated by a clif
   for(int iObjects=0; iObjects < clif->nObjects; iObjects++)
   {
      if(freeClifStructure(&clif->Object[iObjects]) != CF_OK)
      {
         printf("\n ERROR: Freeing memory for clif object");
      }
   }
   free(clif->Object);
   // free all of the arrays
   for(int iArrays=0; iArrays < clif->nArrayObjects; iArrays++)
   {
      if(clif->Array[iArrays].nValues) free(clif->Array[iArrays].Value);
      if(NULL != clif->Array[iArrays].Handle)free(clif->Array[iArrays].Handle);
   }
   if(NULL != clif->Array)free(clif->Array);
   // free all of the Data Values
   for(int iData=0; iData < clif->nDataObjects; iData++)
   {
      if(NULL != clif->Data[iData].Value)free(clif->Data[iData].Value);
      if(NULL != clif->Data[iData].Handle)free(clif->Data[iData].Handle);
   }
   if(NULL!= clif->Data)free(clif->Data);
   if(NULL != clif->Handle)free(clif->Handle);
   return(CF_OK);
}
/* ********************************************************************* */
int rcfClifParser::readClif(FILE *istrm, clifObjectType *clif, char *clifHandle)
{
   char clifStringHandle[MAX_STR_LEN];
   sprintf(clifStringHandle,"%s ={", clifHandle);
   clif->Handle = (char *)calloc(strlen(clifHandle)+1, sizeof(char));
   if( clif->Handle == NULL)
   {
     printf("\n ERROR: readClif: Allocating Memory for Object Handle"); return(CF_FAIL);
   }
   // copy the string to the name
   strcpy(clif->Handle, clifHandle);

   if(clif_get_to_line(istrm,clifStringHandle)!=CF_OK)
   {
         printf("\n ERROR: readClif: finding %s ", clifStringHandle);
         return(CF_FAIL);
   }
   if(readClifLevel(istrm, clif) != CF_OK)
   {
      printf("\n ERROR: readClif: Reading Clif %s", clifStringHandle);
      return(CF_FAIL);
   }
   return(CF_OK);
}
/* ********************************************************************* */
/* ************************************************************************************ */
int rcfClifParser::readSpecificClif(FILE *istrm, clifObjectType *clif, char *clifHandle, char *clifName)
{ // Use to read a clif of type clifHandle with Name = clifName into the clif Object from
  // file istrm
   char clifStringHandle[MAX_STR_LEN];
   sprintf(clifStringHandle,"%s ={",clifHandle);
   /*  Jun 8, 2000: JVS: This is done in readClifFile
   clif->Handle = (char *)calloc(strlen(clifHandle)+1, sizeof(char));
   if( clif->Handle == NULL)
   {
      printf("\n ERROR: readSpecificClif: Allocating Memory for Object Handle"); return(CF_FAIL);
   }
   // copy the string to the name
   strcpy(clif->Handle, clifHandle);
   */
#ifdef DEBUG
   printf ("\n readSpecificClif: ");
#endif

   int beamFound=0;
   char currentClifName[MAX_STR_LEN];
   long iFileOffset; // Offset in the file to the beginning of the clif
   do{
      if(clif_get_to_line(istrm,clifStringHandle)!=CF_OK)
      {
         printf("\n ERROR: readSpecificClif: finding %s ", clifStringHandle);
         return(CF_FAIL);
      }
      // istrmClifStart = istrm; // Pointer to location in the file where this clif starts
      if( (iFileOffset = ftell(istrm)) == -1L)
      {
         printf("\n ERROR: getting file offset"); return(CF_FAIL);
      }
      // get Name
      if(clif_string_read(istrm,(char*)"Name =",currentClifName) != CF_OK)  
      {
         printf("\n ERROR: readSpecificClif: Reading Clif Name = for %s", clifHandle); return(CF_FAIL); 
      }
      /* check if current name matches */
      if(strcmp(currentClifName,clifName) == 0) beamFound = 1;
      if(!beamFound)
      {  /* close the Trial cliff */
         if(clif_get_to_line(istrm,(char*)"};")!=CF_OK)
         {
            printf("\n ERROR: Closing Trial structure in file");
            return(CF_FAIL);
         }
      }
   }while(!beamFound);
#ifdef DEBUG
   printf("\n Found %s %s in file", clifHandle, clifName);
#endif
   /* Assign the "Name" Object in this clif, and read the rest...*/
   if(fseek(istrm,iFileOffset,SEEK_SET)!=0)
   {
      printf("\n ERROR: seeking start of clif in file"); return(CF_FAIL);
   }
   // Read the Clif in starting at the file pointer location
   if( readClifFile(istrm, clif,clifHandle) != CF_OK)
   {
      printf("\n ERROR: readSpecificClif: Reading %s from file", clifHandle); return(CF_FAIL);
   }
#ifdef DEBUG   
   printf("\n Completed Reading in Clif from the file");
#endif
   return(CF_OK);
}
/* ******************************************************************************* */
int rcfClifParser::readClifFile(FILE *istrm, clifObjectType *clif, char *clifHandle)
{
   int nohandle = 0;
   if(NULL != clifHandle)
   {
      clif->Handle = (char *)calloc(strlen(clifHandle)+1, sizeof(char));
      if( clif->Handle == NULL)
      {
         printf("\n ERROR: readClifFile: Allocating Memory for Object Handle"); return(CF_FAIL);
      }
      // copy the string to the name
      strcpy(clif->Handle, clifHandle);
   }
   clif->clifLevel = 0;
   if(readClifLevel(istrm, clif) != CF_OK)
   {
      if(0==nohandle)fprintf(stderr,"\n ERROR: rcfClifParser::readClifFile: Reading Clif %s", clifHandle);
      else fprintf(stderr,"\n ERROR: rcfClifParser::readClifFile: Reading Clif at level 0, with no handle");

      return(CF_FAIL);
   }
   return(CF_OK);
}
/* ******************************************************************* */
int rcfClifParser::readClifLevel(FILE *istrm, clifObjectType *clif)
{
   // initialize values for new clif
   clif->nObjects         = 0;
   clif->nDataObjects     = 0;
   clif->nArrayObjects    = 0;
   clif->Data  = NULL;
   clif->Array = NULL;
   clif->Object = NULL;
   // read in the Clif Data 

   if(1 == debug_flag)
   if(clif->Handle != NULL) printf("\n Reading Clif %s at level %d........", clif->Handle, clif->clifLevel);

   if(readClifData(istrm, clif) != CF_OK)
   {
      printf("\n ERROR: readClifParser::readClifLevel: Reading Clif\n"); 
      return(CF_FAIL);
   }
   
   return(CF_OK);
}
/* ************************************************************** */
int rcfClifParser::readClifArray(FILE *istrm, clifArrayType *Array )
{
   // allocate memory for the data
   // read values till end of clif level
   // parse out data values (save as floats?) using strtok....
   //
   // printf("\n reading Clif Array\n");
   char inString[MAX_STR_LEN];
   char *delimiterString=(char*)",";
   Array->nValues=0;  // initialize number of values
   while( clif_fgets(inString,MAX_STR_LEN,istrm ) ) /* read in the string */
   {
     char *pString; /* pointer to string read in */
     float Value;
     pString = strtok(inString,delimiterString);
     while(pString!=NULL)
     {
        if( sscanf(pString,"%f",&Value) == 1)
	{  // allocate memory
           if(Array->nValues==0) 
              Array->Value = (float *) malloc(sizeof(float));
           else
              Array->Value = (float *) realloc(Array->Value, (Array->nValues+1)*sizeof(float));
           if(Array->Value == NULL)
           {
              printf("\n ERROR: Allocating Array Memory"); return(CF_FAIL);
           }
           // assign value in the Array
           Array->Value[Array->nValues] = Value;
           // increment the number in the array
           Array->nValues++;
        }
        pString = strtok(NULL,delimiterString);
     }
   }

   return(CF_OK);
}
/* ************************************************************** */
int rcfClifParser::readClifDataObject(int *nDataObjects, clifDataType **Data, char *string, char *delim)
{
   func = (char*)"rcfClifParser::readClifDataObject";
   clifDataType *localData;
   if(1 == debug_flag)printf("readClifDataObject received string: %s and delimiter: %s\n",string,delim);
   /* Allocate Memory for the object*/
   if(*nDataObjects == 0)
      localData = (clifDataType *) malloc(sizeof(clifDataType));
   else
      localData = (clifDataType *) realloc((clifDataType *) *Data, 
                                        (*nDataObjects+1)*sizeof(clifDataType));
   if(localData == NULL)
   {
      printf("\n ERROR: Allocating Memory for Clif Data"); return(CF_FAIL);
   }
   *Data = localData; 

   /* Get the Name of the Clif */
   if(readClifStringName(string,delim, &localData[*nDataObjects].Handle) != CF_OK)
   {
      printf("\n ERROR: Getting String Name"); return(CF_FAIL);
   }    
   /* Get the Value of the Clif */
   if(readClifStringValue(string,delim, &localData[*nDataObjects].Value,&localData[*nDataObjects].isquote) != CF_OK)
   {
      printf("\n ERROR: Getting String Value"); return(CF_FAIL);
   }      
   if(1 == debug_flag ) printf("\n %d : %s %s", *nDataObjects, localData[*nDataObjects].Handle, localData[*nDataObjects].Value);
   if(strlen(delim) < MAXDELIM)
   {
     strcpy(localData[*nDataObjects].Delim,delim);
   }
   else
   {
      reportError("Insufficient space for Data Object Delimiter.  Please recompile with greater MAXDELIM parameter");
      return(CF_FAIL);
   }
   *nDataObjects=*nDataObjects+1; 
   
   //   printf("\n Update nDataObjects = %d", *nDataObjects);

   return(CF_OK);
}
/* ***************************************************************************************** */
int rcfClifParser::processClifData(FILE *istrm, clifObjectType *clif, char *string)
{
    char *tempHandle;
     char *strLoc = NULL;
     /* decide what to do with it based upon the types given */
     /* if see a [] ={, then, must make an array (till ends with };
                    ={  create a new object type (and recusrively call read_clif_level) (till ends with };
                    = "string";  Then, string (look for " ")
                    = float;     then float (look for . )
                    = int;       Then int
     */
          char *equalLoc = NULL;
     if(   (strLoc=strstr(string, ".")) != NULL &&  // Check for . in name
	   (equalLoc=strstr(string, "=")) != NULL &&  // and check if before =
	   strLoc < equalLoc )               
     {
        // Extract Clif Handle (Get name of the Clif)
        
        if(readClifStringName(string,(char*)".", &tempHandle) != CF_OK)
        {
           printf("\n ERROR: Getting String Name"); return(CF_FAIL);
        } 
        // printf("\n tempHandle = %s", tempHandle);
        // See if already exist
        int iObject = 0;
        if(  findClifObjectNumber(clif->nObjects, clif->Object, tempHandle, &iObject) != CF_OK )
	{
           // Create if not already exist
          if(clif->nObjects == 0)
             clif->Object = (clifObjectType *) malloc(sizeof(clifObjectType));
          else
             clif->Object = (clifObjectType *) realloc((clifObjectType *) clif->Object, 
                                           (clif->nObjects+1)*sizeof(clifObjectType));
          if(clif->Object == NULL)
          {
             printf("\n ERROR: Allocating Memory for Clif Object"); return(CF_FAIL);
          }
          iObject = clif->nObjects; // assign iObject to current object
          // get the name of this object
          clif->Object[iObject].Handle = tempHandle;
          // initialize the data for this new clif object
          clif->Object[iObject].clifLevel = clif->clifLevel+1;
          clif->Object[iObject].nObjects = 0;
          clif->Object[iObject].nDataObjects = 0;
          clif->Object[iObject].nArrayObjects = 0;
          clif->Object[iObject].Data  = NULL;
          clif->Object[iObject].Array  = NULL;
          clif->Object[iObject].Object = NULL;
          clif->nObjects++;   // increment number of objects 
	}
        else
	{
          free(tempHandle);  // free the memory used by the name
	}
        // strLoc is the location of the remaining string starting at the .
        strLoc++; // increment past the period
        if(processClifData(istrm, &clif->Object[iObject], strLoc ) != CF_OK)
        {
           printf("\n ERROR: rcfClifParser::processClifData: processClifData ");
           return(CF_FAIL); 
        }
        // Add item  processClifData(istrm
     } else if( strstr(string, "[]") != NULL )  /* Create an array */
     {
       /* allocate memory for a new object */
       if(clif->nArrayObjects == 0)
          clif->Array = (clifArrayType *) malloc(sizeof(clifArrayType));
       else
          clif->Array = (clifArrayType *) realloc((clifArrayType *) clif->Array, 
                                        (clif->nArrayObjects+1)*sizeof(clifArrayType));
       if(clif->Array == NULL)
       {
          printf("\n ERROR: Allocating Memory for Clif Array"); return(CF_FAIL);
       }
       // get the name of this array object
       if(readClifStringName(string,(char*)"[]", &clif->Array[clif->nArrayObjects].Handle) != CF_OK)
       {
          printf("\n ERROR: Getting Array Name"); return(CF_FAIL);
       }  
       /* Read In the Array */
       if(readClifArray(istrm, &clif->Array[clif->nArrayObjects] ) != CF_OK)
       {
          printf("\n ERROR: Reading in Array Object"); return(CF_FAIL);
       }
       clif->nArrayObjects++; 
     }
     else if( (strLoc = strstr(string, "={" )) != NULL )  /* Create a new object */
     { 
       /* allocate memory for a new object */

       if(1 == debug_flag)
       {
          printf("\n Parsing as ={");
          printf("\n nObjects = %d", clif->nObjects);
       }

       if(clif->nObjects == 0)
          clif->Object = (clifObjectType *) malloc(sizeof(clifObjectType));
       else
          clif->Object = (clifObjectType *) realloc((clifObjectType *) clif->Object, 
                                        (clif->nObjects+1)*sizeof(clifObjectType));
       if(clif->Object == NULL)
       {
          printf("\n ERROR: Allocating Memory for Clif Object"); return(CF_FAIL);
       }
       // get the name of this object
       if(readClifStringName(string,(char*)"={", &clif->Object[clif->nObjects].Handle) != CF_OK)
       {
          printf("\n ERROR: rcfClifParser::processClifData: Getting Clif Name"); return(CF_FAIL);
       }
       // assign the level of the clif
       clif->Object[clif->nObjects].clifLevel = clif->clifLevel+1;
       // Read the Object 
       if(readClifLevel(istrm, &clif->Object[clif->nObjects]) != CF_OK)
       {
          printf("\n ERROR: rcfClifParser::processClifData: readClifLevel\n");
          return(CF_FAIL); 
       }
       clif->nObjects++;   // increment number of objects 
     }  
     else if ( ( strLoc=strstr(string, "=" ) ) != NULL )
    {  /* Read in a VALUE Object */

      if(1 == debug_flag) printf("\n Parsing as =");

       if( readClifDataObject( &clif->nDataObjects, &clif->Data, string, (char*)"=") != CF_OK)
       {
          printf("\n ERROR: rcfClifParser::processClifData: Reading Clif Data Object from %s", string); return(CF_FAIL);
       }
     }
     else if ( ( strLoc=strstr(string, ":" ) ) != NULL )
    {  /* Read in a VALUE Object */

      if(1 == debug_flag) printf("\n Parsing as :");

       if( readClifDataObject( &clif->nDataObjects, &clif->Data, string, (char*)":") != CF_OK)
       {
          printf("\n ERROR: processClifData: Reading Clif Data Object from %s", string); return(CF_FAIL);
       }
     }
     else
     {
       
        printf("\n ERROR: readClifData: %s is invalid clif_string2", string);
        printf("tempHandle: %s\n", tempHandle);
        printf("\n \t or programmer must learn to handle this situation\n");
        return(CF_FAIL);
     }
     return(CF_OK);
}
/* ***************************************************************************************** */
int rcfClifParser::readClifData(FILE *istrm, clifObjectType *clif)
{
  /* Reads Data  within a CLIF    */
  /* Called when sees a Name ={ */
  /* Returns when sees a };     */
  char string[MAX_STR_LEN];

  while( clifFgets(string,MAX_STR_LEN,istrm ) != NULL ) /* read in the string */
  {
     if(1 == debug_flag) printf("\n readClifData: %s", string);

    if( processClifData(istrm, clif, string) == CF_FAIL) {
      printf("\n ERROR: rcfClifParser::readClifData:");
      // printf("\n\t Processing string %80c...", string);
       return(CF_FAIL);
   }
  }
  if(myerrno == CF_FAIL)
  {
    printf("\n ERROR: rcfClifParser::readClifData\n");
    return(CF_FAIL); 
  }
  /* return on end of context */
  if(myerrno == END_OF_CONTEXT)
  {
     myerrno = CF_OK; /* reset errno */ 
     return(CF_OK); 
  }
  // check for end of file and outside of all clifLevels
  if(myerrno == END_OF_FILE && clif->clifLevel == 0 )
  {
     myerrno = CF_OK; return(CF_OK);
  }
  printf("\n ERROR: rcfClifParser::readClifData unknown error code = %d\n",myerrno); 
  return(CF_FAIL);
}
/* *********************************************************************************** */ 
int rcfClifParser::readClifStringName(char *inString, char *delimiter, char **outString)
{
  if(1==debug_flag)printf("\n Getting clif name from %s", inString);
  char *strLoc = strstr(inString, delimiter);  // get location of delimeter
  /* Remove Space from the Name */
  int iStringStart=0;  
  while( isspace( inString[iStringStart] ) ) iStringStart++;
  int iStringEnd=strLoc-inString;
  if(1==debug_flag)printf("\n strLoc: %s iStringStart: %d  iStringEnd: %d\n",strLoc,iStringStart,iStringEnd);

  while( isspace( inString[iStringEnd-1] ) )   iStringEnd--;
  int Length=iStringEnd-iStringStart; // length of final string
  // allocate memory for the string
  char *nameString = (char *)calloc(Length+1, sizeof(char));
  if( nameString == NULL)
  {
     printf("\n ERROR: getClifName: Allocating Memory for Object Name"); return(CF_FAIL);
  }
  // copy the string to the name
  strncpy(nameString, inString+iStringStart, Length);
  // printf("\n Name is (%d) %s\n", strlen(nameString), nameString);
  // printf("\t -%c-  +%c+", nameString[0],nameString[Length-1]);
  *outString = nameString; // assign pointer to name
  return(CF_OK);
}
/* *********************************************************************************** */ 
int rcfClifParser::readClifStringValue(char *inString, char *delimiter, char **outString,int *isquote)
{
  // printf("\n Getting clif Value from %s", inString);
  char *strLoc = strstr(inString, delimiter);  // get location of delimeter
  char *nameString = NULL;
  // determine where the string starts
  int iStringStart=strLoc-inString+strlen(delimiter);  
  if(iStringStart > (int) strlen(inString) )
  {
     printf("\n ERROR: Getting Value from string %s with iStringStart: %d  strlen: %ld", inString,iStringStart,strlen(inString)); return(CF_FAIL);
  }
  if(iStringStart == (int) strlen(inString) )
  {
     // special case of empty string, this is legal
     nameString = (char *)calloc(1, sizeof(char));
     if( nameString == NULL)
     {
        printf("\n ERROR: getClifName: Allocating Memory for Object Name"); return(CF_FAIL);
     }
     // we generated empty string under nameString
     *outString = nameString; // assign pointer to name
     return(CF_OK); // return immediately
     
  }
  // Value string is not empty, continue processing
  // remove space from the string
  while( isspace( inString[iStringStart] ) ) iStringStart++;
  int iStringEnd=strlen(inString);
  while( isspace( inString[iStringEnd] ) )   iStringEnd--;
  // ensure string ends in semicolon
  if( inString[iStringEnd-1] != ';' )
  {
      //printf("\n ERROR: Getting value from string %s", inString);
      //printf("\n\t String %s does not end in ';'", inString); return(CF_FAIL);
     // do nothing, this is legal
  }
  else
  {
     iStringEnd--;  // remove the semicolon
  }
  // check to see if it is in quotes
  if( inString[iStringEnd-1] == '"'  && inString[iStringStart] == '"' )
  {
    *isquote = 1;
    iStringEnd--; // remove the closing quote
    iStringStart++; // remove the opening quote
  }
  else *isquote = 0;
  
  int Length=iStringEnd-iStringStart; // length of final string
  // check the string length
  if(Length < 0 || Length > (int) strlen(inString) )
  {
     printf("\n ERROR: Invalid string length for string %s", inString); return(CF_FAIL);
  }
  // allocate memory for the string
  nameString = (char *)calloc(Length+1, sizeof(char));
  if( nameString == NULL)
  {
     printf("\n ERROR: getClifName: Allocating Memory for Object Name"); return(CF_FAIL);
  }
  // copy the string to the name
  strncpy(nameString, inString+iStringStart, Length);
  // printf("\n Name is %s\n", nameString);
  *outString = nameString; // assign pointer to name
  return(CF_OK);
}
/* *********************************************************************************** */ 

/* ************************************************************** */
float rcfClifParser::clif_get_value(FILE *istrm, char *string)
/* gets a value from a clif file, ignores comments, complains if leave current scope */
{
   char str[MAX_STR_LEN];
   if(clif_string_after(istrm, string, str)!=CF_OK)
   {
      printf("\n ERROR: clif_get_value, looking for %s", string);
      return(CF_FAIL);
   }
   /* remove spaces from end of the string */
   int len=strlen(str)-1;
   while( isspace(str[len]) ) 
   {
      str[len--]='\0'; /* remove if it is a space */
   }
   /* ensure is a valid clif string */
   if(str[strlen(str)-1] != ';' )
   {
      printf("\n ERROR:clif_get_value: %s is an invalid clif string",str);
      myerrno = CF_FAIL;
      return(CF_FAIL);
   }
   float n;
   if(sscanf(str,"%f",&n)!=1)
   {
      printf("\n ERROR: get_value %s",string);
      myerrno = CF_FAIL;
      return( CF_FAIL );
   }
   // printf("\n Clif value of %f returned",n);
   return(n);
}
/* ************************************************************ */
/* **************************************************************** */
int rcfClifParser::clif_get_to_line(FILE *istrm, char *string)
{
   char str[MAX_STR_LEN];
#ifdef DEBUG_CLIF
    printf("\n clif_get_to_line: Looking for %s (len = %d)", string, strlen(string));
#endif
   if(clif_string_after(istrm,string,str) != CF_OK)
   {
       /* printf("\n ERROR: clif_get_to_line: looking for %s", string); */
       /* don't want to print the error, since one way to see if done is to
          see if no more exist */
      return(CF_FAIL);
   }
#ifdef DEBUG
   printf("\n clif_get_to_line returns OK");
#endif
   return(CF_OK);
}
/* ************************************************************ */
int rcfClifParser::clif_string_after(FILE *istrm, char *string, char *str)
{
   char *pstr=str;
   int ctmp;
   if(1 == debug_flag)
   {
       printf("\n checking for clif string %s (len = %ld)",string, strlen(string) );
   }
   do{
      ctmp=clif_check_if_line(istrm,string, pstr);

      if(1 == debug_flag) 
      {
          printf("\n check_if_line returns: %s (len = %ld)",pstr, strlen(pstr));
      }

      if(ctmp == END_OF_CONTEXT)
      {
         myerrno = END_OF_CONTEXT;
         return(END_OF_CONTEXT);
      }
      if(ctmp == CF_FAIL)
      {
         printf("\n ERROR: return from clif_check_if_line");
         printf("\n\t Looking for %s",string);
         if( strcmp(pstr,"EOF") == 0)
	 {
	    printf("\n ERROR: clif_string_after: End of File on Read");
	    myerrno = CF_FAIL;
	    return( CF_FAIL );
	 }
      }
   }while(ctmp != CF_OK); // pstr == NULL);
#ifdef DEBUG_CLIF
   printf("\n Parsing the string %s",pstr);
   if(myerrno != CF_OK) printf("\n Myerrno is not OK");
   printf("\n String length = %d",strlen(pstr));
#endif
   while(isspace(pstr[0]))pstr++;  // remove leading space
   int len;
   len = strlen(pstr);
#ifdef DEBUG_CLIF
   printf(" after String length = %d",strlen(pstr));
#endif
   
   // remove trailing space from the string
   while(len && isspace(pstr[len-1]))
   { 
      pstr[len-1] = '\0';
      len--;
   }
#ifdef DEBUG_CLIF
   printf(" len now = %d",len);
#endif
   int i;
   for(i=0; i<len && i < MAX_STR_LEN-1; i++) str[i]=*(pstr+i);
   if(i) str[i] = '\0'; /* NULL terminate the string */
#ifdef DEBUG_CLIF
   printf("\n clif_string_after returning OK");
#endif
   return(CF_OK);
}
/* ********************************************************************** */
char* rcfClifParser::strdup(char *originalString)
{
  // int length = strlen(originalString)+1;
#ifdef DEBUG
  printf("\n Strdup: original %d copy %d", strlen(originalString),length); 
#endif
  char *copyString = (char*) malloc( (strlen(originalString)+1)*sizeof(char));
  if( copyString == NULL) return(NULL);
  strcpy(copyString,originalString);
  return(copyString);
}
/* ********************************************************************* */
int rcfClifParser::checkForStoreString(char *input_string)
{
   /* Check for need to replace = { with ={ */
   /* IMPORTANT...Checks if is Store so only does for store */
   if( strstr(input_string, "Store = {") != NULL)
   {
     // Make replica of original string
     char *oldString;
     oldString = strdup(input_string);
     // get length of first part
     int keepLen=strstr(input_string,"= {")-input_string;
     // put ={ on string
     strcpy(input_string+keepLen,"={"); 
     // and place rest on the string (in case more information)
     strcat(input_string,oldString+keepLen+3);
#ifdef DEBUG_STORE
     printf("\n Replacing %s with %s", oldString, input_string);
#endif
     free(oldString);
   } 
   else 
   if( strstr(input_string, "= SimpleString {") != NULL)
   {
     // Make replica of original string
     char *oldString;
     oldString = strdup(input_string);
     // get length of first part
     int keepLen=strstr(input_string,"= SimpleString {")-input_string;
     // put ={ on string
     // July 15, 2003: JVS testing here
     strcpy(input_string+keepLen,"={"); 
     // strcpy(input_string+keepLen,"."); 
     // and place rest on the string (in case more information)
     strcat(input_string,oldString+keepLen+16);
#ifdef DEBUG_STORE
     printf("\n Replacing %s with %s", oldString, input_string);
#endif
     free(oldString);
   }
   return(CF_OK);
}
/* *********************************************************************** */
char* rcfClifParser::clifFgets(char *string, int Max_Len, FILE *fp)
{
  // int debug_flag=1;
   do{
      if( fget_cstring(string, Max_Len, fp) != CF_OK )
      {
         myerrno = END_OF_FILE;
         return(NULL); 
      }
      checkForStoreString(string);
      /* remove spaces off the end of lines */
      int len=strlen(string)-1; /* look at last char of string */
      while( len >=0 && isspace(string[len]) ) 
      {
         string[len--]='\0'; /* remove if it is a space */
      }
   }while(strlen(string) == 0);
   /* Check that string ended properly with a ";" or a "{" */
   int lastChar=strlen(string)-1;
   if( string[lastChar] != ';' && string[lastChar] != '{' && (NULL == strstr(string,":")) )
   {
     if(1 == debug_flag)
     {
        printf("\n Last Char of String %s is %c", string, string[lastChar]);
        printf("\n String end not found, read next line from the file");
     }
     char nextString[MAX_STR_LEN];
     if( clifFgets(nextString,MAX_STR_LEN, fp ) == NULL )
     {
       if( myerrno == END_OF_CONTEXT ) return(NULL);
       if( myerrno == CF_FAIL ){
	 printf("\n ERROR: rcfClifParser::clifFgets\n");
	 return(NULL); 
       }
     }
     int nStrLen=strlen(nextString);
     int iNext=0;
     if(1 == debug_flag) printf("\n Checking for Space");
     while(iNext < nStrLen && isspace(nextString[iNext]) ) iNext++;
     if(iNext == nStrLen) {
       myerrno = NOT_FOUND;
       return(NULL);
     }
     if(1 == debug_flag)
     {
         printf ("\n Adding On String %s", nextString+iNext);
     }

     // check to make sure have room to put this on the string
     if( strlen(string)+nStrLen-iNext >= (unsigned) Max_Len) {
       printf("\n ERROR: clifFgets: composite string length of\n\t%s\n\tand\n\t%s\n\twill be longer than Max_Len=%d ",string,nextString+iNext,Max_Len); 
       myerrno = CF_FAIL;
       return(NULL);
     } 

     // concatonate the two strings together
     strcat(string,nextString+iNext);

     if(1 == debug_flag)
     {
        printf("\n Final String is: %s", string);
        printf("\n Final String Length = %ld", strlen(string));
     }

     if(strlen(string) >= (unsigned) Max_Len)
     {
       printf("\n ERROR: string Length too long, length = %ld, maxLen = %d",strlen(string), Max_Len );
       myerrno = CF_FAIL;
       return(NULL);
       } 
   }
   /* check that not END_OF_CONTEXT */
   if( strstr(string, "};") != NULL)
   {
      myerrno = END_OF_CONTEXT;
      return(NULL);
   }
   // printf("\n clifFgets returning (%d) %s", strlen(string), string);
   return(string);
}
/* ********************************************************************** */
char* rcfClifParser::clif_fgets(char *string, int Max_Len, FILE *fp)
{
   if( fget_cstring(string, Max_Len, fp) != CF_OK )
   {
      printf("\n ERROR: END OF FILE ON READ : %s ", string);
      myerrno = CF_FAIL;
      return(NULL); 
   }
   checkForStoreString(string);
   /* remove spaces off the end of lines */
   int len=strlen(string)-1; /* look at last char of string */
   while( isspace(string[len]) ) 
   {
      string[len--]='\0'; /* remove if it is a space */
   }
   /* check that not END_OF_CONTEXT */
   if( strstr(string, "};") != NULL)
   {
      myerrno = END_OF_CONTEXT;
      return(NULL);
   }
   return(string);
}
/* ********************************************************************** */
int rcfClifParser::clif_check_if_line(FILE *istrm, char *string, char *input_string) 
                                               /* checks if string is on line*/
{                                              /* in a file, return NULL if  */
                                             /* line not start with string */
					      /* returns pointer to end of  */
   int slength;                          /* string if found            */
   
   /* static int level=0; */
   if( fget_cstring(input_string, MAX_STR_LEN, istrm) != CF_OK )
   {
       if(strstr(string, "={") != NULL ) return(END_OF_CONTEXT); // JVS: 11/3/99:
      printf("\n ERROR: END OF FILE ON READ : %s ", string);
      sprintf(input_string,"EOF"); /* 7-11-97: JVS */
      return(CF_FAIL); 
   }
   checkForStoreString(input_string); // check for = { in string
#ifdef DEBUG_CLIF
   printf(" Line: %s (len = %d) \n",input_string, strlen(input_string) );
#endif
   slength = strlen(string);  // compare over length of input string
   int s_val = 0;             // location in string
   // advance out space in the input string
   while(isspace( (input_string[s_val])  )) s_val++;
   // check for match with the string, return if found 
   // use to be strncasecmp but new linux cannot find this, Dec 20, 1999 JVS
#ifdef DEBUG_CLIF
    printf("\n Comparing %s and %s for %d chars", string, input_string+s_val, slength);
#endif
   if( strncmp(string, input_string+s_val, slength) == 0)
   {
#ifdef DEBUG_CLIF
      printf("\n MATCH FOUND");
#endif
      strcpy(input_string,input_string+s_val+slength);   
      return(CF_OK);
   }
   // check for end of context 
   if( strstr(input_string+s_val, "};") != NULL)
   {
      return(END_OF_CONTEXT);
   }
   // check for beginning of context 
   int ctmp;
   if( strstr(input_string+s_val, "={") != NULL)
   { // advance till this context ends
     //  printf("\n Context level %d Started %s",++level,input_string);
      do{
 
         ctmp=clif_check_if_line(istrm,(char*)"xxEND_OF_CONTEXTxx",input_string);
         if(ctmp == CF_FAIL)
	 {
            printf("\n ERROR: clif_check_if_line reports %s", input_string);
            printf("\n\t Looking for %s",string);
            return(CF_FAIL);
         }
      }while(ctmp != END_OF_CONTEXT);
      // printf(" Context level %d exited",level--);
   }
   input_string = NULL;
   return(NOT_FOUND);
}
/* *********************************************************************** */
int rcfClifParser::clif_string_read(FILE *fp, char *string, char *ostring)
{
   char tstring[MAX_STR_LEN];  // setup string of correct length
   if(clif_string_after(fp,string,tstring)!=CF_OK)
   { 
      printf("\n ERROR: clif_string_read: Reading %s",string); 
      return(CF_FAIL);
   }
   /* check if input string ended in : ,if so, then no outer " " or / / around it */
   int ilen = strlen(string);
   if( string[ilen-1] == ':' )
   {
      strcpy(ostring, tstring);
      return(CF_OK);
   }
   /* trim off leading " and ending "; from the string */
   int len=strlen(tstring);
   if(tstring[len-1] != ';' ||
        !(tstring[0] !=  '"' ||  tstring[0] != '\\') ||
        !(tstring[len-2] != '"' || tstring[len-2] != '\\' ))
   {
      printf("\n ERROR: Reading %s,\t %s is an invalid clif string",string,tstring);
      myerrno = CF_FAIL;
      return(CF_FAIL);
   }
   strcpy(tstring,tstring+1); /* remove leading " */
   tstring[len-3]='\0';       /* null terminate the string */
   strcpy(ostring,tstring);
   return(CF_OK);
}
/* *********************************************************************** */
char* rcfClifParser::strnset(char *s, int ch, size_t n)
{  /* mimic strnset command in dos/ os2/ win / ... */
   if(0 == ch)memset(s,0,n); /* protection against uninitialized buffer */
   for(int i=0; i< (int) n; i++)
   {
     if(s[i] == ((char) 0) ) return(s); // return when find null
      s[i] = ch;
   }
   return(s);
}
/* ************************************************************************** */
int rcfClifParser::fget_cstring(char *string, int Max_Str_Len, FILE *fspec)
{
   /* gets a string from the input and removes comments from it */
   /* allows comments in standard "c" syntax, */
   //        starting with /*
   //        ending with  */
   /* also allows c++ type comments, // causes rest of line to be skipped */

   int check;
   char comment_start[4]="/*";  /* signals start of comment */
   char comment_stop[4]="*/";   /* signals end of comment   */
   int clen; /* length of string for start/stop*/
   int ilen; /* length of input string */
   char string_delim ='"';
   bool str_protect = false;

   int olen; /* location on output string */
   int icnt; /* location on string */

   olen = 0;
   memset(string,0,Max_Str_Len); /* null entire output string */
   /* allocate memory for input string */
      
   char istring[MAX_STR_LEN+1]; // input string
   if(Max_Str_Len > MAX_STR_LEN)
   {
      printf("\n ERROR: bad trouble in fget_cstring");
      printf("\n Need to increase MAX_STR_LEN");
      return(CF_FAIL);
   }

   clen = strlen(comment_start);
   /* read in the line, verify that it exists */
   do{
      /* read in a line from the file */
      if(fgets(istring, MAX_STR_LEN, fspec) == NULL) /* output warning if not a valid read */
      {
         return(CF_FAIL);
      }
      ilen = strlen(istring); /* length of input string */
      if(ilen > MAX_STR_LEN) {
	printf("\n ERROR: fget_cstring: Input string somehow longer than MAX_STR_LEN\n"); return(CF_FAIL);
      }
      // printf("%s", istring);
#ifndef MSDOS
      /* get rid of <cr><lf> in pc based machines */
      if(ilen > 2 && istring[ilen-2] == '\x0d')
      {
       istring[ilen-2] = '\n';
       istring[ilen-1] = '\0';
       ilen=strlen(istring);
      }
#endif
      istring[ilen]='\0';     /* null terminate the string */
      if(ilen < clen)         /* not possible to have comment on the line */
      {                       /* so output the string as is */
         strcpy(string,istring);
         olen = strlen(string);
      }
      else
      {
        /* strip comments out of input string */
        icnt=0;
        do{
          if(istring[icnt] == string_delim)
          {
             /* MFatyga - 06-30-2008
                This section processes c++ and c-style comments
                str_protect mechanism suppresses detection of c++ style
                comments inside quoted strings. This is needed because
                of processing of unix-style paths. An accidental inclusion
                of double slash is legal from the standpoint of c-shell,
                but it completely confuses the code below. The behavior is
                now: if double slash similar to c++ comment is found between
                string quotes, it will not be interpreted as c++ comment but 
                rather taken at the face value and left alone.
             */
             if(str_protect)str_protect = false; 
             else str_protect = true;
          }
          check = 1;
          if(icnt < ilen - clen)  /* make sure have enough characters for start of comment */
             check = strncmp(istring+icnt,comment_start,clen); /* check if start of comment */
          if(check == 0) /* comment found for standard c syntax */
          {
#ifdef DEBUG
            printf("\n Comment Found on %s", istring+icnt);
#endif
            /* find end of comment */
            icnt+=clen; /* advance past comment delimeter */
            clen=strlen(comment_stop); /* get length of end of comment delimeter */
            /* look for end of comment till end of string */
            do{
               check = 1;
               if(icnt < ilen - clen)  /* make sure have enough characters for end of comment */
               {
		 // printf("\n Comparing %s", istring+icnt);
                  check = strncmp(istring+icnt, comment_stop,clen);
               }
               if(check != 0)  /* if not end of comment */
               {
                  icnt++; /* increment location on string */
                  if(icnt>ilen) /* if advance past end of string, get a new one */
                  {
                     if(fgets(istring, MAX_STR_LEN, fspec) == NULL) /* output warning if not a valid read */
                     {
                        printf ("\nERROR: Reading File, looking for end of comment %s",comment_stop);
                        printf ("\n Final String %s", istring);
                        // fclose(fspec); /* JVS: Nov 19, 1999 */
                        // free(istring);
                        return(CF_FAIL);
                     }
                     ilen = strlen(istring); /* get length of this new string */
#ifndef MSDOS
                     /* get rid of "newline" in pc based machines */
                     if(ilen > 2 && istring[ilen-2] == '\x0d')
                     {
                        istring[ilen-2] = '\n';
                        istring[ilen-1] = '\0';
                        ilen=strlen(istring);
                     }
#endif
                     istring[ilen]='\0';     /* null terminate the string */
                     icnt = 0; /* reset the counter to the start of the string */
                  }
               }
               else
               {
                  icnt+=clen; /* advance past comment delimiter */
               }
            }while(check != 0); /* end of comment found */
            // printf("\n End Of Comment Found");
          }  /* end if */
          else /* check if comment is in c++ format */
          {
             check = strncmp(istring+icnt, "//",2);
             if((check == 0) && (!str_protect) ) /* c++ style comment found */
             {  /* skip till end of string */
                icnt = ilen;   /* end loop */
                string[olen++]=' ';
                string[olen]='\0'; /* terminate the string */
             }
             else /* is a valid character for the string */
             {
                string[olen++] = istring[icnt++]; /* append value to the string */
                string[olen]='\0';
             }
          }
        }while(icnt < ilen          &&    /* do till end of string */
               olen < Max_Str_Len); /* and output string not too long */
        /* check for only carriage return (should have been caught above) */
        if(olen == 1 && (string[0] == '\0' || string[0] == '\n')){
         olen = 0;
        }

      }  /* end else */
   }while(olen == 0); /* do till read in a string */
   if(olen >= Max_Str_Len)
   {
      printf ("ERROR: fget_cstring: Input line too long");
      return(CF_FAIL);
   }
   return(CF_OK);
}
/* ***************************************************************************** */


