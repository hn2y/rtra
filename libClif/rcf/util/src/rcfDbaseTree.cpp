#include "rcfDbaseTree.hpp"

void rcfDbaseTree::basicInit()
{
    dbasePath.clear();
    resolvedPath.clear();
    sub.clear();
    admin = NULL;
    myAdmin = false;
    myParser = NULL;
    debug_flag = false;
    debug_log = stderr;
    debug_log_own = false;
}

rcfDbaseTree::rcfDbaseTree()
{
    basicInit();
}

rcfDbaseTree::~rcfDbaseTree()
{
    for( unsigned int i=0; i<sub.size(); i++ )
    {
        if(NULL != sub[i]) { delete sub[i]; }
    }
    if(myAdmin && (NULL != admin)) { delete admin; }
    if(NULL != myParser) { delete myParser; }
    if( debug_log_own && (stderr != debug_log) ) { fclose(debug_log); }
}

int rcfDbaseTree::attachToDbase(std::string path)
{
    func = (char*)"rcfDbaseTree::attachToDbase";
    int rtn;

    admin = new rcfAdmin;
    if(NULL == admin)
    {
        reportError("Failure to allocate memory for admin object");
        return CF_FAIL;
    }
    rtn = admin->readConfigFile((char*)(path.c_str()));
    if(CF_OK != rtn)
    {
        reportError("Failure to initialize rcfAdmin from file");
        fprintf(stderr,"Location: %s\n",path.c_str());
        delete admin;
        admin = NULL;
        return CF_FAIL;
    }
    if(!(admin->isReady()))
    {
        reportError("rcfAdmin reports as not ready");
        fprintf(stderr,"Location: %s\n",path.c_str());
        delete admin;
        admin = NULL;
        return CF_FAIL;
    }    
    
    myAdmin = true;
    return CF_OK;
}

int rcfDbaseTree::connectToDbase(std::string path)
{
    func = (char*)"rcfDbaseTree::connectToDbase";
    int rtn;
    rtn = attachToDbase(path);
    if(CF_OK != rtn)
    {
       reportError("Failure to attach RCF database");
       return CF_FAIL;
    }
    else
    {
        return setMyPath(path); // build the tree
    }
}

int rcfDbaseTree::setRcfAdmin(rcfAdmin *extAdmin)
{
    func = (char*)"rcfDbaseTree::setRcfAdmin";

    if(NULL == extAdmin)
    {
        reportError("User passed in NULL pointer in lieu of argument");
        return CF_FAIL;
    }
    if(!(extAdmin->isReady()))
    {
        reportError("user passed in rcfAdmin that reports as not ready");
        admin = NULL;
        return CF_FAIL;
    }
    
    admin = extAdmin;
    myAdmin = false;
    
    return CF_OK;
}

int rcfDbaseTree::setMyDbasePath(std::string upath)
{
    func = (char*)"rcfDbaseTree::setMyDbasePath";
    int verify;
    if(upath.size() <= 0 )
    {
        reportError("Relative path can not be empty. Please use connectToDbase instead");
        return CF_FAIL;
    }
    if(NULL == admin)
    {
        reportError("User requests to set relative database path, but the connection to database has not been made");
        return CF_FAIL;
    }
    if(!(admin->isReady()))
    {
        reportError("internal admin reports as not ready, serious RCF error");
        admin = NULL;
        return CF_FAIL;
    }
    char *tbuf = NULL;
    tbuf = admin->AppendPathToConfigDirectory((char*)(upath.c_str()),&verify);
    if( (0==verify) || (NULL==tbuf) )
    {
        reportError("Failure to convert relative database path to resolved path");
        return CF_FAIL;
    }
    std::string myPath = tbuf;
    delete [] tbuf;
    return setMyPath(myPath);
}

int rcfDbaseTree::setMyPath(std::string upath)
{
    func = (char*)"rcfDbaseTree::setMyPath";
    int rtn,verify;
    bool bverify;

    if(upath.size() <= 0)
    {
        reportError("User passed in empty string as argument");
        return CF_FAIL;
    }
    // load line
    rtn = loadLine((char*)(upath.c_str()));
    if(CF_OK != rtn)
    {
        reportError("Failure to parse line");
        fprintf(stderr,"Failing line: %s\n",upath.c_str());
        return CF_FAIL;
    }
    
    // test if line exists:
    rtn = testLine(&verify);
    if( (CF_OK != rtn) || (0 == verify) )
    {
        reportError("user passed in directory line that does not exist");
        fprintf(stderr,"Offending line: %s\n",upath.c_str());
        return CF_FAIL;
    } 
    // all is well, record location 
    resolvedPath = upath;
    if(debug_flag) { fprintf(debug_log,"recording resolved path: %s\n",resolvedPath.c_str()); }
    if(NULL != admin)
    {
        //record database path
        dbasePath = admin->ComputeRelativePathToConfig(upath,&bverify);
        if( !bverify)
        {
            reportError("Failure to compute relative path to config");
            fprintf(stderr,"offending absolute path: %s\n",upath.c_str());
        }
        if(debug_flag) { fprintf(debug_log,"recording database path: %s\n",dbasePath.c_str()); }
    }
    // stat the line
    int lnfiles,lndirs;
    rtn = statLine(&lndirs,&lnfiles);
    if(CF_OK != rtn)
    {
        reportError("Failure to stat the directory");
        fprintf(stderr,"Offending directory: %s\n",upath.c_str());
        return(CF_FAIL);
    }
    if( (0 == lnfiles) && (0 == lndirs) ) { return CF_OK; }// nothing to do
    else
    {
        if(lnfiles > 0)
        {
            // search for config
            std::string cname = RCF_ADMIN_DEF_CONFIGNAME;
            rtn = testFile((char*)(cname.c_str()),&verify);
            if(CF_OK != rtn)
            {
                reportError("Failure to test for config file");
                fprintf(stderr,"Offending directory: %s\n",upath.c_str());
                return CF_FAIL;
            }
            if(1 == verify)
            {
                // found it
                std::string cpath;
                cpath = upath+separator+cname;
                myParser = new rcfConfigParser;
                if(NULL == myParser)
                {
                    reportError("Failure to allocate memory for configuration parser");
                    return CF_FAIL;
                }
                // load file
                rtn = myParser->loadConfigFile((char*)(cpath.c_str()));
                if(CF_OK != rtn)
                {
                    reportError("Failure to initialize parser with config file");
                    delete myParser;
                    myParser = NULL;
                    return CF_FAIL;
                }
                if(NULL != admin)
                {
                    rtn = myParser->setRcfAdmin(admin);
                    if(CF_OK != rtn)
                    {
                        reportError("Failure to set rcfAdmin in the newly created parser");
                        delete myParser;
                        myParser = NULL;
                        return CF_FAIL;
                    }
                    rtn = myParser->armParser();
                    if(CF_OK != rtn)
                    {
                       reportError("Failure to arm config parser");
                       delete myParser;
                       myParser = NULL;
                       return CF_FAIL;
                    }
                }
            }
        } // end file processing segment
      
        if(lndirs > 0 )
        {
            rcfDbaseTree *lnode = NULL; 
            // process directories. CAUTION -- recursion occurs here
            std::string lnodePath;
            std::string lnodeName;
            int skip = 0;
            for(int idir=0; idir<lndirs; idir++)
            {
                lnodeName = this->getDirectoryNameInLine(idir,&verify);
                if( (0==verify) || (lnodeName.size() <=0) )
                {
                    reportError("Failure to get directory name");
                    fprintf(stderr,"Offending path: %s \t index: %d\n",upath.c_str(),idir);
                    return CF_FAIL;
                }
                if(0 == lnodeName.compare(std::string(".")))
                {
                    skip++;
                    continue;
                }
                if(0 == lnodeName.compare(std::string("..")))
                {
                    skip++;
                    continue;
                }
                lnode = NULL;
                lnode = new rcfDbaseTree;
                if(NULL == lnode)
                {
                    reportError("Failure to allocate memory for the directory tree node");
                    return CF_FAIL;
                }
                if(NULL != admin)
                {
                    rtn = lnode->setRcfAdmin(admin);
                    if(CF_OK != rtn)
                    {
                        reportError("Failure to set rcfAdmin in subordinate directory tree node");
                        return CF_FAIL;
                    }
                }
                lnode->matchOptions(this);
                // now the recursive part
                lnodePath = upath+separator+lnodeName;
                rtn = lnode->setMyPath(lnodePath); // RECURSION
                if(CF_OK != rtn)
                {
                    reportError("Failure to initialize new directory node in recursive loop");
                    fprintf(stderr,"Offending Line: %s\n",lnodePath.c_str());
                    return CF_FAIL;
                }
                // all is really well, store the node
                sub.push_back(lnode);
            } // end of recursive loop over subordinate nodes
         
            // final post-recursive sanity check
            if( sub.size() != (lndirs-skip) )
            {
                reportError("Inconsistent directory count");
                fprintf(stderr,"Expected count: %d \t reported count: %lu\n",lndirs,sub.size());
            }
        } // end sub-directory processing segment
    }
   return CF_OK;
}

int rcfDbaseTree::executeQuery(rcfDbaseQuery * query)
{
    func = (char*)"rcfDbaseTree::executeQuery";
    int rtn;

    if(NULL == query)
    {
        reportError("User passed in NULL pointer in lieu of query");
        return CF_FAIL;
    }
    if( !(query->isQuerySet()) )
    {
        reportError("User passed in query that has not been set");
        return CF_FAIL;
    }
   
    // run the query on self if there is a config parser present
    if(NULL != myParser)
    {
        rtn = query->executeQuery(this);
        if(CF_OK != rtn)
        {
            reportError("Failure to execute query on self");
            fprintf(stderr,"Resolved Path where error occured: %s\n",resolvedPath.c_str());
        }
    }
    // run the query on subordinates (CAUTION: recursion occurs here)
    for( unsigned int i=0; i<sub.size(); i++ )
    {
        rtn = sub[i]->executeQuery(query);
        if(CF_OK != rtn)
        {
            reportError("Failure to execute query on subordinate");
            fprintf(stderr,"Resolved Path where error occured: %s\nt subordinate index %u",resolvedPath.c_str(), i);
        }
    }
   
    return CF_OK;
}

int rcfDbaseTree::setDebugLog(std::string upath)
{
    func = (char*)"rcfDbaseTree::setDebugLog";
    if(upath.size()<=0)
    {
        reportError("user passed empty string in lieu of argument path");
        return CF_FAIL;
    }
    debug_log = NULL;
    debug_log = fopen(upath.c_str(),"w");
    if(NULL == debug_log)
    {
        reportError("Failure to open debug log");
        fprintf(stderr,"attempted open: %s\n",upath.c_str());
        debug_log = stderr;
        return CF_FAIL;
    }
    debug_log_own = true;
    return CF_OK;
}

int rcfDbaseTree::enableDebug(std::string upath)
{
    func = (char*)"rcfDbaseTree::enableDebug";
    int rtn;
    rtn = setDebugLog(upath);
    if(CF_OK != rtn)
    {
        reportError("Failure to set debug log");
        return CF_FAIL;
    }
    debug_flag = true;
    return CF_OK;
}

int rcfDbaseTree::enableDebug()
{
    debug_flag = true;
    debug_log = stderr;
    return CF_OK;
}

void rcfDbaseTree::matchOptions(rcfDbaseTree * arg)
{
    if(NULL == arg) { return; }
    debug_flag = arg->debug_flag;
    debug_log = arg->debug_log;
}

std::string rcfDbaseTree::getMyDbasePath(bool *valid)
{
    if(dbasePath.size() > 0) { *valid = true; }
    else { *valid = false; }
    return dbasePath;
}
std::string rcfDbaseTree::getMyResolvedPath(bool *valid)
{
    if(resolvedPath.size() > 0) { *valid = true; }
    else { *valid = false; }
    return resolvedPath;
}
rcfConfigParser* rcfDbaseTree::getMyParser(bool *valid)
{
    func = (char*)"rcfDbaseTree::getMyParser";
    if(NULL == myParser)
    {
        *valid = false;
        return NULL;
    }
    if(!(myParser->isReady()))
    {
        reportError("Parser is present, but it tests not ready");
        *valid = false;
        return NULL;
    }
    *valid = true;
    return myParser;
}

// ------------------ Start Code for Database Query ------------------------
void rcfDbaseQuery::basicInit()
{
    myQuery.tag.clear();
    myQuery.value.clear();
    myQuery.request = true;
    myComplexQuery.orQueue.clear();
    myComplexQuery.andQueue.clear();
    set = false;
    complex = false;
    dbasePaths.clear();
    resolvedPaths.clear();
    parsers.clear();
}

rcfDbaseQuery::rcfDbaseQuery()
{
    basicInit();
}

rcfDbaseQuery::~rcfDbaseQuery()
{
}

int rcfDbaseQuery::setQuery(rcfConfigQuery & uquery)
{
    if(set)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  Simple query has already been set, create a new object");
        return CF_FAIL;
    }
    if(uquery.tag.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  tag can not be empty");
        return CF_FAIL;
    } 
    else { myQuery.tag = uquery.tag; }

    if(uquery.value.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  value can not be empty can not be empty");
        return CF_FAIL;
    } 
    else { myQuery.value = uquery.value; }
   
    myQuery.request = uquery.request;
    set = true;
    return CF_OK;
}

int rcfDbaseQuery::setQuery(std::string tag, std::string value, bool request)
{
    if(set)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  Simple query has already been set, create a new object");
        return CF_FAIL;
    }
    if(tag.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  tag can not be empty");
        return CF_FAIL;
    } 
    else { myQuery.tag = tag; }

    if(value.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::setQuery  value can not be empty can not be empty");
        return CF_FAIL;
    } 
    else { myQuery.value = value; }
   
    myQuery.request = request;
    set = true;
    return CF_OK;
}

int rcfDbaseQuery::appendComplexQuery(rcfConfigQuery & uquery, int select)
{
    if(set && !complex)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::appendComplexQuery  Simple query has already been set, create a new object");
        return CF_FAIL;
    }
    if(uquery.tag.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::appendComplexQuery  tag can not be empty");
        return CF_FAIL;
    } 
    if(uquery.value.size() <= 0)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::appendComplexQuery  value can not be empty");
        return CF_FAIL;
    } 
    if(0 == select) { myComplexQuery.andQueue.push_back(uquery); }
    else if(1 == select) { myComplexQuery.orQueue.push_back(uquery); }
    else
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::appendComplexQuery  unrecognized selector value, only 0 or 1 allowed");
        fprintf(stderr,"Selector seen: %d\n",select);
        return CF_FAIL;
    }
    set = true;
    complex = true;
    return CF_OK;
}

int rcfDbaseQuery::appendComplexAndQuery(rcfConfigQuery & uquery)
{
    return(appendComplexQuery(uquery,0));
}

int rcfDbaseQuery::appendComplexOrQuery(rcfConfigQuery & uquery)
{
    return(appendComplexQuery(uquery,1));
}

int rcfDbaseQuery::appendComplexAndQuery(std::string tag, std::string value, bool request)
{
    rcfConfigQuery buf;
    buf.tag = tag;
    buf.value = value;
    buf.request = request;
    return appendComplexQuery(buf,0);
}
int rcfDbaseQuery::appendComplexOrQuery(std::string tag, std::string value, bool request)
{
    rcfConfigQuery buf;
    buf.tag = tag;
    buf.value = value;
    buf.request = request;
    return appendComplexQuery(buf,1);
}

int rcfDbaseQuery::executeQuery(rcfDbaseTree *host)
{
    // check if host has a parser, if not bail out with success
    rcfConfigParser *lparser = NULL;
    bool bvalid;
    bool frslt;
    lparser = host->getMyParser(&bvalid);
    if(!bvalid || (NULL == lparser) ) { return CF_OK; }

    // check if host parser is valid, if not bail with failure 
    if( !(lparser->isReady()) )
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::executeQuery host parser is present, but it reports as not ready\n");
        return CF_FAIL;
    }

    // for simple query, test parser, if query found, append result
    if( !complex && set )
    {
        //simple query
        std::string value;
        int ssize = 0;
        bool ifound = false;
        bool rslt = false;
        do
        {
            value = lparser->getRepeatTag(myQuery.tag);
            ssize = value.size();
            if(ssize > 0)
            {
                if(0 == value.compare(myQuery.value))
                {
                    rslt = true;
                    ifound = true;
                    break; // we found it, get out immediately
                }
            }
        }while(ssize > 0);

        if(!ifound) { rslt = false; }
         
        if( rslt && myQuery.request ) { frslt = true; }
        else if( !rslt && !(myQuery.request) ) { frslt = true; }
        else { frslt = false; }

        lparser->rewind(); // leave parser in a pristine state      
    }
    else if(complex && set )
    {
        std::string value;
        int ssize = 0;
        bool ifound = false;        
        bool andRslt = false;
        bool orRslt = false;
        int andSize = myComplexQuery.andQueue.size();
        int orSize = myComplexQuery.orQueue.size();

        if( (0 == orSize) && (0 == andSize) )
        {
            fprintf(stderr,"ERROR: rcfDbaseQuery::executeQuery query object in inconsistent internal state. Reports complex query, but both queues are empty\n");
            return CF_FAIL;
        }

        for(int iAnd=0; iAnd<andSize; iAnd++)
        {
            ifound = false;
            andRslt = true;
            do
            {
                value = lparser->getRepeatTag(myComplexQuery.andQueue[iAnd].tag);
                ssize = value.size();
                if(ssize > 0)
                {
                    if(0 == value.compare(myComplexQuery.andQueue[iAnd].value))
                    {
                        ifound = true;
                        break; // we found it, get out immediately
                    }
                }
            }while(ssize > 0);

            lparser->rewind(); // rewind the parser

            if( (ifound && !(myComplexQuery.andQueue[iAnd].request)) )
            {
                andRslt = false;
                break;
            }
            if( (!ifound && (myComplexQuery.andQueue[iAnd].request)) )
            {
                andRslt = false;
                break;
            }
        }

        for(int iOr=0; iOr<orSize; iOr++)
        {
            ifound = false;
            orRslt = false;
            do
            {
                value = lparser->getRepeatTag(myComplexQuery.orQueue[iOr].tag);
                ssize = value.size();
                if(ssize > 0)
                {
                    if(0 == value.compare(myComplexQuery.orQueue[iOr].value))
                    {
                        ifound = true;
                        break; // we found it, get out immediately
                    }
                }
            }while(ssize > 0);

            lparser->rewind(); // rewind the parser

            if(ifound && (myComplexQuery.orQueue[iOr].request) )
            {
                orRslt = true;
                break;
            }
            if(!ifound && !(myComplexQuery.orQueue[iOr].request) )
            {
                orRslt = true;
                break;
            }
        }

        frslt = andRslt || orRslt;
    }
    else
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::executeQuery inconsistent state of the query object\n");
        return CF_FAIL;
    }

    if(frslt)
    {
        //query passed, download result
        std::string tbuf;
        tbuf = host->getMyResolvedPath(&bvalid);
        if(!bvalid)
        {
            // resolved path ought to be there
            fprintf(stderr,"ERROR: rcfDbaseQuery::executeQuery host does not present valid resolved path\n");
            return CF_FAIL;
        }
        else { resolvedPaths.push_back(tbuf); }
        tbuf.clear();
        tbuf = host->getMyDbasePath(&bvalid);
        if(!bvalid)
        {
            // database path may not be there
        }
        else { dbasePaths.push_back(tbuf); }

        parsers.push_back(lparser); // save pointer to parser
    }

    // all is well, exit with success
    return CF_OK;
}

void rcfDbaseQuery::resetResult()
{
    dbasePaths.clear();
    resolvedPaths.clear();
    parsers.clear();
}

int rcfDbaseQuery::getNumberOfReturns(bool *valid)
{
    *valid = false;
    if( parsers.size() == resolvedPaths.size() )
    {
        *valid = true;
        return parsers.size();
    }
    else
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getNumberOfReturns inconsistent database query state\n");
        fprintf(stderr,"Number of returned parsers: %lu\n",parsers.size());
        fprintf(stderr,"Number of returned resolvedPaths: %lu\n",resolvedPaths.size());
        fprintf(stderr,"Number of returned dbasePaths: %lu\n",dbasePaths.size());
        return 0;
    }
}

std::string rcfDbaseQuery::getResolvedPath(int index,bool *valid)
{
    *valid = false;
    std::string empty = "";
    int nret;
    bool lvalid;

    *valid = false;
    nret = getNumberOfReturns(&lvalid);
    if(!lvalid)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getResolvedPath Failure to test number of returns\n");
        return empty;
    }
    if( (index >= nret) || ( index<0 ) )
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getResolvedPath invalid index value. Value seen: %d  Maximum allowed: %d\n",index,nret-1);
        return empty;
    }
    *valid = true;
    return resolvedPaths[index];
}

std::string rcfDbaseQuery::getDbasePath(int index,bool *valid)
{
    *valid = false;
    std::string empty = "";
    int nret;
    bool lvalid;

    *valid = false;
    nret = getNumberOfReturns(&lvalid);
    if(!lvalid)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getDbasePath Failure to test number of returns\n");
        return empty;
    }
    if( (index >= nret) || ( index<0 ) )
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getDbasePath invalid index value. Value seen: %d  Maximum allowed: %d\n",index,nret-1);
        return empty;
    }
    *valid = true;
    return dbasePaths[index];
}

rcfConfigParser* rcfDbaseQuery::getParser(int index,bool *valid)
{
    *valid = false;
    int nret;
    bool lvalid;

    *valid = false;
    nret = getNumberOfReturns(&lvalid);
    if(!lvalid)
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getParser Failure to test number of returns\n");
        return NULL;
    }
    if( (index >= nret) || ( index<0 ) )
    {
        fprintf(stderr,"ERROR: rcfDbaseQuery::getParser invalid index value. Value seen: %d  Maximum allowed: %d\n",index,nret-1);
        return NULL;
    }
    *valid = true;
    return parsers[index];
}
