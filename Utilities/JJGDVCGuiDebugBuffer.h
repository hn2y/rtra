#ifndef JJGDVCGUIDEBUGBUFFER
#define JJGDVCGUIDEBUGBUFFER
// customized stream buffer designed to be used inside Qt GUI;
// stream buffer can be used in constructor of std::ostream;
// resulting ostream will then write to a string (passed as
// arg to stream buffer constructor) and via qDebug() to cerr

#include <iosfwd>  // streamsize
#include <boost/iostreams/categories.hpp>  // sink_tag
#include <iostream>
#include <fstream>
#include <iomanip>

class JJGDvcGuiDebugBuffer : public boost::iostreams::sink
{
  public:

    std::string & buffstr;
    std::ostream * logstrm;

    std::string & get_buffstr() { return buffstr; }

    void set_logstrm(std::ostream & stream)
      { logstrm = & stream; }

    std::streamsize write(const char* s, std::streamsize n)
    {
        // Write up to n characters to the underlying
        // data sink into the buffer s, returning the
        // number of characters written

        std::string str(s,n);
        qDebug("%s",str.c_str()); // WC: added format string to avoid warning
        if (logstrm) logstrm->write(s, n);
        buffstr = buffstr + str;
        return n;
    }

    // constructor
    JJGDvcGuiDebugBuffer(std::string & bstr) :
      buffstr(bstr), logstrm(0) { }
};
#endif // JJGDVCGUIDEBUGBUFFER

