#ifndef __rcfAdmin
#define __rcfAdmin

#include <string>
#include <vector>

// Created: 09-21-2007
// by M.Fatyga
// This object both stores the top level RCF
// information, and provides global services
// for other RCF objects. This is an RCF private
// utility object, not intended for stand-alone use.
// At creation, the information stored are absolute
// paths to data locations. Service rendered is path
// update for the user (user is an RCF object).

#define RCF_MAXADMINTAGS 100 

// Maximum path length allowed (in chars)
#define RCF_MAXADMINPATHLENGTH 20000
#define RCF_DEFAULT_WRITE_TAG LOCDATA

//default name for rcfconfig file
#define RCF_ADMIN_DEF_CONFIGNAME "config"
//default name for admin directory 
#define RCF_ADMIN_DEF_ADMINDIR "admin"

//The constructor takes the absolute path to the RCF database.
// The object **expects** a specific name of the config file:
// "usr_path_to_rcf"/admin/config . The admin file is now restricted
// to cliff format.  
// config file are as follows:
// type = RCFADMIN; is a mandatory line, all other lines optional.
// each aggregate storage path is defined by a cliff, with tag 'keyword'
// data elements key = MYNAME and path = path_to_storage_area

/**
    \class rcfAdmin
    @ingroup UTIL
*/

class rcfAdmin
{

   private:
      char *func;
      char *tags[RCF_MAXADMINTAGS];
      char *paths[RCF_MAXADMINTAGS];
      char *default_configname;
      char separator[2];
      char *mypath;
      int is_ready;
      int ntags;
      std::vector<std::string> store_tags;
      std::vector<std::string> store_paths;
      std::vector<bool> writable;


  public:

      rcfAdmin();
      /// Create empty object
      rcfAdmin(char *file);
      /// Create admin object by parsing admin config file
      ~rcfAdmin();

      int isReady(); ///< verify that object is ready for use 
      int readConfigFile(char *path);

      /// Append path to data.
      // Note: memory is allocated here, it is
      // users responsibility to de-allocate it after use
      /// Inputs: key refers to the correlation table keyword
      ///         path is a path to be appended
      char* AppendPath(char *key, char *path, int *verify);
      char* AppendPathDeleteOriginal(char *key, char *path, int *verify);
      /// Append path to Config file.
      // Note: memory is allocated here, it is
      // users responsibility to de-allocate it after use
      char* AppendPathToConfig(char *path, int *verify);
      char* AppendPathToConfigDeleteOriginal(char *path, int *verify);
      char* AppendPathToConfigDirectory(char *path, int *verify);
      char* AppendPathToConfigDirectoryDeleteOriginal(char *path, int *verify);
      std::string ComputeRelativePathToConfig(std::string path, bool *verify);

      /// Show path to RCF Database
      char *showMyPath(int *verify);

      /// Verify Key exists and is properly defined
      bool isKeyDetected(std::string key);
      bool isKeyPathDefined(std::string key);
      bool isKeyWritable(std::string key);

      /// Assignment operator
      rcfAdmin& operator=(const rcfAdmin &admin);
  // service functions
  protected:

      void reportError(const char *str);
      void reportWarning(const char *str);
      int storePathToRcf(char *path);

  private:

      void basicInit();
      char* AppendPathToConfigSelect(int select,char *path, int *verify);
      int updatePointerArrays();

      
};

#endif
