#ifndef CF_OBJECT3D_HPP
#define CF_OBJECT3D_HPP

#include "cfCoord3d.hpp"
/** \class cfObject3d
 * \brief act as the base class for cfDvf3d and cfVolume3d.
 *
 * \author Baoshe Zhang
 */
class cfObject3d
{
public:
	virtual const cfCoord3d<float>& getCoord3d() const { return _coordfloat; }
	virtual ~cfObject3d() { }
private:
	cfCoord3d<float> _coordfloat;
};

#endif
