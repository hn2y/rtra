/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/*  read_pinnacle_plans.cc

    routines to read name of plans from Pinnacle Patient File
  
    Created: Jan 20, 1999: JVS

    Modification History:
   Nov 5, 2007: JVS: eliminate myerrno, replace with checkClifError()

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isspace

#include "utilities.h"
#include "read_clif.h"
#include "plan_info.h"
/* *********************************************************************************** */
int read_pinnacle_plans(char *plan_fname, int *n_plans, plan_info_type **plan)
{
    // printf("\n Reading In Plan Info from file %s",plan_fname);
   FILE *fp = fopen(plan_fname,"r");
   if(fp==NULL)
   {
      eprintf("\n ERROR: opening patient file %s", plan_fname);
      return(FAIL);
   }

   if(clif_get_to_line(fp,"PlanList ={")!=OK)
   {
      eprintf("\n ERROR: Finding PlanList in file %s",plan_fname);
      return(FAIL);
   }
   plan_info_type *lplan=NULL;
   while(clif_get_to_line(fp,"Plan ={")==OK)  
   {
      if(*n_plans==0)
         lplan = (plan_info_type *)calloc(1,sizeof(plan_info_type));
      else
         lplan = (plan_info_type *)realloc( (plan_info_type *) lplan,
                                             ((*n_plans+1)*sizeof(plan_info_type)) );
      if(lplan == NULL)
      {
         eprintf("\n ERROR: allocating memory for plan %d",*n_plans+1);
         return(FAIL);
      }
      *plan = lplan;
 
      lplan[*n_plans].id = (int) clif_get_value(fp, "PlanID =");
      //      printf("\n Plan ID = ", lplan->id);
      if(clif_string_read(fp,"PlanName =",lplan[*n_plans].name) != OK)  
         return(FAIL);
      if(clif_get_to_line(fp,"};")!=OK)
      {
         eprintf("\n ERROR: Closing Plan structure in file %s",plan_fname);
         return(FAIL);
      }
      *n_plans=*n_plans+1;
   }
   resetClifError(); // reset
   /* close the PlanList cliff - autoclosed when not fine next Plan */
   /* if(clif_get_to_line(fp,"};")!=OK)
      {
         eprintf("\n ERROR: Closing PlanList structure in file %s",plan_fname);
         return(FAIL);
      }
   */
   fclose(fp);
   return(OK);
}
/* *********************************************************************************** */
