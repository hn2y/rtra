
// Author: M.Fatyga
// Date Created: 06/21/2011
// Object rcfDbaseTree builds 
// a programmatic picture of an RCF database directory tree. 
// This is a low level RCF utility
// Object rcfDbaseQuery cooperates with rcfDbaseTree traversing
// the built tree and examining config files. This object
// comes back with the full list of paths (resolved and relative)
// and fully armed configuration parsers that satisfy the query 

#ifndef __rcfDbaseTree
#define __rcfDbaseTree

#include <string>
#include <vector>
#include <stdio.h>
#include "rcfDirectoryMgr.hpp"
#include "rcfConfigParser.hpp"

/**
    \class rcfDbaseTree
    @ingroup UTIL
*/

class rcfDbaseTree;

typedef struct
{
   std::string tag;
   std::string value;
   bool request;

}rcfConfigQuery;

typedef struct
{
   std::vector<rcfConfigQuery> orQueue;
   std::vector<rcfConfigQuery> andQueue;
   
}rcfComplexQuery;


class rcfDbaseQuery
{
   protected:

      std::vector<std::string> dbasePaths;
      std::vector<std::string> resolvedPaths;
      std::vector<rcfConfigParser*> parsers;
      rcfConfigQuery myQuery;
      rcfComplexQuery myComplexQuery;
      bool complex;
      bool set;

   public:

      rcfDbaseQuery();
      ~rcfDbaseQuery();

      // define the query

      // simple query returns all config parsers which contain
      // tag-value pair specified by user. Optional variable
      // request can be used to create logical complement of
      // the usual query (NOT this tag-value pair).
      // simple query and complex queries (below) are mutually
      // exclusive. If you set a simple query and then try to
      // add a complex query error will be reported and the
      // requested change will not be made.
      int setQuery(rcfConfigQuery & query);
      int setQuery(std::string tag, std::string value, bool request = true);
      // Complex queries create logical combinations of simple
      // queries. There are two logical queues, 'AND' queue and
      // 'OR' queue. Each of these queues is evaluated separately,
      // and then OR of results from the two queues is taken as a
      // final result of the complex query.
      int appendComplexAndQuery(rcfConfigQuery & query);
      int appendComplexAndQuery(std::string tag, std::string value, bool request = true);
      int appendComplexOrQuery(rcfConfigQuery & query);
      int appendComplexOrQuery(std::string tag, std::string value, bool request = true);

      // execute query is used by RCF, but unlikely to be used
      // by the casual user.
      int executeQuery(rcfDbaseTree * host);

      // reset result can be used to re-use the object.
      // It will leave the query definition intact ,but clear
      // results (eg. querying multiple databases)
      void resetResult();


      bool isQuerySet() { return(set); }

      // extract returns to this query
      int getNumberOfReturns(bool *valid);
      std::string getResolvedPath(int index, bool *valid);
      std::string getDbasePath(int index, bool *valid);
      rcfConfigParser *getParser(int index, bool *valid);

   private:

      void basicInit();
      int appendComplexQuery(rcfConfigQuery & query, int select);

};

// class rcfDbaseTree builds an image of the RCF database directory
// tree in memory. Once the tree is built queries can be run 
// on the object.
// NOTE: building the object can be computationally intensive,
// depending on the size of the directory tree. Queries are much
// faster.
// IMPORTANT NOTE: tree building is recursive. There is a small
// chance of blowing a stack during recursion. The object
// will be improved to provide some recursion limit, but for now
// please beware that very large databases (many thousands of nodes)
// could cause stack overflow.

class rcfDbaseTree : public rcfDirectoryMgr
{

   protected:

      std::vector<rcfDbaseTree*> sub;
      std::string dbasePath;
      std::string resolvedPath;
      rcfConfigParser *myParser;
      rcfAdmin *admin;
      bool myAdmin;
      bool debug_flag;
      FILE *debug_log;
      bool debug_log_own;

   public:

      // default constructor
      rcfDbaseTree();
      ~rcfDbaseTree();

      // public interface functions 

      // function connectToDbase connects to RCF database
      // AND builds a tree. Argument path must point to the
      // top of RCF database directory tree 
      int connectToDbase(std::string path);
      // setMyPath can be used to build a tree which is not
      // a proper RCF database (has no admin).
      int setMyPath(std::string path);
      // function attachToDbase connects to RCF database but it
      // DOES NOT build a tree. The purpose of this function
      // is to enable partial trees which do not begin at the
      // top of RCF databse, which will speed up all operations.
      // function setMyDbasePath creates the tree, starting 
      // from the location specified by argument path. Path
      // should be specified RELATIVE to the database root.
      int attachToDbase(std::string path);
      int setMyDbasePath(std::string path);
      // function setRcfAdmin is functionally equivalent to
      // attachToDbase(), but it accepts an external rcfAdmin.
      // casual users should avoid this function.
      int setRcfAdmin(rcfAdmin *admin);

      // housekeeping functions, use not recommended for
      // casual users.
      int enableDebug(std::string path);
      int enableDebug();
      void matchOptions(rcfDbaseTree * arg);

      std::string getMyDbasePath(bool *valid);
      std::string getMyResolvedPath(bool *valid);
      rcfConfigParser* getMyParser(bool *valid);

      // execute query will execute the query, provided that the
      // tree has been built. Once this function returns CF_OK, query
      // object should be filled with returns.
      int executeQuery(rcfDbaseQuery *query);
      

   private:

       void basicInit();
       int setDebugLog(std::string path);
};

#endif
