#include "InterpolationAlgortihms.h"
InterpolationAlgortihms::InterpolationAlgortihms()
{

}


template<typename T>
T InterpolationAlgortihms::bilinear(
        const float &tx,
        const float &ty,
        const T &c00,
        const T &c10,
        const T &c01,
        const T &c11)
{
#if 1
    T a = c00 * (1.f - tx) + c10 * tx;
    T b = c01 * (1.f - tx) + c11 * tx;
    return a * (1.f - ty) + b * ty;
#else
    return (1 - tx) * (1 - ty) * c00 +
            tx * (1 - ty) * c10 +
            (1.f - tx) * ty * c01 +
            tx * ty * c11;
#endif
}

#ifdef WIN32
#define RANDFLOAT float(rand()) / RAND_MAX
#else
#define RANDFLOAT drand48()
#endif


void InterpolationAlgortihms::interp3(
        std::vector<float> & vOutput,
        const std::vector<float> & gridX,
        const std::vector<float> & gridY,
        const std::vector<float> & gridZ,
        const std::vector<float> & vGrid,
        const std::vector<float> & xq,
        const std::vector<float> & yq,
        const std::vector<float> & zq)
{
    if (xq.size() != yq.size() || xq.size() != zq.size())
    {
        return ;
    }

    float x0, y0, z0, x1, y1, z1;
    int ibx, itx, iby, ity, ibz, itz, im;
    int xSize = gridX.size();
    int ySize = gridY.size();
    int zSize = gridZ.size();

    int sliceDim = xSize* ySize;
    int zOff0;
    int zOff1;
    int yOff0;
    int yOff1;
    float ax0;
    float ay0;
    float az0;
    float ax1;
    float ay1;

    float v000;
    float v001;
    float v010;
    float v011;
    float v100;
    float v101;
    float v110;
    float v111;

    float v00 ;
    float v01 ;
    float v10 ;
    float v11 ;

    float v0;
    float v1;


    for (int iPoint = 0; iPoint < xq.size(); ++iPoint)
    {

        if (xq[iPoint] < gridX[0] || xq[iPoint] > gridX[xSize - 1] ||
                yq[iPoint] < gridY[0] || yq[iPoint] > gridY[ySize - 1] ||
                zq[iPoint] < gridZ[0] || zq[iPoint] > gridZ[zSize - 1])
        {
            vOutput[iPoint] = NAN;
        }

        ibx = 0;
        itx = xSize - 1;
        while (ibx < (itx - 1))
        {
            im = ((ibx + itx) >> 1);
            if (xq[iPoint] <= gridX[im])
            {
                itx = im;
            }
            else
            {
                ibx = im;
            }
        }
        x0 = gridX[ibx];
        x1 = gridX[itx];

        iby = 0;
        ity = ySize - 1;
        while (iby < (ity - 1))
        {
            im = ((iby + ity) >> 1);
            if (yq[iPoint] <= gridY[im])
            {
                ity = im;
            }
            else
            {
                iby = im;
            }
        }
        y0 = gridY[iby];
        y1 = gridY[ity];

        ibz = 0;
        itz = zSize - 1;
        while (ibz < (itz - 1))
        {
            im = ((ibz + itz) >> 1);
            if (zq[iPoint] <= gridZ[im])
            {
                itz = im;
            }
            else
            {
                ibz = im;
            }
        }
        z0 = gridZ[ibz];
        z1 = gridZ[itz];



        zOff0 = sliceDim * ibz;
        zOff1 = zOff0 + sliceDim;
        yOff0 = ySize * ibx;
        yOff1 = yOff0 + ySize;
        ax0 = (xq[iPoint] - x0) / (x1 - x0);
        ay0 = (yq[iPoint] - y0) / (y1 - y0);
        az0 = (zq[iPoint] - z0) / (z1 - z0);
        ax1 = 1.0f - ax0;
        ay1 = 1.0f - ay0;


        v000 = vGrid[zOff0 + yOff0 + iby];
        v001 = vGrid[zOff0 + yOff0 + ity];
        v010 = vGrid[zOff0 + yOff1 + iby];
        v011 = vGrid[zOff0 + yOff1 + ity];
        v100 = vGrid[zOff1 + yOff0 + iby];
        v101 = vGrid[zOff1 + yOff0 + ity];
        v110 = vGrid[zOff1 + yOff1 + iby];
        v111 = vGrid[zOff1 + yOff1 + ity];

        v00 = v000 * ay1 + v001 * ay0;
        v01 = v010 * ay1 + v011 * ay0;
        v10 = v100 * ay1 + v101 * ay0;
        v11 = v110 * ay1 + v111 * ay0;

        v0 = v00 * ax1 + v01 * ax0;
        v1 = v10 * ax1 + v11 * ax0;
        vOutput[iPoint] = v0 * (1.0f - az0) + v1 * az0;

    }










}
