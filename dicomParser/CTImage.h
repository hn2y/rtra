#ifndef CTIMAGE_H
#define CTIMAGE_H

#include <iostream>
#include <vector>

// The DCMTK headers
#ifdef UNICODE
#undef UNICODE
#endif


#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmrt/drtimage.h"


class CTImage
{
public:
    CTImage();

    std::string dicomFilePath;


    std::string PatientID;

    std::string StudyID;

    std::string SeriesNumber;

    std::string Modality;


    Sint32 NumberOfFrames;            // (0028,0008)



    DcmFileFormat ctDcmFileFormat;

    std::vector <Uint16> pixelDataVectorUint16; // either DataUint16 or DataUint32 will be used
    std::vector <Uint32> pixelDataVectorUint32; // either DataUint16 or DataUint32 will be used

    bool IsMultiFrame;


    double sliceZvalue; // if modality is CT, gives slice z value

    /// Maximum value in pixel data
    int maxValue;

    /// Maimimum value in pixel data
    int minValue;

    /// UID
    std::string scanUID;

    /// ImagePatient position
    ///
    Uint16 Rows;
    Uint16 Columns;
    Uint16 PixelRepresentation;
    Uint16 BitsAllocated;
    Uint16 BitsStored;
    Uint16 HighBit;
    Uint16 SamplesPerPixel;
    std::vector<double> ImagePositionPatient;

    /// TODO: CT value X values
    std::vector<float> gridXvalues;

    /// TODO: CT value y values
    std::vector<float> gridYvalues;

    // Patient Position
    std::string PatientPosition;



    // oreintation patient
    double ImageOrientationPatient[6];


    // pixel spacing
    double PixelSpacing[2];

    // Patient CT xOffsetCT
    double xOffsetCT;

    // Patient CT yOffsetCT
    double yOffsetCT;

    bool parseCTDicomfile();
    bool setCTDicomFilePath(const std::string ctFilePath);

    bool savePixelDataToASCIIFile(std::string filePath);

    unsigned long pixelCount;

private :

    OFCondition status;



    bool readPixelData();
};

#endif // CTIMAGE_H
