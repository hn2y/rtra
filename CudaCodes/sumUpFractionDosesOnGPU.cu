
// __device__ double fmod	(	double 	x, double 	y)	
//http://developer.download.nvidia.com/compute/cuda/4_1/rel/toolkit/docs/online/group__CUDA__MATH__DOUBLE.html
//http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH__SINGLE.html#group__CUDA__MATH__SINGLE
#include <cuda.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <iostream>


#include <thrust/scan.h>
#include <thrust/iterator/reverse_iterator.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>


// d_doseIn contains  dose values of point cloud points for different fractions in different trails --> d_doseIn =[dp1,dp2, ..., dpN,...]
// d_doseIn size is (numDoseSets*NumoFDosePtsInEachDoseSet) and  numDoseSets=fractionsNum*NumTrials -->numTrials= numDoseSets/fractionNum
// N =  iTrial*fractionsNum*NumoFDosePtsInEachDoseSet+iFrac*NumoFDosePtsInEachDoseSet+iDosePoint --> where iFrac=0:fractionsNum-1, iTrail = 0:NumTrails-1, iDosePoint=0:NumoFDosePtsInEachDoseSet-1
// d_doseIn[N[ denotes the dose receive by the iDosePoint th of the point cloud in trail iTrial and fraction iFrac


// d_doseOut  contains  dose values of cloud points for different trials after suming up the fraction doses for each trial -->  d_doseOut =[dp1,dp2, ..., dpM,...]
// d_doseOut size is  numTrials*NumoFDosePtsInEachDoseSet  and
// d_doseOut[M] = dose recived by point iDosePoint in trial iTrial
// M =iTrial*NumoFDosePtsInEachDoseSet + iDosePoint


// fractionDoseWeights is of fractionsNum ,  dose points in fraction f will be weighted by fractionDoseWeights[f] factor.
// notice fractionDoseWeight is expected to be defined in host



extern "C" void sumUpFractionDosesOnGPU(float *d_doseOut, const float *d_doseIn, const int fractionsNum, const float *fractionDoseWeights, const int numDoseSets,  const int NumoFDosePtsInEachDoseSet, const int devNum);


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		//fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}


__global__ void sumFracDosesKernel(float *d_doseOut, const float *d_doseIn, const int fractionsNum, const float *d_fractionDoseWeights, const int numDoseSets,  const int NumoFDosePtsInEachDoseSet)

{

    // numDoseSets = fractionsNum*NumTrials

	int id_x = blockIdx.x * blockDim.x + threadIdx.x;
	//     int id_y = blockidx.y * blockDim.y + threadIdx.y ;
	//      int absoulteId = id_y*Nby2 + id_x ;
    if (id_x < NumoFDosePtsInEachDoseSet*numDoseSets)
	{

        float myItem = d_doseIn[id_x];
        int iTrial = id_x / NumoFDosePtsInEachDoseSet/fractionsNum; // iTrial
        int indexInTrial= id_x % (NumoFDosePtsInEachDoseSet*fractionsNum); // this number is less than fractionsNum*NumoFDosePtsInEachDoseSet
        int iFrac = indexInTrial/NumoFDosePtsInEachDoseSet;// iFrac
        float  weightedDose=d_fractionDoseWeights[iFrac]*myItem;
        int iDosePoint = id_x % NumoFDosePtsInEachDoseSet; //iDosePoint
        atomicAdd(&d_doseOut[iTrial*NumoFDosePtsInEachDoseSet + iDosePoint], weightedDose); // Atomic tends to be slow  for large number of points but since we have several different bins here the ratio between data points ( NumPtsInEachSet*numDoseSets) and bins (EachDoseSetBinNumbers*numDoseSets) is going to be in order of 100  --> for an OARwith 50000 points and EachDoseSetBinNumbers=500 we get the ratio is 100
	}
}





void sumUpFractionDosesOnGPU(float *d_doseOut, const float *d_doseIn, const int fractionsNum, const float *fractionDoseWeights, const int numDoseSets,  const int NumoFDosePtsInEachDoseSet, const int devNum)
{
	if (numDoseSets == 0)
	{
		std::cout << "dvhCalcGPU : numDoseSets is zero !!!!!!!!!!!" << std::endl;
		return;
	}
    gpuErrchk(cudaSetDevice(devNum));
	//  number of kenel to lanuch  --> np
	int n_threads_per_block = 1 << 10;  // TODO : 1024 threads per block // note: optimal value is harware dependent
    int n_blocks = ceil((double)NumoFDosePtsInEachDoseSet*(double)numDoseSets / (double)n_threads_per_block);
    if (false)
    {
    std::cout << "number blocks for sumUpFractionDosesOnGPU= " << n_blocks << std::endl;
    std::cout << "threads per blocks in sumUpFractionDosesOnGPU = " << n_threads_per_block << std::endl;
    }
//    int * d_volIndoseBins;
//    gpuErrchk(cudaMalloc((void**)&d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets*sizeof(int)));
//    gpuErrchk(cudaMemset(d_volIndoseBins, 0, sizeof(int)*EachDoseSetBinNumbers*numDoseSets));
    float * d_fractionDoseWeights;
    gpuErrchk(cudaMalloc((void**)&d_fractionDoseWeights, fractionsNum*sizeof(float)));
    //gpuErrchk(cudaMemset(d_fractionDoseWeights, 0, sizeof(float)*fractionsNum));
    gpuErrchk(cudaMemcpy( d_fractionDoseWeights,fractionDoseWeights, fractionsNum*sizeof(float), cudaMemcpyHostToDevice));

    // wrap raw pointer with a device_ptr
    //thrust::device_ptr<float> d_dvhsDevicePtr = thrust::device_pointer_cast(d_dvhsRawPointer);

    //thrust::device_vector<float> d_dvhs(d_dvhsDevicePtr, d_dvhsDevicePtr + EachDoseSetBinNumbers*numDoseSets);

    sumFracDosesKernel << <n_blocks, n_threads_per_block >> >(d_doseOut, d_doseIn, fractionsNum, d_fractionDoseWeights, numDoseSets, NumoFDosePtsInEachDoseSet);
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());

    gpuErrchk(cudaFree(d_fractionDoseWeights));


}
