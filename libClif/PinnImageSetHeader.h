/**
 *  @file    PinnImageSetHeader.h
 *  @author  Hamidreza Nourzadeh
 *  @date    10/09/2016
 *  @version 1.0
 *
 *  @brief TO read Pinnacle's ImageSet Header files
 *  @section DESCRIPTION
 *
 *
 */

#ifndef PinnImageSetHeader_H
#define PinnImageSetHeader_H
#include <string>

// This header is partly made based on the cfImage.hpp in RCF library
namespace PinnImageEnums {
enum ScannerType { FBCT, CBCT, UNKNOWN };
enum ScanOptions { HELIX, SURVIEW, UNKNOWN_OPTION };
}


#include <iostream>
#include "utilities.h"
#include "read_clif.h"
#include "jvsDefines.h"

// HN
typedef struct{
    int byte_order; // 0 is LittleEndian | 1 is BigEndian
    int x_dim;
    int y_dim;
    int z_dim;
    int bytes_pix;
    float x_pixdim;
    float y_pixdim;
    float z_pixdim;
    float x_start;
    float y_start;
    float z_start;
    float x_start_dicom; // HN
    float y_start_dicom; // HN
    float X_offset;     // HN
    float Y_offset;     //HN
    std::string patient_id;
    std::string db_name;
    std::string patient_position;
    std::string  Version;       ///< Dicom version
    std::string date;        ///< Date the image was aquired

//    std::string scannerModel;    ///< Model name of the scanner

    // Some extra information
    PinnImageEnums::ScannerType scannerType;     ///< Type of scanner that aquired this image
    PinnImageEnums::ScanOptions scanOptions;    ///< Mode used for scan
    // bool isImageUniform; //  We assumes that imageSet is uniform
    bool isHeaderReady;
    //    bool isImageReady;  // checks if allocation failed or something went wrong in loading the imageSet
    // int orig_hfs; // no need
    //bool isBigEndian; // is imagedata is saved in BigEndian
    // int *SliceNumber; // Don't really need this
    //float *TablePosition; //  since isImageUniform is assummed to be true we do not use this
    // void *bin_img;    // no need to put binary image here
    // std::string checksum;
    //cfMatrix4<float> rotation; //TODO: transformation of the imageSet

}ImageHeader_type;




int readPinnacleImageSetHeader(std::string imageHeaderFilePath, ImageHeader_type &imageHeader);


#endif // PinnImageSetHeader
