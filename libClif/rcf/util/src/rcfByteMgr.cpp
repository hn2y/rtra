
#include <stdio.h>
#include "rcfByteMgr.hpp"

#define RCFBMGR_LITTLE_ENDIAN 1
#define RCFBMGR_BIG_ENDIAN 2
#define RCFBMGR_UNKNOWN_ENDIAN -1 


uint16 rcfByteMgr::reverseByteOrder(uint16 xold)
{
   uint16 xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[1];
   pn[1] = po[0];
   return(xnew);
}
uint32 rcfByteMgr::reverseByteOrder(uint32 xold)
{
   uint32 xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[3];
   pn[1] = po[2];
   pn[2] = po[1];
   pn[3] = po[0];
   return(xnew);
}

int rcfByteMgr::reverseByteOrder(int xold)
{
   int xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[3];
   pn[1] = po[2];
   pn[2] = po[1];
   pn[3] = po[0];
   return(xnew);
}

uint64 rcfByteMgr::reverseByteOrder(uint64 xold)
{
   uint32 xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[7];
   pn[1] = po[6];
   pn[2] = po[5];
   pn[3] = po[4];
   pn[4] = po[3];
   pn[5] = po[2];
   pn[6] = po[1];
   pn[7] = po[0];
   return(xnew);
}
float32 rcfByteMgr::reverseByteOrder(float32 xold)
{
   float32 xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[3];
   pn[1] = po[2];
   pn[2] = po[1];
   pn[3] = po[0];
   return(xnew);
}
float64 rcfByteMgr::reverseByteOrder(float64 xold)
{
   float64 xnew;
   char *pn   = (char *) &xnew;
   char *po   = (char *) &xold;
   pn[0] = po[7];
   pn[1] = po[6];
   pn[2] = po[5];
   pn[3] = po[4];
   pn[4] = po[3];
   pn[5] = po[2];
   pn[6] = po[1];
   pn[7] = po[0];
   return(xnew);
}
int rcfByteMgr::check_byte_order()
{
  /* Determine the byte order on this machine */
  float ftest=1.0; /* assign a float to 1.0 */
  char *pf = (char *) &ftest;
  if(pf[0] == 0 && pf[3] != 0)
  {
    //printf("\n INTEL / ALPHA,LINUX \n");
    return(RCFBMGR_LITTLE_ENDIAN);
  }else if(pf[0] != 0 && pf[3] == 0)
  {
    //printf("\n OTHER (SGI,SUN-SOLARIS)\n ");
    return(RCFBMGR_BIG_ENDIAN);
  }
  else
  {
        fprintf(stderr,"\n ERROR: check_byte_order:: indeterminate byte order");
        fprintf(stderr,"\n \t %x %x %x %x", pf[0],pf[1],pf[2],pf[3]);
        return(RCFBMGR_UNKNOWN_ENDIAN);
  }
}

bool rcfByteMgr::isOrderBigEndian()
{
   if(RCFBMGR_BIG_ENDIAN == check_byte_order())
   {
       return true;
   }
   else
   {
       return false;
   }
}
bool rcfByteMgr::isOrderLittleEndian()
{
    if(RCFBMGR_LITTLE_ENDIAN == check_byte_order())
    {
        return true;
    }
    else
    {
        return false;
    }
}

