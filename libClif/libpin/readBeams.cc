/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* readBeams.cc
   routines to read patient beam information into the patient beam data structure
   Created: 6/9/98: JVS
            6/10/98: Able to read in apertures from pinnacle
   Modification History:
   9/3/98: JVS: aperture vertices read in as floats, then converted to ints for pb routines
   9/8/98: JVS: add convert_wedge_name to convert wedge name for name for file (very crude)
   9/9/98: JVS: set all apertures use_for_dose_computations=TRUE
   9/17/98:JVS: jaw_x1,jaw_y1: Had to set to -1.0 * pinnacle_value to correspond with pb 
                coordinate system (Not exactly sure if this should be x2 or x1, when use x1 
                get no dose computed)
   10/14/98: JVS: Modified aperture code wo will read in multiple contours for an aperture
                  that has multiple contours. 
   10/23/98: JVS: Eliminate reading in of object_code and use_for_dose for aperture
   10/28/98: JVS: Add id_num to the BEAM
   10/29/98: JVS: Improving Output Error Messages
   12/09/98: JVS: Remove version number from machine name
   20/05/99: pjk: Add MLCs
   Jun3 2, 1999: JVS: Improve I/O information regarding reading in trials.
   Oct 20, 1999: JVS: Add compensator.h
   Oct 21, 1999: JVS: Add .valid to aperture, let "As Is", "As Is" allow to pass (but invalid aperture)
   Oct 27, JVS: Add #ifdef's around printf's
   Feb 12, PJK: Add read MUs
   Jun 7, 2000: JVS: Change to readBeams... will now use the new ClifRead routines so
                     we do not have to worry about the order of things in Pinnacle
   Jun 16, 2000: JVS: Update wedge stuff to work with Pinnacle 4.2g as well
   Jun 21, 2000: JVS: Convert wedge name changed to remove spaces from wedge name ONLY
   Jun 28, 2000: JVS/PJK: Copied address of MLC contour to MLC structure
   Aug 31, 2000: JVS: Add in Applicator for electrons
   Jan 12, 2001: JVS: Add Comment about Y2 and Y1 reversed from Pinnacle names
   Feb 2, 2001: JVS: Fix a leak
   Nov 28, 2001: JVS: Implement step-and-shoot IMRT with Pinnacle V 6
   April 12, 2002: JVS: Get rid of false error messages when use Static beams with Pinnacle V6
                        Had to do with looking for sub-item in CPManager...
   April 1, 2003: JVS: For step & shoot IMRT, correct couchStop angle...produced false error message.
   July 16, 2003: JVS: Modifications to read store level (and read dose grid at when run in
                       debug level)! so will read vcuImrtType ... Can now add ability
                       to store the dml file location and check-sum
   Feb 13, 2004: JVS:  Have failure to read vcuImrtType be only a warning
   April 29, 2006: JVS: Pin7.9: Check that Bolus object exists before trying to read it in
   March 31, 2010: JVS: Add read in of SetBeamType or BeamType from Pinnacle
   May 25, 2016: JVS: Make readPbeamModifiers (which reads in apertures) conditional on mlc_defined.
                      If mlc is defined, then apertures are not used, so do not read them in or try
                      to convert them to non-reentrant apertures (compute_mcvmc_beams.cc)
   8/12/2016: JVS: Add ability for regular arc


   Future Additions:

 
   ***Note: Block's must be EXPOSE or BLOCK, AS IS is not a valid choice for
            this code
   Limitations:
   * Boluses are NOT supported


*/
//#define DEBUG_CP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> // for fabs 
#include <ctype.h> // for isspace
// #include "iostream"
#include "string_util.h"
#include "utilities.h"
#include "typedefs.h"
#include "defined_values.h"
#include "apertures.h"
#include "beams.h"
#include "read_clif.h"
#include "modifiers.h"
#include "lib.h" // for definition of TRUE 
#include "myPinnacle.h"
#include "readPinnacle.h" // prototypes for all routines
// using namespace std;
/* *********************************************************************************** */
/* Prototypes for local routines */
int getPinnacleMLCInfo(clifObjectType *Beam, beam_type *pbeam);
int getPinnacleWedgeInfo(clifObjectType *Beam, beam_type *pbeam);
/* *********************************************************************************** */

/* *********************************************************************************** */
int readPinnaclePatientBeams(char *pbeamFnameStem, char *TrialName, int *nBeams, beam_type **pbeams)
{
    // reads in patient beam information from pinnacle
   // open dose region file
   // append .Trial to the name to read in the .Trial information
   char pbeamFilename[MAX_STR_LEN];
   strcpy(pbeamFilename,pbeamFnameStem);  // put first part on name
   strcat(pbeamFilename,".Trial");          // append .Trial to the name  
#ifdef DEBUG
   printf("\n Reading In Patient Beams from file %s",pbeamFilename);
   printf("\n Looking for Trial %s", TrialName);
#endif
   /* Open The File */
   FILE *istrm = fopen(pbeamFilename,"r");
   if(istrm==NULL)
   {
      printf("\n ERROR: opening patient beam file %s", pbeamFilename);return(FAIL);
   }
   clifObjectType *TrialList = new(clifObjectType);
   if(TrialList == NULL)
   {
      printf("\n ERROR: Allocating Memory for clifObjectType\n"); return(FAIL);
   }
   if( readSpecificClif(istrm, TrialList, "Trial", TrialName) != OK)
   {
      printf("\n ERROR: Reading clif from file %s\n", pbeamFilename); return(FAIL);
   }
   fclose(istrm); // close the input stream
   /* determine Pinnacle Version */
#ifdef DEBUG
   printf("\n Completed readSpecificClif");
   // dumpClifStructure(TrialList);
#endif
   char PinnVersion[MAX_STR_LEN];
   if(readStringClifStructure(TrialList, "ObjectVersion.WriteVersion",PinnVersion)!= OK )
   {
      printf("\n ERROR: Getting Pinnacle Version from TrialList"); return(FAIL);
   }
#ifdef DEBUG
   printf("\n Pinnacle Version is %s", PinnVersion);
   // For Debugging
   printf("\n Dump of DoseGrid structure");
   {
     clifObjectType *testObject;
     if(getClifAddress(TrialList,"DoseGrid", &testObject)!= OK)
     {
           printf("\n ERROR: Getting Address of test Object"); return(FAIL);
     }
     dumpClifStructure(testObject);   
   }
   printf("\n Checking for Store String");
#endif
#ifdef READ_STORE //  6/25/2015: JVS: Comment out since not using this store anymore
   // July 15, 2003: Check for  TrialList.Current.Store.StringAt.VCUImrtType;
   {
      if( checkForClifObject(TrialList,"Store") == OK ){
        // printf("\n Store found");
	clifObjectType *storeObject;
        if(getClifAddress(TrialList,"Store", &storeObject)!= OK){
           printf("\n ERROR: Getting Address of Store Object"); return(FAIL);
        }
        // printf("\n Contents of storeObject\n");
        // dumpClifStructure(storeObject);  
           char vcuImrtType[MAX_STR_LEN];
           if(readStringClifStructure(storeObject, "At.VCUImrtType.String",vcuImrtType)!= OK ){
	     printf("\n WARNING: Cannot Read VCUImrtType from storeObject");
             printf("\n dump of Structure\n");
             dumpClifStructure(storeObject);
             // return(FAIL);
	   } else {
              printf("\n Found vcuImrtType = %s", vcuImrtType);
	   }
      }
   }
#endif
   // 2/15/2008: JVS: Check for patient representation specifing different imageName
   /* Not Completed:  Need to get the associations from the Patient file, or
      directly from Pinnacle

   if( OK == checkForClifObject(TrialList,"PatientRepresentation") ) 
   {
     // Version 7.9 and higher
     char tmpPtVolumeName[MAX_STR_LEN];
     if(OK != readStringClifStructure(TrialList, "PatientRepresentation.PatientVolumeName",tmpPtVolumeName)  ){
       printf("\n ERROR: Cannot read PatientRepresentation.PatientVolumeName"); return(FAIL);
     }
     // Translate the volume name into an image name..The association is in the Patient file.
     
   }
   */
#ifdef DEBUG
   printf("\n Pinnacle Version is %s", PinnVersion);
   printf("\n\tReading Data for Trial %s", TrialName);
#endif
   /* Process all of the Beams in the BeamList */
   clifObjectType *BeamList;
   if(getClifAddress(TrialList,"BeamList", &BeamList)!= OK)
   {
      printf("\n ERROR: Getting Address for BeamList"); return(FAIL);
   } 
   if(checkIfObjectList(BeamList) != OK)
   {
      printf("\n ERROR: Invalid Object List"); return(FAIL);
   }
   *nBeams = BeamList->nObjects;
   // Allocate Memory For The Beams
   beam_type *plbeam=NULL; // local copy of patient beam
   plbeam = (beam_type *) calloc(*nBeams, sizeof(beam_type)); // Allocate memory for ALL beams
   if(plbeam == NULL)
   {
      printf("\n ERROR: allocating memory for %d beams",*nBeams);return(FAIL);
   }
   *pbeams = plbeam;
   /* Load In The Data For Each Beam */
   for(int iBeam=0; iBeam < *nBeams; iBeam++)
   {
       // printf("\n Processing Beam %d / %d", iBeam, *nBeams);
       strcpy(plbeam[iBeam].fname, pbeamFilename); // copy in the file name
       if(readPinnaclePbeam(&BeamList->Object[iBeam], &plbeam[iBeam])!=OK)
       {
          printf("\n ERROR: Reading Beam %d", iBeam+1);return(FAIL);
       }
       plbeam[iBeam].id_num = iBeam; // Add ID number for this BEAM 
       
   }
   /* Read in the Isocenters */
   if(readPinnacleIsocenters(pbeamFnameStem, *nBeams, *pbeams) != OK)
   {
      printf("\n ERROR: reading isocenters"); return(FAIL);
   }

#ifdef DEBUG
   printf("\n\t%d beams read in from %s for Trial %s",*nBeams, pbeamFilename, TrialName);
#endif

   // free the structure
   if(freeClifStructure(TrialList) != OK)
   {
      printf("\n ERROR: Freeing memory for clif object");
   }
   delete(TrialList);

   return(OK);
}
/* ****************************************************************************************** */
int readPinnacleIsocenters(char *pbeamFilenameStem, int nBeams, beam_type *pbeam)
{
   // read the isocenters from the Points file
   // printf("\n Reading Isocenters for %d Beams\n",nBeams);
   /* Read all of the Pinnacle Points */
   char pbeamFilename[MAX_STR_LEN];
   strcpy(pbeamFilename,pbeamFilenameStem);  // put first part on name
   strcat(pbeamFilename,".Points");         // append .Points to the name  

   FILE *istrm = fopen(pbeamFilename,"r");     // open the file
   if(istrm==NULL)
   {
      printf("\n ERROR: opening patient beam file %s", pbeamFilename);return(FAIL);
   }
   clifObjectType *PointList  = new(clifObjectType);
   if(PointList == NULL)
   {
      printf("\n ERROR: Allocating Memory for clifObjectType\n"); return(FAIL);
   }
#ifdef DEBUG
   printf("\n Reading PointList File %s", pbeamFilename);
#endif
   if( readClifFile(istrm, PointList,"PointList") != OK)
   {
      printf("\n ERROR: Reading clif from file %s\n", pbeamFilename); return(FAIL);
   }
   
   for(int ibm=0; ibm < nBeams; ibm++)
   {
      // read the isocenters for each beam
#ifdef DEBUG
      printf("\n Reading Isocenter %s for beam # %d (%s)\n", pbeam[ibm].IsocenterName, ibm+1, pbeam[ibm].name);
#endif
      clifObjectType *CurrentPoint = NULL;
      if(getClifAddress(PointList, pbeam[ibm].IsocenterName, &CurrentPoint)!= OK)
      {
         printf("\n ERROR: Getting Address for PointList"); return(FAIL);
      }
      if(    readFloatClifStructure(CurrentPoint, "XCoord",&pbeam[ibm].initial_isoctr_in_is.x )!= OK 
          || readFloatClifStructure(CurrentPoint, "YCoord",&pbeam[ibm].initial_isoctr_in_is.y )!= OK 
          || readFloatClifStructure(CurrentPoint, "ZCoord",&pbeam[ibm].initial_isoctr_in_is.z )!= OK 
        )
     {
        printf("\n ERROR: Reading Isocenter Coordinate for beam %d", ibm+1);
     }      
   }
   // free the structure
   if(freeClifStructure(PointList) != OK)
   {
      printf("\n ERROR: Freeing memory for clif object");
   }
   delete(PointList);
   return(OK);
}
/* ******************************************************************************* */
int readPinnaclePbeam(clifObjectType *Beam, beam_type *pbeam)
{  /* reads in components of a single beam from pinnacle */
    // initialize necessary structure elements 
   pbeam->n_apertures = 0;   
   pbeam->mlc_defined = 0;
   float CouchStop=0; // temp local variable for error checking

#ifdef DEBUG
   printf("\n .................readPinnacleBeam.....................\n");
#endif
   // Things common to all versions of Pinnacle (that I know of)
   if(    readStringClifStructure(Beam, "Name",pbeam->name)!= OK 
       || readStringClifStructure(Beam, "SetBeamType",pbeam->BeamType)!= OK 
       || readStringClifStructure(Beam, "IsocenterName",pbeam->IsocenterName)!= OK 
       || readStringClifStructure(Beam, "MachineNameAndVersion",pbeam->beam_data_id.machine_name)!= OK 
       || readStringClifStructure(Beam, "Modality",pbeam->beam_data_id.modality)!= OK 
       || readStringClifStructure(Beam, "MachineEnergyName",pbeam->beam_data_id.energy)!= OK 
       || readIntClifStructure   (Beam, "UseMLC",           &pbeam->mlc_defined) != OK
       || readFloatClifStructure (Beam, "Weight", &pbeam->weight) != OK ) 
   {
      printf("\n ERROR: Reading Beam Parameters from CLIF"); return(FAIL);
   }
   // Check for existance of CPManager....
   // Things that differ between versions < 6 and > 6 
   /* April 12, 2002: JVS: Re-order things in finding CPManager 
      Alternatively, could check Pinnacle Version, look for <6 or > 6, then look
      at SetBeamType and based on if field is "Static",  or ??? decide what to do
   */
  
#ifdef DEBUG_CP
   printf("\n Checking for CPManager Object");
#endif
   if( checkForClifObject(Beam,"CPManager") == OK )
   {
#ifdef DEBUG_CP
      printf("\n Pinnacle CPManager Objects Found");
#endif
      clifObjectType *cpManager, *cpManagerTmp;
      if(getClifAddress(Beam,"CPManager", &cpManagerTmp)!= OK)
      {
         printf("\n ERROR: Getting Address of cpManager"); return(FAIL);
      }
      if( checkForClifObject(cpManagerTmp,"CPManagerObject") == OK)
      {
         //printf("\n Pinnacle Version 6.0i style CPManager Objects");
         if(getClifAddress(cpManagerTmp,"CPManagerObject", &cpManager)!= OK)
         {
            printf("\n ERROR: Getting Address of cpManager"); return(FAIL);
         }
      }
      else
      {
        cpManager = cpManagerTmp;
      }
     // Determine Number of Control Points....
     int NumberOfControlPoints=0;
     int isGantryStartStopLocked = -1;
     int isCouchStartStopLocked = -1;
     int isCollimatorStartStopLocked = -1;

     if(     readIntClifStructure (cpManager, "NumberOfControlPoints", &NumberOfControlPoints ) != OK
	  || readIntClifStructure (cpManager, "IsGantryStartStopLocked", &isGantryStartStopLocked ) != OK
	  || readIntClifStructure (cpManager, "IsCouchStartStopLocked", &isCouchStartStopLocked ) != OK
	  || readIntClifStructure (cpManager, "IsCollimatorStartStopLocked", &isCollimatorStartStopLocked ) != OK ) {
       printf ("\n ERROR: Reading from CPManager"); return(FAIL);
     }
     if(NumberOfControlPoints != 1) {
       pbeam->imrtMethod = stepAndShootIMRT; // by setting stepAndShoot IMRT, will fail later unless set otherwise 
     }
     bool isVMAT=false;
     if(isGantryStartStopLocked != 1 ) {
       printf("\n Arc beam detected -----"); 
       isVMAT=true;
       // pbeam->imrtMethod = dynamicArc;
       if(strcmp(pbeam->BeamType,"Dynamic Arc") && strcmp(pbeam->BeamType,"Conformal Arc") && strcmp(pbeam->BeamType,"Arc")){
	 printf("\n ERROR: %s beam type detected but isGantryStartStopLocked is not set",pbeam->BeamType); return(FAIL);
       }
     }
     if( isCouchStartStopLocked != 1 ||
         isCollimatorStartStopLocked != 1) {
       printf("\n ERROR: Couch, or Collimator is moving...time to write code"); return(FAIL);
     }
     printf ("\n Beam %s: %s  mode detected with %d NumberOfControlPoints", pbeam->name, pbeam->BeamType, NumberOfControlPoints);
     clifObjectType *cpSegment;
     if(getClifAddress(cpManager,"ControlPointList.#0", &cpSegment)!= OK)
     {
       printf("\n ERROR: Getting Address of cpSegment"); return(FAIL);
     }
     // Read First Segment, Check others.... 
     if(  readFloatClifStructure (cpSegment, "Gantry", &pbeam->gantry) != OK
       || readFloatClifStructure (cpSegment, "Couch", &pbeam->turntable_angle) != OK
       || readFloatClifStructure (cpSegment, "Collimator", &pbeam->coll) != OK
       || readFloatClifStructure (cpSegment, "LeftJawPosition",  &pbeam->jaw_x1) != OK 
       || readFloatClifStructure (cpSegment, "RightJawPosition", &pbeam->jaw_x2) != OK 
       || readFloatClifStructure (cpSegment, "TopJawPosition",   &pbeam->jaw_y1) != OK // Pinnacle Y2
       || readFloatClifStructure (cpSegment, "BottomJawPosition",&pbeam->jaw_y2) != OK  // Pinnacle Y1
       || getPinnacleWedgeInfo(cpSegment, pbeam) != OK)
       {
         printf("\n ERROR: Reading Beam Parameters from CLIF--version > 6.0"); return(FAIL);
       }
     // if locked, set angle to not be changed...
      if(isCouchStartStopLocked == 1) CouchStop = pbeam->turntable_angle;
      if(1==isGantryStartStopLocked) pbeam->arc = 0.0f;
      else pbeam->arc = pbeam->gantry;
      clifObjectType *ModifierList;
      if(getClifAddress(cpSegment,"ModifierList", &ModifierList)!= OK) {
         printf("\n ERROR: Getting Address of ModifierList "); return(FAIL);
      }
      if( pbeam->mlc_defined  ) { // could trap for mlc_defined..
#ifdef DEBUG
         printf("\n Reading MLC info\n");
#endif
         if( getPinnacleMLCInfo(cpSegment, pbeam) != OK ){
            printf("\n ERROR: Reading Beam Parameters from CLIF"); return(FAIL);
         }
      } else {
	if( readPinnaclePbeamModifiers(ModifierList,pbeam)!= OK ){
	  printf("\n ERROR: reading in patient modifiers (apertures)"); return(FAIL);
	}
      }
      // Check other segments 
      for(int iSegment=1; iSegment < NumberOfControlPoints; iSegment++)
      {
	char segString[MAX_STR_LEN];
        sprintf(segString,"ControlPointList.#%d", iSegment);
        if(getClifAddress(cpManager,segString, &cpSegment)!= OK)
        {
          printf("\n ERROR: Getting Address of cpSegment %s",segString); return(FAIL);
        }
        float gantry, turntable_angle, coll, jaw_x1, jaw_x2, jaw_y1, jaw_y2;
        if(  readFloatClifStructure (cpSegment, "Gantry", &gantry) != OK
          || readFloatClifStructure (cpSegment, "Couch", &turntable_angle) != OK
          || readFloatClifStructure (cpSegment, "Collimator", &coll) != OK
          || readFloatClifStructure (cpSegment, "LeftJawPosition",  &jaw_x1) != OK 
          || readFloatClifStructure (cpSegment, "RightJawPosition", &jaw_x2) != OK 
          || readFloatClifStructure (cpSegment, "TopJawPosition",   &jaw_y1) != OK // Pinnacle Y2
          || readFloatClifStructure (cpSegment, "BottomJawPosition",&jaw_y2) != OK ) // Pinnacle Y1
        {
            printf("\n ERROR: Reading Beam Parameters from CLIF--version > 6.0"); return(FAIL);
        }
	if (    (gantry != pbeam->gantry && !isVMAT)
             || turntable_angle != pbeam->turntable_angle 
             || coll != pbeam->coll
             || jaw_x1 != pbeam->jaw_x1
             || jaw_x2 != pbeam->jaw_x2
             || jaw_y1 != pbeam->jaw_y1
	     || jaw_y2 != pbeam->jaw_y2 ){
	  printf("\n ERROR: gantry, couch, collimator, or jaws are not static during CP"); return(FAIL);
	}
	if(isVMAT) pbeam->arc = pbeam->gantry - gantry; // compute the current arc length
      }
   } else {
#ifdef DEBUG_CP
     printf("\n No CPManager Objects Found");
#endif
      if( readFloatClifStructure (Beam, "Gantry", &pbeam->gantry) != OK
       || readFloatClifStructure (Beam, "GantryStop", &pbeam->arc) != OK
       || readFloatClifStructure (Beam, "Couch", &pbeam->turntable_angle) != OK
       || readFloatClifStructure (Beam, "CouchStop", &CouchStop) != OK
       || readFloatClifStructure (Beam, "Collimator", &pbeam->coll) != OK
       || readFloatClifStructure (Beam, "LeftJawPosition",  &pbeam->jaw_x1) != OK 
       || readFloatClifStructure (Beam, "RightJawPosition", &pbeam->jaw_x2) != OK 
       || readFloatClifStructure (Beam, "TopJawPosition",   &pbeam->jaw_y1) != OK // Pinnacle Y2
       || readFloatClifStructure (Beam, "BottomJawPosition",&pbeam->jaw_y2) != OK  // Pinnacle Y1
       || getPinnacleWedgeInfo(Beam, pbeam) != OK)
      {
         printf("\n ERROR: Reading Beam Parameters from CLIF--version < 6"); return(FAIL);
      }
      clifObjectType *ModifierList;
      if(getClifAddress(Beam,"ModifierList", &ModifierList)!= OK)
      {
         printf("\n ERROR: Getting Address of ModifierList "); return(FAIL);
      }
      if( pbeam->mlc_defined  ) {
#ifdef DEBUG
         printf("\n Reading MLC info\n");
#endif
         if( getPinnacleMLCInfo(Beam, pbeam) != OK ){
            printf("\n ERROR: Reading Beam Parameters from CLIF"); return(FAIL);
         }
      } else {
	if( readPinnaclePbeamModifiers(ModifierList,pbeam)!= OK ){
	  printf("\n ERROR: reading in patient modifiers (apertures)"); return(FAIL);
	}
      }
   }
#ifdef DEBUG_CP
   printf ("\n ................ Done with CPManager...Pinnacle Version Stuff....\n");          

   printf("\n Modality = %s", pbeam->beam_data_id.modality);
#endif
   /* Only read MLC for photon beams...since Pinnacle Not (4.2g) Write MLC data for electrons */
   //   if(strcmp(pbeam->beam_data_id.modality, "Photons") ==0 )  // could trap for mlc_defined...
   pbeam->beam_data_id.applicator[0] = 0; // Set initial string to NULL so can use this to see if applicator exists or not.
   if( strcmp(pbeam->beam_data_id.modality, "Electrons") == 0 &&
       readStringClifStructure(Beam,"ElectronApplicatorName", pbeam->beam_data_id.applicator) != OK )
   {
      printf("\n ERROR: Reading Electron Applicator\n"); return(FAIL);
   }
   /* Fix-ups to the data -----Use for All versions of Pinnacle....*/
   // remove version number from the machine   
   {
      unsigned i = 0;
      unsigned strLen = (unsigned) (strlen( pbeam->beam_data_id.machine_name));
      if(strLen > MAX_OBJ_NAME_LEN)
      {
        printf("\n ERROR: Object exceeds maximum length");
        printf("\n\t Object Name = %s", pbeam->beam_data_id.machine_name);
        printf("\n\t strLen = %u, MAX_OBJ_NAME_LEN = %d",strLen,MAX_OBJ_NAME_LEN); 
        return(FAIL);
      }
      while(  (i < strlen( pbeam->beam_data_id.machine_name)) && 
               i < strLen &&
               pbeam->beam_data_id.machine_name[i] != ':')i++;
      pbeam->beam_data_id.machine_name[i]='\0'; // terminate the string after at : 
   }
   pbeam->arc-=pbeam->gantry; // arc length // correct to arc, since read gantry stop...
   if(pbeam->turntable_angle != CouchStop)
   {
      printf("\n ERROR: turntable start angle (%.2f) != turntable stop angle (%.2f) for beam %s", pbeam->turntable_angle, CouchStop, pbeam->name);
   }
#ifdef DEBUG
   printf("\n\t **** ASSIGNING COUCH COORDINATE TO 0,0,0 ****");
   printf("\n\t **** Correct in future **** ");
#endif
   pbeam->couch.x = pbeam->couch.y = pbeam->couch.z = 0.0;
   // pencil beam routines expect jaw positions to be - on opposing side 
   pbeam->jaw_x1*=-1.0;
   pbeam->jaw_y1*=-1.0;

   /* End of Fixups */
#ifdef DEBUG_READ_BEAMS
   printf("\n Reading In Beam Modifiers");
#endif
   /* ------------------- End of Fix-ups --------------------------------- */
   // next, read in the patient modifiers: blocks, compenstators, etc 
   // Pinnacle considers apertures BeamModifiers 
   if( readPinnacleTrayParameters(Beam,pbeam) != OK         ) // Tray Parameters
   {
      printf("\n ERROR: reading in patient modifiers (apertures)"); return(FAIL);
   }
   

   if(OK == checkForClifObject(Beam,"Bolus") ) // April 29, 2006: JVS: Only read in if bolus object exists
   {
      // read in  Bolus 
      clifObjectType *Bolus;
      if(getClifAddress(Beam,"Bolus", &Bolus)!= OK)
      {
         printf("\n ERROR: Getting Address"); return(FAIL);
      }
      if(readPinnacleBolus(Bolus,pbeam) != OK)
      {
         printf("\n ERROR: reading in patient bolus"); return(FAIL);
      }
   } else {
      sprintf(pbeam->bolus_name,"No bolus");
      pbeam->num_boluses = 0;
   }
   // read in compensator
   // read in  Bolus 
   clifObjectType *Compensator;
   if(getClifAddress(Beam,"Compensator", &Compensator)!= OK)
   {
      printf("\n ERROR: Getting Address"); return(FAIL);
   }
   if(readPinnacleCompensator(Compensator, pbeam) != OK)
   {
      printf("\n ERROR: reading in patient compensator"); return(FAIL);
   }   
   // Get the ID number of the dose file
   if( readPinnacleDosefileID( Beam, pbeam) != OK)
   {
      printf("\n ERROR: reading in dose file number"); return(FAIL);
   }

   return(OK);
}
/* ************************************************************************************* */
int readPinnacleDosefileID(clifObjectType *Beam, beam_type *pbeam)
{
   char DoseVolume[MAX_STR_LEN];
   if(    readStringClifStructure(Beam, "DoseVolume", DoseVolume) != OK 
       || readPinnacleXDRId(DoseVolume, &pbeam->dose_grid_id )  != OK ) 
   {
      printf("\n ERROR: Determining DoseVolume ID"); return(FAIL);
   }
   return(OK);
}
/* ************************************************************************************* */
int readPinnacleXDRId(char *iString, int *Value)
{
   int sLen = (int) (strlen(iString));
   while( isspace( iString[sLen-1] ) || iString[sLen-1] == '\\' ) 
   {
      sLen--; // remove \ from end of string 
      iString[sLen] = 0; // Null Terminate the String
   }

   if(strncmp(iString,"\\XDR:",5) != 0)   // check for binary file specification, first 4 char 
   {
      printf("\n ERROR: %s is invalid specifier for a XDR Name",iString);
      return(FAIL);
   }
   if(sscanf(iString+5,"%d",Value) != 1)
   {
      eprintf("\n ERROR: Cannot get specifier  number from %s", iString);
      return(FAIL);
   }   
   return(OK);
}
/* ********************************************************************************* */
int readPinnacleBolus(clifObjectType *Bolus, beam_type *pbeam)
{   // check that no bolus exists (since not currently supported)
   if( readStringClifStructure(Bolus, "Type",pbeam->bolus_name) != OK )
   {
      printf("\n ERROR: Determining Bolus Type"); return(FAIL);
   }
   if( strcmp(pbeam->bolus_name,"No bolus") )
   {
      eprintf("\n ERROR: Bolus Type %s Currently NOT supported",pbeam->bolus_name);
      return(FAIL);
   }
   pbeam->num_boluses = 0;
   return(OK);
}
/* ************************************************************************************ */
int readPinnacleContourList(clifObjectType *ContourList, int *nContours, TwoDContourType **contour)
{  /* reads in a list of contours from Pinnacle (for example a contour of a aperture) */
   // get to the level of the contour
#ifdef DEBUG_READ_BEAMS
      printf("\n In readPinnacleContourList");
#endif
   if(checkIfObjectList(ContourList) != OK)
   {
      printf("\n ERROR: Invalid Object List"); return(FAIL);
   }
   *nContours = ContourList->nObjects;

   TwoDContourType *lContour= (TwoDContourType *)calloc(*nContours,sizeof(TwoDContourType));
   if(lContour == NULL)
   {
      printf("\n ERROR: allocating memory for contour");return(FAIL);
   }
   *contour = lContour;

   // printf("\n Reading In %d contours", *nContours);

   for(int iContour=0; iContour< *nContours; iContour++)
   {
      if(strcmp("CurvePainter", ContourList->Object[iContour].Handle) )
      {
         printf("\n ERROR: readPinnacleContourList: Expected CurvePainter, Found %s",  
                ContourList->Object[iContour].Handle);
         return(FAIL);
      }
      clifObjectType *RawData;
      if(getClifAddress(&ContourList->Object[iContour],"Curve.RawData", &RawData)!= OK)
      {
         printf("\n ERROR: Getting Address"); return(FAIL);
      } 
      if( readPinnacleRawData(RawData, &lContour[iContour].npts, &lContour[iContour].fvert) != OK)
      {
         printf("\n ERROR: Reading In Contour %d in ContourList", iContour); return(FAIL);
      }
   }
   // printf("\n WARNING: Contours read, but NOT closed"); 
   /* Note: Explicitely call closeContour routines in cases that they are needed */
   return(OK);
}
/* ************************************************************************** */
/* ************************************************************************************ */
int readPinnacleMLC(clifObjectType *MLC, beam_type *pbeam)
{
   // reads in the pinnacle mlc, returns FAIL if at End of File
   // returns END_OF_CONTEXT if reads to end of ModifierList
   // first line is the type of modifier 
   // a "modifier" is considered to be an aperture
   // copied from read_pinnacle_pbeam_modifiers above

  if (pbeam->mlc_defined) // do as long as mlc specified  (!=0 for fail safe)
  {
#ifdef DEBUG_READ_BEAMS
    printf("\n Reading Contour List for Aperture");
#endif
    TwoDContourType *tcontour=NULL;
    // allocate memory for mlc contour 
    tcontour = (TwoDContourType *)calloc(1,sizeof(TwoDContourType));
    if(tcontour == NULL){
      printf("\n ERROR: readPinnacleMLC: allocating memory for contour");return(FAIL);
    }
    if( readPinnacleCurve(MLC, &tcontour->npts, &tcontour->fvert) != OK){
      printf("\n ERROR: readPinnacleMLC: readPinnacleCurve for MLC"); return(FAIL);
    }
    pbeam->mlc.contour = tcontour[0];
#ifdef DEBUG_READ_BEAMS
    printf("\n In readPinnacleMLC found %d contour points for MLC",tcontour->npts);
    printf("\n Done read_pinnacle_mlc");
#endif
  }
  return(OK);
}
/* ************************************************************************** */
int getPinnacleWedgeInfo(clifObjectType *Beam, beam_type *pbeam)
{
   // Wedge stuff depends upon Pinnacle Version....Tell version by existance of WedgeContext
   char WedgeOrientation[MAX_STR_LEN];
   if( checkForClifObject(Beam,"WedgeContext") == OK )
   {
      if(  readStringClifStructure(Beam, "WedgeContext.WedgeName", pbeam->wedge_name) != OK
         || readStringClifStructure(Beam, "WedgeContext.Orientation", WedgeOrientation) != OK ) 
      {
         printf("\n ERROR: getWedgeInfo: Routines for v4.2g"); return(FAIL);
      }
      if(    strcmp(WedgeOrientation,"No Wedge") == 0
          || strcmp(WedgeOrientation,"No wedge") == 0
          || strcmp(WedgeOrientation,"NoWedge")  == 0) pbeam->wedge_defined = 0;
      else
        pbeam->wedge_defined = 1;
   }
   else if( readStringClifStructure(Beam, "WedgeName",pbeam->wedge_name)!= OK 
         || readStringClifStructure(Beam, "WedgeOrientation",WedgeOrientation)!= OK 
	 || readIntClifStructure   (Beam, "IsWedgeValid",     &pbeam->wedge_defined) != OK )
   {
      printf("\n ERROR: getWedgeInfo: Routines for v4.0e"); return(FAIL);
   }
   // Convert the wedge name
   if( convertWedgeName(pbeam->wedge_name) != OK)
   { 
      printf("\n ERROR: Cannot convert wedge name %s",pbeam->wedge_name); return(FAIL);
   }
   // this wedge angle correspondance must be checked for each treatment machine....
   if( strncmp(WedgeOrientation,"WedgeLeftToRight",16) == 0) pbeam->wedge_orientation = 90;
   else if(strncmp(WedgeOrientation,"WedgeRightToLeft",16) == 0) pbeam->wedge_orientation = 270;
   else if(strncmp(WedgeOrientation,"WedgeTopToBottom",15) == 0) pbeam->wedge_orientation = 0;
   else if(strncmp(WedgeOrientation,"WedgeBottomToTop",15) == 0) pbeam->wedge_orientation = 180;
   else if(strcmp(WedgeOrientation,"No Wedge")   == 0
          || strcmp(WedgeOrientation,"No wedge") == 0
          || strcmp(WedgeOrientation,"NoWedge")  == 0) pbeam->wedge_orientation = 0;
   else 
   {
      printf("\n ERROR: %s is not a proper wedge orientation",WedgeOrientation);
      return(FAIL);
   }
   return(OK);
}
/* ******************************************************************************* */
int getPinnacleMLCInfo(clifObjectType *Beam, beam_type *pbeam)
{
   //mlc stuff...this too depends upon the Pinnacle Version
#ifdef DEBUG_READ_BEAMS
   printf("\n Reading in MLC data");
#endif
   char MLCMessage[MAX_STR_LEN];
   if(checkForClifObject(Beam,"MLCSegmentList") == OK)
      sprintf(MLCMessage,"MLCSegmentList.BeamSegment.MLCLeafPositions");
   else
      sprintf(MLCMessage,"MLCLeafPositions");
    // get the address of the MLC leaf positions
   clifObjectType *MLCLeafPositions;
   if(getClifAddress(Beam,MLCMessage, &MLCLeafPositions)!= OK){
      printf("\n ERROR: Getting Address"); return(FAIL);
   }
   // Read In The Leaf Positions for this beam
   if(readPinnacleMLC(MLCLeafPositions, pbeam) !=OK){
      printf("\n ERROR: reading in mlc data"); return(FAIL);
   }
   return(OK);
}
/* ******************************************************************************* */
int convertWedgeName(char *Name) /* remove spaces from wedge name */
{
   int nameLen = (int) (strlen(Name));
   char *tmpName = (char *) calloc(nameLen, sizeof(char) );
   if( tmpName == NULL)
   {
      printf("\n ERROR: Allocating Memory for tmpName"); return(FAIL);
   }
   int iOut=0;
   for(int iIn=0; iIn<nameLen; iIn++)
   {
      if(isalnum(Name[iIn]) )
      {
         tmpName[iOut++] = Name[iIn];
         tmpName[iOut] = 0;
      }
   }
   strcpy(Name,tmpName);
   free(tmpName );
   return(OK);
}
/* ******************************************************************************* */
