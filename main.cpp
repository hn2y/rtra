#include "mainwindow.h"
#include <QApplication>


//int main(int argc, char *argv[])
//{
//    QApplication app(argc, argv);
//    MainWindow w;
//    w.show();
//    return app.exec();
//}

//#include "pinnacleFuncWrapper.h"
//#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFileInfo>
#include <QDebug>


enum InputMode
{
    PINNACLE, DICOM, NRRD
};

struct InputParams
{
    // for Pinnacle
    QString planDotRoiFilePath;
    QString patientFilePath;
    QString totalDoseHeaderFilePath;
    QString totalDoseImageFilePath;
    QString totalDoseImageHeaderFilePath;
    QString planDotPinnacleFilePath;
    // for DICOM
    QString dicomPath;
    QString dicomStructurePath; // optional
    QString dicomDosePath; // optional
    // for nrrd
    QString ctImageFilePath;
    QString roiFolderPath;
    QString doseImageFilePath;
    // common
    QString volumeName; // return the makes for all the ROIs associated with this volume
    std::vector<std::string> requestedROINamesQVectorList;
    QStringList requestedROINamesQStringList;
    QString folderPathToSaveResults;
    QString planName;
    QString trialName;
    float sysTranSigmaX;
    float sysTranSigmaY;
    float sysTranSigmaZ;
    float randomTranSigmaX;
    float randomTranSigmaY;
    float randomTranSigmaZ;
    float sysRotSigmaX;
    float sysRotSigmaY;
    float sysRotSigmaZ;
    float randomRotSigmaX;
    float randomRotSigmaY;
    float randomRotSigmaZ;

    float intraFxTranSigmaX = 0;
    float intraFxTranSigmaY = 0;
    float intraFxTranSigmaZ = 0;

    float intraFxRotSigmaX = 0;
    float intraFxRotSigmaY = 0;
    float intraFxRotSigmaZ = 0;

    int numFrac;
    int numSimulation;
    int numSubFx = 1;
    bool debug;
    bool doEncodedMask;
    enum InputMode inputMode = PINNACLE;
};


enum CommandLineParseResult
{
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested,
    CommandRoiListRequested
};

bool fileExists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool folderExists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isDir())
    {
        return true;
    }
    else
    {
        return false;
    }
}


CommandLineParseResult parseCommandLine(QCommandLineParser &parser, InputParams *query, QString *errorMessage)
{
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
    const QCommandLineOption inputModeOption("m", "Input Mode. (p: Pinnacle, d: DICOM, n: nrrd)", "inputMode", "p");
    parser.addOption(inputModeOption);

    // Pinnacle
    const QCommandLineOption patientPathOption("p", "Pinnacle patient file path.", "patientpath");
    parser.addOption(patientPathOption);
    const QCommandLineOption roiPathOption("r", "ROI file path.", "roipath");
    parser.addOption(roiPathOption);
    //    const QCommandLineOption encodeMaskOption("e", "compress/encode the mask(s) true/false", "encodedMask");
    //    parser.addOption(encodeMaskOption);
    const QCommandLineOption totalDoseHeaderFilePathOption("tdh", "total dose header file path.", "TotalDoseHeader");
    parser.addOption(totalDoseHeaderFilePathOption);

    const QCommandLineOption totalDoseImageHeaderOption("tdih", "total dose image header file path",
                                                        "TotalDoseImageHeader");
    parser.addOption(totalDoseImageHeaderOption);

    const QCommandLineOption totalDoseImageOption("tdi", "total dose image  file path", "TotalDoseImage");
    parser.addOption(totalDoseImageOption);

    const QCommandLineOption planDotPinnacleOption("pp", "plan.Pinnacle file path", "planDotPinnalce");
    parser.addOption(planDotPinnacleOption);

    // DICOM
    const QCommandLineOption dicomPathOption("d", "DICOM path.", "DicomPath");
    parser.addOption(dicomPathOption);
    const QCommandLineOption dicomStructurePathOption("ds", "DICOM Structure file path. (optional)", "StructurePath");
    parser.addOption(dicomStructurePathOption);
    const QCommandLineOption dicomDosePathOption("dd", "DICOM Dose file path. (optional)", "DosePath");
    parser.addOption(dicomDosePathOption);

    // NRRD
    const QCommandLineOption nrrdCTImagePathOption("nc", "NRRD CT image file path.", "CTImagePath");
    parser.addOption(nrrdCTImagePathOption);
    const QCommandLineOption nrrdStructurePathOption("ns", "NRRD Structure folder path.", "StructurePath");
    parser.addOption(nrrdStructurePathOption);
    const QCommandLineOption nrrdDosePathOption("nd", "NRRD Dose image file path.", "DosePath");
    parser.addOption(nrrdDosePathOption);

    const QCommandLineOption resultFolderOption("resultfolder", "a folder that results will be saved", "ResultFolder");
    parser.addOption(resultFolderOption);

    // translation
    const QCommandLineOption sysTranSigmaXOption("stX", "Systematic translation error STD in X direction[cm]",
                                                 "sysTranSigmaX");
    parser.addOption(sysTranSigmaXOption);

    const QCommandLineOption sysTranSigmaYOption("stY", "Systematic translation error STD in Y direction[cm]",
                                                 "sysTranSigmaY");
    parser.addOption(sysTranSigmaYOption);

    const QCommandLineOption sysTranSigmaZOption("stZ", "Systematic translation error STD in Z direction[cm]",
                                                 "sysTranSigmaZ");
    parser.addOption(sysTranSigmaZOption);


    const QCommandLineOption randomTranSigmaXOption("rtX", "Random translation error STD in X direction[cm]",
                                                    "randomTranSigmaX");
    parser.addOption(randomTranSigmaXOption);

    const QCommandLineOption randomTranSigmaYOption("rtY", "Random translation error STD in Y direction[cm]",
                                                    "randomTranSigmaY");
    parser.addOption(randomTranSigmaYOption);

    const QCommandLineOption randomTranSigmaZOption("rtZ", "Random translation error STD in Z direction[cm]",
                                                    "randomTranSigmaZ");
    parser.addOption(randomTranSigmaZOption);


    const QCommandLineOption intrafrctionTranSigmaXOption("itX", "intraFx translation error STD in X direction[cm]",
                                                          "intraFxTranSigmaX");
    parser.addOption(intrafrctionTranSigmaXOption);

    const QCommandLineOption intrafrctionTranSigmaYOption("itY", "intraFx translation error STD in Y direction[cm]",
                                                          "intraFxTranSigmaY");
    parser.addOption(intrafrctionTranSigmaYOption);

    const QCommandLineOption intrafrctionTranSigmaZOption("itZ", "intraFx translation error STD in Z direction[cm]",
                                                          "intraFxTranSigmaZ");
    parser.addOption(intrafrctionTranSigmaZOption);


    // Rotation
    const QCommandLineOption sysRotSigmaXOption("srX", "Systematic rotation error STD about X [Deg]", "sysRotSigmaX");
    parser.addOption(sysRotSigmaXOption);

    const QCommandLineOption sysRotSigmaYOption("srY", "Systematic rotation error STD about Y [Deg]", "sysRotSigmaY");
    parser.addOption(sysRotSigmaYOption);

    const QCommandLineOption sysRotSigmaZOption("srZ", "Systematic rotation error STD about Z [Deg]", "sysRotSigmaZ");
    parser.addOption(sysRotSigmaZOption);


    const QCommandLineOption randomRotSigmaXOption("rrX", "Random rotation error STD about X [Deg]", "randomRotSigmaX");
    parser.addOption(randomRotSigmaXOption);

    const QCommandLineOption randomRotSigmaYOption("rrY", "Random rotation error STD about Y [Deg]", "randomRotSigmaY");
    parser.addOption(randomRotSigmaYOption);

    const QCommandLineOption randomRotSigmaZOption("rrZ", "Random rotation error STD about Z [Deg]", "randomRotSigmaZ");
    parser.addOption(randomRotSigmaZOption);


    const QCommandLineOption intrafrctionRotSigmaXOption("irX", "intraFx rotation error STD about X [Deg]",
                                                         "intraFxRotSigmaX");
    parser.addOption(intrafrctionRotSigmaXOption);

    const QCommandLineOption intrafrctionRotSigmaYOption("irY", "intraFx rotation error STD about Y [Deg]",
                                                         "intraFxRotSigmaY");
    parser.addOption(intrafrctionRotSigmaYOption);

    const QCommandLineOption intrafrctionRotSigmaZOption("irZ", "intraFx rotation error STD about Z [Deg]",
                                                         "intraFxRotSigmaZ");
    parser.addOption(intrafrctionRotSigmaZOption);


    const QCommandLineOption numFracOption("frac", "Number of fractions for this patient", "NumOfFractions");
    parser.addOption(numFracOption);

    const QCommandLineOption numSimulationOption("numSim", "Number of virtual treatment simulations.", "numSimulation");
    parser.addOption(numSimulationOption);


    const QCommandLineOption numSubFxOption("subfrac", "Number of sub fractions for intrafaction motions.",
                                            "numSubFraction");
    parser.addOption(numSubFxOption);


    const QCommandLineOption verboseOption("verbose", "Verbosity flag, if exist verbosity is on.");
    parser.addOption(verboseOption);

    const QCommandLineOption trialNameOption("trialname", "Trial name.", "trialName");
    parser.addOption(trialNameOption);

    const QCommandLineOption planNameOption("planname", "Plan name.", "planName");
    parser.addOption(planNameOption);


    const QCommandLineOption listRoisOption("roilist", "provides a list of ROIs that exist in roi file");
    parser.addOption(listRoisOption);

    parser.addPositionalArgument("roiNames", "The roi name(s) to perform robust analysis.");
    const QCommandLineOption helpOption = parser.addHelpOption();
    const QCommandLineOption versionOption = parser.addVersionOption();


    if (!parser.parse(QCoreApplication::arguments()))
    {
        *errorMessage = parser.errorText();
        return CommandLineError;
    }


    if (parser.isSet(versionOption))
        return CommandLineVersionRequested;

    if (parser.isSet(helpOption))
        return CommandLineHelpRequested;


    if (parser.isSet(inputModeOption))
    {
        const QString inputModeStr = parser.value(inputModeOption);
        if(inputModeStr == "p")
        {
            query->inputMode = PINNACLE;
        }
        else if(inputModeStr == "d")
        {
            query->inputMode = DICOM;
        }
        else if(inputModeStr == "n")
        {
            query->inputMode = NRRD;
        }
        else
        {
            *errorMessage = QString("Input Mode %1 is not supported, p: Pinnacle, d: DICOM, n: nrrd").arg(inputModeStr);
            return CommandLineError;
        }
    }
    // Pinnacle
    if(query->inputMode == PINNACLE)
    {
        if (parser.isSet(patientPathOption))
        {
            const QString patientFile = parser.value(patientPathOption);
            query->patientFilePath = patientFile;
            // now check if the patient file is acctually exists
            if (!fileExists(query->patientFilePath))
            {
                *errorMessage = QString("Patient file does not exist: %1").arg(query->patientFilePath);
                return CommandLineError;
            }

        }
        else
        {
            *errorMessage = "Patient File is not set!";
            return CommandLineError;
        }


        if (parser.isSet(roiPathOption))
        {
            const QString roiPathString = parser.value(roiPathOption);
            query->planDotRoiFilePath = roiPathString;
            if (!fileExists(query->planDotRoiFilePath))
            {
                *errorMessage = QString("Roi file does not exist: %1").arg(query->planDotRoiFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Roi File is not set!";
            return CommandLineError;
        }


        if (parser.isSet(planDotPinnacleOption))
        {
            const QString planDotPinnaclePathQString = parser.value(planDotPinnacleOption);
            query->planDotPinnacleFilePath = planDotPinnaclePathQString;
            if (!fileExists(query->planDotPinnacleFilePath))
            {
                *errorMessage = QString("plan.Pinnacle file does not exist: %1").arg(query->planDotPinnacleFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "plan.Pinnacle is not set!";
            return CommandLineError;
        }

        if (parser.isSet(listRoisOption))
        {
            return CommandRoiListRequested;
        }

        /////*********************************** TOTAL DOSE ****************************************************////

        if (parser.isSet(totalDoseHeaderFilePathOption))
        {
            const QString tdhString = parser.value(totalDoseHeaderFilePathOption);
            query->totalDoseHeaderFilePath = tdhString;
            if (!fileExists(query->totalDoseHeaderFilePath))
            {
                *errorMessage = QString("Total dose header file does not exist: %1").arg(
                        query->totalDoseHeaderFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Total dose header is not set!";
            return CommandLineError;
        }

        if (parser.isSet(totalDoseImageHeaderOption))
        {
            const QString tdihString = parser.value(totalDoseImageHeaderOption);
            query->totalDoseImageHeaderFilePath = tdihString;
            if (!fileExists(query->totalDoseImageHeaderFilePath))
            {
                *errorMessage = QString("Total dose image header(tdih) file does not exist: %1").arg(
                        query->totalDoseImageHeaderFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Total dose image header (tdih) is not set!";
            return CommandLineError;
        }

        if (parser.isSet(totalDoseImageOption))
        {
            const QString tdiString = parser.value(totalDoseImageOption);
            query->totalDoseImageFilePath = tdiString;
            if (!fileExists(query->totalDoseImageHeaderFilePath))
            {
                *errorMessage = QString("Total dose image binary (tdi) file does not exist: %1").arg(
                        query->totalDoseImageFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Total dose image binary (tdi) is not set!";
            return CommandLineError;
        }
    }
    else if(query->inputMode == DICOM)
    {
        if (parser.isSet(dicomPathOption))
        {
            const QString dicomPath = parser.value(dicomPathOption);
            query->dicomPath = dicomPath;
            // now check if the folder is actually exists
            if (!folderExists(query->dicomPath))
            {
                *errorMessage = QString("DICOM path does not exist: %1").arg(query->dicomPath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "DICOM path is not set!";
            return CommandLineError;
        }

        if (parser.isSet(dicomStructurePathOption))
        {
            const QString dicomStructurePath = parser.value(dicomStructurePathOption);
            query->dicomStructurePath = dicomStructurePath;
            // now check if the file is actually exists
            if (!fileExists(query->dicomStructurePath))
            {
                *errorMessage = QString("DICOM Structure path does not exist: %1").arg(query->dicomStructurePath);
                return CommandLineError;
            }
        }
        else
        {
            query->dicomStructurePath = "";
        }

        if (parser.isSet(dicomDosePathOption))
        {
            const QString dicomDosePath = parser.value(dicomDosePathOption);
            query->dicomDosePath = dicomDosePath;
            // now check if the file is actually exists
            if (!fileExists(query->dicomDosePath))
            {
                *errorMessage = QString("DICOM Dose path does not exist: %1").arg(query->dicomDosePath);
                return CommandLineError;
            }
        }
        else
        {
            query->dicomDosePath = "";
        }
    }
    else if(query->inputMode == NRRD) {
        if (parser.isSet(nrrdCTImagePathOption))
        {
            const QString nrrdCTImagePath = parser.value(nrrdCTImagePathOption);
            query->ctImageFilePath = nrrdCTImagePath;
            // now check if the file is actually exists
            if (!fileExists(query->ctImageFilePath))
            {
                *errorMessage = QString("CT image file does not exist: %1").arg(query->ctImageFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "CT image is not set!";
            return CommandLineError;
        }

        if (parser.isSet(nrrdStructurePathOption))
        {
            const QString nrrdStructurePath = parser.value(nrrdStructurePathOption);
            query->roiFolderPath = nrrdStructurePath;
            // now check if the folder is actually exists
            if (!folderExists(query->roiFolderPath))
            {
                *errorMessage = QString("Structure folder does not exist: %1").arg(query->roiFolderPath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Structure folder is not set!";
            return CommandLineError;
        }

        if (parser.isSet(nrrdDosePathOption))
        {
            const QString nrrdDosePath = parser.value(nrrdDosePathOption);
            query->doseImageFilePath = nrrdDosePath;
            // now check if the file is actually exists
            if (!fileExists(query->doseImageFilePath))
            {
                *errorMessage = QString("Dose image file does not exist: %1").arg(query->doseImageFilePath);
                return CommandLineError;
            }
        }
        else
        {
            *errorMessage = "Dose image is not set!";
            return CommandLineError;
        }
    }
    else {

    }


    const QStringList positionalArguments = parser.positionalArguments();
    if (positionalArguments.isEmpty())
    {
        *errorMessage = "Argument 'roiname(s)' missing.";
        return CommandLineError;
    }
    //    if (positionalArguments.size() > 1) {
    //        *errorMessage = "Several 'roinames' arguments specified.";
    //        return CommandLineError;
    //    }

    query->requestedROINamesQStringList = positionalArguments;
    for (int iRoi = 0; iRoi < positionalArguments.size(); ++iRoi)
    {
        query->requestedROINamesQVectorList.push_back(positionalArguments.at(iRoi).toStdString());

    }
    /////*********************************** plan Name****************************************************////

    if (parser.isSet(planNameOption))
    {
        const QString planNameQString = parser.value(planNameOption);
        query->planName = planNameQString;
    }
    else
    {
        *errorMessage = "Plan name is not set";
        return CommandLineError;
    }
    /////*********************************** Trial Name****************************************************////

    if (parser.isSet(trialNameOption))
    {
        const QString trialnameQString = parser.value(trialNameOption);
        query->trialName = trialnameQString;
    }
    else
    {
        *errorMessage = "Trial name is not set";
        return CommandLineError;
    }




    /////*********************************** STD of Translation errors ****************************************************////

    //systematic Trans
    if (parser.isSet(sysTranSigmaXOption))
    {
        const QString sysTranSigmaXString = parser.value(sysTranSigmaXOption);
        bool ok = false;
        query->sysTranSigmaX = sysTranSigmaXString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysTranSigmaX is expected : %1").arg(sysTranSigmaXString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "sysTranSigmaX is not set!";
        return CommandLineError;
    }

    if (parser.isSet(sysTranSigmaYOption))
    {
        const QString sysTranSigmaYString = parser.value(sysTranSigmaYOption);
        bool ok = false;
        query->sysTranSigmaY = sysTranSigmaYString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysTranSigmaX is expected : %1").arg(sysTranSigmaYString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "sysTranSigmaY is not set!";
        return CommandLineError;
    }

    if (parser.isSet(sysTranSigmaZOption))
    {
        const QString sysTranSigmaZString = parser.value(sysTranSigmaZOption);
        bool ok = false;
        query->sysTranSigmaZ = sysTranSigmaZString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysTranSigmaX is expected : %1").arg(sysTranSigmaZString);
            return CommandLineError;
        }
    }
    else
    {
        *errorMessage = "sysTranSigmaZ is not set!";
        return CommandLineError;
    }
    //random Trans

    if (parser.isSet(randomTranSigmaXOption))
    {
        const QString randomTranSigmaXOptionString = parser.value(randomTranSigmaXOption);
        bool ok = false;
        query->randomTranSigmaX = randomTranSigmaXOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomTranSigmaX is expected : %1").arg(
                    randomTranSigmaXOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomTranSigmaX is not set!";
        return CommandLineError;
    }

    if (parser.isSet(randomTranSigmaYOption))
    {
        const QString randomTranSigmaYOptionString = parser.value(randomTranSigmaYOption);
        bool ok = false;
        query->randomTranSigmaY = randomTranSigmaYOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomTranSigmaX is expected : %1").arg(
                    randomTranSigmaYOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomTranSigmaY is not set!";
        return CommandLineError;
    }


    if (parser.isSet(randomTranSigmaZOption))
    {
        const QString randomTranSigmaZOptionString = parser.value(randomTranSigmaZOption);
        bool ok = false;
        query->randomTranSigmaZ = randomTranSigmaZOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomTranSigmaZ is expected : %1").arg(
                    randomTranSigmaZOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomTranSigmaZ is not set!";
        return CommandLineError;
    }


    //intraFx Trans
    if (parser.isSet(intrafrctionTranSigmaXOption))
    {
        const QString intrafrctionTranSigmaXOptionString = parser.value(intrafrctionTranSigmaXOption);
        bool ok = false;
        query->intraFxTranSigmaX = intrafrctionTranSigmaXOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxTranSigmaX is expected : %1").arg(
                    intrafrctionTranSigmaXOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxTranSigmaX is not set!";
        //        return CommandLineError;

        query->intraFxTranSigmaX = 0.0;

    }
    if (parser.isSet(intrafrctionTranSigmaYOption))
    {
        const QString intrafrctionTranSigmaYOptionString = parser.value(intrafrctionTranSigmaYOption);
        bool ok = false;
        query->intraFxTranSigmaY = intrafrctionTranSigmaYOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxTranSigmaY is expected : %1").arg(
                    intrafrctionTranSigmaYOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxTranSigmaY is not set!";
        //        return CommandLineError;

        query->intraFxTranSigmaY = 0.0;

    }

    if (parser.isSet(intrafrctionTranSigmaZOption))
    {
        const QString intrafrctionTranSigmaZOptionString = parser.value(intrafrctionTranSigmaZOption);
        bool ok = false;
        query->intraFxTranSigmaZ = intrafrctionTranSigmaZOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxTranSigmaZ is expected : %1").arg(
                    intrafrctionTranSigmaZOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxTranSigmaY is not set!";
        //        return CommandLineError;

        query->intraFxTranSigmaZ = 0.0;
    }

    /////*********************************** STD of Rotation errors ****************************************************////

    if (parser.isSet(sysRotSigmaXOption))
    {
        const QString sysRotSigmaXOptionString = parser.value(sysRotSigmaXOption);
        bool ok = false;
        query->sysRotSigmaX = sysRotSigmaXOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysRotSigmaX is expected : %1").arg(sysRotSigmaXOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "sysRotSigmaX is not set!";
        return CommandLineError;
    }

    if (parser.isSet(sysRotSigmaYOption))
    {
        const QString sysRotSigmaYOptionString = parser.value(sysRotSigmaYOption);
        bool ok = false;
        query->sysRotSigmaY = sysRotSigmaYOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysRotSigmaY is expected : %1").arg(sysRotSigmaYOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "sysRotSigmaY is not set!";
        return CommandLineError;
    }


    if (parser.isSet(sysRotSigmaZOption))
    {
        const QString sysRotSigmaZOptionString = parser.value(sysRotSigmaZOption);
        bool ok = false;
        query->sysRotSigmaZ = sysRotSigmaZOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for sysRotSigmaZ is expected : %1").arg(sysRotSigmaZOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "sysRotSigmaZ is not set!";
        return CommandLineError;
    }

    if (parser.isSet(randomRotSigmaXOption))
    {
        const QString randomRotSigmaXOptionString = parser.value(randomRotSigmaXOption);
        bool ok = false;
        query->randomRotSigmaX = randomRotSigmaXOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomRotSigmaX is expected : %1").arg(
                    randomRotSigmaXOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomRotSigmaX is not set!";
        return CommandLineError;
    }

    if (parser.isSet(randomRotSigmaYOption))
    {
        const QString randomRotSigmaYOptionString = parser.value(randomRotSigmaYOption);
        bool ok = false;
        query->randomRotSigmaY = randomRotSigmaYOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomRotSigmaY is expected : %1").arg(
                    randomRotSigmaYOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomRotSigmaY is not set!";
        return CommandLineError;
    }

    if (parser.isSet(randomRotSigmaZOption))
    {
        const QString randomRotSigmaZOptionString = parser.value(randomRotSigmaZOption);
        bool ok = false;
        query->randomRotSigmaZ = randomRotSigmaZOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for randomRotSigmaZ is expected : %1").arg(
                    randomRotSigmaZOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "randomRotSigmaZ is not set!";
        return CommandLineError;
    }



    //intraFx Rotation
    if (parser.isSet(intrafrctionRotSigmaXOption))
    {
        const QString intrafrctionRotSigmaXOptionString = parser.value(intrafrctionRotSigmaXOption);
        bool ok = false;
        query->intraFxRotSigmaX = intrafrctionRotSigmaXOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxRotSigmaX is expected : %1").arg(
                    intrafrctionRotSigmaXOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxRotSigmaX is not set!";
        //        return CommandLineError;

        query->intraFxRotSigmaX = 0.0;

    }
    if (parser.isSet(intrafrctionRotSigmaYOption))
    {
        const QString intrafrctionRotSigmaYOptionString = parser.value(intrafrctionRotSigmaYOption);
        bool ok = false;
        query->intraFxRotSigmaY = intrafrctionRotSigmaYOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxRotSigmaY is expected : %1").arg(
                    intrafrctionRotSigmaYOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxRotSigmaY is not set!";
        //        return CommandLineError;

        query->intraFxRotSigmaY = 0.0;

    }

    if (parser.isSet(intrafrctionRotSigmaZOption))
    {
        const QString intrafrctionRotSigmaZOptionString = parser.value(intrafrctionRotSigmaZOption);
        bool ok = false;
        query->intraFxRotSigmaZ = intrafrctionRotSigmaZOptionString.toFloat(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for intraFxRotSigmaZ is expected : %1").arg(
                    intrafrctionRotSigmaZOptionString);
            return CommandLineError;
        }
    }
    else
    {
        //        *errorMessage = "intraFxRotSigmaY is not set!";
        //        return CommandLineError;

        query->intraFxRotSigmaZ = 0.0;
    }

    /////***********************************Number of fractions****************************************************////
    if (parser.isSet(numFracOption))
    {
        const QString numFracOptionString = parser.value(numFracOption);
        bool ok = false;
        query->numFrac = numFracOptionString.toInt(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for NumOfFractions is expected : %1").arg(numFracOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "NumOfFractions is not set!";
        return CommandLineError;
    }


    if (parser.isSet(numSubFxOption))
    {
        const QString numSubFxOptionString = parser.value(numSubFxOption);
        bool ok = false;
        query->numSubFx = numSubFxOptionString.toInt(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for numSubFx is expected : %1").arg(numSubFxOptionString);
            return CommandLineError;
        }

    }
    else
    {
        //        *errorMessage = "numSubFx is not set!";
        //        return CommandLineError;
        query->numSubFx = 0;
    }



    /////***********************************Number of Simulations****************************************************////
    if (parser.isSet(numSimulationOption))
    {
        const QString numSimulationOptionString = parser.value(numSimulationOption);
        bool ok = false;
        query->numSimulation = numSimulationOptionString.toInt(&ok);
        if (!ok)
        {
            *errorMessage = QString("Numeric value for number of simulations is expected : %1").arg(
                    numSimulationOptionString);
            return CommandLineError;
        }

    }
    else
    {
        *errorMessage = "numSimulation is not set!";
        return CommandLineError;
    }


    /////***********************************Result folder ****************************************************////


    if (parser.isSet(resultFolderOption))
    {
        const QString resultfolder = parser.value(resultFolderOption);
        query->folderPathToSaveResults = resultfolder;
        if (!folderExists(query->folderPathToSaveResults))
        {
            *errorMessage = QString("result folder does not exist: %1").arg(query->folderPathToSaveResults);
            return CommandLineError;
        }
    }
    else
    {
        *errorMessage = "Result folder is not set!";
        return CommandLineError;
    }
    /////***********************************Verbosity ****************************************************////


    if (parser.isSet(verboseOption))
    {
        query->debug = true;

    }
    else
    {
        query->debug = false;

    }





    //    if (parser.isSet(encodeMaskOption))
    //    {
    //        QStringList myOptions;
    //        myOptions << "true" << "True" << "yes" << "false" << "False" <<"no";
    //        switch (myOptions.indexOf(parser.value(encodeMaskOption)))
    //        {
    //         case 0:case 1: case 2:
    //            query->doEncodedMask = true;
    //            break;
    //        case 3:case 4: case 5:
    //            query->doEncodedMask = false;
    //            break;
    //        default:
    //            *errorMessage = QString("parameter provided for e option is not known. Supported parameters (true/True/yes/false/False/no): %1").arg(parser.value(encodeMaskOption)) ;
    //            return CommandLineError;
    //            break;
    //        }
    //    }
    //    else
    //    {
    //         query->doEncodedMask = false;
    //    }
    return CommandLineOk;
}


//Call example
// RTRA -p /home/hamid/gitRepos/NKI/patient_319/Patient -r  /home/hamid/gitRepos/NKI/patient_319/Plan_2/plan.roi GTV1_00
//-tdh /home/hamid/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.header
//-tdih /home/hamid/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img.header
//-tdi /home/hamid/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img
//-stX 0.3  -stY 0.31 -stZ 0.41 -rtX 0.2 -rtY 0.25 -rtZ 0.35 -srX 0 -srY 0.01 -srZ 0.02 -rrX 0.03 -rrY 0.04 -rrZ 0.05
//  -frac 30  -numSim 1000 -resultfolder /home/hamid/temp

int main(int argc, char *argv[])
{
    //    QCoreApplication app(argc, argv);
    //    QCoreApplication::setApplicationName("RTRA");
    //    QCoreApplication::setApplicationVersion("1.0");
    //pinnacleFuncWrapper pinnFuncWrapper;

    QApplication app(argc, argv);
    QApplication::setApplicationName("RTRA");
    QApplication::setApplicationVersion("1.0");
    MainWindow w;
    //      return 0;
    QCommandLineParser parser;
    parser.setApplicationDescription(
            "Robustness Analyzer performs robustness analysis for the desired ROIs given pinnacle patient file, and total dose files.");
    InputParams inputParams;
    QString errorMassage;


    switch (parseCommandLine(parser, &inputParams, &errorMassage))
    {
        case CommandLineOk:
            qDebug() << "Requested Rois: " << inputParams.requestedROINamesQStringList;
            for (int iRoi = 0; iRoi < inputParams.requestedROINamesQVectorList.size(); ++iRoi)
            {
                qDebug() << inputParams.requestedROINamesQVectorList[iRoi].c_str();
            }
            if (inputParams.inputMode == PINNACLE)
            {
                qDebug() << "Input Patient file: " << inputParams.patientFilePath;
                qDebug() << "Input Roi file : " << inputParams.planDotRoiFilePath;
                qDebug() << "Input plan.Pinnacle file : " << inputParams.planDotPinnacleFilePath;
                qDebug() << "The total dose header: " << inputParams.totalDoseHeaderFilePath;
                qDebug() << "The total dose image header: " << inputParams.totalDoseImageHeaderFilePath;
                qDebug() << "The total dose image: " << inputParams.totalDoseImageFilePath;
            }
            else if (inputParams.inputMode == DICOM)
            {
                qDebug() << "Input DICOM Path: " << inputParams.dicomPath;
                qDebug() << "Input DICOM Structure Path: " << inputParams.dicomStructurePath;
                qDebug() << "Input DICOM Dose Path: " << inputParams.dicomDosePath;
            }
            else if (inputParams.inputMode == NRRD)
            {
                qDebug() << "Input CT image file: " << inputParams.ctImageFilePath;
                qDebug() << "Input ROI Structure folder: " << inputParams.roiFolderPath;
                qDebug() << "Input Dose image file: " << inputParams.doseImageFilePath;
            }

            qDebug() << "The result folder : " << inputParams.folderPathToSaveResults;
            qDebug() << "STD of systematic translation error in X, Y, Z: " << inputParams.sysTranSigmaX
                     << inputParams.sysTranSigmaY << inputParams.sysTranSigmaZ;
            qDebug() << "STD of random translation error in X, Y, Z: " << inputParams.randomTranSigmaX
                     << inputParams.randomTranSigmaY << inputParams.randomTranSigmaZ;
            qDebug() << "STD of systematic rotation error in X, Y, Z: " << inputParams.sysRotSigmaX
                     << inputParams.sysRotSigmaY << inputParams.sysRotSigmaZ;
            qDebug() << "STD of random rotation error in X, Y, Z: " << inputParams.randomRotSigmaX
                     << inputParams.randomRotSigmaY << inputParams.randomRotSigmaZ;
            qDebug() << "Number of fractions: " << inputParams.numFrac;
            qDebug() << "Number of sub fractions: " << inputParams.numSubFx;
            if (inputParams.numSubFx == 0)
            {
                qDebug() << "Setting all the intraFx parameters to zeros since numSubFx =0";
                inputParams.intraFxTranSigmaX = 0;
                inputParams.intraFxTranSigmaY = 0;
                inputParams.intraFxTranSigmaZ = 0;
                inputParams.intraFxRotSigmaX = 0;
                inputParams.intraFxRotSigmaY = 0;
                inputParams.intraFxRotSigmaZ = 0;
            }

            qDebug() << "STD of intraFx translation error in X, Y, Z: " << inputParams.intraFxTranSigmaX
                     << inputParams.intraFxTranSigmaY << inputParams.intraFxTranSigmaZ;
            qDebug() << "STD of intraFx rotation error in X, Y, Z: " << inputParams.intraFxRotSigmaX
                     << inputParams.intraFxRotSigmaY << inputParams.intraFxRotSigmaZ;

            qDebug() << "Number of virtual treatment simulations: " << inputParams.numSimulation;
            qDebug() << "Trial name: " << inputParams.trialName;
            qDebug() << "Plan name: " << inputParams.planName;
            inputParams.debug = false;
            qDebug() << "Debug Mode?" << inputParams.debug;

            //        qDebug() << "Will the binary masks be encoded? " <<inputParams.doEncodedMask;
            break;
        case CommandRoiListRequested:
            qDebug() << "Input Patient file: " << inputParams.patientFilePath;
            qDebug() << "Input Roi file : " << inputParams.planDotRoiFilePath;
            //        pinnFuncWrapper.listRoisForGivenPatient(inputParams.patientFilePath,inputParams.planDotRoiFilePath);
            w.listRoisForGivenPatient(inputParams.patientFilePath, inputParams.planDotRoiFilePath);

            return 0;
            break;
        case CommandLineError:
            fputs(qPrintable(errorMassage), stderr);
            fputs("\n \n ", stderr);
            fputs(qPrintable(parser.helpText()), stderr);
            return 1;
            break;
        case CommandLineVersionRequested:
            printf("%s %s\n", qPrintable(QCoreApplication::applicationName()),
                   qPrintable(QCoreApplication::applicationVersion()));
            return 0;
            break;
        case CommandLineHelpRequested:
            parser.showHelp();
            Q_UNREACHABLE();
            break;
    }




    //        pinnFuncWrapper.robustnessAnalyzerPinnacle(inputParams.patientFilePath,
    //                                                   inputParams.planDotRoiFilePath,
    //                                                   inputParams.requestedROINamesQVectorList,
    //                                                   inputParams.totalDoseHeaderFilePath,
    //                                                   inputParams.totalDoseImageFilePath,
    //                                                   inputParams.totalDoseImageHeaderFilePath,
    //                                                   inputParams.folderPathToSaveResults,
    //                                                   inputParams.sysTranSigmaX,
    //                                                   inputParams.sysTranSigmaY,
    //                                                   inputParams.sysTranSigmaZ,
    //                                                   inputParams.randomTranSigmaX,
    //                                                   inputParams.randomTranSigmaY,
    //                                                   inputParams.randomTranSigmaZ,
    //                                                   inputParams.sysRotSigmaX,
    //                                                   inputParams.sysRotSigmaY,
    //                                                   inputParams.sysRotSigmaZ,
    //                                                   inputParams.randomRotSigmaX,
    //                                                   inputParams.randomRotSigmaY,
    //                                                   inputParams.randomRotSigmaZ,
    //                                                   inputParams.numFrac,
    //                                                   inputParams.numSimulation,
    //                                                   inputParams.debug);

    //        w.robustnessAnalyzerPinnacle(inputParams.patientFilePath,
    //                                                   inputParams.planDotRoiFilePath,
    //                                                   inputParams.planDotPinnacleFilePath,
    //                                                   inputParams.requestedROINamesQVectorList,
    //                                                   inputParams.totalDoseHeaderFilePath,
    //                                                   inputParams.totalDoseImageFilePath,
    //                                                   inputParams.totalDoseImageHeaderFilePath,
    //                                                   inputParams.folderPathToSaveResults,
    //                                                   inputParams.sysTranSigmaX,
    //                                                   inputParams.sysTranSigmaY,
    //                                                   inputParams.sysTranSigmaZ,
    //                                                   inputParams.randomTranSigmaX,
    //                                                   inputParams.randomTranSigmaY,
    //                                                   inputParams.randomTranSigmaZ,
    //                                                   inputParams.sysRotSigmaX,
    //                                                   inputParams.sysRotSigmaY,
    //                                                   inputParams.sysRotSigmaZ,
    //                                                   inputParams.randomRotSigmaX,
    //                                                   inputParams.randomRotSigmaY,
    //                                                   inputParams.randomRotSigmaZ,
    //                                                   inputParams.numFrac,
    //                                                   inputParams.numSimulation,
    //                                                   inputParams.trialName,
    //                                                   inputParams.planName,
    //                                                   inputParams.debug);


    if (inputParams.inputMode == PINNACLE)
    {
        w.robustnessAnalyzerPinnacle(inputParams.patientFilePath,
                                     inputParams.planDotRoiFilePath,
                                     inputParams.planDotPinnacleFilePath,
                                     inputParams.requestedROINamesQVectorList,
                                     inputParams.totalDoseHeaderFilePath,
                                     inputParams.totalDoseImageFilePath,
                                     inputParams.totalDoseImageHeaderFilePath,
                                     inputParams.folderPathToSaveResults,
                                     inputParams.sysTranSigmaX,
                                     inputParams.sysTranSigmaY,
                                     inputParams.sysTranSigmaZ,
                                     inputParams.randomTranSigmaX,
                                     inputParams.randomTranSigmaY,
                                     inputParams.randomTranSigmaZ,
                                     inputParams.sysRotSigmaX,
                                     inputParams.sysRotSigmaY,
                                     inputParams.sysRotSigmaZ,
                                     inputParams.randomRotSigmaX,
                                     inputParams.randomRotSigmaY,
                                     inputParams.randomRotSigmaZ,
                                     inputParams.intraFxTranSigmaX,
                                     inputParams.intraFxTranSigmaY,
                                     inputParams.intraFxTranSigmaZ,
                                     inputParams.intraFxRotSigmaX,
                                     inputParams.intraFxRotSigmaY,
                                     inputParams.intraFxRotSigmaZ,
                                     inputParams.numFrac,
                                     inputParams.numSimulation,
                                     inputParams.numSubFx,
                                     inputParams.trialName,
                                     inputParams.planName,
                                     inputParams.debug);
    }
    else if (inputParams.inputMode == DICOM)
    {
        w.robustnessAnalyzerDICOM(inputParams.dicomPath,
                                  inputParams.dicomStructurePath,
                                  inputParams.dicomDosePath,
                                  inputParams.requestedROINamesQVectorList,
                                  inputParams.folderPathToSaveResults,
                                  inputParams.sysTranSigmaX,
                                  inputParams.sysTranSigmaY,
                                  inputParams.sysTranSigmaZ,
                                  inputParams.randomTranSigmaX,
                                  inputParams.randomTranSigmaY,
                                  inputParams.randomTranSigmaZ,
                                  inputParams.sysRotSigmaX,
                                  inputParams.sysRotSigmaY,
                                  inputParams.sysRotSigmaZ,
                                  inputParams.randomRotSigmaX,
                                  inputParams.randomRotSigmaY,
                                  inputParams.randomRotSigmaZ,
                                  inputParams.intraFxTranSigmaX,
                                  inputParams.intraFxTranSigmaY,
                                  inputParams.intraFxTranSigmaZ,
                                  inputParams.intraFxRotSigmaX,
                                  inputParams.intraFxRotSigmaY,
                                  inputParams.intraFxRotSigmaZ,
                                  inputParams.numFrac,
                                  inputParams.numSimulation,
                                  inputParams.numSubFx,
                                  inputParams.trialName,
                                  inputParams.planName,
                                  inputParams.debug);
    }
    else if (inputParams.inputMode == NRRD)
    {

    }

    //app.exec();
}
