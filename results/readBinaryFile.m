function data = readBinaryFile(FileName)
fid = fopen(FileName,'r','l');
data= fread(fid,'double','l');
fclose(fid);