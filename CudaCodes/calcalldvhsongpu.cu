
// thrust header
#include <cuda.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
//// https://code.google.com/p/thrust/wiki/QuickStartGuide

#include <omp.h>

extern "C"  void interp3(
        float * vOutput,
        const int     nQueryPoints,
        const int     xSize,
        const int     ySize,
        const int     zSize,
        const float * gridX,
        const float * gridY,
        const float * gridZ,
        const float * vGrid,
        const float * xq,
        const float * yq,
        const float * zq);

extern "C" void negateAll(float *vectorToNegate, int vecSize);
extern "C" void dvhCalcGPU(float *dvhs, float *doseBins, const int EachBinLength, const float *dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum);
extern "C" void allocateMemOnDevice(float* d_allocated, size_t numElement);



typedef thrust::device_vector<float> dVec;
typedef dVec *pointerDVec;
void allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPoints(std::vector<float *> &d_inPolygonPointsZforROIiRawPointerVec, std::vector<pointerDVec> &d_inPolygonPointsXforROIiPointerVec, std::vector<float *> &d_doseGridYvaluesRawPointerVec, std::vector<float *> &d_doseGridXvaluesRawPointerVec, std::vector<pointerDVec> &d_doseGridXvaluesPointerVec, std::vector<float *> &d_inPolygonPointsXforROIiRawPointerVec, std::vector<pointerDVec> &d_inPolygonPointsYforROIiPointerVec, std::vector<pointerDVec> &d_doseArraryPointerVec, std::vector<pointerDVec> &d_doseGridYvaluesPointerVec, std::vector<pointerDVec> &d_rotationMatricesPointerVec, std::vector<float *> &d_doseArrayRawPointerVec, std::vector<pointerDVec> &d_translationVectorsPointerVec, std::vector<pointerDVec> &d_inPolygonPointsZforROIiPointerVec, std::vector<pointerDVec> &d_doseGridZvaluesPointerVec, int iRoi, std::vector<float *> &d_doseGridZvaluesRawPointerVec, std::vector<float *> &d_inPolygonPointsYforROIiRawPointerVec, std::vector<float *> &d_rotationMatricesRawPointerVec, std::vector<float *> &d_translationVectorsRawPointerVec);

// test Functions
void testInterp3GPU();
void testDvhCalcGPU();


void testDvhCalcGPU()
{
    std::string class_member = "RobustnessAnalyzer::testDvhCalcGPU:" ;
    logstream << class_member <<  "------------------Testing DvhCalGPU---------------" << std::endl;
    // dose_in ==> size = NumPtsInEachSet * numDoseSets
    // doseBins => size = numDoseBins* numDoseSets
    // NumPtsInEachSet
    // DoseBinWidth
    // dvhCalcGPU(int *binedDoses, const int numDoseBins, const float *dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets);
    int numDoseSets = 3;
    float doseMax = 100;
    float _doseBinWidth = 2.0;
    // std::vector<double> oneSetDose = generateRange(_doseBinWidth / 2, .1, doseMax);
    std::vector<double> oneSetDose = { 1, 5, 10, 99.8, 99.99, 100 };
    int numDoseBins_ = 50;
    int NumPtsInEachSet = oneSetDose.size();
    std::vector<double> dose_in;
    for (size_t i = 0; i < numDoseSets; i++)
    {
        dose_in.insert(dose_in.end(), oneSetDose.begin(), oneSetDose.end());
    }

    thrust::device_vector<float> d_dose_in(dose_in.begin(), dose_in.end());
    float* d_dose_inRawPointer = thrust::raw_pointer_cast(&d_dose_in[0]);


    //thrust::host_vector<int> binedDoses(numDoseBins_*numDoseSets, 0);
    //thrust::device_vector<int> d_binedDoses = binedDoses;
    //int* d_binedDosesRawPointer = thrust::raw_pointer_cast(&d_binedDoses[0]);

    std::vector<float> binedDoses(numDoseBins_*numDoseSets, 0);
    float* binedDosesRawPointer = &binedDoses[0];

    std::vector<float> dvhs(numDoseBins_*numDoseSets, 0);
    float* dvhsRawPointer = &dvhs[0];
    int devNum=0;
    dvhCalcGPU(dvhsRawPointer,binedDosesRawPointer, numDoseBins_, d_dose_inRawPointer, _doseBinWidth, NumPtsInEachSet, numDoseSets,devNum);

    logstream << class_member <<  ":d_binedDoses[0]  = " << binedDoses[0] << std::endl;
    logstream << class_member <<  ":d_binedDoses[numDoseBins*numDoseSets-1]  = " << binedDoses[numDoseBins_*numDoseSets - 2] << std::endl;
    std::string filePathToWrite= "./testDvhCalcOutput.bin";

    writeVectorToBinaryFileQT(filePathToWrite,dvhs);
    std::string filePathToWriteBinedDoses= "./testBinedDoseOutput.bin";
    writeVectorToBinaryFileQT(filePathToWriteBinedDoses,binedDoses);
}

// Test functions --> used to verify the functionality of the software
void testInterp3GPU()
{
    std::string class_member = "RobustnessAnalyzer::testInterp3:" ;
    logstream << class_member <<  "------------------Testing Interp3 on GPU---------------" << std::endl;

    // https://www.youtube.com/watch?v=kUKQ9s7R6WE gpu debugging
    std::vector<double> Xgrid = generateRange(-5.0, 1, 5);
    std::vector<double> Ygrid = generateRange(0, 1, 10);
    std::vector<double> Zgrid = generateRange(-10, 1, 0);
    std::vector<double> XmeshGridPts(Xgrid.size()*Ygrid.size()*Zgrid.size(), 0);
    std::vector<double> YmeshGridPts(Xgrid.size()*Ygrid.size()*Zgrid.size(), 0);
    std::vector<double> ZmeshGridPts(Xgrid.size()*Ygrid.size()*Zgrid.size(), 0);
    std::vector<double> VmeshGridPts(Xgrid.size()*Ygrid.size()*Zgrid.size(), 0);

    //xyzMeshGrid(Xgrid, Ygrid, Zgrid, XmeshGridPts, YmeshGridPts, ZmeshGridPts);

    for (size_t i = 0; i < XmeshGridPts.size(); i++)
    {
        // VmeshGridPts[i] = (XmeshGridPts[i] + YmeshGridPts[i])*(XmeshGridPts[i] + YmeshGridPts[i]) + ZmeshGridPts[i];
        VmeshGridPts[i] = (XmeshGridPts[i] + YmeshGridPts[i]) * 5 + ZmeshGridPts[i];
    }


    thrust::device_vector<float> d_Xgrid(Xgrid.begin(), Xgrid.end());
    thrust::device_vector<float> d_Ygrid(Ygrid.begin(), Ygrid.end());
    thrust::device_vector<float> d_Zgrid(Zgrid.begin(), Zgrid.end());
    float* d_XgridRawPointer = thrust::raw_pointer_cast(&d_Xgrid[0]);
    float* d_YgridRawPointer = thrust::raw_pointer_cast(&d_Ygrid[0]);
    float* d_ZgridRawPointer = thrust::raw_pointer_cast(&d_Zgrid[0]);



    thrust::device_vector<float> d_XmeshGridPts(XmeshGridPts.begin(), XmeshGridPts.end());
    thrust::device_vector<float> d_YmeshGridPts(YmeshGridPts.begin(), YmeshGridPts.end());
    thrust::device_vector<float> d_ZmeshGridPts(ZmeshGridPts.begin(), ZmeshGridPts.end());
    thrust::device_vector<float> d_VmeshGridPts(VmeshGridPts.begin(), VmeshGridPts.end());
    float* d_XmeshGridRawPointer = thrust::raw_pointer_cast(&d_XmeshGridPts[0]);
    float* d_YmeshGridRawPointer = thrust::raw_pointer_cast(&d_YmeshGridPts[0]);
    float* d_ZmeshGridRawPointer = thrust::raw_pointer_cast(&d_ZmeshGridPts[0]);
    float* d_VmeshGridRawPointer = thrust::raw_pointer_cast(&d_VmeshGridPts[0]);

    // make query points vectors on host and device
    thrust::host_vector<float> Xq(3, 0);
    thrust::host_vector<float> Yq(3, 0);
    thrust::host_vector<float> Zq(3, 0);
    thrust::host_vector<float> Vq(3, 0);
    thrust::device_vector<float> d_Xq = Xq;
    thrust::device_vector<float> d_Yq = Yq;
    thrust::device_vector<float> d_Zq = Zq;
    thrust::device_vector<float> d_Vq = Vq;
    float* d_XqRawPointer = thrust::raw_pointer_cast(&d_Xq[0]);
    float* d_YqRawPointer = thrust::raw_pointer_cast(&d_Yq[0]);
    float* d_ZqRawPointer = thrust::raw_pointer_cast(&d_Zq[0]);
    float* d_VqRawPointer = thrust::raw_pointer_cast(&d_Vq[0]);
    d_Xq[0] = -1.5;
    d_Yq[0] = 11;
    d_Zq[0] = -5;

    //for (size_t i = 0; i < d_Xgrid.size(); i++)
    //{
    //	cout << "d_Xgrid[" << i << "] = " << d_Xgrid[i] << std::endl;
    //}

    // note that Xgrid, Ygrid and Zgrid should be monotonically increasing
    // if for instance Xgrid is not monotonically increasing, we can negate Xgrid and d_Xq values

    interp3(d_VqRawPointer, d_Vq.size(),
            Xgrid.size(),
            Ygrid.size(),
            Zgrid.size(),
            d_XgridRawPointer, d_YgridRawPointer, d_ZgridRawPointer, d_VmeshGridRawPointer,
            d_XqRawPointer, d_YqRawPointer, d_ZqRawPointer);

    logstream << "d_Xq[0] = " << d_Xq[0] << ", d_Yq[0] = " << d_Yq[0] << ", d_Zq[0] = " << d_Zq[0] << std::endl;
    logstream << "d_Vq[0] = " << d_Vq[0] << std::endl;

    if (d_Vq[0] != d_Vq[0]) // TODO: This is how to check if a variable is  NAN  ref : http://stackoverflow.com/questions/570669/checking-if-a-double-or-float-is-nan-in-c
    {

        logstream << "The point d_Vq[0] is outside the grid box" << std::endl;
    }



}


void allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPoints(std::vector<float*>& d_inPolygonPointsZforROIiRawPointerVec,
                                                                                            std::vector<pointerDVec>& d_inPolygonPointsXforROIiPointerVec, std::vector<float*>& d_doseGridYvaluesRawPointerVec, std::vector<float*> &d_doseGridXvaluesRawPointerVec,
                                                                                            std::vector<pointerDVec>& d_doseGridXvaluesPointerVec, std::vector<float*>& d_inPolygonPointsXforROIiRawPointerVec, std::vector<pointerDVec> &d_inPolygonPointsYforROIiPointerVec,
                                                                                            std::vector<pointerDVec>& d_doseArraryPointerVec, std::vector<pointerDVec>& d_doseGridYvaluesPointerVec, std::vector<pointerDVec> &d_rotationMatricesPointerVec,
                                                                                            std::vector<float*> &d_doseArrayRawPointerVec, std::vector<pointerDVec> &d_translationVectorsPointerVec, std::vector<pointerDVec> &d_inPolygonPointsZforROIiPointerVec,
                                                                                            std::vector<pointerDVec>& d_doseGridZvaluesPointerVec, int iRoi, std::vector<float*> &d_doseGridZvaluesRawPointerVec, std::vector<float*>& d_inPolygonPointsYforROIiRawPointerVec,
                                                                                            std::vector<float*>& d_rotationMatricesRawPointerVec,std::vector<float*> &d_translationVectorsRawPointerVec)
{
    std::string class_member = "RobustnessAnalyzer::allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPoints:" ;

    int cntQualifiedDevices=-1;
    for (int idev = 0; idev < numGPUs; idev++)
    {
        if (IsDeviceSelectedForComputation[idev])
        {
            cntQualifiedDevices ++;
            cudaSetDevice(idev);
            //            if (iRoi==0) // TODO : Notice we don't need to delete dose information from devices we can do this when iRoi==numberRoi
            //            {
            d_doseArraryPointerVec[cntQualifiedDevices] = new dVec(rtDose->doseArray.begin(), rtDose->doseArray.end());
            d_doseGridXvaluesPointerVec[cntQualifiedDevices] = new dVec(rtDose->doseGridXvalues.begin(), rtDose->doseGridXvalues.end());
            d_doseGridYvaluesPointerVec[cntQualifiedDevices] = new dVec(rtDose->doseGridYvalues.begin(), rtDose->doseGridYvalues.end());
            d_doseGridZvaluesPointerVec[cntQualifiedDevices] = new dVec(rtDose->doseGridZvalues.begin(), rtDose->doseGridZvalues.end());
            //            }

            // we need to creat the following resources for each ROI and delete them before doing calculation on the next ROI
            d_inPolygonPointsXforROIiPointerVec[cntQualifiedDevices] = new dVec(rtStructure->roiVector[iRoi].inPolygonPointsX.begin(), rtStructure->roiVector[iRoi].inPolygonPointsX.end());
            d_inPolygonPointsYforROIiPointerVec[cntQualifiedDevices] = new dVec(rtStructure->roiVector[iRoi].inPolygonPointsY.begin(), rtStructure->roiVector[iRoi].inPolygonPointsY.end());
            d_inPolygonPointsZforROIiPointerVec[cntQualifiedDevices] = new dVec(rtStructure->roiVector[iRoi].inPolygonPointsZ.begin(), rtStructure->roiVector[iRoi].inPolygonPointsZ.end());
            d_translationVectorsPointerVec[cntQualifiedDevices] = new dVec(simulationModelParams.at(iRoi).TranslationVectors.begin(), simulationModelParams.at(iRoi).TranslationVectors.end());
            d_rotationMatricesPointerVec[cntQualifiedDevices] = new dVec(simulationModelParams.at(iRoi).RotationMatrices.begin(), simulationModelParams.at(iRoi).RotationMatrices.end());

            //thrust::copy_n(d_translationVectorsPointerVec[cntQualifiedDevices]->data(), 10, std::ostream_iterator<float>(std::cout, ","));            //                thrust::device_vector<float> d_doseArrary(rtDose->doseArray.begin(), rtDose->doseArray.end());// Note: doseArray is saved in row-major order


            //                //thrust::device_vector<float> d_doseArrayCoordinateX(doseArrayCoordinatesX.begin(), doseArrayCoordinatesX.end());
            //                //thrust::device_vector<float> d_doseArrayCoordinateY(doseArrayCoordinatesY.begin(), doseArrayCoordinatesY.end());
            //                //thrust::device_vector<float> d_doseArrayCoordinateZ(doseArrayCoordinatesZ.begin(), doseArrayCoordinatesZ.end());
            //                thrust::device_vector<float> d_doseGridXvalues(rtDose->doseGridXvalues.begin(), rtDose->doseGridXvalues.end());
            //                thrust::device_vector<float> d_doseGridYvalues(rtDose->doseGridYvalues.begin(), rtDose->doseGridYvalues.end());
            //                thrust::device_vector<float> d_doseGridZvalues(rtDose->doseGridZvalues.begin(), rtDose->doseGridZvalues.end());

            //                thrust::device_vector<float> d_inPolygonPointsXforROIi(rtStructure->roiVector[iRoi].inPolygonPointsX.begin(), rtStructure->roiVector[iRoi].inPolygonPointsX.end());
            //                thrust::device_vector<float> d_inPolygonPointsYforROIi(rtStructure->roiVector[iRoi].inPolygonPointsY.begin(), rtStructure->roiVector[iRoi].inPolygonPointsY.end());
            //                thrust::device_vector<float> d_inPolygonPointsZforROIi(rtStructure->roiVector[iRoi].inPolygonPointsZ.begin(), rtStructure->roiVector[iRoi].inPolygonPointsZ.end());


            //                const int N = 100;
            //                thrust::device_ptr<int> int_array = thrust::device_malloc<int>(N);
            //                // deallocate with device_free
            //                thrust::device_free(int_array);
            //                dbl2* rawPointerExample = thrust::raw_pointer_cast(&int_array[0]);


            // getting raw pointer of each vector defined on device
            // float* d_doseArrayRawPointer = thrust::raw_pointer_cast(&d_doseArrary[0]);
            d_doseArrayRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseArraryPointerVec[cntQualifiedDevices]->data());

            d_doseGridXvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridXvaluesPointerVec[cntQualifiedDevices]->data());
            d_doseGridYvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data());
            d_doseGridZvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsXforROIiRawPointerVec[cntQualifiedDevices]= thrust::raw_pointer_cast(d_inPolygonPointsXforROIiPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsYforROIiRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_inPolygonPointsYforROIiPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsZforROIiRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_inPolygonPointsZforROIiPointerVec[cntQualifiedDevices]->data());
            d_rotationMatricesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_rotationMatricesPointerVec[cntQualifiedDevices]->data());
            d_translationVectorsRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_translationVectorsPointerVec[cntQualifiedDevices]->data());




            //               //float* d_doseArrayCoordinateXRawPointer = thrust::raw_pointer_cast(&d_doseArrayCoordinateX[0]);
            //               //float* d_doseArrayCoordinateYRawPointer = thrust::raw_pointer_cast(&d_doseArrayCoordinateY[0]);
            //              //float* d_doseArrayCoordinateZRawPointer = thrust::raw_pointer_cast(&d_doseArrayCoordinateZ[0]);
            //              float* d_doseGridXvaluesRawPointer = thrust::raw_pointer_cast(&d_doseGridXvalues[0]);
            //                float* d_doseGridYvaluesRawPointer = thrust::raw_pointer_cast(&d_doseGridYvalues[0]);
            //                float* d_doseGridZvaluesRawPointer = thrust::raw_pointer_cast(&d_doseGridZvalues[0]);

            //                float* d_inPolygonPointsXforROIiRawPointer = thrust::raw_pointer_cast(&d_inPolygonPointsXforROIi[0]);
            //                float* d_inPolygonPointsYforROIiRawPointer = thrust::raw_pointer_cast(&d_inPolygonPointsYforROIi[0]);
            //                float* d_inPolygonPointsZforROIiRawPointer = thrust::raw_pointer_cast(&d_inPolygonPointsZforROIi[0]);

            // now get the raw pointer of a chunck of transformation vectors (translationVectors + rotationMatrices ) that we want to perform the transformation for
            // we perform  as many as transformations that is could fit in the Device.
            // float* d_rotationMatricesRawPointer = thrust::raw_pointer_cast(&d_rotationMatrices[numPerformedTransformation * 9]);     // transformationIdx*9
            // float* d_translationVectorsRawPointer = thrust::raw_pointer_cast(&d_translationVectors[numPerformedTransformation * 3]); // transformationIdx*3


            //logstream << " Test is rtDose->doseGridXvalues = " <<  rtDose->doseGridXvalues.size() << "and d_doseGridXvaluesPointerVec[cntQualifiedDevices]->size() = "  << d_doseGridXvaluesPointerVec[cntQualifiedDevices]->size() << std::endl;
            // Make sure that grid values are monotonically increasing
            // This part is done because interp requires monotonically increasing value


            // To use access the device_vector we need to dererence the pointer to device_vector first
            //                 thrust::device_vector<float> dx=  *d_doseGridXvaluesPointerVec[cntQualifiedDevices];
            //                 for (int var = 0; var < d_doseGridXvaluesPointerVec[cntQualifiedDevices]->size(); ++var)
            //                 {
            //                    logstream << "v3[" << var << "] = " <<dx[var] << std::endl;
            //                 }

            if ((rtDose->doseGridXvalues[rtDose->doseGridXvalues.size() - 1] -rtDose->doseGridXvalues[0]) < 0)
            {
                //thrust::transform(d_doseGridXvalues.begin(), d_doseGridXvalues.end(), d_doseGridXvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsXforROIi.begin(), d_inPolygonPointsXforROIi.end(), d_inPolygonPointsXforROIi.begin(), thrust::negate<float>());
                // https://groups.google.com/forum/#!topic/thrust-users/1ttLbwE3H5Q
                negateAll(d_doseGridXvaluesRawPointerVec[cntQualifiedDevices], d_doseGridXvaluesPointerVec[cntQualifiedDevices]->size());
                //negateAll(d_inPolygonPointsXforROIiRawPointer, d_inPolygonPointsXforROIi.size());


            }

            if ((rtDose->doseGridYvalues[rtDose->doseGridYvalues.size() - 1] - rtDose->doseGridYvalues[0]) < 0)
            {
                //thrust::transform(d_doseGridYvalues.begin(), d_doseGridYvalues.end(), d_doseGridYvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsYforROIi.begin(), d_inPolygonPointsYforROIi.end(), d_inPolygonPointsYforROIi.begin(), thrust::negate<float>());
                //std::cout << "\nbefor negation:" << std::endl;
                //   logstream << class_member <<  "\n before negation applied d_doseGridYvaluesPointerVec values of 200 pts :  " << std::endl;

                // thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));


                negateAll(d_doseGridYvaluesRawPointerVec[cntQualifiedDevices],  d_doseGridYvaluesPointerVec[cntQualifiedDevices]->size());
                //negateAll(d_inPolygonPointsYforROIiRawPointer, d_inPolygonPointsYforROIi.size());
                //std::cout << "\nAfter negation:" << std::endl;
                // thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(), 10, std::ostream_iterator<float>(std::cout, ","));

                //  logstream << class_member <<  "\n after negation applied d_doseGridYvaluesPointerVec values of 200 pts :  " << std::endl;

                //   thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));




            }
            if ((rtDose->doseGridZvalues[rtDose->doseGridZvalues.size() - 1] - rtDose->doseGridZvalues[0]) < 0)
            {
                //logstream << class_member <<  "\n before negation applied d_doseGridZvaluesPointerVec values of 200 pts :  " << std::endl;

                //thrust::copy_n(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));

                //thrust::transform(d_doseGridZvalues.begin(), d_doseGridZvalues.end(), d_doseGridZvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsZforROIi.begin(), d_inPolygonPointsZforROIi.end(), d_inPolygonPointsZforROIi.begin(), thrust::negate<float>());
                negateAll(d_doseGridZvaluesRawPointerVec[cntQualifiedDevices],  d_doseGridZvaluesPointerVec[cntQualifiedDevices]->size());
                //negateAll(d_inPolygonPointsZforROIiRawPointer, d_inPolygonPointsZforROIi.size());
                // logstream << class_member <<  "\n after negation applied d_doseGridZvaluesPointerVec values of 200 pts :  " << std::endl;
                //   thrust::copy_n(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));


            }
        }// (IsDeviceSelectedForComputation[idev])
    }
}

void testSumUpFractionDosesOnGPU()
{
    std::string class_member = "RobustnessAnalyzer::testSumUpFractionDosesOnGPU:" ;
    logstream << class_member <<  "------------------Testing sumUpFractionDosesOnGPU---------------" << std::endl;
    // sumUpFractionDosesOnGPU(float *d_doseOut,
    // const float *d_doseIn,
    // const int fractionsNum,
    // const float *fractionDoseWeights,
    // const int numDoseSets,
    // const int NumoFDosePtsInEachDoseSet,
    // const int devNum);

    // dose_in ==> size = NumPtsInEachSet * numDoseSets
    int numTrial=2;
    int numFrac = 3;
    int numDoseSet = numTrial*numFrac;

    // std::vector<double> oneSetDose = generateRange(_doseBinWidth / 2, .1, doseMax);
    std::vector<float> oneSetDose = generateFloatRange(.05, .1, 1000);//{ 1, 2, 3,4};
    std::vector<float> tempDose(oneSetDose.size(),0);


    int NumPtsInEachSet = oneSetDose.size();
    std::vector<float> dose_in;
    for (int iTrial = 0; iTrial < numTrial; ++iTrial)
    {
        std::transform (oneSetDose.begin(), oneSetDose.end(), tempDose.begin(), [iTrial](float d) -> float{ return d + 10.0*(float)iTrial; });


        for (int iFrac = 0; iFrac < numFrac; ++iFrac)
        {
            std::transform (tempDose.begin(), tempDose.end(), tempDose.begin(), [iFrac](float d) -> float{ return d + (float) iFrac; });
            dose_in.insert(dose_in.end(), tempDose.begin(), tempDose.end());
        }

    }
    std::vector<float> fractionDoseWeights(numFrac,1/static_cast<float>(numFrac));

    //float* fractionDoseWeights= new float[numFrac];
    //for (int iFrac = 0; iFrac < numFrac; ++iFrac)
    //{
    //        fractionDoseWeights[iFrac]=1/(float) numFrac;
    //  }

    thrust::device_vector<float> d_dose_in(dose_in.begin(), dose_in.end());
    float* d_dose_inRawPointer = thrust::raw_pointer_cast(&d_dose_in[0]);
    std::cout<< "d_dose_in = " << std::endl;
    if (numTrial*NumPtsInEachSet>1000)
    {
        thrust::copy_n(d_dose_in.data(), 1000, std::ostream_iterator<float>(std::cout, ","));
    }
    else
    {
        thrust::copy_n(d_dose_in.data(), numDoseSet*NumPtsInEachSet, std::ostream_iterator<float>(std::cout, ","));

    }
    thrust::host_vector<float> dose_out(numTrial*NumPtsInEachSet);

    thrust::device_vector<float> d_dose_out = dose_out;
    float* d_dose_outRawPointer = thrust::raw_pointer_cast(&d_dose_out[0]);
    int devNum=0;
    sumUpFractionDosesOnGPU(d_dose_outRawPointer,
                            d_dose_inRawPointer,
                            numFrac,
                            &fractionDoseWeights[0],
            numDoseSet,
            NumPtsInEachSet,
            devNum);

    std::cout<< "d_dose_out = " << std::endl;
    if (numTrial*NumPtsInEachSet>1000)
    {
        thrust::copy_n(d_dose_out.data(), 100, std::ostream_iterator<float>(std::cout, ","));
    }
    else
    {
        thrust::copy_n(d_dose_out.data(), numTrial*NumPtsInEachSet, std::ostream_iterator<float>(std::cout, ","));
    }

}

//void calcALLDvhsHere()
//{

//    std::vector<float> fractionsDoseWeights(numFractions,1/static_cast<float>(numFractions));


//    // Memory required to store roiModel.inPolygonPoints into GPU for each ROI

//    for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); iRoi++)
//    {

//        logstream << class_member << "Calculating memory requirement for ROI " << iRoi << " On GPU (float each number 4 bytes)" << std::endl;
//        auto numNominalQueryPts = rtStructure->roiVector[iRoi].inPolygonPointsX.size();

//        auto memoryForNominalQueryinBytes = numNominalQueryPts * 3 * 4; // each points needs three
//        auto memoryForNominalQueryPtsinMB = memoryForNominalQueryinBytes / 1024.0 / 1024.0;
//        auto memoryRequiredForDoseMatrixinMB = rtDose->doseArray.size() * 4 * 4 / 1024 / 1024;  // 4 bytes times 4 number for each pt (x,v,z,v) note x,y,z are in
//        auto memoryRequiredForBinedDoseAfterTransformation = numDoseBins*numTrials*numFractions * 4 / 1024 / 1024;  //  Make a vector with this size at Host side  then transfer all device date to this host vector

//        { // this bracket to tell compiler to clean up tempBinDoseVec
//            //            std::vector<int> tempBinDoseVec(numDoseBins*numTrials*numFractions, 0);
//            //            diffDVHsForTransformedPts.push_back(tempBinDoseVec);
//            //            std::vector<float> tempdvhs(numDoseBins*numTrials*numFractions, 0);
//            //            cDVHForTransformedPts.push_back(tempdvhs);

//            std::vector<float> tempTrailDvhs(numDoseBins*numTrials, 0);
//            cDVHForTrial.push_back(tempTrailDvhs);

//            std::vector<float> tempDiffDvhs(numDoseBins*numTrials, 0);
//            diffDVHForTrial.push_back(tempDiffDvhs);
//        }
//        size_t minmunMemRequiredwith = 20 * memoryForNominalQueryPtsinMB * 1024 * 1024 + memoryRequiredForDoseMatrixinMB * 1024 * 1024;  // to be safe and probably be benificial in terms of performance, we put a minimum required memory for GPU , here is 20*memoryrequired for query pts + Memory required for dose coordinates and its values (x,y,z,v)

//        if (!checkAvailableMemoryOnDevices())
//        {
//            logstream << class_member << " not enough GPU memory for viable speed up computation. Use CPU implementaion instead." << std::endl;
//            return false;
//        }

//        // Select the Devices for computation which have avialable memory more than minmunMemRequiredwith
//        std::vector<bool> temp2(numGPUs,false);
//        IsDeviceSelectedForComputation= temp2; //=   TODO: to be changed in future
//        for (int idev = 0; idev < numGPUs; ++idev) {

//            if (freeMemOnEachDevice[idev]>minmunMemRequiredwith)
//            {
//                IsDeviceSelectedForComputation[idev] = true;
//                logstream << class_member <<  "device " << idev << " is selected for computation." << std::endl;
//            }
//        }



//        // http://cs.nyu.edu/courses/spring12/CSCI-GA.3033-012/lecture9.pdf
//        // http://www3.cs.stonybrook.edu/~mueller/teaching/cse591_GPU/S3465-Multi-GPU-Programming.pdf
//        // memory allocation on the selected device
//        //the following fails -->  https://github.com/thrust/thrust/issues/526
//        //        typedef thrust::device_vector<float> d_Vec;
//        //        typedef d_Vec *p_vec;
//        //        std::vector<p_vec> vRes;

//        //        for (unsigned int idev=0; idev< numGPUs;idev++)
//        //        {
//        //            cudaSetDevice(idev);
//        //            p_vec hvConscience = new d_Vec(1024);
//        //            vRes.push_back(hvConscience);

//        //        }


//        // TODO: consider gpu to gpu memory transfer for other devices http://stackoverflow.com/questions/628041/how-to-copy-memory-between-different-gpus-in-cuda
//        numQualifiedDevices= std::count_if (IsDeviceSelectedForComputation.begin(), IsDeviceSelectedForComputation.end(), [](bool a){ return a==true;});
//        transformationJobDistributionVec.clear();
//        indicesOfQualifiedDevices.clear();
//        for (unsigned int iDev = 0; iDev < IsDeviceSelectedForComputation.size(); ++iDev) {
//            if (IsDeviceSelectedForComputation[iDev])
//            {
//                indicesOfQualifiedDevices.push_back(iDev);
//                transformationJobDistributionVec.push_back(std::vector<unsigned int>(0));
//            }
//        }



//        std::vector<pointerDVec> d_doseArraryPointerVec(numQualifiedDevices); // every single pointer might point to resources on a different device
//        std::vector<float*> d_doseArrayRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_doseGridXvaluesPointerVec(numQualifiedDevices);
//        std::vector<float*> d_doseGridXvaluesRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_doseGridYvaluesPointerVec(numQualifiedDevices);
//        std::vector<float*> d_doseGridYvaluesRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_doseGridZvaluesPointerVec(numQualifiedDevices);
//        std::vector<float*> d_doseGridZvaluesRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_inPolygonPointsXforROIiPointerVec(numQualifiedDevices);
//        std::vector<float*> d_inPolygonPointsXforROIiRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_inPolygonPointsYforROIiPointerVec(numQualifiedDevices);
//        std::vector<float*> d_inPolygonPointsYforROIiRawPointerVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_inPolygonPointsZforROIiPointerVec(numQualifiedDevices);
//        std::vector<float*> d_inPolygonPointsZforROIiRawPointerVec(numQualifiedDevices);

//        // will get the raw pointer of a chunck of transformation vectors (translationVectors + rotationMatrices ) that we want to perform the transformation for
//        // we perform  as many as transformations that is could fit in the Device.
//        //   float* d_rotationMatricesRawPointer = thrust::raw_pointer_cast(&d_rotationMatrices[numPerformedTransformation * 9]);     // transformationIdx*9
//        // float* d_translationVectorsRawPointer = thrust::raw_pointer_cast(&d_translationVectors[numPerformedTransformation * 3]); // transformationIdx*3

//        std::vector<pointerDVec> d_translationVectorsPointerVec(numQualifiedDevices);
//        std::vector<float*>  d_translationVectorsRawPointerVec(numQualifiedDevices);
//        std::vector<pointerDVec> d_rotationMatricesPointerVec(numQualifiedDevices);
//        std::vector<float*>  d_rotationMatricesRawPointerVec(numQualifiedDevices);


//        // std::vector<thrust::device_ptr<float>> d_transformedPtsXVec(numQualifiedDevices);
//        //std::vector<thrust::device_ptr<float>> d_transformedPtsYVec(numQualifiedDevices);
//        // std::vector<thrust::device_ptr<float>> d_transformedPtsZVec(numQualifiedDevices);
//        // std::vector<thrust::device_ptr<float>> d_DoseValueTransformedPtsVec(numQualifiedDevices);

//        std::vector<pointerDVec> d_transformedPtsXVec(numQualifiedDevices);
//        std::vector<pointerDVec> d_transformedPtsYVec(numQualifiedDevices);
//        std::vector<pointerDVec> d_transformedPtsZVec(numQualifiedDevices);
//        std::vector<pointerDVec> d_DoseValueTransformedPtsVec(numQualifiedDevices);
//        std::vector<pointerDVec> d_DoseValueForTransformedPtsFractionsCombinedVec(numQualifiedDevices);


//        std::vector<float*> d_transformedPtsXRawPointerVec(numQualifiedDevices);
//        std::vector<float*> d_transformedPtsYRawPointerVec(numQualifiedDevices);
//        std::vector<float*> d_transformedPtsZRawPointerVec(numQualifiedDevices);
//        std::vector<float*> d_DoseValueForTransformedPtsRawPointerVec(numQualifiedDevices);
//        std::vector<float*> d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec(numQualifiedDevices);








//        // memory allocation for dose matrix and its coordinates on selected GPU for computation
//        allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPoints(d_inPolygonPointsZforROIiRawPointerVec, d_inPolygonPointsXforROIiPointerVec,
//                                                                           d_doseGridYvaluesRawPointerVec, d_doseGridXvaluesRawPointerVec, d_doseGridXvaluesPointerVec,
//                                                                           d_inPolygonPointsXforROIiRawPointerVec, d_inPolygonPointsYforROIiPointerVec, d_doseArraryPointerVec,
//                                                                           d_doseGridYvaluesPointerVec, d_rotationMatricesPointerVec, d_doseArrayRawPointerVec,
//                                                                           d_translationVectorsPointerVec, d_inPolygonPointsZforROIiPointerVec, d_doseGridZvaluesPointerVec,
//                                                                           iRoi, d_doseGridZvaluesRawPointerVec, d_inPolygonPointsYforROIiRawPointerVec,
//                                                                           d_rotationMatricesRawPointerVec,d_translationVectorsRawPointerVec);  // for  idev


//        // Check available memory after allocation the memory for inPolygonPoint, dose array, and dose coordinates
//        if (!checkAvailableMemoryOnDevices())
//        {
//            return false;
//        }

//        numberOfPossibleTransfomationInEachKernelLaunch.clear();
//        for (int iQualDev = 0; iQualDev < indicesOfQualifiedDevices.size(); ++iQualDev)
//        {
//            int tempNumTrans = floor(static_cast<double>(freeMemOnEachDevice[indicesOfQualifiedDevices[iQualDev]] / (memoryForNominalQueryPtsinMB * 1024 * 1024 * 4.0 / 3.0)* 0.98));// 0.98 should be adjusted to have a free space on each device to make sure that transformation is done properly.
//            int tempNumTransDividedByNumFrac= tempNumTrans/numFractions;
//            int numTrans_MultipleOfNumOfFractionForEachTrial=tempNumTransDividedByNumFrac*numFractions;
//            numberOfPossibleTransfomationInEachKernelLaunch.push_back(numTrans_MultipleOfNumOfFractionForEachTrial);
//        }

//        // Schedule transformation jobs on the available qualifiled Devices
//        logstream << class_member << "Scheduling tranformation jobs on the available qualified devices for ROI  " << iRoi << std::endl;

//        size_t currentScheduledTranJob=0;
//        while (currentScheduledTranJob<numFractions*numTrials-1)
//        {

//            for (int iQualDev = 0; iQualDev < indicesOfQualifiedDevices.size(); ++iQualDev)
//            {
//                if (numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]>numFractions*numTrials-1 -currentScheduledTranJob)
//                {
//                    transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob); // starting transformation index for   iQualDev
//                    transformationJobDistributionVec[iQualDev].push_back(numFractions*numTrials-1); // end transformation index for  iQualDev
//                    currentScheduledTranJob= numFractions*numTrials;
//                    break;
//                }
//                else
//                {
//                    transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob); // starting transformation index for   iQualDev
//                    transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob + numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]-1); // end transformation index for  iQualDev
//                    currentScheduledTranJob= currentScheduledTranJob + numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]; // Should point to the next undone index
//                }
//            } // iQualDev
//        } // while ends here

//        logstream << class_member << "tranformation jobs are scheduled " << std::endl;

//        // perform transformation jobs
//        cudaError_t cudaStatus;
//        omp_set_num_threads(transformationJobDistributionVec.size());  // transformationJobDistributionVec.size() gives us number of qualified gpu devices

//        //http://stackoverflow.com/questions/21616395/multi-gpu-cuda-thrust
//        for (int iBatchLaunch = 0; iBatchLaunch < transformationJobDistributionVec[0].size()/2; ++iBatchLaunch)
//        {

//#pragma omp parallel
//            {
//                unsigned int iQualDev = omp_get_thread_num();
//                if (transformationJobDistributionVec[iQualDev].size()>iBatchLaunch*2)
//                {
//                    cudaStatus= cudaSetDevice(indicesOfQualifiedDevices[iQualDev]);
//                    if (cudaStatus != cudaSuccess) {
//                        logstream <<class_member << "cudaSetDevice failed! or " << indicesOfQualifiedDevices[iQualDev] <<  "  Do you have a CUDA-capable GPU installed?" << std::endl;
//                        //  return false;
//                    }
//                    //  numberOfPossibleTransfomationInEachKernelLaunch is a multiple of numFractions so startIndexOfTransformationsToLaunch is also a multiple of numFractions
//                    auto startIndexOfTransformationsToLaunch= transformationJobDistributionVec[iQualDev][iBatchLaunch*2];
//                    auto stopIndexOfTransformationsToLaunch= transformationJobDistributionVec[iQualDev][iBatchLaunch*2+1];


//                    // create the resultant tranformation vectors  on device along with its device_ptr
//                    size_t numTransToLaunch=stopIndexOfTransformationsToLaunch-startIndexOfTransformationsToLaunch+1;
//                    if (numTransToLaunch%numFractions!=0)
//                    {
//                        logstream << "ERROR: number of Transformations is expected to be multiple of number of Fractions" << std::endl;
//                        // return false;
//                    }
//                    const size_t numElem = numTransToLaunch*rtStructure->roiVector.at(iRoi).inPolygonPointsX.size();
//                    const size_t numOfTrialForThisLaunch  = numTransToLaunch/numFractions;
//                    const size_t numOfElemAfterSummingUpFractionDosesForThisLaunch  = numOfTrialForThisLaunch*rtStructure->roiVector.at(iRoi).inPolygonPointsX.size();

//                    thrust::host_vector<float> tempArray1(numElem); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu
//                    //thrust::device_ptr<float> dp = thrust::device_malloc<float>(numElem);
//                    //thrust::device_vector<float> v3(dp, dp + numElem);
//                    thrust::host_vector<float> tempArray2(numOfElemAfterSummingUpFractionDosesForThisLaunch); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu

//                    // thrust::copy_n(d_transformedPtsXVec[iQualDev]->data(),10,std::ostream_iterator<float>(std::cout, ","));
//                    d_transformedPtsXVec[iQualDev] = new dVec(tempArray1); // new dVec(tempArray1);
//                    d_transformedPtsYVec[iQualDev] = new dVec(tempArray1);
//                    d_transformedPtsZVec[iQualDev] = new dVec(tempArray1);
//                    d_DoseValueTransformedPtsVec[iQualDev] = new dVec(tempArray1);
//                    d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev] = new dVec(tempArray2);

//                    d_transformedPtsXRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsXVec[iQualDev]->data());
//                    d_transformedPtsYRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsYVec[iQualDev]->data());
//                    d_transformedPtsZRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsZVec[iQualDev]->data());
//                    d_DoseValueForTransformedPtsRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_DoseValueTransformedPtsVec[iQualDev]->data());
//                    d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev] = thrust::raw_pointer_cast(d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev]->data());

//                    logstream << class_member << " Applying overall transformation (rotations and translations) from #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch << " for ROI: " << rtStructure->roiVector[iRoi].RoiName  << " on Device " << indicesOfQualifiedDevices[iQualDev]<< std::endl;

//                    launchTransformPts(d_transformedPtsXRawPointerVec[iQualDev],
//                                       d_transformedPtsYRawPointerVec[iQualDev],
//                                       d_transformedPtsZRawPointerVec[iQualDev],
//                                       d_inPolygonPointsXforROIiRawPointerVec[iQualDev],
//                                       d_inPolygonPointsYforROIiRawPointerVec[iQualDev],
//                                       d_inPolygonPointsZforROIiRawPointerVec[iQualDev],
//                                       d_inPolygonPointsXforROIiPointerVec[iQualDev]->size(),
//                                       d_rotationMatricesRawPointerVec[iQualDev]+startIndexOfTransformationsToLaunch * 9,
//                                       d_translationVectorsRawPointerVec[iQualDev]+startIndexOfTransformationsToLaunch * 3,
//                                       numTransToLaunch,
//                                       indicesOfQualifiedDevices[iQualDev]);


//                    // Make sure that grid values are monotonically increasing
//                    if ((rtDose->doseGridXvalues[rtDose->doseGridXvalues.size() - 1] - rtDose->doseGridXvalues[0]) < 0)
//                    {
//                        //thrust::transform(d_doseGridXvalues.begin(), d_doseGridXvalues.end(), d_doseGridXvalues.begin(), thrust::negate<float>());
//                        //thrust::transform(d_inPolygonPointsXforROIi.begin(), d_inPolygonPointsXforROIi.end(), d_inPolygonPointsXforROIi.begin(), thrust::negate<float>());
//                        // https://groups.google.com/forum/#!topic/thrust-users/1ttLbwE3H5Q
//                        negateAll(d_transformedPtsXRawPointerVec[iQualDev], d_transformedPtsXVec[iQualDev]->size());
//                        //negateAll(d_transformedPtsXRawPointerVec[iQualDev], numElem);

//                        //negateAll(d_inPolygonPointsXforROIiRawPointer, d_inPolygonPointsXforROIi.size());


//                    }
//                    if ((rtDose->doseGridYvalues[rtDose->doseGridYvalues.size() - 1] - rtDose->doseGridYvalues[0]) < 0)
//                    {
//                        //thrust::transform(d_doseGridYvalues.begin(), d_doseGridYvalues.end(), d_doseGridYvalues.begin(), thrust::negate<float>());
//                        //thrust::transform(d_inPolygonPointsYforROIi.begin(), d_inPolygonPointsYforROIi.end(), d_inPolygonPointsYforROIi.begin(), thrust::negate<float>());
//                        // cout << "d_transformedPtsY[0] before negation " << d_transformedPtsY[0] << endl;
//                        negateAll(d_transformedPtsYRawPointerVec[iQualDev], d_transformedPtsYVec[iQualDev]->size());
//                        //negateAll(d_transformedPtsYRawPointerVec[iQualDev], numElem);

//                        // cout << "d_transformedPtsY[0] after negation " << d_transformedPtsY[0] << endl;
//                        //negateAll(d_inPolygonPointsYforROIiRawPointer, d_inPolygonPointsYforROIi.size());

//                    }
//                    if ((rtDose->doseGridZvalues[rtDose->doseGridZvalues.size() - 1] - rtDose->doseGridZvalues[0]) < 0)
//                    {
//                        //thrust::transform(d_doseGridZvalues.begin(), d_doseGridZvalues.end(), d_doseGridZvalues.begin(), thrust::negate<float>());
//                        //thrust::transform(d_inPolygonPointsZforROIi.begin(), d_inPolygonPointsZforROIi.end(), d_inPolygonPointsZforROIi.begin(), thrust::negate<float>());
//                        //cout << "d_transformedPtsZ[0] before negation " << d_transformedPtsZ[0] << endl;

//                        negateAll(d_transformedPtsZRawPointerVec[iQualDev], d_transformedPtsZVec[iQualDev]->size());
//                        //negateAll(d_transformedPtsZRawPointerVec[iQualDev], numElem);

//                        //cout << "d_transformedPtsZ[0] after negation " << d_transformedPtsZ[0] << endl;
//                        //negateAll(d_inPolygonPointsZforROIiRawPointer, d_inPolygonPointsZforROIi.size());

//                    }


//                    logstream << class_member << "Calculating dose at tranformed point clouds from transformation #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch << " for ROI: " << rtStructure->roiVector[iRoi].RoiName  << " on Device " << indicesOfQualifiedDevices[iQualDev]<< std::endl;


//                    interp3(d_DoseValueForTransformedPtsRawPointerVec[iQualDev],
//                            numElem,
//                            d_doseGridXvaluesPointerVec[iQualDev]->size(),
//                            d_doseGridYvaluesPointerVec[iQualDev]->size(),
//                            d_doseGridZvaluesPointerVec[iQualDev]->size(),
//                            d_doseGridXvaluesRawPointerVec[iQualDev],
//                            d_doseGridYvaluesRawPointerVec[iQualDev],
//                            d_doseGridZvaluesRawPointerVec[iQualDev],
//                            d_doseArrayRawPointerVec[iQualDev],
//                            d_transformedPtsXRawPointerVec[iQualDev],
//                            d_transformedPtsYRawPointerVec[iQualDev],
//                            d_transformedPtsZRawPointerVec[iQualDev]);

//                    // for debug purpose
//                    if (!(rtStructure->roiVector[iRoi].RoiName).compare("CTV"))
//                    {
//                        //thrust::host_vector<float> tempArray2(d_DoseValueTransformedPtsVec[iQualDev]->size()); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu

//                        std::cout << d_DoseValueTransformedPtsVec[iQualDev]->operator[](0) << std::endl; //TODO: don't forget this operator!
//                        //thrust::copy_n(&tempArray2[0],1,std::ostream_iterator<float>(logstream, ","));
//                        //   thrust::copy_n(&d_transformedPtsYVec[iQualDev][0],1,std::ostream_iterator<float>(logstream, ","));
//                        //  thrust::copy_n(&d_transformedPtsZVec[iQualDev][0],1,std::ostream_iterator<float>(logstream, ","));

//                        //std::cout << "x=" <<  d_transformedPtsXVec[iQualDev][0] << "y=" <<  d_transformedPtsYVec[iQualDev][0] << "z=" <<  d_transformedPtsZVec[iQualDev][0] << std::endl;
//                        //std::cout << "Dose is  = " <<  d_DoseValueTransformedPtsVec[iQualDev][0] << std::endl;
//                    }



//                    // For each trial,  we need to compute the sum of the dose recive by each point in the point could in all the fractions
//                    // d_DoseValueForTransformedPtsRawPointerVec[iQualDev] --> d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev]

//                    sumUpFractionDosesOnGPU(d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev],
//                                            d_DoseValueForTransformedPtsRawPointerVec[iQualDev],
//                                            numFractions,
//                                            &fractionsDoseWeights[0],
//                            numTransToLaunch,
//                            rtStructure->roiVector[iRoi].inPolygonPointsX.size(),
//                            indicesOfQualifiedDevices[iQualDev]);


//                    logstream << class_member << "\n DVH calculation for dose at cloud points from transportation #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch << " for ROI: " << rtStructure->roiVector[iRoi].RoiName  << " on Device " << indicesOfQualifiedDevices[iQualDev]<< std::endl;
//                    dvhCalcGPU(&cDVHForTrial[iRoi][startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
//                            &diffDVHForTrial[iRoi][startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
//                            numDoseBins,
//                            d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev],
//                            doseBinWidth,
//                            rtStructure->roiVector[iRoi].inPolygonPointsX.size(),
//                            numTransToLaunch/numFractions, // we made sure that numTransToLaunch is integer multiple of numFractions --> so this gives number of Trials in this launch
//                            indicesOfQualifiedDevices[iQualDev]);



//                    // if (iQualDev==1)
//                    // {
//                    //            thrust::copy_n(&cDVHForTransformedPts[iRoi][startIndex*numDoseBins],100,std::ostream_iterator<float>(logstream, ","));
//                    // }

//                    delete(d_transformedPtsXVec[iQualDev]);
//                    delete(d_transformedPtsYVec[iQualDev]);
//                    delete(d_transformedPtsZVec[iQualDev]);
//                    delete(d_DoseValueTransformedPtsVec[iQualDev]);
//                    delete(d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev]);

//                    //                    thrust::device_free(d_transformedPtsXVec[iQualDev]);
//                    //                    thrust::device_free(d_transformedPtsYVec[iQualDev]);
//                    //                    thrust::device_free(d_transformedPtsZVec[iQualDev]);
//                    //                    thrust::device_free(d_DoseValueTransformedPtsVec[iQualDev]);



//                } // if transformationJobDistributionVec[iQualDev].size() ends here
//            } // #pragma omp parallel

//        }// iBatchLaunch





//        //        if (iRoi==rtStructure->roiVector.size()) // TODO : Notice we don't need to delete dose information from devices we can do this when iRoi==numberRoi
//        //        {
//        for (int iQualDev = 0; iQualDev < transformationJobDistributionVec.size(); ++iQualDev)
//        {
//            delete(d_doseArraryPointerVec[iQualDev]);
//            delete(d_doseGridXvaluesPointerVec[iQualDev]);
//            delete(d_doseGridYvaluesPointerVec[iQualDev]);
//            delete(d_doseGridZvaluesPointerVec[iQualDev] );
//        }
//        //        }

//        // we need to creat the following resources for each ROI and delete them before doing calculation on the next ROI
//        for (int iQualDev = 0; iQualDev < transformationJobDistributionVec.size(); ++iQualDev)
//        {
//            delete(d_inPolygonPointsXforROIiPointerVec[iQualDev]);
//            delete(d_inPolygonPointsYforROIiPointerVec[iQualDev]);
//            delete(d_inPolygonPointsZforROIiPointerVec[iQualDev]);
//            delete(d_translationVectorsPointerVec[iQualDev]);
//            delete(d_rotationMatricesPointerVec[iQualDev]);
//        }

//    } // iRoi for


//}
