close all force
clear variables
flagPlot =true;

%% 1) read the CTVolume
cTImageInfo=readImageHeader('CTVolumePatientSImageSetNKIVCU^PS^S00^I0002.bin.header');
fid=fopen('CTVolumePatientSImageSetNKIVCU^PS^S00^I0002.bin.img');
CTImageVector= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'uint16','l');%x_dim*y_dim*z_dim
CTImageVolume=reshape(CTImageVector,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]);
fclose(fid);
fileImageXgrid = ['ImageSetXGridData_319_Plan_.bin'];
fileImageYgrid = ['ImageSetYGridData_319_Plan_.bin'];
fileImageZgrid = ['ImageSetZGridData_319_Plan_.bin'];

fid=fopen(fileImageXgrid);
CTgridX= fread(fid,cTImageInfo.x_dim,'double','l');
fclose(fid);

fid=fopen(fileImageYgrid);
CTgridY= fread(fid,cTImageInfo.y_dim,'double','l');
fclose(fid);

fid=fopen(fileImageZgrid);
CTgridZ= fread(fid,cTImageInfo.z_dim,'double','l');
fclose(fid);

% % Here we verified that pixel data from CT and the one that we save with
%   software are the same
% ctVolumePixelData= ['ImageSetPixelData_319_Plan_.bin'];
% fid=fopen(ctVolumePixelData);
% CTVolumePixelDataFromSoftware= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'uint16','l');
% fclose(fid);

%% now we will find the X,Y,Z grid by oomputation
%    for (int ix = 0; ix < imageSet.imageHeader.x_dim; ++ix) {
%        imageSet.volumeXGridDataVectorInCm[ix]= (imageSet.imageHeader.x_start_dicom + (static_cast<float>(ix))*imageSet.imageHeader.x_pixdim);
%    }
%   for (int iy = 0; iy < imageSet.imageHeader.y_dim; ++iy) {
%        imageSet.volumeYGridDataVectorInCm[iy]= (imageSet.imageHeader.y_start_dicom + (static_cast<float>(imageSet.imageHeader.y_dim) -static_cast<float>(iy)-1.0)*imageSet.imageHeader.y_pixdim);
%    }
%    for (int iz = 0; iz < imageSet.imageHeader.z_dim; ++iz) {
%        imageSet.volumeZGridDataVectorInCm[iz]= (imageSet.imageHeader.z_start + (static_cast<float>(iz))*imageSet.imageHeader.z_pixdim);
%    }
CTXgridComputed = cTImageInfo.x_start_dicom + [0:(cTImageInfo.x_dim-1)]'*cTImageInfo.x_pixdim;
CTYgridComputed = cTImageInfo.y_start_dicom + [(cTImageInfo.y_dim-1):-1:0]'*cTImageInfo.y_pixdim;
CTZgridComputed = cTImageInfo.z_start+ [0:(cTImageInfo.z_dim-1)]'*cTImageInfo.z_pixdim;

index=sub2ind([cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim],265,220,1);
[iX,iY,iZ]=ind2sub([cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim],index);
indices2Coordintte = @(ix,iy,iz) [ CTXgridComputed(ix), CTYgridComputed(iy) , CTZgridComputed(iz)];
disp(['CT Value for coorinate --> (X,Y,Z) = ', num2str(indices2Coordintte(iX,iY,iZ)), '--> ', num2str(CTImageVector(index))])
oneVoxelVolume = cTImageInfo.x_pixdim*cTImageInfo.y_pixdim*cTImageInfo.z_pixdim;
disp(['Voxel mass (gr) for coorinate --> (X,Y,Z) = ', num2str(indices2Coordintte(iX,iY,iZ)), '--> ', num2str(CTImageVector(index)/1000*oneVoxelVolume)])







[ctMeshGridY,ctMeshGridX,ctMeshGridZ]= meshgrid(single(CTgridY),single(CTgridX),single(CTgridZ));

xMaxMinCT= minmax(CTgridX(:)');
yMaxMinCT= minmax(CTgridY(:)');
zMaxMinCT= minmax(CTgridZ(:)');
if flagPlot
    figure(100)
    hSlice=slice(ctMeshGridY,ctMeshGridX,ctMeshGridZ,CTImageVolume,single((yMaxMinCT(2)+yMaxMinCT(1))/2),single((xMaxMinCT(2)+xMaxMinCT(1))/2) ,single((zMaxMinCT(2)+zMaxMinCT(1))/2));
    set(hSlice,'EdgeColor','none',...
        'FaceColor','interp',...
        'FaceAlpha','interp')
    alpha('color')
    
    % shading flat
    colormap(jet)
    alphamap('rampdown')
    alphamap('increase',.1)
    axis equal
    hold on
    
end






%% 2) BitMap Mask for an ROI
ROIName= 'GTV1_00';
%ROIName= 'FemurR_00';
%  ROIName= 'Bladder_00';
%  ROIName= 'Rectum_00';
fileX= ['binaryBitmapPointIndicesX_', ROIName, '_319_Plan_.bin'];
fileY= ['binaryBitmapPointIndicesY_', ROIName, '_319_Plan_.bin'];
fileZ= ['binaryBitmapPointIndicesZ_', ROIName, '_319_Plan_.bin'];
fileXInfo = dir(fileX);
fid=fopen(fileX);
indicesX= fread(fid,fileXInfo.bytes/2,'uint16','l');
fid=fopen(fileY);
indicesY= fread(fid,fileXInfo.bytes/2,'uint16','l');
fid=fopen(fileZ);
indicesZ= fread(fid,fileXInfo.bytes/2,'uint16','l');
colMajorIndex=sub2ind([cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim],indicesX,indicesY,indicesZ);
CTImageVolume(colMajorIndex) = 0;
SliceBrowser(CTImageVolume)

fileX= ['binaryBitmapPointCoordinatesX_', ROIName, '_319_Plan_.bin'];
fileY= ['binaryBitmapPointCoordinatesY_', ROIName, '_319_Plan_.bin'];
fileZ= ['binaryBitmapPointCoordinatesZ_', ROIName, '_319_Plan_.bin'];
fileXInfo = dir(fileX);
fid=fopen(fileX);
RoiPtsX= fread(fid,fileXInfo.bytes/8,'double','l');
fid=fopen(fileY);
RoiPtsY= fread(fid,fileXInfo.bytes/8,'double','l');
fid=fopen(fileZ);
RoiPtsZ= fread(fid,fileXInfo.bytes/8,'double','l');




%% Now read Dose --> DoseCoorX,DoseCoorY,DoseCoorZ,DoseVal
filePathDoseCoorX = 'TotalDoseArrayCoordinatesX_319_Plan_.bin';
fid=fopen(filePathDoseCoorX); 
fileInfoDoseCoorX=dir(filePathDoseCoorX);
DoseCoorX= fread(fid,fileInfoDoseCoorX.bytes/8,'double','l');

filePathDoseCoorY = 'TotalDoseArrayCoordinatesY_319_Plan_.bin';
fid=fopen(filePathDoseCoorY); 
fileInfoDoseCoorY=dir(filePathDoseCoorY);
DoseCoorY= fread(fid,fileInfoDoseCoorY.bytes/8,'double','l');

filePathDoseCoorZ = 'TotalDoseArrayCoordinatesZ_319_Plan_.bin';
fid=fopen(filePathDoseCoorZ); 
fileInfoDoseCoorZ=dir(filePathDoseCoorZ);
DoseCoorZ= fread(fid,fileInfoDoseCoorZ.bytes/8,'double','l');

filePathDoseVal = 'TotalDoseArray_319_Plan_.bin';
fid=fopen(filePathDoseVal); 
fileInfoDoseVal=dir(filePathDoseVal);
DoseVal= fread(fid,fileInfoDoseVal.bytes/8,'double','l');


filePathDoseGridX = 'TotalDoseGridCoordinatesX_319_Plan_.bin';
fid=fopen(filePathDoseGridX); 
fileInfoDoseGridX=dir(filePathDoseGridX);
DoseGridCoordinateX= fread(fid,fileInfoDoseGridX.bytes/8,'double','l');

DoseGridCoordinateX = linspace(DoseGridCoordinateX(1),DoseGridCoordinateX(end),length(DoseGridCoordinateX));

filePathDoseGridY = 'TotalDoseGridCoordinatesY_319_Plan_.bin';
fid=fopen(filePathDoseGridY); 
fileInfoDoseGridY=dir(filePathDoseGridY);
DoseGridCoordinateY= fread(fid,fileInfoDoseGridY.bytes/8,'double','l');
DoseGridCoordinateY = linspace(DoseGridCoordinateY(1),DoseGridCoordinateY(end),length(DoseGridCoordinateY));

filePathDoseGridZ = 'TotalDoseGridCoordinatesZ_319_Plan_.bin';
fid=fopen(filePathDoseGridZ); 
fileInfoDoseGridZ=dir(filePathDoseGridZ);
DoseGridCoordinateZ= fread(fid,fileInfoDoseGridZ.bytes/8,'double','l');
DoseGridCoordinateZ = linspace(DoseGridCoordinateZ(1),DoseGridCoordinateZ(end),length(DoseGridCoordinateZ));


DoseVolume=reshape(DoseVal,[length(DoseGridCoordinateX),length(DoseGridCoordinateY),length(DoseGridCoordinateZ)]);

xMaxMinDose= minmax(DoseGridCoordinateX(:)');
yMaxMinDose= minmax(DoseGridCoordinateY(:)');
zMaxMinDose= minmax(DoseGridCoordinateZ(:)');


%%
% 
% if DoseGridCoordinateZ(end) < DoseGridCoordinateZ(1)
%     DoseGridCoordinateZ= -DoseGridCoordinateZ;
%     RoiPtsZ=-RoiPtsZ;
%     disp('Negate z')
% end
% 
% if DoseGridCoordinateY(end) < DoseGridCoordinateY(1)
%     DoseGridCoordinateY= -DoseGridCoordinateY;
%     RoiPtsY=-RoiPtsY;
%     disp('Negate Y')
%     
% end
% if DoseGridCoordinateX(end) < DoseGridCoordinateX(1)
%     %doseMatXVal= doseMatXVal(end:-1:1);
%     %doseMat=doseMat(end:-1:1,:,:);
%     DoseGridCoordinateX= -DoseGridCoordinateX;
%     RoiPtsX=-RoiPtsX;
%     disp('Negate X')
% end

[DoseMeshGridY,DoseMeshGridX,DoseMeshGridZ]= meshgrid(single(DoseGridCoordinateY),single(DoseGridCoordinateX),single(DoseGridCoordinateZ));

% RoiPtsX=DoseMeshGridX(500:end-500)';
% RoiPtsY=DoseMeshGridY(500:end-500)';
% RoiPtsZ=DoseMeshGridZ(500:end-500)';
% RoiPtsX = DoseGridCoordinateX(65);
% RoiPtsY = DoseGridCoordinateY(50);
% RoiPtsZ = DoseGridCoordinateZ(40);


if  flagPlot
    figure(100)
    hSlice=slice(DoseMeshGridY,DoseMeshGridX,DoseMeshGridZ,DoseVolume,single((yMaxMinDose(2)+yMaxMinDose(1))/2),single((xMaxMinDose(2)+xMaxMinDose(1))/2) ,single((zMaxMinDose(2)+zMaxMinDose(1))/2));
    set(hSlice,'EdgeColor','none',...
        'FaceColor','interp',...
        'FaceAlpha','interp')
    alpha('color')
    
    % shading flat
    colormap(jet)
    alphamap('rampdown')
    alphamap('increase',.1)
    axis equal
    hold on
    
end
if flagPlot
    figure(100)
    
    step=10;
    plot3(RoiPtsY(1:step:end),RoiPtsX(1:step:end),RoiPtsZ(1:step:end),'Marker','o','Color','red','LineStyle','none')
end
disp(['Min Max of the nominal pts to interpolate in X direction = ', num2str(minmax(RoiPtsX'))])
disp(['Min Max of the nominal pts to interpolate in Y direction = ', num2str(minmax(RoiPtsY'))])
disp(['Min Max of the nominal pts to interpolate in Z direction = ', num2str(minmax(RoiPtsZ'))])

nomDose = interp3(DoseMeshGridY,DoseMeshGridX,DoseMeshGridZ,DoseVolume,RoiPtsY,RoiPtsX,RoiPtsZ);

MaxPtDose= max(max(max(DoseVolume)))
[ix,iy,iz]=ind2sub(size(DoseVolume),find(abs(DoseVolume-MaxPtDose)<1e-3));
disp(['Maximum point dose location is at  =  (', num2str(DoseGridCoordinateX(ix)),',',num2str(DoseGridCoordinateY(iy)),',',num2str(DoseGridCoordinateZ(iz)),')'])

%% DVH+
nBins=350;
binWidth=MaxPtDose/nBins;
binCenters= binWidth/2:binWidth:MaxPtDose;

bin_counts_nom= hist(nomDose, binCenters);
binCountsCumSum=cumsum(bin_counts_nom(nBins:-1:1));
figure, plot(binCenters, binCountsCumSum(end:-1:1)/length(RoiPtsX))
%% 
figure
doseBinFilePath = 'DoseBinCenters_Patient_319_Plan_.bin';
DchFileName =['AllDchs_Patient_319_Plan__roi_',ROIName,'.bin'];
DoseBins = readBinaryFile(doseBinFilePath);
dataB = readBinaryFile(DchFileName);
dchMat=reshape(dataB,[ length(dataB)/length(DoseBins) length(DoseBins)])';
volumeBinValues= linspace(0,1,length(dataB)/length(DoseBins));


if flagPlot
    imagesc(dchMat');
    colormap(jet)
    set(gca,'YDir','normal')
    set(gca,'XTickLabel',DoseBins(get(gca,'XTick')))
    set(gca,'YTickLabel',get(gca,'YTick')/size(dchMat,2))
end

PlannedDVHFileName = ['PlannedDVH_Patient_319_Plan__roi_',ROIName,'.bin'] ; 
dataPlannedDVH = readBinaryFile(PlannedDVHFileName);

if flagPlot
figure 

plot(DoseBins,dataPlannedDVH)
end

%% DMH 
cDMHPath = ['cDMH_319_Plan__roi_',ROIName,'.bin'];
cDMH= readBinaryFile(cDMHPath);
figure 
plot(DoseBins, dataPlannedDVH)
hold on
plot(DoseBins, cDMH(1:length(DoseBins)))
%%
fclose all