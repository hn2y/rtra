#include "rcfGlobalMethods.hpp"
#include <algorithm>
#include <cctype>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include "config.h"
#ifdef HAVE_OPENSSL_MD5_H
#include <openssl/md5.h>
#endif

using namespace std;

extern "C" void LinkTest()
{
    return;
}

void rcfPrintRunTimeInformation(int argc, char *argv[])
{
   printf("\n Command Line: ");
   for(int i=0; i<argc; i++) printf(" %s", argv[i]);   
   printf("\n\tCopyright 2009,2011 Virginia Commonwealth University\n\tAll Rights Reserved\n");   
   printf ("\n\tModule %s\n\tCompiled on %s at %s",argv[0], __DATE__ , __TIME__);
   time_t t;
   t = time(NULL);
   printf("\n\tRun on %s\n",  ctime(&t) );
}
/* ************************************************** */
void rcfPrintRunTimeInformation(int argc, char *argv[], const char *Prog_Name, const float Revision)
{
   printf("\n Command Line: ");
   for(int i=0; i<argc; i++) printf(" %s", argv[i]);
   printf("\n Program %s Revision %f",  Prog_Name,Revision);
   printf("\n\tCopyright 2009,2011 Virginia Commonwealth University\n\tAll Rights Reserved\n");
   printf ("\n\tModule %s\n\tCompiled on %s at %s",argv[0], __DATE__ , __TIME__);
   time_t t;
   t = time(NULL);
   printf("\n\tRun on %s\n",  ctime(&t) );
}
/* *********************************************************** */
void rcf_print_runtime_info(int argc, char *argv[]) {
  rcfPrintRunTimeInformation(argc, argv);
}

bool CompareStrings(std::string string1, std::string string2 )
{
    std::transform(string1.begin(), string1.end(), string1.begin(), ::tolower);
    std::transform(string2.begin(), string2.end(), string2.begin(), ::tolower);

    if(0 == string1.compare(string2) ) { return true; }
    else { return false; }
}

void TrimString( std::string& str )
{
    char const* whitespace = " \t\r\n";
    std::string::size_type notwhite = str.find_first_not_of(whitespace);

    // this string is white space only, clear
    if( string::npos == notwhite )
    {
        str = "";
        return;
    }

    // remove whitespace if it exists
    if( std::string::npos != notwhite )
    {
        str.erase(0,notwhite);
        str.erase(str.find_last_not_of(whitespace) + 1, str.length() - str.find_last_not_of(whitespace) + 1);
    }
}

std::string GetCompileTime()
{
    std::string compileTime;
    compileTime = (std::string)__DATE__ + (std::string)" " + (std::string)__TIME__;
    return compileTime;
}

std::string GetExecutionStartTime()
{
    std::string executionTime;
    time_t t;
    t = time(NULL);
    executionTime = (std::string)ctime(&t);
    return executionTime;
}

void RemoveDoubleSlashes( string& input )
{    
    unsigned int index = input.find("//");
    unsigned int end = string::npos;
    
    while( end != index )
    {        
        //input = input.replace( index, 2, "/", 1 );
        input = input.replace( index, 1, "/");
        index = input.find("//", index + 1);
    }   
}

void RemoveTrailingSlash( string& input )
{
    if( input.length() > 0 )
    {
        if( '/' == input[input.length() - 1] )
        {
            input = input.substr(0, input.length() - 1);
        }
    }
}

void StringToLowerCase(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

void StringToUpperCase(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

string CalculateChecksum(unsigned char* buffer, int byteCount)
{
    #if defined(HAVE_OPENSSL_MD5_H) && defined(HAVE_CRYPTO_LIB)
        unsigned char md[MD5_DIGEST_LENGTH];
        MD5((unsigned char*)buffer, byteCount, md);
        stringstream stream;
        for( unsigned int i = 0; i < sizeof(md); ++i )
        {
            stream << std::hex     // print numbers as hexadecimal
              << std::setw(2)      // print two digits
              << std::setfill('0') // pad with '0'
              << static_cast<int>(md[i]);
        }
        return stream.str();
    #else
        std::cout << "WARNING:MD5 header/library not included, cannot calculate checksum" << endl;
        return "";
    #endif
    return "";
}

std::string GetLibraryVersion()
{
    #ifdef VERSION
    return VERSION;
    #else
    return "unknown";
    #endif
}
