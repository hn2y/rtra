/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* ct_routines.cc
   routines to deal with CT image
   Modification History:
      April 26, 2000: JVS: Cut routines from image_contour.cc
      June 30, 2000: JVS:  Check for non-equispace images...
                           just bomb for now if they are found....
      Feb 11, 2005: Got rid of incompatible new's and replaced by calloc's
                    I don't know WHO modified these into the gate routines, but it was not me!
      Aug 14, 2007: JVS: Add type cast (ITYPE) to avoid compiler warning
      May 5, 2015: JVS: add <cmath> for yak solaris compile
*/
#include <stdio.h>
#include <stdlib.h> // calloc and free
#include <string.h> // for strcmp
#include <math.h> // for fabs
#include <cmath>
using namespace std;
#include "utilities.h"
#include "option.h"
#include "string_util.h"
#include "typedefs.h"
#include "translation.h"
#include "ct_routines.h"
#include "read_clif.h"
/* *********************************************************************** */
typedef struct imageZType{
   int   Slice;
   float Z;
}imageZTypeStruct;
#define MAX_DIFF 0.0001 // differnace between numbers to be called the same
/* *********************************************************************** */
int gateCheckForNonEquispacedImage(char *imageFilename, int *slice) 
{  // Use this later as prototype for actually reading/using the image sectionZ's 
  // printf("\n Checking of NonEquispaced Images\n");
  FILE *imageStrm;
  imageStrm = open_file(imageFilename,".ImageInfo","r");
  if(imageStrm != NULL)  // found ImageInfo file so read it
  {
     /* read in the ClifObjects */
     clifObjectType *ImageInfoList = new(clifObjectType); // allocate memory
     // read in the Clif
     if( readClifFile(imageStrm, ImageInfoList,"ImageInfoList") != OK)
     {
        printf("\n ERROR: Reading clif from file %s\n", imageFilename); return(FAIL);
     }
     // printf("\n Detected %d Objects",ImageInfoList->nObjects);
     // imageZType *imageInfo = new(imageZType[ImageInfoList->nObjects]);
     imageZType *imageInfo = (imageZType *) calloc(ImageInfoList->nObjects, sizeof(imageZType) );
     if(NULL == imageInfo) {
       printf("\n ERROR: Allocating memory for imageInfo"); return(FAIL);
     }
     // Parse out the Values into a structure
     for(int iObject=0; iObject<ImageInfoList->nObjects;iObject++)
     {
        char sliceName[MAX_STR_LEN];
        sprintf(sliceName,"#\"#%d\".SliceNumber",iObject);
        char zName[MAX_STR_LEN];
        sprintf(zName,"#\"#%d\".TablePosition",iObject);
        if( readIntClifStructure  (ImageInfoList, sliceName, &imageInfo[iObject].Slice) != OK  ||
            readFloatClifStructure(ImageInfoList, zName,     &imageInfo[iObject].Z ) != OK  )
	{
           printf("\n ERROR: Reading object %d", iObject);
           return(FAIL);
        }
        // printf("\n Found Slice %d at %f", imageInfo[iObject].Slice,imageInfo[iObject].Z);
     }
     int nSlices = ImageInfoList->nObjects;
     delete ( ImageInfoList );
     fclose(imageStrm);

     /* ************************* end of the read routine ************************** */
     /* Check for equispaced slices */
     float Max_Allowed_Diff=1e-4;
     float SliceMissingDiff=0.1;
     float imageSpacing =  imageInfo[1].Z - imageInfo[0].Z;
     for(int iSlice=1; iSlice<nSlices; iSlice++)
     {
       // printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
        if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - imageSpacing ) > Max_Allowed_Diff )
	{
           printf("\n Non-uniform CT image spacing detected at slice %d", iSlice);
           printf("\n\t Expect Spacing of %f", imageSpacing);
           printf("\n\t Found  Spacing of %f", (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z));
           printf("\n\t Differance found of %g",  (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z) - imageSpacing);
           printf("\n\t Maximum Allowed Differance is %g", Max_Allowed_Diff);
           printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
           printf("\n\t Slice %d, Z = %f",iSlice-1,imageInfo[iSlice-1].Z);
           if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - imageSpacing ) > SliceMissingDiff )
	   {
             printf("\n There is probably a slice missing");
             *slice = iSlice;
             return(NOSLICE);
	   }
           return(NONEQUI);
        }
     }
     free(imageInfo);
  }
  return(OK);
}
/* *********************************************************************** */
int gateCheckForNonEquispacedImage(char *imageFilename) 
{  // Use this later as prototype for actually reading/using the image sectionZ's 
  // printf("\n Checking of NonEquispaced Images\n");
  FILE *imageStrm;
  imageStrm = open_file(imageFilename,".ImageInfo","r");
  if(imageStrm != NULL)  // found ImageInfo file so read it
  {
     /* read in the ClifObjects */
     clifObjectType *ImageInfoList = new(clifObjectType); // allocate memory
     // read in the Clif
     if( readClifFile(imageStrm, ImageInfoList,"ImageInfoList") != OK)
     {
        printf("\n ERROR: Reading clif from file %s\n", imageFilename); return(FAIL);
     }
     // printf("\n Detected %d Objects",ImageInfoList->nObjects);
     // imageZType *imageInfo = new(imageZType[ImageInfoList->nObjects]);
     imageZType *imageInfo = (imageZType *) calloc(ImageInfoList->nObjects, sizeof(imageZType) );
     if(NULL == imageInfo) {
       printf("\n ERROR: Allocating memory for imageInfo"); return(FAIL);
     }
     // Parse out the Values into a structure
     for(int iObject=0; iObject<ImageInfoList->nObjects;iObject++)
     {
        char sliceName[MAX_STR_LEN];
        sprintf(sliceName,"#\"#%d\".SliceNumber",iObject);
        char zName[MAX_STR_LEN];
        sprintf(zName,"#\"#%d\".TablePosition",iObject);
        if( readIntClifStructure  (ImageInfoList, sliceName, &imageInfo[iObject].Slice) != OK  ||
            readFloatClifStructure(ImageInfoList, zName,     &imageInfo[iObject].Z ) != OK  )
	{
           printf("\n ERROR: Reading object %d", iObject);
           return(FAIL);
        }
        // printf("\n Found Slice %d at %f", imageInfo[iObject].Slice,imageInfo[iObject].Z);
     }
     int nSlices = ImageInfoList->nObjects;
     delete ( ImageInfoList );
     fclose(imageStrm);

     /* ************************* end of the read routine ************************** */
     /* Check for equispaced slices */
     float Max_Allowed_Diff=1e-4;
     float SliceMissingDiff=0.1;
     float imageSpacing =  imageInfo[1].Z - imageInfo[0].Z;
     for(int iSlice=1; iSlice<nSlices; iSlice++)
     {
       // printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
        if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - imageSpacing ) > Max_Allowed_Diff )
	{
           printf("\n Non-uniform CT image spacing detected at slice %d", iSlice);
           printf("\n\t Expect Spacing of %f", imageSpacing);
           printf("\n\t Found  Spacing of %f", (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z));
           printf("\n\t Differance found of %g",  (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z) - imageSpacing);
           printf("\n\t Maximum Allowed Differance is %g", Max_Allowed_Diff);
           printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
           printf("\n\t Slice %d, Z = %f",iSlice-1,imageInfo[iSlice-1].Z);
           if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - imageSpacing ) > SliceMissingDiff )
	   {
             printf("\n There is probably a slice missing");
             return(NONEQUI);
	   }
           return(NOSLICE);
        }
     }
     free(imageInfo);
  }
  return(OK);
}
/* *********************************************************************** */
int gateReadCtImage(char *image_fname, image_header_type *img_header, ITYPE *image)
{
  /* Open the image stream */
  FILE *image_strm;
  image_strm = open_file(image_fname,".img","r");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.img \n",image_fname);return(FAIL);
  }

  /* allocate memory for the image */
  size_t image_size = img_header->x_dim*img_header->y_dim*img_header->z_dim;

  // double size as adding extra slices
  image = (ITYPE *)calloc(2*image_size,sizeof(ITYPE));
  if(image == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(FAIL);
  }
#ifdef DEBUG_CT
  printf("\n Reading in the CT image %s", image_fname);
#endif
  unsigned sread = (unsigned) (fread(image,sizeof(ITYPE),image_size,image_strm));
  /* close the data stream */
  fclose(image_strm);
#ifdef DEBUG_CT
  printf("\n %u floats read from %s.img",sread, image_fname);
  printf(" (%u bytes )",sread*sizeof(ITYPE));
#endif
  if(sread != image_size) return(FAIL);
  return(OK);
}
/* *********************************************************************** */
ITYPE *gateReadCtImage(char *image_fname, image_header_type *img_header)
{
  /* Check for non-eqs image */
  if(checkForNonEquispacedImage(image_fname) != OK)
  {
     printf("\n ERROR: Non-equispaced Image Detected in read_ct_image");
     return(NULL);
  }
  /* Open the image stream */
  FILE *image_strm;
  image_strm = open_file(image_fname,".img","r");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.img \n",image_fname);return(NULL);
  }

  /* allocate memory for the image */
  size_t image_size = img_header->x_dim*img_header->y_dim*img_header->z_dim;

  ITYPE *image = (ITYPE *)calloc(image_size,sizeof(ITYPE));
  if(image == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(NULL);
  }
#ifdef DEBUG_CT
  printf("\n Reading in the CT image %s", image_fname);
#endif
  unsigned sread = (unsigned) (fread(image,sizeof(ITYPE),image_size,image_strm));
  /* close the data stream */
  fclose(image_strm);
#ifdef DEBUG_CT
  printf("\n %u floats read from %s.img",sread, image_fname);
  printf(" (%u bytes )",sread*sizeof(ITYPE));
#endif
  if(sread != image_size) return(NULL);

#ifdef INVERT_IMAGE
  /* Invert the order of the y lines in the image */
  // for(int ib=0; ib<5; ib++) putchar(7);
  printf ("\n ************ CT image is being inverted **********\n");
  ITYPE *iimage = (ITYPE *)calloc(image_size,sizeof(ITYPE));
  if(iimage == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(NULL);
  }
  int icnt = 0;
  for(int k=0; k < img_header->z_dim; k++) /* Loop over EACH image */
     for(int j = img_header->y_dim-1; j >= 0; j--) /* Loop Over Each row */
        for(int i=0; i < img_header->x_dim; i++)
        {
           int index = i + img_header->x_dim * ( j + k * img_header->y_dim);
           iimage[icnt++] = image[index];
	}
  free(image);
  return(iimage);
#else
  return(image);
#endif
}
/* *********************************************************************** */
int insertSlice(image_header_type *img_header, ITYPE *image, int sliceToFix)
{

  // create slice
  ITYPE *newSlice;
  size_t SliceSize = img_header->x_dim*img_header->y_dim;
  newSlice = (ITYPE *)calloc(SliceSize,sizeof(ITYPE));
  if(newSlice == NULL){
     eprintf("\n ERROR: Cannot alloate memory for slice\n");return(FAIL);
  }
  
  printf("\nGetting new slice parameters");fflush(stdout);fflush(stderr); 
  for (int i=0;i<img_header->x_dim*img_header->y_dim;i++)
    newSlice[i] = (ITYPE) ((image[sliceToFix*SliceSize+i] + image[(sliceToFix-1)*SliceSize+i])/2.0);
  printf("\nGot new slice");fflush(stdout);fflush(stderr); 

  // increment header
  img_header->z_dim++;

  // insert slice
  for (int j = img_header->z_dim-1; j>-1 ; j--)
  {
    printf("\n Looping backwards over slices %d",j);fflush(stdout);fflush(stderr); 
    if (j>=sliceToFix)     
    {
      for (int k=0;k<img_header->x_dim*img_header->y_dim;k++)
        image[j*SliceSize+k] = image[(j-1)*SliceSize+k];
    }
    if (j==sliceToFix)     
    {
      for (int k=0;k<img_header->x_dim*img_header->y_dim;k++)
        image[j*SliceSize+k] = newSlice[k];
    }
  }


  return(OK);
}
/* *********************************************************************** */
