#-------------------------------------------------
#
# Project created by QtCreator 2016-09-26T14:05:25
#
#-------------------------------------------------

QT       += core xml gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = RTRA
CONFIG   += c++11
#CONFIG   += console
CONFIG   -= app_bundle# dont know what it is
TEMPLATE = app


PROJECTPATH = $$_PRO_FILE_PWD_
message(PROJECTPATH = $$_PRO_FILE_PWD_)
#DESTDIR     = $$PWD

CONFIG(debug, debug|release) {
    DESTDIR = build/debug
} else {
    DESTDIR = build/release
}
OBJECTS_DIR = $$DESTDIR/obj
message(OBJECTS_DIR = $$OBJECTS_DIR)

SOURCES += main.cpp\
        mainwindow.cpp \
        libClif/PinnacleFiles.cpp \
        libClif/read_clif.cc \
        libClif/readClif.cc \
        libClif/libjvs/option.cc \
        libClif/libjvs/utilities.cc \
        roisworker.cpp \
#    libClif/include/null.cc \
#    libClif/libpin/classBasedPinnacle.cc \
#    libClif/libpin/read_beams.cc \
#    libClif/libpin/read_binary.cc \
#    libClif/libpin/read_modifiers.cc \
    libClif/libpin/read_patient.cc \
#    libClif/libpin/read_pinnacle_plans.cc \
     libClif/libpin/read_pinnacle_roi.cc \
#    libClif/libpin/readBeams.cc \
#    libClif/libpin/readModifiers.cc \
     libClif/libpin/readPinnacleCurve.cc \
    libClif/readPinnacleImagesetHeader.cpp \
    libClif/read_pinnacle_volume.cpp \
#    libClif/libpin/readPinnaclePatient.cc \
#    libClif/libpin/roiRoutines.cc \
    Utilities/GuiException.cpp \
    dicomParser/RtDose.cpp \
    dicomParser/RtStructure.cpp \
    dicomParser/Roi.cpp \
    dicomParser/CTImage.cpp\
    dicomParser/DicomFilesProp.cpp\
    dicomParser/PointInPolygon.cpp \
    rtfilesparser.cpp \
    dicomParser/roi2.cpp \
    dicomParser/RtStructure2.cpp \
    libClif/RtDosePinn.cpp \
    MotionModels/ComputationEngine.cpp \
    MotionModels/DeformableBodyMotionModel.cpp \
    MotionModels/DeformationModel.cpp \
    MotionModels/DelineationModel.cpp \
    MotionModels/GumModel.cpp \
    MotionModels/ModelData.cpp \
    MotionModels/ModelDataSet.cpp \
    MotionModels/RidgidBodyMotionModel.cpp \
    MotionModels/RotationAndTranslationModel.cpp \ 
    Algorithm/RobustnessAnalyzer.cpp \
    Utilities/gpusAndCpusInfo.cpp \
    qcustomplot/qcustomplot.cpp \
    Algorithm/InterpolationAlgortihms.cpp \
    pinnacleFuncWrapper.cpp
   


HEADERS  += mainwindow.h \
        libClif/PinnacleFiles.h \
        libClif/libjvs/endian.h \
        libClif/libjvs/jvsDefines.h \
        libClif/libjvs/option.h \
        libClif/libjvs/printRunTimeInformation.h \
        libClif/libjvs/utilities.h \
        Common/PatientInfo.h \
        roisworker.h \
#        libClif/rcf/include/cfTypedefs.hpp \
#        libClif/libpin/myPinnComm.h \
#        libClif/libpin/null.hpp \
        Utilities/colors.h \
        libClif/PinnImageSetHeader.h \
        Utilities/someHelpers.h \
        Utilities/GuiException.h \
        dicomParser/RtDose.h \
        dicomParser/Roi.h \
        dicomParser/RtStructure.h \
        dicomParser/CTImage.h \
        dicomParser/pointInPolygon.h \
        dicomParser/DicomFilesProp.h \
    rtfilesparser.h \
    dicomParser/roi2.h \
    dicomParser/RtStructure2.h \
    libClif/RtDosePinn.h \
    MotionModels/GumModel.h \
    MotionModels/ModelData.h \
    MotionModels/ModelDataSet.h \
    MotionModels/RidgidBodyMotionModel.h \
    MotionModels/RotationAndTranslationModel.h \
    MotionModels/DeformableBodyMotionModel.h \
    MotionModels/DeformationModel.h \
    MotionModels/DelineationModel.h \ 
    Algorithm/RobustnessAnalyzer.h \
    Utilities/gpusAndCpusInfo.h \
    qcustomplot/qcustomplot.h \
    Algorithm/InterpolationAlgortihms.h \
    pinnacleFuncWrapper.h
  

INCLUDEPATH += $$PWD/libClif
INCLUDEPATH += $$PWD/dicomParser
INCLUDEPATH += $$PWD/Utilities
INCLUDEPATH += $$PWD/libClif/include
INCLUDEPATH += $$PWD/libClif/libjvs
#INCLUDEPATH += $$PWD/libClif/rcf/include
#INCLUDEPATH += $$PWD/libClif/rcf/util/include





# dcmtk toolkit, order of the libraries is important
win32{
# HN: for my software
#INCLUDEPATH +=  F:/DCMTKBuildWin10/config/include
#INCLUDEPATH +=  F:/UVA/gitRepos/DCMTK/dcmdata/include
#INCLUDEPATH +=  F:/UVA/gitRepos/DCMTK/oflog/include
#INCLUDEPATH +=  F:/UVA/gitRepos/DCMTK/ofstd/include
#INCLUDEPATH +=  F:/UVA/gitRepos/DCMTK/dcmimgle/include # dcmrt
#INCLUDEPATH +=  F:/UVA/gitRepos/DCMTK/dcmrt/include # dcmrt
#LIBS += -LF:\\DCMTKBuildWin10\\lib\\Debug\\ -ladvapi32 -lws2_32 -lofstd -loflog -lnetapi32 -lwsock32 -ldcmdata -ldcmimgle -ldcmimage -ldcmrt
CONFIG(release, debug | release): INCLUDEPATH += C:/xLibs/DCMTK3.6.1/release/include/
CONFIG(release, debug | release): DEPENDPATH += C:/xLibs/DCMTK3.6.1/release/include/
CONFIG(debug, debug | release): INCLUDEPATH += C:/xLibs/DCMTK3.6.1/debug/include/
CONFIG(debug, debug | release): DEPENDPATH += C:/xLibs/DCMTK3.6.1/debug/include/


CONFIG(release, debug | release): LIBS += -LC:/xLibs/DCMTK3.6.1/release/lib  -loflog -lofstd -ldcmdata -ldcmimage -ldcmrt -ldcmimgle
CONFIG(debug  , debug | release): LIBS += -LC:/xLibs/DCMTK3.6.1/debug/lib -loflog -lofstd -ldcmdata -ldcmimage -ldcmrt -ldcmimgle

}

!win32{
INCLUDEPATH +=  /home/shared/xlibs/DCMTK/release/include/
DEPENDPATH  += /home/shared/xlibs/DCMTK/release/include/
#INCLUDEPATH +=  /usr/local/include/
#DEPENDPATH += /usr/local/include/
CONFIG(release, debug | release): LIBS += -L/home/shared/xlibs/DCMTK/release/lib  -lz -lofstd -loflog -ldcmdata -ldcmimgle -ldcmimage -ldcmrt
CONFIG(debug  , debug | release): LIBS += -L/home/shared/xlibs/DCMTK/debug/lib -lz -lofstd -loflog -ldcmdata -ldcmimgle -ldcmimage -ldcmrt
}


# boost library
win32{
INCLUDEPATH += C:/xLibs/boost_1_63_0/
DEPENDPATH +=C:/xLibs/boost_1_63_0/
CONFIG(release, debug | release): LIBS += -LC:/xLibs/boost_1_63_0/lib64-msvc-12.0/ -lboost_iostreams-vc120-mt-1_63
CONFIG(debug, debug | release): LIBS += -LC:/xLibs/boost_1_63_0/lib64-msvc-12.0/ -boost_iostreams-vc120-mt-gd-1_63

}


# Cuda sources
CUDA_SOURCES += CudaCodes\cuda_interface.cu \
                CudaCodes\dvh_cuda.cu \
                CudaCodes\interp3.cu  \
                CudaCodes\negate.cu   \
                CudaCodes\transformPts.cu\
                CudaCodes\allcoateMemOnDevice.cu\
                CudaCodes\calculateAllDiffAndCulmulativeDvhs.cu\
                CudaCodes\sumUpFractionDosesOnGPU.cu\
                CudaCodes\calculateAllDiffAndCulmulativeDvhsForPinnacle.cu


# Path to cuda toolkit install
win32{
message(1 -> Adding cuda include paths for windows)

CUDA_DIR      = "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.5"
}

!win32{
message(1 -> Adding cuda include paths for linux)

CUDA_DIR      = "/usr/local/cuda/"
}
#
# Path to header and libs files
INCLUDEPATH  += $$CUDA_DIR/include
win32{
message(1.5 -> Adding QMAKE_LIBDIR in windows)

QMAKE_LIBDIR += $$CUDA_DIR/lib/x64
}
!win32{
message(1.5 -> Adding QMAKE_LIBDIR in Linux/Mac)
QMAKE_LIBDIR += $$CUDA_DIR/lib64
}
SYSTEM_TYPE = 64            # '32' or '64', depending on your system
# libs used in your code
LIBS += -lcuda -lcudart
# GPU architecture
win32{
message(2 -> Adding CUDA_ARCH in windows)
CUDA_ARCH     = sm_61
}

!win32{
message(2 -> Adding CUDA_ARCH in linux)
CUDA_ARCH     = sm_61

}

# Here are some NVCC flags I've always used by default.
NVCCFLAGS     = --use_fast_math
# Prepare the extra compiler configuration (taken from the nvidia forum - i'm not an expert in this part)
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')

# MSVCRT link option (static or dynamic, it must be the same with your Qt SDK link option)
win32 {
MSVCRT_LINK_FLAG_DEBUG = "/MDd"
MSVCRT_LINK_FLAG_RELEASE = "/MD"
}

# Tell Qt that we want add more stuff to the Makefile
QMAKE_EXTRA_COMPILERS += cuda
# Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
    # Debug mode
    message(2 -> Cuda Debug mode )
    cuda_d.input = CUDA_SOURCES
    cuda_d.output = $$OBJECTS_DIR/${QMAKE_FILE_BASE}.obj
    message(cuda_d.output = $$OBJECTS_DIR/${QMAKE_FILE_BASE}.obj)
win32{
    cuda_d.commands = $$CUDA_DIR/bin/nvcc -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE \
                     -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -Xcompiler $$MSVCRT_LINK_FLAG_DEBUG
}
!win32{
    cuda_d.commands = $$CUDA_DIR/bin/nvcc -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE \
                     -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}

}
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
    message(3 -> Cuda :Release mode )

    # Release mode
    cuda.input = CUDA_SOURCES
    cuda.output = $$OBJECTS_DIR/${QMAKE_FILE_BASE}.obj
    message($$cuda.output )


win32 {
    cuda.commands = $$CUDA_DIR/bin/nvcc $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE \
                    -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -Xcompiler $$MSVCRT_LINK_FLAG_RELEASE
}
!win32 {
    cuda.commands = $$CUDA_DIR/bin/nvcc $$NVCC_OPTIONS $$CUDA_INC $$LIBS --machine $$SYSTEM_TYPE \
                    -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
}

    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}



# openCV libraries
win32{
INCLUDEPATH += C:/xLibs/opencv310/include/
DEPENDPATH +=  C:/xLibs/opencv310/include/
CONFIG(release, debug | release): LIBS += -LC:/xLibs/opencv310/release/ -lopencv_core310 -lopencv_imgproc310 -lopencv_imgcodecs310 -lopencv_features2d310 -lopencv_highgui310 -lopencv_cudaarithm310
CONFIG(debug  , debug | release): LIBS += -LC:/xLibs/opencv310/debug/ -lopencv_core310d -lopencv_imgproc310d -lopencv_imgcodecs310d -lopencv_features2d310d  -lopencv_highgui310d -lopencv_cudaarithm310d
}

!win32{
INCLUDEPATH += /home/shared/xlibs/openCV/release/include/
DEPENDPATH +=  /home/shared/xlibs/openCV/release/include/
CONFIG(release, debug | release): LIBS += -L/home/shared/xlibs/openCV/release/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_features2d -lopencv_highgui -lopencv_cudaarithm
CONFIG(debug, debug | release): LIBS += -L/home/shared/xlibs/openCV/debug/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_features2d -lopencv_highgui -lopencv_cudaarithm
}



DISTFILES += \
    CudaCodes/allcoateMemOnDevice \
    CudaCodes/allcoateMemOnDevice.cu \
    CudaCodes/cuda_interface.cu \
    CudaCodes/dvh_cuda.cu \
    CudaCodes/interp3.cu \
    CudaCodes/lkbfrombineddose.cu \
    CudaCodes/negate.cu \
    CudaCodes/testMemoryAllocation.cu \
    CudaCodes/transformPts.cu \
    CudaCodes/calcalldvhsongpu.cu \
    CudaCodes/calculateAllDiffAndCulmulativeDvhs.cu \
    CudaCodes/sumUpFractionDosesOnGPU.cu \
    CudaCodes/calculateAllDiffAndCulmulativeDvhsForPinnacle.cu

linux-g++ {
QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp
}

win32-msvc* {
    MSVC_VER = $$(VisualStudioVersion)
    equals(MSVC_VER, 14.0){
        message("msvc 2015")
    }
win32{
QMAKE_CXXFLAGS+= -openmp -DHAVE_CONFIG_H
QMAKE_LFLAGS +=  -openmp
}


}
