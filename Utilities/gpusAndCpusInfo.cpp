#include "gpusAndCpusInfo.h"

gpusAndCpusInfo::gpusAndCpusInfo(std::ostream &stream):logstream(stream)
{
    isVerbose= false;
}

gpusAndCpusInfo::gpusAndCpusInfo():logstream(std::cout)
{
    isVerbose= false;
}


bool gpusAndCpusInfo::getNumCpuAndGpus()
{
    std::string class_member = "gpusAndCpusInfo::getNumCpuAndGpus:" ;

    numCPUs=omp_get_num_procs();
    // display CPU  configuration
    if (isVerbose)
    {
        logstream << class_member << "number of host CPUs:" << std::to_string(numCPUs) << std::endl;
    }

    // determine the number of CUDA capable GPUs
    cudaGetDeviceCount(&numGPUs);


    if (numGPUs < 1)
    {
        logstream << class_member << "ERORR -> no CUDA capable devices were detected" << std::endl;
        return false;
    }
    else
    {
        if (isVerbose)
        {
            logstream << class_member << "number of CUDA devices= " << numGPUs << std::endl;
        }
    }
    return true;
}

bool gpusAndCpusInfo::getAvilableMemoryOnDevices()
{
    std::string class_member = "gpusAndCpusInfo::checkAvailableMemoryOnDevices:" ;

    std::vector<size_t> temp(numGPUs, 0);
    freeMemOnEachDevice=temp;


    size_t totalMemBytes;
    size_t freeMemBytes;
    for (int idev = 0; idev < numGPUs; idev++)
    {
        cudaSetDevice(idev);
        cudaError_t cuda_status = cudaMemGetInfo(&freeMemBytes, &totalMemBytes);
        if (cudaSuccess != cuda_status){

            logstream << class_member <<  "ERORR cudaMemGetInfo for cuda device number " << idev << " =  " << cudaGetErrorString(cuda_status) << std::endl;
            return false;
        }

        freeMemOnEachDevice[idev] = freeMemBytes;
        if (isVerbose)
        {
            logstream << class_member  << "Free Memory of cuda device " << idev << " = " << freeMemBytes << "  (bytes)  " << std::endl;
        }
        //logstream << class_member  << "Total Memory of cuda device " << idev << " = " << totalMemBytes << "  (bytes)  " << std::endl;
    }

    return true;

}
