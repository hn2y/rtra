/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* roiRoutines.cc

   routines for dealins with ROI....

   Modification History:
      April 25, 2002: JVS: Created
      August 14, 2007: JVS: Update notes about saving volume dat
      Nov 5, 2007: JVS: eliminate myerrno, replace with checkClifError()
      Sept 21, 2009: JVS: Writing over-rides for Pinnacle >8.0
      May 21, 2011: JVS: Now works when .roi file is empty...
      Oct 2, 2012: JVS: Remove directory from sendPinnacleCommand since not used
*/
/* ********************************************************************************** */
/** \file
    \author J V Siebers

     roiRoutines.cc

     These routines are part of the VCU created c++ libpin.
     libpin contains routines which interfaces things to Pinnacle
    
**/
#include <stdlib.h>      // for calloc
#include <stdio.h>
#include <string.h>      // for strcat and strcpy

#include "string_util.h" // for get_value
#include "utilities.h"   // for error routines
#include "typedefs.h"

#include "read_clif.h"
#include "myPinnacle.h"
#include "myPinnComm.h"
#include "pinnacleScript.h"

#include "roiRoutines.h"
/* ********************************************************************************* */
int checkForPinnacleDensityOverrides(long myPinnWindowID, int *nOverride)
  /// determines if any ROI's listed in Pinnacle have density overrides.
  /// If the densities or over-ridden, then must use over-ridden data for dose calculation
{
   /* returns 1 if density overrides exist, 0 if they do not */
    int nOverridesFound=0;
   if(myPinnWindowID > 0)
   {
      /* Determine the number of contours */
      //  RoiList.Count;
      int iStatus = 1;
      int nROIs;
      char pinQuery[MAX_STR_LEN];
      strcpy(pinQuery,"RoiList.Count"); // 2007
      iStatus = iPinnComm_QueryInt(NULL, pinQuery, &nROIs);
      if (!iStatus) {
         fprintf(stderr, "\n ERROR: Determining Number of ROIs \n");
         return(FAIL);
      }
      printf("\n %d roi's found in Pinnacle Plan", nROIs);
     /* For each contour, check if density override is specified */
     // RoiList.#"#%d".OverrideDataSetData
      //     void *pvROI;
      for(int iROI=0; iROI < nROIs; iROI++)
      {
        char pcMessage[PINN_COMM_STRING_LEN];
        char roiName[PINN_COMM_STRING_LEN];
        int iOverride = -1;

        // Optional, get the name......
	sprintf(pcMessage,"RoiList.#%d.Name",iROI);
	// printf("\n Sending command (string) %s", pcMessage);
	iStatus = iPinnComm_QueryString(NULL,pcMessage, roiName);
        if (!iStatus) {
           fprintf(stderr, "\n ERROR: Querying for ROI name");
           return(FAIL);
        }
	sprintf(pcMessage,"RoiList.%s.OverrideDataSetData",roiName);
    
        // if don't get the name, then this has the same effect
      	// sprintf(pcMessage,"RoiList.#%d.OverrideDataSetData",iROI);
	printf("\n Sending command (string) %s", pcMessage);
	iStatus = iPinnComm_QueryInt(NULL,pcMessage, &iOverride);
        if (!iStatus) {
           fprintf(stderr, "\n ERROR: Querying if density override exists");
           return(FAIL);
        }
        // Count up number of ROIs that have override turned on
        printf("\t iOverride = %d", iOverride);
        if(iOverride)
	{
	    printf("\n Override detected for object %d, name %s", iROI, roiName);
           nOverridesFound++;
	}
      }
      *nOverride = nOverridesFound;
      printf("\n Found %d ROIs with density overrides specifed", nOverridesFound);
      return(OK);
   }
   else
   { 
     /* Manually check for density overrides in Plan.roi file */
   }
   return(OK);
}
/* ********************************************************************************* */
int checkForPinnacleDensityOverrides(char *pbeamFileNameStem, int *nOverride)
  /// checks for density overrides
{
   // Open the ROI file ...
   char roiFileName[MAX_STR_LEN];
   strcpy(roiFileName,pbeamFileNameStem);  // put first part on name
   strcat(roiFileName,".roi");            // append .Trial to the name  
   printf("\n Checking for density overrides in file %s", roiFileName);
   FILE *iStrm;
   iStrm = fopen(roiFileName,"r");
   if(iStrm == NULL){
       printf("\n ERROR: opening input file %s\n",roiFileName);return(FAIL);
   }
   int nOverridesFound=0;
   /*  NOTE: Cannot use normal readClif routines because in the points.roi file
             1) the density_units line does not end in a ;
             2) The points={ do not end in a ;
             (See example below)
           density =        1;
           density_units:   g/cm^3
           override_data =  0;
    
points={
15.75 36.2812 153.5
18.0938 36.2812 153.5
19.7812 36.1875 153.5

   clifObjectType *RoiList = new(clifObjectType);
   if( readClifFile(iStrm, RoiList,"RoiList") != OK)
   {
      printf("\n ERROR: Reading clif from file %s\n", roiFileName); return(FAIL);
   }
   // Number of ROI's = number of objects
   int nROI = RoiList->nObjects;
   printf("\n %d Roi's detected", nROI);

*/
   // Will use simple method of scanning file for override_data =  0; 
   do{
      int iOverride=(int) get_value_quiet(iStrm, "override_data =");
      if(iOverride > 0) nOverridesFound+=iOverride;
#ifdef DEBUG
      printf("\n iOverride = %d\tnOveridesFound = %d", iOverride, nOverridesFound);
#endif
   }while(checkStringError() != FAIL);
   resetStringError(); // reset myerrno
   *nOverride = nOverridesFound;
   fclose(iStrm);
   // delete(RoiList);
   return(OK);
}
/* ********************************************************************************* */
int saveOverrideCtData(long myPinnWindowID, case_info_type *patient)
  /// saves CT data with over-rides
/// Note, this function does not work with Pinnacle versions > 8.0 due to Pinnacle changes,
/// \todo update this function to work with later versions of Pinnacle
/// This function should not be needed anymore (After 8/14/07) since tcl now writes the over-ridden data
/// using proc getEquispacedDensityOverriddenData { trialName } and -imageSet flag is passed to mcmvc
/// The pencil beam code, however, was not updated--but now it is (9/21/09)
{
    /* April 26, 2002: Since we cannot send scripts to the Clinical system to run, this
       code presently asks you to manually run the script required.
       When can auto run scripts on that machine, need to wait till file is made before
       continuing (wait after the spawn). */


  /* Create New image file name */
  sprintf(patient->ImageName,"%s/ctDataWithOverrides", patient->Directory);
  printf("\n\n\7\7 Patient Image Name With Overrides = %s", patient->ImageName);
  if( myPinnWindowID < 0 )
  {
      char *ExtPinnWindowID = getenv("PINN_WINDOW_ID");
      char *ExtPinnComputer = getenv("PINN_COMPUTER");
      if(ExtPinnWindowID == NULL || ExtPinnComputer == NULL )
      {
        printf("\n ERROR: Cannot save OverrideCTData without active Pinnacle Session"); return(FAIL);
      }
      printf("\n Run Script to save CT-Data with over-rides, then hit enter to continue >");
      fflush(stdout);
      char tmpString[MAX_STR_LEN];
      fgets(tmpString,MAX_STR_LEN,stdin);
      printf("\n You have hit <enter>");
  }
  char pcMessage[PINN_COMM_STRING_LEN];
  // Either of the following two commands work
/*
  Pinnacle V < 8.0
// Really should check pinnacle version here....
  if(PinnWindowID > 0)
     sprintf(pcMessage,"VolumeList.#0.SaveVolumeData = %s", patient->ImageName);
  else
     sprintf(pcMessage,"VolumeList.#\"#0\".SaveVolumeData = \"%s\";", patient->ImageName);
  //  sprintf(pcMessage,"TrialList.Current.PatientVolume.SaveVolumeData = %s", patient->ImageName);
  printf("\n Saving CT data with overrides");
  // printf("\n Issuing Command %s", pcMessage);
  if(PinnWindowID > 0) // only send command for valid PinnWindowID...April 26,2002: JVS: Hack
  {
     if( sendPinnacleCommand(PinnWindowID,patient->Directory, pcMessage) != OK )
     {
        fprintf(stderr, "\n ERROR: Issuing Command %s", pcMessage);
        return(FAIL);
     }
  }
*/
  /*
  int iStatus = iPinnComm_IssueRootCommand(pcMessage);
  if (!iStatus) {
           fprintf(stderr, "\n ERROR: Issuing Command %s", pcMessage);
           return(FAIL);
  }
  */
  // #else
  /* Pinnacle V8.0 and greater */
  //  VolumeList.Add = TrialList.\#\"$trialName\".PatientRepresentation.OverrideDisplayVolume.Address;
  //VolumeList.Last.SaveVolumeData = Store.At.OutputFileName.Value;
  // VolumeList.Last.Remove = \"\";"
  if(myPinnWindowID > 0) {
   //   sprintf(pcMessage,"TrialList.\#\"%s\".PatientRepresentation.OverrideDisplayVolume.SaveVolumeData = %s", patient->TrialName, patient->ImageName);
    // printf("\n Adding to volumeList ");
     sprintf(pcMessage,"VolumeList.Add = TrialList.Current.PatientRepresentation.OverrideDisplayVolume");
     //     if( sendPinnacleCommand(myPinnWindowID,patient->Directory, pcMessage) != OK ) {
     if( sendPinnacleCommand(myPinnWindowID, pcMessage) != OK ) {
        fprintf(stderr, "\n ERROR: Issuing Command %s", pcMessage);
        return(FAIL);
     }
     printf("\n Saving CT data with overrides");
     sprintf(pcMessage,"VolumeList.Last.SaveVolumeData = %s", patient->ImageName);
     // if( sendPinnacleCommand(myPinnWindowID,patient->Directory, pcMessage) != OK ) {
     if( sendPinnacleCommand(myPinnWindowID, pcMessage) != OK ) {
        fprintf(stderr, "\n ERROR: Issuing Command %s", pcMessage);
        return(FAIL);
     }
     // printf("\n Removing added volume ");
     sprintf(pcMessage,"VolumeList.Last.Remove = \"\"");
     // if( sendPinnacleCommand(myPinnWindowID,patient->Directory, pcMessage) != OK ) {
     if( sendPinnacleCommand(myPinnWindowID, pcMessage) != OK ) {
        fprintf(stderr, "\n ERROR: Issuing Command %s", pcMessage);
        return(FAIL);
     }
  }
  // #endif
  printf("\t ....Done");
   return(OK);
}
/* ********************************************************************************* */

