#include "cfClifElement.hpp"
#include "rcfGlobalMethods.hpp"
#include <string>
#include <iostream>

using namespace std;
using namespace clif_element;

cfClifElement::cfClifElement()
{
    handle = "";
    value = "";
    style = clif_element::ASSIGNMENT;
}
    
cfClifElement::~cfClifElement()
{

}

cfClifElement::cfClifElement( string line )
{
    SetElement(line);
}

cfClifElement& cfClifElement::operator=(const cfClifElement& element)
{
    if( this == &element)
    {
        return *this;
    }

    handle = element.handle;
    value = element.value;
    style = element.style;
    return *this;
}
    
void cfClifElement::SetElement( string line )
{
    int equalsIndex = 0;

    // remove surrounding whitespace
    TrimString(line);
    // if the last character is a ';', remove it
    if( line[line.size() - 1 ] == ';' )
    {
        line = line.substr(0, line.size() - 1);
    }

    if( (int)string::npos != (equalsIndex = line.find('=') ) )
    {                      
        if( equalsIndex > 0 )
        {              
            handle = line.substr(0, equalsIndex - 1);                               
            value = line.substr(equalsIndex + 1, line.size() - equalsIndex - 1 );
            style = clif_element::ASSIGNMENT;
            TrimString(value);
        }
        else
        {
            cerr << "ERROR:cfClifElement::SetElement: '=' found with out leading handle"
                << endl;
            return;
        }       
    }
    else if( (int)string::npos != (equalsIndex = line.find_first_of(':') ) )
    {
        if( equalsIndex > 0 )
        {
            handle = line.substr(0, equalsIndex);
            value = line.substr(equalsIndex + 1, line.size() - equalsIndex);
            style = clif_element::COLON;
            TrimString(handle);
            TrimString(value);
        }
        else
        {
            cerr << "ERROR:cfClifElement::SetElement: ':' found with out leading handle"
                << endl;
            return;
        }
    }
}

string cfClifElement::GetValue( bool suppressQuotes )
{
    // remove leading and trailing quotes if 'suppressQuotes' is true (default)
    if( suppressQuotes )
    {
        string valueWithOutQuotes = value;
        // remove leading '"'
        if( valueWithOutQuotes[0] == '\"' )
        {
            valueWithOutQuotes = valueWithOutQuotes.substr(1, valueWithOutQuotes.size() -1 );
        }
        // remove trailing '"'
        if( valueWithOutQuotes[valueWithOutQuotes.size() - 1] == '\"' )
        {
            valueWithOutQuotes = valueWithOutQuotes.substr(0, valueWithOutQuotes.size() - 1 );
        }
        return valueWithOutQuotes;
    }
    else
    {
        return value;
    }
}
