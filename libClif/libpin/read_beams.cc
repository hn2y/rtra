/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* read_patient_beams.cc
   routines to read patient beam information into the patient beam data structure

   Created: 6/9/98: JVS
            6/10/98: Able to read in apertures from pinnacle
   Modification History:
   9/3/98: JVS: aperture vertices read in as floats, then converted to ints for pb routines
   9/8/98: JVS: add convert_wedge_name to convert wedge name for name for file (very crude)
   9/9/98: JVS: set all apertures use_for_dose_computations=TRUE
   9/17/98:JVS: jaw_x1,jaw_y1: Had to set to -1.0 * pinnacle_value to correspond with pb 
                coordinate system (Not exactly sure if this should be x2 or x1, when use x1 
                get no dose computed)
   10/14/98: JVS: Modified aperture code wo will read in multiple contours for an aperture
                  that has multiple contours. 
   10/23/98: JVS: Eliminate reading in of object_code and use_for_dose for aperture
   10/28/98: JVS: Add id_num to the BEAM
   10/29/98: JVS: Improving Output Error Messages
   12/09/98: JVS: Remove version number from machine name
   20/05/99: pjk: Add MLCs
   Jun3 2, 1999: JVS: Improve I/O information regarding reading in trials.
   Oct 20, 1999: JVS: Add compensator.h
   Oct 21, 1999: JVS: Add .valid to aperture, let "As Is", "As Is" allow to pass (but invalid aperture)
   Oct 27, JVS: Add #ifdef's around printf's
   Feb 12, PJK: Add read MUs
   June 7, 2000: JVS: remove ind_jaws...
   Nov 5, 2007: JVS: eliminate myerrno, replace with checkClifError()
 
   ***Note: Block's must be EXPOSE or BLOCK, AS IS is not a valid choice for
            this code

   Limitations:
   * Boluses are NOT supported
   * Compensators are NOT currently supported (need to be added)

   Open Issues:
   * Need to read in intensity matrix for IMRT
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> /* for fabs */
#include "string_util.h"
#include "utilities.h"
#include "typedefs.h"
#include "defined_values.h"
#include "apertures.h"
#include "beams.h"
#include "read_clif.h"
#include "modifiers.h"
#include "lib.h" /* for definition of TRUE */
#include "myPinnacle.h"
#include "readPinnacle.h"


/*
int read_pinnacle_bolus(FILE *fp, beam_type *pbeam);
int read_pinnacle_mlc_list(FILE *fp,  TwoDContourType *contour);
int read_pinnacle_isocenters(char *, int n_beams, beam_type *pbeams);
int read_pinnacle_patient_beams(char *pbeam_fname, char *Trial_Name, int *n_beams, beam_type **pbeams);
int read_pinnacle_pbeam(FILE *fp, beam_type *pbeam);
int read_pinnacle_mlc(FILE *istrm, beam_type *pbeam);
int read_pinnacle_point(char *pbeam_fname_stem, char *point_name, Point_3d_float *point);
int convert_fvertices_to_int(int n_vertices, float units,
                             Point_2d_float *fvertices, 
                             Point_2d_int **vertices);
int convert_wedge_name(char *name);
int read_pinnacle_curve(FILE *fp, TwoDContourType *contour);
int read_pinnacle_curve(FILE *fp, int *npts, Point_2d_float **contour);
int read_pinnacle_dosefile_id(FILE *fp, beam_type *pbeam);
*/
/* *********************************************************************************** */
int read_pinnacle_patient_beams(char *pbeam_fname_stem, char *Trial_Name, int *n_beams, beam_type **pbeams)
{
    // reads in patient beam information from pinnacle
   // open dose region file
   // append .Trial to the name to read in the .Trial information
   char pbeam_fname[MAX_STR_LEN];
   strcpy(pbeam_fname,pbeam_fname_stem);  // put first part on name
   strcat(pbeam_fname,".Trial");          // append .Trial to the name  
#ifdef DEBUG
   printf("\n Reading In Patient Beams from file %s",pbeam_fname);
   printf("\n Looking for Trial %s", Trial_Name);
#endif
   FILE *fp = fopen(pbeam_fname,"r");
   if(fp==NULL)
   {
      eprintf("\n ERROR: opening patient beam file %s", pbeam_fname);
      return(FAIL);
   }
   // may need to deal with different prescriptions here too
   // advance to location where beams are stored
   int beam_found = 0;
   char Current_Trial_Name[MAX_STR_LEN];
   do{
      if(clif_get_to_line(fp,"Trial ={")!=OK)
      {
         eprintf("\n ERROR: finding Trial in file %s",pbeam_fname);
         return(FAIL);
      }
      // get and report Trial Name
      if(clif_string_read(fp,"Name =",Current_Trial_Name) != OK)  
         return(FAIL); 
#ifdef DEBUG
      printf("\n\tFound Trial %s", Current_Trial_Name);
#endif
      /* check if current trial matches */
      if(strcmp(Current_Trial_Name,Trial_Name) == 0) beam_found = 1;
      if(!beam_found)
      {  /* close the Trial cliff */
#ifdef DEBUG
         printf("\t ....Skipping");
#endif
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
            return(FAIL);
         }
      }
   }while(!beam_found);

#ifdef DEBUG
   printf("\n\tReading Data for Trial %s", Current_Trial_Name);
#endif

       /* read in beams for current Trial */
      if(clif_get_to_line(fp,"BeamList ={")!=OK)
      {
         eprintf("\n ERROR: finding BeamList ={ in file %s",pbeam_fname);
         return(FAIL);
      }
      /* read beams until we run out of them */
      *n_beams = 0;
      beam_type *plbeam=NULL; // local copy of patient beam
      while(clif_get_to_line(fp,"Beam ={")==OK)  
      {
         /* allocate memory for this new beam */
         if(*n_beams==0)
            plbeam = (beam_type *)calloc(1,sizeof(beam_type));
         else
            plbeam = (beam_type *)realloc( (beam_type *) plbeam,
                                                 ((*n_beams+1)*sizeof(beam_type)) );
         if(plbeam == NULL)
         {
            eprintf("\n ERROR: allocating memory for beam %d",*n_beams+1);
            return(FAIL);
         }
         *pbeams = plbeam;
         strcpy(plbeam[*n_beams].fname,pbeam_fname);
         if(read_pinnacle_pbeam(fp, &plbeam[*n_beams])!=OK)
         {
           eprintf("\n ERROR: Reading Beam %d", *n_beams+1); 
           return(FAIL);
         }
         /* close the current level (the beam) of the clif */
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing beam structure %d in file %s",*n_beams+1,pbeam_fname);
            return(FAIL);
         }
         // printf("\t %s read done",plbeam[*n_beams].name);

         plbeam[*n_beams].id_num = *n_beams; // Add ID number for this BEAM 
         *n_beams=*n_beams+1;
      }
   resetClifError();  // clear errno (set when failed to read beam
   /* the context of BeamList is read to end when run out of beams */
   /* if(clif_get_to_line(fp,"};")!=OK)
      {
         eprintf("\n ERROR: Closing BeamList structure in file %s",pbeam_fname);
         return(FAIL);
      }
   */
   // close context of Trial structure
   if(clif_get_to_line(fp,"};")!=OK)
   {
      eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
      return(FAIL);
   }
   fclose(fp);
   if(read_pinnacle_isocenters(pbeam_fname_stem, *n_beams, *pbeams) != OK)
   {
      eprintf("\n ERROR: reading isocenters"); return(FAIL);
   }
#ifdef DEBUG
   printf("\n\t%d beams read in from %s for Trial %s",*n_beams, pbeam_fname, Current_Trial_Name);
#endif
   return(OK);
}
/* ************************************************************************************** */
int read_pinnacle_isocenters(char *pbeam_fname_stem, int n_beam, beam_type *pbeams)
{
   // read the isocenters from the Points file
   for(int ibm=0; ibm < n_beam; ibm++)
   {
      // read the isocenters for each beam
       // printf("\n ***** BEAM %s, %d ****", pbeams[ibm].name,ibm+1);
      if(read_pinnacle_point(pbeam_fname_stem, pbeams[ibm].IsocenterName, &pbeams[ibm].initial_isoctr_in_is)!=OK)
      {
         eprintf("\n ERROR: Reading Point %s for %s",pbeams[ibm].IsocenterName,pbeam_fname_stem);
         return(FAIL);
      }
   }
   return(OK);
}
/* ****************************************************************************************** */
int read_pinnacle_point(char *pbeam_fname_stem, char *point_name, Point_3d_float *point)
{
   char pbeam_fname[MAX_STR_LEN];
   strcpy(pbeam_fname,pbeam_fname_stem);  // put first part on name
   strcat(pbeam_fname,".Points");         // append .Points to the name  

   FILE *fp = fopen(pbeam_fname,"r");     // open the file
   if(fp==NULL)
   {
      eprintf("\n ERROR: opening patient beam file %s", pbeam_fname);
      return(FAIL);
   }
   // look for poi
   // printf("\n Searching for point %s",point_name);
   int match=0;
   do{
      if(clif_get_to_line(fp,"Poi ={")!=OK)
      {
         eprintf("\n ERROR: Finding Poi %s in file %s",point_name,pbeam_fname);
         return(FAIL);
      }
      char current_poi_name[MAX_STR_LEN];
   // read point name
      if(clif_string_read(fp,"Name =",current_poi_name) != OK)  
        return(FAIL); 
      // look for matching name
      // printf("\n Current Point Name = %s", current_poi_name);
      if(strcmp(current_poi_name,point_name)==0) match++;
      else  // close current context if name not match
      {
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing Poi %s context in file %s",current_poi_name,pbeam_fname);
            return(FAIL);
         }
      } 
   }while(!match);
   // read in coordinates
   if(checkClifError() !=OK) eprintf("\n ERROR: clif read routines report error set");
   // error check for unknown things
   point->x = clif_get_value(fp,"XCoord =");
   point->y = clif_get_value(fp,"YCoord =");
   point->z = clif_get_value(fp,"ZCoord =");
   if(checkClifError() !=OK){ eprintf("\n ERROR: reading coordinates for POI %s",point_name); return(FAIL);}
   // error check for unknown things
   if( (clif_get_value(fp,"XRotation =") != 0.0) ||
       (clif_get_value(fp,"YRotation =") != 0.0) ||
       (clif_get_value(fp,"ZRotation =") != 0.0) )
   {
      eprintf("\n ERROR: POI Rotation not yet supported (Poi = %s)",point_name);
      return(FAIL);
   }
   fclose(fp);
   return(OK);
}
/* ******************************************************************************* */
int read_pinnacle_pbeam(FILE *fp, beam_type *pbeam)
{  /* reads in components of a single beam from pinnacle */
    // initialize necessary structure elements 
   pbeam->n_apertures = 0;
    // read in name, etc
   if(clif_string_read(fp,"Name =",pbeam->name)!=OK)           // name of the beam
       { eprintf("\n ERROR: Reading beam name"); return(FAIL);}

#ifdef DEBUG
   printf("\n Reading in Beam %s",pbeam->name);
   printf("\n File Name : %s", pbeam->fname);
   printf("\n %d", MAX_OBJ_NAME_LEN);
#endif
   if(clif_string_read(fp,"IsocenterName =",pbeam->IsocenterName) != OK)  // isocenter
     return(FAIL);                                   // coordinates must be read from .poi file  
   // get the machine data
   if(clif_string_read(fp,"MachineNameAndVersion =",pbeam->beam_data_id.machine_name)!=OK)
      return(FAIL);
   // remove version number from the machine
   {
      unsigned i = 0;
      while(  (i < strlen( pbeam->beam_data_id.machine_name)) && 
               i < MAX_STR_LEN &&
               pbeam->beam_data_id.machine_name[i] != ':')i++;
      pbeam->beam_data_id.machine_name[i]='\0'; /* terminate the string after at : */
   }
   if(clif_string_read(fp,"Modality =",pbeam->beam_data_id.modality)!=OK)
       return(FAIL);
   if(clif_string_read(fp,"MachineEnergyName =",pbeam->beam_data_id.energy)!=OK)
       return(FAIL);
   pbeam->gantry = clif_get_value(fp, "Gantry =");
   float tmp = clif_get_value(fp, "GantryStop =");
   pbeam->arc = tmp-pbeam->gantry; // arc length
   /* couch */
   pbeam->turntable_angle = clif_get_value(fp, "Couch = ");
   tmp = clif_get_value(fp, "CouchStop =");
      if(checkClifError() !=OK) eprintf("\n ERROR: reading CouchStop");
   if(pbeam->turntable_angle != tmp)
      eprintf("\n ERROR: turntable start angle != turntable stop angle");

#ifdef DEBUG
   printf("\n\t **** ASSIGNING COUCH COORDINATE TO 0,0,0 ****");
   printf("\n\t **** Correct in future **** ");
#endif
   pbeam->couch.x = pbeam->couch.y = pbeam->couch.z = 0.0;

   /*  */   
   pbeam->coll = clif_get_value(fp, "Collimator =");
   // ??? need to check how the jaw positions correspond!!!
   //   pbeam->ind_jaw_x_mode_on = (int) clif_get_value(fp, "IsLeftRightIndependent =");
   // pbeam->ind_jaw_y_mode_on = (int) clif_get_value(fp, "IsTopBottomIndependent =");
   pbeam->jaw_x1 = clif_get_value(fp, "LeftJawPosition =");
      if(checkClifError() !=OK) eprintf("\n ERROR: reading LeftJawPosition");
   pbeam->jaw_x2 = clif_get_value(fp, "RightJawPosition =");
      if(checkClifError() !=OK) eprintf("\n ERROR: reading RightJawPosition");
   pbeam->jaw_y1 = clif_get_value(fp, "TopJawPosition =");
      if(checkClifError() !=OK) eprintf("\n ERROR: reading TopJawPosition");
   pbeam->jaw_y2 = clif_get_value(fp, "BottomJawPosition =");
      if(checkClifError() !=OK) {eprintf("\n ERROR: reading BottomJawPosition"); return(FAIL);}

   /* pencil beam routines expect jaw positions to be - on opposing side */
   pbeam->jaw_x1*=-1.0;
   pbeam->jaw_y1*=-1.0;

   //mlc
   pbeam->mlc_defined = (int) clif_get_value(fp, "UseMLC =");
      if(checkClifError() !=OK) {eprintf("\n ERROR: determining if MLC exists"); return(FAIL);}
#ifdef DEBUG_READ_BEAMS
   printf("\n Reading in MLC data using (old) read_beams");
#endif
   if(read_pinnacle_mlc(fp,pbeam) !=OK)
   {
      eprintf("\n ERROR: reading in mlc data"); return(FAIL);
   }
   
   // wedge
   if(clif_string_read(fp,"WedgeName =",pbeam->wedge_name)!=OK)
       { eprintf("\n ERROR: Reading wedge name"); return(FAIL);}
   /*   if( convert_wedge_name(pbeam->wedge_name) != OK) */
   if( convertWedgeName(pbeam->wedge_name) != OK)
       { eprintf("\n ERROR: Cannot convert wedge name %s",pbeam->wedge_name); return(FAIL);}
   char WedgeOrientation[MAX_STR_LEN];
   if(clif_string_read(fp,"WedgeOrientation =",WedgeOrientation)!=OK)
       { eprintf("\n ERROR: Reading WedgeOrientation"); return(FAIL);}
   // ??? this wedge angle correspondance must be checked
   if( strncmp(WedgeOrientation,"WedgeLeftToRight",16) == 0) pbeam->wedge_orientation = 90;
        else if(strncmp(WedgeOrientation,"WedgeRightToLeft",16) == 0) pbeam->wedge_orientation = 270;
        else if(strncmp(WedgeOrientation,"WedgeTopToBottom",15) == 0) pbeam->wedge_orientation = 0;
        else if(strncmp(WedgeOrientation,"WedgeBottomToTop",15) == 0) pbeam->wedge_orientation = 180;
        else if(strcmp(WedgeOrientation,"No Wedge")          == 0) pbeam->wedge_orientation = 0;
        else if(strcmp(WedgeOrientation,"NoWedge")          == 0) pbeam->wedge_orientation = 0;
        else 
        {
           eprintf("\n ERROR: %s is not a proper wedge orientation",WedgeOrientation);
           return(FAIL);
        }
   pbeam->wedge_defined = (int) clif_get_value(fp, "IsWedgeValid =");
      if(checkClifError() !=OK) {eprintf("\n ERROR: reading IsWedgeValid"); return(FAIL);}

   // next, read in the patient modifiers: blocks, compenstators, etc 
   // Pinnacle considers apertures BeamModifiers 
   if(clif_get_to_line(fp,"ModifierList ={")!=OK)
   {
      eprintf("\n ERROR: finding ModifierList in file");
      return(FAIL);
   }
   // read in beam modifiers (apertures)
#ifdef DEBUG_READ_BEAMS
   printf("\n Reading In Beam Modifiers");
#endif
   if(read_pinnacle_pbeam_modifiers(fp,pbeam) !=OK)
   {
      eprintf("\n ERROR: reading in patient apertures"); return(FAIL);
   }
   // read in  Bolus 
   if(read_pinnacle_bolus(fp,pbeam) != OK)
   {
      eprintf("\n ERROR: reading in patient boluses"); return(FAIL);
   }
   // read in compensator
   if(read_pinnacle_compensator(fp,pbeam) != OK)
   {
      eprintf("\n ERROR: reading in patient compensator"); return(FAIL);
   }   
   // Get the ID number of the dose file
   if(read_pinnacle_dosefile_id(fp,pbeam) != OK)
   {
      eprintf("\n ERROR: reading in dose file number"); return(FAIL);
   }

   pbeam->weight = clif_get_value(fp, "Weight = ");

   return(OK);
}
int read_pinnacle_dosefile_id(FILE *fp, beam_type *pbeam)
{
   char tmp_str[MAX_STR_LEN];
   if(clif_string_read(fp,"DoseVolume =",tmp_str)!=OK)          
   { 
      eprintf("\n ERROR: Reading Dose Volume  Fname"); return(FAIL);
   }
   // create file name
   if(strncmp(tmp_str,"XDR:",4) != 0)   // check for binary file specification, first 4 char 
   {
      eprintf("\n ERROR: %s is invalid specifier for a dose file name",tmp_str);
      return(FAIL);
   }
   int id_num = 0;
   if(sscanf(tmp_str+4,"%d",&id_num) != 1)
   {
      eprintf("\n ERROR: Cannot get dose grid file ID number from %s", tmp_str);
      return(FAIL);
   }
   pbeam->dose_grid_id = id_num;
   
   return(OK);
}

/* ********************************************************************************* */
int read_pinnacle_bolus(FILE *fp, beam_type *pbeam)
{   // check that no bolus exists (since not currently supported)
   if(clif_get_to_line(fp,"Bolus ={")!=OK)
   {
      eprintf("\n ERROR: finding Bolus in file"); return(FAIL);
   }
   if(clif_string_read(fp,"Type =",pbeam->bolus_name)!=OK)
   { 
      eprintf("\n ERROR: Reading Bolus Type"); return(FAIL);
   }
   if( strcmp(pbeam->bolus_name,"No bolus") )
   {
      eprintf("\n ERROR: Bolus Type %s Currently NOT supported",pbeam->bolus_name);
      return(FAIL);
   }
   pbeam->num_boluses = 0;
   // close out bolus context
      if(clif_get_to_line(fp,"};")!=OK)
      {
         eprintf("\n ERROR: Closing bolus in file ");return(FAIL);
      }
   return(OK);
}
/* ************************************************************************************ */
int read_pinnacle_contour_list(FILE *fp, int *n_cont, TwoDContourType **contour)
{  /* reads in a list of contours from Pinnacle (for example a contour of a aperture)
      The contours are nested beneath ContourList,CurvePainter,Curve, and
      RawData, this routine climbs down these levels, reads the contour,
      then climbs back out */
   // get to the level of the contour
#ifdef DEBUG_READ_BEAMS
      printf("\n In read_pinnacle_contour_list");
#endif
   if(clif_get_to_line(fp,"ContourList ={")!=OK)
   {
      eprintf("\n ERROR: finding ContourList in file"); return(FAIL);
   }

   TwoDContourType *tcontour=NULL;
#ifdef DEBUG_READ_BEAMS
   printf("\n About to clif for curve painter");
#endif

    /* read in curve painter curve data */
   while(clif_get_to_line(fp,"CurvePainter ={") ==OK ) /* do as long as curves specified */
   {
#ifdef DEBUG_READ_BEAMS
      printf("\n About to allocate memory for contour %d", *n_cont);
#endif
      /* allocate memory for next contour */
      if( *n_cont == 0 )
         tcontour = (TwoDContourType *)calloc(1,sizeof(TwoDContourType));
      else
         tcontour = (TwoDContourType *)realloc( (TwoDContourType *) tcontour, 
                                       (*n_cont+1)*sizeof(TwoDContourType));
      if(contour == NULL)
      {
         eprintf("\n ERROR: allocating memory for contour");
         return(FAIL);
      }
      /* from read_pinnacle_curve for mlc compatibility */
#ifdef DEBUG_READ_BEAMS
      printf("\n Now clif curve");
#endif
      if(clif_get_to_line(fp,"Curve ={")!=OK)
      {
         eprintf("\n ERROR: finding Curve in file"); return(FAIL);
      } 
      /* read in the contour */  
#ifdef DEBUG_READ_BEAMS
      printf("\n Read in contour with %d points", tcontour[*n_cont].npts);
#endif
      if(read_pinnacle_curve(fp,&(tcontour[*n_cont]) )!= OK)
      {
	     eprintf("\n ERROR: reading curve "); return(FAIL);
      }
      *n_cont+=1; /* increment number of contours read in- need?*/
      /* close 'Curve and curvepoint' */
      for(int i=0;i<2; i++)  // Points,RawData
      {
         if(clif_get_to_line(fp,"};")!=OK)
         {
           eprintf("\n ERROR: Closing contour in file ");return(FAIL);
         }
#ifdef DEBUG_READ_BEAMS
        printf("\n Closing 2 levels");
#endif
      }
      if(checkClifError() != OK) return(FAIL);
   }
   resetClifError(); /* clear errno set when fails CurvePainter ends */
   *contour = tcontour;
   return(OK);
}
/* ************************************************************************************ */
//int read_pinnacle_mlc_list(FILE *fp, TwoDContourType **contour)
int read_pinnacle_mlc_list(FILE *fp, TwoDContourType  *contour)
{  
   /* copied from read_pinnacle_contour_list */
   // get to the level of the contour
   if(clif_get_to_line(fp,"MLCLeafPositions ={")!=OK)
   {
      eprintf("\n ERROR: finding MLC leaf positions in file"); return(FAIL);
   }

   
   TwoDContourType *tcontour=NULL;

   /* allocate memory for mlc contour */
   
   tcontour = (TwoDContourType *)calloc(1,sizeof(TwoDContourType));

   if(tcontour == NULL)
   {
      eprintf("\n ERROR: allocating memory for contour");
      return(FAIL);
   }
   
   /* read in the contour */  
#ifdef DEBUG_READ_BEAMS
   printf("\n Read in mlc contour points");
#endif
   if(read_pinnacle_curve(fp,&(tcontour[0]) )!= OK)
   {
          eprintf("\n ERROR: reading curve "); return(FAIL);
   }
   if(checkClifError() != OK) return(FAIL);

   /* Close MLC leaf positions */
   if(clif_get_to_line(fp,"};")!=OK)
   {
      eprintf("\n ERROR: Closing mlc leaf positions in file ");return(FAIL);
   }
#ifdef DEBUG_READ_BEAMS
   printf("\n Closed clif MLC leaf positions");
#endif

   *contour = tcontour[0];
   return(OK);
}
/* *************************************************************************** */ 
int read_pinnacle_curve(FILE *fp, TwoDContourType *contour)
{
   return(read_pinnacle_curve(fp, &contour->npts, &contour->fvert));
}
int read_pinnacle_curve(FILE *fp, int *npts, Point_2d_float **contour)
{
#ifdef DEBUG_READ_BEAMS
   printf("\n Reading Pinnacle Curve");
#endif
   /* moved to read_pinnacle_contour_list for compatibility with MLC */
   /*   if(clif_get_to_line(fp,"Curve ={")!=OK)
   {
      eprintf("\n ERROR: finding Curve in file"); return(FAIL);
      } */ 
   if(clif_get_to_line(fp,"RawData ={")!=OK)
   {
      eprintf("\n ERROR: finding RawData in file"); return(FAIL);
   }
   int NumberOfDimensions = (int) clif_get_value(fp,"NumberOfDimensions =");
      if(checkClifError() !=OK) {eprintf("\n ERROR: reading NumberOfDimensions"); return(FAIL);}
   if(NumberOfDimensions != 2)
   {
     eprintf("\n ERROR: NumberOfDimensions (%d) != 2 in read_pinnacle_curve",NumberOfDimensions);
     return(FAIL);
   }
   *npts = (int) clif_get_value(fp,"NumberOfPoints =");
#ifdef DEBUG_READ_BEAMS
   printf("\n num dim = %d and num points = %d",NumberOfDimensions,*npts);
#endif
   if(clif_get_to_line(fp,"Points[] ={")!=OK)
   {
      eprintf("\n ERROR: finding Points[] in file"); return(FAIL);
   }

   Point_2d_float *value;
   // allocate memory for the verticies
   value = (Point_2d_float *)calloc(*npts,sizeof(Point_2d_float));
   if(value == NULL)
   {
      eprintf("\n ERROR: allocating memory for contour");
      return(FAIL);
   }
   /* at end, use *contour = value to assign value */
   // read in the verticies points
   for(int ival=0;ival<*npts;ival++)
   {
      char string[MAX_STR_LEN];
      if(clif_fgets(string,MAX_STR_LEN,fp)==NULL)
      {
         eprintf("\n ERROR: reading vertices (%d)",ival+1);
         return(FAIL);
      }
      if(sscanf(string,"%f,%f",&value[ival].x,&value[ival].y) !=2)
      {
         eprintf("\n ERROR: scanning vertice %d from string %s", ival, string);
         return(FAIL);
      }
#ifdef DEBUG_READ_BEAMS
   printf("\n Point = %d vertex = (%f, %f)",ival,value[ival].x,value[ival].y);
#endif
   }   
   // ensure that last point = first (contour closed), if not, close it
   if( value[0].x != value[*npts-1].x ||
       value[0].y != value[*npts-1].y )
   {
     *npts = *npts+1;  /* reallocate memory for additional point */
     value = (Point_2d_float *)realloc((Point_2d_float *) value, 
	    (*npts)*sizeof(Point_2d_float)); 
     value[*npts-1].x = value[0].x; /* assign coordinates to first point */
     value[*npts-1].y = value[0].y;
#ifdef DEBUG_READ_BEAMS
     printf("\n Closed aperture with point %f %f",value[*npts-1].x,value[*npts-1].y);
#endif
   }
   // close each level 
   for(int i=0;i<2; i++)  // Points,RawData
   {
      if(clif_get_to_line(fp,"};")!=OK)
      {
         eprintf("\n ERROR: Closing contour in file ");return(FAIL);
      }
#ifdef DEBUG_READ_BEAMS
     printf("\n Closing 2 levels");
#endif
   }
   *contour = value;
   return(OK);
}

/* ************************************************************************************ */
int convert_fvertices_to_int(int n_vertices, float units,
                             Point_2d_float *fvertices, 
                             Point_2d_int **vertices)
{
   Point_2d_int *value;
   float inv_units;
   float x,y;

   /* allocate memory for storing values */
   value = (Point_2d_int *)calloc(n_vertices,sizeof(Point_2d_int));
   if(value == NULL)
   {
      eprintf("\n ERROR: allocating memory for contour");
      return(FAIL);
   }
   *vertices = value;
   inv_units = 1.000000/units;
   for(int i=0; i<n_vertices; i++)
   {
      /* convert units */
      x =  (fvertices[i].x * inv_units);   
      y =  (fvertices[i].y * inv_units);
      /* do round-off correctly : x */
      if( (x - floor(x)) < (ceil(x) - x))
           value[i].x = (int) floor(x);
      else
           value[i].x = (int) ceil(x);
      /* do round-off correctly : y */
      if( (y - floor(y)) < (ceil(y) - y))
           value[i].y = (int) floor(y);
      else
           value[i].y = (int) ceil(y);
      /* check value (if units was too large, then would over-run int's */
      if(fabs(value[i].x-inv_units*fvertices[i].x) > 0.5 ||
         fabs(value[i].y-inv_units*fvertices[i].y) > 0.5 ) 
      {
         eprintf("\n ERROR: convert_fvertices_to_int");
         eprintf("\n \t units = %f, inv_units = %f", units, inv_units);
         eprintf("\n \t Floats: %f %f",fvertices[i].x,fvertices[i].y);
         eprintf("\n \t Ints  : %d %d",  value[i].x, value[i].y);
         eprintf("\n \t Deltas: %f %f",fabs(value[i].x-inv_units*fvertices[i].x),
                               fabs(value[i].y-inv_units*fvertices[i].y));
         return(FAIL);
      }
   }
   return(OK);
}
/* ************************************************************************************ */
int read_pinnacle_mlc(FILE *fp, beam_type *pbeam)
{
   // reads in the pinnacle mlc, returns FAIL if at End of File
   // returns END_OF_CONTEXT if reads to end of ModifierList
   // first line is the type of modifier 
   // a "modifier" is considered to be an aperture
   // copied from read_pinnacle_pbeam_modifiers above

   if (pbeam->mlc_defined) /* do as long as mlc specified  (!=0 for fail safe)*/
   {
     /*
#ifdef DEBUG_READ_BEAMS
         printf("\n Allocating Memory for mlc");
#endif
         pbeam->mlc = (mlc_type *)calloc(1,sizeof(mlc_type));
         if(pbeam->mlc == NULL)
         {
            eprintf("\n ERROR: allocating memory for mlc");
            return(FAIL);
         }
     */
#ifdef DEBUG_READ_BEAMS
         printf("\n Reading Contour List for Aperture");
#endif
         if(read_pinnacle_mlc_list(fp, &pbeam->mlc.contour)!=OK)
         {
            eprintf("\n ERROR: reading pinnacle contour list");
            return(FAIL);
         }

         if(checkClifError() != OK) return(FAIL);
#ifdef DEBUG_READ_BEAMS
         printf("\n Done read_pinnacle_mlc");
#endif
   }
   return(OK);
}
/* ************************************************************************** */
int convert_wedge_name(char *name)
{
    /* temp routine, should use same names or read in a file that has
       the conversions in it.  This was done for quick and dirty */
   if(strcmp("Lower 15 deg",name)==0){sprintf(name,"lwdg15");return(OK);}
   if(strcmp("Lower 30 deg",name)==0){sprintf(name,"lwdg30");return(OK);}
   if(strcmp("Lower 45 deg",name)==0){sprintf(name,"lwdg45");return(OK);}
   if(strcmp("Lower 60 deg",name)==0){sprintf(name,"lwdg60");return(OK);}

   if(strcmp("Upper 15 deg",name)==0){sprintf(name,"uwdg15");return(OK);}
   if(strcmp("Upper 30 deg",name)==0){sprintf(name,"uwdg30");return(OK);}
   if(strcmp("Upper 45 deg",name)==0){sprintf(name,"uwdg45");return(OK);}
   if(strcmp("Upper 60 deg",name)==0){sprintf(name,"uwdg60");return(OK);}

   if(strcmp("No Wedge",name)==0 ||
      strcmp("No wedge",name)==0 ||
      strcmp("NoWedge",name) ==0 ){sprintf(name,"nowdg");return(OK);}
   eprintf("\n\t ERROR: Cannot find %s",name);
   eprintf("\n\t length = %d",strlen(name));
   return(FAIL);
}
/* ************************************************************************** */
