
close all force

close all force
clear variables
cTImageInfo=readImageHeader('CTVolumePatientAImageSetNKIVCU^PA^S00^I0002.bin.header');



fid=fopen('CTVolumePatientAImageSetNKIVCU^PA^S00^I0002.bin.img');
CTImageVector= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'uint16','l');%x_dim*y_dim*z_dim
% CTImageVector=CTImageVector(end:-1:1);

CTImageVolume=reshape(CTImageVector,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]);
fclose(fid);





flagPlot =true;
doseBinFilePath = 'DoseBinCenters_Patient_319_Plan_.bin';
DchFileName ='AllDchs_Patient_319_Plan__roi_GTV1_00.bin';
DoseBins = readBinaryFile(doseBinFilePath);
dataB = readBinaryFile(DchFileName);
dchMat=reshape(dataB,[ length(dataB)/length(DoseBins) length(DoseBins)])';
volumeBinValues= linspace(0,1,length(dataB)/length(DoseBins));

% 
% if flagPlot
%     imagesc(dchMat');
%     colormap(jet)
%     set(gca,'YDir','normal')
%     set(gca,'XTickLabel',DoseBins(get(gca,'XTick')))
%     set(gca,'YTickLabel',get(gca,'YTick')/size(dchMat,2))
% end
% 
% PlannedDVHFileName = 'PlannedDVH_Patient_319_Plan__roi_GTV1_00.bin' ; 
% dataPlannedDVH = readBinaryFile(PlannedDVHFileName);
% 
% if flagPlot
% figure 
% 
% plot(DoseBins,dataPlannedDVH)
% end