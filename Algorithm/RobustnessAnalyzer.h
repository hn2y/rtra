#ifndef ROBUSTNESSANALYZER_H
#define ROBUSTNESSANALYZER_H




#include <map>
#include <limits>       // std::numeric_limits
#include <set>
#include <random>
#include <numeric>      // std::partial_sum

// OpenMP header
#include <omp.h>



// thrust header
#include <cuda_runtime.h>

//#include <cuda.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
//#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
//#include <thrust/transform.h>
//#include <thrust/device_malloc.h>
//#include <thrust/device_free.h>
// https://code.google.com/p/thrust/wiki/QuickStartGuide


#include "dicomParser/RtDose.h"
#include "dicomParser/RtStructure.h"
#include "MotionModels/ModelData.h"

// qcustomplot
#include "qcustomplot/qcustomplot.h" // used in plotDVHs
#include <QVector> // used in qcustomplot


#include "Utilities/gpusAndCpusInfo.h"
// qt headers
#include <QObject>     // used for interactive garph  parts
#include <QHBoxLayout>
#include <QSlider>
#include <QSpinBox>

// Pinnacle Files
#include "libClif/RtDosePinn.h"
#include "libClif/include/roi_type.h"
#include "libClif/PinnacleFiles.h"





// Define Cuda Related Library Here
//extern "C" void launchTransformPts(float *XqTransformed, float *YqTransformed, float *ZqTransformed, float *Xqn, float *Yqn, float *Zqn, int np, float *R_All, float *T_All, int numTrans, int devNum);
//extern "C" void testAdd(int * ary1, int * ary2);
//extern "C" void sumUpFractionDosesOnGPU(float *d_doseOut, const float *d_doseIn, const int fractionsNum, const float *fractionDoseWeights, const int numDoseSets,  const int NumoFDosePtsInEachDoseSet, const int devNum);

// extern "C" void vecFloatOnDevice(int numElement, thrust::device_vector<float>& v);
// interp3 perfroms triliniear interpolation in GPU,   the gridpoint coordinates must be monotonically increasing
// gridX, gridY and gridZ are device raw pointers to gridpoint coordinates
//extern "C"  void interp3(
//    float * vOutput,
//    const int     nQueryPoints,
//    const int     xSize,
//    const int     ySize,
//    const int     zSize,
//    const float * gridX,
//    const float * gridY,
//    const float * gridZ,
//    const float * vGrid,
//    const float * xq,
//    const float * yq,
//    const float * zq);

//extern "C" void negateAll(float *vectorToNegate, int vecSize);
//extern "C" void dvhCalcGPU(float *dvhs, float *doseBins, const int EachBinLength, const float *dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum);
//extern "C" void allocateMemOnDevice(float* d_allocated, size_t numElement);
extern "C" cudaError_t calculateAllDiffAndCulmulativeDvhs(std::vector<float> &cDVHForTrial,
                                                          std::vector<float> &diffDVHForTrial,
                                                          std::vector<float> &voxelDoseMeanAllTrials,
                                                          std::vector<float> &voxelDoseVarianceAllTrials,
                                                          std::vector<float> fractionsDoseWeights,
                                                          std::vector<double> doseArray,
                                                          std::vector<double> doseGridX,
                                                          std::vector<double> doseGridY,
                                                          std::vector<double> doseGridZ,
                                                          std::vector<double> PointsCloudX,
                                                          std::vector<double> PointsCloudY,
                                                          std::vector<double> PointsCloudZ,
                                                          std::vector<float> RotationMatrices,
                                                          std::vector<float> TranlationVectors,
                                                          const int numDoseBins,
                                                          const float doseBinWidth);

extern "C" cudaError_t calculateAllDiffAndCulmulativeDvhsForPinnacle(std::vector<float> &cDVHForTrial,
                                                          std::vector<float> &diffDVHForTrial,
                                                          std::vector<float> &voxelDoseMeanAllTrials,
                                                          std::vector<float> &voxelDoseVarianceAllTrials,
                                                          std::vector<float> fractionsDoseWeights,
                                                          std::vector<float> doseArray,
                                                          std::vector<float> doseGridX,
                                                          std::vector<float> doseGridY,
                                                          std::vector<float> doseGridZ,
                                                          std::vector<float> PointsCloudX,
                                                          std::vector<float> PointsCloudY,
                                                          std::vector<float> PointsCloudZ,
                                                          std::vector<float> PartialVolume,
                                                          std::vector<float> RotationMatrices,
                                                          std::vector<float> TranlationVectors,
                                                          const int numDoseBins,
                                                          const float doseBinWidth,
                                                          const bool isVerbose);


class RobustnessAnalyzer : public QObject
{
    Q_OBJECT

public:

    struct SimulationModelParams
    {


        // All displacements in mm and
        float XdispSys_Std = 0.0;
        float YdispSys_Std = 0.0;
        float ZdispSys_Std = 0.0;
        float XpdfDispSysMean = 0.0;
        float YpdfDispSysMean = 0.0;
        float ZpdfDispSysMean = 0.0;
        std::vector<float> deltaXSystematicVec; //randn(1, numTrials) * XdispSys_Std + XpdfDispSysMean;
        std::vector<float> deltaYSystematicVec;//  = randn(1, numTrials) * YdispSys_Std + YpdfDispSysMean;
        std::vector<float>  deltaZSystematicVec;// = randn(1, numTrials) * ZdispSys_Std + ZpdfDispSysMean;


        float XpdfDispRandMean = 0.0;
        float YpdfDispRandMean = 0.0;
        float ZpdfDispRandMean = 0.0;

        float XdispRnd_Std = 0.0;
        float YdispRnd_Std = 0.0;
        float ZdispRnd_Std = 0.0;
        float XpdfSysRotMean = 0.0;
        float YpdfSysRotMean = 0.0;
        float ZpdfSysRotMean = 0.0;

        float XpdfRandRotMean = 0.0;
        float YpdfRandRotMean = 0.0;
        float ZpdfRandRotMean = 0.0;

        float XrotSys_Std = 0.0;
        float YrotSys_Std = 0.0;
        float ZrotSys_Std = 0.0;

        float XrotRnd_Std = 0.0;
        float YrotRnd_Std = 0.0;
        float ZrotRnd_Std = 0.0;
        std::vector<float> rotXSystematicVec;//  = randn(1, numTrials) * XrotSys_Std + XpdfRotMean;
        std::vector<float> rotYSystematicVec; // = randn(1, numTrials) * YrotSys_Std + YpdfRotMean;
        std::vector<float> rotZSystematicVec;      //  = randn(1, numTrials) * ZrotSys_Std + ZpdfRotMean;




        //indexTransformation = iTrial*numFractions + iFrac; //iTrial = 0:numTrials-1 ,  iFrac = 0:numFractions-1
        // startindex = indexTransformation * 9  stopindex=(indexTransformation * 9+8) // rotation start -stop
        std::vector<float> RotationMatrices;
        // startindex = indexTransformation * 3  stopindex=(indexTransformation * 3+2) // translation start -stop
        std::vector<float> TranslationVectors;


    };
    bool IsIntraFxRandomGaussian= false;
    std::vector<SimulationModelParams> simulationModelParams;

    RobustnessAnalyzer();
    RobustnessAnalyzer(std::ostream &stream);
    ModelData* modelData;
    RtDose* rtDose;
    RtStructure* rtStructure;
    QCustomPlot* customPlotDvhs;
    QCustomPlot* customPlotPDvhs;
    QCustomPlot* customPlotDvhsConfIntervals;
    QCustomPlot* customPlotDch;
    QWidget * pDVHsWidget;
    QWidget * pDVHsConfIntWidget;

    std::vector<int> desiredPDVHsConfidenceLevel;
    std::vector<Qt::PenStyle> genPenStyles();

    // Pinnacle
    PinnacleFiles * pinnacleFiles; // contains all the information regarding the patient except rtDose





    // test Functions
    //void testInterp3GPU();
   // void testDvhCalcGPU();
    //void testLoadCSVFileTOPts(); // TODO: implement this to check other pointcloud generation alogirthm performance
    // void testSumUpFractionDosesOnGPU();
    void testWriteVectorToAsciiAndBinary();


    // PDVH will be constructed using this vector , For each ROI, each transformation will have say np points. These points are bined after interpolation in the specified binDose, save into a vector volCountInBinDosesForTransformedPts[nROI].
    // volCountInBinDosesForTransformedPts[nROI] contains numFractions*numTrails*numDoseBins elements,  volCountInBinDosesForTransformedPts[nROI][indexDoseBin + indexTransformation*numDoseBins]
    // indexTransformation = iTrial*numFractions + iFrac; //iTrial = 0:numTrials-1 ,  iFrac = 0:numFractions-1
    // indexDoseBin = 1:numDoseBins
    std::vector<std::vector <int>> diffDVHsForTransformedPts;
    std::vector<std::vector <float>> diffDVHForTrial;
    std::vector<std::vector <float>> diffDMHForTrial;
    std::vector<std::vector <float>> cDVHForTransformedPts;
    std::vector<std::vector <float>> cDVHForTrial;
    std::vector<std::vector <float>> cDMHForTrial;
    std::vector<std::vector <float>> voxelDoseMeanForTrials;
    std::vector<std::vector <float>> voxelDoseStdForTrials;
    std::vector<std::vector <float>> NTCPForTrial;  // TODO: move this in different class e.g. EndPointModels/EvaluationModels
    std::vector<std::vector <float>> TCPForTrial;   // TODO: move this in different class e.g. EndPointModels/EvaluationModels
    std::vector<std::vector <float>> cDch; // Represents a numDoseBins by 500 matrix in column major form  which contains Dose Coverage Histogram (rows reperesents dose bins and cols repersent
    std::vector<std::vector <float>> cDVHsMean;
    std::vector<std::vector <float>> cDVHsMedian;
    std::vector<std::vector <float>> cDVHs95thPercentile;
    std::vector<std::vector <float>> cDVHs5thPercentile;
    std::vector<std::vector <float>> cDVHsStandardDeviation;
    std::vector<std::vector <float>> pDVHs; //
    std::vector<std::vector <float>> plannedDVHs; //



    // Bin centers in CDVH and PDVH
    std::vector<double> doseBinCenters;


    // --------------------------- functions
    //  setting and getting params

     int getNumDoseBins();
     int getNumTrials();
     int getNumFractions();
     int getNumSubFractions();

     std::string getResultFolderPath();
    bool getUseGPU();
    void setGPUUsage(const bool flagOn);
    void setDoseBinCenters(std::vector<double> & doseBinCentersVec);
    void setResultFolderPath(std::string _resultFolderPath);

    void calculateAllDVHsOnCpu();
    bool calculateAllDVHsOnCpuForPinnaclePatient();

    template<typename T,typename TDoseArray, typename TQueryPts, typename TRotationMatrices, typename TTranslationVectors>
    bool calculateAllDiffAndCulmulativeDvhsForPinnaclePatientOnCPU(std::vector<T> &_cDVHForTrial,
                                                            std::vector<T> &_diffDVHForTrial,
                                                            std::vector<T> &_voxelDoseMeanForTrials,
                                                            std::vector<T> &_voxelDoseStdForTrials,
                                                            const std::vector<T> &_fractionsDoseWeights,
                                                            const std::vector<TDoseArray> &_doseArray,
                                                            const std::vector<TDoseArray> &_doseGridXvalues,
                                                            const std::vector<TDoseArray> &_doseGridYvalues,
                                                            const std::vector<TDoseArray> &_doseGridZvalues,
                                                            const std::vector<TQueryPts> &_QueryPointsCoordinatesX,
                                                            const std::vector<TQueryPts> &_QueryPointsCoordinatesY,
                                                            const std::vector<TQueryPts> &_QueryPointsCoordinatesZ,
                                                            const std::vector<TRotationMatrices> &_RotationMatrices,
                                                            const std::vector<TTranslationVectors> &_TranslationVectors,
                                                            const int & _numDoseBins,const float & _doseBinWidth);

    void setRtDose(RtDose* inRtDose);
    void setRtStructure(RtStructure* inRtStruc);
    void setPinnacleFiles(PinnacleFiles* _pinnacleFiles);
    void setRoiPinnPropVector(std::vector<roi_prop>* inRoiPropVector);
    void setModelData(ModelData* inModelData);

    //  ********************************** CUDA computation methods ****************************
    //	****************************************************************************************
    //  ****************************************************************************************
        // this function breaks the task into subtask and distribute them among available computation resources
        // TODO:: might be unrelated for this part  http://devblogs.nvidia.com/parallelforall/gpu-pro-tip-cuda-7-streams-simplify-concurrency/
        // Defalut stream (asynch and synchronous call thrust) http://stackoverflow.com/questions/27278552/cuda-kernel-launched-after-call-to-thrust-is-synchronous-or-asynchronous
        // http://www.techenablement.com/the-cuda-thrust-api-now-supports-streams-and-concurrent-tasks/ --> Specify a launch configuration, Decompose the problem into sub-tasks and Marshal parameters
        // http://sschaetz.github.io/mgpu/#
        // Bulk --> Inside Thrust: Building Parallel Algorithms with Bulk  https://www.youtube.com/watch?v=4v24yYoyLf4
        // http://stackoverflow.com/questions/21616395/multi-gpu-cuda-thrust
        // Memory Requirement http://stackoverflow.com/questions/10979229/how-to-estimate-gpu-memory-requirements-for-thrust-based-implementation
    bool calculateAllDVHsOnGpu();
    bool calculateAllDVHSonGpuForPinnaclePatient();


    /// fills  std::vector<std::vector <float>> cDch where cDch[n] is vector that contains a Dch matrix for ROi n
    bool calculateDchs();

    /// fills  std::vector<std::vector <float>> pDVHs where pDVHs[iRoi][]
    bool plotPDVHs();
    bool plotPDVHSNew();


//    bool checkAvailableGPUsAndCPUs();
//    bool checkAvailableMemoryOnDevices();
    bool generateTransformationsParams();
    bool generateTransformationsParamsPinnacle();
    bool generateTransformationsParamsWithIntraFractionPinnacle();
   // bool writeAllDvhsToFile();
    bool writeAllDchsToFile();
    bool writeAllDchsToFile(std::string path);


    bool plotDVHs();
    bool plotDVHsConfInterval();
    bool plotDCH(const std::string& RoiName);
    bool calculatePDVHsFromDCHsAtSpecifiedConfLevel();
    bool calculateDVHsMeanAndVarianceMedianPercentiles();
    bool calculatePDVHsConfLevelInitialValues();
    bool WritecDVHs95thPercentile();
    bool WritecDVHs95thPercentile(std::string path);
    bool WritecDVHsMedian();
    bool WritecDVHsMedian(std::string path);

    bool WritecDVHsMean();
    bool WritecDVHsMean(std::string path);

    bool WritecDVHsSTD();
    bool WritecDVHsSTD(std::string path);

    bool WritecDVHs5thPercentile();
    bool WritecDVHs5thPercentile(std::string path);

    bool WritecDVHSForTransformedPoints();
    bool WritecDVHSForTrails(); // this writes the dvhs of numTrails different simulation
    bool WritecDVHSForTrails(std::string path); // this writes the dvhs of numTrails different simulation
    bool WriteDiffDvhsForTrials();
    bool WriteDiffDvhsForTrials(std::string path);
    bool WritePlannedDVHS();
    bool WritePlannedDVHS(std::string path);



    void setDoseBinWidth(double _doseBinWith);




private slots:
    void updatePDVHConfLevelForAnROI();
    void setDesiredPDVHsConfidenceLevels(int, int );
    void setSlidersAndqSpinBoxesConfidanceValueSlot(std::vector<int>);
    void updatePDVHSGraphs();
public slots:
    void setNumTrials(int numTrls);
    void setNumFractions( int numFrac);
    void setNumDoseBins(int numBins);
    void setNumSubFraction(int numSubFrac);

    void setPinnacleFlag(bool pinnIsBeingUsed);

//    void testSlotForPDVHsConfValue();

signals:
    void emitDesiredPDVHsConfidenceLevels(std::vector<int>);

private:
    QList<QSpinBox *> qSpinBoxList;
    QList<QSlider *> qSliderList;
    QList<QLabel *> qLabelList;

    std::string resultFolderPath;
    std::ostream &logstream;
    int numTrials;
    int numFractions;
    int numSubFractions;
    int numDoseBins;
    int numVolBinForDCHandPDVH;
    int numQualifiedDevices;
    // this will contains staring and number of transformation to be perform for each
    // supported/qulified device -transformationJobDistributionVec[iDev] is  2*NTransSets by 1 vector  where
    // NTransSets is the number of transformation sets to be performed by device iDev
//    std::vector<std::vector<unsigned int>>  transformationJobDistributionVec;
//    std::vector<unsigned int>  indicesOfQualifiedDevices;
//    std::vector<unsigned int> numberOfPossibleTransfomationInEachKernelLaunch;


    bool isPinnacleFlag;

    // Bin width in DVH and PDVH
    double doseBinWidth;
   // int numGPUs; // number of CUDA GPUs (Devices)
   // int numCPUs; // number of CPU cores
    std::vector<size_t> freeMemOnEachDevice;
    std::vector<bool> IsDeviceSelectedForComputation; // TODO: for future advanced selection of GPU in config file
    bool useGPU;
    typedef thrust::device_vector<float> dVec;
    typedef dVec *pointerDVec;
//    void allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPoints(std::vector<float *> &d_inPolygonPointsZforROIiRawPointerVec, std::vector<pointerDVec> &d_inPolygonPointsXforROIiPointerVec, std::vector<float *> &d_doseGridYvaluesRawPointerVec, std::vector<float *> &d_doseGridXvaluesRawPointerVec, std::vector<pointerDVec> &d_doseGridXvaluesPointerVec, std::vector<float *> &d_inPolygonPointsXforROIiRawPointerVec, std::vector<pointerDVec> &d_inPolygonPointsYforROIiPointerVec, std::vector<pointerDVec> &d_doseArraryPointerVec, std::vector<pointerDVec> &d_doseGridYvaluesPointerVec, std::vector<pointerDVec> &d_rotationMatricesPointerVec, std::vector<float *> &d_doseArrayRawPointerVec, std::vector<pointerDVec> &d_translationVectorsPointerVec, std::vector<pointerDVec> &d_inPolygonPointsZforROIiPointerVec, std::vector<pointerDVec> &d_doseGridZvaluesPointerVec, int iRoi, std::vector<float *> &d_doseGridZvaluesRawPointerVec, std::vector<float *> &d_inPolygonPointsYforROIiRawPointerVec, std::vector<float *> &d_rotationMatricesRawPointerVec, std::vector<float *> &d_translationVectorsRawPointerVec);

//    std::string patientID;
//    std::string doseCalcImageSet;
      int numRois;
      bool isVerbose;
//    double maxDose;
};

#endif // ROBUSTNESSANALYZER_H
