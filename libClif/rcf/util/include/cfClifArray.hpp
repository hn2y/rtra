#ifndef CFCLIFARRAY_HPP
#define CFCLIFARRAY_HPP

#include <vector>
#include <string>

class cfClifArray
{
    public:
        cfClifArray();
        ~cfClifArray();
        void AddElement( std::string element );
        void AddElement( float element );
        std::string GetHandle() { return handle; }
        std::string GetElement( int index ) { return arrayElements[index]; }
        void SetHandle( std::string arrayHandle );
        int GetNumberOfElement() { return arrayElements.size(); }
        bool GetPoints( int** points );
        int GetPoints( float* points );
        std::vector<std::string> arrayElements;   
    private:
        std::string handle;
         
};
#endif
