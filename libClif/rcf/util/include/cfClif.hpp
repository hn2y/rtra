#ifndef CFCLIF_HPP
#define CFCLIF_HPP

#include <fstream>
#include <vector>
#include <string>
#include "cfClifArray.hpp"
#include "cfClifElement.hpp"
#include "cfClifTripleList.hpp"

class cfClif
{
    public:
        cfClif();
        ~cfClif();
        int GetNumberOfClifs() { return clifList.size(); }
        int GetNumberOfArrays() { return arrays.size(); }
        int GetNumberOfDataElements() { return data.size(); }
                
        std::string GetDataValue( std::string dataHandle );
        std::string GetDataValue( unsigned int index );
        std::string GetDataHandle( unsigned int index );
        clif_element::ClifElementStyle GetStyle( unsigned int index );

        bool GetDataValue( std::string dataHandle, char& number );
        bool GetDataValue( std::string dataHandle, unsigned char& number );
        bool GetDataValue( std::string dataHandle, short& number );
        bool GetDataValue( std::string dataHandle, unsigned short& number );
        bool GetDataValue( std::string dataHandle, int& number );
        bool GetDataValue( std::string dataHandle, unsigned int& number );
        bool GetDataValue( std::string dataHandle, long& number );
        bool GetDataValue( std::string dataHandle, unsigned long& number );
        bool GetDataValue( std::string dataHandle, float& number );
        bool GetDataValue( std::string dataHandle, double& number );
        bool GetDataValue( std::string dataHandle, std::string& str );
        
        cfClifArray* GetArray( std::string arrayHandle );
        cfClif* GetClif(int index ) { return clifList[index]; } // FIXME - add out of bounds checking
        cfClif* GetClif(std::string clifHandle);
        cfClifElement* GetClifElement(int index );
        cfClifElement* GetClifElement(std::string handle );
        std::string GetHandle() { return handle; }
        
        int AssignVariableWithElement(std::string elementHandle, int& var);
        int AssignVariableWithElement(std::string elementHandle, float& var);
        int AssignVariableWithElement(std::string elementHandle, double& var);
        int AssignVariableWithElement(std::string elementHandle, char* var);
        int AssignVariableWithElement(std::string elementHandle, std::string& var);
        void SetHandle( std::string clifHandle );

        void AddClif( cfClif* clif );
        void AddClif( cfClif& clif );
        void AddArray( cfClifArray* array );
        void AddElement( cfClifElement* element );
        void AddElement( std::string elementHandle, std::string elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, char elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, unsigned char elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, short elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, unsigned short elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, int elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, unsigned int elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, long elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, unsigned long elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, float elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        void AddElement( std::string elementHandle, double elementValue, clif_element::ClifElementStyle style = clif_element::ASSIGNMENT );
        
        int WriteClif( std::string path );
        void RemoveClif( unsigned int index );
        void RemoveClif( std::string clifHandle );
        void RemoveElement( unsigned int index );
        void RemoveElement( std::string elementHandle );
        
        void SortClifListByElementValue( std::string elementTag );

        cfClif& operator=(const cfClif& clif);

        cfClif* GetRootClif() { return root; }
        cfClif* GetParentClif() { return parent; }
        int GetNumberOfLevelsFromRootClif();

   // private: FIXME
        std::string handle;
        std::vector<cfClifElement*> data;
        std::vector<cfClifArray*> arrays;
        std::vector<cfClif*> clifList;
        cfClifTripleList* tripleList;
    private:
        void GetNumberOfLevelsFromRootClifRecursive(int& level);
        std::string AddTabs( int tabCount );
        void WriteClifRecursive( std::ofstream& writer, int* stackDepth );
        void WriteArray( std::ofstream& writer, int* stackDepth, int arrayIndex );
        cfClif* parent; ///< Pointer to parent clif, NULL if this is the top clif
        cfClif* root;   ///< Pointer to root clif, (this == root) if this clif is the root
        
};
#endif
