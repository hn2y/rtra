#ifndef PATIENTSTRUCT
#define PATIENTSTRUCT

struct DicomSeries
{
    std::string PatientID;
    std::string StudyID;
    std::string SeriesNumber;

    std::string ModalityID;
    std::vector<std::string> FilePathVec;
    std::vector<std::string> FileNameVec;
};

struct DicomStudy
{
    std::string PatientID;
    std::string StudyID;
    std::vector<DicomSeries> SeriesVec;

};

struct DicomPatient
{
    std::string PatientID;
    std::vector<DicomStudy> StudyVec;
};
#endif // PATIENTSTRUCT

