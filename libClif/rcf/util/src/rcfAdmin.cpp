#include "rcfAdmin.hpp"
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include "config.h"
#include "cfClifReader.hpp"
#include "rcfGlobalMethods.hpp"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

void rcfAdmin::basicInit()
{
   ntags = 0; 
   is_ready = 0;
   mypath = NULL;
   int length = 0;
   default_configname = NULL;
   func = (char*)"rcfAdmin::basicInit";

   memset(tags,0,sizeof(tags));
   memset(paths,0,sizeof(paths));

  // determine the separator
  if(0 == strcmp("UNAME_SYSTEM","SunOS") )
  {
     strcpy(separator,"/");
  }
  else if(0 == strcmp("UNAME_SYSTEM","Linux") )
  {
     strcpy(separator,"/");
  }
  else if(0 == strcmp("UNAME_SYSTEM","Windows") )
  {
     strcpy(separator,"\\");
  }
  else
  {
     // we default here, if we are wrong, error will
     // occur in file open later, and the bad path will 
     // be reported
     strcpy(separator,"/");
  }
  // allocate and set default config name
  length = strlen(RCF_ADMIN_DEF_CONFIGNAME);
  default_configname = new char[length+1];
  if(NULL == default_configname)
  {
     reportError("Failure to allocate memory for default config name");
     return;
  }
  strcpy(default_configname,RCF_ADMIN_DEF_CONFIGNAME);

}

rcfAdmin::rcfAdmin()
{
   basicInit();
}

rcfAdmin::rcfAdmin(char *path)
{
   int length;
   int rtn;
   basicInit();
   func = (char*)"rcfAdmin::rcfAdmin";

   if(NULL == path)
   {
      reportError("NULL pointer passed instead of path");
      return;
   }
   if(0 == (length = strlen(path)) )
   {
      reportError("Empty String passed instead of path");
      return;
   }
   
   rtn = readConfigFile(path);
   if(CF_OK != rtn)
   {
      fprintf(stderr,"%s failure to read the configuration file %s\n",func,path);
      return;
   }
   
   return;
}

rcfAdmin::~rcfAdmin()
{
   if(NULL != mypath)
   {
     delete [] mypath;
     mypath = NULL;
   }
   if(NULL != default_configname)
   {
      delete[] default_configname;
      default_configname = NULL;
   }
}

int rcfAdmin::isReady()
{
   return(is_ready);
}

bool rcfAdmin::isKeyWritable(std::string key)
{
   func = (char*)"rcfAdmin::isKeyWritable";

   for(int i=0; i<ntags; i++)
   {
      if(0 == key.compare(std::string(tags[i])) )
      {
         // found the key
         if(writable[i]) return(true);
         else return(false);
      }
   }
   // if we are here, we have not found the key
   return(false);
}

int rcfAdmin::updatePointerArrays()
{
   func = (char*)"rcfAdmin::updatePointerArrays";
   //sanity checks
   int lntags = store_tags.size();
   int lnpaths = store_paths.size();
   int lnflags = writable.size();
   bool islocdata = false;
   std::string tlocdata = "LOCDATA";

   if( (lntags != lnpaths) || (lntags != lnflags) )
   {
      reportError("inconsistent state of storage arrays");
      fprintf(stderr,"ntags: %d  npaths: %d  nflags: %d\n",lntags,lnpaths,lnflags);
      return(CF_FAIL);
   }

   if( lntags >= RCF_MAXADMINTAGS)
   {
      reportError("Number of tags exceeds maximum allowed");
      fprintf(stderr,"Number of tags: %d, maximum tags: %d\n",lntags+1,RCF_MAXADMINTAGS);
      return(CF_FAIL);
   }

   for( int i=0; i<lntags; i++)
   {
      tags[i] = (char*)(store_tags[i].c_str());
      paths[i] = (char*)(store_paths[i].c_str());
      if(0 == tlocdata.compare(store_tags[i]))islocdata = true;
   }

   if(!islocdata) reportWarning("Default data depository location tag missing");

   ntags = lntags;
   return(CF_OK);
}

int rcfAdmin::readConfigFile(char* path)
{
   func = (char*)"rcfAdmin::readConfigFile";
   int rtn;
   if(NULL == path)
   {
      reportError("NULL pointer passed instead of path");
      return(CF_FAIL);
   }
   if(0 == strlen(path) )
   {
      reportError("Empty String passed instead of path");
      return(CF_FAIL);
   }

   rtn = storePathToRcf(path);
   if(CF_OK != rtn)
   {
      reportError("failure to store path to rcf");
      return(CF_FAIL);
   }

    string configPath;
    configPath = path + (string)separator + "admin" + (string)separator + "config";
    cfClifReader adminConfigReader;
    adminConfigReader.OpenClif(configPath);
    if( !adminConfigReader.IsOpen() )
    {
        reportError("Failure to initialize the Config File parser");
        return(CF_FAIL);
    }

    //Combine open and read commands?
    adminConfigReader.ReadClif();
    int numberOfClifs = adminConfigReader.GetNumberOfClifs();    
    if( 0 >= numberOfClifs )
    {        
        reportError("No valid clifs (ie no tag/value pairs) in the admin file");
        return(CF_FAIL);
    }

    string type = adminConfigReader.GetDataValue("type");
    if( 0 != type.compare("RCFADMIN") )
    {     
        reportError("The configuration file does not identify itself as RCFADMIN. Execution terminates.");
        return(CF_FAIL);
    }
    
    string key;
    string dbPath;
    bool twritable = false;
    const string yesString = "yes";
 
    for( int i = 0; i < numberOfClifs; i++ )
    {
        cfClif* currentClif = adminConfigReader.GetClif(i);
        if( NULL == currentClif )
        {
            reportError("Failure to execute apparently valid clif. Execution terminates.");
            return(CF_FAIL);
        }

        string clifHandle = currentClif->GetHandle();
        if( !CompareStrings(clifHandle, "keyword") )
        {
            reportWarning("Unrecognized handle of apparently valid clif. Skipping");
            //fprintf(stderr,"Handle: %s\n",clifHandle);
            cout <<  "Handle: " << clifHandle << endl;
            continue;
        }
        key.clear();
        dbPath.clear();
        twritable = false;

        if( 2 > currentClif->GetNumberOfDataElements() )
        {
            reportError("Failure to find tag/value pair in apparently valid keyword. Execution terminates.");
            return(CF_FAIL);
        }

        string keyValue = currentClif->GetDataValue("key");
        if( keyValue.length() <= 0 )
        {
            reportError("Failure to find tag/value pair in apparently valid keyword. Execution terminates.");
            return(CF_FAIL);
        }
        else
        {
            key = keyValue;
        }

        string pathValue = currentClif->GetDataValue("path");
        if( pathValue.length() <= 0 )
        {
            reportError("Failure to find tag/value pair in apparently valid keyword. Execution terminates.");
            return(CF_FAIL);
        }
        else
        {
            dbPath = pathValue;
        }

        string writableValue = currentClif->GetDataValue("writable");
        if( writableValue.length() > 0 )
        {
            if( CompareStrings(yesString, writableValue) )
            {
                twritable = true;
            }
        }
        
        store_tags.push_back(key);
        store_paths.push_back(dbPath);
        writable.push_back(twritable);
    }

   // put parser on stack
   /*rcfClifParser parser(lbuffer);
   if(!(parser.isClifReady()))
   {
      reportError("Failure to initialize the Config File parser");
      return(CF_FAIL);
   }

   int ndata = 0;
   int nclif = 0;

   rtn = parser.getNumberOfClifObjects(&nclif);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of clif objects in the admin file");
      return(CF_FAIL);
   }
   if( nclif<=0 )
   {
      reportError("No valid clifs (ie no tag/value pairs) in the admin file");
      return(CF_FAIL);
   }

   rtn = parser.getNumberOfClifObjects(&ndata);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of data objects in the admin file");
      return(CF_FAIL);
   }
   if( ndata<=0 )
   {
      reportError("No valid data objects (ie no file type identification) in the admin file");
      return(CF_FAIL);
   }

   char *type = NULL;

   // first check if we have a correct file here
   type = parser.getDataBufferInSelectedClif("type",&valid);
   if( (NULL == type) || (0 == valid) )
   {
      //there must be type identification here. Reject
      reportError("type tag not found");
      return(CF_FAIL);
   }
   else
   {
      if(0 != strcmp(type,"RCFADMIN") )
      {
         reportError("The configuration file does not identify itself as RCFADMIN. Execution terminates.");
         return(CF_FAIL);
      }
   }

   int ndatain = 0;
   char *handle = NULL;
   char *data = NULL;
   std::string ttag;
   std::string tvalue;
   bool twritable = false;
   std::string stryes = "yes";
   //store all tags
   for(int i=0; i<nclif; i++)
   {
      rtn = parser.selectClifObject(i);
      if(CF_OK != rtn)
      {
         reportError("Failure to execute apparently valid clif. Execution terminates.");
         return(CF_FAIL);
      }
      handle = parser.getHandleOfSelectedClif(&valid);
      if( (CF_OK != rtn) || (0 == valid) || (NULL == handle) )
      {
         reportWarning("Failure to find handle of apparently valid clif. Skipping");
         continue;
      }
      if( 0 != strcmp(handle,"keyword") )
      {
         reportWarning("Unrecognized handle of apparently valid clif. Skipping");
         fprintf(stderr,"Handle: %s\n",handle);
         continue;
      }
      ttag.clear();
      tvalue.clear();
      twritable = false;
      rtn = parser.getNumberOfDataObjectsInSelectedClif(&ndatain);
      if(CF_OK != rtn)
      {
         reportError("Failure to find number of data objects in apparently valid clif. Execution terminates.");
         return(CF_FAIL);
      }
      if( ndatain < 2 )
      {
         reportError("Failure to find tag/value pair in apparently valid keyword. Execution terminates.");
         return(CF_FAIL);
      }
      // get keyword,path,and write status
    
      data = parser.getDataBufferInSelectedClif("key",&valid);
      if( (NULL == data) || (0 == valid) )
      {
         reportError("Failure to find tag/value pair in apparently valid keyword. Execution terminates.");
         return(CF_FAIL);
      } 
      else
      {
         ttag = std::string(data);
      }
      data = parser.getDataBufferInSelectedClif("path",&valid);
      if( (NULL == data) || (0 == valid) )
      {
         reportError("Failure to find value in apparently valid tag/value pair. Execution terminates.");
         return(CF_FAIL);
      } 
      else
      {
         tvalue = std::string(data);
      }
      data = parser.getDataBufferInSelectedClif("writable",&valid);
      if( (NULL == data) || (0 == valid) )
      {
         // OK, default to false 
      } 
      else
      {
         if(0 == stryes.compare(std::string(data)) )twritable = true; 
         // otherwise default to false
      }
      // if we are here, we have just processed a valid keyword, log it as such
      store_tags.push_back(ttag);
      store_paths.push_back(tvalue);
      writable.push_back(twritable);

      parser.resetClifObjectSelection();
   }**/

   // done loading keywords, update working arrays
   rtn = updatePointerArrays();
   if( CF_OK != rtn )
   {
      reportError("Failure to update pointer arrays");
      return(CF_FAIL);
   }

   is_ready = 1;
   return(CF_OK);
}

char* rcfAdmin::AppendPath(char *key, char *path, int *verify)
{
   func = (char*)"rcfAdmin::AppendPath";
   int pathlength;
   int ifound = 0;
   int index;
   *verify = 0;
 
   if(NULL == key)
   {
      reportError("User requests path append, but provides no key (null pointer)");
      return(NULL);
   }
   if(0 == strlen(key))
   {
      reportError("User requests path append, but provides no key (empty string of length zero)");
      return(NULL);
   }
   if(NULL == path)
   {
      reportError("User requests path append, but provides no path to append (null pointer)");
      return(NULL);
   }
   if(0 == strlen(path))
   {
      reportError("User requests path append, but provides no path to append (empty string of length zero)");
      return(NULL);
   }
   if(0 == is_ready)
   {
      reportError("User requests path append, but the rcfAdmin object is not ready");
      return(NULL);
   }

   // search for key
   for(int i=0; i<ntags; i++)
   {
     if(0 == strcmp(key,tags[i]))
     {
        ifound = 1;
        index = i;
        break;
     } 
   }
   if(0 == ifound)
   {
      // issue warning, this might not have been intentional
      reportWarning("No match to the key found");
      fprintf(stderr,"User key that failed to match: %s\n",key);
      return(NULL);
   }
   else
   {
      if(NULL == paths[index] )
      {
         // this is a logical error. User should
         // not be asking us to concatenate with 
         // non-existent path
         return(NULL);
      }
      // create new string and concatenate
      pathlength = strlen(path);
      pathlength += strlen(paths[index]);
      char *ptr = NULL;
      ptr = new char[pathlength+2]; 
      if(NULL == ptr)
      {
         reportError("Failure in memory allocation");
         fprintf(stderr,"size of request: %d\n",pathlength+2);
         return(NULL);
      } 
      else
      {
         strcpy(ptr,paths[index]);
         strcat(ptr,separator);
         strcat(ptr,path);
         *verify = 1;
         return(ptr);
      }
      
   }

   // If we are here, something is wrong. We return
   // successful pointer in the if/else clause above.
   return(NULL);
}

char* rcfAdmin::AppendPathDeleteOriginal(char *key, char *path, int *verify)
{
   func = (char*)"rcfAdmin::AppendPathDeleteOriginal";
   char *rtn_ptr = NULL;

   rtn_ptr = AppendPath(key,path,verify);
   if( (0 == *verify) || ( NULL == rtn_ptr) )return(NULL);

   if(NULL != path)
   {
       delete[] path;
       path = NULL;
   }

   return(rtn_ptr);
}

char* rcfAdmin::AppendPathToConfig(char *path, int *verify)
{
   return(AppendPathToConfigSelect(1,path,verify));
}
char* rcfAdmin::AppendPathToConfigDirectory(char *path, int *verify)
{
   return(AppendPathToConfigSelect(0,path,verify));
}
char* rcfAdmin::AppendPathToConfigSelect(int select,char *path, int *verify)
{
   func = (char*)"rcfAdmin::AppendPathToConfigSelect";
   int pathlength;
   *verify = 0;

   if(NULL == path)
   {
      reportError("User requests path append, but provides no path to append (null pointer)");
      return(NULL);
   }
   if(0 == strlen(path))
   {
      reportError("User requests path append, but provides no path to append (empty string of length zero)");
      return(NULL);
   }
   if(0 == is_ready)
   {
      reportError("User requests path append, but the rcfAdmin object is not ready");
      return(NULL);
   }
   if(NULL == mypath)
   {
      reportError("User requests path append, but the rcfAdmin object has not been properly initialized");
      return(NULL);
   }
   if( 0 == strlen(mypath) )
   {
      reportError("User requests path append, but the rcfAdmin object has not been properly initialized (rcf path not defined)");
      return(NULL);
   }

   // screen the selector value
   switch(select)
   {
      case 0:
      case 1:
         break;
      default:
         reportError("Illegal selector value");
         fprintf(stderr,"Seen: %d  allowed 0 or 1\n",select);
         return(NULL);
         break; 
   }

   // create new string and concatenate
   pathlength = strlen(path);
   pathlength += strlen(mypath);
   if(1 == select)pathlength += strlen(default_configname);
   char *ptr = NULL;
   ptr = new char[pathlength+3];
   if(NULL == ptr)
   {
      reportError("Failure in memory allocation");
      fprintf(stderr,"size of request: %d\n",pathlength+3);
      return(NULL);
   } 
   else
   {
      strcpy(ptr,mypath);
      strcat(ptr,separator);
      strcat(ptr,path);
      strcat(ptr,separator);
      if(1 == select)strcat(ptr,default_configname);
      *verify = 1;
      return(ptr);
   }
      

   // If we are here, something is wrong. We return
   // successful pointer in the if/else clause above.
   return(NULL);
}

char* rcfAdmin::AppendPathToConfigDeleteOriginal(char *path, int *verify)
{
   char *rtn_ptr = NULL;

   rtn_ptr = AppendPathToConfig(path,verify);
   if( (0 == *verify) || ( NULL == rtn_ptr) )return(NULL);

   if(NULL != path)
   {
       delete[] path;
       path = NULL;
   }

   return(rtn_ptr);
}
char* rcfAdmin::AppendPathToConfigDirectoryDeleteOriginal(char *path, int *verify)
{
   char *rtn_ptr = NULL;

   rtn_ptr = AppendPathToConfigDirectory(path,verify);
   if( (0 == *verify) || ( NULL == rtn_ptr) )return(NULL);

   if(NULL != path)
   {
       delete[] path;
       path = NULL;
   }

   return(rtn_ptr);
}

std::string rcfAdmin::ComputeRelativePathToConfig(std::string path, bool *verify)
{
   func = (char*)"rcfAdmin::ComputeRelativePathToConfig";
   *verify = false;
   std::string empty = "";
   // sanity check
   if(NULL == mypath)
   {
      reportError("This object has not been properly initialized, path to database appears undefined");
      return(empty);
   }
   if(path.size() <= 0)
   {
      reportError("Empty string passed by user in lieu of data");
      return(empty);
   }
   std::string lmypath = mypath;
   size_t found;
   found = path.find(lmypath);
   if(std::string::npos == found)
   {
      reportError("User data and current database path are incompatible");
      fprintf(stderr,"User data: %s \t Path to database: %s\n",path.c_str(),mypath);
      return(empty);
   }
   if(found != 0)
   {
      reportError("current database path does not open user string, operation can not be performed");
      fprintf(stderr,"User data: %s \t Path to database: %s\n",path.c_str(),mypath);
      return(empty);
   }
   // all looks good, do the clipping
   std::string rslt = path;
   rslt.erase(0,lmypath.size());
   found = rslt.find(separator);
   if(0 == found)
   {
      //strip leading separator if accidentally left over
      rslt.erase(0,1);
   }
   *verify = true;
   return(rslt);
}

int rcfAdmin::storePathToRcf(char *path)
{
   func = (char*)"rcfAdmin::storePathToRcf";
   int length;
   // sanity checks
   if(NULL == path)
   {
      reportError("NULL pointer passed in lieu of path to RCF");
      return(CF_FAIL);
   }
   if( 0 == (length = strlen(path)) )
   {
      reportError("empty string passed in lieu of path to RCF");
      return(CF_FAIL);
   }
   mypath = new char[length+1];
   if(NULL == mypath)
   {
      reportError("Failure to allocate memory");
      fprintf(stderr,"size of request: %d\n", (length+1));
      return(CF_FAIL);
   }

   // copy info
   strcpy(mypath,path);
   return(CF_OK);
}


void rcfAdmin::reportError(const char *string)
{
   // sanity checks
   if( NULL == string )
   {
      fprintf(stderr,"WARNING: %s called Error Report with empty message (NULL pointer detected) \n",func);
      return;
   }
   if( 0 == strlen(string) )
   {
      fprintf(stderr,"WARNING: %s called Error Report with empty message (empty string) \n",func);
      return;
   }

   fprintf(stderr,"%s reports ERROR: %s \n",func,string);
}

bool rcfAdmin::isKeyDetected(std::string key)
{
   for(int i=0; i<ntags; i++)
   {
      if(0 == key.compare(std::string(tags[i])) )
      {
         // found the key
         return(true);
      }
   }
   // if we are here, we have not found the key
   return(false);
}

bool rcfAdmin::isKeyPathDefined(std::string key)
{
   for(int i=0; i<ntags; i++)
   {
      if(0 == key.compare(std::string(tags[i])) )
      {
         // found the key
         if(NULL != paths[i]) return(true);
         else return(false);
      }
   }
   // if we are here, we have not found the key
   return(false);
}

void rcfAdmin::reportWarning(const char *string)
{
   // sanity checks
   if( NULL == string )
   {
      fprintf(stderr,"WARNING: %s called Warning Report with empty message (NULL pointer detected) \n",func);
      return;
   }
   if( 0 == strlen(string) )
   {
      fprintf(stderr,"WARNING: %s called Warning Report with empty message (empty string) \n",func);
      return;
   }

   fprintf(stderr,"%s reports WARNING: %s \n",func,string);
}

char* rcfAdmin::showMyPath(int *verify)
{
   *verify = 0;
   if(NULL == mypath)return(NULL);
   else
   {
      if(strlen(mypath) > 0)
      {
         *verify = 1;
          return mypath;
      }
      else return(NULL);
   }
}

rcfAdmin& rcfAdmin::operator=(const rcfAdmin &admin)
{
    //FIXME - make sure this does correct deallocation first
    // check for self-assignment
    if( this == &admin )
    {
        return *this;
    }

    // func
    if( NULL != func )
    {
        //delete func;
        //func = NULL;
    }
    if( NULL != admin.func )
    {
        func = new char[sizeof(admin.func)];
        *func = *admin.func;
    }

    // tags
    for( unsigned int i = 0; i < RCF_MAXADMINTAGS; i++ )
    {
        if( NULL != tags[i] )
        {
            //delete tags[i];
            //tags[i] = NULL;
        }
        if( NULL != admin.tags[i] )
        {
            tags[i] = new char[sizeof(admin.tags[i])];
            memcpy(tags[i], admin.tags[i], sizeof(admin.tags[i]));
        }
    }

    // paths
    for( unsigned int i = 0; i < RCF_MAXADMINTAGS; i++ )
    {
        if( NULL != paths[i] )
        {
            //delete paths[i];
        //    paths[i] = NULL;
        }

        if( NULL != admin.paths[i] )
        {
            paths[i] = new char[sizeof(admin.paths[i])];
            memcpy(paths[i], admin.paths[i], sizeof(admin.paths[i]));
        }
    }
         
    memcpy(separator, admin.separator, 2);

    // default_configname
    if( NULL != default_configname )
    {
        //delete default_configname;
        default_configname = NULL;
    }
    if( NULL != admin.default_configname )
    {
        default_configname = new char[sizeof(admin.default_configname)];
        *default_configname = *admin.default_configname;
    }

    // mypath
    if( NULL != mypath )
    {
        //delete mypath;
        //mypath = NULL;
    }
    if( NULL != admin.mypath )
    {
        mypath = new char[sizeof(admin.mypath)];
        *mypath = *admin.mypath;
    }
    is_ready = admin.is_ready;
    ntags = admin.ntags;    

    //store_tags.clear();
    for( unsigned int i = 0; i < admin.store_tags.size(); i++ )
    {
        store_tags.push_back(admin.store_tags[i]);
    }

    //store_paths.clear();
    for( unsigned int i = 0; i < admin.store_paths.size(); i++ )
    {
        store_paths.push_back(admin.store_paths[i]);
    }

    //writable.clear();
    for( unsigned int i = 0; i < admin.writable.size(); i++ )
    {
        writable.push_back(admin.writable[i]);
    }
    
    return *this;
}
