#include "cfDicomDirectoryMgr.hpp"
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <iostream>
#include "cfDicomSliceReader.hpp"

using namespace std;
using namespace DICOM_MGR;

cfDicomDirectoryMgr::cfDicomDirectoryMgr()
{
    //Initialize();
}
    
DICOM_MGR::DicomHeaderTags cfDicomDirectoryMgr::GetSetInformation(unsigned int index)
{
    if( index < tagVector.size() )
    {
        return tagVector[index];
    }
    else
    {
        cerr << "ERROR:cfDicomDirectoryMgr::GetSetInformation: Index was invalid, no data to return" << endl;        
        DICOM_MGR::DicomHeaderTags blank;
        return blank;
    }    
}

int cfDicomDirectoryMgr::ReadDirectory(string directory)
{
    vector<string> files = vector<string>();
    DIR* dp = NULL;
    struct dirent *dirp;
    if((dp = opendir(directory.c_str())) == NULL)
    {
        cerr << "ERROR: cfDicomSetReader::ReadFiles: Could not open directory "
                << directory << endl;
        return CF_FAIL;
    }   
    
    // load buffer with files found in this directory
    while ((dirp = readdir(dp)) != NULL)
    {
        files.push_back(string(dirp->d_name));
    }
   
    string dicomFilename;
    for (unsigned int i = 0; i < files.size(); i++)
    {
        dicomFilename = directory + "/" + files[i];
        
        if( 0 != dicomFilename.substr(dicomFilename.length() - 4, 4).compare(".dcm") 
                && 0 != dicomFilename.substr(dicomFilename.length() - 8, 8).compare(".sdcopen") 
                && 0 != dicomFilename.substr(dicomFilename.length() - 4, 4).compare(".img") )
        {
            continue;
        }
        else if( 0 == files[i].substr(0,2).compare("RS") || 0 == files[i].substr(0,2).compare("RE") )
        {
            cout << "WARNING: DICOM filename stars with either RS or RE, these files are currently not supported, skipping" << endl;
            continue;
        }
 
        cfDicomSliceReader* currentSlice = new cfDicomSliceReader(dicomFilename);
        currentSlice->SuppressBinaryRead(true);      
                
        if( CF_OK != currentSlice->ReadSlice() )
        {
            continue;
        }

        DicomHeaderTags currentTags;
        currentTags.modality = currentSlice->GetFieldValue(MODALITY);
        currentTags.patientID = currentSlice->GetFieldValue(PATIENT_ID);
        currentTags.patientName = currentSlice->GetFieldValue(PATIENT_NAME);
        currentTags.seriesInstanceUID = currentSlice->GetFieldValue(SERIES_INSTANCE_UID);
        currentTags.seriesNumber = currentSlice->GetFieldValue(SERIES_NUMBER);
        currentTags.studyDate = currentSlice->GetFieldValue(STUDY_DATE);
        currentTags.studyID = currentSlice->GetFieldValue(STUDY_ID);
        currentTags.studyInstanceUID = currentSlice->GetFieldValue(STUDY_INSTANCE_UID);
        currentTags.studyTime = currentSlice->GetFieldValue(STUDY_TIME);
            
        if( NULL != currentSlice )
        {
            delete currentSlice;
            currentSlice = NULL;
        }
        
        if( 0 == tagVector.size() )
        {
            tagVector.push_back(currentTags); 
            numberOfSlices.push_back(1);
        }
        else
        {
            bool wasMatchFound = false;
            for( unsigned int i = 0; i < tagVector.size(); i++ )
            {
                if( (currentTags.seriesInstanceUID == tagVector[i].seriesInstanceUID) 
                    && (currentTags.studyInstanceUID == tagVector[i].studyInstanceUID) )
                {                    
                    wasMatchFound = true;
                    numberOfSlices[i]++;
                    break;
                }                                    
            }            
            
            if( !wasMatchFound )
            {
                tagVector.push_back(currentTags);
                numberOfSlices.push_back(1);
            }
        }
    }
    
    cout << "Read " <<  tagVector.size() << " unique series/study UIDs: " << endl;
     
    if( NULL != dp )
    {
        free(dp);
        dp = NULL;
    }
     
    return CF_OK;
}
