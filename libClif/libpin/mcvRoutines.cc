/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* mcvRoutines.cc
   Copyright 2000: MCV
   Generic routines used by most of the MCV codes
   Created: Sept 20, 2000: JVS
   Modification History:
            23 Oct 2000: PJK: Added string.h to compile on wolf g++
            05 Sept 2002: JVS: Add getVoxelVolume
            05 April 2005: JVS: Add imageName  name to patient_path so will allow
                                computations on multiple different image sets....
            01 June 2005: JVS: turn imageName off to patient_path so maintain backwards
                               compatibility
*/
/* ***************************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utilities.h"
#include "beam.h"
#include "case_info.h"
#include "mcvRoutines.h"
#include "dose_region.h" // for prototype of getVoxelVolume
/* ***************************************************************************** */
int createFieldInfo(case_info_type *patient, beam_type *pbeam, int nBeams)
{
#ifdef DEBUG
  printf("\n createFieldInfo \n"); fflush(stdout);
#endif
   /* extract the PlanStub */
   int start=0;
   for(unsigned istr=0;istr<strlen(patient->PlanFileName)-1;istr++)
      if( patient->PlanFileName[istr] == '/' ) start=istr+1;
      strcpy(patient->PlanStub,patient->PlanFileName+start);
   /* Create the patient_path */
#ifdef DEBUG
  printf("\n Create Patient Path \n"); fflush(stdout);
#endif
#ifndef ALLOW_DIFFERENT_IMAGE
   sprintf(patient->patient_path,"%s.%s.%s",patient->PatientName,
                                                patient->PlanName,
                                                patient->TrialName);
#else
  {
    char imageStub[MAX_STR_LEN];
    int sval = strlen(patient->ImageName)-1;
    while( patient->ImageName[sval] != '/') sval--;
    strcpy(imageStub, patient->ImageName+sval+1);
    sprintf(patient->patient_path,"%s.%s.%s.%s",patient->PatientName,
                                                imageStub,
                                                patient->PlanName,
                                                patient->TrialName);
  }
#endif
  clean_name(patient->patient_path);
#ifdef DEBUG
  printf("\n Allocate Memory For beamInfo \n"); fflush(stdout);
  printf("\n nBeams = %d, sizeof(beamInfoType) = %d", nBeams, sizeof(beamInfoType));
  fflush(stdout);
#endif
   /* Allocate memory for beamInfo */
   patient->beamInfo = (beamInfoType *)calloc(nBeams, sizeof(beamInfoType));
   if(patient->beamInfo == NULL)
   {
      printf("\n ERROR: Allocating Memory for %d beamInfo", nBeams); return(FAIL);
   }
   /* Assign Beam Info */
#ifdef DEBUG
  printf("\n Assign Beam Info \n"); fflush(stdout);
#endif
   for(int iBeam=0;iBeam<nBeams;iBeam++)
   { 
      strcpy(patient->beamInfo[iBeam].beamName, pbeam[iBeam].name);
      sprintf(patient->beamInfo[iBeam].fileNameStub,"%s.%s",patient->TrialName, pbeam[iBeam].name);
      clean_name(patient->beamInfo[iBeam].fileNameStub);
   }
   /* get the voxel volume */
#ifdef DEBUG
  printf("\n Getting Voxel Volume \n"); fflush(stdout);
#endif
   if (getVoxelVolume(patient) != OK) 
   {
     printf("\n ERROR: getVoxelVolume"); return(FAIL);
   }
   return(OK);
}
/* ***************************************************************************** */
