%% Calculated the distibution for intrafraction case that we have summation
%  of subfractions motions each based on either random motion of patient or
%  linear motion from a point to a random point
% close all force
clear variables
clc
numVirtualTreatmentSim=1e4;
numFx=2;
numSubFx=25;
sysSigmaX=0;
randomSigmaX=0;
intraSigmaX=0.2;

chiPdf = @(x,k) x.^(k-1).*exp(-x.^2/2)/2^(k/2-1)/gamma(k/2);
chiCdf =@ (x,k) gammainc(k/2,x.^2/2,'upper') / gamma(k/2);
chiSquaredPdf = @(x,k) x.^(k/2-1).*exp(-x.^2/2)/2^(k/2)/gamma(k/2);
edge2binCenter = @(edges) edges(1:end-1)+ diff(edges)/2;

dx= zeros(numFx*numVirtualTreatmentSim*numSubFx,1);
iFxPortionX=zeros(numFx*numVirtualTreatmentSim*numSubFx,1);
SysPortionX=zeros(numFx*numVirtualTreatmentSim*numSubFx,1);
randomPortionX=zeros(numFx*numVirtualTreatmentSim*numSubFx,1);

flagLinearMotion=true;

sysX= sysSigmaX*randn(numVirtualTreatmentSim,1);
xIntraFinalPosition = intraSigmaX*randn(numVirtualTreatmentSim*numFx,1);
% xIntraFinalPosition=3*intraSigmaX -2*3*intraSigmaX*rand(numVirtualTreatmentSim*numFx,1);

for iSim=1:numVirtualTreatmentSim
    randomX= randomSigmaX*randn(numFx,1);
    for iFx =1:numFx
        intraX=intraSigmaX*randn(numSubFx,1);
        indexStopPoistion=(iSim-1)*numFx+iFx;
        
        startIndex=(iSim-1)*numFx*numSubFx+(iFx-1)*numSubFx+1;
        stoptIndex=(iSim-1)*numFx*numSubFx+(iFx)*numSubFx;
        if ~flagLinearMotion
            dx(startIndex:stoptIndex)=intraX+randomX(iFx)+sysX(iSim);
            iFxPortionX(startIndex:stoptIndex)= intraX;
        else
            iFxPortionX(startIndex:stoptIndex)= linspace(0,xIntraFinalPosition(indexStopPoistion),numSubFx);
            dx(startIndex:stoptIndex)= iFxPortionX(startIndex:stoptIndex)+randomX(iFx)+sysX(iSim);
            

        end
    end
    
end
[Nx,edgesX] = histcounts(iFxPortionX,linspace(1.2*min(iFxPortionX),1.2*max(iFxPortionX),50));
xDataX=edge2binCenter(edgesX)';
% plot(xDataX,Nx/max(Nx));
plot(xDataX,Nx/max(Nx));

% histogram(dx)
% figure
% histogram(iFxPortionX)

% comparing distrinutioin of  iFxPortionX  in linear motion and guassian case, 
% the linear motion is less conservative 
