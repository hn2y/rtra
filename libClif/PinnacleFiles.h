#ifndef PINNACLEFILESIF_H
#define PINNACLEFILESIF_H



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // for isspace



#include "libClif/libjvs/utilities.h"
#include "libClif/libjvs/option.h"
#include "libClif/include/read_clif.h"
#include "libClif/RtDosePinn.h"

#include "roi_type.h" // libClif/include/roi_type.h // This  contains structure for ROIs
#include "case_info.h" //libClif/include/case_info.h // Contains structure for Patient file
#include <iostream>
#include <vector>
#include <set>
#include <map>



class PinnacleFiles
{
public:
    PinnacleFiles();
    ~PinnacleFiles();


     struct freqAndConsInd{
         std::vector<unsigned int> constIndices;
         unsigned int freq;
     };

    void setPatientFilePath(std::string _patientFilePath);
    void setTrialFilePath(std::string _planDotTrailFilePath);
    void setRoiFilePath(std::string _planDotRoiFilePath);
    void setPatientFolderPath(std::string _patientFolderPath);
    void setTotalDoseFilePath(std::string _totalDoseFilePath);
    void setRtDosePinn(RtDosePinn * _rtDosePinn);
    RtDosePinn *rtDosePinn;

    //Patient and related imageSets
    int readPatientFileAndImageSets();

    // set StartWithDICOM flag -->  This flag is stored in plan.Pinnacle
    // inside each plan folder.
    // Using this scheme, all imageset would have the same coordinates and plan.roi file inside plan folder will obey them.
    // on the other and we can not have several plans for each patient with different StartWithDICOM flag.
    // Because of this each patient will have only one plan associated with it.
    //To perform studies with multiple plans, we need to have multiple patient instances which is not efficient.
    // TODO: look for better way to handle this.
    void setIsStartWithDicom(bool _IsStartWithDicom);





    // TODO : finish Writing this
    int ReadTrialList( std::string nameOfTrialToUseInTesting);


    //ROI
    int getRoiNamesVector(std::vector<std::string> &roiNamesVector);
    int printRoisDataForGivenRoiName(std::string roiName);
    int readTotalDose();
    int readRoisProperties();
    int readRoiDataForGivenRoiName(std::string ROIName);
    int readRoiDataForAllRoisOnGivenImageSetName(std::string ImageSetName);
    std::vector<roi_prop> roisPropVector;
    int deduceRoiTypeFromName();
    int calculateCenterOfMassForGivenRoiName(std::string ROIName);
    int createBitmapForGivenRoiName(std::string ROIName); // This function needs Patient and roiPropVector
    int createBitmapVoxelMassForGivenRoiName(std::string ROIName); // This function needs Patient and roiPropVector as well as calculated bitmap
    int createBitmapForAllRoisOnGivenImageSetName(std::string ImageSetName); // This function needs Patient to be loaded
    int saveBitmapForGivenRoiName(std::string bitMapFileName, std::string ROIName);
    int savePartialVolumeOFBitmapForGivenRoiName(std::string partialVolFileName, std::string ROIName);
    int saveIndicesNonzerosElemInBinaryBitmapForGivenRoiName(std::string indicesVecFileName, std::string ROIName);
    int savenumCornerInsidePolygonForNonzeroPointsOfBinaryBitmapForGivenRoiName(std::string numConrnersVecFileName, std::string ROIName);


    // Patient
    patient_type Patient;



    //General function
    void usage(); //TODO: proper comments is needed
    void setVebose(bool _verbose);
    //
    int closestNumberIndexInVectorLessThanOrEqualToValue(std::vector<float> const& vec, float value);
    int closestNumberIndexInVectorGreaterThanOrEqualToValue(std::vector<float> const& vec, float value);


    //essentially the same as overloaded version with Point_2d_float, the z coordinate is ignored
    std::vector<int> inPolygonIndices(const std::vector<Point_3d_float> &pVec, const std::vector<Point_3d_float> &polygon);
    std::vector<int> inPolygonIndices(const std::vector<Point_2d_float> &pVec, const std::vector<Point_2d_float> &polygon);
    std::vector<int> inPolygonIndices(const std::vector<Point_2d_float> &pVec, const std::vector<Point_3d_float> &polygon);


    //https://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
    bool pnpoly(int nvert, std::vector<double> &vertx, std::vector<double> &verty, double testx, double testy);

    std::vector<Point_2d_float> xyMeshGrid(std::vector<float> &xGridPts, std::vector<float> &yGridPts);
    std::vector<Point_3d_float> xyzMeshGrid(std::vector<float> &xGridPts, std::vector<float> &yGridPts, std::vector<float> &zGridPts);

    template<typename Func>
    std::vector<unsigned int> findSatisfiyingIndices(std::vector<unsigned int> &v, Func f);

    template<typename Func>
    std::vector<int> findSatisfiyingIndices(std::vector<int> &v, Func f);

    template<typename Func>
    std::vector<int> findSatisfiyingIndices(std::vector<float> &v, Func f);

    template<typename Func>
    std::vector<int> findSatisfiyingIndices(std::vector<double> &v, Func f);

    template<typename T>
    std::map<T,unsigned int> histogram(std::vector<T> &a);

    template<typename T>
    std::map<T,freqAndConsInd> histogramWithReconstructionIndices(std::vector<T> &a);

private:
    bool verbose;




    std::vector<std::string> imageSetsFilePathVector;
    std::string patientFilePath;
    std::string patientFolderPath;


    // Plan
    std::string planDotTrailFilePath; // path to the selected pinnacle plan

    // ROI
    int numRois;
    std::string planDotRoiFilePath; // path to plan.roi in the working plan folder
    
    // Total dose header file path
    std::string totalDoseFilePath; // notice Total 


};

#endif // PINNACLEFILESIF_H
