#ifndef __CFTYPEDEFS_HPP
#define __CFTYPEDEFS_HPP

#include <new>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h> // needed to use NULL
#include "cfGenDefines.h"
#include "cfClifArray.hpp" // in util/include
#include <string.h>

/**
    @file
    @ingroup INCLUDE
    @brief Templated class definitions
*/

class cfVolumeInfo;
template<class C> class cfPoint3D;
template<class C> class cfPoint2D;
template<class C> class cfVolume;
template<class C> class cfMatrix4;
template<class C> class cfBox;
template<class C> class cfTriangle;

typedef enum
{
   BYTE_ORDER_UNDEF,
   BYTE_ORDER_BIG_ENDIAN,
   BYTE_ORDER_LITTLE_ENDIAN

}cfByteOrder;

typedef enum
{
   WRITE_ALL,
   WRITE_MESH_ONLY,
   WRITE_MESH_ONLY_IN_CLONE,
   WRITE_CONTOURS_ONLY
}cfRoiPinnacleMeshControl;

template<class C> class cfPoint2D
{
public:
	// constructor
   	cfPoint2D(): x(0), y(0){ };
   	cfPoint2D(C _x, C _y): x(_x), y(_y) { }
    cfPoint2D<C> operator=(const cfPoint2D<C>& p)
    {
 	   this->x = p.x;
 	   this->y = p.y;
 	   return *this;
    }
    bool operator==(const cfPoint2D<C>& p)
    {
    	return (x==p.x && y==p.y);
    }

    C x;
    C y;
};

/** \class cfPoint3D
 * \brief Represent a 3-D point
 */
template<class C> class cfPoint3D
{
public:
	// constructor
	cfPoint3D(): x(0), y(0), z(0){ };
	cfPoint3D(C _x, C _y, C _z): x(_x), y(_y), z(_z) {}
        cfPoint3D(int value) { x = value; y = value; z = value; }

        cfPoint3D<C>& operator+=(const cfPoint3D<C> &rhs)
        {
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            return *this;
        }
        
        cfPoint3D<C>& operator/=(const cfPoint3D<C> &rhs)
        {
            x /= rhs.x;
            y /= rhs.y;
            z /= rhs.z;
            return *this;
        }

        cfPoint3D<C>& operator*=(const cfPoint3D<C> &rhs)
        {
            x *= rhs.x;
            y *= rhs.y;
            z *= rhs.z;
            return *this;
        }

        const cfPoint3D<C> operator+(const cfPoint3D<C> &p) const
        {
            cfPoint3D<C> result = *this;
            result += p;
            return result;
	}

        const cfPoint3D<C> operator*(const float value) const
	{
            cfPoint3D<C> result = *this;
            result *= value;
            return result;
	}

        // FIXME - fix the other operators
        cfPoint3D<C>& operator*=(const float value)
	{
            x *= value;
            y *= value;
            z *= value;
            return *this;
	}

        cfPoint3D<C>& operator-=(const cfPoint3D<C> &rhs)
        {
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            return *this;
        }

        const cfPoint3D<C> operator-(const cfPoint3D<C> &p) const
        {
            cfPoint3D<C> result = *this;
            result -= p;
            return result;
	}

	cfPoint3D<C> operator=(const cfPoint3D<C>& p)
	{
		this->x = p.x;
		this->y = p.y;
		this->z = p.z;
		return *this;
	}

        cfPoint3D<C> operator=(const int value)
	{
		x = value;
		y = value;
		z = value;
		return *this;
	}
	/* overload unary operator -(negative) */
	cfPoint3D<C> operator-() const
	{
            return cfPoint3D<C>(-x, -y, -z);
	}

	bool operator==(const cfPoint3D<C>& p)
	{
            return (this->x == p.x) && (this->y == p.y) && (this->z == p.z);
	}
        
        bool operator!=(const cfPoint3D<C>& p)
	{
            return (this->x != p.x) || (this->y != p.y) || (this->z != p.z);
	}
        
        void Set( C xParam, C yParam, C zParam )
        {
            x = xParam;
            y = yParam;
            z = zParam;
        }

   public:

   C x;
   C y;
   C z;

}; // cfPoint3D

/** \brief operator << for cfPoint3D
 *
 * \param output the output stream
 * \param point the 3-D point
 * \return the output stream
 */
template<class C>
std::ostream& operator<<(std::ostream& output, const cfPoint3D<C>& point)
{
	output << "(" << point.x << ", " << point.y << ", " << point.z << ")";
	return output;
}

/** \class cfMatrix4
 * \brief Represent a 4x4 matrix for transforms
 */
template<class C> class cfMatrix4
{
public:
	// constructor
	cfMatrix4()
	{
		identity();
                name = "";
	};

	void operator+=(const cfMatrix4<C>& p)
	{
		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				element[j][i] += p.element[j][i];
			}
		}
	}

	void operator-=(const cfMatrix4<C>& p)
	{
		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				element[j][i] -= p.element[j][i];
			}
		}
	}

	cfMatrix4<C> operator+(const cfMatrix4<C>& p) const
	{
		cfMatrix4<C> t;

		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				t.element[j][i] = element[j][i] + p.element[j][i];
			}
		}

		return t;
	}

	cfMatrix4<C> operator-(const cfMatrix4<C>& p) const
	{
		cfMatrix4<C> t;

		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				t.element[j][i] = element[j][i] - p.element[j][i];
			}
		}

		return t;
	}

	cfMatrix4<C> operator=(const cfMatrix4<C>& p)
	{
		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				this->element[j][i] = p.element[j][i];
			}
		}

		return *this;
	}

	cfMatrix4<C> operator*(const cfMatrix4<C>& p) const
	{
		cfMatrix4<C> t;
		t.null();

		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				for(int k=0;k<4;k++) {
					t.element[i][j] += element[i][k] * p.element[k][j];
				}
			}
		}

		return t;
	}       

	//transpose
	void transpose()
	{
		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				element[j][i] = element[i][j];
			}
		}
	}

	//null
	void null()
	{
		for(int j=0;j<4;j++) {
			for(int i=0;i<4;i++) {
				element[j][i] = 0;
			}
		}
	}

	//identity
	void identity()
	{
            //set to identity
            element[0][0]=1;
            element[0][1]=0;
            element[0][2]=0;
            element[0][3]=0;

            element[1][0]=0;
            element[1][1]=1;
            element[1][2]=0;
            element[1][3]=0;

            element[2][0]=0;
            element[2][1]=0;
            element[2][2]=1;
            element[2][3]=0;

            element[3][0]=0;
            element[3][1]=0;
            element[3][2]=0;
            element[3][3]=1;
	}

        bool IsIdentity()
        {
            if(    element[0][0] == 1 && element[0][1] == 0 && element[0][2] == 0 && element[0][3] == 0
                && element[1][0] == 0 && element[1][1] == 1 && element[1][2] == 0 && element[1][3] == 0
                && element[2][0] == 0 && element[2][1] == 0 && element[2][2] == 1 && element[2][3] == 0
                && element[3][0] == 0 && element[3][1] == 0 && element[3][2] == 0 && element[3][3] == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        cfClifArray GetClifMatrix()
        {
            cfClifArray array;
            for( unsigned int i = 0; i < 4; i++ )
            {
                for( unsigned int j = 0; j < 4; j++ )
                {
                    std::stringstream stream;
                    stream << element[i][j];
                    array.AddElement(stream.str());
                }
            }
            return array;
        }
    public:

	C element[4][4];
        std::string name;       //< Name to describe the purpose of the matrix and for printing


}; // cfMatrix4

template<class C>
std::ostream& operator<<(std::ostream& output, const cfMatrix4<C>& matrix)
{
	output << matrix.name << " ={";
        for( unsigned int i = 0; i < 4; i++ )
        {
            for( unsigned int j = 0; j < 4; j++ )
            {
                output << matrix.element[i][j];
                if( i != 3 || j != 3 )
                {
                    output << ",";
                }
            }            
        }
        output << "};" << std::endl;
	return output;
}

template<class C> class cfBox
{       
    public:
    cfBox() {}
    cfBox(C minX, C minY, C minZ, C maxX, C maxY, C maxZ)
    {
        min.x = minX;
        min.y = minY;
        min.z = minZ;
        max.x = maxX;
        max.y = maxY;
        max.z = maxZ;
    }
   
    bool operator==(const cfBox<C> &rhs) const 
    {
        if( min.x == rhs.min.x && min.y == rhs.min.y && min.z == rhs.min.z &&
                max.x == rhs.max.x && max.y == rhs.max.y && max.z == rhs.max.z)
        {
            return true;
        }
        else { return false; }            
    }
    
    bool operator!=(const cfBox<C> &rhs) const 
    {
        return !(*this == rhs);
    }
   
    cfBox<C>& operator=(const cfBox<C> &rhs)
    {
        if( this == &rhs )
        {
            return *this;
        }
       
        min.x = rhs.min.x;
        min.y = rhs.min.y;
        min.z = rhs.min.z;
        max.x = rhs.max.x;
        max.y = rhs.max.y;
        max.z = rhs.max.z;
       
        return *this;
    }
   
    cfPoint3D<C> min;
    cfPoint3D<C> max;
};

template<class C>
std::ostream& operator<<(std::ostream& output, const cfBox<C>& box)
{
    output << "Box Min: " << box.min << std::endl;
    output << "Box Max: " << box.max << std::endl;
    return output;
}

template<class C> class cfTriangle
{
    public:
        cfPoint3D<C> p1;
        cfPoint3D<C> p2;
        cfPoint3D<C> p3;
};

// stores transformations, currently just shifts
// could add rotations later
class cfTransformation
{
    public:
        cfPoint3D<float> shift;
        cfMatrix4<float> rt; //rotate/translate
};

// enums
typedef enum
{
   UNKNOWN,
   CM,
   MM,
   VOXELS

}rcf_space_dims;

typedef enum
{
   IMAGE,
   DVF,
   CBPROJ,
   ROI,
   DOSE,
   BITMAP,
   PATIENT,
   PATIENTPLAN,
   TIMESERIES,
   COMPBITMAP,
   DICOMSET

}cfObjectType;

// data structure used to store volume information
class cfVolumeInfo
{
  public:
     cfVolumeInfo()
     {
        origin = cfPoint3D<float>(0, 0, 0);
        voxelSize = cfPoint3D<float>(0.0, 0.0, 0.0);
        nElements = cfPoint3D<unsigned int>(0, 0, 0);
        isUniform = cfPoint3D<bool>(true,true,true);
        slicePositionX = NULL;
        slicePositionY = NULL;
        slicePositionZ = NULL;        
        _isAllocated = CF_FAIL;
     }
    ~cfVolumeInfo()
    {
        if( NULL != slicePositionX )
        {
            delete[] slicePositionX;
            slicePositionX = NULL;
        }

        if( NULL != slicePositionY )
        {
            delete[] slicePositionY;
            slicePositionY = NULL;
        }

        if( NULL != slicePositionZ )
        {
            delete[] slicePositionZ;
            slicePositionZ = NULL;
        }
        _isAllocated = CF_FAIL;
    };

  int allocateMemory()
  {

    if(nElements.x <= 0 || nElements.y <= 0 || nElements.z <= 0)
    {
       std::cerr << "The numbers (" << nElements.x << ", " << nElements.y << ", " << nElements.z
                 << ") of nElements must greater than 0" << std::endl;
       _isAllocated = CF_FAIL;
       return CF_FAIL;
    }

    /* allocate memory for x-axis */
    try
    {
        if( NULL != slicePositionX )
        {
            delete[] slicePositionX;
        }
        slicePositionX = new float[nElements.x];
    }
    catch(std::bad_alloc&)
    {
       _isAllocated = CF_FAIL;
       std::cerr << "Error in Memory Allocation for X-axis!!!" << std::endl;
       return CF_FAIL;
    }

    /* allocate memory for y-axis. if fail, delete memory for x-axis */
    try
    {
        if( NULL != slicePositionY )
        {
            delete[] slicePositionY;
        }
        slicePositionY = new float[nElements.y];
    }
    catch(std::bad_alloc&)
    {
       _isAllocated = CF_FAIL;
       std::cerr << "Error in Memory Allocation for Y-axis!!!" << std::endl;
       delete[] slicePositionX;
       slicePositionX = NULL;
       return CF_FAIL;
    }

    /* allocate memory for z-axis. if fail, delete memory for x- and y-axis*/
    try
    {
        if( NULL != slicePositionZ )
        {
            delete[] slicePositionZ;
        }
        slicePositionZ = new float[nElements.z];
        _isAllocated = CF_OK;
    }
    catch(std::bad_alloc&)
    {
       _isAllocated = CF_FAIL;
       std::cerr << "Error in Memory Allocation for Z-axis!!!" << std::endl;
       delete[] slicePositionX;
       slicePositionX = NULL;
       delete[] slicePositionY;
       slicePositionY = NULL;
       return CF_FAIL;
    }

    _isAllocated = CF_OK;
    return CF_OK;
  }

    void operator=(const cfVolumeInfo& info)
    {
        origin = info.origin;
        voxelSize = info.voxelSize;
        nElements = info.nElements;
        isUniform = info.isUniform;

        if( CF_OK == allocateMemory() )
        {
            if( NULL == slicePositionX ||  NULL == slicePositionY || NULL == slicePositionZ )
            {
                std::cerr << "ERROR:cfVolumeInfo: At least one of the slice position arrays is NULL" << std::endl;
                return;
            }
            memcpy(slicePositionX, info.slicePositionX, (nElements.x * sizeof(float)));
            memcpy(slicePositionY, info.slicePositionY, (nElements.y * sizeof(float)));
            memcpy(slicePositionZ, info.slicePositionZ, (nElements.z * sizeof(float)));
        }
        else
        {
            std::cerr << "ERROR:cfVolumeInfo: Assignment operator could not copy data" << std::endl;
            return;
        }
    }

    bool operator==(const cfVolumeInfo& info)
    {        
        if( (origin == info.origin) && (voxelSize == info.voxelSize) && (nElements == info.nElements)
               && (isUniform == info.isUniform) )
        {
            //FIXME - check slice posistion as well?
            return true;
        }
        else
        {            
            return false;
        }
    }
    
    bool operator!=(const cfVolumeInfo& info)
    {
        return !(*this == info);
    }

    cfPoint3D<float> origin;               // origin of the image
    cfPoint3D<float> voxelSize;            // voxel sizes in x-y-z directions
    cfPoint3D<unsigned int> nElements;     // number of elements in x-y-z directions
    cfPoint3D<bool> isUniform;             // indicate whether the image is uniform in x-y-z directions
    float* slicePositionX;                  // slice positions in x directions
    float* slicePositionY;                  // slice positions in y directions
    float* slicePositionZ;                  // slice positions in z directions
  private:
    int _isAllocated;
};

// data structure store image, dose etc.
template<class C> class cfVolume
{
  public:
    cfVolume()
    {
       value = NULL;
       _isAllocated = CF_FAIL;
    }
    ~cfVolume()
    {
        if( _isAllocated != CF_FAIL )
        {
            // FIXME - FS
            // this line is causing an unexpected crash,
            // pointer looked good but still crashed on delete
            if( NULL != value )
            {
                delete[] value;
                value = NULL;
            }
            _isAllocated = CF_FAIL;
        }
    };

    int allocateMemory()
    {
       if(volume.nElements.x <= 0 ||
          volume.nElements.y <= 0 ||
          volume.nElements.z <= 0)
       {
           std::cerr << "The numbers (" << volume.nElements.x << ", " <<
                                           volume.nElements.y << ", " <<
                                           volume.nElements.z <<
                                           ") of nElements must greater than 0" << std::endl;
           _isAllocated = CF_FAIL;
           return CF_FAIL;
       }

       try
       {
          value = new C[volume.nElements.x*volume.nElements.y*volume.nElements.z];
          _isAllocated = CF_OK;
       }
       catch(std::bad_alloc&)
       {
          _isAllocated = CF_FAIL;
          std::cerr << "Error Memory Allocation for a Volume!!!" << std::endl;
          return CF_FAIL;
       }
       return CF_OK;
    }

    cfVolumeInfo   volume;             // store the information of volume, it could be image, etc.
    C*              value;             // stores the value of volume (could be intensity, dose, etc.)
  private:
    int _isAllocated;
};

#endif
