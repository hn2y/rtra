#include "RobustnessAnalyzer.h"




RobustnessAnalyzer::RobustnessAnalyzer(std::ostream &stream):logstream(stream)
{
    isVerbose= false;
    modelData=0;
    rtDose=0;
    rtStructure=0;
    numFractions=0;
    numTrials=1000;
    numDoseBins = 500;
    numVolBinForDCHandPDVH=500;
    customPlotDvhs=0;
    customPlotPDvhs=0;
    pDVHsWidget=0;
    customPlotDvhsConfIntervals=0;
    doseBinWidth=0;
    isPinnacleFlag=false;
    pinnacleFiles = NULL;
    numSubFractions=0;
    IsIntraFxRandomGaussian= false;
    resultFolderPath.clear();
}

RobustnessAnalyzer::RobustnessAnalyzer():logstream(std::cout)
{
    isVerbose= false;
    modelData=0;
    rtDose=0;
    rtStructure=0;
    numFractions=0;
    numTrials=1000;
    numDoseBins = 500;
    numVolBinForDCHandPDVH=500;
    customPlotDvhs=0;
    customPlotPDvhs=0;
    pDVHsWidget=0;
    customPlotDvhsConfIntervals=0;
    doseBinWidth=0;
    isPinnacleFlag=false;
    pinnacleFiles = NULL;
    numSubFractions=0;
    IsIntraFxRandomGaussian= false;
    resultFolderPath.clear();

}




//bool RobustnessAnalyzer::()

bool RobustnessAnalyzer::calculateDVHsMeanAndVarianceMedianPercentiles()
{
    std::string class_member = "RobustnessAnalyzer::calculateDVHsMeanAndVariance:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no trail cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;
    }

    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }


    // cDVHForTransformedPts[iRoi][iTrans*numDoseBins+iDoseBin];
    plannedDVHs.clear();
    plannedDVHs =std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    cDVHsMean.clear();
    cDVHsMean = std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    cDVHsStandardDeviation=std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    int numDvhs = numTrials;
    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        for (int iTrans = 0; iTrans < numDvhs; ++iTrans)
        {
            for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
            {
                cDVHsMean[iRoi][iDoseBin]= cDVHsMean[iRoi][iDoseBin]+ cDVHForTrial[iRoi][iTrans*numDoseBins+iDoseBin];
                if (iTrans==0)
                {
                    plannedDVHs[iRoi][iDoseBin]= cDVHForTrial[iRoi][iDoseBin];
                }
            }

        }
        // claculating mean
        std::transform(cDVHsMean[iRoi].begin(), cDVHsMean[iRoi].end(), cDVHsMean[iRoi].begin(), [numDvhs](float n){ return n/numDvhs; });

        // first sum_deviation
        for (int iTrans = 0; iTrans < numDvhs; ++iTrans)
        {
            for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
            {
                cDVHsStandardDeviation[iRoi][iDoseBin] += (cDVHForTrial[iRoi][iTrans*numDoseBins+iDoseBin]-cDVHsMean[iRoi][iDoseBin])*
                        (cDVHForTrial[iRoi][iTrans*numDoseBins+iDoseBin]-cDVHsMean[iRoi][iDoseBin]);
            }
        }

        // claculating mean
        std::transform(cDVHsStandardDeviation[iRoi].begin(), cDVHsStandardDeviation[iRoi].end(), cDVHsStandardDeviation[iRoi].begin(), [numDvhs](float n){ return sqrtf(n/numDvhs); });
    }

    // calculation median Dvh for each ROI
    cDVHsMedian.clear();
    cDVHs95thPercentile.clear();
    cDVHs5thPercentile.clear();
    cDVHsMedian = std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    cDVHs95thPercentile= std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    cDVHs5thPercentile= std::vector<std::vector<float>>(numRois,std::vector<float>(numDoseBins,0));
    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        {
            std::vector<float> sortedValuesInEachDoseBin(numDvhs,0);
            for (int iTrans = 0; iTrans < numDvhs; ++iTrans)
            {
                sortedValuesInEachDoseBin[iTrans]= cDVHForTrial[iRoi][iTrans*numDoseBins+iDoseBin];
            }


            std::sort(sortedValuesInEachDoseBin.begin(),sortedValuesInEachDoseBin.end());
            cDVHsMedian[iRoi][iDoseBin] = numDvhs % 2 ? sortedValuesInEachDoseBin[numDvhs/2] : (sortedValuesInEachDoseBin[numDvhs/2-1]+sortedValuesInEachDoseBin[numDvhs/2])/2;
            int pos95th =floor(numDvhs*0.95);
            int pos5th =floor(numDvhs*0.05);

            cDVHs95thPercentile[iRoi][iDoseBin] = sortedValuesInEachDoseBin[pos95th];
            cDVHs5thPercentile[iRoi][iDoseBin] = sortedValuesInEachDoseBin[pos5th];
            // Alternatively we could use the following code for median without sorting vector v
            //std::nth_element(v.begin(), v.begin() + v.size()/2, v.end());
            // std::cout << "The median is " << v[v.size()/2] << '\n';



            //            Percentiles
            //            Median is in fact a private case of the 50th percentile. For the range 60, 70, 89, 95, 100, the 25th percentile should be 70 because 25 percent of the elements in this range are below this value To find the element that is at the 25th percentile, use the following nth_element() call.:
            //            nth_element(grades.begin(),
            //                  grades.begin()+int((grades.size()*0.25),
            //                  grades.end());
            //            int p_25=
            //            *(grades.begin()+int(grades.size()*.25));
            //            cout<<p_25<< "is @ the 25th percentile" << endl;


        }
    }


    return true;
}

bool RobustnessAnalyzer::plotDCH(const std::string& RoiName)
{
    std::string class_member = "RobustnessAnalyzer::plotDCH:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;

    }
    if (this->cDch.size()<1)
    {
        logstream << class_member << " no cDch data has been found.  " << std::endl;

        if (!calculateDchs())
        {
            return false;
        }
    }
    int RoiNum=-1;
    numRois= 0;
    double maxDose;
    std::string patientID;
    std::string doseCalcImageSet;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
        maxDose = (double)pinnacleFiles->rtDosePinn->getMaxDose();
        patientID= pinnacleFiles->Patient.PatientID;
        doseCalcImageSet= pinnacleFiles->Patient.ImageSetList[0].ImageName;
        for (int iRoi = 0; iRoi < numRois; ++iRoi) {

            if (pinnacleFiles->roisPropVector[iRoi].name.compare(RoiName)==0)
            {
                RoiNum=iRoi;
                break;
            }
        }
        if (RoiNum==-1)
        {
            logstream << class_member << " Could not find the requested ROI in the roi list. " << std::endl;
            return false;
        }
    }
    else
    {
        numRois = rtStructure->roiVector.size();
        maxDose= rtDose->maxDose;
        patientID= rtStructure->patientID;
        doseCalcImageSet= rtDose->doseCalcImageSet;
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            if (rtStructure->roiVector[iRoi].RoiName.compare(RoiName)==0)
            {
                RoiNum=iRoi;
                break;
            }
        }
        if (RoiNum==-1)
        {
            logstream << class_member << " Could not find the requested ROI in the roi list. " << std::endl;
            return false;
        }
    }



    customPlotDch = new QCustomPlot;
    // configure axis rect:
    //customPlotDch->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlotDch->axisRect()->setupFullAxesBox(true);
    customPlotDch->xAxis->setLabel("Dose / Gy");
    customPlotDch->yAxis->setLabel("Fractional Volume");

    // set up the QCPColorMap:
    QCPColorMap *colorMap = new QCPColorMap(customPlotDch->xAxis, customPlotDch->yAxis);


    colorMap->data()->setSize(numDoseBins, numVolBinForDCHandPDVH); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(0, maxDose), QCPRange(0, 1.0)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions

    // now we assign some data, by accessing the QCPColorMapData instance of the color map:
    //    double x;
    //    double y;
    for (int iDoseBin=0; iDoseBin<numDoseBins; ++iDoseBin)
    {
        for (int iVolBin=0; iVolBin<numVolBinForDCHandPDVH; ++iVolBin)
        {
            //            colorMap->data()->cellToCoord(iDoseBin, iVolBin, &x, &y);
            //            double r = 3*qSqrt(x*x+y*y)+1e-2;
            //            z = 2*x*(qCos(r+2)/r-qSin(r+2)/r);
            colorMap->data()->setCell(iDoseBin, iVolBin, cDch[RoiNum][iDoseBin*numVolBinForDCHandPDVH+iVolBin]);
        }
    }

    // add a color scale:
    QCPColorScale *colorScale = new QCPColorScale(customPlotDch);
    customPlotDch->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("Coverage Probability");

    // set the color gradient of the color map to one of the presets:
    colorMap->setGradient(QCPColorGradient::gpJet);
    // we could have also created a QCPColorGradient instance and added own colors to
    // the gradient, see the documentation of QCPColorGradient for what's possible.

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMap->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlotDch);
    customPlotDch->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    customPlotDch->rescaleAxes();
    customPlotDch->setGeometry(400, 250, 640, 480);
    customPlotDch->yAxis->setScaleRatio(customPlotDch->xAxis, 1.0);
    customPlotDch->xAxis->setRange(0, maxDose);
    customPlotDch->yAxis->setRange(0, 1.0);
    customPlotDch->plotLayout()->insertRow(0);
    customPlotDch->plotLayout()->addElement(0, 0, new QCPPlotTitle(customPlotDch,  RoiName.c_str()));
    customPlotDch->axisRect()->setupFullAxesBox();

    customPlotDch->replot();
    //customPlotDch->show();

    return true;
}

bool RobustnessAnalyzer::calculatePDVHsFromDCHsAtSpecifiedConfLevel()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHsFromDCHsAtSpecifiedConfLevel:" ;
    if (cDch.size() <1)
    {
        logstream << class_member << "ERORR --> no cDch data has been found. RobustnessAnalyzer->cDch is empty! " << std::endl;
        return false;
    }

    if (desiredPDVHsConfidenceLevel.size()<1)
    {
        logstream << class_member << " ERORR --> no desiredPDVHsConfidenceLevel data has been found. RobustnessAnalyzer->desiredPDVHsConfidenceLevel is empty! " << std::endl;
        return false;
    }
    pDVHs.clear();

    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }
    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        pDVHs.push_back( std::vector<float>(numDoseBins,0));

        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        {

            for (int iVolBin = 0; iVolBin < numVolBinForDCHandPDVH; ++iVolBin)
            {

                if (cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+numVolBinForDCHandPDVH-1]>=static_cast<float>(desiredPDVHsConfidenceLevel[iRoi])/100.0) // this means the whole volume is covered with the desired level probability
                {
                    pDVHs[iRoi][iDoseBin]=1.0;
                    break;
                }


                if (cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolBin] < static_cast<float>(desiredPDVHsConfidenceLevel[iRoi])/100.0)
                {
                    bool isTargetVolume;
                    if (isPinnacleFlag)
                    {
                        isTargetVolume = pinnacleFiles->roisPropVector[iRoi].IsTargetVolume;
                    }
                    else
                    {
                        isTargetVolume = rtStructure->roiVector[iRoi].IsTargetVolume;
                    }
                    if (isTargetVolume)
                    {
                        pDVHs[iRoi][iDoseBin] = static_cast<float>(iVolBin)/static_cast<float>(numVolBinForDCHandPDVH);

                    }
                    else
                    {
                        pDVHs[iRoi][iDoseBin] = static_cast<float>(iVolBin)/static_cast<float>(numVolBinForDCHandPDVH);
                    }
                    break;

                }


            }
        }

        //        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        //        {
        //            logstream << "For ROI: " << rtStructure->roiVector[iRoi].RoiName << " At dose bin : " << iDoseBin  << " pDVH = " << pDVHs[iRoi][iDoseBin]
        //                         << " and  dch[end]="  << cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+numVolBinForDCHandPDVH-1]<< std::endl;
        //        }


    }


    return true;
}

std::vector<Qt::PenStyle> RobustnessAnalyzer::genPenStyles()
{

    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }

    std::vector<Qt::PenStyle> penStyles;
    for (int iPenStyle = 1; iPenStyle < numRois+2; ++iPenStyle)
    {
        int rem;
        if (iPenStyle> 5)
        {
            rem=iPenStyle%5;
            if (rem ==0)
            {
                rem=5;
            }


        }
        else
        {
            rem=iPenStyle;
        }

        penStyles.push_back(static_cast<Qt::PenStyle>(rem));
    }

    return penStyles;
}

bool RobustnessAnalyzer::plotDVHsConfInterval()
{
    std::string class_member = "RobustnessAnalyzer::plotDVHsConfInterval:" ;
    if (this->cDVHsMean.size()<1)
    {
        logstream << class_member << " no cDVHsMean data has been found. RobustnessAnalyzer->cDVHsMean is empty! " << std::endl;
        return false;
    }

    if (this->cDVHsStandardDeviation.size()<1)
    {
        logstream << class_member << " no cDVHsStandardDeviation data has been found. RobustnessAnalyzer->cDVHsStandardDeviation is empty! " << std::endl;
        return false;
    }
    // check if there is an existing customPlotDvhsConfIntervals object
    if (customPlotDvhsConfIntervals!=0)
    {
        delete(customPlotDvhsConfIntervals);
    }
    QVector<double> dvhsIntervalXvalues(numDoseBins);

    for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
    {
        dvhsIntervalXvalues[iDoseBin] = doseBinCenters[iDoseBin];
    }

    std::vector<Qt::PenStyle> penStyles = genPenStyles();
    customPlotDvhsConfIntervals = new QCustomPlot();
    customPlotDvhsConfIntervals->legend->clear();
    customPlotDvhsConfIntervals->setAutoAddPlottableToLegend(0);


    numRois= 0;
    double maxDose;
    std::string patientID;
    std::string doseCalcImageSet;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
        maxDose = (double)pinnacleFiles->rtDosePinn->getMaxDose();
        patientID= pinnacleFiles->Patient.PatientID;
        doseCalcImageSet= pinnacleFiles->Patient.ImageSetList[0].ImageName;

    }
    else
    {
        numRois = rtStructure->roiVector.size();
        maxDose= rtDose->maxDose;
        patientID= rtStructure->patientID;
        doseCalcImageSet= rtDose->doseCalcImageSet;
    }


    for (int iRoi = 0; iRoi < numRois; iRoi++)
    {
        std::vector<unsigned int> DisplayColor;
        std::string RoiName;
        if (isPinnacleFlag)
        {
            DisplayColor = pinnacleFiles->roisPropVector[iRoi].DisplayColor;
            RoiName = pinnacleFiles->roisPropVector[iRoi].name;
        }
        else
        {
            DisplayColor = rtStructure->roiVector[iRoi].DisplayColor;
            RoiName = rtStructure->roiVector[iRoi].RoiName;
        }

        auto qPenMeanDvh = QPen(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2], 255));
        //qPenMeanDvh.setStyle(penStyles[iRoi+1]);
        qPenMeanDvh.setStyle(Qt::DashLine);
        qPenMeanDvh.setWidth(2.5);
        //cDVHForTransformedPts[iRoi]

        auto qPenPlannedDvh = QPen(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2], 255));
        qPenPlannedDvh.setStyle(Qt::SolidLine);
        qPenPlannedDvh.setWidth(2.5);

        auto qPenUpperAndLowerCofBand = QPen(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2], 200));
        qPenUpperAndLowerCofBand.setStyle(Qt::DotLine);
        qPenUpperAndLowerCofBand.setWidth(1);






        // numDoseBins
        QVector<double> dvhInterMeanValY(numDoseBins);
        QVector<double> dvhPlannedY(numDoseBins);
        // add confidence band graphs:
        QVector<double> dvhsUpperConfidenceBandY(numDoseBins);
        QVector<double> dvhsLowerConfidenceBandY(numDoseBins);

        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        {
            dvhInterMeanValY[iDoseBin] =static_cast<double>(cDVHsMean[iRoi][iDoseBin]);
            // dvhsUpperConfidenceBandY[iDoseBin] =static_cast<double>(cDVHsMean[iRoi][iDoseBin]+2*cDVHsStandardDeviation[iRoi][iDoseBin]);
            //dvhsLowerConfidenceBandY[iDoseBin] =static_cast<double>(cDVHsMean[iRoi][iDoseBin]-2*cDVHsStandardDeviation[iRoi][iDoseBin]);
            dvhsUpperConfidenceBandY[iDoseBin] =static_cast<double>(cDVHs95thPercentile[iRoi][iDoseBin]);
            dvhsLowerConfidenceBandY[iDoseBin] =static_cast<double>(cDVHs5thPercentile[iRoi][iDoseBin]);
            //  dvhPlannedY[iDoseBin] = static_cast<double>(cDVHForTrial[iRoi][iDoseBin]); // the first DVH has to be the planned Dvh
            dvhPlannedY[iDoseBin] =  static_cast<double>(plannedDVHs[iRoi][iDoseBin]); // the first DVH has to be the planned Dvh

        }

        // Adding mean graph
        customPlotDvhsConfIntervals->addGraph();
        customPlotDvhsConfIntervals->graph(iRoi*4)->setData(dvhsIntervalXvalues,dvhInterMeanValY);
        customPlotDvhsConfIntervals->graph(iRoi*4)->setPen(qPenMeanDvh);
        // set name and legend of mean Graph
        customPlotDvhsConfIntervals->graph(iRoi*4)->setName(QString(RoiName.c_str())+" M");
        //customPlotDvhsConfIntervals->legend->addItem(new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4)));

        // Adding Planned DVH graph
        customPlotDvhsConfIntervals->addGraph();
        customPlotDvhsConfIntervals->graph(iRoi*4+1)->setData(dvhsIntervalXvalues,dvhPlannedY);
        customPlotDvhsConfIntervals->graph(iRoi*4+1)->setPen(qPenPlannedDvh);
        customPlotDvhsConfIntervals->graph(iRoi*4+1)->setName(QString(RoiName.c_str()));
        //customPlotDvhsConfIntervals->legend->addItem(new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4+1)));



        // Adding upper Confidence Band graph
        customPlotDvhsConfIntervals->addGraph();
        customPlotDvhsConfIntervals->graph(iRoi*4+2)->setData(dvhsIntervalXvalues,dvhsUpperConfidenceBandY);
        customPlotDvhsConfIntervals->graph(iRoi*4+2)->setPen(qPenUpperAndLowerCofBand);
        customPlotDvhsConfIntervals->graph(iRoi*4+2)->setBrush(QBrush(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2],150)));

        // Adding Lower Confidence Band graph
        customPlotDvhsConfIntervals->addGraph();
        customPlotDvhsConfIntervals->graph(iRoi*4+3)->setData(dvhsIntervalXvalues,dvhsLowerConfidenceBandY);
        customPlotDvhsConfIntervals->graph(iRoi*4+3)->setPen(qPenUpperAndLowerCofBand);

        // fill band
        customPlotDvhsConfIntervals->graph(iRoi*4+2)->setChannelFillGraph(customPlotDvhsConfIntervals->graph(iRoi*4+3));

        // Add legend for band only for the last Roi
        if (iRoi==numRois-1)
        {


            customPlotDvhsConfIntervals->graph(iRoi*4+2)->setName(QString("Conf. Band 90%"));
            //            customPlotDvhsConfIntervals->legend->addItem(new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4+2)));
            customPlotDvhsConfIntervals->legend->addElement(0,0,new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4+2)));


            auto qPenMeanDvhLegend = QPen(QColor(0,0,0));
            qPenMeanDvhLegend.setStyle(Qt::DashLine);
            qPenMeanDvhLegend.setWidth(2.5);
            customPlotDvhsConfIntervals->addGraph();
            customPlotDvhsConfIntervals->graph(iRoi*4+4)->setPen(qPenMeanDvhLegend);
            customPlotDvhsConfIntervals->graph(iRoi*4+4)->setName(QString("Mean"));
            QCPPlottableLegendItem* meanDvhLegendItem= new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4+4));
            meanDvhLegendItem->setTextColor(QColor(0,0,0));
            //customPlotDvhsConfIntervals->legend->addItem(meanDvhLegendItem);
            customPlotDvhsConfIntervals->legend->addElement(0,1,meanDvhLegendItem);



            auto qPenPlanedDvhLegend = QPen(QColor(0,0,0));
            qPenPlanedDvhLegend.setStyle(Qt::SolidLine);
            qPenPlanedDvhLegend.setWidth(2.5);
            customPlotDvhsConfIntervals->addGraph();
            customPlotDvhsConfIntervals->graph(iRoi*4+5)->setPen(qPenPlanedDvhLegend);
            customPlotDvhsConfIntervals->graph(iRoi*4+5)->setName(QString("Plan"));
            QCPPlottableLegendItem* planedDvhLegendItem= new QCPPlottableLegendItem(customPlotDvhsConfIntervals->legend, customPlotDvhsConfIntervals->graph(iRoi*4+5));
            planedDvhLegendItem->setTextColor(QColor(0,0,0));
            //            customPlotDvhsConfIntervals->legend->addItem(planedDvhLegendItem);
            customPlotDvhsConfIntervals->legend->addElement(0,2,planedDvhLegendItem);

        }

    }

    customPlotDvhsConfIntervals->setFixedSize(800, 600);
    customPlotDvhsConfIntervals->xAxis->setRange(0, maxDose*1.02);
    customPlotDvhsConfIntervals->yAxis->setRange(0, 1.03);
    customPlotDvhsConfIntervals->xAxis->setLabel("Dose / Gy");
    customPlotDvhsConfIntervals->yAxis->setLabel("Fractional Volume");

    customPlotDvhsConfIntervals->xAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold)); // 1 means bold
    customPlotDvhsConfIntervals->yAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold)); // 1 means bold

    // set legend
    customPlotDvhsConfIntervals->legend->setVisible(true);
    auto legendFont = QFont("Helvetica", 10);
    legendFont.setBold(1);
    customPlotDvhsConfIntervals->legend->setFont(legendFont);
    auto qLegendBorderPen = QPen(QColor(0,0,0,255));
    qLegendBorderPen.setWidth(2.5);
    customPlotDvhsConfIntervals->legend->setBorderPen(qLegendBorderPen);

    // set title of plot:
    customPlotDvhsConfIntervals->plotLayout()->insertRow(0);
    customPlotDvhsConfIntervals->plotLayout()->addElement(0, 0, new QCPPlotTitle(customPlotDvhsConfIntervals,  "DVHs Confidence Intervals"));

    // set legends to be outside the graph on the top
    QCPLayoutGrid *subLayout = new QCPLayoutGrid;
    subLayout->addElement(0, 0, new QCPLayoutElement);
    subLayout->addElement(0, 1, customPlotDvhsConfIntervals->legend);
    subLayout->addElement(0, 2, new QCPLayoutElement);
    customPlotDvhsConfIntervals->plotLayout()->insertRow(1);
    customPlotDvhsConfIntervals->plotLayout()->addElement(1, 0, subLayout);
    customPlotDvhsConfIntervals->plotLayout()->setRowStretchFactor(1,0.001);

    customPlotDvhsConfIntervals->axisRect()->setupFullAxesBox();
    customPlotDvhsConfIntervals->xAxis->setBasePen(qLegendBorderPen);
    customPlotDvhsConfIntervals->xAxis2->setBasePen(qLegendBorderPen);
    customPlotDvhsConfIntervals->yAxis->setBasePen(qLegendBorderPen);
    customPlotDvhsConfIntervals->yAxis2->setBasePen(qLegendBorderPen);
    customPlotDvhsConfIntervals->xAxis->setTickPen(qLegendBorderPen);
    customPlotDvhsConfIntervals->xAxis2->setTickPen(qLegendBorderPen);
    customPlotDvhsConfIntervals->yAxis->setTickPen(qLegendBorderPen);
    customPlotDvhsConfIntervals->yAxis2->setTickPen(qLegendBorderPen);
    customPlotDvhsConfIntervals->yAxis->setTickLabelFont(legendFont);
    customPlotDvhsConfIntervals->xAxis->setTickLabelFont(legendFont);
    QVector<double> volTicks;
    for (int iVolTick = 0; iVolTick <= 10 ; ++iVolTick)
    {
        volTicks << static_cast<double>(iVolTick)*0.1;
    }

    customPlotDvhsConfIntervals->yAxis->setTickVector(volTicks);





    customPlotDvhsConfIntervals->replot();
    //    customPlotDvhsConfIntervals->show();
    std::string fileNameString =resultFolderPath + "/DVHsConfInterval_"+patientID+"_Plan_"+doseCalcImageSet+ "_" +nowInString()+".pdf";
    customPlotDvhsConfIntervals->savePdf(QString(fileNameString.c_str())); // TODO: get it from the user
    return true;
}

bool RobustnessAnalyzer::plotDVHs()
{
    std::string class_member = "RobustnessAnalyzer::plotDVHs:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;

    }

    // check if there is an existing customPlot object
    if (customPlotDvhs!=0)
    {
        delete(customPlotDvhs);
    }

    QVector<double> dvhXvalues(numDoseBins);
    //this->doseBinCenters
    for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
    {
        dvhXvalues[iDoseBin] = doseBinCenters[iDoseBin];
    }
    

    std::vector<Qt::PenStyle> penStyles = genPenStyles();



    numRois= 0;
    double maxDose;
    std::string patientID;
    std::string doseCalcImageSet;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
        maxDose = (double)pinnacleFiles->rtDosePinn->getMaxDose();
        patientID= pinnacleFiles->Patient.PatientID;
        doseCalcImageSet= pinnacleFiles->Patient.ImageSetList[0].ImageName;
    }
    else
    {
        numRois = rtStructure->roiVector.size();
        maxDose= rtDose->maxDose;
        patientID= rtStructure->patientID;
        doseCalcImageSet= rtDose->doseCalcImageSet;
    }


    customPlotDvhs = new QCustomPlot();
    customPlotDvhs->legend->clear();
    customPlotDvhs->setAutoAddPlottableToLegend(0);
    int numDvhs = numFractions*numTrials;
    for (int iRoi = 0; iRoi < numRois; iRoi++)
    {
        std::vector<unsigned int> DisplayColor;
        std::string RoiName;
        if (isPinnacleFlag)
        {
            DisplayColor = pinnacleFiles->roisPropVector[iRoi].DisplayColor;
            RoiName = pinnacleFiles->roisPropVector[iRoi].name;
        }
        else
        {
            DisplayColor = rtStructure->roiVector[iRoi].DisplayColor;
            RoiName = rtStructure->roiVector[iRoi].RoiName;
        }

        auto qPen = QPen(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2], 255));
        qPen.setStyle(penStyles[iRoi]);
        qPen.setWidth(2.5);
        //cDVHForTransformedPts[iRoi]

        for (int iTrans = 0; iTrans < numDvhs; ++iTrans)
        {
            // numDoseBins
            QVector<double> dvhYvalues(numDoseBins); //  QVector<float>::fromStdVector(&cDVHForTransformedPts[iRoi][iTrans*numDoseBins]);

            for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
            {
                dvhYvalues[iDoseBin] =static_cast<double>(cDVHForTrial[iRoi][iTrans*numDoseBins+iDoseBin]);

            }


            customPlotDvhs->addGraph();
            customPlotDvhs->graph(iTrans+iRoi* numDvhs)->setData(dvhXvalues,dvhYvalues);
            customPlotDvhs->graph(iTrans+iRoi* numDvhs)->setPen(qPen);


            if (iTrans==0)
            {
                customPlotDvhs->graph(iTrans+iRoi* numDvhs)->setName(QString(RoiName.c_str()));
                customPlotDvhs->legend->addItem(new QCPPlottableLegendItem(customPlotDvhs->legend, customPlotDvhs->graph(iTrans+iRoi* numDvhs)));

                // customPlot->legend->elementAt(iRoi)->
            }


        }

        
        //customPlot->replot();
    }



    customPlotDvhs->setGeometry(400, 250, 900, 650);
    customPlotDvhs->xAxis->setRange(0, maxDose*1.55);
    customPlotDvhs->yAxis->setRange(0, 1.03);
    customPlotDvhs->xAxis->setLabel("Dose (Gy)");
    customPlotDvhs->yAxis->setLabel("Vol (%)");
    customPlotDvhs->xAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold)); // 1 means bold
    customPlotDvhs->yAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold)); // 1 means bold

    // set legend
    customPlotDvhs->legend->setVisible(false);
    auto legendFont = QFont("Helvetica", 6);
    legendFont.setBold(1);
    customPlotDvhs->legend->setFont(legendFont);
    auto qLegendBorderPen = QPen(QColor(0,0,0,255));
    qLegendBorderPen.setWidth(2.5);
    customPlotDvhs->legend->setBorderPen(qLegendBorderPen);



    // set title of plot:
    customPlotDvhs->plotLayout()->insertRow(0);
    customPlotDvhs->plotLayout()->addElement(0, 0, new QCPPlotTitle(customPlotDvhs, concatChar( "DVHs of Patient:" ,patientID.c_str())));
    customPlotDvhs->axisRect()->setupFullAxesBox();
    customPlotDvhs->xAxis->setBasePen(qLegendBorderPen);
    customPlotDvhs->xAxis2->setBasePen(qLegendBorderPen);
    customPlotDvhs->yAxis->setBasePen(qLegendBorderPen);
    customPlotDvhs->yAxis2->setBasePen(qLegendBorderPen);
    customPlotDvhs->xAxis->setTickPen(qLegendBorderPen);
    customPlotDvhs->xAxis2->setTickPen(qLegendBorderPen);
    customPlotDvhs->yAxis->setTickPen(qLegendBorderPen);
    customPlotDvhs->yAxis2->setTickPen(qLegendBorderPen);
    customPlotDvhs->yAxis->setTickLabelFont(legendFont);
    customPlotDvhs->xAxis->setTickLabelFont(legendFont);


    customPlotDvhs->replot();
    customPlotDvhs->show();
    //https://kjellkod.wordpress.com/2013/01/22/exploring-c11-part-2-localtime-and-time-again/

    //nowInString.erase(std::remove_if(nowInString.begin(), nowInString.end(), isspace), nowInString.end());

    //std::string fileNameString ="./DVHs_"+rtStructure->patientID + nowInString+".pdf";
    std::string fileNameString =resultFolderPath + "/DVHs_"+patientID+"_"+nowInString()+".pdf";
    customPlotDvhs->savePdf(QString(fileNameString.c_str())); // TODO: get it from the user

    return true;

}

bool RobustnessAnalyzer::calculateAllDVHsOnGpu()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHsOnGpu:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }
    if (rtDose==0)
    {
        logstream << class_member << " rtDose has not been set." << std::endl;
        return false;
    }

    if (rtStructure==0)
    {
        logstream << class_member << " rtStructure has not been set." << std::endl;
        return false;
    }
    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }


    if (doseBinCenters.size()==0)
    {
        logstream << class_member << "doseBinCenters has not been set. We are going to automatically set it based on rtDose max dose. " << std::endl;

        if (numDoseBins<1)
        {
            logstream << class_member << "ERROR numDoseBins< 1 " << std::endl;
            return false;
        }
        doseBinWidth = (rtDose->maxDose*1.02)/ numDoseBins;
        logstream << class_member << " doseBinWidth = (rtDose->maxDose*1.02)/ numDoseBins = " << doseBinWidth << std::endl;
        doseBinCenters = generateRange(doseBinWidth / 2.0, doseBinWidth, rtDose->maxDose*1.02);
        if (doseBinCenters.size()!=numDoseBins)
        {
            logstream << class_member << "doseBinCenters.size()!=numDoseBins, debug the code!" << std::endl;
            return false;
        }
    }
    else
    {
        if (doseBinCenters.size()<2)
        {
            logstream << class_member << "ERROR doseBinCenters length is less than 2." << std::endl;
            return false;
        }

        doseBinWidth=doseBinCenters[1]-doseBinCenters[0];
        if (doseBinWidth/2!=doseBinCenters[0])
        {
            logstream << class_member << "doseBinCenters are not set properly, they should follow this pattern [doseBinWidth/2,doseBinWidth+doseBinWidth/2,2*doseBinWidth+doseBinWidth/2,...]  ." << std::endl;

        }

        for (int iBin = 0; iBin < doseBinCenters.size()-1; ++iBin) {

            if (fabs(fabs(doseBinCenters[iBin+1]-doseBinCenters[iBin]) -doseBinWidth)>1e-6)
            {
                logstream << class_member << "doseBinCenters are not equispaced." << std::endl;
                return false;
            }

        }
        numDoseBins= doseBinCenters.size();
        if (numDoseBins<2)
        {
            // this should not happen but it is being checked anyway.
            logstream << class_member << "ERROR numDoseBins< 2 " << std::endl;
            return false;
        }
    }

    //writeVectorToAsciiFile("doseBinCenters.txt", doseBinCenters); // TODO : make it selectable in the gui

    auto deviceInfo = gpusAndCpusInfo();

    if (!deviceInfo.getNumCpuAndGpus())
    {
        return false;
    }




    rtDose->doseArrayBoundingBox.maxX = thrust::reduce(rtDose->doseArrayCoordinatesX.begin(), rtDose->doseArrayCoordinatesX.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    rtDose->doseArrayBoundingBox.minX = thrust::reduce(rtDose->doseArrayCoordinatesX.begin(), rtDose->doseArrayCoordinatesX.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    rtDose->doseArrayBoundingBox.maxY = thrust::reduce(rtDose->doseArrayCoordinatesY.begin(), rtDose->doseArrayCoordinatesY.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    rtDose->doseArrayBoundingBox.minY = thrust::reduce(rtDose->doseArrayCoordinatesY.begin(), rtDose->doseArrayCoordinatesY.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    rtDose->doseArrayBoundingBox.maxZ = thrust::reduce(rtDose->doseArrayCoordinatesZ.begin(), rtDose->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    rtDose->doseArrayBoundingBox.minZ = thrust::reduce(rtDose->doseArrayCoordinatesZ.begin(), rtDose->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());


    std::vector<float> fractionsDoseWeights(numFractions,1/static_cast<float>(numFractions));


    //    // Memory required to store roiModel.inPolygonPoints into GPU for each ROI
    cDVHForTrial.clear();
    diffDVHForTrial.clear();
    voxelDoseMeanForTrials.clear();
    voxelDoseMeanForTrials.clear();

    for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); iRoi++)
    {
        if (isVerbose)
        {
            logstream << class_member << "Calculating memory requirement for ROI " << iRoi << " On GPU (float each number 4 bytes)" << std::endl;
        }
        { // this bracket to tell compiler to clean up tempBinDoseVec
            std::vector<float> tempTrailDvhs(numDoseBins*numTrials, 0);
            cDVHForTrial.push_back(tempTrailDvhs);
            std::vector<float> tempDiffDvhs(numDoseBins*numTrials, 0);
            diffDVHForTrial.push_back(tempDiffDvhs);
            std::vector<float> tempVoxelDoseMean(rtStructure->roiVector.at(iRoi).inPolygonPointsX.size());
            voxelDoseMeanForTrials.push_back(tempVoxelDoseMean);
            voxelDoseStdForTrials.push_back(tempVoxelDoseMean);
        }


        calculateAllDiffAndCulmulativeDvhs(cDVHForTrial[iRoi],
                                           diffDVHForTrial[iRoi],
                                           voxelDoseMeanForTrials[iRoi],
                                           voxelDoseStdForTrials[iRoi],
                                           fractionsDoseWeights,
                                           rtDose->doseArray,
                                           rtDose->doseGridXvalues,
                                           rtDose->doseGridYvalues,
                                           rtDose->doseGridZvalues,
                                           rtStructure->roiVector.at(iRoi).inPolygonPointsX,
                                           rtStructure->roiVector.at(iRoi).inPolygonPointsY,
                                           rtStructure->roiVector.at(iRoi).inPolygonPointsZ,
                                           simulationModelParams.at(iRoi).RotationMatrices,
                                           simulationModelParams.at(iRoi).TranslationVectors,
                                           numDoseBins,
                                           doseBinWidth);



    } // iRoi ends here


    return true;
}

bool RobustnessAnalyzer::calculateAllDVHSonGpuForPinnaclePatient()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHsOnGpu:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }

    if (pinnacleFiles==NULL)
    {
        logstream << class_member << " pinnacleFiles pointer has not been set." << std::endl;
        return false;
    }

    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }


    if (pinnacleFiles->rtDosePinn==NULL)
    {
        logstream << class_member << " rtDose pointer has not been set for PinnacleFiles object." << std::endl;
        return false;
    }

    if (pinnacleFiles->roisPropVector.size() ==0)
    {
        logstream << class_member << " No roi is found in roisPropVector in PinnacleFiles --> forgot to set and compute roi props?." << std::endl;
        return false;

    }

    if (doseBinCenters.size()==0)
    {
        logstream << class_member << "doseBinCenters has not been set. We are going to automatically set it based on rtDose max dose. " << std::endl;

        if (numDoseBins<1)
        {
            logstream << class_member << "ERROR numDoseBins< 1 " << std::endl;
            return false;
        }
        doseBinWidth = (pinnacleFiles->rtDosePinn->getMaxDose()*1.02)/ numDoseBins;
        logstream << class_member << " doseBinWidth = (pinnacleFiles->rtDosePinn->maxDose*1.02)/ numDoseBins = " << doseBinWidth << std::endl;
        doseBinCenters = generateRange(doseBinWidth / 2.0, doseBinWidth, pinnacleFiles->rtDosePinn->getMaxDose()*1.02);
        if (doseBinCenters.size()!=numDoseBins)
        {
            logstream << class_member << "doseBinCenters.size()!=numDoseBins, debug the code!" << std::endl;
            return false;
        }
    }
    else
    {
        if (doseBinCenters.size()<2)
        {
            logstream << class_member << "ERROR doseBinCenters length is less than 2." << std::endl;
            return false;
        }

        doseBinWidth=doseBinCenters[1]-doseBinCenters[0];
        if (doseBinWidth/2!=doseBinCenters[0])
        {
            logstream << class_member << "doseBinCenters are not set properly, they should follow this pattern [doseBinWidth/2,doseBinWidth+doseBinWidth/2,2*doseBinWidth+doseBinWidth/2,...]  ." << std::endl;

        }

        for (int iBin = 0; iBin < doseBinCenters.size()-1; ++iBin) {

            if (fabs(fabs(doseBinCenters[iBin+1]-doseBinCenters[iBin]) -doseBinWidth)>1e-6)
            {
                logstream << class_member << "doseBinCenters are not equispaced." << std::endl;
                return false;
            }

        }
        numDoseBins= doseBinCenters.size();
        if (numDoseBins<2)
        {
            // this should not happen but it is being checked anyway.
            logstream << class_member << "ERROR numDoseBins< 2 " << std::endl;
            return false;
        }
    }

    writeVectorToAsciiFile("doseBinCenters.txt", doseBinCenters); // TODO : make it selectable in the gui
    auto deviceInfo = gpusAndCpusInfo();

    if (!deviceInfo.getNumCpuAndGpus())
    {
        return false;
    }

    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxX = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minX = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxY = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minY = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxZ = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minZ = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());

    std::vector<float> fractionsDoseWeights(numFractions,1/static_cast<float>(numFractions));


    //    // Memory required to store roiModel.inPolygonPoints into GPU for each ROI
    cDVHForTrial.clear();
    diffDVHForTrial.clear();
    voxelDoseMeanForTrials.clear();
    voxelDoseMeanForTrials.clear();



    for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); iRoi++)
    {
        logstream << class_member << "Calculating DVHs and DMHs for Roi[" << iRoi <<"] = "<< pinnacleFiles->roisPropVector[iRoi].name << std::endl;

        if (isVerbose)
        {
            logstream << class_member << "Calculating memory requirement for ROI " << iRoi << " On GPU (float each number 4 bytes)" << std::endl;
        }
        { // this bracket to tell compiler to clean up tempBinDoseVec
            std::vector<float> tempTrailDvhs(numDoseBins*numTrials, 0);
            cDVHForTrial.push_back(tempTrailDvhs);
            std::vector<float> tempDiffDvhs(numDoseBins*numTrials, 0);
            diffDVHForTrial.push_back(tempDiffDvhs);
            std::vector<float> tempVoxelDoseMean(pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesX.size());
            voxelDoseMeanForTrials.push_back(tempVoxelDoseMean);
            voxelDoseStdForTrials.push_back(tempVoxelDoseMean);
            cDMHForTrial.push_back(std::vector<float>(numDoseBins*numTrials, 0));
            diffDMHForTrial.push_back(std::vector<float>(numDoseBins*numTrials, 0));
        }

        calculateAllDiffAndCulmulativeDvhsForPinnacle(cDVHForTrial[iRoi],
                                                      diffDVHForTrial[iRoi],
                                                      voxelDoseMeanForTrials[iRoi],
                                                      voxelDoseStdForTrials[iRoi],
                                                      fractionsDoseWeights,
                                                      pinnacleFiles->rtDosePinn->doseArray,
                                                      pinnacleFiles->rtDosePinn->doseGridXvalues,
                                                      pinnacleFiles->rtDosePinn->doseGridYvalues,
                                                      pinnacleFiles->rtDosePinn->doseGridZvalues,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesX,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesY,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesZ,
                                                      pinnacleFiles->roisPropVector.at(iRoi).partialVolumeOfBinaryBitmapVoxels,
                                                      simulationModelParams.at(iRoi).RotationMatrices,
                                                      simulationModelParams.at(iRoi).TranslationVectors,
                                                      numDoseBins,
                                                      doseBinWidth,
                                                      isVerbose );
        // DMH : TODO: This could be done with one call since all the information is in GPU when calculating DVHs
        calculateAllDiffAndCulmulativeDvhsForPinnacle(cDMHForTrial[iRoi],                                           // the only difference wrt dvh call
                                                      diffDMHForTrial[iRoi],                                        // the only difference wrt dvh call
                                                      voxelDoseMeanForTrials[iRoi],
                                                      voxelDoseStdForTrials[iRoi],
                                                      fractionsDoseWeights,
                                                      pinnacleFiles->rtDosePinn->doseArray,
                                                      pinnacleFiles->rtDosePinn->doseGridXvalues,
                                                      pinnacleFiles->rtDosePinn->doseGridYvalues,
                                                      pinnacleFiles->rtDosePinn->doseGridZvalues,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesX,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesY,
                                                      pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesZ,
                                                      pinnacleFiles->roisPropVector.at(iRoi).massOfBinaryBitmapVoxels, // the only difference wrt dvh call
                                                      simulationModelParams.at(iRoi).RotationMatrices,
                                                      simulationModelParams.at(iRoi).TranslationVectors,
                                                      numDoseBins,
                                                      doseBinWidth,
                                                      isVerbose);

    } // iRoi ends here

    return true;

}

bool RobustnessAnalyzer::calculateDchs()
{
    std::string class_member = "RobustnessAnalyzer::calculateDchs:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;

    }
    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }



    cDch= std::vector<std::vector <float>>(numRois,std::vector<float>(numDoseBins*numVolBinForDCHandPDVH,0));



    int numDvhs = numTrials;

    std::vector<int> EachBinVec(numDvhs,0);

    for (int iRoi = 0; iRoi < numRois; iRoi++)
    {
        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        {
            //std::fill(EachBinVec.begin(), EachBinVec.end(), 0);

            for (int iDvh = 0; iDvh < numDvhs; ++iDvh)
            {
                EachBinVec[iDvh] =static_cast<int>(floor(cDVHForTrial[iRoi][iDvh*numDoseBins+iDoseBin]*numVolBinForDCHandPDVH));
            }

            std::sort(EachBinVec.begin(), EachBinVec.end());

            // all is covered if the minimum element in dCH matrix is  numVolBinForDCHandPDVH
            if (EachBinVec[0]==numVolBinForDCHandPDVH)
            {
                for (int iVolPixelCovered = 0; iVolPixelCovered < numVolBinForDCHandPDVH; ++iVolPixelCovered)
                {
                    cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]= 1.0;
                }
                continue;
            }

            // no pixel is covered  if the last element is zero
            if (EachBinVec[numDvhs-1]==0)
            {
                for (int iVolPixelCovered = 0; iVolPixelCovered < numVolBinForDCHandPDVH; ++iVolPixelCovered)
                {
                    cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]=0.0;
                }
                continue;
            }



            for (int iDvh = 0; iDvh < numDvhs; ++iDvh)
            {
                if (EachBinVec[iDvh]==0)
                {
                    continue;
                }
                if (iDvh==0) // first when it is not zero
                {
                    for (int iVolPixelCovered = 0; iVolPixelCovered < EachBinVec[iDvh]; ++iVolPixelCovered)
                    {
                        cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]=1.0;
                    }
                    continue;
                }
                if (EachBinVec[iDvh]!=EachBinVec[iDvh-1])
                {
                    //dch_Rectal2(jDoseBin,sortedJtheBin(iDvh-1)+1:sortedJtheBin(iDvh))=(numDvhs-iDvh+1)/numDvhs;

                    for (int iVolPixelCovered = EachBinVec[iDvh-1]; iVolPixelCovered < EachBinVec[iDvh]; ++iVolPixelCovered)
                    {
                        cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]=static_cast<float>(numDvhs-iDvh)/static_cast<float>(numDvhs);
                    }
                }



            }
        }

    }

    //    //TODO:  The following method is slow  but it is better for GPU implementation
    //  std::vector<std::vector <float>>  cDch2= std::vector<std::vector <float>>(rtStructure->roiVector.size(),std::vector<float>(numDoseBins*numVolBinForDCHandPDVH,0));


    //        int numPixelCovered;
    //        for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); iRoi++)
    //        {
    //            for (int iTrans = 0; iTrans < numDvhs; ++iTrans)
    //            {
    //                for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
    //                {
    //                    //  cDch[iRoi][] = cDch[iRoi][] + cDVHForTransformedPts[iRoi][iTrans*numDoseBins+iDoseBin]*numVolBinForDCHandPDVH;

    //                    numPixelCovered = floor( cDVHForTransformedPts[iRoi][iTrans*numDoseBins+iDoseBin]*(numVolBinForDCHandPDVH));

    //                    for (int iVolPixelCovered = 0; iVolPixelCovered < numPixelCovered; ++iVolPixelCovered)
    //                    {

    //                        cDch2[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]+=1.0;
    //                    }



    //                }
    //            }
    //            std::transform(cDch2[iRoi].begin(), cDch2[iRoi].end(), cDch2[iRoi].begin(), [numDvhs](float n){ return n/numDvhs; });
    //        }

    //        for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); ++iRoi)
    //        {
    //            for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
    //            {
    //                for (int iVolPixelCovered = 0; iVolPixelCovered < numPixelCovered; ++iVolPixelCovered)
    //                {

    //                        if (abs(cDch[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered]-cDch2[iRoi][iDoseBin*numVolBinForDCHandPDVH+iVolPixelCovered])>1e-3)
    //                        {
    //                            logstream << "iRoi = " << iRoi  << "iDoseBin= " <<   iDoseBin << " iVolPixelCovered = " << iVolPixelCovered<<  std::endl;
    //                        }
    //                }
    //            }

    //        }


    return true;
}

bool RobustnessAnalyzer::calculatePDVHsConfLevelInitialValues()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHsConfLevelInitialValues:" ;


    if (rtStructure==0 & pinnacleFiles->roisPropVector.size()<1)
    {
        logstream << class_member << " rtStructure or roisPropVector  has not been set." << std::endl;
        return false;
    }


    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }

    desiredPDVHsConfidenceLevel.clear();

    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        bool IsTargetVolume;
        if (isPinnacleFlag)
        {

            IsTargetVolume= pinnacleFiles->roisPropVector[iRoi].IsTargetVolume;
        }
        else
        {
            IsTargetVolume = rtStructure->roiVector[iRoi].IsTargetVolume;
        }


        if (IsTargetVolume)
        {
            // Confidence Level for PDVHs based on the ROI's' type
            desiredPDVHsConfidenceLevel.push_back(95);


        }
        else
        {
            desiredPDVHsConfidenceLevel.push_back(5);
        }



    }




    return true;

}


void RobustnessAnalyzer::updatePDVHConfLevelForAnROI()
{



    for (int indexROI = 0; indexROI < qSliderList.size(); ++indexROI)
    {



        if (desiredPDVHsConfidenceLevel[indexROI] != qSliderList[indexROI]->value())
        {
            setDesiredPDVHsConfidenceLevels(qSliderList[indexROI]->value(), indexROI);
        }

    }

}


void RobustnessAnalyzer::setSlidersAndqSpinBoxesConfidanceValueSlot(std::vector<int> emittedDesiredPDVHSConfidenceLevels)
{
    std::string class_member = "RobustnessAnalyzer::setSlidersConfidanceValueSlot:" ;

    for (int iSlider = 0; iSlider < emittedDesiredPDVHSConfidenceLevels.size(); ++iSlider)
    {
        qSliderList[iSlider]->setValue(emittedDesiredPDVHSConfidenceLevels[iSlider]);
        qSpinBoxList[iSlider]->setValue(emittedDesiredPDVHSConfidenceLevels[iSlider]);
    }
}

void RobustnessAnalyzer::updatePDVHSGraphs()
{
    std::string class_member = "RobustnessAnalyzer::updatePDVHSGraphs:" ;
    if (!calculatePDVHsFromDCHsAtSpecifiedConfLevel())
    {
        logstream  << class_member<< " ERROR --> In calling calculatePDVHsFromDCHsAtSpecifiedConfLevel" << std::endl;
        return;
    }

    customPlotPDvhs->legend->clear();
    customPlotPDvhs->setAutoAddPlottableToLegend(0);


    std::vector<Qt::PenStyle> penStyles = genPenStyles();

    QVector<double> pdvhXvalues(numDoseBins);
    for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
    {
        pdvhXvalues[iDoseBin] = doseBinCenters[iDoseBin];
    }

    numRois= 0;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();

    }
    else
    {
        numRois = rtStructure->roiVector.size();
    }

    customPlotPDvhs->clearItems();
    for (int iRoi = 0; iRoi < numRois; iRoi++)
    {
        std::vector<unsigned int> DisplayColor;
        std::string RoiName;
        if (isPinnacleFlag)
        {
            DisplayColor = pinnacleFiles->roisPropVector[iRoi].DisplayColor;
            RoiName = pinnacleFiles->roisPropVector[iRoi].name;
        }
        else
        {
            DisplayColor = rtStructure->roiVector[iRoi].DisplayColor;
            RoiName = rtStructure->roiVector[iRoi].RoiName;
        }


        auto qPen = QPen(QColor(DisplayColor[0], DisplayColor[1], DisplayColor[2], 255));
        //        qPen.setStyle(penStyles[0]);
        qPen.setStyle(penStyles[iRoi]);
        qPen.setWidth(2.5);
        QVector<double> pdvhYvalues(numDoseBins); //  QVector<float>::fromStdVector(&cDVHForTransformedPts[iRoi][iTrans*numDoseBins]);

        for (int iDoseBin = 0; iDoseBin < numDoseBins; ++iDoseBin)
        {
            pdvhYvalues[iDoseBin] =static_cast<double>(pDVHs[iRoi][iDoseBin]);

        }
        customPlotPDvhs->addGraph();
        customPlotPDvhs->graph(iRoi)->setData(pdvhXvalues,pdvhYvalues);
        customPlotPDvhs->graph(iRoi)->setPen(qPen);
        customPlotPDvhs->graph(iRoi)->setName(QString(RoiName.c_str()));
        //customPlotPDvhs->legend->addItem(new QCPPlottableLegendItem(customPlotPDvhs->legend, customPlotPDvhs->graph(iRoi)));
        customPlotPDvhs->legend->addElement(iRoi/5,iRoi%5,new QCPPlottableLegendItem(customPlotPDvhs->legend, customPlotPDvhs->graph(iRoi)));
    }

    customPlotPDvhs->legend->setVisible(true);
    auto legendFont = QFont("Helvetica", 10);
    legendFont.setBold(1);

    customPlotPDvhs->legend->setFont(legendFont);
    auto qLegendBorderPen = QPen(QColor(0,0,0,255));
    qLegendBorderPen.setWidth(2.5);
    customPlotPDvhs->legend->setBorderPen(qLegendBorderPen);

    customPlotPDvhs->replot();
    //customPlotPDvhs->show();


}


void RobustnessAnalyzer::setDesiredPDVHsConfidenceLevels(int value, int roiIndex)
{
    std::string class_member = "RobustnessAnalyzer::setDesiredPDVHsConfidenceLevels:" ;

    if (desiredPDVHsConfidenceLevel.size()<1 | desiredPDVHsConfidenceLevel.size()<=roiIndex)
    {
        logstream  << class_member<< " ERROR --> roiIndex is invalid or desiredPDVHsConfidenceLevel is not set " << std::endl;
        return;
    }
    if ( desiredPDVHsConfidenceLevel[roiIndex] != value)
    {
        desiredPDVHsConfidenceLevel[roiIndex] = value;
        // qDebug() << "new value for slider" << roiIndex << " = " << value;
        emit emitDesiredPDVHsConfidenceLevels(desiredPDVHsConfidenceLevel);
    }
}

bool RobustnessAnalyzer::plotPDVHs()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHs:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;

    }
    if (this->cDch.size()<1)
    {
        logstream << class_member << " no cDch data has been found.  " << std::endl;

        if (!calculateDchs())
        {
            return false;
        }
    }

    if (this->desiredPDVHsConfidenceLevel.size()<1)
    {
        logstream << class_member << " desiredPDVHsConfidenceLevel has been found.  " << std::endl;
        logstream << class_member << " Attempting to initialize it based on Selected ROI's Type.  " << std::endl;

        if (!calculatePDVHsConfLevelInitialValues())
        {
            return false;
        }
    }

    if (pDVHs.size()<1)
    {

        logstream << class_member << " PDVHs has been calculated.  " << std::endl;

        if (!calculatePDVHsFromDCHsAtSpecifiedConfLevel())
        {
            logstream << class_member << " Error in calculating PDVHs for each ROI from DCHs based on  the specified confident level.  " << std::endl;
            return false;
        }

    }

    pDVHsWidget = new QWidget;

    pDVHsWidget->setWindowTitle("PDVHS");

    QGridLayout* pDVHGridlayout = new QGridLayout(pDVHsWidget);
    pDVHsWidget->setLayout(pDVHGridlayout);


    numRois= 0;
    double maxDose;
    std::string patientID;
    std::string doseCalcImageSet;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
        maxDose = (double)pinnacleFiles->rtDosePinn->getMaxDose();
        patientID= std::to_string(pinnacleFiles->Patient.PatientID);
        doseCalcImageSet= pinnacleFiles->rtDosePinn->doseCalcImageSet;
    }
    else
    {
        numRois = rtStructure->roiVector.size();
        maxDose= rtDose->maxDose;
        patientID= rtStructure->patientID;
        doseCalcImageSet= rtDose->doseCalcImageSet;
    }


    qSpinBoxList.clear();
    qSliderList.clear();
    qLabelList.clear();
    customPlotPDvhs= new QCustomPlot(pDVHsWidget);
    customPlotPDvhs->setFixedSize(800, 600);
    customPlotPDvhs->xAxis->setRange(0, maxDose*1.55);
    customPlotPDvhs->yAxis->setRange(0, 1.03);
    customPlotPDvhs->xAxis->setLabel("Dose / Gy");
    customPlotPDvhs->yAxis->setLabel("Fractional Volume ");

    pDVHGridlayout->addWidget(customPlotPDvhs,0,0,10,10);
    for (int iRoi = 0; iRoi < numRois; iRoi++)
    {
        bool isTargetVolume;
        std::string RoiName;
        if (isPinnacleFlag)
        {
            isTargetVolume = pinnacleFiles->roisPropVector[iRoi].IsTargetVolume;
            RoiName = pinnacleFiles->roisPropVector[iRoi].name;
        }
        else
        {
            isTargetVolume = rtStructure->roiVector[iRoi].IsTargetVolume;
            RoiName = rtStructure->roiVector[iRoi].RoiName;
        }
        qSpinBoxList.push_back(new QSpinBox(pDVHsWidget));
        qSliderList.push_back(new QSlider(Qt::Horizontal,pDVHsWidget));
        qSliderList.last()->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
        qSpinBoxList[iRoi]->setRange(1,100);
        qSliderList[iRoi]->setRange(1,100);
        QObject::connect(qSpinBoxList[iRoi], SIGNAL(valueChanged(int)),  qSliderList[iRoi], SLOT(setValue(int)));
        QObject::connect(qSliderList[iRoi], SIGNAL(valueChanged(int)),  qSpinBoxList[iRoi], SLOT(setValue(int)));

        if (isTargetVolume)
        {
            qSliderList[iRoi]->setValue(desiredPDVHsConfidenceLevel[iRoi]);

        }
        else
        {
            qSliderList[iRoi]->setValue(desiredPDVHsConfidenceLevel[iRoi]);
        }
        // Olivier Goffart - Signals and Slots in Qt 5 --> https://www.youtube.com/watch?v=pwNd8gq6PZY
        //QObject::connect(qSliderList[iRoi], SIGNAL(valueChanged(int)), this, [=](int newValue){this->updatePDVHConfLevelForAnROI(newValue,iRoi);} );

        QObject::connect(qSliderList[iRoi], SIGNAL(valueChanged(int)), this,SLOT(updatePDVHConfLevelForAnROI()));





        qLabelList.push_back(new QLabel(pDVHsWidget));
        qLabelList.last()->setText(RoiName.c_str());
        qLabelList.last()->setFont(QFont("Helvetica", 12,QFont::Bold));
        pDVHGridlayout->addWidget(qLabelList[iRoi],10+iRoi*2,0,2,1);
        pDVHGridlayout->addWidget(qSliderList[iRoi],10+iRoi*2,1,2,8);
        pDVHGridlayout->addWidget(qSpinBoxList[iRoi],10+iRoi*2,9,2,1);
    }
    QObject::connect(this, SIGNAL(emitDesiredPDVHsConfidenceLevels(std::vector<int>)), this ,SLOT(setSlidersAndqSpinBoxesConfidanceValueSlot(std::vector<int>)));
    updatePDVHSGraphs();

    QObject::connect(this, SIGNAL(emitDesiredPDVHsConfidenceLevels(std::vector<int>)), this ,SLOT(updatePDVHSGraphs()));

    customPlotPDvhs->plotLayout()->insertRow(0);
    customPlotPDvhs->plotLayout()->addElement(0, 0, new QCPPlotTitle(customPlotPDvhs, "PDVHs on the selected confidence level for Patient"));
    customPlotPDvhs->axisRect()->setupFullAxesBox();
    customPlotPDvhs->replot();
    pDVHsWidget->show();
    pDVHsWidget->setFixedSize(pDVHsWidget->width(),pDVHsWidget->height());

    //    QTimer *timer=new QTimer(pDVHsWidget);
    //    connect(timer,SIGNAL(timeout()),this,SLOT(testSlotForPDVHsConfValue()));
    //    timer->start(1000);
    return true;
}

bool RobustnessAnalyzer::plotPDVHSNew()
{
    std::string class_member = "RobustnessAnalyzer::calculatePDVHs:" ;
    if (this->cDVHForTrial.size()<1)
    {
        logstream << class_member << " no cDVH data has been found. RobustnessAnalyzer->cDVHForTrial is empty! " << std::endl;
        return false;

    }
    if (this->cDch.size()<1)
    {
        logstream << class_member << " no cDch data has been found.  " << std::endl;

        if (!calculateDchs())
        {
            return false;
        }
    }

    if (this->desiredPDVHsConfidenceLevel.size()<1)
    {
        logstream << class_member << " desiredPDVHsConfidenceLevel has been found.  " << std::endl;
        logstream << class_member << " Attempting to initialize it based on Selected ROI's Type.  " << std::endl;

        if (!calculatePDVHsConfLevelInitialValues())
        {
            return false;
        }
    }

    if (pDVHs.size()<1)
    {

        logstream << class_member << " PDVHs has been calculated found.  " << std::endl;

        if (!calculatePDVHsFromDCHsAtSpecifiedConfLevel())
        {
            logstream << class_member << " Error in calculating PDVHs for each ROI from DCHs based on  the specified confident level." << std::endl;
            return false;
        }

    }




    numRois= 0;
    double maxDose;
    std::string patientID;
    std::string doseCalcImageSet;
    if (isPinnacleFlag)
    {
        numRois = pinnacleFiles->roisPropVector.size();
        maxDose = (double)pinnacleFiles->rtDosePinn->getMaxDose();
        patientID= std::to_string(pinnacleFiles->Patient.PatientID);
        doseCalcImageSet= pinnacleFiles->rtDosePinn->doseCalcImageSet;
    }
    else
    {
        numRois = rtStructure->roiVector.size();
        maxDose= rtDose->maxDose;
        patientID= rtStructure->patientID;
        doseCalcImageSet= rtDose->doseCalcImageSet;
    }


    customPlotPDvhs= new QCustomPlot;
    customPlotPDvhs->setGeometry(400, 250, 800, 480);
    customPlotPDvhs->xAxis->setRange(0, maxDose*1.02);
    customPlotPDvhs->yAxis->setRange(0, 1.03);
    customPlotPDvhs->xAxis->setLabel("Dose / Gy");
    customPlotPDvhs->xAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold));
    customPlotPDvhs->yAxis->setLabel("Fractional Volume");
    customPlotPDvhs->yAxis->setLabelFont(QFont("Helvetica", 12,QFont::Bold));

    customPlotPDvhs->xAxis->setTickLabelFont(QFont(QFont().family(), 12));
    customPlotPDvhs->yAxis->setTickLabelFont(QFont(QFont().family(), 12));

    updatePDVHSGraphs();
    QCPPlotTitle* qcpPlotTitle = new QCPPlotTitle(customPlotPDvhs, "PDVHs on the selected confidence level (95% for TVs and 5% for NTs)");
    qcpPlotTitle->setFont(QFont("Helvetica", 13,QFont::Bold));
    qcpPlotTitle->setAntialiased(true);
    customPlotPDvhs->plotLayout()->insertRow(0);
    customPlotPDvhs->plotLayout()->addElement(0, 0, qcpPlotTitle);
    QCPLayoutGrid *subLayout = new QCPLayoutGrid;
    subLayout->addElement(0, 0, new QCPLayoutElement);
    subLayout->addElement(0, 1, customPlotPDvhs->legend);
    subLayout->addElement(0, 2, new QCPLayoutElement);
    customPlotPDvhs->plotLayout()->insertRow(1);
    customPlotPDvhs->plotLayout()->addElement(1, 0, subLayout);
    customPlotPDvhs->plotLayout()->setRowStretchFactor(1,0.001);
    customPlotPDvhs->axisRect()->setupFullAxesBox();
    customPlotPDvhs->replot();
    //    pDVHsWidget->show();
    return true;
}

//bool RobustnessAnalyzer::writeAllDvhsToFile()
//{
//    try
//    {
//        for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); ++iRoi)
//        {
//            writeVectorToAsciiFile(resultFolderPath + "/AllDvhs_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDVHForTransformedPts[iRoi]);
//        }
//    }
//    catch(std::exception e)
//    {
//        // TODO : handle this
//        return false;
//    }

//    return true;
//}



bool RobustnessAnalyzer::writeAllDchsToFile()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //writeVectorToAsciiFile(resultFolderPath + "/AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllDchs_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDch[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllDchs_Patient_" + rtStructure->patientID +"_Plan_" + rtDose->doseCalcImageSet + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDch[iRoi]);

            }
        }
    }
    catch(std::exception e)
    {

        // TODO : handle this
        return false;
    }

    return true;



}

bool RobustnessAnalyzer::writeAllDchsToFile(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path + "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", cDch[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDch[iRoi]);
            }


        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}


bool RobustnessAnalyzer::WritecDVHs95thPercentile()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);


            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs95thPercentile_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHs95thPercentile[iRoi]);

            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs95thPercentile_Patient_" + rtStructure->patientID +"_Plan_" + rtDose->doseCalcImageSet + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHs95thPercentile[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHs95thPercentile(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            //            writeVectorToBinaryFileQT(path + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHs95thPercentile[iRoi]);


            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path + "_roi_" + pinnacleFiles->roisPropVector[iRoi].name  + ".bin", cDVHs95thPercentile[iRoi]);

            }
            else
            {
                writeVectorToBinaryFileQT(path + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHs95thPercentile[iRoi]);

            }

        }


    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}



bool RobustnessAnalyzer::WritecDVHs5thPercentile()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {

                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs5thPercentile_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHs5thPercentile[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs5thPercentile_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHs5thPercentile[iRoi]);

            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHs5thPercentile(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path + "_roi_" + pinnacleFiles->roisPropVector[iRoi].name  + ".bin", cDVHs5thPercentile[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHs5thPercentile[iRoi]);

            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHSForTransformedPoints()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHForTransformedPts[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHs_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHForTransformedPts[iRoi]);
            }

        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHSForTrails()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHsForTrails_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name, cDVHForTrial[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllcDVHsForTrails_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHForTrial[iRoi]);

            }

        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHSForTrails(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path +  "_roi_" +  pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHForTrial[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHForTrial[iRoi]);

            }
        }
    }

    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WriteDiffDvhsForTrials()
{


    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllDiffDVHsForTrails_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", diffDVHForTrial[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/AllDiffDVHsForTrails_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", diffDVHForTrial[iRoi]);

            }

        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }
    return true;
}

bool RobustnessAnalyzer::WriteDiffDvhsForTrials(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", diffDVHForTrial[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", diffDVHForTrial[iRoi]);

            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }
    return true;
}

bool RobustnessAnalyzer::WritePlannedDVHS()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);

            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/PlannedDVH_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", plannedDVHs[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/PlannedDVH_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", plannedDVHs[iRoi]);

            }

        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritePlannedDVHS(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path + "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", plannedDVHs[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", plannedDVHs[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsMedian()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/DVHsMedian_Patient_" +  std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", cDVHsMedian[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/DVHsMedian_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsMedian[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsMedian(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT( path +  "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHsMedian[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT( path +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsMedian[iRoi]);

            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsMean()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/DVHsMean_Patient_" + std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", cDVHsMean[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/DVHsMean_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsMean[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsMean(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHsMean[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsMean[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsSTD()
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/STDDVHs_Patient_" +  std::to_string(pinnacleFiles->Patient.PatientID) +"_Plan_" + pinnacleFiles->rtDosePinn->doseCalcImageSet+ "_roi_" + pinnacleFiles->roisPropVector[iRoi].name+ ".bin", cDVHsStandardDeviation[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(resultFolderPath + "/STDDVHs_Patient_" + rtStructure->patientID + "_Plan_" + rtDose->doseCalcImageSet +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsStandardDeviation[iRoi]);
            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

bool RobustnessAnalyzer::WritecDVHsSTD(std::string path)
{
    try
    {
        for (int iRoi = 0; iRoi < numRois; ++iRoi)
        {
            //            writeVectorToAsciiFile(path + "AllDchs_Patient_" + rtStructure->patientID + "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".txt", cDch[iRoi]);
            if (isPinnacleFlag)
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + pinnacleFiles->roisPropVector[iRoi].name + ".bin", cDVHsStandardDeviation[iRoi]);
            }
            else
            {
                writeVectorToBinaryFileQT(path +  "_roi_" + rtStructure->roiVector[iRoi].RoiName + ".bin", cDVHsStandardDeviation[iRoi]);


            }
        }
    }
    catch(std::exception e)
    {
        // TODO : handle this
        return false;
    }

    return true;
}

void RobustnessAnalyzer::calculateAllDVHsOnCpu()
{

}

bool RobustnessAnalyzer::calculateAllDVHsOnCpuForPinnaclePatient()
{


    std::string class_member = "RobustnessAnalyzer::calculateAllDVHsOnCpuForPinnaclePatient:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }

    if (pinnacleFiles==NULL)
    {
        logstream << class_member << " pinnacleFiles pointer has not been set." << std::endl;
        return false;
    }

    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }


    if (pinnacleFiles->rtDosePinn==NULL)
    {
        logstream << class_member << " rtDose pointer has not been set for PinnacleFiles object." << std::endl;
        return false;
    }

    if (pinnacleFiles->roisPropVector.size() ==0)
    {
        logstream << class_member << " No roi is found in roisPropVector in PinnacleFiles --> forgot to set and compute roi props?." << std::endl;
        return false;

    }

    if (doseBinCenters.size()==0)
    {
        logstream << class_member << "doseBinCenters has not been set. We are going to automatically set it based on rtDose max dose. " << std::endl;

        if (numDoseBins<1)
        {
            logstream << class_member << "ERROR numDoseBins< 1 " << std::endl;
            return false;
        }
        doseBinWidth = (pinnacleFiles->rtDosePinn->getMaxDose()*1.02)/ numDoseBins;
        logstream << class_member << " doseBinWidth = (pinnacleFiles->rtDosePinn->maxDose*1.02)/ numDoseBins = " << doseBinWidth << std::endl;
        doseBinCenters = generateRange(doseBinWidth / 2.0, doseBinWidth, pinnacleFiles->rtDosePinn->getMaxDose()*1.02);
        if (doseBinCenters.size()!=numDoseBins)
        {
            logstream << class_member << "doseBinCenters.size()!=numDoseBins, debug the code!" << std::endl;
            return false;
        }
    }
    else
    {
        if (doseBinCenters.size()<2)
        {
            logstream << class_member << "ERROR doseBinCenters length is less than 2." << std::endl;
            return false;
        }

        doseBinWidth=doseBinCenters[1]-doseBinCenters[0];
        if (doseBinWidth/2!=doseBinCenters[0])
        {
            logstream << class_member << "doseBinCenters are not set properly, they should follow this pattern [doseBinWidth/2,doseBinWidth+doseBinWidth/2,2*doseBinWidth+doseBinWidth/2,...]  ." << std::endl;

        }

        for (int iBin = 0; iBin < doseBinCenters.size()-1; ++iBin) {

            if (fabs(fabs(doseBinCenters[iBin+1]-doseBinCenters[iBin]) -doseBinWidth)>1e-6)
            {
                logstream << class_member << "doseBinCenters are not equispaced." << std::endl;
                return false;
            }

        }
        numDoseBins= doseBinCenters.size();
        if (numDoseBins<2)
        {
            // this should not happen but it is being checked anyway.
            logstream << class_member << "ERROR numDoseBins< 2 " << std::endl;
            return false;
        }
    }

    writeVectorToAsciiFile("doseBinCenters.txt", doseBinCenters); // TODO : make it selectable in the gui
    auto deviceInfo = gpusAndCpusInfo();

    if (!deviceInfo.getNumCpuAndGpus())
    {
        return false;
    }

    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxX = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minX = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesX.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxY = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minY = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesY.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.maxZ = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::min(), thrust::maximum<float>());
    pinnacleFiles->rtDosePinn->doseArrayBoundingBox.minZ = thrust::reduce(pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.begin(), pinnacleFiles->rtDosePinn->doseArrayCoordinatesZ.end(), std::numeric_limits<float>::max(), thrust::minimum<float>());

    std::vector<float> fractionsDoseWeights(numFractions,1/static_cast<float>(numFractions));


    //    // Memory required to store roiModel.inPolygonPoints into GPU for each ROI
    cDVHForTrial.clear();
    diffDVHForTrial.clear();
    voxelDoseMeanForTrials.clear();
    voxelDoseMeanForTrials.clear();


    for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); iRoi++)
    {
        logstream << class_member << "Calculating memory requirement for ROI " << iRoi << " On GPU (float each number 4 bytes)" << std::endl;

        { // this bracket to tell compiler to clean up tempBinDoseVec
            std::vector<float> tempTrailDvhs(numDoseBins*numTrials, 0);
            cDVHForTrial.push_back(tempTrailDvhs);
            std::vector<float> tempDiffDvhs(numDoseBins*numTrials, 0);
            diffDVHForTrial.push_back(tempDiffDvhs);
            std::vector<float> tempVoxelDoseMean(pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesX.size());
            voxelDoseMeanForTrials.push_back(tempVoxelDoseMean);
            voxelDoseStdForTrials.push_back(tempVoxelDoseMean);
        }


        calculateAllDiffAndCulmulativeDvhsForPinnaclePatientOnCPU(cDVHForTrial[iRoi],
                                                                  diffDVHForTrial[iRoi],
                                                                  voxelDoseMeanForTrials[iRoi],
                                                                  voxelDoseStdForTrials[iRoi],
                                                                  fractionsDoseWeights,
                                                                  pinnacleFiles->rtDosePinn->doseArray,
                                                                  pinnacleFiles->rtDosePinn->doseGridXvalues,
                                                                  pinnacleFiles->rtDosePinn->doseGridYvalues,
                                                                  pinnacleFiles->rtDosePinn->doseGridZvalues,
                                                                  pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesX,
                                                                  pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesY,
                                                                  pinnacleFiles->roisPropVector.at(iRoi).binaryBitmapPointCoordinatesZ,
                                                                  simulationModelParams.at(iRoi).RotationMatrices,
                                                                  simulationModelParams.at(iRoi).TranslationVectors,
                                                                  numDoseBins,
                                                                  doseBinWidth);



    }
    return true;


}

//bool RobustnessAnalyzer::checkAvailableMemoryOnDevices()
//{
//    std::string class_member = "RobustnessAnalyzer::checkAvailableMemoryOnDevices:" ;
//    std::vector<size_t> temp(numGPUs, 0);
//    freeMemOnEachDevice=temp;



//    size_t totalMemBytes;
//    size_t freeMemBytes;
//    for (int idev = 0; idev < numGPUs; idev++)
//    {
//        cudaSetDevice(idev);
//        cudaError_t cuda_status = cudaMemGetInfo(&freeMemBytes, &totalMemBytes);
//        if (cudaSuccess != cuda_status){

//            logstream << class_member <<  "ERORR cudaMemGetInfo for cuda device number " << idev << " =  " << cudaGetErrorString(cuda_status) << std::endl;
//            return false;
//        }

//        freeMemOnEachDevice[idev] = freeMemBytes;
//        logstream << class_member  << "Free Memory of cuda device " << idev << " = " << freeMemBytes << "  (bytes)  " << std::endl;
//        //logstream << class_member  << "Total Memory of cuda device " << idev << " = " << totalMemBytes << "  (bytes)  " << std::endl;
//    }

//    return true;
//}

//bool RobustnessAnalyzer::checkAvailableGPUsAndCPUs()
//{
//    std::string class_member = "RobustnessAnalyzer::checkAvailableGPUsAndCPUs:" ;
//    numCPUs=omp_get_num_procs();
//    // display CPU  configuration
//    logstream << class_member << "number of host CPUs:" << numCPUs << std::endl;


//    // determine the number of CUDA capable GPUs
//    cudaGetDeviceCount(&numGPUs);

//    if (numGPUs < 1)
//    {
//        logstream << class_member << "ERORR -> no CUDA capable devices were detected" << std::endl;
//        return false;
//    }
//    else
//    {
//        logstream << class_member << "number of CUDA devices= " << numGPUs << std::endl;
//    }
//    return true;
//}

void RobustnessAnalyzer::setRtDose(RtDose *inRtDose)
{
    std::string class_member = "RobustnessAnalyzer::setRtDose:" ;

    rtDose= inRtDose;
    logstream  << class_member <<  " Sets the RtDose." << std::endl;

}

void RobustnessAnalyzer::setRtStructure(RtStructure *inRtStruc)
{
    std::string class_member = "RobustnessAnalyzer::setRtStructure:" ;

    rtStructure = inRtStruc;
    logstream  << class_member <<  " Sets the RtStructure." << std::endl;
}

void RobustnessAnalyzer::setPinnacleFiles(PinnacleFiles *_pinnacleFiles)
{
    std::string class_member = "RobustnessAnalyzer::setPinnacleFiles:" ;

    pinnacleFiles = _pinnacleFiles;
    if (isVerbose)
    {
        logstream  << class_member <<  " Sets the setPinnacleFiles folder." << std::endl;
    }
}



void RobustnessAnalyzer::setModelData(ModelData *inModelData)
{
    std::string class_member = "RobustnessAnalyzer::setModelData:" ;

    modelData=inModelData;
    if (isVerbose)
    {
        logstream  << class_member <<  " Sets the Model Data." << std::endl;
    }
}




void RobustnessAnalyzer::testWriteVectorToAsciiAndBinary() //TODO : Test
{
    std::string class_member = "RobustnessAnalyzer::testWriteVectorToAsciiAndBinary:" ;

    std::vector<double> v;
    v.push_back(1.4);
    v.push_back(2.5);
    v.push_back(3.6);
    v.push_back(4.7);

    std::string filePathAscii = "./testWriteVectorToAscii.txt";
    std::string filePathBinary = "./testWriteVectorToBinary.bin";
    writeVectorToBinaryFileQT(filePathBinary,v);
    writeVectorToAsciiFile(filePathAscii,v);


}

void RobustnessAnalyzer::setNumFractions(int numFrac)
{
    numFractions=numFrac;
    if (isVerbose)
    {
        logstream <<"SET: setNumFractions = " << numFractions<< std::endl;
    }
}


void RobustnessAnalyzer::setNumSubFraction(int numSubFrac)
{
    numSubFractions=numSubFrac;
    if (isVerbose)
    {
        logstream <<"SET: numSubFractions = " << numSubFractions<< std::endl;
    }
}




int  RobustnessAnalyzer::getNumFractions()
{
    return numFractions;

}

int RobustnessAnalyzer::getNumSubFractions()
{
    return numSubFractions;
}

std::string RobustnessAnalyzer::getResultFolderPath()
{
    return resultFolderPath;

}

bool RobustnessAnalyzer::getUseGPU()
{
    return useGPU;
}


void RobustnessAnalyzer::setGPUUsage(const bool flagOn)
{
    useGPU= flagOn;
}

void RobustnessAnalyzer::setDoseBinCenters(std::vector<double> &doseBinCentersVec)
{
    doseBinCenters=doseBinCentersVec;
}

void RobustnessAnalyzer::setResultFolderPath(std::string _resultFolderPath)
{
    resultFolderPath = _resultFolderPath;
}

void RobustnessAnalyzer::setDoseBinWidth(double _doseBinWith)
{
    doseBinWidth=_doseBinWith;
}


void RobustnessAnalyzer::setNumTrials(int numTrls)
{
    numTrials=numTrls;
    //    logstream <<"SET: numTrails = " << numTrials<< std::endl;
}

int  RobustnessAnalyzer::getNumTrials()
{
    return numTrials;
}

void RobustnessAnalyzer::setNumDoseBins(int numBins)
{
    numDoseBins= numBins;
    //    logstream <<"SET: numDoseBins = " << numDoseBins<< std::endl;
}

void RobustnessAnalyzer::setPinnacleFlag(bool pinnIsBeingUsed)
{
    isPinnacleFlag = pinnIsBeingUsed ;
}

int  RobustnessAnalyzer::getNumDoseBins()
{
    return numDoseBins;
}

bool RobustnessAnalyzer::generateTransformationsParams()
{
    // The outputs of this function which are used in the computation part are simulationModelParams.at(iRoi).TranslationVectors, simulationModelParams.at(iRoi).RotationMatrices
    // http://www.rit.edu/cos/uphysics/uncertainties/Uncertaintiespart2.html --> Uncertainties and Error Propagation

    std::string class_member = "RobustnessAnalyzer::generateTransformationsParams:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }
    if (rtDose==0)
    {
        logstream << class_member << " rtDose has not been set." << std::endl;
        return false;
    }
    if (rtStructure==0 )
    {
        logstream << class_member << " rtStructure has not been set." << std::endl;
        return false;
    }
    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }

    // Fill the simulation parameters from modelData : TODO: we need to modify this for delineation uncertainty

    //simulationModelParams
    {
        auto temp = std::vector<SimulationModelParams>(rtStructure->roiVector.size(),SimulationModelParams());
        simulationModelParams = temp;
    }

    // Systematic RigidBody
    std::vector<float> deltaXSystematicRigidBodyVec;
    std::vector<float> deltaYSystematicRigidBodyVec;
    std::vector<float> deltaZSystematicRigidBodyVec;
    std::vector<float> rotXSystematicRigidBodyVec;
    std::vector<float> rotYSystematicRigidBodyVec;
    std::vector<float> rotZSystematicRigidBodyVec;

    std::vector<float> xdispRigidBodyRandVec;
    std::vector<float> ydispRigidBodyRandVec;
    std::vector<float> zdispRigidBodyRandVec;
    std::vector<float> xrotRigidBodyRandVec;
    std::vector<float> yrotRigidBodyRandVec;
    std::vector<float> zrotRigidBodyRandVec;



    // these will be used for each ROI
    //    std::vector<float> deltaXSystematic;
    //    std::vector<float> deltaYSystematic;
    //    std::vector<float> deltaZSystematic;
    //    std::vector<float> rotXSystematic;
    //    std::vector<float> rotYSystematic;
    //    std::vector<float> rotZSystematic;

    std::vector<float> deltaXRand;
    std::vector<float> deltaYRand;
    std::vector<float> deltaZRand;
    std::vector<float> xrotRand;
    std::vector<float> yrotRand;
    std::vector<float> zrotRand;

    Point3 rigidBodyRotationPoint;
    Point3 roiRotationPoint; // each roi rotation point
    if ( modelData->rigidBodyMotionExist)
    {
        // Generate systematic displacment errors
        deltaXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.x, numTrials);
        deltaYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.y, numTrials);
        deltaZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.z, numTrials);

        // Generate systematic rotation errors
        rotXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.x, numTrials);
        rotYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.y, numTrials);
        rotZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.z, numTrials);
        // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center


        for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); ++iRoi)
        {
            if  (rtStructure->roiVector[iRoi].RoiName.compare(modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM)==0)
            {
                rigidBodyRotationPoint = rtStructure->roiVector[iRoi].RoiCOM;
                break;
            }
            if (iRoi == rtStructure->roiVector.size()-1)
            {
                logstream << class_member << " The structure that specifies the center of rotation for rigid body motion is not found in roiVector." << std::endl;
                return false;
            }
        }

        // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
        // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
    }


    if ( modelData->rigidBodyMotionExist)
    {
        xdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.x, numTrials*numFractions);
        ydispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.y, numTrials*numFractions);
        zdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.z, numTrials*numFractions);
        xrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.x, numTrials*numFractions);
        yrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.y, numTrials*numFractions);
        zrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.z, numTrials*numFractions);
    }



    for (int iRoi = 0; iRoi < rtStructure->roiVector.size(); ++iRoi)
    {



        //

        if (modelData->gumMap.find(rtStructure->roiVector.at(iRoi).RoiName)==modelData->gumMap.end())
        {
            // not found // This should not happen since we check the roiVector when parsing XML config file
            logstream << class_member << "ERROR --> This should not happen since we check the roiVector when parsing XML config file. " << std::endl;
            return false;
        }

        std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator  iter =modelData->gumMap.find(rtStructure->roiVector.at(iRoi).RoiName);

        RotationAndTranslationModel rotTransModel = boost::get<0>(iter->second);

        // Now add rotTransModel model for the
        if (rotTransModel.IsModelSet())
        {


            // Generate systematic displacment errors for Roi number iRoi
            simulationModelParams.at(iRoi).deltaXSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).deltaYSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).deltaZSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.z, numTrials);





            // Generate systematic rotation errors for Roi number iRoi
            simulationModelParams.at(iRoi).rotXSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).rotYSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).rotZSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.z, numTrials);


            // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
            //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
            // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
            // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;

            for (int jRoi = 0; jRoi < rtStructure->roiVector.size(); ++jRoi)
            {
                if  (rtStructure->roiVector[jRoi].RoiName.compare(rotTransModel.rotCenter.RoiIDForRotationAroundCOM)!=0)
                {
                    roiRotationPoint = rtStructure->roiVector[jRoi].RoiCOM;
                    break;
                }
                if (jRoi == rtStructure->roiVector.size()-1)
                {
                    logstream << class_member << "ERROR -->  Could not found the structure that specifies the center of rotation in rotation and translation model of Roi " << rtStructure->roiVector[iRoi].RoiName << " in the selected ROI list."<< std::endl;
                    return false;
                }
            }
        }




        std::vector<float> temp7(numTrials*numFractions * 9, 0);
        simulationModelParams.at(iRoi).RotationMatrices = temp7;
        std::vector<float> temp8(numTrials*numFractions * 3, 0);
        simulationModelParams.at(iRoi).TranslationVectors = temp8;


        for (auto iTrial = 0; iTrial < numTrials; iTrial++)
        {



            // Now add rotTransModel model for the
            if (rotTransModel.IsModelSet())
            {

                // Generate random displacment errors for Roi number iRoi for all fractions
                deltaXRand = randn(0, rotTransModel.randTranErrorSigma.x, numFractions);
                deltaYRand = randn(0, rotTransModel.randTranErrorSigma.y, numFractions);
                deltaZRand = randn(0, rotTransModel.randTranErrorSigma.z, numFractions);

                // Generate random rotation errors for Roi number iRoi for all fractions
                xrotRand= randn(0, rotTransModel.randRotErrorSigma.x, numFractions);
                yrotRand = randn(0, rotTransModel.randRotErrorSigma.y, numFractions);
                zrotRand= randn(0, rotTransModel.randRotErrorSigma.z, numFractions);

                // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
                //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
                // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
                // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
            }




            for (auto iFrac = 0; iFrac < numFractions; iFrac++)
            {

                auto indexTransformation = iTrial*numFractions + iFrac;

                if (indexTransformation < numFractions) // no uncertainty in the first transformation consisting of numFractions fractions
                {
                    RotationMatrixFromEulerAngles(0,0,0,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2] =0;

                    continue;
                }

                if (modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet())
                {
                    // X1-O1=R1(X0-O1)+T1, where O1 center of rotation and R1 is rotation matrix, and T1 is a shift after rotation --> X1 = R1*X0+O1-R1*O1+T1 == RX0+T --> R=R1, T = O1-R1*O1+T1
                    float xrot = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyRandVec[indexTransformation];
                    float yrot = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyRandVec[indexTransformation];
                    float zrot = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyRandVec[indexTransformation];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    //T = O1-R1*O1+T1
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], rigidBodyRotationPoint, transformedCOM );
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x-transformedCOM.x + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyRandVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y-transformedCOM.y + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyRandVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z-transformedCOM.z + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyRandVec[indexTransformation];

                }
                else if (!modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // X2-O2=R2(X0-O2)+T2, where O2 center of rotation and R2 is rotation matrix --> X2 = R2*X0+O2-R2*O2 +T2 == RX0+T --> R=R2, T = O2-R2*O2+T2
                    float xrot = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrot = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrot = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    // T = O2-R2*O2 +T2
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], roiRotationPoint, transformedCOM );


                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = roiRotationPoint.x-transformedCOM.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = roiRotationPoint.y-transformedCOM.y +simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = roiRotationPoint.z-transformedCOM.z +simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac];

                }

                else if (modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // if we have both motions (Rigidbody and individual structure motions)
                    // X1-O1=R1(X0-O1)+T1 (1)
                    // X2-On2=R2(X1-On2)+T2 (2)--> where On2 is the trasform center of rotation of ROI number iRoi with rigidbody motion --> On2= R1(O2-O1)+ T1+O1 (3)
                    // (1),(2), and (3) --> X2 = R2(R1(X0-O1)+T1+O1 - R1(O2-O1)-T1) + T2 +  R1(O2-O1)+ T1+O1 = R2R1X0 + R2T1-R2R1O2 - R2T1
                    // X2= R2 R1 X0 - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2 == RX0+T --> R=R2*R1  , T = - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2

                    float xrotRigidBody = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyRandVec[indexTransformation];
                    float yrotRigidBody = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyRandVec[indexTransformation];
                    float zrotRigidBody = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyRandVec[indexTransformation];
                    float xrotRoi = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrotRoi = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrotRoi = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    std::vector<float> R1(9,0);
                    std::vector<float> R2(9,0);
                    RotationMatrixFromEulerAngles(xrotRigidBody,yrotRigidBody,zrotRigidBody,&R1[0]);
                    RotationMatrixFromEulerAngles(xrotRoi,yrotRoi,zrotRoi,&R2[0]);
                    RotMatrixTimesRotMatrix(&R2[0],&R1[0],&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    Point3 R2R1O2;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9],roiRotationPoint,R2R1O2); // R2*R1*O2
                    Point3 O2_O1;
                    O2_O1.x= roiRotationPoint.x-rigidBodyRotationPoint.x;
                    O2_O1.y= roiRotationPoint.y-rigidBodyRotationPoint.y;
                    O2_O1.z= roiRotationPoint.z-rigidBodyRotationPoint.z;
                    Point3 R1TimesO2_O1;
                    RotMatrixTimesPoint3(&R1[0],O2_O1,R1TimesO2_O1);//  R1 (O2- O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac] + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyRandVec[indexTransformation] - R2R1O2.x +R1TimesO2_O1.x; // O1+T2+T1 - R2R1O2 + R1(O2-O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y + simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac] + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyRandVec[indexTransformation]- R2R1O2.y + R1TimesO2_O1.y;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z + simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac] + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyRandVec[indexTransformation]- R2R1O2.z + R1TimesO2_O1.z;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                }

                else if (!modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet()) // TODO: no uncertainty in this case so DVH is enough in this case this could be handled in the pdvh computation part.
                {

                }

            } // iFrac
        } //  iTrial

    } // iRoi for


    return true;
}

bool RobustnessAnalyzer::generateTransformationsParamsPinnacle()
{
    // The outputs of this function which are used in the computation part are simulationModelParams.at(iRoi).TranslationVectors, simulationModelParams.at(iRoi).RotationMatrices
    // http://www.rit.edu/cos/uphysics/uncertainties/Uncertaintiespart2.html --> Uncertainties and Error Propagation

    std::string class_member = "RobustnessAnalyzer::generateTransformationsParamsPinnacle:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }
    if (pinnacleFiles==NULL)
    {
        logstream << class_member << " pinnacleFiles pointer has not been set." << std::endl;
        return false;
    }

    if (pinnacleFiles->rtDosePinn==NULL)
    {
        logstream << class_member << " rtDose pointer has not been set for PinnacleFiles object." << std::endl;
        return false;
    }

    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }
    if (pinnacleFiles->roisPropVector.size() ==0)
    {
        logstream << class_member << " No roi is found in roisPropVector in PinnacleFiles --> forgot to set and compute roi props?." << std::endl;
        return false;

    }
    // Fill the simulation parameters from modelData : TODO: we need to modify this for delineation uncertainty

    //simulationModelParams
    {
        auto temp = std::vector<SimulationModelParams>(pinnacleFiles->roisPropVector.size(),SimulationModelParams());
        simulationModelParams = temp;
    }
    // Systematic RigidBody
    std::vector<float> deltaXSystematicRigidBodyVec;
    std::vector<float> deltaYSystematicRigidBodyVec;
    std::vector<float> deltaZSystematicRigidBodyVec;
    std::vector<float> rotXSystematicRigidBodyVec;
    std::vector<float> rotYSystematicRigidBodyVec;
    std::vector<float> rotZSystematicRigidBodyVec;

    std::vector<float> xdispRigidBodyRandVec;
    std::vector<float> ydispRigidBodyRandVec;
    std::vector<float> zdispRigidBodyRandVec;
    std::vector<float> xrotRigidBodyRandVec;
    std::vector<float> yrotRigidBodyRandVec;
    std::vector<float> zrotRigidBodyRandVec;
    // these will be used for each ROI
    //    std::vector<float> deltaXSystematic;
    //    std::vector<float> deltaYSystematic;
    //    std::vector<float> deltaZSystematic;
    //    std::vector<float> rotXSystematic;
    //    std::vector<float> rotYSystematic;
    //    std::vector<float> rotZSystematic;

    std::vector<float> deltaXRand;
    std::vector<float> deltaYRand;
    std::vector<float> deltaZRand;
    std::vector<float> xrotRand;
    std::vector<float> yrotRand;
    std::vector<float> zrotRand;

    Point3 rigidBodyRotationPoint;
    Point3 roiRotationPoint; // each roi rotation point

    if ( modelData->rigidBodyMotionExist)
    {
        // Generate systematic displacment errors
        deltaXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.x, numTrials);
        deltaYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.y, numTrials);
        deltaZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.z, numTrials);

        // Generate systematic rotation errors
        rotXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.x, numTrials);
        rotYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.y, numTrials);
        rotZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.z, numTrials);
        // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center


        for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); ++iRoi)
        {
            if  (pinnacleFiles->roisPropVector[iRoi].name.compare(modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM)==0)
            {
                rigidBodyRotationPoint.x = pinnacleFiles->roisPropVector[iRoi].RoiCOM.x;
                rigidBodyRotationPoint.y = pinnacleFiles->roisPropVector[iRoi].RoiCOM.y;
                rigidBodyRotationPoint.z = pinnacleFiles->roisPropVector[iRoi].RoiCOM.z;
                break;
            }
            if (iRoi == pinnacleFiles->roisPropVector.size()-1)
            {
                logstream << class_member << " The structure that specifies the center of rotation for rigid body motion is not found in roiVector." << std::endl;
                return false;
            }
        }

        // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
        // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
    }


    if ( modelData->rigidBodyMotionExist)
    {
        xdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.x, numTrials*numFractions);
        ydispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.y, numTrials*numFractions);
        zdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.z, numTrials*numFractions);
        xrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.x, numTrials*numFractions);
        yrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.y, numTrials*numFractions);
        zrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.z, numTrials*numFractions);
    }




    for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); ++iRoi)
    {



        //

        if (modelData->gumMap.find(pinnacleFiles->roisPropVector.at(iRoi).name)==modelData->gumMap.end())
        {
            // not found // This should not happen since we check the roiVector when parsing XML config file
            logstream << class_member << "ERROR --> This should not happen since we check the roiVector when parsing XML config file. " << std::endl;
            return false;
        }

        std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator  iter =modelData->gumMap.find(pinnacleFiles->roisPropVector[iRoi].name);

        RotationAndTranslationModel rotTransModel = boost::get<0>(iter->second);

        // Now add rotTransModel model for the
        if (rotTransModel.IsModelSet())
        {


            // Generate systematic displacment errors for Roi number iRoi
            simulationModelParams.at(iRoi).deltaXSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).deltaYSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).deltaZSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.z, numTrials);





            // Generate systematic rotation errors for Roi number iRoi
            simulationModelParams.at(iRoi).rotXSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).rotYSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).rotZSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.z, numTrials);


            // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
            //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
            // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
            // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;

            for (int jRoi = 0; jRoi < pinnacleFiles->roisPropVector.size(); ++jRoi)
            {
                if  (pinnacleFiles->roisPropVector[jRoi].name.compare(rotTransModel.rotCenter.RoiIDForRotationAroundCOM)!=0)
                {
                    roiRotationPoint.x = pinnacleFiles->roisPropVector[jRoi].RoiCOM.x;
                    roiRotationPoint.y = pinnacleFiles->roisPropVector[jRoi].RoiCOM.y;
                    roiRotationPoint.z = pinnacleFiles->roisPropVector[jRoi].RoiCOM.z;

                    break;
                }
                if (jRoi == pinnacleFiles->roisPropVector.size()-1)
                {
                    logstream << class_member << "ERROR -->  Could not found the structure that specifies the center of rotation in rotation and translation model of Roi " << rtStructure->roiVector[iRoi].RoiName << " in the selected ROI list."<< std::endl;
                    return false;
                }
            }
        }




        std::vector<float> temp7(numTrials*numFractions * 9, 0);
        simulationModelParams.at(iRoi).RotationMatrices = temp7;
        std::vector<float> temp8(numTrials*numFractions * 3, 0);
        simulationModelParams.at(iRoi).TranslationVectors = temp8;


        for (auto iTrial = 0; iTrial < numTrials; iTrial++)
        {



            // Now add rotTransModel model for the
            if (rotTransModel.IsModelSet())
            {

                // Generate random displacment errors for Roi number iRoi for all fractions
                deltaXRand = randn(0, rotTransModel.randTranErrorSigma.x, numFractions);
                deltaYRand = randn(0, rotTransModel.randTranErrorSigma.y, numFractions);
                deltaZRand = randn(0, rotTransModel.randTranErrorSigma.z, numFractions);

                // Generate random rotation errors for Roi number iRoi for all fractions
                xrotRand= randn(0, rotTransModel.randRotErrorSigma.x, numFractions);
                yrotRand = randn(0, rotTransModel.randRotErrorSigma.y, numFractions);
                zrotRand= randn(0, rotTransModel.randRotErrorSigma.z, numFractions);

                // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
                //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
                // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
                // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
            }




            for (auto iFrac = 0; iFrac < numFractions; iFrac++)
            {

                auto indexTransformation = iTrial*numFractions + iFrac;

                if (indexTransformation < numFractions) // no uncertainty in the first transformation consisting of numFractions fractions
                {
                    RotationMatrixFromEulerAngles(0,0,0,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2] =0;

                    continue;
                }

                if (modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet())
                {
                    // X1-O1=R1(X0-O1)+T1, where O1 center of rotation and R1 is rotation matrix, and T1 is a shift after rotation --> X1 = R1*X0+O1-R1*O1+T1 == RX0+T --> R=R1, T = O1-R1*O1+T1
                    float xrot = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyRandVec[indexTransformation];
                    float yrot = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyRandVec[indexTransformation];
                    float zrot = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyRandVec[indexTransformation];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    //T = O1-R1*O1+T1
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], rigidBodyRotationPoint, transformedCOM );
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x-transformedCOM.x + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyRandVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y-transformedCOM.y + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyRandVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z-transformedCOM.z + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyRandVec[indexTransformation];

                }
                else if (!modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // X2-O2=R2(X0-O2)+T2, where O2 center of rotation and R2 is rotation matrix --> X2 = R2*X0+O2-R2*O2 +T2 == RX0+T --> R=R2, T = O2-R2*O2+T2
                    float xrot = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrot = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrot = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    // T = O2-R2*O2 +T2
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], roiRotationPoint, transformedCOM );


                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = roiRotationPoint.x-transformedCOM.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = roiRotationPoint.y-transformedCOM.y +simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = roiRotationPoint.z-transformedCOM.z +simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac];

                }

                else if (modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // if we have both motions (Rigidbody and individual structure motions)
                    // X1-O1=R1(X0-O1)+T1 (1)
                    // X2-On2=R2(X1-On2)+T2 (2)--> where On2 is the trasform center of rotation of ROI number iRoi with rigidbody motion --> On2= R1(O2-O1)+ T1+O1 (3)
                    // (1),(2), and (3) --> X2 = R2(R1(X0-O1)+T1+O1 - R1(O2-O1)-T1) + T2 +  R1(O2-O1)+ T1+O1 = R2R1X0 + R2T1-R2R1O2 - R2T1
                    // X2= R2 R1 X0 - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2 == RX0+T --> R=R2*R1  , T = - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2

                    float xrotRigidBody = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyRandVec[indexTransformation];
                    float yrotRigidBody = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyRandVec[indexTransformation];
                    float zrotRigidBody = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyRandVec[indexTransformation];
                    float xrotRoi = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrotRoi = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrotRoi = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    std::vector<float> R1(9,0);
                    std::vector<float> R2(9,0);
                    RotationMatrixFromEulerAngles(xrotRigidBody,yrotRigidBody,zrotRigidBody,&R1[0]);
                    RotationMatrixFromEulerAngles(xrotRoi,yrotRoi,zrotRoi,&R2[0]);
                    RotMatrixTimesRotMatrix(&R2[0],&R1[0],&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    Point3 R2R1O2;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9],roiRotationPoint,R2R1O2); // R2*R1*O2
                    Point3 O2_O1;
                    O2_O1.x= roiRotationPoint.x-rigidBodyRotationPoint.x;
                    O2_O1.y= roiRotationPoint.y-rigidBodyRotationPoint.y;
                    O2_O1.z= roiRotationPoint.z-rigidBodyRotationPoint.z;
                    Point3 R1TimesO2_O1;
                    RotMatrixTimesPoint3(&R1[0],O2_O1,R1TimesO2_O1);//  R1 (O2- O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac] + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyRandVec[indexTransformation] - R2R1O2.x +R1TimesO2_O1.x; // O1+T2+T1 - R2R1O2 + R1(O2-O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y + simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac] + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyRandVec[indexTransformation]- R2R1O2.y + R1TimesO2_O1.y;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z + simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac] + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyRandVec[indexTransformation]- R2R1O2.z + R1TimesO2_O1.z;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                }

                else if (!modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet()) // TODO: no uncertainty in this case so DVH is enough in this case this could be handled in the pdvh computation part.
                {

                }

            } // iFrac
        } //  iTrial

    } // iRoi for


    return true;

}

bool RobustnessAnalyzer::generateTransformationsParamsWithIntraFractionPinnacle()
{
    // TODO: This is written in a way that it works with the old function that only had systematic and random uncertianties
    // need to change it in a way that it supports more complex scenarios

    // The outputs of this function which are used in the computation part are simulationModelParams.at(iRoi).TranslationVectors, simulationModelParams.at(iRoi).RotationMatrices
    // http://www.rit.edu/cos/uphysics/uncertainties/Uncertaintiespart2.html --> Uncertainties and Error Propagation

    std::string class_member = "RobustnessAnalyzer::generateTransformationsParamsPinnacle:" ;

    if (modelData==0)
    {
        logstream << class_member << " modelData has not been set." << std::endl;
        return false;
    }
    if (pinnacleFiles==NULL)
    {
        logstream << class_member << " pinnacleFiles pointer has not been set." << std::endl;
        return false;
    }

    if (pinnacleFiles->rtDosePinn==NULL)
    {
        logstream << class_member << " rtDose pointer has not been set for PinnacleFiles object." << std::endl;
        return false;
    }


    if (numFractions==0)
    {
        logstream << class_member << " numFractions has not been set." << std::endl;
        return false;
    }

    if (numSubFractions==0)
    {
        logstream << class_member << " numSubFractions has not been set. It is needed to model intrafraction motion" << std::endl;
        return false;
    }

    if (pinnacleFiles->roisPropVector.size() ==0)
    {
        logstream << class_member << " No roi is found in roisPropVector in PinnacleFiles --> forgot to set and compute roi props?." << std::endl;
        return false;

    }
    // Fill the simulation parameters from modelData : TODO: we need to modify this for delineation uncertainty

    //simulationModelParams
    {
        auto temp = std::vector<SimulationModelParams>(pinnacleFiles->roisPropVector.size(),SimulationModelParams());
        simulationModelParams = temp;
    }

    // Systematic RigidBody
    std::vector<float> deltaXSystematicRigidBodyVec;
    std::vector<float> deltaYSystematicRigidBodyVec;
    std::vector<float> deltaZSystematicRigidBodyVec;
    std::vector<float> rotXSystematicRigidBodyVec;
    std::vector<float> rotYSystematicRigidBodyVec;
    std::vector<float> rotZSystematicRigidBodyVec;

    std::vector<float> xdispRigidBodyRandVec;
    std::vector<float> ydispRigidBodyRandVec;
    std::vector<float> zdispRigidBodyRandVec;
    std::vector<float> xrotRigidBodyRandVec;
    std::vector<float> yrotRigidBodyRandVec;
    std::vector<float> zrotRigidBodyRandVec;

    // intra fraction rigidbody motion
    std::vector<float> xDispRigidBodyIntraFractionVec;
    std::vector<float> yDispRigidBodyIntraFractionVec;
    std::vector<float> zDispRigidBodyIntraFractionVec;

    std::vector<float> xRotRigidBodyIntraFractionVec;
    std::vector<float> yRotRigidBodyIntraFractionVec;
    std::vector<float> zRotRigidBodyIntraFractionVec;


    // These will contain the overall motion in each subfraction (sys+random+intra)
    std::vector<float> xdispRigidBodyOverallVec;
    std::vector<float> ydispRigidBodyOverallVec;
    std::vector<float> zdispRigidBodyOverallVec;
    std::vector<float> xrotRigidBodyOverallVec;
    std::vector<float> yrotRigidBodyOverallVec;
    std::vector<float> zrotRigidBodyOverallVec;


    // these will be used for each ROI
    //    std::vector<float> deltaXSystematic;
    //    std::vector<float> deltaYSystematic;
    //    std::vector<float> deltaZSystematic;
    //    std::vector<float> rotXSystematic;
    //    std::vector<float> rotYSystematic;
    //    std::vector<float> rotZSystematic;

    std::vector<float> deltaXRand;
    std::vector<float> deltaYRand;
    std::vector<float> deltaZRand;
    std::vector<float> xrotRand;
    std::vector<float> yrotRand;
    std::vector<float> zrotRand;

    Point3 rigidBodyRotationPoint;
    Point3 roiRotationPoint; // each roi rotation point

    if ( modelData->rigidBodyMotionExist)
    {
        // Generate systematic displacment errors
        deltaXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.x, numTrials);
        deltaYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.y, numTrials);
        deltaZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysTranErrorSigma.z, numTrials);

        // Generate systematic rotation errors
        rotXSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.x, numTrials);
        rotYSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.y, numTrials);
        rotZSystematicRigidBodyVec = randn(0, modelData->rigidBodyMotionModel->sysRotErrorSigma.z, numTrials);


        // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center


        for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); ++iRoi)
        {
            if  (pinnacleFiles->roisPropVector[iRoi].name.compare(modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM)==0)
            {
                rigidBodyRotationPoint.x = pinnacleFiles->roisPropVector[iRoi].RoiCOM.x;
                rigidBodyRotationPoint.y = pinnacleFiles->roisPropVector[iRoi].RoiCOM.y;
                rigidBodyRotationPoint.z = pinnacleFiles->roisPropVector[iRoi].RoiCOM.z;
                break;
            }
            if (iRoi == pinnacleFiles->roisPropVector.size()-1)
            {
                logstream << class_member << " The structure that specifies the center of rotation for rigid body motion is not found in roiVector." << std::endl;
                return false;
            }
        }


        // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
        // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
    }



    if ( modelData->rigidBodyMotionExist)
    {

        if (numSubFractions <1)
        {
            logstream << class_member << " Either numSubFractions has not been set. Or  It is set wrongly. For intrafraction motion, it needs to be set as an integer value >0  " << std::endl;
            return false;
        }

        float numActualFractionsFloat= static_cast<float>(numFractions)/static_cast<float>(numSubFractions);
        int numActualFractions= static_cast<int>(numActualFractionsFloat);

        if (std::floor(numActualFractionsFloat) != numActualFractionsFloat)
        {
            logstream << class_member << "numFractions should be multiple of numSubFractions." << std::endl;
            return false;
        }


        xdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.x, numTrials*numActualFractions);
        ydispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.y, numTrials*numActualFractions);
        zdispRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randTranErrorSigma.z, numTrials*numActualFractions);
        xrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.x, numTrials*numActualFractions);
        yrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.y, numTrials*numActualFractions);
        zrotRigidBodyRandVec = randn(0, modelData->rigidBodyMotionModel->randRotErrorSigma.z, numTrials*numActualFractions);


        xdispRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);
        ydispRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);
        zdispRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);
        xrotRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);
        yrotRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);
        zrotRigidBodyOverallVec = std::vector<float>(numTrials*numFractions,0.0);



        if (IsIntraFxRandomGaussian) // if interafraction motion has not pattern and random
        {
            xDispRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x, numTrials*numFractions);
            yDispRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y, numTrials*numFractions);;
            zDispRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z, numTrials*numFractions);;
            xRotRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x, numTrials*numFractions);;
            yRotRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y, numTrials*numFractions);;
            zRotRigidBodyIntraFractionVec = randn(0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z, numTrials*numFractions);;



        }
        else  // when there is a pattern of motion we
        {
            xDispRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);
            yDispRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);
            zDispRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);
            xRotRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);
            yRotRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);
            zRotRigidBodyIntraFractionVec = std::vector<float>( numTrials*numFractions,0);


            int iOverallElement;

            for (int iSim = 0; iSim < numTrials; ++iSim) {
                std::vector<float> xDispRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x,numFractions/numSubFractions);
                std::vector<float> yDispRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y,numFractions/numSubFractions);
                std::vector<float> zDispRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z,numFractions/numSubFractions);
                std::vector<float> xRotRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x,numFractions/numSubFractions);
                std::vector<float> yRotRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y,numFractions/numSubFractions);
                std::vector<float> zRotRigidBodyMaxError= randn(0.0, modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z,numFractions/numSubFractions);



                for (int iActualFraction = 0; iActualFraction < numFractions/numSubFractions; ++iActualFraction)
                {
                    std::vector<float> xDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,xDispRigidBodyMaxError[iActualFraction],numSubFractions);
                    std::vector<float> yDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,yDispRigidBodyMaxError[iActualFraction],numSubFractions);
                    std::vector<float> zDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,zDispRigidBodyMaxError[iActualFraction],numSubFractions);

                    std::vector<float> xRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,xRotRigidBodyMaxError[iActualFraction],numSubFractions);
                    std::vector<float> yRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,yRotRigidBodyMaxError[iActualFraction],numSubFractions);
                    std::vector<float> zRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,zRotRigidBodyMaxError[iActualFraction],numSubFractions);

                    for (int iSubFx = 0; iSubFx < numSubFractions; ++iSubFx)
                    {
                        iOverallElement= iSim*numFractions +  iActualFraction*numSubFractions+iSubFx;

                        xDispRigidBodyIntraFractionVec[iOverallElement] = xDispRigidBodyIntraFractionMotionPatternVec[iSubFx];
                        yDispRigidBodyIntraFractionVec[iOverallElement] = yDispRigidBodyIntraFractionMotionPatternVec[iSubFx];
                        zDispRigidBodyIntraFractionVec[iOverallElement] = zDispRigidBodyIntraFractionMotionPatternVec[iSubFx];

                        xRotRigidBodyIntraFractionVec[iOverallElement] = xRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                        yRotRigidBodyIntraFractionVec[iOverallElement] = yRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                        zRotRigidBodyIntraFractionVec[iOverallElement] = zRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                    }
                }
            }
        }
        int iOverallElement;

        for (int iSim = 0; iSim < numTrials; ++iSim) {
            for (int iActualFraction = 0; iActualFraction < numFractions/numSubFractions; ++iActualFraction)
            {

                for (int iSubFx = 0; iSubFx <numSubFractions ; ++iSubFx)
                {
                    iOverallElement= iSim*numFractions +  iActualFraction*numSubFractions+iSubFx;

                    xdispRigidBodyOverallVec[iOverallElement]=xdispRigidBodyRandVec[iActualFraction] + xDispRigidBodyIntraFractionVec[iOverallElement];
                    ydispRigidBodyOverallVec[iOverallElement]=ydispRigidBodyRandVec[iActualFraction] + yDispRigidBodyIntraFractionVec[iOverallElement];
                    zdispRigidBodyOverallVec[iOverallElement]=zdispRigidBodyRandVec[iActualFraction] + zDispRigidBodyIntraFractionVec[iOverallElement];
                    xrotRigidBodyOverallVec[iOverallElement]=xrotRigidBodyRandVec[iActualFraction] + xRotRigidBodyIntraFractionVec[iOverallElement];
                    yrotRigidBodyOverallVec[iOverallElement]=yrotRigidBodyRandVec[iActualFraction] + yRotRigidBodyIntraFractionVec[iOverallElement];
                    zrotRigidBodyOverallVec[iOverallElement]=zrotRigidBodyRandVec[iActualFraction] + zRotRigidBodyIntraFractionVec[iOverallElement];
                }
            }
        }

    }




    for (int iRoi = 0; iRoi < pinnacleFiles->roisPropVector.size(); ++iRoi)
    {



        //

        if (modelData->gumMap.find(pinnacleFiles->roisPropVector.at(iRoi).name)==modelData->gumMap.end())
        {
            // not found // This should not happen since we check the roiVector when parsing XML config file
            logstream << class_member << "ERROR --> This should not happen since we check the roiVector when parsing XML config file. " << std::endl;
            return false;
        }

        std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator  iter =modelData->gumMap.find(pinnacleFiles->roisPropVector[iRoi].name);

        RotationAndTranslationModel rotTransModel = boost::get<0>(iter->second);

        // Now add rotTransModel model for the
        if (rotTransModel.IsModelSet())
        {


            // Generate systematic displacment errors for Roi number iRoi
            simulationModelParams.at(iRoi).deltaXSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).deltaYSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).deltaZSystematicVec = randn(0, rotTransModel.sysTranErrorSigma.z, numTrials);





            // Generate systematic rotation errors for Roi number iRoi
            simulationModelParams.at(iRoi).rotXSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.x, numTrials);
            simulationModelParams.at(iRoi).rotYSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.y, numTrials);
            simulationModelParams.at(iRoi).rotZSystematicVec = randn(0, rotTransModel.sysRotErrorSigma.z, numTrials);


            // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
            //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
            // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
            // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;

            for (int jRoi = 0; jRoi < pinnacleFiles->roisPropVector.size(); ++jRoi)
            {
                if  (pinnacleFiles->roisPropVector[jRoi].name.compare(rotTransModel.rotCenter.RoiIDForRotationAroundCOM)!=0)
                {
                    roiRotationPoint.x = pinnacleFiles->roisPropVector[jRoi].RoiCOM.x;
                    roiRotationPoint.y = pinnacleFiles->roisPropVector[jRoi].RoiCOM.y;
                    roiRotationPoint.z = pinnacleFiles->roisPropVector[jRoi].RoiCOM.z;

                    break;
                }
                if (jRoi == pinnacleFiles->roisPropVector.size()-1)
                {
                    logstream << class_member << "ERROR -->  Could not found the structure that specifies the center of rotation in rotation and translation model of Roi " << rtStructure->roiVector[iRoi].RoiName << " in the selected ROI list."<< std::endl;
                    return false;
                }
            }
        }




        std::vector<float> temp7(numTrials*numFractions * 9, 0);
        simulationModelParams.at(iRoi).RotationMatrices = temp7;
        std::vector<float> temp8(numTrials*numFractions * 3, 0);
        simulationModelParams.at(iRoi).TranslationVectors = temp8;


        for (auto iTrial = 0; iTrial < numTrials; iTrial++)
        {



            // Now add rotTransModel model for the
            if (rotTransModel.IsModelSet())
            {


                // Generate random displacment errors for Roi number iRoi for all fractions
                std::vector<float> tempDeltaXRandForActualFraction =  randn(0, rotTransModel.randTranErrorSigma.x, numFractions/numSubFractions);
                std::vector<float> tempDeltaYRandForActualFraction =  randn(0, rotTransModel.randTranErrorSigma.y, numFractions/numSubFractions);
                std::vector<float> tempDeltaZRandForActualFraction =  randn(0, rotTransModel.randTranErrorSigma.z, numFractions/numSubFractions);

                std::vector<float> tempRotXRandForActualFraction =  randn(0, rotTransModel.randRotErrorSigma.x, numFractions/numSubFractions);
                std::vector<float> tempRotYRandForActualFraction =  randn(0, rotTransModel.randRotErrorSigma.y, numFractions/numSubFractions);
                std::vector<float> tempRotZRandForActualFraction =  randn(0, rotTransModel.randRotErrorSigma.z, numFractions/numSubFractions);


                if (IsIntraFxRandomGaussian) // if interafraction motion has not pattern and random
                {
                    deltaXRand = randn(0, rotTransModel.intraFractionTranErrorSigma.x, numFractions);
                    deltaYRand = randn(0, rotTransModel.intraFractionTranErrorSigma.y, numFractions);
                    deltaZRand = randn(0, rotTransModel.intraFractionTranErrorSigma.z, numFractions);
                    xrotRand= randn(0, rotTransModel.intraFractionRotErrorSigma.x, numFractions);
                    yrotRand = randn(0, rotTransModel.intraFractionRotErrorSigma.y, numFractions);
                    zrotRand= randn(0, rotTransModel.intraFractionRotErrorSigma.z, numFractions);

                }
                else
                {
                    deltaXRand = std::vector<float>(numFractions,0.0);
                    deltaYRand = std::vector<float>(numFractions,0.0);
                    deltaZRand = std::vector<float>(numFractions,0.0);
                    xrotRand= std::vector<float>(numFractions,0.0);
                    yrotRand = std::vector<float>(numFractions,0.0);
                    zrotRand= std::vector<float>(numFractions,0.0);

                    int iOverallElement=0;
                    for (int iActualFraction = 0; iActualFraction < numFractions/numSubFractions; ++iActualFraction)
                    {
                        std::vector<float> xDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionTranErrorSigma.x),numSubFractions);
                        std::vector<float> yDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionTranErrorSigma.y),numSubFractions);
                        std::vector<float> zDispRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionTranErrorSigma.z),numSubFractions);

                        std::vector<float> xRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionRotErrorSigma.x),numSubFractions);
                        std::vector<float> yRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionRotErrorSigma.y),numSubFractions);
                        std::vector<float> zRotRigidBodyIntraFractionMotionPatternVec = signFreelinspace(0.0f,randn(0,rotTransModel.intraFractionRotErrorSigma.z),numSubFractions);

                        for (int iSubFx = 0; iSubFx < numSubFractions; ++iSubFx)
                        {
                            iOverallElement= iActualFraction*numSubFractions+iSubFx;

                            deltaXRand[iOverallElement] = xDispRigidBodyIntraFractionMotionPatternVec[iSubFx];
                            deltaYRand[iOverallElement] = yDispRigidBodyIntraFractionMotionPatternVec[iSubFx];
                            deltaZRand[iOverallElement] = zDispRigidBodyIntraFractionMotionPatternVec[iSubFx];

                            xrotRand[iOverallElement] = xRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                            yrotRand[iOverallElement] = yRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                            zrotRand[iOverallElement] = zRotRigidBodyIntraFractionMotionPatternVec[iSubFx];
                        }
                    }
                }

                for (int iActualFraction = 0; iActualFraction < numFractions/numSubFractions; ++iActualFraction)
                {
                    int startIndOfIthActualFraction = iActualFraction*(numSubFractions);
                    int endIndOfIthActualFraction = iActualFraction*(numSubFractions)+numSubFractions;

                    for (int iOverallElement = startIndOfIthActualFraction; iOverallElement <endIndOfIthActualFraction ; ++iOverallElement)
                    {
                        deltaXRand[iOverallElement]=tempDeltaXRandForActualFraction[iActualFraction] + deltaXRand[iOverallElement];
                        deltaYRand[iOverallElement]=tempDeltaYRandForActualFraction[iActualFraction] + deltaYRand[iOverallElement];
                        deltaZRand[iOverallElement]=tempDeltaZRandForActualFraction[iActualFraction] + deltaZRand[iOverallElement];
                        xrotRand[iOverallElement]=tempRotXRandForActualFraction[iActualFraction] + xrotRand[iOverallElement];
                        yrotRand[iOverallElement]=tempRotYRandForActualFraction[iActualFraction] + yrotRand[iOverallElement];
                        zrotRand[iOverallElement]=tempRotZRandForActualFraction[iActualFraction] + zrotRand[iOverallElement];
                    }
                }
                // If we want to rotate about ROICOM of an ROI, we shoudl find matching Roi index that we want to rotate about it's rotation center
                //roiToRotateAbout = modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM;
                // machingROIIndex=find(roiToRotateAbout == rtStructure->roiVector(:).RoiName)
                // rotationPoint=rtStructure->roiVector.at(machingROIIndex)->RoiCOM;
            }




            for (auto iFrac = 0; iFrac < numFractions; iFrac++)
            {

                auto indexTransformation = iTrial*numFractions + iFrac;

                if (indexTransformation < numFractions) // no uncertainty in the first transformation consisting of numFractions fractions
                {
                    RotationMatrixFromEulerAngles(0,0,0,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1] =0;
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2] =0;

                    continue;
                }

                if (modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet())
                {
                    // X1-O1=R1(X0-O1)+T1, where O1 center of rotation and R1 is rotation matrix, and T1 is a shift after rotation --> X1 = R1*X0+O1-R1*O1+T1 == RX0+T --> R=R1, T = O1-R1*O1+T1
                    float xrot = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyOverallVec[indexTransformation];
                    float yrot = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyOverallVec[indexTransformation];
                    float zrot = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyOverallVec[indexTransformation];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    //T = O1-R1*O1+T1
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], rigidBodyRotationPoint, transformedCOM );
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x-transformedCOM.x + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyOverallVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y-transformedCOM.y + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyOverallVec[indexTransformation];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z-transformedCOM.z + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyOverallVec[indexTransformation];

                }
                else if (!modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // X2-O2=R2(X0-O2)+T2, where O2 center of rotation and R2 is rotation matrix --> X2 = R2*X0+O2-R2*O2 +T2 == RX0+T --> R=R2, T = O2-R2*O2+T2
                    float xrot = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrot = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrot = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    // indexTransformation * 9:(indexTransformation * 9+8) // rotation start -stop
                    RotationMatrixFromEulerAngles(xrot,yrot,zrot,&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);

                    // indexTransformation * 3:(indexTransformation * 3+2) // translation start -stop
                    // T = O2-R2*O2 +T2
                    Point3 transformedCOM;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9], roiRotationPoint, transformedCOM );


                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = roiRotationPoint.x-transformedCOM.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = roiRotationPoint.y-transformedCOM.y +simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac];
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = roiRotationPoint.z-transformedCOM.z +simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac];

                }

                else if (modelData->rigidBodyMotionExist && rotTransModel.IsModelSet())
                {
                    // if we have both motions (Rigidbody and individual structure motions)
                    // X1-O1=R1(X0-O1)+T1 (1)
                    // X2-On2=R2(X1-On2)+T2 (2)--> where On2 is the trasform center of rotation of ROI number iRoi with rigidbody motion --> On2= R1(O2-O1)+ T1+O1 (3)
                    // (1),(2), and (3) --> X2 = R2(R1(X0-O1)+T1+O1 - R1(O2-O1)-T1) + T2 +  R1(O2-O1)+ T1+O1 = R2R1X0 + R2T1-R2R1O2 - R2T1
                    // X2= R2 R1 X0 - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2 == RX0+T --> R=R2*R1  , T = - R2 R1 O2 + R1 (O2- O1) + O1 + T1 + T2

                    float xrotRigidBody = rotXSystematicRigidBodyVec[iTrial] + xrotRigidBodyRandVec[indexTransformation];
                    float yrotRigidBody = rotYSystematicRigidBodyVec[iTrial] + yrotRigidBodyRandVec[indexTransformation];
                    float zrotRigidBody = rotZSystematicRigidBodyVec[iTrial] + zrotRigidBodyRandVec[indexTransformation];
                    float xrotRoi = simulationModelParams.at(iRoi).rotXSystematicVec[iTrial] + xrotRand[iFrac];
                    float yrotRoi = simulationModelParams.at(iRoi).rotYSystematicVec[iTrial] + yrotRand[iFrac];
                    float zrotRoi = simulationModelParams.at(iRoi).rotZSystematicVec[iTrial] + zrotRand[iFrac];
                    std::vector<float> R1(9,0);
                    std::vector<float> R2(9,0);
                    RotationMatrixFromEulerAngles(xrotRigidBody,yrotRigidBody,zrotRigidBody,&R1[0]);
                    RotationMatrixFromEulerAngles(xrotRoi,yrotRoi,zrotRoi,&R2[0]);
                    RotMatrixTimesRotMatrix(&R2[0],&R1[0],&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9]);
                    Point3 R2R1O2;
                    RotMatrixTimesPoint3(&simulationModelParams.at(iRoi).RotationMatrices[indexTransformation * 9],roiRotationPoint,R2R1O2); // R2*R1*O2
                    Point3 O2_O1;
                    O2_O1.x= roiRotationPoint.x-rigidBodyRotationPoint.x;
                    O2_O1.y= roiRotationPoint.y-rigidBodyRotationPoint.y;
                    O2_O1.z= roiRotationPoint.z-rigidBodyRotationPoint.z;
                    Point3 R1TimesO2_O1;
                    RotMatrixTimesPoint3(&R1[0],O2_O1,R1TimesO2_O1);//  R1 (O2- O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 0]  = rigidBodyRotationPoint.x + simulationModelParams.at(iRoi).deltaXSystematicVec[iTrial] + deltaXRand[iFrac] + deltaXSystematicRigidBodyVec[iTrial] + xdispRigidBodyOverallVec[indexTransformation] - R2R1O2.x +R1TimesO2_O1.x; // O1+T2+T1 - R2R1O2 + R1(O2-O1)

                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 1]  = rigidBodyRotationPoint.y + simulationModelParams.at(iRoi).deltaYSystematicVec[iTrial] + deltaYRand[iFrac] + deltaYSystematicRigidBodyVec[iTrial] + ydispRigidBodyOverallVec[indexTransformation]- R2R1O2.y + R1TimesO2_O1.y;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                    simulationModelParams.at(iRoi).TranslationVectors[indexTransformation * 3 + 2]  = rigidBodyRotationPoint.z + simulationModelParams.at(iRoi).deltaZSystematicVec[iTrial] + deltaZRand[iFrac] + deltaZSystematicRigidBodyVec[iTrial] + zdispRigidBodyOverallVec[indexTransformation]- R2R1O2.z + R1TimesO2_O1.z;// // O1+T2+T1 - R2R1O2 + R1(O2-O1)
                }

                else if (!modelData->rigidBodyMotionExist && !rotTransModel.IsModelSet()) // TODO: no uncertainty in this case so DVH is enough in this case this could be handled in the pdvh computation part.
                {

                }

            } // iFrac
        } //  iTrial

    } // iRoi for


    return true;

    return true;

}

template<typename T,typename TDoseArray, typename TQueryPts, typename TRotationMatrices, typename TTranslationVectors>
bool RobustnessAnalyzer::calculateAllDiffAndCulmulativeDvhsForPinnaclePatientOnCPU(std::vector<T> &_cDVHForTrial,
                                                                                   std::vector<T> &_diffDVHForTrial,
                                                                                   std::vector<T> &_voxelDoseMeanForTrials,
                                                                                   std::vector<T> &_voxelDoseStdForTrials,
                                                                                   const std::vector<T> &_fractionsDoseWeights,
                                                                                   const std::vector<TDoseArray> &_doseArray,
                                                                                   const std::vector<TDoseArray> &_doseGridXvalues,
                                                                                   const std::vector<TDoseArray> &_doseGridYvalues,
                                                                                   const std::vector<TDoseArray> &_doseGridZvalues,
                                                                                   const std::vector<TQueryPts> &_QueryPointsCoordinatesX,
                                                                                   const std::vector<TQueryPts> &_QueryPointsCoordinatesY,
                                                                                   const std::vector<TQueryPts> &_QueryPointsCoordinatesZ,
                                                                                   const std::vector<TRotationMatrices> &_RotationMatrices,
                                                                                   const std::vector<TTranslationVectors> &_TranslationVectors,
                                                                                   const int & _numDoseBins,const float & _doseBinWidth)
{


    return true;
}

//void RobustnessAnalyzer::testLoadCSVFileTOPts()
//{
//    inPolygonPointsX[0].clear();
//    inPolygonPointsY[0].clear();
//    inPolygonPointsZ[0].clear();

//    std::ifstream in("F:\\UVA\\gitRepos\\GPUCodes\\MatlabGPU\\example\\pointCloudData.txt", std::ifstream::in);
//    string line;
//    vector< double > numbers;

//    while (std::getline(in, line, '\n'))
//    {
//        numbers.clear();
//        split(line, ',', numbers);
//        inPolygonPointsX[0].push_back(numbers[0]*10);
//        inPolygonPointsY[0].push_back(numbers[1]*10);
//        inPolygonPointsZ[0].push_back(numbers[2]*10);


//    }

//}

