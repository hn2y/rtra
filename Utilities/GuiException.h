#ifndef GUIEXCEPTION_H
#define GUIEXCEPTION_H


#include <string>
#include <sstream>

// forward declarations of template friend inserter & extractor
// required for friends whose definition is outside file scope
class GuiException;
// std::istream &operator>>(std::istream &stream, JJGDvcGuiException &e);
std::ostream &operator<<(std::ostream &stream, const GuiException &e);

/*****************************************************************************/

// class GuiException : public QtConcurrent::Exception
class GuiException : public std::exception
{
  public:

    mutable std::ostringstream mStream;
    mutable std::string mWhat;

  public:

    // mandated by Qt for thread safety
    // virtual void raise() const { throw *this; }
    // virtual QtConcurrent::Exception * clone() const
    //   { return new JJGDvcGuiException(*this); }

    // copy constructor, required to copy mStream contents
    GuiException(const GuiException &e)
      // : QtConcurrent::Exception() { mWhat = e.mStream.str(); }
      { mWhat = e.mStream.str(); }

    // over-rides std::exception.what()
    virtual const char *what() const throw() {
      if ( mStream.str().size() ) {
        mWhat += mStream.str();
        mStream.str("");
      }
      return mWhat.c_str();
    }

    // allows stream-style writes to exception
    template <typename T> GuiException& operator<<(const T& t) {
      mStream << t;
      return *this;
    }

    // allows std::ostream manipulators (functions) to be used
    typedef std::ostream& (*ostream_manipulator)(std::ostream&);
    GuiException& operator<<(ostream_manipulator m)
    {
      m(mStream);
      return *this;
    }

    // constructors / destructors
    // GuiException( ) : QtConcurrent::Exception(), mWhat("") { }
    // GuiException(std::string s) : QtConcurrent::Exception(), mWhat(s) { }
    GuiException( ) : mWhat("") { }
    GuiException(std::string s) : mWhat(s) { }
    ~GuiException() throw() { }
};


#endif // GUIEXCEPTION_H
