#include "rtfilesparser.h"
#include <QFileInfo>

#include "PinnacleFiles.h"


bool rtFilesParser::readPinnaclePatient(QString sPinnPatientFilePath, Patient *oPatient)
{
    return false;
}

bool rtFilesParser::readPinnacleRigionOfInterestForAGivenRtStructure(RTStructureSet *oRtStructure, QVector<RegionOfInterest> &ROIsQVector)
{

    return false;
}







bool rtFilesParser::readDicomPatientFolder(QString sPatientFolderPath, QVector<Patient> &_patientQVector)
{



    _patientQVector.clear();

    return false;
}

bool rtFilesParser::readDicomPatientFromSelectedFiles(QVector<QString> &sPatientDicomFilesSelectedPath, QVector<Patient> &_patientQVector)
{

    return false;
}

bool rtFilesParser::readDicomRTStructueSetsForAGivenPatient(Patient *oPatient, QVector<RTStructureSet> &oRtStructureSetQVector)
{
    return false;
}

bool rtFilesParser::readDicomROISForAGivenRtStructure(RTStructureSet *oRtStructure, QVector<RegionOfInterest> &ROIsQVector)
{
    return false;
}






rtFilesParser::rtFilesParser(QObject *parent) : QObject(parent)
{

    patientFolder = QString(); // QString().isNull() will be true
    dicomFilesProp = NULL;

}

rtFilesParser::~rtFilesParser()
{
    if (NULL != dicomFilesProp)
    {
        delete(dicomFilesProp);
    }

}



bool rtFilesParser::detectPatientFolderType()
{

    if (patientFolder.isNull())
    {
        qDebug() << "Patient folder is not set!" ;
        return false;
    }

    // check if Patient file exist
    QFileInfo patientFilePathInfo(QDir(patientFolder).filePath("Patient"));

    if (patientFilePathInfo.exists() && patientFilePathInfo.isFile())
    {
        // TODO: Check the content of the Patient file to make sure, it is indeed a pinnacle patient folder
       isPinnaclePatientFolder = true;
       return true;
    }
    else // now check if the it is a dicom file
    {
        qDebug() << "testing for a DICOM folder";
        if (NULL != dicomFilesProp)
        {
            delete(dicomFilesProp);
            dicomFilesProp = new DicomFilesProp();
        }
        else
        {
            dicomFilesProp = new DicomFilesProp();
        }
        dicomFilesProp->setDicomFolderPath(patientFolder.toStdString());
        dicomFilesProp->parseFolder();

        if (dicomFilesProp->dicomFilesVec.size()==0)
        {
            qDebug() << "No dicom files found in the folder" ;
            isDicomFolder = false;
            return false;
        }
        else
        {
            qDebug() << "Number of dicom files in folder" << patientFolder << " is : " << dicomFilesProp->dicomFilesVec.size();
            isDicomFolder = true;
            dicomFilesProp->printParsedFolderInfo();

            //DICOM :  Here we load all the Rois infomation in the Rtstructure except the data, i.e. contour data, ...

            if (!patientQVector.isEmpty())
            {
                patientQVector.clear();
            }


            patientQVector = QVector<Patient>(dicomFilesProp->PatientsVec.size());

            for (int iPatient = 0; iPatient < patientQVector.size(); ++iPatient)
            {


                patientQVector[iPatient].sPatientID = QString::fromStdString(dicomFilesProp->PatientsVec.at(iPatient).PatientID);

                //readDicomPatientFromSelectedFiles()
            }



            // Choose some ROIs
            // Make Mask for selected ROIs

        }



    }







}

bool rtFilesParser::setPatientFolder(QString _patientFolder)
{
    QDir dir(_patientFolder);
    if (dir.exists())
    {
        patientFolder  = _patientFolder;
    }
    else
    {
        qDebug() <<  "Patient Folder: "<< _patientFolder << " do not exist!!" ;


        return false;
    }

    return true;
}
