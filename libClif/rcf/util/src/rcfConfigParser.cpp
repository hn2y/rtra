#include "rcfLineParser.hpp"
#include "rcfConfigParser.hpp"
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <errno.h>
#include <map>

using namespace std;

void rcfConfigParser::basicInit()
{
   nlines = 0;
   is_ready = 0;
   func = NULL;
   memset(lines,0,sizeof(lines));
   memset(last_tag,0,sizeof(last_tag));
   last_found = 0;

   // set parser token
   parse_token = NULL;
   parse_token = new char[2];
   if(NULL == parse_token)
   {
      fprintf(stderr,"ERROR: rcfConfigParser::basicInit failure to allocate memory for the parser token \n");
   }
   else
   {
      strcpy(parse_token,":");
   }
   admin = NULL;
   tables = NULL;
   ntables = 0;
   fid = NULL;
   parser_labels.clear();
   parsers.clear();
   my_parsers.clear();
   write_as_clif = false;
   databaseConfigPath.clear();
   resolvedConfigPath.clear();
   keepSpacesFlag = false;
   
}

std::ostream& operator<<(std::ostream& output, rcfConfigParser* parser)
{
    map<string, string>::iterator itr;
    map<string, string> parserMap = parser->getKeyValueMap();

    output << endl << "***** rcfConfigParser *****" << endl;
    for( itr = parserMap.begin(); itr != parserMap.end(); itr++ )
    {
        output << "tag: " << (*itr).first << " value: " << (*itr).second << endl;
    }
    output << "*****" << endl;
    
    return output;
}

rcfConfigParser::rcfConfigParser()
{
    func = NULL;
    fid = NULL;    
    parse_token = NULL;
    admin = NULL;
    tables = NULL;    
    basicInit();
}

rcfConfigParser::rcfConfigParser(char *path)
{
   basicInit();
   loadConfigFile(path);
}

int rcfConfigParser::loadConfigFile(char *configFileName)
{
   func = (char*)"rcfConfigParser::loadConfigFile";
   char *upd_configFileName = NULL;

   if(NULL == configFileName)
   {
      fprintf(stderr," ERROR: %s NULL pointer passed instead of configFileName \n",func);
      return(CF_FAIL);
   }
   if(0 == strlen(configFileName))
   {
      fprintf(stderr," ERROR: %s Empty string passed as configFileName \n",func);
      return(CF_FAIL);
   }


   if(NULL != admin)
   {
      if(admin->isReady())
      {
         int verify = 0;
         upd_configFileName = admin->AppendPathToConfig(configFileName,&verify);
         if( (NULL == upd_configFileName) || (0 == verify) )
         {
            fprintf(stderr,"ERROR: %s failed to append path to config file %s\n",func,configFileName);
            reportError("ERROR: Failure to append path to config file");
            return(CF_FAIL);
         }
         fid = NULL;
         // open the file
         fid = fopen(upd_configFileName,"r");
         if(NULL == fid)
         {
            fprintf(stderr,"ERROR: %s failed to open the configuration file %s\n",func,upd_configFileName);
            return(CF_FAIL);
         }
         databaseConfigPath = configFileName;
         resolvedConfigPath = upd_configFileName;
      }
   }
   else
   {
      fid = NULL;
      // open the file
      fid = fopen(configFileName,"r");
      resolvedConfigPath = configFileName;
      databaseConfigPath.clear();
   }

   // the file should be opened now
   if(NULL == fid)
   {
      fprintf(stderr,"ERROR: %s failed to open the configuration file %s\n",func,configFileName);
      return(CF_FAIL);
   }
   else
   {

      int rtn;
      rtn = readConfigFile();
      if(CF_OK == rtn)
      {
         if(NULL != upd_configFileName)
         {
             delete [] upd_configFileName;
         }
         upd_configFileName = NULL;       
         return(rtn);
      }
      // try to read as clif
      if(NULL != upd_configFileName)
      {
        rtn = readConfigFileAsClif(upd_configFileName);
        delete [] upd_configFileName;
        upd_configFileName = NULL;
      }
      else
      {
         rtn = readConfigFileAsClif(configFileName);
      }

      if(CF_OK == rtn)
      {
         return(rtn);
      }
      else
      {
        // if we are here, nothing worked, say so
        fprintf(stderr,"ERROR: %s failed to INTERPRET the configuration file %s\n",func,configFileName);
        return(CF_FAIL);
      }
   }

   // should not be able to get this far, but check and delete if we did
   if(NULL != upd_configFileName)
   {
       delete [] upd_configFileName;
   }
   upd_configFileName = NULL;

   return(CF_OK);
}

int rcfConfigParser::setRcfAdmin(rcfAdmin *usr_admin)
{
   func = (char*)"rcfConfigParser::setRcfAdmin";
   // sanity checks
   if(NULL == usr_admin)return(CF_FAIL);
   if(!(usr_admin->isReady()))return(CF_FAIL);
   admin = usr_admin;
   return(CF_OK);
}


int rcfConfigParser::buildParserFromClif(rcfClifParser *clif)
{
   func = (char*)"rcfConfigParser::buildParserFromClif";
   int rtn;
   int verify;
   // sanity checks

   if(NULL == clif)
   {
      reportError("Null pointer passed instead of argument");
      return(CF_FAIL);
   }
   if(!(clif->isClifReady()))
   {
      // invalid clif passed 
      reportError("Invalid clif passed as argument");
      return(CF_FAIL);
   } 
   int ndata;
   rtn = clif->getNumberOfDataObjectsInSelectedClif(&ndata);
   if( (CF_OK != rtn) || (ndata <= 0) )return(CF_FAIL);

   int size = 0;
   char *ctag = NULL;
   char *cvalue = NULL;

   // looks like we got valid clif data
   for(int i=0; i<ndata; i++)
   {
      ctag = clif->getHandleOfDataObjectInSelectedClif(i,&verify);
      if( (NULL == ctag) || (0 == verify) ) { continue; }
      if( strlen(ctag) <= 0) { continue; }
      for(unsigned int ii = 0; ii < strlen(ctag); ii++)
      {
          ctag[ii] = toupper(ctag[ii]);
      }
      cvalue = clif->getDataBufferInSelectedClif(i,&verify);
      if( (NULL == cvalue) || (0 == verify) ) continue; 
      if( strlen(cvalue) <= 0)continue;
      size = strlen(ctag)+strlen(cvalue)+3;
      // allocate more memory than needed, for light line
      // processing afterwards
      if(NULL != lines[nlines])
      {
         delete[] lines[nlines];
         lines[nlines] = NULL;
      }

      lines[nlines] = new char[size + 10];
      if(NULL == lines[nlines])
      {
         fprintf(stderr,"%s failure to allocate memory for line: %d, size of request: %d \n",func,nlines,size+10);
         return(CF_FAIL);
      }
      strcpy(lines[nlines],ctag);
      strcat(lines[nlines],":");
      strcat(lines[nlines],cvalue);
      strcat(lines[nlines],":");
      nlines++;
   }

   if(nlines > 0)is_ready = 1;
   return(CF_OK);
}


int rcfConfigParser::readConfigFileAsClif(char *path)
{
   func = (char*)"rcfConfigParser::readConfigFileAsClif";

   if(NULL == path)
   {
      fprintf(stderr,"%s NULL pointer passed as path \n",func);
      return(CF_FAIL);
   }
   rcfClifParser parser(path);
   if(!(parser.isClifReady()))
   {
      // this is not a clif, fail this function quietly 
      return(CF_FAIL);
   } 
   return( buildParserFromClif(&parser) );
}


int rcfConfigParser::readConfigFile()
{
   func = (char*)"rcfConfigParser::readConfigFile";
   char lbuffer[2049];
   int length;
   nlines = 0;
   int nbad = 0;


   if(NULL == fid)
   {
      fprintf(stderr,"%s Attempt to read a file that has not been opened \n",func);
      return(CF_FAIL);
   }
   bool stop = false;
   do
   {
      memset(lbuffer,0,2049);
      fgets(lbuffer,2048,fid);      
      length = 0;
      length = strlen(lbuffer);
      if(  0 != feof(fid) || (nlines >= RCF_MAXCONFIG) )
      {
          if( NULL != fid )
          {
              fclose(fid);
              fid = NULL;
          }
          //process last line if something is there
          if(length <= 0)break;
          else stop = true;
      }
      // allocate more memory than needed, for light line
      // processing afterwards
      if(NULL != lines[nlines])
      {
         delete[] lines[nlines];
         lines[nlines] = NULL;
      }

      lines[nlines] = new char[length + 10];
      if(NULL == lines[nlines])
      {
         fprintf(stderr,"%s failure to allocate memory for line: %d, size of request: %d \n",func,nlines,length+1);
         return(CF_FAIL);
      }
      strcpy(lines[nlines],lbuffer);
      //check the type of line
      std::string verify = lines[nlines];
      std::string colon = ":";
      size_t found;
      found = verify.find_first_of(":");
      if(found == std::string::npos) nbad++;
      // do not require terminating colon, let line parser deal with it, but warn
      if( nbad <= 0 )
      {
         found = verify.find_first_of(":",found+1);
         if(found == std::string::npos) 
         {
            fprintf(stderr,"%s WARNING: no terminating colon in config line, parser will ATTEMPT to parse a potentially malformed config file \n",func);
            fprintf(stderr,"line triggering this warning: %s\n",lines[nlines]);
            return(CF_FAIL);
         }
      }
      nlines++;
      
   }while(!stop);

   
   if(nbad > 0)
   {
      // do not report, we will try cliff instead
      for(int i=0; i<nlines; i++)
      {
         delete [] lines[i];
         lines[i] = NULL;
      }
      nlines = 0;
      return(CF_FAIL);
   }

   if(nlines > 0)is_ready = 1;
   return(CF_OK);

}

rcfConfigParser::~rcfConfigParser()
{
   func = (char*)"rcfConfigParser::~rcfConfigParser";
   if( lines != NULL )
    {
        for(int i = 0; i < nlines; i++)
        {
            if(NULL != lines[i])
            {
                delete[] lines[i];
                lines[i] = NULL;
            }
        }
    }

    if(NULL != parse_token)
    {
        delete[] parse_token;
        parse_token = NULL;
    }
    if( (ntables>0) && (NULL != tables) )
    {
        delete[] tables;
        tables = NULL;
    }

    if( NULL != fid )
    {
        fclose(fid);
        fid = NULL;
    }
     if(parsers.size() > 0 )
    {
       for(unsigned int i=0; i<parsers.size(); i++)
       {
         if(NULL != parser_labels[i])
         {
             delete parser_labels[i];
             parser_labels[i] = NULL;
         }
         if(my_parsers[i])
         {
            if(NULL != parsers[i])
            {
                delete parsers[i];
                parsers[i] = NULL;               
            }
         }
       }
         parsers.clear();
         my_parsers.clear();
    }
   
   if( NULL!= tables )
   {
       delete tables;
       tables = NULL;
   }
}

// Private function getUniqueTag searches for specified tag
// and extracts the string (value) associated with the tag.
// The function searches for the first occurence of the tag,
// delivers value if it finds the tag. Unlike the public interface
// function, it does not handle repeat requests, or rcfAdmin updates.

int rcfConfigParser::getUniqueTag(char *tag, char *value, int maxlength, int *valid)
{
   func = (char*)"rcfConfigParser::getUniqueTag";
   int ntokens;
   char *token = NULL;
   rcfLineParser *parser;
   int length;
   int rtn;

   if(NULL == parse_token)
   {
      reportError("Parser Token has not been initialized ");
      return(CF_FAIL);
   }
   if(NULL == tag)
   {
      reportError("Null pointer passed as tag");
      return(CF_FAIL);
   }
   if(0 == strlen(tag))
   {
      reportError("Empty string passed as tag");
      return(CF_FAIL);
   }

   *valid = 0;
   for(int i=0; i<nlines; i++)
   {
      parser = new rcfLineParser(10,parse_token);
      rtn = parser->parseLine(lines[i]);

      if(CF_OK != rtn)
      {
         fprintf(stderr,"%s failure to parse the line: %s\n",func,lines[i]);
         return(rtn);
      }
      /* get number of tokens seen, must be at least 2 for this line to matter */
      ntokens=0;

      rtn = parser->getNumberOfTokens(&ntokens);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"%s failure to get number of tokens for line: %s\n",func,lines[i]);
         return(rtn);
      }
      if(ntokens < 2 )
      {
         fprintf(stderr,"%s WARNING: a line with fewer than 2 tokens, skipping %s\n",func,lines[i]);
         continue;  // move to the next line
      }
      for(int itok=0; itok<ntokens; itok++)
      {
         rtn = parser->getToken(itok,&token);
         if(CF_OK != rtn)
         {
            fprintf(stderr,"%s failure to get a token number %d for line: %s\n",func,i,lines[i]);
            return(rtn);
         }
         if(0 == strcmp(tag,token))
         {
            // hooray, we found it!!! return the value and get out
            rtn = parser->getToken(itok+1,&token);
            if(CF_OK != rtn)
            {
               fprintf(stderr,"%s failure to get a value token number %d for line: %s\n",func,i+1,lines[i]);
               return(rtn);
            }
            else
            {
               length = strlen(token);
               if(length < maxlength )
               {
                  strcpy(value,token);
                  *valid = 1;
                  delete parser;
                  parser = NULL;
                  return(CF_OK);
               }
               else
               {
                  fprintf(stderr,"%s ERROR: the token %s was found with value %s, but user provided inadequate output buffer length. Provided: %d  required: %d \n",func,tag,token,maxlength,length);
                  return(CF_FAIL);
               }
            }
          }
      } /* end loop over tokens */

      delete parser;
      parser = NULL;
   } /* end loop over lines */

   return(CF_OK);

}

/**
    These Interface functions will always find the first
    occurrence of tag-value pair in the parser. These functions
    use getRepeatTag in their implementation (see below).
 */
int rcfConfigParser::getTag(char *tag, char *value, int maxlength, int *valid)
{
   rewind();
   return(getRepeatTag(tag,value,maxlength,valid));
}
string rcfConfigParser::getTag( string tag )
{
   rewind();
   return(getRepeatTag(tag));
}
int rcfConfigParser::getTag(char *tag, int *value,int *valid)
{
   rewind();
   return(getRepeatTag(tag,value,valid));
}
int rcfConfigParser::getTag(char *tag, float *value, int *valid)
{
   rewind();
   return(getRepeatTag(tag,value,valid));
}

/**
    Public Interface function getRepeatTag searches for specified tag
    and extracts the string (value) associated with the tag.
    The function is equipped to handle multiple occurences of the
    same tag, as long as they occur in different lines. It is also
    equipped to handle updates of relative paths to absolute paths
    using the rcfAdmin object.
 */
int rcfConfigParser::getRepeatTag(char *tag, char *value, int maxlength, int *valid)
{
  //< tag       : Input  : The "Tag" that you are looking for in the file. e.g. FORMAT
  //< value     : Output : The string that is after the TAG in the file (once found)
  //< maxlength : Input  : Length of buffer to search
  //< valid     : Output : Status of the result.  1 = OK , 0 = FAIL

   func = (char*)"rcfConfigParser::getTag";
   int ntokens;
   char *token = NULL;
   rcfLineParser *parser;
   int length;
   int rtn;
   int min_line;
   int update_index = -1;
   char lkey[RCF_MAXKEY];
   int lvalid = 0;

   if(NULL == parse_token)
   {
      reportError("Parser Token has not been initialized ");
      return(CF_FAIL);
   }
   if(NULL == tag)
   {
      reportError("Null pointer passed as tag");
      return(CF_FAIL);
   }
   if(0 == strlen(tag))
   {
      reportError("Empty string passed as tag");
      return(CF_FAIL);
   }

   // check if this tag potentially needs updating
   if( NULL != admin)
   {
      if(admin->isReady())
      {
         if(ntables > 0)
         {
            for(int i=0; i<ntables; i++)
            {
               if( 0 == strcmp(tag,tables[i].key))
               {
                  // check if the relative key has been set
                  if( strlen(tables[i].rel_key) > 0 )
                  {
                     // it has, extract the token if available
                     rtn = getUniqueTag(tables[i].rel_key,lkey,RCF_MAXKEY-1,&lvalid);
                     if( (CF_OK != rtn) || (0 == lvalid) )
                     {
                        // issue warning, but proceed. Link between tags is registered,
                        // but the secondary tag is absent.
		        fprintf(stderr,"WARNING: %s :looking for tag %s\n", func,tag); //
                        reportWarning("Link between tags is registered, but the secondary tag is absent");
                        fprintf(stderr,"Missing Tag: %s\n",tables[i].rel_key);
                        update_index = -1;
                     }
                     else update_index = i;
                  }
               }
            }
         }
      }
   }

   if(0 == strcmp(last_tag,tag) )
      min_line = last_found +1;      
   else
   {
      // not a repeat request, reset tag tracking
      min_line = 0;
      last_found = 0;
      memset(last_tag,0,sizeof(last_tag));
   }

   *valid = 0;
   for(int i=min_line; i<nlines; i++)
   {
      parser = new rcfLineParser(10,parse_token);
      if(NULL == parser)
      {
         fprintf(stderr,"ERROR: failure to allocate memory for line parser \n"); 
         return(CF_FAIL);
      }
      if(keepSpacesFlag)parser->keepSpaces();

      rtn = parser->parseLine(lines[i]);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
         fprintf(stderr,"%s failure to parse the line: %s\n",func,lines[i]);
         return(rtn);
      }
      /* get number of tokens seen, must be at least 2 for this line to matter */
      ntokens=0;

      rtn = parser->getNumberOfTokens(&ntokens);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
         fprintf(stderr,"%s failure to get number of tokens for line: %s\n",func,lines[i]);
         return(rtn);
      }
      if(ntokens < 2 )
      {
         fprintf(stderr,"WARNING: %s :looking for tag %s\n", func,tag); //
         fprintf(stderr,"%s WARNING: a line with fewer than 2 tokens, skipping %s\n",func,lines[i]);
         continue;  // move to the next line
      }
      for(int itok=0; itok<ntokens; itok++)
      {
         rtn = parser->getToken(itok,&token);         
         if(CF_OK != rtn)
         {
            fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
            fprintf(stderr,"%s failure to get a token number %d for line: %s\n",func,i,lines[i]);
            return(rtn);
         }
         if(0 == strcmp(tag,token))
         {
            // hooray, we found it!!! return the value and get out
            rtn = parser->getToken(itok+1,&token);
            if(CF_OK != rtn)
            {
               fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
               fprintf(stderr,"%s failure to get a value token number %d for line: %s\n",func,i+1,lines[i]);
               return(rtn);
            }
            else
            {
               length = strlen(token);
               if(length < maxlength )
               {
                  if(update_index >= 0)
                  {
                     // use rcfAdmin to append this string
                     char *tptr = NULL;
                     tptr = admin->AppendPath(lkey,token,&lvalid);
                     if( (NULL == tptr) || (0 == lvalid) )
                     {
                       //report warning, but move on
                       fprintf(stderr,"WARNING: %s :looking for tag %s\n", func,tag); //
                       reportWarning("Attempt to update string with key failed for unknown reasons");
                       fprintf(stderr,"key searched for in the rcfAdmin object: %s\n",lkey);
                     }
                     else
                     {
                        length = strlen(tptr);
                        if(length < maxlength)
                        {
                           strcpy(value,tptr);
                           strcpy(last_tag,tag);
                           last_found = i;
                           delete parser;
                           parser = NULL;
                           delete [] tptr;
                           tptr = NULL;
                           *valid = 1;
                           return(CF_OK);
                        }
                        else
                        {
                           //report error
                           fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
                           reportError("Attempt to update string produces a combined string that is too long. Can not proceed");
                           fprintf(stderr,"appended string: %s\n",tptr);
                           return(CF_FAIL);
                        }
                     }
                  }
                  else
                  {
                     strcpy(value,token);
                     strcpy(last_tag,tag);
                     last_found = i;
                     delete parser;
                     parser = NULL;
                     *valid = 1;
                     return(CF_OK);
                  }
               }
               else
               {
		 fprintf(stderr,"ERROR: %s :looking for tag %s\n", func,tag); //
                  fprintf(stderr,"%s ERROR: the token %s was found with value %s, but user provided inadequate output buffer length. Provided: %d  required: %d \n",func,tag,token,maxlength,length);
                  return(CF_FAIL);
               }
            }
          }
      } /* end loop over tokens */

      delete parser;
      parser = NULL;
   } /* end loop over lines */
   return(CF_OK);
}

string rcfConfigParser::getRepeatTag( string tag )
{
    char value[1025] = {'\0'};
    int maxlength = 1024;
    int valid = 0;

    if( CF_FAIL == getRepeatTag(const_cast<char*>(tag.c_str()), value, maxlength, &valid) )
    {
        cerr << "ERROR:rcfConfigParser::getRepeatTag: Returned CF_FAIL from getRepeatTag" << endl;
        return "";
    }
    else
    {
        if( valid == INVALID )
        {
            // simply means that tag was not found
            return "";
        }
    }

    string valueString(value);
    return valueString;
}

int rcfConfigParser::getRepeatTag(char *tag, int *value,int *valid)
{
   func = (char*)"rcfConfigParser::getTag_int";
   char buffer[1025];
   int maxlength = 1024;
   int rtn;
   rtn = getRepeatTag(tag,buffer,maxlength,valid);
   if(CF_OK != rtn)
   {
      fprintf(stderr,"%s ERROR: failure attempting to get tag with return %d \n",func,rtn);
      return(CF_FAIL);
   }
   *value = atoi(buffer);
   return(CF_OK);
}

int rcfConfigParser::getRepeatTag(char *tag, float *value, int *valid)
{
   func = (char*)"rcfConfigParser::getTag_float";
   char buffer[1025];
   int maxlength = 1024;
   int rtn;
   rtn = getRepeatTag(tag,buffer,maxlength,valid);
   if(CF_OK != rtn)
   {
      fprintf(stderr,"%s ERROR: failure attempting to get tag with return %d \n",func,rtn);
      return(CF_FAIL);
   }
   *value = atof(buffer);
   return(CF_OK);
}

int rcfConfigParser::isReady()
{
   return(is_ready);
}

void rcfConfigParser::reportError(const char *str)
{
   fprintf(stderr,"ERROR:  %s reports that %s\n",func,str);
}
void rcfConfigParser::reportWarning(const char *str)
{
   fprintf(stderr,"WARNING:  %s reports that %s\n",func,str);
}

int rcfConfigParser::setUserLine(char *tag, char *value)
{
   func = (char*)"rcfConfigParser::setUserTag";
   int tlength;
   int vlength;

   is_ready = 0;

   if(NULL == tag)
   {
      reportError("Null string passed as user tag ");
      return(CF_FAIL);
   }

   if(0 == (tlength = strlen(tag)) )
   {
      reportError("Empty string passed as user value");
      return(CF_FAIL);
   }
   if(NULL == value)
   {
      reportError("Null string passed as user value ");
      return(CF_FAIL);
   }

   if(0 == (vlength = strlen(value)) )
   {
      reportError("Empty string passed as user value");
      return(CF_FAIL);
   }
   // allocate memory for both values, + 2* length of parse token
   // + terminating null
   int length = tlength + vlength + (2*strlen(parse_token)) +1;
   lines[nlines] = NULL;
   lines[nlines] = new char[length];
   if(NULL == lines[nlines])
   {
      reportError("Failure to allocate memory for a new line");
      return(CF_FAIL);
   }
   // all is well, make the line
   strcpy(lines[nlines],tag);
   strcat(lines[nlines],parse_token);
   strcat(lines[nlines],value);
   strcat(lines[nlines],parse_token);
   nlines++;

   is_ready = 1;

   return(CF_OK);
}

// append specified line, to build a composite line
int rcfConfigParser::appendUserLine(int index, char *tag, char *value)
{
   func = (char*)"rcfConfigParser::appendUserTag_int";
   int tlength;
   int vlength;

   is_ready = 0;

   if(index >= nlines)
   {
      reportError("Illegal index value, greater than the number of lines ");
      return(CF_FAIL);
   }
   if(index < 0)
   {
      reportError("Illegal (negative) index value");
      return(CF_FAIL);
   }

   if(NULL == tag)
   {
      reportError("Null string passed as user tag ");
      return(CF_FAIL);
   }

   if(0 == (tlength = strlen(tag)) )
   {
      reportError("Empty string passed as user value");
      return(CF_FAIL);
   }
   if(NULL == value)
   {
      reportError("Null string passed as user value ");
      return(CF_FAIL);
   }

   if(0 == (vlength = strlen(value)) )
   {
      reportError("Empty string passed as user value");
      return(CF_FAIL);
   }
   if(NULL == lines[index])
   {
      reportError("OBJECT ERROR: NULL pointer in place where line was expected");
      return(CF_FAIL);
   }
   // append the line.

   // length of current line
   int curlength = strlen(lines[index]);
   // compute length of part to be appended
   // allocate memory for both values, + 2* length of parse token
   // + terminating null
   int length = tlength + vlength + (2*strlen(parse_token)) +1;
   int full_length = curlength + length;
   char *tbuffer = NULL;
   tbuffer = new char[full_length];
   if(NULL == tbuffer)
   {
      reportError("Failure to allocate memory for a new line");
      return(CF_FAIL);
   }
   strcpy(tbuffer,lines[index]);
   delete[] lines[index];
   lines[index] = tbuffer;
   // all is well, append the line
   strcat(lines[index],tag);
   strcat(lines[index],parse_token);
   strcat(lines[index],value);
   strcat(lines[index],parse_token);

   is_ready = 1;

   return(CF_OK);
}
// append the last line that has been created
int rcfConfigParser::appendUserLine(char *tag, char *value)
{
   return(appendUserLine(nlines-1,tag,value));
}



int rcfConfigParser::writeConfigFile(char *path, char* name)
{
   func = (char*)"rcfConfigParser::writeConfigFile";
   int verify;
   int length;
   int rtn;
   char *upd_path = NULL;

   if(NULL == path)
   {
      reportError("NULL pointer passed in lieu of path to output file");
      return(CF_FAIL);
   }
   if(0 == strlen(path))
   {
      reportError("empty string passed in lieu of path to output file");
      return(CF_FAIL);
   }
   // verify that the object is ready
   if(0 == is_ready)
   {
      reportError("attempt to output config file, but object is not ready.");
      return(CF_FAIL);
   }
   if(nlines <= 0)
   {
      reportError("attempt to output config file, but there are no lines");
      return(CF_FAIL);
   }
   // final sanity check
   int i;

   for(i=0; i<nlines; i++)
   {
      if(NULL == lines[i])
      {
         fprintf(stderr,"ERROR: rcfConfigParser::writeConfigFile null pointer where line is expected detected at index %d of %d\n",i,nlines-1);
         return(CF_FAIL);
      }
      if(0 == strlen(lines[i]) )
      {
         fprintf(stderr,"ERROR: rcfConfigParser::writeConfigFile empty string where line is expected detected at index %d of %d\n",i,nlines-1);
         return(CF_FAIL);
      }
   }

   // all is well, open the file
   // if rcfAdmin is defined, update the path

   //FILE *ofid = NULL;
   std::ofstream outfile;

   if(NULL != admin)
   {
      upd_path = admin->AppendPathToConfig(path,&verify);
      if((NULL == upd_path) || (0 == verify))
      {
         fprintf(stderr,"ERROR: rcfConfigParser::writeConfigFile failure to update path to config file\n");
         return(CF_FAIL);
      }
      outfile.open(upd_path);
   }
   else
   {
      // append default configname
      if( NULL == name )
      {
        length = strlen(path) + strlen(RCF_ADMIN_DEF_CONFIGNAME);
      }
      else
      {
          length = strlen(path) + strlen(name);
      }
      upd_path = new char[length+2];
      if(NULL == upd_path)
      {
         fprintf(stderr,"ERROR: rcfConfigParser::writeConfigFile failure to allocate memory for updated path. Size of request: %d\n",length+1);
         return(CF_FAIL);
      }
      strcpy(upd_path,path);
      strcat(upd_path,"/");

      if( NULL == name )
      {
          strcat(upd_path,RCF_ADMIN_DEF_CONFIGNAME);
      }
      else
      {
          strcat(upd_path,name);
      }
      outfile.open(upd_path);
   }


   if(!(outfile.is_open()))
   {
         fprintf(stderr,"ERROR: rcfConfigParser::writeConfigFile Failure to open output file %s\n",upd_path);
         delete[] upd_path;
         upd_path = NULL;
         return(CF_FAIL);
   }

   // get rid of temporary buffer
   delete[] upd_path;
   upd_path = NULL;

   // spit
   if(write_as_clif) rtn = writeClifConfigFile(outfile);
   else rtn = writeDefaultConfigFile(outfile);
   
   if(CF_OK != rtn)
   {
      reportError("Failure to write out the config file");
      return(CF_FAIL);
   }

   outfile.close();

   return(CF_OK);
}

int rcfConfigParser::writeClifConfigFile(std::ofstream & outfile)
{
   func = (char*)"rcfConfigParser::writeClifConfigFile";
   if(!(outfile.is_open()))
   {
      reportError("Output Stream Not Ready");
      return(CF_FAIL);
   }
   if( (1 != is_ready) || (nlines <= 0 ) )
   {
      reportError("Request to output config parser which is not ready or empty");
      return(CF_FAIL);
   }
   std::map<std::string,std::string> lmap;
   lmap = getKeyValueMap();

   int size = lmap.size();
   if( size <= 0 )
   {
      reportError("Failure to obtain key-value map");
      return(CF_FAIL);
   } 
   std::string separator = " = \"";
   std::string terminator = "\";";
   std::map<std::string,std::string>::iterator it;
   for( it = lmap.begin(); it != lmap.end(); it++)
   {
      outfile<<(*it).first<<separator<<(*it).second<<terminator<<endl;
   }
   return(CF_OK);
}

int rcfConfigParser::writeDefaultConfigFile(std::ofstream & outfile)
{
   func = (char*)"rcfConfigParser::writeDefaultConfigFile";
   if(!(outfile.is_open()))
   {
      reportError("Output Stream Not Ready");
      return(CF_FAIL);
   }
   if( (1 != is_ready) || (nlines <= 0 ) )
   {
      reportError("Request to output config parser which is not ready or empty");
      return(CF_FAIL);
   }
   for(int i=0; i<nlines; i++)
   {
      outfile<<lines[i]<<"\n";
   }
   return(CF_OK);
}

int rcfConfigParser::setNumberOfTriggerKeys(int nkeys)
{
   func = (char*)"rcfConfigParser::setNumberOfTriggerKeys";
   if(nkeys <= 0)
   {
      reportError("Illegal request to create reference key array.  The number of elements must be greater than zero");
      return(CF_FAIL);
   }
   
   if( NULL != tables )
   {
       delete[] tables;
   }
   tables = new cfRelPathTable[nkeys];
   if(NULL == tables)
   {
      reportError("failure to allocate memory for the key tables");
      fprintf(stderr,"size of request: %d\n",(int)(nkeys*sizeof(cfRelPathTable)));
      return(CF_FAIL);
   }
   // make sure everything is initialized
   memset(tables,0,nkeys*sizeof(cfRelPathTable));

   ntables = nkeys;

   return(CF_OK);
}

int rcfConfigParser::setTriggerKey(int nkey, char *key)
{
   func = (char*)"rcfConfigParser::setTriggerKey";
   // sanity checks
   if( nkey >= ntables)
   {
      reportError("Illegal index in a request to set primary key");
      fprintf(stderr,"index provided: %d  maximum index: %d\n",nkey,ntables-1);
      return(CF_FAIL);
   }
   if(NULL == key)
   {
      reportError("A request to set key, but the pointer to key is NULL");
      return(CF_FAIL);
   }
   if(0 == strlen(key) )
   {
      reportError("A request to set key, but length of the string is zero");
      return(CF_FAIL);
   }
   if( strlen(key) > RCF_MAXKEY-1 )
   {
      reportError("A request to set key, but length of the string is larger than allowed");
      fprintf(stderr,"The length: %d  Maximum allowed: %d  The string: %s\n",(int)strlen(key),RCF_MAXKEY-1,key);
      return(CF_FAIL);
   }
   strcpy(tables[nkey].key,key);
   return(CF_OK);
}

int rcfConfigParser::addSubordinateKey(int nkey, char *subkey)
{
   func = (char*)"rcfConfigParser::addSubordinateKey_int_char";
   int length;

   if( nkey >= ntables)
   {
      reportError("Illegal index in a request to set subordinate key");
      fprintf(stderr,"index provided: %d  maximum index: %d\n",nkey,ntables-1);
      return(CF_FAIL);
   }
   if(NULL == subkey)
   {
      reportError("A request to set key, but the pointer to key is NULL");
      return(CF_FAIL);
   }
   if(0 == (length = strlen(subkey)) )
   {
      reportError("A request to set key, but length of the string is zero");
      return(CF_FAIL);
   }

   // get the pointer
   cfRelPathTable *ptr = tables + nkey;
   if(0 != strlen(ptr->rel_key) )
   {
      reportError("A request to set relative key, but the key has been set already");
      return(CF_FAIL);
   }
   else
   {
      strcpy(ptr->rel_key,subkey);
   }

   return(CF_OK);
}

int rcfConfigParser::addSubordinateKey(char *key, char *subkey)
{
   func = (char*)"rcfConfigParser::addSubordinateKey_char_char";
   int length;

   if(NULL == key)
   {
      reportError("A request to locate key, but the pointer to key is NULL");
      return(CF_FAIL);
   }
   if(0 == (length = strlen(key)) )
   {
      reportError("A request to locate key, but length of the string is zero");
      return(CF_FAIL);
   }
   if(ntables <= 0)
   {
      reportError("A request to locate key, but no keys were defined");
      return(CF_FAIL);
   }
   for(int i=0; i<ntables; i++)
   {
      if( 0 == strcmp(key,tables[i].key))return(addSubordinateKey(i,subkey));
   }

   // it is a failure if we get here
   reportError("Requested key was not found");
   return(CF_FAIL);
}

int rcfConfigParser::armParser()
{
   func = (char*)"rcfConfigParser::armParser";
   int rtn;

   if(NULL == admin)return(CF_OK); // nothing to do
   if(!(admin->isReady()))
   {
      reportError("Admin object is attached but not ready");
      return(CF_FAIL);
   }
   // all is well, proceed to arm

   rtn = this->setNumberOfTriggerKeys(1);
   if(CF_OK != rtn)
   {
      reportError("Failure to set number of trigger keys");
      return(CF_FAIL);
   } 
   rtn = this->setTriggerKey(0,(char*)"PATH");
   if(CF_OK != rtn)
   {
      reportError("Failure to set trigger key PATH");
      return(CF_FAIL);
   }
   rtn = this->addSubordinateKey(0,(char*)"RCFROOT");
   if(CF_OK != rtn)
   {
      reportError("Failure to set subordinate key RCFROOT");
      return(CF_FAIL);
   }
   // all is really well, standard arming is done
   return(CF_OK);
}

void rcfConfigParser::rewind()
{
   last_found = 0;
   memset(last_tag,0,sizeof(last_tag));
}

rcfConfigParser& rcfConfigParser::operator=(const rcfConfigParser& parser)
{
   func = (char*)"rcfConfigParser::operator=";
   int length;
   char *ptr;

   if(parser.nlines > 0)
   {
      for(int i=0; i<parser.nlines; i++)
      {
         ptr = parser.lines[i];
         if(NULL == ptr)
         {
            lines[i] = NULL;
         }
         else
         {
            length = strlen(ptr);
            lines[i] = new char[length+1];
            if(NULL == lines[i])
            {
               reportError("Failure to allocate memory for a line");
               fprintf(stderr,"Length of request: %d\n",length);
               return(*this);
            }
            strcpy(lines[i],ptr);
         }
      }
      nlines = parser.nlines;
   }
   if(parser.ntables > 0)
   {
      tables = new cfRelPathTable[parser.ntables];
      if(NULL == tables)
      {
         reportError("Failure to allocate memory for tables");
         return(*this);
      }
      for(int i=0; i<parser.ntables; i++)
      {
         strcpy(tables[i].key,parser.tables[i].key);
         strcpy(tables[i].rel_key,parser.tables[i].rel_key);
      }
      ntables = parser.ntables;
   }
   fid = NULL; // do not carry open file in the copy, even if it is still open
   is_ready = parser.is_ready;
   databaseConfigPath = parser.databaseConfigPath;
   resolvedConfigPath = parser.resolvedConfigPath;
   return(*this);
}

rcfConfigParser* rcfConfigParser::getParser(std::string tag, bool *exception)
{
    rewind();
   func = (char*)"rcfConfigParser::getParser";
   *exception = false;
   
   // first search through existing parsers
   if(parsers.size() > 0)
   {
      for(unsigned int i=0; i<parsers.size(); i++)
      {          
         if(0 == parser_labels[i]->compare(tag))
         {       
            // found it. return the pointer
            if(NULL != parsers[i])
            {
               return(parsers[i]);
            }
            else
            {
               // this is an error, report it here
               reportError("Requested tag was found in a parser queue, but the parser has not been found");
               *exception = true;
               return(NULL);
            }
         }
      }
   }
   // if we are here, we need to search stored strings
   char *lbuffer = new char[20001];
   if(NULL == lbuffer)
   {
        reportError("Failure to allocate memory for temporary buffer");
        fprintf(stderr,"size of request: 20001 bytes \n");
        *exception = true;
        return(NULL);
   }

   int rtn,valid;
   rtn = getTag((char*)(tag.c_str()),lbuffer,20000,&valid);
   if(CF_OK != rtn)
   {
        reportError("Failure searching for tag");
        fprintf(stderr,"tag: %s \n",tag.c_str());
        *exception = true;
        if( NULL != lbuffer )
        {
            delete[] lbuffer;
            lbuffer = NULL;
        }
        return(NULL);
   }
   if(0 == valid)
   {
        if( NULL != lbuffer )
        {
            delete[] lbuffer;
            lbuffer = NULL;
        }
        // this is ok, fail silently
        return(NULL);
   }
   // we found the tag, attempt to retrieve the file
   rcfConfigParser *lparser = new rcfConfigParser;
   if(NULL == lparser)
   {
        reportError("Failure to create the config parser");
        *exception = true;
        if( NULL != lbuffer )
        {
            delete[] lbuffer;
            lbuffer = NULL;
        }
        return(NULL);
   }
   if(NULL != admin)
   {
      rtn = lparser->setRcfAdmin(admin);
      if(CF_OK != rtn)
      {
        reportError("Failure to link to the existing rcfAdmin pointer");
        delete lparser;
        *exception = true;
        if( NULL != lbuffer )
        {
            delete[] lbuffer;
            lbuffer = NULL;
        }
        return(NULL);
      }
   }
   // attempt to fill the parser
   rtn = lparser->loadConfigFile(lbuffer);
   if(CF_OK != rtn)
   {
        reportError("Failure to load config file specified by the value buffer");
        delete lparser;
        if( NULL != lbuffer )
        {
            delete[] lbuffer;
            lbuffer = NULL;
        }
        *exception = true;
        return(NULL);
   }
   
   if( NULL != lbuffer )
   {
       delete[] lbuffer; // done with this one
       lbuffer = NULL;
   }

   if(!(lparser->isReady()) )
   {
      reportError("Created parser is not ready");
      delete lparser;
      *exception = true;
      return(NULL);
   }

   // transfer tables to the new parser, if tables exist
   if( (ntables > 0) && (NULL != tables) )
   {
      rtn = lparser->transferTables(this);
      if(CF_OK != rtn)
      {
         reportError("Failure to transfer tables to the subordinate parser");
         *exception = true;
         return(NULL);
      }
   }

   // all is well. Attach this parser to parser queue for future retrieval
   std::string *local_label = new std::string(tag.c_str());
   parser_labels.push_back(local_label);
   parsers.push_back(lparser);
   my_parsers.push_back(true);

   // return parser to user

   return(lparser);
}

int rcfConfigParser::addReferenceParserSelect(std::string tag, rcfConfigParser *parser,bool manage)
{
   func = (char*)"rcfConfigParser::addReferenceParserSelect";
   int rtn;

   // sanity checks
   if(NULL == parser)
   {
      reportError("NULL pointer passed in as data");
      return(CF_FAIL);
   }
   if(!(parser->isReady()))
   {
      reportError("Invalid parser passed in as data");
      return(CF_FAIL);
   }
   if( tag.length() <= 0 )
   {
      // there must be a tag, otherwise this parser is not recoverable
      reportError("A unique tag must be provided, otherwise parser is not recoverable");
      return(CF_FAIL);
   }

   // arm this parser
   if(NULL != admin)
   {
      rtn = parser->setRcfAdmin(admin);
      if(CF_OK != rtn)
      {
         reportError("Failure to link to the existing rcfAdmin pointer");
         return(CF_FAIL);
      }
   }
   // transfer tables to the new parser, if tables exist
   if( (ntables > 0) && (NULL != tables) )
   {
      rtn = parser->transferTables(this);
      if(CF_OK != rtn)
      {
         reportError("Failure to transfer tables to the subordinate parser");
         return CF_FAIL;
      }
   }


   std::string *local_label = new std::string(tag.c_str());
   parser_labels.push_back(local_label);
   parsers.push_back(parser);
   my_parsers.push_back(manage);
   return(CF_OK);
}

int rcfConfigParser::addReferenceParser(std::string tag, rcfConfigParser *parser)
{
   return(addReferenceParserSelect(tag,parser,false));
}
int rcfConfigParser::addManagedReferenceParser(std::string tag, rcfConfigParser *parser)
{
   return(addReferenceParserSelect(tag,parser,true));
}

int rcfConfigParser::transferTables(rcfConfigParser *parser)
{
   func = (char*)"rcfConfigParser::transferTables";
   // sanity checks
   if(NULL == parser)
   {
      reportError("NULL pointer passed in as data");
      return(CF_FAIL);
   }
   if( (ntables > 0) || (NULL != tables) )
   {
      // we declare it an error, this is a request to overwrite existing tables
      reportError("Can not overwrite existing routing tables");
      return(CF_FAIL);
   }
   if(parser->ntables <= 0)
   {
      // nothing to do, quit silently
      return(CF_OK);
   }
   if(NULL == parser->tables)
   {
      // this is an error in source parser
      reportError("Inconsistent state detected in the source parser: routing tables are indicated, but the pointer to tables is NULL");
      return(CF_FAIL);
   }
   // all is well, create tables
   tables = new cfRelPathTable[parser->ntables];
   if(NULL == tables)
   {
      reportError("failure to create routing tables array");
      fprintf(stderr,"number of elements requested: %d\n",parser->ntables);
      return(CF_FAIL);
   }

   // we can use memcpy here
   memcpy((void*)tables,(void*)(parser->tables),sizeof(cfRelPathTable)*(parser->ntables));
   ntables = parser->ntables;

   return(CF_OK);
}
/* replace a key/value pair.
 * If the specified key has existed, change its value to the new value.
 * Otherwise, use setUserline.
 * \param tag the key
 * \param value the value
 */
int rcfConfigParser::replace(std::string tag, std::string value)
{
	for(int i=0; i<nlines; i++)
	{
		std::string str(lines[i]);
		if(str.find(tag) != str.npos)
		{
			delete[] lines[i];

			// allocate memory for both values, + 2* length of parse token
			// + terminating null
			int tlength = tag.size();
			int vlength = value.size();
			int length = tlength + vlength + (2*strlen(parse_token)) +1;
			lines[i] = NULL;
			lines[i] = new char[length];
			if(NULL == lines[i])
			{
				reportError("Failure to allocate memory for a new line");
				return(CF_FAIL);
			}
			// all is well, make the line
			strcpy(lines[i],tag.c_str());
			strcat(lines[i],parse_token);
			strcat(lines[i],value.c_str());
			strcat(lines[i],parse_token);
			return CF_OK;
		}
	}
	/* if key does not exist, create it */
	return setUserLine((char*)tag.c_str(), (char*)value.c_str());
}

std::map<std::string, std::string> rcfConfigParser::getKeyValueMap()
{
	std::map<std::string, std::string> kvMap;


	if(NULL == parse_token)
	{
		reportError("Parser Token has not been initialized ");
		return kvMap;
	}

	rcfLineParser *parser = new rcfLineParser(10,parse_token);
	for(int i=0; i<nlines; i++)
	{
		int rtn;

		/* process a line */
		rtn = parser->parseLine(lines[i]);
        if( CF_OK != rtn )
        {
            reportError("Could not parse line");
            return kvMap;
        }

		int ntokens = 0;
		rtn = parser->getNumberOfTokens(&ntokens);
        if( CF_OK != rtn )
        {
            reportError("Could not get the number of tokens");
            return kvMap;
        }
		if(ntokens < 2 ) continue;

		for(int itok=ntokens%2; itok<ntokens; itok+=2)
		{
			char *token = NULL;
			rtn = parser->getToken(itok,&token);
            if( CF_OK != rtn )
            {
                reportError("Could not get token");
                return kvMap;
            }
			std::string key(token);

			rtn = parser->getToken(itok+1,&token);
            if( CF_OK != rtn )
            {
                reportError("Could not get token");
                return kvMap;
            }
			std::string value(token);

			/* add a key/value to map */
			kvMap.insert(std::pair<std::string, std::string>(key,value));
		}
	}
	delete parser;
	parser = NULL;

	/* return the map */
	return kvMap;
}

std::string rcfConfigParser::getDatabasePath(bool *verify)
{
   *verify = false;
   if(databaseConfigPath.size() > 0 )
   {
      *verify = true;
      return(databaseConfigPath);
   }
   else return(std::string(""));
}
std::string rcfConfigParser::getResolvedPath(bool *verify)
{
   *verify = false;
   if(resolvedConfigPath.size() > 0 )
   {
      *verify = true;
      return(resolvedConfigPath);
   }
   else return(std::string(""));
}
void rcfConfigParser::keepSpaces()
{
   keepSpacesFlag = true;
}
void rcfConfigParser::removeSpaces()
{
   keepSpacesFlag = false;
}
bool rcfConfigParser::noCaseCompare(std::string s1, std::string s2)
{
   int l1 = s1.size();
   int l2 = s2.size();
   if( (0 == l1) && (0 == l2) )return(true);
   if(l1 != l2)return(false);
   char *buf1 = new char[l1+1];
   char *buf2 = new char[l2+1];
   if( (NULL == buf1) || (NULL == buf2) )
   {
      fprintf(stderr,"ERROR: rcfConfigParser::noCaseCompare failure to allocate memory for temporary buffers, size of request: %d \n",2*(l1+1));
      return(false);
   }
   memset(buf1,0,l1+1);
   memset(buf2,0,l2+1);
   strcpy(buf1,s1.c_str());
   strcpy(buf2,s2.c_str());
   for(int i=0; i<l1; i++)
   {
      buf1[i] = tolower(buf1[i]);
      buf2[i] = tolower(buf2[i]);
   }
   s1 = std::string(buf1);
   s2 = std::string(buf2);
   delete [] buf1;
   delete [] buf2;
   if(0 == s1.compare(s2))return(true);
   else return(false);
}
