
// Author: M.Fatyga
// Date Created: 03/14/2006
// Object rcfDirectoryMgr examines and 
// creates or deletes the directory tree
// This is a low level RCF utility

#ifndef __rcfDirectoryMgr
#define __rcfDirectoryMgr

#define MAXTOKENS 100

#include <string>
#include <vector>
#include <stdio.h>

/**
    \class rcfDirectoryMgr
    @ingroup UTIL
*/

class rcfDirectoryMgr
{

   protected:

      int nlevels;
      char* tokens[MAXTOKENS];
      char* root;
      char *func;
      int object_failed;
      char *usr_buffer;
      int leading_separator;
      char separator[2];
      std::vector<std::string*> dirnames;
      std::vector<std::string*> filnames;
      std::vector<std::string*> wrkdirs;
      std::vector<std::string*> wrkfils;
      FILE *logid;
      int create_depth;

   public:

      // this constructor sets a root directory
      // for the object. The root directory 
      // is automatically prepended to the directory
      // line handled by this object. 
      rcfDirectoryMgr(char *root);
      // default constructor
      rcfDirectoryMgr();
      ~rcfDirectoryMgr();

   public:

      // public interface functions 

      // set root on the object that has been created via
      // default constructor. This is a prefered method,
      // as user can check the error return. The root directory
      // MUST EXIST. Error will be returned if it does not, and
      // the object will set an internal error state.

      int setRoot(char *root);

      // this function will create directory line out of
      // Unix path. Users are cautioned that in rear cases 
      // parsing problems could occur.
      int loadLine(char *line);
      //load this line, but clip nclip number of tokens at the end of the line
      int loadLine(char *line,int nclip);

      // add a token to the directory tree which is managed
      // by the object. Every time the function is called, new
      // directory level is added. This function prepares an internal
      // directory structure, but the directory is not made until
      // createLine is called 

      int appendLine(char *token);

      // get number of levels stored, and get
      // back a token corresponding to a given level. 
      int getNumberOfLevels(int *nlevels);
      char *getLevel(int level, int *verify);


      // check if the directory line stored in the 
      // object already exists. Integer level allows
      // the user to check the line up to any level
      // that is legal (stored).
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 
      int testLine(int *verify);
      int testLine(int level, int *verify);

      // check if a file exists in the specified (loaded) 
      // directory line. Integer level allows
      // the user to check the line up to any level
      // that is legal (stored).
      // testFile specifically checks that the type of
      // object is a file, while testName will verify
      // just an object of a given name
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 
      int testFile(char *file,int *verify);
      int testFile(char *file,int level, int *verify);
      int testName(char *file,int *verify);
      int testName(char *file,int level, int *verify);
      int deleteFile(char *file,int *verify);
      int deleteFile(char *file,int level, int *verify);
      int deleteDirectory(char *file,int *verify);
      int deleteDirectory(char *file,int level, int *verify);
      int clearLine();
      int clearFilesOnLine();
      int clearDirectoriesOnLine();

      // actually create the directory tree (starting
      // from root, if defined). Integer level allows
      // users to create a partial tree (up to level)
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 

      int createLine(int *verify);
      int createLine(int level,int *verify);

      // return a buffer with a full path to the managed
      // directory. Note: every time this function is called,
      // the existing buffer gets deallocated. User is responsible
      // for copying out data to their own buffer if they want
      // it preserved. Object manages buffer deallocation.
      // fname will be appended to the end of the returned buffer.
      // Function accepts NULL in lieu of fname.
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 

      char* makePathBuffer(int *verify, char *fname);
      char* makePathBuffer(int level, int *verify, char *fname);
      std::string makePathBuffer(int *verify, std::string fname);
      std::string makePathBuffer(int level, int *verify, std::string fname);

      //exclude root from the path, otherwise the same as above
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 
      char* makeRelPathBuffer(int *verify, char *fname);
      char* makeRelPathBuffer(int level, int *verify, char *fname);
      std::string makeRelPathBuffer(int *verify, std::string fname);
      std::string makeRelPathBuffer(int level, int *verify, std::string fname);

      // delete stored directory line
      // Caution: 'rm -rf' is used here.
      // NOTE: if level > 0, partial line is counted from the beginning up
      // NOTE: if level < 0, partial line is counted from the end down 
      int deleteLine(int level,int *verify);
      // delete that portion of the line that has been created
      // by this object
      int deleteLine(int *verify);

      // accumulate data in this diretory, based on the argument directory 
      // If there are files in the argument directory that do not exist in the
      // current directory, they will be copied in. Same with directories, except
      // cp -r will be used. 
      // NOTE: only level > 0  usrlevel > 0 accepted here
      int accumulate(rcfDirectoryMgr & dir);
      int accumulate(int level,rcfDirectoryMgr & dir,int usrlvl);
      int accumulateFilteredFiles(rcfDirectoryMgr & dir);

      // drop data into this directory. All files
      // found in the immediate subdirectories of the argument directory will 
      // be copied into this directory. NOTE: there is nothing recursive
      // here. Files in the argument directory will be ignored,  
      // and directories within examined subdirectories will be ignored.
      // this is a one-level copy, designed for extracting multiple 
      // DICOM image sets from directories where they are stored into
      // a single directory from which they will be uploaded.  
      int dropData(rcfDirectoryMgr & dir);

      // stat the line, and access file/directory names
      // outputs number of directories found (first output) 
      // and files found (second output)
      int statLine(int *ndirs,int *nfiles);
      int statLine(int *ndirs,int *nfiles, int nlevel);
      // get file names. Index follows C convention
      std::string getFileNameInLine(unsigned int index, int *verify);
      std::string getDirectoryNameInLine(unsigned int index, int *verify);

      // a family of functions which compares contents of two directories
      // full control of levels
      int compare(int level,rcfDirectoryMgr & usrdir,int usrlvl,bool recurse,bool *result);
      // compare directories that the full line points to, allows to control 
      // recursive checking of subordinate directories
      int compare(rcfDirectoryMgr & usrdir,bool recurse,bool *result);
      // do not recursively check subordinate directories.
      // Just compare names of files and directories
      int compare(rcfDirectoryMgr & usrdir,bool *result);



      //filter file/directory list by substring within the name. If line has not been 
      // yet examined, it will be automatically examined by this function
      int filterFilesBySubstring(std::string user, int *nfiles);
      int filterDirectoriesBySubstring(std::string user, int *ndirs);

      //filter file list by file type. Unix period will be prepended to user string, 
      // each file name will be examined for the presence of this extension at the
      // end of file name. If line has not yet been examined, it will be examined
      // automatically 
      int filterFilesByType(std::string user, int *nfiles);

      // get file names. Index follows C convention
      std::string getFilteredFileName(unsigned int index,int *verify);
      std::string getFilteredDirectoryName(unsigned int index, int *verify);

      // misc status functions
      bool isObjectValid();

      //misc utility functions
      //remove spaces from the string
      int cleanStringToken(char *usrbuf);
      // append string with operating system separator
      std::string appendTerminalSeparator(std::string user);

      //printing, mostly for debugging purposes
      // prints to stderr
      int printContent(int level);
      int printContent();

      // set log file for system commands
      int setSystemLog(FILE *usrlogid);

      //as a public service
      std::string getSeparator();

   protected:

      void reportError(const char *string);
      int operateOnLine(int select, int level, int *verify);
      int operateOnFile(char *file,int select, int level, int *verify);
      int clearLineContents(int select);
      int examineLine(int level);
      int repairUnixCommandString(std::string *usrstr);
      int systemAndLog(const char *command);

   private:

       void basicInit();
       char* makePathBufferSelect(int select,int level, int *verify, char *fname);

      
};

#endif
