#ifndef SOMEHELPERS
#define SOMEHELPERS

#include <vector>
#include <random>
#include <iostream>
#include <fstream>      // std::ofstream
#include <algorithm> // std::transform
#include <functional> // std::negate
#include <ctime> // std::time  --> used in nowInString
#include <stdio.h> // FILE for fread
#include <iterator> // std::ios_base::begin


#include <chrono>// to use -->  std::this_thread::sleep_for(std::chrono::milliseconds(x));
#include <thread>// to use -->  std::this_thread::sleep_for(std::chrono::milliseconds(x));

#include <stdexcept> // exception

#include <QtXml>

#include "typedefs.h"    ///< Include the basic types

struct Point {
    double x, y;
};

using PointFloat = Point_2d_float;

struct Point3 {
    double x, y , z;
};

using Point3Float = Point_3d_float;

//using namespace std;
#define PI           3.14159265358979323846  /* pi */

//#ifdef _WIN32
//#define NOMINMAX
//#include <windows.h>

//void sleep(unsigned milliseconds)
//{
//    Sleep(milliseconds);
//}
//#else
//#include <unistd.h>

//void sleep(unsigned milliseconds)
//{
//    usleep(milliseconds * 1000); // takes microseconds
//}
//#endif


namespace
{
/**
 * @brief retrieveXMLElementsInString
 * @param root --> root QDomElement node
 * @param tag  --> tag to search
 * @param att  --> attribute within the tag
 * @param logstream --> stream to log events.
 * @return
 */
std::vector<std::string>  retrieveXMLElementsInString(QDomElement root, QString tag, QString att,std::ostream &logstream )
{
    std::vector<std::string> outputAttributesVec;
    QDomNodeList nodes = root.elementsByTagName(tag);

    logstream << "# of nodes with tag  "  << tag.toStdString()  << " is : "  << nodes.count() << std::endl;
    for(int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if(elm.isElement())
        {
            QDomElement e = elm.toElement();
            outputAttributesVec.push_back( e.attribute(att).toStdString());
            logstream << e.attribute(att).toStdString() << std::endl;
        }
    }
    return outputAttributesVec;
}
/**
 * @brief printProgressBar : Prints progress bar in the desired ouput stream
 * usage:
 *  for (float progress = 0; progress < 1; progress=progress+.1)
    {
        printProgressBar(logstream,progress);
       // std::this_thread::sleep_for(std::chrono::milliseconds(500)) ;
    }
    printProgressBar(logstream,1.0);
 *
 * @param logstream
 * @param progress
 */
//for (float progress = 0; progress < 1; progress=progress+.1)
//{
//    printProgressBar(logstream,progress);
//   // std::this_thread::sleep_for(std::chrono::milliseconds(500)) ;
//}
//printProgressBar(logstream,1.0);

// https://coderwall.com/p/jsxrdq/c-erase-elements-from-containers-by-indices
// This will be used to delete a List of indices from a container.
// Example : here we erase indices in ar from container v.
// int ar[] = { 2, 0, 4 };
// v.erase(ToggleIndices(v, std::begin(ar), std::end(ar)), v.end());
template<typename Cont, typename It>
auto ToggleIndices(Cont &cont, It beg, It end) -> decltype(std::end(cont))
{
    int helpIndx(0);
    return std::stable_partition(std::begin(cont), std::end(cont),
                                 [&](decltype(*std::begin(cont)) & val) -> bool {
        return std::find(beg, end, helpIndx++) == end;
    });
}

void printProgressBar(std::ostream &logstream, const float & progress)
{
    int barWidth = 60;
    logstream << "[";
    int pos =  barWidth * progress;
    for (int i = 0; i < barWidth; ++i)
    {
        if (i < pos) logstream << "=";
        else if (i == pos) logstream << ">";
        else logstream << " ";
    }
    logstream << "] " << int(progress * 100.0) << " %\r";

    logstream.flush();

}



bool writeVectorToBinaryFile(std::string filePath, std::vector<float>& inputVector)
{
    //std::ofstream FILE(filePath, std::ios::out | std::ofstream::binary);
    //std::ofstream FILE(filePath, std::ios::out );
    //std::copy(inputVector.begin(), inputVector.end(), std::ostreambuf_iterator<char>(FILE));

    //FILE* pFile;
    //pFile = fopen("file.binary", "wb");
    //fwrite(a, 1, size*sizeof(double), pFile);

    FILE * pFile;

    //char buffer[] = { 'x' , 'y' , 'z' };
    pFile = fopen (filePath.c_str(), "wb");
    if (pFile == 0)
    {
        std::cout << "ERROR:  could not open the file: " << filePath   << std::endl;
        return false;
    }
    auto return_value = fwrite (&inputVector[0] , sizeof(float), inputVector.size(), pFile);
    if (return_value != inputVector.size())
    {
        std::cout << " ERROR: in writing file : " << filePath   << std::endl;
        return false;
    }

    if (fclose (pFile)!=0)
    {
        std::cout << " ERROR: in closing file : " << filePath   << std::endl;
        return false;
    }
    return true;
}

bool writeArrayToBinaryFile(std::string filePath, double* inputVector, int size)
{
    //std::ofstream FILE(filePath, std::ios::out | std::ofstream::binary);
    //std::ofstream FILE(filePath, std::ios::out );
    //std::copy(inputVector.begin(), inputVector.end(), std::ostreambuf_iterator<char>(FILE));

    //FILE* pFile;
    //pFile = fopen("file.binary", "wb");
    //fwrite(a, 1, size*sizeof(double), pFile);

    FILE * pFile;
    //char buffer[] = { 'x' , 'y' , 'z' };



    pFile = fopen (filePath.c_str(), "wb");
    if (pFile == 0)
    {
        std::cout << "ERROR:  could not open the file: " << filePath   << std::endl;
        return false;
    }

    auto return_value = fwrite (&inputVector[0] , sizeof(float), size, pFile);
    if (return_value != size)
    {
        std::cout << " ERROR: in writing file : " << filePath   << std::endl;
        return false;
    }

    if (fclose (pFile)!=0)
    {
        std::cout << " ERROR: in closing file : " << filePath   << std::endl;
        return false;
    }

    return true;

}

void writeVectorToBinaryFile(std::string filePath, std::vector<double>& inputVector)
{
    std::ofstream FILE(filePath, std::ios::out | std::ofstream::binary);
    //std::ofstream FILE(filePath, std::ios::out );
    std::copy(inputVector.begin(), inputVector.end(), std::ostreambuf_iterator<char>(FILE));
}

void writeVectorToBinaryFileQT(std::string filePath, std::vector<double>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    QFile FILE(filePath.c_str());
    if (!FILE.open(QIODevice::WriteOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    QDataStream out(&FILE);
    out.setByteOrder(QDataStream::LittleEndian);
    //std::copy(inputVector.begin(),inputVector.end(),out);
    for (int count = 0; count < inputVector.size(); count++){
        out << inputVector[count];
    }
    FILE.close();
}

void writeVectorToBinaryFileQT(std::string filePath, std::vector<float>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    QFile FILE(filePath.c_str());
    if (!FILE.open(QIODevice::WriteOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    QDataStream out(&FILE);
    out.setByteOrder(QDataStream::LittleEndian);
    //std::copy(inputVector.begin(),inputVector.end(),out);
    for (int count = 0; count < inputVector.size(); count++){
        out << inputVector[count];
    }
    FILE.close();
}


void writeVectorToBinaryFileQT(std::string filePath, std::vector<int>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    QFile FILE(filePath.c_str());
    if (!FILE.open(QIODevice::WriteOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    QDataStream out(&FILE);
    out.setByteOrder(QDataStream::LittleEndian);
    //std::copy(inputVector.begin(),inputVector.end(),out);
    for (int count = 0; count < inputVector.size(); count++){
        out << inputVector[count];
    }
    FILE.close();
}


void writeVectorToBinaryFileQT(std::string filePath, std::vector<short int>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    QFile FILE(filePath.c_str());
    if (!FILE.open(QIODevice::WriteOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    QDataStream out(&FILE);
    out.setByteOrder(QDataStream::LittleEndian);
    //std::copy(inputVector.begin(),inputVector.end(),out);
    for (int count = 0; count < inputVector.size(); count++){
        out << inputVector[count];
    }
    FILE.close();
}


void writeVectorToBinaryFileQT(std::string filePath, std::vector<short unsigned int>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    QFile FILE(filePath.c_str());
    if (!FILE.open(QIODevice::WriteOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    QDataStream out(&FILE);
    out.setByteOrder(QDataStream::LittleEndian);
    //std::copy(inputVector.begin(),inputVector.end(),out);
    for (int count = 0; count < inputVector.size(); count++){
        out << inputVector[count];
    }
    FILE.close();
}

void writeVectorToAsciiFile(std::string filePath, std::vector<double>& inputVector)
{
    std::ofstream FILE(filePath);
    if (FILE.is_open())
    {
        for (int count = 0; count < inputVector.size(); count++){
            FILE << inputVector[count] << "\n";
        }
        FILE.close();
    }
    else std::cout << "Unable to open file " + filePath;
}


void readLitteEndianBinaryFileToStdVector(std::string filePath, std::vector<float>& inputVector)
{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    // http://qtsimplify.blogspot.com/2013/05/reading-n-bytes-of-data-from.html
    // http://stackoverflow.com/questions/19081385/c-read-big-endian-binary-float
    //http://stackoverflow.com/questions/4680470/problems-saving-float-array-to-binary-file-and-reading-back-c
    FILE* file ;
    file = fopen(filePath.c_str(),"rb");
    if (file==NULL)
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    for (int count = 0; count < inputVector.size(); count++)
    {
        float f;
        fread(&f, sizeof(float), 1, file);
        //in >> temp ;
        inputVector[count] = f;
    }

    fclose(file);

    //    QFile FILE(filePath.c_str());
    //    if (!FILE.open(QIODevice::ReadOnly ))
    //    {
    //        std::cout << "ERROR in opening file " << filePath << std::endl;
    //        return;
    //    }
    //    QDataStream in(&FILE);
    //    in.setByteOrder(QDataStream::LittleEndian);
    //    float temp;
    //    for (int count = 0; count < inputVector.size(); count++){
    //        in >> temp ;
    //             inputVector[count] = float(temp);
    //        }
    //QString str;


    //QByteArray allBytes = FILE.readAll();
    //allBytes.toFloat();

    //std::copy(inputVector.begin(),inputVector.end(),out);

    //    FILE.close();
}

void readLitteEndianBinaryFileToStdDoubleVector(std::string filePath, std::vector<double>& inputVector)

{
    //http://stackoverflow.com/questions/11118580/writing-binary-files-in-qt-and-reading-in-matlab
    // http://qtsimplify.blogspot.com/2013/05/reading-n-bytes-of-data-from.html
    // http://stackoverflow.com/questions/19081385/c-read-big-endian-binary-float
    //http://stackoverflow.com/questions/4680470/problems-saving-float-array-to-binary-file-and-reading-back-c
    FILE* file ;
    file = fopen(filePath.c_str(),"rb");
    if (file==NULL)
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return;
    }
    for (int count = 0; count < inputVector.size(); count++)
    {
        float f;
        fread(&f, sizeof(float), 1, file);
        //in >> temp ;
        inputVector[count] = f;
    }

    fclose(file);

    //    QFile FILE(filePath.c_str());
    //    if (!FILE.open(QIODevice::ReadOnly ))
    //    {
    //        std::cout << "ERROR in opening file " << filePath << std::endl;
    //        return;
    //    }
    //    QDataStream in(&FILE);
    //    in.setByteOrder(QDataStream::LittleEndian);
    //    float temp;
    //    for (int count = 0; count < inputVector.size(); count++){
    //        in >> temp ;
    //             inputVector[count] = float(temp);
    //        }
    //QString str;


    //QByteArray allBytes = FILE.readAll();
    //allBytes.toFloat();

    //std::copy(inputVector.begin(),inputVector.end(),out);

    //    FILE.close();
}

//long GetFileSize(std::string filename)
//{
//    struct stat stat_buf;
//    int rc = stat(filename.c_str(), &stat_buf);
//    return rc == 0 ? stat_buf.st_size : -1;
//}

enum DataByteOrder {
    LitteEndian,
    BigEndian,
    Unknown
};

template <typename T>
bool readBinayFileQt(std::string filePath, DataByteOrder byteOrder, size_t numberOfElementToRead,  std::vector<T> &outputVector)
{
    //http://doc.qt.io/qt-4.8/qtendian.html


    QFile qFile(filePath.c_str());
    if (!qFile.open(QIODevice::ReadOnly))
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return false;
    }
    // qDebug() <<  "File Size is : "<< qFile.size() ;



    QDataStream dataQDataStream(&qFile);

    dataQDataStream.setFloatingPointPrecision(QDataStream::SinglePrecision);

    dataQDataStream.setByteOrder(QDataStream::LittleEndian);
    //    if (IsBigEndian)
    //    {
    //        dataQDataStream.setByteOrder(QDataStream::BigEndian);
    //    }
    //    else
    //    {
    //        dataQDataStream.setByteOrder(QDataStream::LittleEndian);
    //    }

    outputVector.clear();
    outputVector = std::vector<T> (numberOfElementToRead);
    int numBytesFromFile;

    // TODO : Not sure if we need to find the current machine endian first --> It is assumed that   dataQDataStream.setByteOrder(QDataStream::LittleEndian/BigEndian)
    // force reading in littleEndian/BigEndian. But since we are using readRawData, it is our responsibility to take care of the Endian
    // But, it is not clear to me if readRawData will load the data based on the machine byteorder or based on the dataQDataStream.setByteOrder
    // Since our machines are little endian we wont have any problem here.
    numBytesFromFile= dataQDataStream.readRawData(reinterpret_cast<char*>(outputVector.data()) ,numberOfElementToRead*sizeof(T));

    qFile.close();


    if (numBytesFromFile < 0)
    {
        std::cout << "ERROR readBinayFileQt in reading file: " << filePath << std::endl;
        return false;
    }
    if (numBytesFromFile != numberOfElementToRead*sizeof(T))
    {
        //            std::cout << "sizeof(T) = " << sizeof(T)<< std::endl;;
        //            std::cout << "numBytesFromFile != numberOfElementToRead*sizeof(T) ==>" << "numBytesFromFile" << numBytesFromFile<< " and numberOfElementToRead*sizeof(T) = " << numberOfElementToRead*sizeof(T) << std::endl;;
        std::cout << "ERROR readBinayFileQt in reading file: " << filePath << std::endl;

        return false;
    }


    if (byteOrder == BigEndian)
    {
        quint16* qint16Pointer;
        quint32* qint32Pointer;
        quint64* qint64Pointer;

        if (sizeof(T) == 2)
        {
            qint16Pointer= reinterpret_cast<quint16*>(outputVector.data());
            for (int iVal = 0; iVal < outputVector.size(); ++iVal)
            {
                *(qint16Pointer+iVal)= qFromBigEndian<quint16>(qint16Pointer+iVal);
            }
        }
        else if (sizeof(T) == 4)
        {
            qint32Pointer= reinterpret_cast<quint32*>(outputVector.data());

            for (int iVal = 0; iVal < outputVector.size(); ++iVal)
            {
                *(qint32Pointer+iVal)= qFromBigEndian<quint32>(qint32Pointer+iVal);
            }
        }
        else if (sizeof(T) == 8)
        {
            qint64Pointer= reinterpret_cast<quint64*>(outputVector.data());

            for (int iVal = 0; iVal < outputVector.size(); ++iVal)
            {
                *(qint64Pointer+iVal)= qFromBigEndian<quint64>(qint64Pointer+iVal);
            }
        }
        else
        {
            qDebug() << "Unknown -- >  sizeof(T) " << sizeof(T) ;
            return true;
        }


    }

    //    qDebug() << " Number of Bytes read " << numBytesFromFile;


    return true;
}



bool readLittleEndianBinaryFileToStdCharVector(std::string filePath, std::vector< char>& inputVector, long & numberOfBytesRead)
{

    FILE* file ;
    numberOfBytesRead = 0;
    file = fopen(filePath.c_str(),"rb");
    if (file==NULL)
    {
        std::cout << "ERROR in opening file " << filePath << std::endl;
        return false;
    }
    const int bufferSize = 1;
    auto buffer = std::vector<char>(bufferSize);
    int bytes;
    while ((bytes = fread(&buffer[0], bufferSize, sizeof(char), file)) > 0)
    {
        inputVector.push_back(buffer[0]);
        //inputVector.insert(inputVector.end(), buffer.begin(),buffer.begin()+bytes);
    }
    fclose(file);
    numberOfBytesRead = inputVector.size();



    return true;
}

char*  concatChar(const char * str1, const char * str2)
{
    char * str3 = (char *) malloc(1 + strlen(str1)+ strlen(str2) );
    strcpy(str3, str1);
    strcat(str3, str2);
    return str3;
}

void writeVectorToAsciiFile(std::string filePath, std::vector<float>& inputVector)
{
    std::ofstream FILE(filePath);
    if (FILE.is_open())
    {
        for (int count = 0; count < inputVector.size(); count++){
            FILE << inputVector[count] << "\n";
        }
        FILE.close();
    }
    else std::cout << "Unable to open file " + filePath;
}

std::pair<std::vector<double>, std::vector<int>> sortValuesAndSortIndices(std::vector<double>& x)
{
    std::vector<int> sortIndices(x.size());
    std::vector<double> xSorted(x);

    std::size_t n(0);
    // fill sort indices from 0 ... x.size-1
    std::generate(std::begin(sortIndices), std::end(sortIndices), [&]{ return n++; });

    // To sort x
    std::sort(std::begin(xSorted),
              std::end(xSorted),
              [](double a, double b) { return a < b; });
    // Get the sort indecies
    std::sort(std::begin(sortIndices),
              std::end(sortIndices),
              [&](int i1, int i2) { return x[i1] < x[i2]; });

    std::pair < std::vector<double>, std::vector<int> >  output;
    output.first = xSorted;
    output.second = sortIndices;
    return output;
}
std::pair<std::vector<int>, std::vector<int>> sortValuesAndSortIndices(std::vector<int>& x)
{
    std::vector<int> sortIndices(x.size());
    std::vector<int> xSorted(x);

    std::size_t n(0);
    // fill sort indices from 0 ... x.size-1
    std::generate(std::begin(sortIndices), std::end(sortIndices), [&]{ return n++; });

    // To sort x
    std::sort(std::begin(xSorted),
              std::end(xSorted),
              [](int a, int b) { return a < b; });
    // Get the sort indecies
    std::sort(std::begin(sortIndices),
              std::end(sortIndices),
              [&](int i1, int i2) { return x[i1] < x[i2]; });

    std::pair < std::vector<int>, std::vector<int> >  output;
    output.first = xSorted;
    output.second = sortIndices;
    return output;
}




template<typename Func>
std::vector<unsigned int> findSatisfiyingIndices(std::vector<unsigned int> &v, Func f)
{
    std::vector<unsigned int> results;

    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end()) {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}


template<typename Func>
std::vector<int> findSatisfiyingIndices(std::vector<float> &v, Func f)
{
    std::vector<int> results;

    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end()) {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

template<typename Func>
std::vector<int> findSatisfiyingIndices(std::vector<double>& v, Func f)
{
    std::vector<int> results;

    auto it = std::find_if(std::begin(v), std::end(v), f);
    while (it != std::end(v)) {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

template <class Func>
std::vector<int> findSatisfiyingIndices(std::vector<int>& v, Func f)
{
    std::vector<int> results;

    auto it = std::find_if(std::begin(v), std::end(v), f);
    while (it != std::end(v)) {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

float degreeToRad(float d)
{
    return d*PI / 180.0;
}


std::string nowInString()
{
    std::time_t t = std::time(0);   // get time now
    struct tm * now = localtime( & t );
    std::string nowInString=std::to_string(now->tm_year + 1900) + "-"+ std::to_string(now->tm_mon + 1)+"-" + std::to_string(now->tm_mday)+"_"+std::to_string(now->tm_hour)+"-"+std::to_string(now->tm_min);
    return nowInString;
}

std::string nowInString2()
{
    std::time_t t = std::time(0);   // get time now
    struct tm * now = localtime( & t );
    std::string nowInString=std::to_string(now->tm_year + 1900) + "/"+ std::to_string(now->tm_mon + 1)+"/" + std::to_string(now->tm_mday)+" "+std::to_string(now->tm_hour)+":"+std::to_string(now->tm_min);
    return nowInString;
}
template<typename T>
std::vector<T> linspace(T a, T b, int n)
{
    std::vector<T> array;
    double step = (b - a) / (n - 1);

    while (a <= b) {
        array.push_back(a);
        a += step;           // could recode to better handle rounding errors
    }
    return array;
}

template<typename T>
std::vector<T> signFreelinspace(T a, T b, int n)
{
    std::vector<T> array;


    double step = (b - a) / (n - 1);

    if (a< b)
    {
        while (a < b) {
            array.push_back(a);
            a += step;           // could recode to better handle rounding errors
        }
    }
    else if (b < a)
    {
        while (b < a) {
            array.push_back(a);
            a += step;           // could recode to better handle rounding errors
        }
    }
    else
    {
        array = std::vector<T>(n,a);
    }
    return array;
}

std::vector<double> generateRange(double startPt, double step, double stopPt)
{
    std::vector<double> array;
    while (startPt <= stopPt) {
        array.push_back(startPt);
        startPt += step;         // could recode to better handle rounding errors
    }
    return array;
}

std::vector<float> generateFloatRange(float startPt, float step, float stopPt)
{
    std::vector<float> array;
    while (startPt <= stopPt) {
        array.push_back(startPt);
        startPt += step;         // could recode to better handle rounding errors
    }
    return array;
}

std::vector<int> generateIntRange(int startPt, int step, int stopPt)
{
    std::vector<int> array;
    while (startPt <= stopPt) {
        array.push_back(startPt);
        startPt += step;         // could recode to better handle rounding errors
    }
    return array;
}

bool fileExists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}
/// generates random numbers form uniform distribution (0,1)
std::vector<float> rand(const int vecLength)
{
    std::uniform_real_distribution<float> unif(0.0, 1.0);
    std::random_device rand_dev;          // Use random_device to get a random seed.
    std::mt19937 rand_engine(rand_dev()); // mt19937 is a good pseudo-random number  generator.
    std::vector<float> output(vecLength, 0);
    for (int i = 0; i < vecLength; i++)
    {
        output[i] = unif(rand_engine);
    }
    return output;
}


/// generates random numbers form uniform distribution within  (lower_bound,upper_bound) range
std::vector<float> rand(const float lower_bound, const float upper_bound, const int vecLength)
{
    if (upper_bound <= lower_bound)
    {
        std::cout << " lower_bound  is less than upper_bound, we swap them and continue!!!" << std::endl;
    }

    std::vector<float> output = rand(vecLength);


    for (int i = 0; i < vecLength; i++)
    {
        output[i] = output[i] * (upper_bound - lower_bound) + lower_bound;
    }

    return output;
}


/// generate random numbers form normal distribution with mean zero and variance 1
std::vector<float> randn(const int vecLength)
{

    std::normal_distribution<float> normalDist(0.0, 1.0);
    std::random_device rand_dev;          // Use random_device to get a random seed.
    std::mt19937 rand_engine(rand_dev()); // mt19937 is a good pseudo-random number  generator.
    std::vector<float> output(vecLength, 0);
    for (int i = 0; i < vecLength; i++)
    {
        output[i] = normalDist(rand_engine);
    }
    return output;
}
///generate random scaler from normal distribution with the given mean and standard deviation value

float  randn(const float mean, const float std_dev)
{

    std::normal_distribution<float> normalDist(mean, std_dev);
    std::random_device rand_dev;          // Use random_device to get a random seed.
    std::mt19937 rand_engine(rand_dev()); // mt19937 is a good pseudo-random number  generator.
    return normalDist(rand_engine);
}


/// generate random numbers form normal distribution with the given mean and standard deviation value
std::vector<float> randn(const float mean, const float std_dev, const int vecLength)
{
    std::vector<float> output(vecLength, 0);
    if (std_dev == 0.0)
    {
        output = std::vector<float>(vecLength, mean);
        return output;
    }

    std::normal_distribution<float> normalDist(mean, std_dev);
    std::random_device rand_dev;          // Use random_device to get a random seed.
    std::mt19937 rand_engine(rand_dev()); // mt19937 is a good pseudo-random number  generator.
    for (int i = 0; i < vecLength; i++)
    {
        output[i] = normalDist(rand_engine);
    }
    return output;
}



/// negates selected coordinate in pointVector

void negateSelectedCoordinates(std::vector<Point3>& pointVec, std::vector<bool>& Coordinate)
{
    if (Coordinate.size()>3)
    {
        std::cout << " Cooridnate input in " << "RoiModel::negateSelectedCoordinates  has more than three inputs " << std::endl;
    }

    for (auto & p : pointVec)
    {
        if (Coordinate[0])
            p.x = -p.x;

        if (Coordinate[1])
            p.y = -p.y;

        if (Coordinate[2])
            p.z = -p.z;
    }
}

/// negates selected coordinate among pointVecX , pointVecY and pointVecZ
void negateSelectedCoordinates(std::vector<double>& pointVecX, std::vector<double>& pointVecY, std::vector<double>& pointVecZ, std::vector<bool>& Coordinate)
{
    if (Coordinate.size() > 3)
    {
        std::cout << " Cooridnate input in " << "RoiModel::negateSelectedCoordinates  has more than three inputs " << std::endl;
    }



    if (Coordinate[0])
    {
        std::transform(pointVecX.begin(), pointVecX.end(), pointVecX.begin(), std::negate<double>());
    }

    if (Coordinate[1])
    {
        std::transform(pointVecY.begin(), pointVecY.end(), pointVecY.begin(), std::negate<double>());
    }
    if (Coordinate[2])
    {
        std::transform(pointVecZ.begin(), pointVecZ.end(), pointVecZ.begin(), std::negate<double>());
    }

}




std::vector<Point3> xyzMeshGrid(std::vector<double>& xGridPts, std::vector<double>& yGridPts, std::vector<double>& zGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    auto sizeZ = zGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    Point3 P;
    P.x = 0.0;
    P.y = 0.0;
    P.z = 0.0;


    std::vector<Point3> outputPoint(sizeX*sizeY*sizeZ, P);
    for (int k = 0; k < sizeZ; k++)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                //X[i*sizeY + j] = xGridPts[i];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].x = xGridPts[i];
                //Y[i*sizeY + j] = yGridPts[j];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].y = yGridPts[j];
                //Z[i*sizeY + j] = ZGridPts[k];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].z = zGridPts[k];
            }
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;

}


void xyzMeshGrid(const std::vector<double>& xGridPts, const std::vector<double>& yGridPts, const std::vector<double>& zGridPts, std::vector<double>& X, std::vector<double>& Y, std::vector<double>& Z)
{
    //
    ////////std::vector<double> a;
    ////////a.push_back(1);
    ////////a.push_back(2);
    ////////std::vector<double> b;
    ////////b.push_back(3);
    ////////b.push_back(4);
    ////////b.push_back(5);
    ////////std::vector<double> c;
    ////////c.push_back(6);
    ////////c.push_back(7);
    ////////c.push_back(8);
    ////////c.push_back(9);
    ////////auto testMeshgrid = xyzMeshGrid(a, b, c);


    // X,Y and Z are the output of the xyzMeshGrid
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    auto sizeZ = zGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);


    X = std::vector<double>(sizeX*sizeY*sizeZ, 0);
    Y = std::vector<double>(sizeX*sizeY*sizeZ, 0);
    Z = std::vector<double>(sizeX*sizeY*sizeZ, 0);
    int index = 0;
    for (int k = 0; k < sizeZ; k++)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                //index = k*sizeX*sizeY + j*sizeX + i; // col major
                index = k*sizeX*sizeY + i*sizeY + j; // row major

                //X[i*sizeY + j] = xGridPts[i];
                X[index] = xGridPts[i];

                //Y[i*sizeY + j] = yGridPts[j];
                Y[index] = yGridPts[j];
                //Z[i*sizeY + j] = ZGridPts[k];
                Z[index] = zGridPts[k]; // col major

            }
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/

}


void xyzMeshGrid(const std::vector<float>& xGridPts, const std::vector<float>& yGridPts, const std::vector<float>& zGridPts, std::vector<float>& X, std::vector<float>& Y, std::vector<float>& Z)
{
    //
    ////////std::vector<double> a;
    ////////a.push_back(1);
    ////////a.push_back(2);
    ////////std::vector<double> b;
    ////////b.push_back(3);
    ////////b.push_back(4);
    ////////b.push_back(5);
    ////////std::vector<double> c;
    ////////c.push_back(6);
    ////////c.push_back(7);
    ////////c.push_back(8);
    ////////c.push_back(9);
    ////////auto testMeshgrid = xyzMeshGrid(a, b, c);


    // X,Y and Z are the output of the xyzMeshGrid
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    auto sizeZ = zGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);


    X = std::vector<float>(sizeX*sizeY*sizeZ, 0);
    Y = std::vector<float>(sizeX*sizeY*sizeZ, 0);
    Z = std::vector<float>(sizeX*sizeY*sizeZ, 0);
    int index = 0;
    for (int k = 0; k < sizeZ; k++)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                //index = k*sizeX*sizeY + j*sizeX + i; // col major
                index = k*sizeX*sizeY + i*sizeY + j; // row major

                //X[i*sizeY + j] = xGridPts[i];
                X[index] = xGridPts[i];

                //Y[i*sizeY + j] = yGridPts[j];
                Y[index] = yGridPts[j];
                //Z[i*sizeY + j] = ZGridPts[k];
                Z[index] = zGridPts[k]; // col major

            }
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/

}


std::vector<PointFloat> xyMeshGrid(std::vector<float>& xGridPts, std::vector<float>& yGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    PointFloat P;
    P.x = 0.0;
    P.y = 0.0;

    std::vector<PointFloat> outputPoint(sizeX*sizeY, P);
    for (int i = 0; i < sizeX; i++)
    {
        for (int j = 0; j < sizeY; j++)
        {
            //X[i*sizeY + j] = xGridPts[i];
            outputPoint[i*sizeY + j].x = xGridPts[i];
            //Y[i*sizeY + j] = yGridPts[j];
            outputPoint[i*sizeY + j].y = yGridPts[j];
        }
    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;
}

std::vector<Point> xyMeshGrid(std::vector<double>& xGridPts, std::vector<double>& yGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    Point P;
    P.x = 0.0;
    P.y = 0.0;

    std::vector<Point> outputPoint(sizeX*sizeY, P);
    for (int i = 0; i < sizeX; i++)
    {
        for (int j = 0; j < sizeY; j++)
        {
            //X[i*sizeY + j] = xGridPts[i];
            outputPoint[i*sizeY + j].x = xGridPts[i];
            //Y[i*sizeY + j] = yGridPts[j];
            outputPoint[i*sizeY + j].y = yGridPts[j];
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;
}


std::vector<Point3Float> xyzMeshGrid(std::vector<float>& xGridPts, std::vector<float>& yGridPts, std::vector<float>& zGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    auto sizeZ = zGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    Point3Float P;
    P.x = 0.0;
    P.y = 0.0;
    P.z = 0.0;


    std::vector<Point3Float> outputPoint(sizeX*sizeY*sizeZ, P);
    for (int k = 0; k < sizeZ; k++)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                //X[i*sizeY + j] = xGridPts[i];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].x = xGridPts[i];
                //Y[i*sizeY + j] = yGridPts[j];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].y = yGridPts[j];
                //Z[i*sizeY + j] = ZGridPts[k];
                outputPoint[k*sizeX*sizeY + i*sizeY + j].z = zGridPts[k];
            }
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;

}


bool pnpoly(int nvert, std::vector<double>& vertx, std::vector<double>& verty, double testx, double testy)
{
    int i, j;
    bool c = false;
    for (i = 0, j = nvert-1; i < nvert; j = i++) {
        if ( ((verty[i]>testy) != (verty[j]>testy)) &&
             (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
            c = !c;
    }
    return c;
}

bool pnpoly(int nvert, std::vector<float>& vertx, std::vector<float>& verty, float testx, float testy)
{
    int i, j;
    bool c = false;
    for (i = 0, j = nvert-1; i < nvert; j = i++) {
        if ( ((verty[i]>testy) != (verty[j]>testy)) &&
             (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
            c = !c;
    }
    return c;
}


std::vector<int> inPolygonIndices(const std::vector<Point3Float> &pVec, const std::vector<Point3Float>& polygon)

{
    int nvert = polygon.size();
    std::vector<float> vertx(nvert,0);
    std::vector<float> verty(nvert,0);

    for (int iVert=0; iVert <nvert; iVert++)
    {
        vertx[iVert]= polygon[iVert].x;
        verty[iVert]= polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert,vertx, verty,pVec[i].x,pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
    return inPolyIndices;

}

// function ignores the z coordinates of polygon
std::vector<int> inPolygonIndices(const std::vector<PointFloat> &pVec, const std::vector<Point3Float>& polygon)
{
    int nvert = polygon.size();
    std::vector<float> vertx(nvert,0);
    std::vector<float> verty(nvert,0);

    for (int iVert=0; iVert <nvert; iVert++)
    {
        vertx[iVert]= polygon[iVert].x;
        verty[iVert]= polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert,vertx, verty,pVec[i].x,pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}

std::vector<int> inPolygonIndices(const std::vector<PointFloat> &pVec, const std::vector<PointFloat>& polygon)
{
    int nvert = polygon.size();
    std::vector<float> vertx(nvert,0);
    std::vector<float> verty(nvert,0);

    for (int iVert=0; iVert <nvert; iVert++)
    {
        vertx[iVert]= polygon[iVert].x;
        verty[iVert]= polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert,vertx, verty,pVec[i].x,pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}






void RotationMatrixFromEulerAngles(const float thetaX, const float thetaY,const float thetaZ, float* R)
{

    // R=std::vector<float>(9,0);
    float cx = cos(degreeToRad(thetaX));
    float cy = cos(degreeToRad(thetaY));
    float cz = cos(degreeToRad(thetaZ));
    float sx = sin(degreeToRad(thetaX));
    float sy = sin(degreeToRad(thetaY));
    float sz = sin(degreeToRad(thetaZ));
    R[0] = cy*cz;
    R[1] = sy*sx*cz - sz*cx;
    R[2] = sy*cx*cz + sz*sx;
    R[3] = cy*sz;
    R[4] = sy*sx*sz + cz*cx;
    R[5] = sy*cx*sz - cz*sx;
    R[6] = -sy;
    R[7] = cy*sx;
    R[8] = cy*cx;


    // Reference matlab implementation
    //            //// angle2dcm( r1, r2, r3) function matlab
    //            //// angles = [r1(:) r2(:) r3(:)];
    //            //// dcm = zeros(3,3,size(angles,1));
    //            //// cang = cos(angles);
    //            //// sang = sin(angles);
    //            ////case 'zyx'
    //            ////	%[cy*cz, cy*sz, -sy]
    //            ////	% [sy*sx*cz - sz*cx, sy*sx*sz + cz*cx, cy*sx]
    //            ////	% [sy*cx*cz + sz*sx, sy*cx*sz - cz*sx, cy*cx]

    //            ////	dcm(1, 1, :) = cang(:, 2).*cang(:, 1);
    //            ////	dcm(1, 2, :) = cang(:, 2).*sang(:, 1);
    //            ////	dcm(1, 3, :) = -sang(:, 2);
    //            ////	dcm(2, 1, :) = sang(:, 3).*sang(:, 2).*cang(:, 1) - cang(:, 3).*sang(:, 1);
    //            ////	dcm(2, 2, :) = sang(:, 3).*sang(:, 2).*sang(:, 1) + cang(:, 3).*cang(:, 1);
    //            ////	dcm(2, 3, :) = sang(:, 3).*cang(:, 2);
    //            ////	dcm(3, 1, :) = cang(:, 3).*sang(:, 2).*cang(:, 1) + sang(:, 3).*sang(:, 1);
    //            ////	dcm(3, 2, :) = cang(:, 3).*sang(:, 2).*sang(:, 1) - sang(:, 3).*cang(:, 1);
    //            ////	dcm(3, 3, :) = cang(:, 3).*cang(:, 2);

}
void RotMatrixTimesPoint3(float* R1, const Point3& P, Point3 & transformedPoint )
{
    transformedPoint.x=  R1[0]*P.x + R1[3]*P.y + R1[6]*P.z;
    transformedPoint.y=  R1[1]*P.x + R1[4]*P.y + R1[7]*P.z;
    transformedPoint.z=  R1[2]*P.x + R1[5]*P.y + R1[8]*P.z;
}

void RotMatrixTimesRotMatrix(float* R1, float* R2, float* R)
{
    //    R =std::vector<float>(9,0);
    //    R1 =
    //    [ X0, X3, X6]
    //    [ X1, X4, X7]
    //    [ X2, X5, X8]
    //    R2 =
    //    [ Y0, Y3, Y6]
    //    [ Y1, Y4, Y7]
    //    [ Y2, Y5, Y8]
    // R1*R2 =
    //    [ X0*Y0 + X3*Y1 + X6*Y2, X0*Y3 + X3*Y4 + X6*Y5, X0*Y6 + X3*Y7 + X6*Y8]
    //    [ X1*Y0 + X4*Y1 + X7*Y2, X1*Y3 + X4*Y4 + X7*Y5, X1*Y6 + X4*Y7 + X7*Y8]
    //    [ X2*Y0 + X5*Y1 + X8*Y2, X2*Y3 + X5*Y4 + X8*Y5, X2*Y6 + X5*Y7 + X8*Y8]
    //    if (R1.size()!=9 || R2.size()!=9 )
    //    {
    //        std::cout << "ERROR in Rotation matrix sizes" << std::endl;
    //         throw std::invalid_argument("ERROR in Rotation matrix sizes" );
    //    }
    R[0] = R1[0]*R2[0] + R1[3]*R2[1] + R1[6]*R2[2];
    R[1] = R1[1]*R2[0] + R1[4]*R2[1] + R1[7]*R2[2];
    R[2] = R1[2]*R2[0] + R1[5]*R2[1] + R1[8]*R2[2];
    R[3] = R1[0]*R2[3] + R1[3]*R2[4] + R1[6]*R2[5];
    R[4] = R1[1]*R2[3] + R1[4]*R2[4] + R1[7]*R2[5];
    R[5] = R1[2]*R2[3] + R1[5]*R2[4] + R1[8]*R2[5];
    R[6] = R1[0]*R2[6] + R1[3]*R2[7] + R1[6]*R2[8];
    R[7] = R1[1]*R2[6] + R1[4]*R2[7] + R1[7]*R2[8];
    R[8] = R1[2]*R2[6] + R1[5]*R2[7] + R1[8]*R2[8];

}


void interp3CPU(
        float * VqOutput,
        const int     nQueryPoints,
        const int     xSize,
        const int     ySize,
        const int     zSize,
        const float * gridX,
        const float * gridY,
        const float * gridZ,
        const float * vGrid,
        const float * xq,
        const float * yq,
        const float * zq)
{


    for (int idx = 0; idx < nQueryPoints; ++idx)

    {




        float x = xq[idx];
        float y = yq[idx];
        float z = zq[idx];




        if (x < gridX[0] || x > gridX[xSize - 1] ||
                y < gridY[0] || y > gridY[ySize - 1] ||
                z < gridZ[0] || z > gridZ[zSize - 1])
        {
            VqOutput[idx] = std::numeric_limits<float>::quiet_NaN();;
            continue;
        }

        float x0, y0, z0, x1, y1, z1;
        int ibx, itx, iby, ity, ibz, itz, im;

        ibx = 0;
        itx = xSize - 1;
        while (ibx < (itx - 1))
        {
            im = ((ibx + itx) >> 1);
            if (x <= gridX[im])
            {
                itx = im;
            }
            else
            {
                ibx = im;
            }
        }
        x0 = gridX[ibx];
        x1 = gridX[itx];

        iby = 0;
        ity = ySize - 1;
        while (iby < (ity - 1))
        {
            im = ((iby + ity) >> 1);
            if (y <= gridY[im])
            {
                ity = im;
            }
            else
            {
                iby = im;
            }
        }
        y0 = gridY[iby];
        y1 = gridY[ity];

        ibz = 0;
        itz = zSize - 1;
        while (ibz < (itz - 1))
        {
            im = ((ibz + itz) >> 1);
            if (z <= gridZ[im])
            {
                itz = im;
            }
            else
            {
                ibz = im;
            }
        }
        z0 = gridZ[ibz];
        z1 = gridZ[itz];

        int sliceDim = xSize * ySize;
        int zOff0 = sliceDim * ibz;
        int zOff1 = zOff0 + sliceDim;
        int yOff0 = ySize * ibx;
        int yOff1 = yOff0 + ySize;

        float ax0 = (x - x0) / (x1 - x0);
        float ay0 = (y - y0) / (y1 - y0);
        float az0 = (z - z0) / (z1 - z0);
        float ax1 = 1.0f - ax0;
        float ay1 = 1.0f - ay0;

        float v000 = vGrid[zOff0 + yOff0 + iby];
        float v001 = vGrid[zOff0 + yOff0 + ity];
        float v010 = vGrid[zOff0 + yOff1 + iby];
        float v011 = vGrid[zOff0 + yOff1 + ity];
        float v100 = vGrid[zOff1 + yOff0 + iby];
        float v101 = vGrid[zOff1 + yOff0 + ity];
        float v110 = vGrid[zOff1 + yOff1 + iby];
        float v111 = vGrid[zOff1 + yOff1 + ity];

        float v00 = v000 * ay1 + v001 * ay0;
        float v01 = v010 * ay1 + v011 * ay0;
        float v10 = v100 * ay1 + v101 * ay0;
        float v11 = v110 * ay1 + v111 * ay0;

        float v0 = v00 * ax1 + v01 * ax0;
        float v1 = v10 * ax1 + v11 * ax0;

        VqOutput[idx] = v0 * (1.0f - az0) + v1 * az0;
    }
} // interp3CPU


//bool pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
//{
//  int i, j;
//  bool c = false;
//  for (i = 0, j = nvert-1; i < nvert; j = i++) {
//    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
//     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
//       c = !c;
//  }
//  return c;
//}

void Sub2IndForColMajorTensor(std::vector<int> & indexVec, int sizeX, int sizeY, int sizeZ, std::vector<int> & SubX, std::vector<int> & SubY, std::vector<int> & SubZ)
{
    // index = k*sizeX*sizeY + j*sizeX + i; // col major , i =0:sizeX-1 , j=0:sizeY-1, k =0:sizeZ-1
    if (SubX.size()*SubY.size()*SubZ.size()==0)
    {
        std::cout << "ERROR:  At least one of the Sub indices vector (SubX/SubY/SubZ) has  zero size" << std::endl;
        return;
    }
    indexVec = std::vector<int>(SubX.size(),-1);

    if (sizeX*sizeY*sizeZ==0)
    {
        std::cout << "ERROR:  At least one of the size variables is zero" << std::endl;
        return;
    }
    for (int i = 0; i < indexVec.size(); ++i)
    {
        indexVec[i]=  SubZ[i]*sizeX*sizeY + SubY[i]*sizeX + SubX[i];
    }
    return;
}

void Sub2IndForRowMajorTensor(std::vector<int> & indexVec, int sizeX, int sizeY, int sizeZ, std::vector<int> & SubX, std::vector<int> & SubY, std::vector<int> & SubZ)
{
    //   index = k*sizeX*sizeY + i*sizeY + j; // row major   , i =0:sizeX-1 , j=0:sizeY-1, k =0:sizeZ-1

    if (SubX.size()*SubY.size()*SubZ.size()==0)
    {
        std::cout << "ERROR:  At least one of the Sub indices vector (SubX/SubY/SubZ) has  zero size" << std::endl;
        return;
    }
    indexVec = std::vector<int>(SubX.size(),-1);

    if (sizeX*sizeY*sizeZ==0)
    {
        std::cout << "ERROR:  At least one of the size variables is zero" << std::endl;
        return;
    }
    for (int i = 0; i < indexVec.size(); ++i)
    {
        indexVec[i]=  SubZ[i]*sizeX*sizeY + SubX[i]*sizeY + SubY[i];
    }
    return;
}
void ind2SubForColMajorTensor(std::vector<int> & indexVec, int sizeX, int sizeY, int sizeZ, std::vector<int> & SubX, std::vector<int> & SubY, std::vector<int> & SubZ)
{
    // index = k*sizeX*sizeY + j*sizeX + i; // col major , i =0:sizeX-1 , j=0:sizeY-1, k =0:sizeZ-1
    SubX = std::vector<int>(indexVec.size(),-1);
    SubY = std::vector<int>(indexVec.size(),-1);
    SubZ = std::vector<int>(indexVec.size(),-1);

    if (sizeX*sizeY*sizeZ==0)
    {
        std::cout << "ERROR:  At least one of the size variables is zero" << std::endl;
        return;
    }
    for (int i = 0; i < indexVec.size(); ++i)
    {
        SubZ[i]= indexVec[i]/(sizeX*sizeY);
        SubY[i]= (indexVec[i]%(sizeX*sizeY))/sizeX;
        SubX[i]= (indexVec[i]%(sizeX*sizeY))%sizeX;
    }
    return;
}

void ind2SubForRowMajorTensor(std::vector<int> & indexVec, int sizeX, int sizeY, int sizeZ, std::vector<int> & SubX, std::vector<int> & SubY, std::vector<int> & SubZ)
{
    //   index = k*sizeX*sizeY + i*sizeY + j; // row major   , i =0:sizeX-1 , j=0:sizeY-1, k =0:sizeZ-1
    SubX = std::vector<int>(indexVec.size(),-1);
    SubY = std::vector<int>(indexVec.size(),-1);
    SubZ = std::vector<int>(indexVec.size(),-1);

    if (sizeX*sizeY*sizeZ==0)
    {
        std::cout << "ERROR:  At least one of the size variables is zero" << std::endl;
        return;
    }
    for (int i = 0; i < indexVec.size(); ++i)
    {
        SubZ[i]= indexVec[i]/(sizeX*sizeY);
        SubX[i]= (indexVec[i]%(sizeX*sizeY))/sizeY;
        SubY[i]= (indexVec[i]%(sizeX*sizeY))%sizeY;
    }
    return;
}


std::vector<std::string> instersection(std::vector<std::string> &v1, std::vector<std::string> &v2)
{

    std::vector<std::string> v3;

    std::sort(v1.begin(), v1.end());
    std::sort(v2.begin(), v2.end());

    set_intersection(v1.begin(),v1.end(),v2.begin(),v2.end(),back_inserter(v3));

    return v3;
}


} // namespace ends


#endif // SOMEHELPERS

