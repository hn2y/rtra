
// Author: M.Fatyga
// Date Created: 03/14/2006
// This class is a placeholder
// for odd graphics utilities.

/**
   \class rcfGrphUtilMgr
   @ingroup utile

   History:
       January 18, 2008: CY added WarpVolumeTrilinear which will warp volume
                            according tri-linear interpolation.
                         CY added WarpVolumeNearestNeighbor which will warp
                            volume according to nearest neighbor
                         CY added WarpVolumeWeightedAverage which will warp
                            volume according to weighted average.
       February 03, 2008: CY added getTargetIndex which will return the targetIndex.
                            This is the index of the lower-left corner of the
                            voxel which contains targetPoint

       July 11, 2008: CY made three changes.
                          1. If the volume is uniform, then slicePosition is not
                             needed anymore.
                          2. All the variables inside the methods are promoted to
                             float to prevent underflow problem.
                          3. The user can turn extrapolation on or off. It it is
                             turned off, then any points outside the volume box will
                             not get any value. By default, it is turned off.
                          4. Change the slicePosition to slicePositionX, slicePositionY
                             and slicePositionZ to save memory space
*/

#ifndef __rcfGraphUtilMgr
#define __rcfGraphUtilMgr

#include <math.h>
#include <iostream>
#include "cfTypedefs.hpp"
#include "cfVolume3d.hpp"
#include "cfGenDefines.h"

class rcfGraphUtilMgr
{

   public:

      template <class C>
      int WarpVolumeTrilinear(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value);

      template <class C>
      int WarpVolumeTrilinear(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate);

      template <class C, class D>
      int WarpVolumeTrilinear(cfVolume3d<C,D> srcVolume, cfVolume3d<C,D>& tgtVolume);


      template <class C>
      int WarpVolumeNearestNeighbor(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value);

      template <class C>
      int WarpVolumeNearestNeighbor(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate);

      template <class C>
      int WarpVolumeWeightedAverage(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value);
      template <class C>
      int WarpVolume(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value,int select);

      template <class C>
      int WarpVolumeWeightedAverage(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate);



      int trilinear(cfPoint3D<float> *p, float *d, int xsize, int ysize, int zsize, float *dxyz);


   private:

      float errorMessage(int x0, int y0, int z0, int xsize, int ysize, int zsize);

      template <class C>
      int   getTargetIndex(cfVolume<C> *targetVolume, cfPoint3D<float> targetPoint, cfPoint3D<int> *targetIndex);

      template<class C, class D>
      D trilinearInterpolate(cfPoint3D<C> p0, cfPoint3D<C> p1, D v[2][2][2], cfPoint3D<C> p);
};

/**************************************************************
 *
 * Author: Mirek Fatyga
 * Created: June 26, 2008
 * This is a wrapper method, created to allow for more efficient
 * user code
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolume(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value,int select)
{
   switch(select)
   {
      case 0:
         return(WarpVolumeTrilinear(targetVolume,targetPoint,numberOfPoints,value));
         break;
      case 1:
         return(WarpVolumeNearestNeighbor(targetVolume,targetPoint,numberOfPoints,value));
         break;
      case 2:
         return(WarpVolumeWeightedAverage(targetVolume,targetPoint,numberOfPoints,value));
         break;
      default:
         std::cerr<<"Illegal method selection, no interpolation performed \n";
         return(CF_FAIL);
         break;
   }
}


/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 18, 2008
 * This method is trying to warp an array of points using tri
 * linear interpolation (assume the user do not want extrapolation
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           origin is the same as the targetVolume).
 *        numberOfPoints indicates how many points there are
 * Output: value is the interpolated value of targetPoint
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolumeTrilinear(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value)
{
  return WarpVolumeTrilinear(targetVolume, targetPoint, numberOfPoints, value, false);
}

/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 18, 2008
 * This method is trying to warp an array of points using nearest
 * neighborinterpolation (assume the user do not want extrapolation)
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           origin is the same as the targetVolume).
 *        numberOfPoints indicates how many points there are
 * Output: value is the interpolated value of targetPoint
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolumeWeightedAverage(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value)
{
  return WarpVolumeWeightedAverage(targetVolume, targetPoint, numberOfPoints, value, false);
}

/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 18, 2008
 * This method is trying to warp an array of points using nearest
 * neighborinterpolation (assume the user do not want extrapolation)
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           origin is the same as the targetVolume).
 *        numberOfPoints indicates how many points there are
 * Output: value is the interpolated value of targetPoint
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolumeNearestNeighbor(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value)
{
  return WarpVolumeNearestNeighbor(targetVolume, targetPoint, numberOfPoints, value, false);
}



/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 18, 2008
 * This method is trying to warp an array of points using tri
 * linear interpolation
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           same coordinate system of the targetVolume).
 *        numberOfPoints indicates how many points there are
 *        extrapolate indicates whether extrapolation should be
 *           done or not
 * Output: value is the interpolated value of targetPoint
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolumeTrilinear(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate)
{
   // sanity check
   if(NULL == targetVolume)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeTrilinear() targetVolume is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == targetPoint)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeTrilinear() targetPoint is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == value)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeTrilinear() value is NULL.\n");
     return CF_FAIL;
   }

   // retrieve boundary information of volume
   cfBox<float>* volumeBoundary = new cfBox<float>();
   volumeBoundary->min.x = targetVolume->volume.origin.x;
   volumeBoundary->min.y = targetVolume->volume.origin.y;
   volumeBoundary->min.z = targetVolume->volume.origin.z;

   // int targetVoxelIndex = targetVolume->volume.nElements.x * targetVolume->volume.nElements.y * targetVolume->volume.nElements.z - 1;
   if(targetVolume->volume.isUniform.x){
     volumeBoundary->max.x = targetVolume->volume.origin.x + targetVolume->volume.voxelSize.x*(targetVolume->volume.nElements.x - 1);
   }else{
     volumeBoundary->max.x = targetVolume->volume.slicePositionX[targetVolume->volume.nElements.x-1];
   }

   if(targetVolume->volume.isUniform.y){
     volumeBoundary->max.y = targetVolume->volume.origin.y + targetVolume->volume.voxelSize.y*(targetVolume->volume.nElements.y - 1);
   }else{
     volumeBoundary->max.y = targetVolume->volume.slicePositionY[targetVolume->volume.nElements.y-1];
   }

   if(targetVolume->volume.isUniform.z){
     volumeBoundary->max.z = targetVolume->volume.origin.z + targetVolume->volume.voxelSize.z*(targetVolume->volume.nElements.z - 1);
   }else{
     volumeBoundary->max.z = targetVolume->volume.slicePositionZ[targetVolume->volume.nElements.z-1];
   }

   // iterate every targetPoint
   int pointItr;
   for(pointItr=0; pointItr < numberOfPoints; pointItr++){

     // calculate the x, y, z, indices
     bool inTargetBox;
     inTargetBox = (targetPoint[pointItr].x >= volumeBoundary->min.x) && (targetPoint[pointItr].x <= volumeBoundary->max.x)
                && (targetPoint[pointItr].y >= volumeBoundary->min.y) && (targetPoint[pointItr].y <= volumeBoundary->max.y)
                && (targetPoint[pointItr].z >= volumeBoundary->min.z) && (targetPoint[pointItr].z <= volumeBoundary->max.z);

     // trying to get targetIndex
     cfPoint3D<int> targetIndex;
     if(CF_OK != getTargetIndex(targetVolume, targetPoint[pointItr], &targetIndex))
     {
       fprintf(stderr, "ERROR: rcfGraphUtilMgr::WarpVolumeTrilinear() Failed to get targetIndex.\n");
       return CF_FAIL;
     }

     //if the user require to do extrapolation, then all the points (inside and outside image box) will
     // be calculated.
     if(extrapolate)
     {
         inTargetBox = true;
     }

     // first check whether the point is in the target box, then do tri-linear interpolation
     if(inTargetBox)
     {
       // get the voxel size in x-y-z directions
       cfPoint3D<float> voxelSize;
       if(targetVolume->volume.isUniform.x){
	 voxelSize.x = targetVolume->volume.voxelSize.x;
       }else{
         voxelSize.x = targetVolume->volume.slicePositionX[targetIndex.x + 1] - targetVolume->volume.slicePositionX[targetIndex.x];
       }

       if(targetVolume->volume.isUniform.y){
	 voxelSize.y = targetVolume->volume.voxelSize.y;
       }else{
         voxelSize.y = targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetVolume->volume.slicePositionY[targetIndex.y];
       }

       if(targetVolume->volume.isUniform.z){
	  voxelSize.z = targetVolume->volume.voxelSize.z;
       }else{
         voxelSize.z = targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetVolume->volume.slicePositionZ[targetIndex.z];
       }

       // cout<<"voxelSize (x, y, z) "<<voxelSize.x<<" "<<voxelSize.y<<" "<<voxelSize.z<<"\n";

       // get the value of the eight grid points of the box which contains the targetPoint[pointItr]
       int voxelIndex[8];
       float   voxelValue[8]; // promote to float to avoid roundoff problem
       voxelIndex[0] = targetIndex.z * targetVolume->volume.nElements.x * targetVolume->volume.nElements.y
                     + targetIndex.y * targetVolume->volume.nElements.x
                     + targetIndex.x;
       voxelIndex[1] = voxelIndex[0] + targetVolume->volume.nElements.x;
       voxelIndex[2] = voxelIndex[1] + 1;
       voxelIndex[3] = voxelIndex[0] + 1;

       voxelIndex[4] = voxelIndex[0] + targetVolume->volume.nElements.x * targetVolume->volume.nElements.y;
       voxelIndex[5] = voxelIndex[4] + targetVolume->volume.nElements.x;
       voxelIndex[6] = voxelIndex[5] + 1;
       voxelIndex[7] = voxelIndex[4] + 1;

       voxelValue[0] = targetVolume->value[voxelIndex[0]];
       voxelValue[1] = targetVolume->value[voxelIndex[1]];
       voxelValue[2] = targetVolume->value[voxelIndex[2]];
       voxelValue[3] = targetVolume->value[voxelIndex[3]];

       voxelValue[4] = targetVolume->value[voxelIndex[4]];
       voxelValue[5] = targetVolume->value[voxelIndex[5]];
       voxelValue[6] = targetVolume->value[voxelIndex[6]];
       voxelValue[7] = targetVolume->value[voxelIndex[7]];

       // interpolate along z direction
       float zValue[4];
       if(!targetVolume->volume.isUniform.z){ //if the targetVolume is not uniform along z-axis

          zValue[0] = (voxelValue[4]*(targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[targetIndex.z])
                    +  voxelValue[0]*(targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetPoint[pointItr].z))/voxelSize.z;

          zValue[1] = (voxelValue[5]*(targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[targetIndex.z])
	            +  voxelValue[1]*(targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetPoint[pointItr].z))/voxelSize.z;

          zValue[2] = (voxelValue[6]*(targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[targetIndex.z])
	            +  voxelValue[2]*(targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetPoint[pointItr].z))/voxelSize.z;

          zValue[3] = (voxelValue[7]*(targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[targetIndex.z])
	            +  voxelValue[3]*(targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetPoint[pointItr].z))/voxelSize.z;

       }else{ // if it is uniform along z-axis

	  zValue[0] = (voxelValue[4]*(targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetIndex.z * voxelSize.z))
                    +  voxelValue[0]*(targetVolume->volume.origin.z + (targetIndex.z + 1) * voxelSize.z - targetPoint[pointItr].z))/voxelSize.z;

          zValue[1] = (voxelValue[5]*(targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetIndex.z * voxelSize.z))
	            +  voxelValue[1]*(targetVolume->volume.origin.z + (targetIndex.z + 1) * voxelSize.z - targetPoint[pointItr].z))/voxelSize.z;

          zValue[2] = (voxelValue[6]*(targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetIndex.z * voxelSize.z))
	            +  voxelValue[2]*(targetVolume->volume.origin.z + (targetIndex.z + 1) * voxelSize.z- targetPoint[pointItr].z))/voxelSize.z;

          zValue[3] = (voxelValue[7]*(targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetIndex.z * voxelSize.z))
	            +  voxelValue[3]*(targetVolume->volume.origin.z + (targetIndex.z + 1) * voxelSize.z - targetPoint[pointItr].z))/voxelSize.z;
       }

       // interpolate along y direction
       float yValue[2];
       if(!targetVolume->volume.isUniform.y){ //if targetVolume is not uniform along y-axis

          yValue[0] = (zValue[1]*(targetPoint[pointItr].y - targetVolume->volume.slicePositionY[targetIndex.y])
	            +  zValue[0]*(targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetPoint[pointItr].y))/voxelSize.y;

          yValue[1] = (zValue[2]*(targetPoint[pointItr].y - targetVolume->volume.slicePositionY[targetIndex.y])
	            +  zValue[3]*(targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetPoint[pointItr].y))/voxelSize.y;

       }else{ // if targetVolume

          yValue[0] = (zValue[1]*(targetPoint[pointItr].y - (targetVolume->volume.origin.y + targetIndex.y * voxelSize.y))
	            +  zValue[0]*(targetVolume->volume.origin.y + (targetIndex.y + 1) * voxelSize.y - targetPoint[pointItr].y))/voxelSize.y;

          yValue[1] = (zValue[2]*(targetPoint[pointItr].y - (targetVolume->volume.origin.y + targetIndex.y * voxelSize.y))
	            +  zValue[3]*(targetVolume->volume.origin.y + (targetIndex.y + 1) * voxelSize.y - targetPoint[pointItr].y))/voxelSize.y;

       }

       // interpolate along x direction
       if(!targetVolume->volume.isUniform.x){ //if targetVolume is not uniform along x-axis

          value[pointItr] = (C)((yValue[1]*(targetPoint[pointItr].x - targetVolume->volume.slicePositionX[targetIndex.x])
	                  +  yValue[0]*(targetVolume->volume.slicePositionX[targetIndex.x + 1] - targetPoint[pointItr].x))/voxelSize.x);

       }else{ //if targetVolume is uniform

          value[pointItr] = (C)((yValue[1]*(targetPoint[pointItr].x - (targetVolume->volume.origin.x + targetIndex.x * voxelSize.x))
	                  +  yValue[0]*(targetVolume->volume.origin.x + (targetIndex.x + 1) * voxelSize.x - targetPoint[pointItr].x))/voxelSize.x);

       }

     }//if(inTargetBox

   }//for(pointItr=0

   if( NULL != volumeBoundary )
   {
       delete volumeBoundary;
       volumeBoundary = NULL;
   }

   return CF_OK;
}


/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 18, 2008
 * This method is trying to warp an array of points using nearest
 * neighbor search
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           same coordinate system of the targetVolume).
 *        numberOfPoints indicates how many points there are
 * Output: value are the interpolated value of targetPoint
 *
 *************************************************************/
template <class C>
int rcfGraphUtilMgr::WarpVolumeNearestNeighbor(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate)
{
   // sanity check
   if(NULL == targetVolume)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeNearestNeighbor() targetVolume is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == targetPoint)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeNearestNeighbor() targetPoint is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == value)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeNearestNeighbor() value is NULL.\n");
     return CF_FAIL;
   }

   // retrieve boundary information of volume
   cfBox<float>* volumeBoundary;
   volumeBoundary = new cfBox<float>();
   volumeBoundary->min.x = targetVolume->volume.origin.x;
   volumeBoundary->min.y = targetVolume->volume.origin.y;
   volumeBoundary->min.z = targetVolume->volume.origin.z;

   if(targetVolume->volume.isUniform.x){
     volumeBoundary->max.x = targetVolume->volume.origin.x + targetVolume->volume.voxelSize.x*(targetVolume->volume.nElements.x - 1);
   }else{
     volumeBoundary->max.x = targetVolume->volume.slicePositionX[targetVolume->volume.nElements.x - 1];
   }

   if(targetVolume->volume.isUniform.y){
     volumeBoundary->max.y = targetVolume->volume.origin.y + targetVolume->volume.voxelSize.y*(targetVolume->volume.nElements.y - 1);
   }else{
     volumeBoundary->max.y = targetVolume->volume.slicePositionY[targetVolume->volume.nElements.y - 1];
   }

   if(targetVolume->volume.isUniform.z){
     volumeBoundary->max.z = targetVolume->volume.origin.z + targetVolume->volume.voxelSize.z*(targetVolume->volume.nElements.z - 1);
   }else{
     volumeBoundary->max.z = targetVolume->volume.slicePositionZ[targetVolume->volume.nElements.z - 1];
   }

   // iterate every targetPoint
   int pointItr;
   for(pointItr=0; pointItr < numberOfPoints; pointItr++){
     // calculate the x, y, z, indices
     cfPoint3D<int> targetIndex;
     bool inTargetBox;
     inTargetBox = (targetPoint[pointItr].x >= volumeBoundary->min.x) && (targetPoint[pointItr].x <= volumeBoundary->max.x)
                && (targetPoint[pointItr].y >= volumeBoundary->min.y) && (targetPoint[pointItr].y <= volumeBoundary->max.y)
                && (targetPoint[pointItr].z >= volumeBoundary->min.z) && (targetPoint[pointItr].z <= volumeBoundary->max.z);

     // get targetIndex
     if(CF_OK != getTargetIndex(targetVolume, targetPoint[pointItr], &targetIndex))
     {
       fprintf(stderr, "ERROR: rcfGraphUtilMgr::WarpVolumeNearestNeighbor() Failed to get targetIndex.\n");
       return CF_FAIL;
     }

     if(extrapolate)inTargetBox = true;

     // if targetPoint[pointItr] is in the target box, then find its nearest neighbor
     if(inTargetBox)
     {
       // calculate the distance to the corner of each voxel
       // float distance[8];
       float totalDifference, minDifference = 1.0E+10;
       cfPoint3D<float> difference;
       cfPoint3D<int> indexItr;
       int totalIndex;
       for(indexItr.z = targetIndex.z; indexItr.z <= targetIndex.z + 1; indexItr.z++){
         for(indexItr.y = targetIndex.y; indexItr.y <= targetIndex.y + 1; indexItr.y++){
           for(indexItr.x = targetIndex.x; indexItr.x <= targetIndex.x + 1; indexItr.x++){

             if(targetVolume->volume.isUniform.x){
               difference.x = targetPoint[pointItr].x - (targetVolume->volume.origin.x + targetVolume->volume.voxelSize.x * indexItr.x);
	     }else{
               difference.x = targetPoint[pointItr].x - targetVolume->volume.slicePositionX[indexItr.x];
	     }
             if(targetVolume->volume.isUniform.y){
               difference.y = targetPoint[pointItr].y - (targetVolume->volume.origin.y + targetVolume->volume.voxelSize.y * indexItr.y);
	     }else{
               difference.y = targetPoint[pointItr].y - targetVolume->volume.slicePositionY[indexItr.y];
	     }
             if(targetVolume->volume.isUniform.z){
               difference.z = targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetVolume->volume.voxelSize.z * indexItr.z);
	     }else{
               difference.z = targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[indexItr.z];
	     }

             totalDifference = difference.x * difference.x + difference.y * difference.y + difference.z * difference.z;

             if(minDifference > totalDifference){
               totalIndex = indexItr.z * targetVolume->volume.nElements.x * targetVolume->volume.nElements.y
	                  + indexItr.y * targetVolume->volume.nElements.x
                          + indexItr.x;
               minDifference = totalDifference;
               value[pointItr] = targetVolume->value[totalIndex];
             }


	   }//for(indexItr.x = targetIndex.x;
	 }//for(indexItr.y = targetIndex.y;
       }//for(indexItr.z = targetIndex.z;

     }//if(inTargetBox

   }//for(pointItr=0

   return CF_OK;
}


/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: January 24, 2008
 * This method is trying to warp an array of points using weighted
 * average
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint are the points located in targetVolume. All
 *           the points are in the absolute coordinate system (the
 *           same coordinate system of the targetVolume).
 *        numberOfPoints indicates how many points there are
 * Output: value are the interpolated value of targetPoint
 *
 *************************************************************/
template<class C>
int rcfGraphUtilMgr::WarpVolumeWeightedAverage(cfVolume<C> *targetVolume, cfPoint3D<float> *targetPoint, int numberOfPoints, C *value, bool extrapolate)
{
   // sanity check
   if(NULL == targetVolume)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeWeightedAverage() targetVolume is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == targetPoint)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeWeightedAverage() targetPoint is NULL.\n");
     return CF_FAIL;
   }

   if(NULL == value)
   {
     fprintf(stderr, "ERROR rcfGraphUtilMgr::WarpVolumeWeightedAverage() value is NULL.\n");
     return CF_FAIL;
   }

   // retrieve boundary information of volume
   cfBox<float>* volumeBoundary = new cfBox<float>();
   volumeBoundary->min.x = targetVolume->volume.origin.x;
   volumeBoundary->min.y = targetVolume->volume.origin.y;
   volumeBoundary->min.z = targetVolume->volume.origin.z;

   if(targetVolume->volume.isUniform.x){
     volumeBoundary->max.x = targetVolume->volume.origin.x + targetVolume->volume.voxelSize.x*(targetVolume->volume.nElements.x - 1);
   }else{
     volumeBoundary->max.x = targetVolume->volume.slicePositionX[targetVolume->volume.nElements.x - 1];
   }

   if(targetVolume->volume.isUniform.y){
     volumeBoundary->max.y = targetVolume->volume.origin.y + targetVolume->volume.voxelSize.y*(targetVolume->volume.nElements.y - 1);
   }else{
     volumeBoundary->max.y = targetVolume->volume.slicePositionY[targetVolume->volume.nElements.y - 1];
   }

   if(targetVolume->volume.isUniform.z){
     volumeBoundary->max.z = targetVolume->volume.origin.z + targetVolume->volume.voxelSize.z*(targetVolume->volume.nElements.z - 1);
   }else{
     volumeBoundary->max.z = targetVolume->volume.slicePositionZ[targetVolume->volume.nElements.z - 1];
   }

   // iterate every targetPoint
   int pointItr;
   for(pointItr=0; pointItr < numberOfPoints; pointItr++){
     // calculate the x, y, z, indices
     cfPoint3D<int> targetIndex;
     bool inTargetBox;
     inTargetBox = (targetPoint[pointItr].x >= volumeBoundary->min.x) && (targetPoint[pointItr].x <= volumeBoundary->max.x)
                && (targetPoint[pointItr].y >= volumeBoundary->min.y) && (targetPoint[pointItr].y <= volumeBoundary->max.y)
                && (targetPoint[pointItr].z >= volumeBoundary->min.z) && (targetPoint[pointItr].z <= volumeBoundary->max.z);

     // get targetIndex
     if(CF_OK != getTargetIndex(targetVolume, targetPoint[pointItr], &targetIndex))
     {
       fprintf(stderr, "ERROR: rcfGraphUtilMgr::WarpVolumeNearestNeighbor() Failed to get targetIndex.\n");
       return CF_FAIL;
     }

     // if user want to calculate points outside the volume box, then always set
     // the extrapolate to be true.
     if(extrapolate) inTargetBox = true;

     // make sure it is not a boundary box, so the seven point are well-defined
     bool notBoundaryVoxel = (targetIndex.x > 0) && (targetIndex.x < targetVolume->volume.nElements.x - 1)
                          && (targetIndex.y > 0) && (targetIndex.y < targetVolume->volume.nElements.y - 1)
                          && (targetIndex.z > 0) && (targetIndex.z < targetVolume->volume.nElements.z - 1);

     if(inTargetBox)
     {

       int voxelIndex[7];
       float   voxelValue[7];
       voxelIndex[0] = targetIndex.z * targetVolume->volume.nElements.x * targetVolume->volume.nElements.y
                     + targetIndex.y * targetVolume->volume.nElements.x
                     + (targetIndex.x - 1);
       if(notBoundaryVoxel){
         voxelIndex[1] = voxelIndex[0] + 1;
         voxelIndex[2] = voxelIndex[1] + 1;

         voxelIndex[3] = voxelIndex[1] - targetVolume->volume.nElements.x;
         voxelIndex[4] = voxelIndex[1] + targetVolume->volume.nElements.x;

         voxelIndex[5] = voxelIndex[1] - targetVolume->volume.nElements.x * targetVolume->volume.nElements.y;
         voxelIndex[6] = voxelIndex[1] + targetVolume->volume.nElements.x * targetVolume->volume.nElements.y;

         voxelValue[0] = targetVolume->value[voxelIndex[0]];
         voxelValue[1] = targetVolume->value[voxelIndex[1]];
         voxelValue[2] = targetVolume->value[voxelIndex[2]];

         voxelValue[3] = targetVolume->value[voxelIndex[3]];
         voxelValue[4] = targetVolume->value[voxelIndex[4]];

         voxelValue[5] = targetVolume->value[voxelIndex[5]];
         voxelValue[6] = targetVolume->value[voxelIndex[6]];

         cfPoint3D<float> averageValue;

         // calculate weighted average along x-axis
         float omega[2];
         if(targetVolume->volume.isUniform.x){

	   omega[0] = fabs((targetVolume->volume.voxelSize.x * (targetIndex.x + 1) - targetVolume->volume.voxelSize.x * targetIndex.x)
                          /(targetPoint[pointItr].x - (targetVolume->volume.origin.x + targetVolume->volume.voxelSize.x * targetIndex.x)));
           omega[1] = fabs((targetVolume->volume.voxelSize.x * (targetIndex.x + 1) - targetVolume->volume.voxelSize.x * targetIndex.x)
                          /(targetVolume->volume.voxelSize.x * (targetIndex.x + 1) + targetVolume->volume.origin.x - targetPoint[pointItr].x));
         }else{

	   omega[0] = fabs((targetVolume->volume.slicePositionX[targetIndex.x + 1] - targetVolume->volume.slicePositionX[targetIndex.x])
                          /(targetPoint[pointItr].x - targetVolume->volume.slicePositionX[targetIndex.x]));
           omega[1] = fabs((targetVolume->volume.slicePositionX[targetIndex.x + 1] - targetVolume->volume.slicePositionX[targetIndex.x])
                          /(targetVolume->volume.slicePositionX[targetIndex.x + 1] - targetPoint[pointItr].x));

           //omega[0] = fabs((targetVolume->volume.slicePosition[voxelIndex[2]].x - targetVolume->volume.slicePosition[voxelIndex[1]].x)
           //               /(targetPoint[pointItr].x - targetVolume->volume.slicePosition[voxelIndex[1]].x));
           //omega[1] = fabs((targetVolume->volume.slicePosition[voxelIndex[2]].x - targetVolume->volume.slicePosition[voxelIndex[1]].x)
           //               /(targetVolume->volume.slicePosition[voxelIndex[2]].x - targetPoint[pointItr].x));
         }

	 averageValue.x = (omega[0]*(voxelValue[1] + voxelValue[0])*0.5 + omega[1]*(voxelValue[1] + voxelValue[2])*0.5)/(omega[0]+omega[1]);

         // calculate weighted average along y-axis (note targetVolume->volume.origin cancels sometime)
         if(targetVolume->volume.isUniform.y){

           omega[0] = fabs((targetVolume->volume.voxelSize.y * (targetIndex.y + 1) - targetVolume->volume.voxelSize.y * targetIndex.y)
                          /(targetPoint[pointItr].y - (targetVolume->volume.origin.y + targetVolume->volume.voxelSize.y * targetIndex.y)));
           omega[1] = fabs((targetVolume->volume.voxelSize.y * (targetIndex.y + 1) - targetVolume->volume.voxelSize.y * targetIndex.y)
                          /(targetVolume->volume.voxelSize.y * (targetIndex.y + 1) + targetVolume->volume.origin.y - targetPoint[pointItr].y));
         }else{

           omega[0] = fabs((targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetVolume->volume.slicePositionY[targetIndex.y])
                          /(targetPoint[pointItr].y - targetVolume->volume.slicePositionY[targetIndex.y]));
           omega[1] = fabs((targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetVolume->volume.slicePositionY[targetIndex.y])
                          /(targetVolume->volume.slicePositionY[targetIndex.y + 1] - targetPoint[pointItr].y));

           //omega[0] = fabs((targetVolume->volume.slicePosition[voxelIndex[4]].y - targetVolume->volume.slicePosition[voxelIndex[1]].y)
           //               /(targetPoint[pointItr].y - targetVolume->volume.slicePosition[voxelIndex[1]].y));
           //omega[1] = fabs((targetVolume->volume.slicePosition[voxelIndex[4]].y - targetVolume->volume.slicePosition[voxelIndex[1]].y)
           //               /(targetVolume->volume.slicePosition[voxelIndex[4]].y - targetPoint[pointItr].y));
         }

	 averageValue.y = (omega[0]*(voxelValue[1] + voxelValue[3])*0.5 + omega[1]*(voxelValue[1] + voxelValue[4])*0.5)/(omega[0]+omega[1]);

         // calculate weighted average along z-axis
         if(targetVolume->volume.isUniform.z){

           omega[0] = fabs((targetVolume->volume.voxelSize.z * (targetIndex.z + 1) - targetVolume->volume.voxelSize.z * targetIndex.z)
                          /(targetPoint[pointItr].z - (targetVolume->volume.origin.z + targetVolume->volume.voxelSize.z * targetIndex.z)));
           omega[1] = fabs((targetVolume->volume.voxelSize.z * (targetIndex.z + 1) - targetVolume->volume.voxelSize.z * targetIndex.z)
                          /(targetVolume->volume.voxelSize.z * (targetIndex.z + 1) + targetVolume->volume.origin.z - targetPoint[pointItr].z));

         }else{

           omega[0] = fabs((targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetVolume->volume.slicePositionZ[targetIndex.z])
                          /(targetPoint[pointItr].z - targetVolume->volume.slicePositionZ[targetIndex.z]));
           omega[1] = fabs((targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetVolume->volume.slicePositionZ[targetIndex.z])
			  /(targetVolume->volume.slicePositionZ[targetIndex.z + 1] - targetPoint[pointItr].z));


	   //omega[0] = fabs((targetVolume->volume.slicePosition[voxelIndex[6]].z - targetVolume->volume.slicePosition[voxelIndex[1]].z)
           //               /(targetPoint[pointItr].z - targetVolume->volume.slicePosition[voxelIndex[1]].z));
           //omega[1] = fabs((targetVolume->volume.slicePosition[voxelIndex[6]].z - targetVolume->volume.slicePosition[voxelIndex[1]].z)
           //               /(targetVolume->volume.slicePosition[voxelIndex[6]].z - targetPoint[pointItr].z));
         }

	 averageValue.z = (omega[0]*(voxelValue[1] + voxelValue[5])*0.5 + omega[1]*(voxelValue[1] + voxelValue[6])*0.5)/(omega[0]+omega[1]);

         value[pointItr] = (C)((averageValue.x + averageValue.y + averageValue.z)*0.333333);

       }else{//if(notBoundaryVoxel)

         value[pointItr] = targetVolume->value[voxelIndex[1]];
       } // if(notBoundaryVoxel){ }else{


     }//if(inTargetBox

   }//for(pointItr=0

   return CF_OK;
}


/**************************************************************
 *
 * Author: Chenyu Yan
 * Created: February 3, 2008
 * This method is trying to warp an array of points using weighted
 * average
 *
 * Input: targetVolume is the object (image, dose etc.) that we
 *           want to warp.
 *        targetPoint is the point location in targetVolume
 * Output:targetIndex is the index of the lower-left grid point of
 *           the box in targetVolume which contains targetPoint
 *
 *************************************************************/
template<class C>
int rcfGraphUtilMgr::getTargetIndex(cfVolume<C> *targetVolume, cfPoint3D<float> targetPoint, cfPoint3D<int> *targetIndex)
{
     // sanity check
     if(NULL == targetVolume)
     {
       std::cerr << "ERROR: rcfGraphUtilMgr::getTargetIndex() targetVolume is NULL." << std::endl;
       return CF_FAIL;
     }

     if(NULL == targetIndex)
     {
       std::cerr << "ERROR: rcfGraphUtilMgr::getTargetIndex() targetIndex is NULL." << std::endl;
       return CF_FAIL;
     }

     // variables needed by binary search
     cfPoint3D<int> upperIndex, lowerIndex, midIndex;

     // initialization to all the variables defined above
     upperIndex.x = 0;
     upperIndex.y = 0;
     upperIndex.z = 0;

     lowerIndex.x = 0;
     lowerIndex.y = 0;
     lowerIndex.z = 0;

     midIndex.x = 0;
     midIndex.y = 0;
     midIndex.z = 0;

     // calculate x index
     if(targetVolume->volume.isUniform.x)
     {
       targetIndex->x = (int)floor((targetPoint.x - targetVolume->volume.origin.x)/targetVolume->volume.voxelSize.x);
     }
     else
     {
       // perform a binary search along
       upperIndex.x = targetVolume->volume.nElements.x;
       lowerIndex.x = 0;
       midIndex.x = (int)floor((upperIndex.x + lowerIndex.x)*0.5);
       while(upperIndex.x > (lowerIndex.x+1))
       {

            if(targetVolume->volume.slicePositionX[midIndex.x] >= targetPoint.x)
            {
                upperIndex.x = midIndex.x;
            }
            else
            {
                lowerIndex.x = midIndex.x;
            }
            midIndex.x = (int)floor((upperIndex.x + lowerIndex.x)*0.5);
        }
        targetIndex->x = lowerIndex.x;
    }
     midIndex.x = lowerIndex.x;

     // calculate y index
     if(targetVolume->volume.isUniform.y)
     {
       targetIndex->y = (int)floor((targetPoint.y - targetVolume->volume.origin.y)/targetVolume->volume.voxelSize.y);
     }else{
       // perform a binary search
       upperIndex.y = targetVolume->volume.nElements.y;
       lowerIndex.y = 0;
       midIndex.y = (int)floor((upperIndex.y + lowerIndex.y)*0.5);
       while(upperIndex.y > (lowerIndex.y+1)){

	  if(targetVolume->volume.slicePositionY[midIndex.y] >= targetPoint.y)
          {
	     upperIndex.y = midIndex.y;
          }else{
             lowerIndex.y = midIndex.y;
          }
          midIndex.y = (int)floor((upperIndex.y + lowerIndex.y)*0.5);
       }
       targetIndex->y = lowerIndex.y;
     }
     midIndex.y = lowerIndex.y;

     // calculate z index
     if(targetVolume->volume.isUniform.z)
     {
       targetIndex->z = (int)floor((targetPoint.z - targetVolume->volume.origin.z)/targetVolume->volume.voxelSize.z);
     }else{
       // performa a binary search
       upperIndex.z = targetVolume->volume.nElements.z;
       lowerIndex.z = 0;
       midIndex.z = (int)floor((upperIndex.z + lowerIndex.z)*0.5);
       while(upperIndex.z > (lowerIndex.z+1))
       {
	  if(targetVolume->volume.slicePositionZ[midIndex.z] >= targetPoint.z)
          {
	     upperIndex.z = midIndex.z;
          }else{
             lowerIndex.z = midIndex.z;
          }
          midIndex.z = (int)floor((upperIndex.z + lowerIndex.z)*0.5);
       }
       targetIndex->z = lowerIndex.z;
     }

     // prevent underflow
     if(targetIndex->x < 0)
     {
         targetIndex->x = 0;
     }

     if(targetIndex->y < 0)
     {
         targetIndex->y = 0;
     }

     if(targetIndex->z < 0)
     {
         targetIndex->z = 0;
     }

     // prevent overflow
     if((unsigned int)targetIndex->x > (targetVolume->volume.nElements.x-2))
     {
         targetIndex->x = targetVolume->volume.nElements.x - 2;
     }

     if((unsigned int)targetIndex->y > (targetVolume->volume.nElements.y-2))
     {
         targetIndex->y = targetVolume->volume.nElements.y - 2;
     }

     if((unsigned int)targetIndex->z > (targetVolume->volume.nElements.z-2))
     {
         targetIndex->z = targetVolume->volume.nElements.z - 2;
     }

     return CF_OK;
}


#endif
