#ifndef GUMMODEL_H
#define GUMMODEL_H
#include <iostream>
#include "Utilities/someHelpers.h"
enum ProbabilityDensityFunction {Gaussian,Uniform}; // TODO : include this as well
// enum RotationCenterType { ROICenterOfMass};
//enum  RotationCenterType{COM};

struct RotationCenter{
    enum {ROICenterOfMass} RotationCenterType;
   std::string RoiIDForRotationAroundCOM;
};


class GumModel
{
public:
    GumModel() { }
    virtual ~GumModel() { }

    std::string  associatedRoiID;
    std::string  guModelType;
    std::string  guModelFile;


};

#endif // GUMMODEL_H
