//  HN
// This code is taken from COP code at UVA written by JJG

#include "GuiException.h"

// define extractor for std::ostream
std::ostream& operator<<(std::ostream& stream, const GuiException& e) {
    stream << e.what();
    return stream;
}
