#include "cfClif.hpp"
#include "cfClifArray.hpp"
#include "cfClifElement.hpp"
#include "cfClifReader.hpp"
#include "cfGenDefines.h" // FIXME - make class less dependent on these headers
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;
using namespace clif_element;

const string TAB = "    ";

cfClif::cfClif()
{
    parent = NULL;
    tripleList = NULL;
    root = this;
}

cfClif::~cfClif()
{
    // FIXME - should this use data.clear() instead?   
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( NULL != data[i] )
        {            
            delete data[i];
            data[i] = NULL;
        }
    }
    data.clear();
    
    for( unsigned int i = 0; i < arrays.size(); i++ )
    {
        if( NULL != arrays[i] )
        {
            delete arrays[i];
            arrays[i] = NULL;
        }
    }
    arrays.clear();

    //FIXME - might need to add method to track attached pointers
    for( unsigned int i = 0; i < clifList.size(); i++ )
    {
        if( NULL != clifList[i] )
        {
            delete clifList[i];
            clifList[i] = NULL;
        }
    }
    clifList.clear();
    parent = NULL;
    root = NULL;
}
void cfClif::AddElement( cfClifElement* element )
{    
    cfClifElement* tmp = new cfClifElement();
    *tmp = *element;
    data.push_back(tmp);
}

cfClif& cfClif::operator=(const cfClif& clif)
{
    if( this == &clif )
    {
        return *this;
    }
    else if( NULL == &clif )
    {
        cout << "ERROR:cfRoi2::operator=: clif image is NULL" << endl;
        return *this;
    }
    
    handle = clif.handle;
 
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( NULL != data[i])
        {
            delete data[i];
        }
    }
    data.clear();

    for( unsigned int i = 0; i < clif.data.size(); i++ )
    {
        AddElement(clif.data[i]);
    }

    for( unsigned int i = 0; i < arrays.size(); i++ )
    {
        if( NULL != arrays[i] )
        {
            delete arrays[i];
            arrays[i] = NULL;
        }
    }
    arrays.clear();

    for( unsigned int i = 0; i < clif.arrays.size(); i++ )
    {
        AddArray(clif.arrays[i]);
    }

    for( unsigned int i = 0; i < clifList.size(); i++ )
    {
        if( NULL != clifList[i] )
        {
            delete clifList[i];
            clifList[i] = NULL;
        }
    }
    clifList.clear();

    for( unsigned int i = 0; i < clif.clifList.size(); i++ )
    {
        cfClif* tmp = new cfClif();
        *tmp = *clif.clifList[i];
        AddClif(tmp);
    }
    
    return *this;
}

cfClifElement* cfClif::GetClifElement(int index )
{
    return data[index];
}

cfClifElement* cfClif::GetClifElement(std::string handle )
{
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( 0 == data[i]->GetHandle().compare(handle) )
        {
            return data[i];
        }
    }
    return NULL;
}

int cfClif::GetNumberOfLevelsFromRootClif()
{
    int numberOfLevels = 0;
    GetNumberOfLevelsFromRootClifRecursive(numberOfLevels);
    return numberOfLevels;
}

void cfClif::GetNumberOfLevelsFromRootClifRecursive(int& level)
{
    if( this == root )
    {
        return;
    }
    else
    {
        if( NULL == parent )
        {
            cout << "ERROR: This is not the top level but the parent clif is NULL" << endl;
            return;
        }
        level++;
        parent->GetNumberOfLevelsFromRootClifRecursive(level);
    }
}

void cfClif::AddClif( cfClif* clif )
{
    clif->parent = this;
    clif->root = this->root;
    
    // recursively set child clifs' root to new root clif
    // ADD HERE... FIXME
    
    clifList.push_back( clif );
}

void cfClif::AddClif( cfClif& clif )
{
    cfClif* tmp = new cfClif();
    *tmp = clif;
    AddClif(tmp);
}

void cfClif::AddArray( cfClifArray* array )
{
    arrays.push_back( array );
}

void cfClif::RemoveClif( unsigned int index )
{
    clifList.erase(clifList.begin() + index, clifList.begin() + index + 1);
}
    
void cfClif::RemoveClif( string clifHandle )
{
    for( unsigned int i = 0; i < clifList.size(); i++ )
    {
        if( clifList[i]->handle == clifHandle )
        {
            RemoveClif(i);
            break;
        }
    }
}

cfClif* cfClif::GetClif(std::string clifHandle)
{
    for( unsigned int i = 0; i < clifList.size(); i++ )
    {
        if( 0 == clifList[i]->GetHandle().compare(clifHandle ) )
        {
            return clifList[i];
        }
    }
    return NULL;
}

void cfClif::RemoveElement( unsigned int index )
{
    if( index >= 0 && index < data.size() )
    {
        data.erase(data.begin() + index, data.begin() + index + 1);
    }
}
    
void cfClif::RemoveElement( std::string elementHandle )
{
    for( unsigned int index = 0; index < data.size(); index++ )
    {
        if( data.at(index)->GetHandle().compare(elementHandle) == 0 )
        {
            data.erase(data.begin() + index, data.begin() + index + 1);
            break;
        }
    }
}

void cfClif::AddElement( string elementHandle, string elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    temp.SetValue(elementValue);
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, char elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, unsigned char elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, int elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, unsigned int elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, long elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, unsigned long elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, float elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

void cfClif::AddElement( string elementHandle, double elementValue, clif_element::ClifElementStyle style )
{
    cfClifElement temp;
    temp.SetHandle(elementHandle);
    stringstream ss;
    ss << elementValue;
    temp.SetValue( ss.str() );
    temp.SetStyle(style);
    AddElement(&temp);
}

string cfClif::GetDataValue( string dataHandle )
{
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( data[i]->GetHandle() == dataHandle )
        {
            return data[i]->GetValue();
        }
    }
    
    return "";
}

bool cfClif::GetDataValue( std::string dataHandle, std::string& str )
{
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( data[i]->GetHandle() == dataHandle )
        {
            str = data[i]->GetValue();
            return true;
        }
    }
    
    return false;
}

bool cfClif::GetDataValue( string dataHandle, char& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool cfClif::GetDataValue( string dataHandle, unsigned char& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

 bool cfClif::GetDataValue( string dataHandle, int& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }    
}

 bool cfClif::GetDataValue( string dataHandle, unsigned int& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

 bool cfClif::GetDataValue( string dataHandle, long& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

 bool cfClif::GetDataValue( string dataHandle, unsigned long& number )
{
    // convert string to int
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

 bool cfClif::GetDataValue( string dataHandle, float& number )
{
    // convert string to float
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }   
}

  bool cfClif::GetDataValue( string dataHandle, double& number )
{
    // convert string to float
    string value = GetDataValue(dataHandle);
    stringstream ss;
    ss << value;
    ss >> number;

    // check if the conversion was successful
    if( !(ss.bad()) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

clif_element::ClifElementStyle cfClif::GetStyle( unsigned int index )
{
    if( index < data.size() )
    {
        return data[index]->GetStyle();
    }
    else
    {
        cerr << "WARNING: Index " << index << " is not in range of data list with length "
            << data.size() << endl;
        return clif_element::ASSIGNMENT;
    }
}

string cfClif::GetDataValue( unsigned int index )
{
    if( index < data.size() )
    {
        return data[index]->GetValue();
    }
    else
    {
        cerr << "WARNING: Index " << index << " is not in range of data list with length "
            << data.size() << endl;        
        return "";
    }
}

string cfClif::GetDataHandle( unsigned int index )
{
    if( index < data.size() )
    {
        return data[index]->GetHandle();
    }
    else
    {
        cerr << "WARNING: Index " << index << " is not in range of data list with length "
            << data.size() << endl;
        return "";
    }
}

cfClifArray* cfClif::GetArray( string arrayHandle )
{  
    for( unsigned int i = 0; i < arrays.size(); i++ )
    {
        if( 0 == arrays[i]->GetHandle().compare(arrayHandle) )
        {
            return arrays[i];
        }
    }

    return NULL;
}

void cfClif::SetHandle( string clifHandle )
{
    handle = clifHandle;
}

int cfClif::WriteClif( std::string path )
{
    int stackDepth = 0;
    ofstream writer(path.c_str());
    if( writer.is_open() )
    {
        WriteClifRecursive(writer, &stackDepth);
        writer.close();
        return CF_OK;
    }
    else
    {
        cerr << "ERROR: Could not open clif file for writing: " << path << endl;
        return CF_FAIL;
    }
}

void cfClif::WriteClifRecursive( ofstream& writer, int* stackDepth )
{
    (*stackDepth)++;
    for( unsigned int i = 0; i < data.size(); i++ )
    {
        if( clif_element::COLON == data[i]->GetStyle() )
        {
            writer << AddTabs(*stackDepth) << data[i]->GetHandle() << " : " << data[i]->GetValue() << endl;
        }
        else
        {
            writer << AddTabs(*stackDepth) << data[i]->GetHandle() << " = " << data[i]->GetValue() << ";" << endl;
        }
    }
    
    // write arrays - add later
    for( unsigned int i = 0; i < arrays.size(); i++ )
    {
        WriteArray( writer, stackDepth, i );
    }
    
    for( unsigned int i = 0; i < clifList.size(); i++ )
    {
        
        writer << AddTabs(*stackDepth) << clifList[i]->handle << " ={" << endl;
        clifList[i]->WriteClifRecursive( writer, stackDepth );
        writer << AddTabs(*stackDepth) << "};" << endl;
        
    }   
    (*stackDepth)--;
}

void cfClif::WriteArray( ofstream& writer, int* stackDepth, int arrayIndex )
{
    writer << AddTabs(*stackDepth) << arrays[arrayIndex]->GetHandle() << "[] ={" << endl;
    for( unsigned int i = 0; i < arrays[arrayIndex]->arrayElements.size(); i++ )
    {
        writer << TAB << AddTabs(*stackDepth) << arrays[arrayIndex]->GetElement(i);
        // add comma only if its not the last element
        if( i < arrays[arrayIndex]->arrayElements.size() - 1)
        {
            writer << ",";
        }
        writer << endl;
    }
    writer << AddTabs(*stackDepth) << "};" << endl;
}

string cfClif::AddTabs( int tabCount )
{
    string tabString = "";   
    for( int i = 1; i < tabCount; i++ )
    {
        tabString += TAB;
    }
    return tabString;
}

int cfClif::AssignVariableWithElement(string elementHandle, int& var)
{
    string dataString = GetDataValue(elementHandle);
    var = atoi(dataString.c_str());
    if( !dataString.empty() )
    {
        return CF_OK;
    }
    else
    {
        cerr << "WARNING: Element handle " << elementHandle << " was not found" << endl;
        return CF_FAIL;
    }
}

int cfClif::AssignVariableWithElement(string elementHandle, float& var)
{
    string dataString = GetDataValue(elementHandle);
    var = (float)atof(dataString.c_str());
    if( !dataString.empty() )
    {
        return CF_OK;
    }
    else
    {
        cerr << "WARNING: Element handle " << elementHandle << " was not found" << endl;
        return CF_FAIL;
    }
}

int cfClif::AssignVariableWithElement(string elementHandle, double& var)
{
    string dataString = GetDataValue(elementHandle);
    var = atof(dataString.c_str());
    if( !dataString.empty())
    {
        return CF_OK;
    }
    else
    {
        cerr << "WARNING: Element handle " << elementHandle << " was not found" << endl;
        return CF_FAIL;
    }
}

int cfClif::AssignVariableWithElement(string elementHandle, char* var)
{
    //FIXME- not implemented 
    
    //var = new char[100];
    //string tempVar = GetDataValue(elementHandle);
    //strcpy(var, tempVar.c_str());
    return CF_OK;
}

int cfClif::AssignVariableWithElement(string elementHandle, string& var)
{
    var = GetDataValue(elementHandle);
    return CF_OK;
}
