/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy
   of these files.
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input

   Please contact us if you have any questions
*/
/** \file
    \author J V Siebers

     read_pinnacle_roi.cc

     These routines are part of the VCU created c++ libpin.
     libpin contains routines which interfaces things to Pinnacle

**/
/* 
   read_pinnacle_roi.cc

   Created: Jan 25, 1999: JVS:
   
   Modification History:
      April 25, 2000: Change strncmp to Current_ROI_Name so not confuse names like SKIN, SKIN1
      Dec 26, 2000:PJK: Added roi->name
      July 25, 2002: PJK: Added write_pinnacle_roi- writes with less info than a normal roi file, but can be improted to pinnalce, or appended to plan.roi.
      August 29, 2002: PJK/RG: added volume_name to roi for reading and writing
      January 22, 2003: PJK: Changed write_pinnacle_roi to write roi->name rather than the filename
      January 22, 2003: PJK: Added reading and writing color to roi
      read_pinnacle_roi.cc
      June 3, 2004: JVS: checkPinnacleROI....makes sure contour is closed AND has only curve per slice
      May 20, 2004: PJK: Changed strncmp to strcmp so that will not recognize PTV as PTVtumor etc
      read_pinnacle_roi.cc

      Feb 14, 2005: KW:  Added reading and writing  vertices, triangles, for
      surface_mesh, and mean_mesh in Pinnacle 7.7 and above.  However, this
      will not affect reading and writing in Pin 6 ROI format.
      KW: IMPORTANT!!! If you wish to write in the mesh format need to use the write_pinnacle_roi routine that needs one extra
      flag: int mesh_on as the 2nd variable: If mesh_on  = 1 it writes only in the mesh information.
      
      If the ROI file containes any mesh information (such as surface_mesh..         etc)it will read in that info whatever the mesh_on flag says.
      
      The structures roi->mesh[0], and roi->mesh[1] filles in the surface_mesh       and mean mesh info respectively.
      
      After you read in the mesh info you can obtain the x,y,z coordinates
      in 3D space for each point in ROI by:

      roi->mesh[0].vertex[vt].x  = roi->mesh[0].vertex[vt].x* mmTocm + meshOffsetX ;
      roi->mesh[0].vertex[vt].y  = roi->mesh[0].vertex[vt].y* mmTocm + meshOffsetY ;
      roi->mesh[0].vertex[vt].z  = roi->mesh[0].vertex[vt].z* mmTocm + meshOffsetZ ;

      To get the   meshOffsetX, meshOffsetY,meshOffsetZ need to ron a script
      that is located at /wolf/dsk1/mcv/Scripts/writeMeshContourScript .
      mmTocm =  0.1

      February 18, 2007 MF
      fixe bugs in write_pinnacle_roi. Change file opening to "a" from "w", allowing for multiple ROIs being
      stored in a single file. Make sure that the output file is closed at the function exit. Otherwise,
      (important) 2-3 lines at the end of the ROI will be cut off.
   Nov 5, 2007: JVS: eliminate myerrno, replace with checkClifError()
   Feb 25, 2010: JVS put #ifdef DEBUG_MESH around KW's verbose output
   May 16, 2012: JVS: Eliminate compiler warnings -- get rid of passing roi_name to two functions since not used
   Dec 29, 2015: JVS: Seperate out sortROI from checkPinnacleROI
   Oct 2, 2016: HN : Added a function named scan_pinnacle_roi_full_prop which fills a new struct called roi_prop that contains all properties
                    listed in the roi file except data(i.e. curve and surface_mesh). This helps to filter rois based on properties.

   Oct 18, 2016: HN : change all the pointer inside struct to STL containter to void memory leak
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utilities.h"
#include "read_clif.h"
#include "roi_type.h"
#include "option.h"
#include <limits>  // std::numeric_limits
#include <algorithm>    // std::max

#include <iostream> // cout
/* Read in a roi from pinnacle */
// #define DEBUG
/* ************************************************************************************************* */
int sortROICurvesByZCoordinate(roi_type *roi);
/// reads in an roi, sorts the roi's by Z
int checkPinnacleROI(char *roiFileName, char *roiName)
{
    roi_type *roi = new(roi_type);

    /* Read in the roi */  /* Read in the ROI in standard Pinnacle Format */
    if(read_pinnacle_roi(roiFileName, roiName, roi) != OKFlag){
        eprintf("\n ERROR: Reading Pinnacle ROI %s from %s",roiName,roiFileName); return(FAIL);
    }

    if(OKFlag != sortROICurvesByZCoordinate(roi)) {
        eprintf("\n ERROR: %s, roi %s", __FUNCTION__,roiName);
    }
    printf ("\n roi %s is OK", roiName);
    delete (roi);
    return(OKFlag);
}
/* ***************************************************************************************************** */
bool copyCurve(roi_curve_type *destination, roi_curve_type *source) {
    destination->npts  = source->npts;
    if (destination->point.size()>0)
    {
        destination->point.clear();
    }

    destination->point = std::vector<Point_3d_float>(destination->npts);

    memcpy(&destination->point[0], &source->point[0], source->npts*sizeof(Point_3d_float));
    return true;
}
/* ***************************************************************************************************** */
bool swapCurves(roi_curve_type *aCurve, roi_curve_type *bCurve) {
    //

    roi_curve_type tempCurve = roi_curve_type{};

    // Copy aCurve to tempCurve
    copyCurve(&tempCurve,aCurve);
    //
    // Copy bCurve to aCurve
    copyCurve(aCurve,bCurve);
    //
    // Copy tempCurve to bCurve
    copyCurve(bCurve,&tempCurve);

    return true;
}


/* **************************************** */
int sortROICurvesByZCoordinate(roi_type *roi) {
    // -----------------------------------------------------------
    // NEED TO SORT ROI's by INCREASING Z for bitmap making program
    // This is a HACK, get the Z's for all of the contours
    printf("\n %s " , __FUNCTION__);
    float *roi_z_array;
    roi_z_array = (float *) calloc(roi->n_curves, sizeof(float) );
    if(roi_z_array == NULL){
        eprintf("\n ERROR: memory allocation for roi_z_array"); return false;
    }
    // Get all the Z's
    for( int ic=0; ic< roi->n_curves; ic++){
        Point_3d_float *point = &roi->curve[ic].point[0];
        roi_z_array[ic] = point->z;
    }
    // Sort the Z's
    /*
  {
     int i=0;
     do{
        if( roi_z_array[i] > roi_z_array[i+1] ){
           float ztemp      = roi_z_array[i+1];
           roi_z_array[i+1] = roi_z_array[i];
           roi_z_array[i]   = ztemp;
           if(i) i--;
           if(i) i--;
        } else if(  roi_z_array[i] == roi_z_array[i+1] ) {
           eprintf("\n WARNING: Multiple Contours for ROI at Z = %f",roi_z_array[i]);
           i++;
           // return(FAIL);
        }
        else i++;
     }while(i < roi->n_curves-1);
   }
  */
    {
        int i=0;
        do{
            if( i < roi->n_curves-1) {
                if ( roi->curve[i].point[0].z  > roi->curve[i+1].point[0].z ){
                    swapCurves(&roi->curve[i], &roi->curve[i+1]);
                    if(i) i--;
                    if(i) i--;
                } else if(  roi->curve[i].point[0].z == roi->curve[i+1].point[0].z ) {
                    eprintf("\n WARNING: Multiple Contours for ROI at Z = %f",roi->curve[i].point[0].z);
                    i++;
                    // return(FAIL);
                }
                else i++;
            }
        }while(i < roi->n_curves-1);
#ifdef DEBUG_CONTOUR
        printf("\n\n After Sort Z's\n");
        for(i=0; i<roi.n_curves; i++)
            printf("\n %f %f %f", roi.curve[i].point[0].x,
                    roi.curve[i].point[0].y,
                    roi.curve[i].point[0].z);
#endif
    }
    return true;
}


////HN reading roi file fails using the regular way (i.e. way that we read Patient file)
//int read_pinnacle_roi_full_prop(char *RoiFilename,
//                             std::vector<roi_prop2> &roi_full_prop,
//                             int max_roi)
//{
//    char roiFileName[MAX_STR_LEN];
//    strcpy(roiFileName,RoiFilename);  // put first part on name
//    strcat(roiFileName,".roi");          // append .Trial to the name

//    FILE *fp = fopen(roiFileName,"r");
//    if(fp==NULL)
//    {
//        std::cout << " ERROR: opening Roi  file " <<  roiFileName <<std::endl;
//        return(FAIL);
//    }

//    // Read Roi list
//    clifObjectType roiListClif = clifObjectType{};

//    // read the entire clif object in....
//       if ( OK != readClifFile(fp,&roiListClif,"roi")) {
//           std::cout << "ERROR: read_pinnacle_roi_full_prop readClifFile" << std::endl;
//           fclose(fp);
//           return FAIL;
//       }
//    fclose(fp);

//    // check object just to make sure
//    if(OK != checkIfObjectList(&roiListClif)) {
//        std::cout << " ERROR: read_pinnacle_roi_full_prop:  This should never happen " << std::endl; return FAIL;

//    }
//   int nRois= roiListClif.nObjects;
//   std::cout << "Number of Rois = " << nRois << std::endl;

//    return OK;
//}




//HN read in a vector of roi_prop
int scan_pinnacle_roi_full_prop(char *pRoiFileName,
                                std::vector<roi_prop> &roi_full_prop,
                                int *nfound,
                                int max_roi)
{

    int lfound;
    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pRoiFileName);  // put first part on name
#ifdef DEBUG
    printf("\n DEBUG MODE:  Reading In ROI's from file %s",roi_fname);
#endif
    FILE *fp;
    fp = fopen(roi_fname,"r");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening input file %s\n",roi_fname);return(FAIL);
    }
    *nfound = 0;
    lfound = 0;
    roi_full_prop.clear();
    char tempCharArray[MAX_STR_LEN];
    do{
        if( clif_get_to_line( fp,"roi={" ) !=OKFlag )
        {
             break;
        }

        roi_full_prop.push_back(roi_prop{});

        if(clif_string_read(fp,"name:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed roi name not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().name = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));



        if(clif_string_read(fp,"volume_name:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed volume_name not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().volume_name = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));



        if(clif_string_read(fp,"stats_volume_name:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed stats_volume_name not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().stats_volume_name = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));


        if(clif_string_read(fp,"author:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed author not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().author = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));


        if(clif_string_read(fp,"organ_name:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed organ_name not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().organ_name = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));


        roi_full_prop.back().flags = (int) clif_get_value(fp,"flags =");

        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting flags (roi_prop->flags)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        if(clif_string_read(fp,"roiinterpretedtype:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed roiinterpretedtype not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().roiinterpretedtype = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));


        if(clif_string_read(fp,"color:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed color not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().color = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));


        roi_full_prop.back().curve_min_area = (float) clif_get_value(fp,"curve_min_area =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting curve_min_area (roi_prop->curve_min_area)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().lower = (int) clif_get_value(fp,"lower =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting lower (roi_prop->lower)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().upper = (int) clif_get_value(fp,"upper =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting upper (roi_prop->upper)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().radius = (float) clif_get_value(fp,"radius =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting radius (roi_prop->radius)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().density = (float) clif_get_value(fp,"density =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting density (roi_prop->density)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        if(clif_string_read(fp,"density_units:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed density_units not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().density_units = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));

        roi_full_prop.back().override_data = (int) clif_get_value(fp,"override_data =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting override_data (roi_prop->override_data)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().override_order = (int) clif_get_value(fp,"override_order =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting override_order (roi_prop->override_order)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().override_material = (int) clif_get_value(fp,"override_material =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting override_material (roi_prop->override_material)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        if(clif_string_read(fp,"material:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed material not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().material = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));

        roi_full_prop.back().invert_density_loading = (int) clif_get_value(fp,"invert_density_loading =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting invert_density_loading (roi_prop->invert_density_loading)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().volume = (float) clif_get_value(fp,"volume =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting volume (roi_prop->volume)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }



        roi_full_prop.back().pixel_min = (int) clif_get_value(fp,"pixel_min =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting pixel_min (roi_prop->pixel_min)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().pixel_max = (int) clif_get_value(fp,"pixel_max =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting pixel_max (roi_prop->pixel_max)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().pixel_mean = (float) clif_get_value(fp,"pixel_mean =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting pixel_mean (roi_prop->pixel_mean)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().pixel_std = (float) clif_get_value(fp,"pixel_std =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting pixel_std (roi_prop->pixel_std)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().bBEVDRROutline = (int) clif_get_value(fp,"bBEVDRROutline =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting bBEVDRROutline (roi_prop->bBEVDRROutline)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().display_on_other_vols = (int) clif_get_value(fp,"display_on_other_vols =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting display_on_other_vols (roi_prop->display_on_other_vols)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().is_linked = (int) clif_get_value(fp,"is_linked =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting is_linked (roi_prop->is_linked)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().auto_update_contours = (int) clif_get_value(fp,"auto_update_contours =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting auto_update_contours (roi_prop->auto_update_contours)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        if(clif_string_read(fp,"UID:",tempCharArray) != OKFlag)
        {
            eprintf("\n ERROR: misformed UID not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop.back().UID = std::string(tempCharArray);
        memset(&tempCharArray[0],0,sizeof(tempCharArray));

        roi_full_prop.back().stoppingpower = (float) clif_get_value(fp,"stoppingpower =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting stoppingpower (roi_prop->stoppingpower)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }


        roi_full_prop.back().ctnumber = (int) clif_get_value(fp,"ctnumber =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting ctnumber (roi_prop->ctnumber)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop.back().is_created_by_atlasseg = (int) clif_get_value(fp,"is_created_by_atlasseg =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting is_created_by_atlasseg (roi_prop->is_created_by_atlasseg)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        // TODO: only presents in Pinnacle 9.7
//        roi_full_prop.back().is_warped = (int) clif_get_value(fp,"is_warped =");
//        if(checkClifError() != OKFlag)
//        {
//            eprintf("\n ERROR: getting is_warped (roi_prop->is_warped)");
//            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
//            fclose(fp); return(FAIL);
//        }

        /* find number of curves in the contour */
        roi_full_prop.back().n_curves = (int) clif_get_value(fp,"num_curve =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting num_curve (roi_prop->n_curves)");
            eprintf(" For ROI %s in file %s", roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }

        /* close the roi clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing roi %s in file %s",roi_full_prop.back().name.c_str(), roi_fname);
            fclose(fp); return(FAIL);
        }
        lfound++;
    } while( !feof(fp) && (lfound < max_roi) );
    if(lfound>0) *nfound = lfound;

    fclose(fp);
    return(OKFlag);
}





// HN: this version does not allocate memeory for roi_curve_type and roi_mesh_type vectors in old way
// to avoid possible memory leakage insted a std container is used
int read_pinnacle_roi_data_for_given_roiName(char *pRoiFileName,std::string roiName,std::vector<roi_prop> &roi_full_prop )
{

    if (roi_full_prop.size() == 0)
    {
        std::cout << "ERROR: size for roi_full_prop is zero. Did you forget calling scan_pinnacle_roi_full_prop?" << std::endl;
        return FAIL;
    }
    int roiIndexInVector=-1;

    for (int iRoi = 0; iRoi < roi_full_prop.size(); ++iRoi)
    {
        if (roi_full_prop[iRoi].name.compare(roiName) == 0)
        {
            roiIndexInVector=iRoi;
            break;
        }
    }
    if (roiIndexInVector==-1)
    {
        std::cout << "ERROR:read_pinnacle_roi_data_for_given_roiName --> roiName could not be found in input roi_full_prop?" << std::endl;
        return FAIL;
    }


    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pRoiFileName);  // put first part on name
    FILE *fp;
    fp = fopen(roi_fname,"r");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening input file %s\n",roi_fname);return(FAIL);
    }

    /* get name of this roi */
    char Current_ROI_Name[MAX_STR_LEN];
    do{
        if( clif_get_to_line( fp,"roi={" ) !=OKFlag )
        {
            eprintf("\n ERROR: finding roi={");
            eprintf("\n\t ROI %s", roiName.c_str());
            fclose(fp); return(FAIL);
        }
        if(clif_string_read(fp,"name:",Current_ROI_Name) != OKFlag)
        {
            fclose(fp); return(FAIL);
        }
        if( strcmp(Current_ROI_Name,roiName.c_str()) != 0 )
        {
            /* close the roi clif */
            if(clif_get_to_line(fp,"};")!=OKFlag)
            {
                eprintf("\n ERROR: Closing roi %s in file %s",Current_ROI_Name, roi_fname);
                fclose(fp); return(FAIL);
            }
        }
    }//while( strncmp(Current_ROI_Name,roi_name,strlen(Current_ROI_Name)) != 0 );
    while( strcmp(Current_ROI_Name,roiName.c_str()) != 0 );



    /* find number of curves in the contour */
    int num_curve= (int) clif_get_value(fp,"num_curve =");
    if(checkClifError() != OKFlag)
    {
        eprintf("\n ERROR: getting num_curve (roi->n_curves)");
        eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
        fclose(fp); return(FAIL);
    }

    if (num_curve !=roi_full_prop[roiIndexInVector].n_curves)
    {
        std::cout << "ERROR : mishmatch in number of curves. This should not happen." << std::endl;
        return FAIL;
    }

    // allocate memory for n_curves
    //roi->curve = (roi_curve_type *) calloc( roi->n_curves, sizeof(roi_curve_type));
    roi_full_prop[roiIndexInVector].curveVector =std::vector<roi_curve_type>(roi_full_prop[roiIndexInVector].n_curves);

    for(int icurve = 0; icurve < roi_full_prop[roiIndexInVector].n_curves; icurve++)
    {
        if( clif_get_to_line( fp, "curve={" ) != OKFlag ){
            eprintf("\n ERROR: Getting curve={");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        roi_full_prop[roiIndexInVector].curveVector[icurve].npts = (int) clif_get_value(fp, "num_points =");

        if(checkClifError() != OKFlag){
            eprintf("\n ERROR: Getting num_points");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        // allocate memory for the points
        //roi_full_prop[roiIndexInVector].curveVector[icurve].point = (Point_3d_float *)calloc(roi_full_prop[roiIndexInVector].curveVector[icurve].npts, sizeof(Point_3d_float));
        //       if(roi_full_prop[roiIndexInVector].curveVector[icurve].point == NULL){
        //       eprintf("\n ERROR: Allocating memory for point");   fclose(fp); return(FAIL);
        //       }
        roi_full_prop[roiIndexInVector].curveVector[icurve].point = std::vector<Point_3d_float>(roi_full_prop[roiIndexInVector].curveVector[icurve].npts);


        // read in the points
        if( read_pinnacle_3d_points(fp, roi_full_prop[roiIndexInVector].curveVector[icurve].npts, &roi_full_prop[roiIndexInVector].curveVector[icurve].point[0] ) != OKFlag){
            eprintf("\n ERROR: Reading in points");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }

        /* close the roi clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing roi in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }


    
    //KW:  Read in  vertices and triangles for Surface_mesh

    if( !(clif_get_to_line( fp,"surface_mesh={" ) != OKFlag ))
    {
#ifdef DEBUG_MESH
        printf("\n SUCCESS: finding surface_mesh={");
        printf("\n\t ROI %s", roiName.c_str());
        printf("\n KW: reading surface mesh");
#endif


        roi_full_prop[roiIndexInVector].meshVector = std::vector<roi_mesh_type>(2);

        //KW: Get # of vertices.
        roi_full_prop[roiIndexInVector].meshVector[0].n_vertices = (int) clif_get_value(fp,"number_of_vertices =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_vertices (roi_full_prop[roiIndexInVector].meshVector[0].n_vertices)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }


        /* HN: allocate memory for n_vertices*/
        roi_full_prop[roiIndexInVector].meshVector[0].vertex = std::vector<Point_3d_float>(roi_full_prop[roiIndexInVector].meshVector[0].n_vertices);

        // KW: Get # of Triangels
        roi_full_prop[roiIndexInVector].meshVector[0].n_triangles = (int) clif_get_value(fp,"number_of_triangles =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_trianles (roi_full_prop[roiIndexInVector].meshVector[0].n_triangles)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }

        //HN
        roi_full_prop[roiIndexInVector].meshVector[0].triangle = std::vector<Point_3d_int>(roi_full_prop[roiIndexInVector].meshVector[0].n_triangles);


        if (roi_full_prop[roiIndexInVector].meshVector[0].n_vertices !=0)
        {

            /*KW: read in the vertices */
            if( read_pinnacle_3d_vertices(fp, roi_full_prop[roiIndexInVector].meshVector[0].n_vertices, &roi_full_prop[roiIndexInVector].meshVector[0].vertex[0]) != OKFlag)
            {
                eprintf("\n ERROR: Reading in vertices");
                eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
                fclose(fp);return(FAIL);
            }

        }
#ifdef DEBUG_MESH
        for(int i=0; i<roi_full_prop[roiIndexInVector].meshVector[0].n_vertices; i++)
        {
            //  printf("\n KW: roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].x is (%f), roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].y is (%f), roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].z is (%f)\n",roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].x, roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].y, roi_full_prop[roiIndexInVector].meshVector[0].vertex[i].z );
        }
#endif

        if (roi_full_prop[roiIndexInVector].meshVector[0].n_triangles !=0)
        {
            /* KW:read in the triangles */
            if( read_pinnacle_3d_triangles(fp, roi_full_prop[roiIndexInVector].meshVector[0].n_triangles, &roi_full_prop[roiIndexInVector].meshVector[0].triangle[0]) != OKFlag)
            {
                eprintf("\n ERROR: Reading in triangles");
                eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
                fclose(fp);return(FAIL);
            }

        }
#ifdef DEBUG_MESH
        for(int i=0; i<roi_full_prop[roiIndexInVector].meshVector[0].n_triangles; i++)
        {
            //  printf("\n KW: roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].x is (%d), roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].y is (%d), roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].z is (%d)\n",roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].x, roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].y, roi_full_prop[roiIndexInVector].meshVector[0].triangle[i].z );
        }
#endif

        /* close the surface mesh clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing surface mesh in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }

    //KW:  Read in  vertices and triangles for mean_mesh

    if( !(clif_get_to_line( fp,"mean_mesh={" ) != OKFlag ))
    {
#ifdef DEBUG_MESH
        printf("\n SUCCESS: finding mean_mesh={");
        printf("\n\t ROI %s", roi_name);
#endif

        //KW: We already allocated memory for mesh[0], and mesh[1] in Surface mesh
        //KW: Get # of vertices.
        roi_full_prop[roiIndexInVector].meshVector[1].n_vertices = (int) clif_get_value(fp,"number_of_vertices =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_vertices (roi_full_prop[roiIndexInVector].meshVector[1].n_vertices)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }


        // HN
        roi_full_prop[roiIndexInVector].meshVector[1].vertex = std::vector<Point_3d_float>(roi_full_prop[roiIndexInVector].meshVector[1].n_vertices);


        // KW: Get # of Triangels
        roi_full_prop[roiIndexInVector].meshVector[1].n_triangles = (int) clif_get_value(fp,"number_of_triangles =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_trianles (roi_full_prop[roiIndexInVector].meshVector[1].n_triangles)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }

        roi_full_prop[roiIndexInVector].meshVector[1].triangle = std::vector<Point_3d_int>(roi_full_prop[roiIndexInVector].meshVector[1].n_triangles);



        /*KW: read in the vertices */
        if (roi_full_prop[roiIndexInVector].meshVector[1].n_vertices !=0)
        {
            if( read_pinnacle_3d_vertices(fp, roi_full_prop[roiIndexInVector].meshVector[1].n_vertices, &roi_full_prop[roiIndexInVector].meshVector[1].vertex[0]) != OKFlag)
            {
                eprintf("\n ERROR: Reading in vertices");
                eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
                fclose(fp);return(FAIL);
            }
        }

#ifdef DEBUG_MESH
        for(int i=0; i<roi_full_prop[roiIndexInVector].meshVector[1].n_vertices; i++)
        {
            //printf("\n KW: roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].x is (%f), roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].y is (%f), roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].z is (%f)\n",roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].x, roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].y, roi_full_prop[roiIndexInVector].meshVector[1].vertex[i].z );
        }
#endif


        /* KW:read in the triangles */
        if (roi_full_prop[roiIndexInVector].meshVector[1].n_triangles !=0)
        {
            if( read_pinnacle_3d_triangles(fp, roi_full_prop[roiIndexInVector].meshVector[1].n_triangles, &roi_full_prop[roiIndexInVector].meshVector[1].triangle[0]) != OKFlag)
            {
                eprintf("\n ERROR: Reading in triangles");
                eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
                fclose(fp);return(FAIL);
            }
        }
#ifdef DEBUG_MESH
        for(int i=0; i<roi_full_prop[roiIndexInVector].meshVector[1].n_triangles; i++)
        {
            //    printf("\n KW: roi_full_prop[roiIndexInVector].meshVector[1].triangle[i].x is (%d), roi_full_prop[roiIndexInVector].meshVector[1].triangle[i].y is (%d), roi_full_prop[roiIndexInVector].meshVector[1].triangle[i].z is (%d)\n",roi_full_prop[roiIndexInVector].meshVector[1].triangle[i].x, roi_full_prop[roiIndexInVector].meshVector[1].triangle[i].y, roi->mesh[1].triangle[i].z );
        }

#endif

        /* close the mean mesh clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing mean mesh in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }

    fclose(fp);

    roi_full_prop[roiIndexInVector].isDataLoaded =true;

    roi_full_prop[roiIndexInVector].boundingCuboid.xMax = -std::numeric_limits<float>::max();
    roi_full_prop[roiIndexInVector].boundingCuboid.xMin = std::numeric_limits<float>::max();
    roi_full_prop[roiIndexInVector].boundingCuboid.yMax = -std::numeric_limits<float>::max();
    roi_full_prop[roiIndexInVector].boundingCuboid.yMin  = std::numeric_limits<float>::max();
    roi_full_prop[roiIndexInVector].boundingCuboid.zMax = -std::numeric_limits<float>::max();
    roi_full_prop[roiIndexInVector].boundingCuboid.zMin = std::numeric_limits<float>::max();


    if (!roi_full_prop[roiIndexInVector].curveVector.empty())
    {
        for (int iCurve = 0; iCurve < roi_full_prop[roiIndexInVector].curveVector.size(); ++iCurve)
        {
            roi_full_prop[roiIndexInVector].curveZLocations.push_back(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[0].z);
            //            std::cout << "curveZLocations["<< iCurve << "] = " << roi_full_prop[roiIndexInVector].curveZLocations[iCurve] << std::endl;
            // TODO: assuming all the points have the save z values
            roi_full_prop[roiIndexInVector].boundingCuboid.zMax=std::max(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[0].z,roi_full_prop[roiIndexInVector].boundingCuboid.zMax);
            roi_full_prop[roiIndexInVector].boundingCuboid.zMin=std::min(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[0].z,roi_full_prop[roiIndexInVector].boundingCuboid.zMin);

            float minXCurve = std::numeric_limits<float>::max();
            float minYCurve = std::numeric_limits<float>::max();
            float maxXCurve = -std::numeric_limits<float>::max();
            float maxYCurve = -std::numeric_limits<float>::max();

            for (int iPoint = 0; iPoint < roi_full_prop[roiIndexInVector].curveVector[iCurve].point.size(); ++iPoint)
            {
                maxXCurve=std::max(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[iPoint].x,maxXCurve);
                minXCurve=std::min(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[iPoint].x,minXCurve);
                maxYCurve=std::max(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[iPoint].y,maxYCurve);
                minYCurve=std::min(roi_full_prop[roiIndexInVector].curveVector[iCurve].point[iPoint].y,minYCurve);
            }

            roi_full_prop[roiIndexInVector].curveVector[iCurve].maxX= maxXCurve;
            roi_full_prop[roiIndexInVector].curveVector[iCurve].minX= minXCurve;
            roi_full_prop[roiIndexInVector].curveVector[iCurve].maxY= maxYCurve;
            roi_full_prop[roiIndexInVector].curveVector[iCurve].minY= minYCurve;
#ifdef DEBUG
            std::cout << "CurveNumber = "  <<iCurve << std::endl;
            std::cout << "\t maxXCurve = " << maxXCurve << std::endl;
            std::cout << "\t minXCurve = " << minXCurve << std::endl;
            std::cout << "\t maxYCurve = " << maxYCurve << std::endl;
            std::cout << "\t minYCurve = " << minYCurve<< std::endl;
#endif

            roi_full_prop[roiIndexInVector].boundingCuboid.xMax=std::max(maxXCurve,roi_full_prop[roiIndexInVector].boundingCuboid.xMax);
            roi_full_prop[roiIndexInVector].boundingCuboid.xMin=std::min(minXCurve,roi_full_prop[roiIndexInVector].boundingCuboid.xMin);
            roi_full_prop[roiIndexInVector].boundingCuboid.yMax=std::max(maxYCurve,roi_full_prop[roiIndexInVector].boundingCuboid.yMax);
            roi_full_prop[roiIndexInVector].boundingCuboid.yMin=std::min(minYCurve,roi_full_prop[roiIndexInVector].boundingCuboid.yMin);


        }


#ifdef DEBUG
        std::cout << "boundingBox.xMax = " << roi_full_prop[roiIndexInVector].boundingCuboid.xMax << std::endl;
        std::cout << "boundingBox.xMin = " << roi_full_prop[roiIndexInVector].boundingCuboid.xMin << std::endl;
        std::cout << "boundingBox.yMax = " << roi_full_prop[roiIndexInVector].boundingCuboid.yMax << std::endl;
        std::cout << "boundingBox.yMin = " << roi_full_prop[roiIndexInVector].boundingCuboid.yMin << std::endl;
        std::cout << "boundingBox.zMax = " << roi_full_prop[roiIndexInVector].boundingCuboid.zMax << std::endl;
        std::cout << "boundingBox.zMin = " << roi_full_prop[roiIndexInVector].boundingCuboid.zMin << std::endl;
#endif

    }
    else
    {
        std::cout <<  "Warnning: curveVector is empty for: " <<roi_full_prop[roiIndexInVector].name << std::endl;
//        return FAIL;
    }


    return OKFlag;
}

/* ***************************************************************************************************** */
/** Routine scan_pinnacle_rois gets a list of names of all ROIs in
 * the input file. The routine expects that an array of char* has
 * been pre-allocated by the calling program. The length of this array
 * is passed in the parameter max_roi. CAUTION: this routine allocates
 * memory. It is the responsibility of the calling program to deallocate
 * this memory when done.
 * Inputs
 *        pbeam_fname_stem  - roi file
 *        char **roi_list - head of an array of char*
 *        max_roi - length of char* array
 * Outputs:
 *         nfound - number of found ROIs
 *         names of ROIs are stored in array roi_list
 **/
/// gets list of roi's in a .roi file
int scan_pinnacle_rois(char *pbeam_fname_stem,
                       char **roi_list, int *nfound,
                       int max_roi)
{
    /* open the file with the roi stuff in it */

    int length;
    int lfound;
    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pbeam_fname_stem);  // put first part on name
    strcat(roi_fname,".roi");          // append .Trial to the name
#ifdef DEBUG
    printf("\n DEBUG MODE:  Reading In ROI's from file %s",roi_fname);
#endif
    FILE *fp;
    fp = fopen(roi_fname,"r");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening input file %s\n",roi_fname);return(FAIL);
    }
    *nfound = 0;
    lfound = 0;
    char Current_ROI_Name[MAX_STR_LEN];
    do{
        if( clif_get_to_line( fp,"roi={" ) !=OKFlag )
        {
            fclose(fp); break;
        }
        if(clif_string_read(fp,"name:",Current_ROI_Name) != OKFlag)
        {
            eprintf("\n ERROR: misformed roi name not found) in file %s\n",roi_fname);
            fclose(fp); return(FAIL);
        }
#ifdef DEBUG
        printf("\n Found ROI (%s)", Current_ROI_Name);
#endif
        /* store the name */
        length = (int) strlen(Current_ROI_Name);
        roi_list[lfound] = (char*)calloc(length+1,sizeof(char));
        if(NULL == roi_list[lfound])
        {
            eprintf("\n ERROR: Failure to allocate memory for ROI name in file %s\n",Current_ROI_Name);
            fclose(fp); return(FAIL);
        }
        strcpy(roi_list[lfound],Current_ROI_Name);
        /* close the roi clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing roi %s in file %s",Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        lfound++;
    } while( !feof(fp) && (lfound < max_roi) );
    if(lfound>0) *nfound = lfound;
    fclose(fp);
    return(OKFlag);
}
/* ***************************************************************************************************** */
// #define DEBUG_ROI
/// read_pinnacle_roi: reads an roi from a .roi file
int read_pinnacle_roi(char *pbeam_fname_stem,
                      char *roi_name,
                      roi_type *roi)
{
#ifdef DEBUG_ROI
    printf("\n DEBUG_ROI: Entered read_pinnacle_roi ");
#endif

    /* open the file with the roi stuff in it */

    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pbeam_fname_stem);  // put first part on name
    strcat(roi_fname,".roi");          // append .Trial to the name
#ifdef DEBUG_ROI
    printf("\n DEBUG_ROI: Reading In ROI's from file %s",roi_fname);
#endif
    FILE *fp;
    fp = fopen(roi_fname,"r");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening input file %s\n",roi_fname);return(FAIL);
    }

    /* get name of this roi */
    char Current_ROI_Name[MAX_STR_LEN];
    do{
        if( clif_get_to_line( fp,"roi={" ) !=OKFlag )
        {
            eprintf("\n ERROR: finding roi={");
            eprintf("\n\t ROI %s", roi_name);
            fclose(fp); return(FAIL);
        }
        if(clif_string_read(fp,"name:",Current_ROI_Name) != OKFlag)
        {
            fclose(fp); return(FAIL);
        }
        //printf("\n Found ROI (%s). Looking for (%s) strcmp: %d", Current_ROI_Name,roi_name,strcmp(Current_ROI_Name,roi_name));
        /* check it against desired name */
        // if( strncmp(Current_ROI_Name,roi_name,strlen(roi_name)) != 0 )
        //if( strncmp(Current_ROI_Name,roi_name,strlen(Current_ROI_Name)) != 0 )
        if( strcmp(Current_ROI_Name,roi_name) != 0 )
        {
            /* close the roi clif */
            if(clif_get_to_line(fp,"};")!=OKFlag)
            {
                eprintf("\n ERROR: Closing roi %s in file %s",Current_ROI_Name, roi_fname);
                fclose(fp); return(FAIL);
            }
        }
    }//while( strncmp(Current_ROI_Name,roi_name,strlen(Current_ROI_Name)) != 0 );
    while( strcmp(Current_ROI_Name,roi_name) != 0 );

    sprintf(roi->name,"%s",roi_name);

    // get volume_name
    if(clif_string_read(fp,"volume_name:",roi->volume_name) != OKFlag)
    {
        fprintf(stderr,"read_pinnacle_roi: failure to get volume_name \n");
        fclose(fp); return(FAIL);
    }

    // get volume_name
    if(clif_string_read(fp,"color:",roi->color) != OKFlag)
    {
        fprintf(stderr,"read_pinnacle_roi: failure to get color \n");
        fclose(fp); return(FAIL);
    }

    /* find number of curves in the contour */
    roi->n_curves = (int) clif_get_value(fp,"num_curve =");
    //fprintf(stderr,"got number of curves: %d errno: %d\n",roi->n_curves,myerrno);
    if(checkClifError() != OKFlag)
    {
        eprintf("\n ERROR: getting num_curve (roi->n_curves)");
        eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
        fclose(fp); return(FAIL);
    }
    // allocate memory for n_curves
    //    roi->curve = (roi_curve_type *) calloc( roi->n_curves, sizeof(roi_curve_type));
    //    if(roi->curve == NULL){
    //        eprintf("\n ERROR: Allocating memory for curves");fclose(fp); return(FAIL);
    //    }
    //fprintf(stderr,"Looping over n_curves: %d\n",roi->n_curves);
    roi->curve = std::vector<roi_curve_type>(roi->n_curves);
    for(int icurve = 0; icurve < roi->n_curves; icurve++)
    {
        if( clif_get_to_line( fp, "curve={" ) != OKFlag ){
            eprintf("\n ERROR: Getting curve={");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        roi->curve[icurve].npts = (int) clif_get_value(fp, "num_points =");
        if(checkClifError() != OKFlag){
            eprintf("\n ERROR: Getting num_points");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }

        roi->curve[icurve].point = std::vector<Point_3d_float>(roi->curve[icurve].npts);

        // read in the points
        if( read_pinnacle_3d_points(fp, roi->curve[icurve].npts, &roi->curve[icurve].point[0] ) != OKFlag){
            eprintf("\n ERROR: Reading in points");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }

        /* close the roi clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing roi in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }

#ifdef DEBUG_ROI
    printf("\n DEBUG_ROI: Reading in surface_mesh" );
#endif      
    //KW:  Read in  vertices and triangles for Surface_mesh
    
    if( !(clif_get_to_line( fp,"surface_mesh={" ) != OKFlag ))
    {
#ifdef DEBUG_MESH
        printf("\n SUCCESS: finding surface_mesh={");
        printf("\n\t ROI %s", roi_name);
        printf("\n KW: reading surface mesh");
#endif
        /* allocate memory for 2_mesh: [0] for surface mesh, [1] for mean meash */
        //        roi->mesh= (roi_mesh_type *) calloc( 2, sizeof(roi_mesh_type));
        //        if(roi->mesh == NULL)
        //        {
        //            eprintf("\n ERROR: Allocating memory for mesh");
        //            fclose(fp); return(FAIL);
        //        }

        roi->mesh = std::vector<roi_mesh_type>(2);

        //KW: Get # of vertices.
        roi->mesh[0].n_vertices = (int) clif_get_value(fp,"number_of_vertices =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_vertices (roi->mesh[0].n_vertices)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        /* HN: allocate memory for n_vertices*/
        //      roi->mesh[0].vertex = (Point_3d_float *) calloc( roi->mesh[0].n_vertices, sizeof(Point_3d_float));
        //      if(roi->mesh[0].vertex == NULL)
        //	{
        //	  eprintf("\n ERROR: Allocating memory for vertices");
        //	  fclose(fp); return(FAIL);
        //	}
        roi->mesh[0].vertex = std::vector<Point_3d_float>(roi->mesh[0].n_vertices);

        // KW: Get # of Triangels
        roi->mesh[0].n_triangles = (int) clif_get_value(fp,"number_of_triangles =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_trianles (roi->mesh[0].n_triangles)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        //      /* KW: allocate memory for n_triangles*/
        //      roi->mesh[0].triangle = (Point_3d_int *) calloc( roi->mesh[0].n_triangles, sizeof(Point_3d_int));
        //      if(roi->mesh[0].triangle == NULL)
        //	{
        //	  eprintf("\n ERROR: Allocating memory for triangles ");
        //	  fclose(fp); return(FAIL);
        //	}

        //HN
        roi->mesh[0].triangle = std::vector<Point_3d_int>(roi->mesh[0].n_triangles);


        /*KW: read in the vertices */
        if( read_pinnacle_3d_vertices(fp, roi->mesh[0].n_vertices, &roi->mesh[0].vertex[0]) != OKFlag)
        {
            eprintf("\n ERROR: Reading in vertices");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }

#ifdef DEBUG_MESH
        for(int i=0; i<roi->mesh[0].n_vertices; i++)
        {
            //  printf("\n KW: roi->mesh[0].vertex[i].x is (%f), roi->mesh[0].vertex[i].y is (%f), roi->mesh[0].vertex[i].z is (%f)\n",roi->mesh[0].vertex[i].x, roi->mesh[0].vertex[i].y, roi->mesh[0].vertex[i].z );
        }
#endif


        /* KW:read in the triangles */
        if( read_pinnacle_3d_triangles(fp, roi->mesh[0].n_triangles, &roi->mesh[0].triangle[0]) != OKFlag)
        {
            eprintf("\n ERROR: Reading in triangles");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }

#ifdef DEBUG_MESH
        for(int i=0; i<roi->mesh[0].n_triangles; i++)
        {
            //  printf("\n KW: roi->mesh[0].triangle[i].x is (%d), roi->mesh[0].triangle[i].y is (%d), roi->mesh[0].triangle[i].z is (%d)\n",roi->mesh[0].triangle[i].x, roi->mesh[0].triangle[i].y, roi->mesh[0].triangle[i].z );
        }
#endif

        /* close the surface mesh clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing surface mesh in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }
    
    //KW:  Read in  vertices and triangles for mean_mesh
    
    if( !(clif_get_to_line( fp,"mean_mesh={" ) != OKFlag ))
    {
#ifdef DEBUG_MESH
        printf("\n SUCCESS: finding mean_mesh={");
        printf("\n\t ROI %s", roi_name);
#endif

        //KW: We already allocated memory for mesh[0], and mesh[1] in Surface mesh
        //KW: Get # of vertices.
        roi->mesh[1].n_vertices = (int) clif_get_value(fp,"number_of_vertices =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_vertices (roi->mesh[1].n_vertices)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }

        //        /* KW: allocate memory for n_vertices*/
        //        roi->mesh[1].vertex = (Point_3d_float *) calloc( roi->mesh[1].n_vertices, sizeof(Point_3d_float));
        //        if(roi->mesh[1].vertex == NULL)
        //        {
        //            eprintf("\n ERROR: Allocating memory for vertices");
        //            fclose(fp); return(FAIL);
        //        }

        // HN
        roi->mesh[1].vertex = std::vector<Point_3d_float>(roi->mesh[1].n_vertices);


        // KW: Get # of Triangels
        roi->mesh[1].n_triangles = (int) clif_get_value(fp,"number_of_triangles =");
        if(checkClifError() != OKFlag)
        {
            eprintf("\n ERROR: getting number_of_trianles (roi->mesh[1].n_triangles)");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp); return(FAIL);
        }
        /* KW: allocate memory for n_triangles CHECK THIS*/
        //        roi->mesh[1].triangle = (Point_3d_int *) calloc( roi->mesh[1].n_triangles, sizeof(Point_3d_int));
        //        if(roi->mesh[0].triangle == NULL)
        //        {
        //            eprintf("\n ERROR: Allocating memory for triangles ");
        //            fclose(fp); return(FAIL);
        //        }
        roi->mesh[1].triangle = std::vector<Point_3d_int>(roi->mesh[1].n_triangles);

        //      printf("\n KW: roi->mesh[1].n_vertices is %i\n",roi->mesh[1].n_vertices);
        //printf("\n KW: roi->mesh[1].n_triangles is %i\n",roi->mesh[1].n_triangles);


        /*KW: read in the vertices */
        if( read_pinnacle_3d_vertices(fp, roi->mesh[1].n_vertices, &roi->mesh[1].vertex[0]) != OKFlag)
        {
            eprintf("\n ERROR: Reading in vertices");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }

#ifdef DEBUG_MESH
        for(int i=0; i<roi->mesh[1].n_vertices; i++)
        {
            //printf("\n KW: roi->mesh[1].vertex[i].x is (%f), roi->mesh[1].vertex[i].y is (%f), roi->mesh[1].vertex[i].z is (%f)\n",roi->mesh[1].vertex[i].x, roi->mesh[1].vertex[i].y, roi->mesh[1].vertex[i].z );
        }
#endif


        /* KW:read in the triangles */
        if( read_pinnacle_3d_triangles(fp, roi->mesh[1].n_triangles, &roi->mesh[1].triangle[0]) != OKFlag)
        {
            eprintf("\n ERROR: Reading in triangles");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
#ifdef DEBUG_MESH
        for(int i=0; i<roi->mesh[1].n_triangles; i++)
        {
            //    printf("\n KW: roi->mesh[1].triangle[i].x is (%d), roi->mesh[1].triangle[i].y is (%d), roi->mesh[1].triangle[i].z is (%d)\n",roi->mesh[1].triangle[i].x, roi->mesh[1].triangle[i].y, roi->mesh[1].triangle[i].z );
        }

#endif

        /* close the mean mesh clif */
        if(clif_get_to_line(fp,"};")!=OKFlag)
        {
            eprintf("\n ERROR: Closing mean mesh in file ");
            eprintf(" For ROI %s in file %s", Current_ROI_Name, roi_fname);
            fclose(fp);return(FAIL);
        }
    }
    
    fclose(fp);
    return(OKFlag);
}
/* ***************************************************************************************************** */
/// reads points listed in a .roi file.....Probably should use the clif read routines instead
int read_pinnacle_3d_points(FILE *fp, int npoints, Point_3d_float *point)
{
    if( clif_get_to_line( fp,"points={" ) !=OKFlag )
    {
        eprintf("\n ERROR: finding points={"); return(FAIL);
    }
    for(int i=0; i<npoints; i++)
    {
        char string[MAX_STR_LEN];
        if(clif_fgets(string,MAX_STR_LEN,fp)==NULL)
        {
            eprintf("\n ERROR: reading points (%d)",i+1);
            return(FAIL);
        }
        if(sscanf(string,"%f%f%f",&point[i].x,&point[i].y,&point[i].z) !=3)
        {
            eprintf("\n ERROR: scanning point %d from string %s", i, string);
            return(FAIL);
        }
    }
    if(clif_get_to_line(fp,"};")!=OKFlag)
    {
        eprintf("\n ERROR: Closing  points ");return(FAIL);
    }
    return(OKFlag);
}

/* ***************************************************************************************************** */
/// reads vertices from a .roi file.....Probably should use clif read routines instead
int read_pinnacle_3d_vertices(FILE *fp, int npoints, Point_3d_float *point)
{
    if( clif_get_to_line( fp,"vertices={" ) !=OKFlag )
    {
        eprintf("\n ERROR: finding vertices={"); return(FAIL);
    }
    for(int i=0; i<npoints; i++)
    {
        char string[MAX_STR_LEN];
        if(clif_fgets(string,MAX_STR_LEN,fp)==NULL)
        {
            eprintf("\n ERROR: reading vertices (%d)",i+1);
            return(FAIL);
        }
        if(sscanf(string,"%f%f%f",&point[i].x,&point[i].y,&point[i].z) !=3)
        {
            eprintf("\n ERROR: scanning vertix %d from string %s", i, string);
            return(FAIL);
        }
    }
    if(clif_get_to_line(fp,"};")!=OKFlag)
    {
        eprintf("\n ERROR: Closing  vertices ");return(FAIL);
    }
    return(OKFlag);
}
/* ***************************************************************************************************** */
int read_pinnacle_3d_triangles(FILE *fp, int npoints, Point_3d_int *triangle)
{
    if( clif_get_to_line( fp,"triangles={" ) !=OKFlag )
    {
        eprintf("\n ERROR: finding triangles={"); return(FAIL);
    }
    //   printf("\n KW: TEST 2");
    for(int i=0; i<npoints; i++)
    {
        char string[MAX_STR_LEN];
        if(clif_fgets(string,MAX_STR_LEN,fp)==NULL)
        {
            eprintf("\n ERROR: reading triangles (%d)",i+1);
            return(FAIL);
        }
        //  printf("\n KW: TEST 3");
        if(sscanf(string,"%hd%hd%hd",&triangle[i].x,&triangle[i].y,&triangle[i].z) !=3)
        {
            eprintf("\n ERROR: scanning triangle %d from string %s", i, string);
            return(FAIL);
        }
    }
    if(clif_get_to_line(fp,"};")!=OKFlag)
    {
        eprintf("\n ERROR: Closing  triangles ");return(FAIL);
    }
    return(OKFlag);
}
/* ***************************************************************************************************** */
/* ***************************************************************************************************** */
/// writes out .roi file for Pinnacle
int write_pinnacle_roi(char *pbeam_fname_stem,
                       //                      char *roi_name,
                       roi_type *roi)
{
    /* open the file to write to */
    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pbeam_fname_stem);  // put first part on name
    strcat(roi_fname,".roi");          // append .Trial to the name
#ifdef DEBUG
    printf("\n DEBUG MODE:  Reading In ROI's from file %s",roi_fname);
#endif
    FILE *fp;
    fp = fopen(roi_fname,"a");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening output file %s\n",roi_fname);return(FAIL);
    }

    // now start writing the file
    fprintf(fp,"roi={\n");
    //fprintf(fp,"name: %s\n",roi_name);
    fprintf(fp,"name: %s\n",roi->name);
    fprintf(fp,"volume_name: %s\n",roi->volume_name);
    fprintf(fp,"color: %s\n",roi->color);
    fprintf(fp,"num_curve = %d;\n",roi->n_curves);
    for(int icurve = 0; icurve < roi->n_curves; icurve++)
    {
        fprintf(fp,"\tcurve={\n");
        fprintf(fp,"\tnum_points = %d;\n",roi->curve[icurve].npts);
        fprintf(fp,"\t\tpoints={\n");

        for(int i=0; i<roi->curve[icurve].npts; i++)
        {
            fprintf(fp,"%3.3f %3.3f %3.3f\n",roi->curve[icurve].point[i].x,roi->curve[icurve].point[i].y,roi->curve[icurve].point[i].z);

        } // end of curve
        fprintf(fp,"\t\t};\n"); // end of points
        fprintf(fp,"\t};\n"); // end of curve

    } // end of all curves

    fprintf(fp,"};\n"); // end of roi
    fclose(fp);

    return(OKFlag);
}
/* ***************************************************************************************************** */
/// Krishni's overload of the function which notes if mesh is on or not
int write_pinnacle_roi(char *pbeam_fname_stem,int mesh_on,
                       //                      char *roi_name,
                       roi_type *roi)
{
    /* open the file to write to */
    char roi_fname[MAX_STR_LEN];
    strcpy(roi_fname,pbeam_fname_stem);  // put first part on name
    strcat(roi_fname,".roi");          // append .Trial to the name
#ifdef DEBUG
    printf("\n DEBUG MODE:  Reading In ROI's from file %s",roi_fname);
#endif
    FILE *fp;
    fp = fopen(roi_fname,"a");
    if(fp == NULL)
    {
        eprintf("\n ERROR: opening output file %s\n",roi_fname);return(FAIL);
    }

    // now start writing the file

    //KW:  If  Pin 7.7 and above write only mesh vertices and triangles
    //     but no curves.

    if(1 == mesh_on )  // JVS: Fixed, was mesh_on = 1
    {
        printf ("\n mesh flag detected. using vertices instead of points in write_pinnacle_roi");

        fprintf(fp,"roi={\n");
        //fprintf(fp,"name: %s\n",roi_name);
        fprintf(fp,"name: %s\n",roi->name);
        fprintf(fp,"volume_name: %s\n",roi->volume_name);
        fprintf(fp,"color: %s\n",roi->color);
        fprintf(fp,"num_curve = %d;\n",0);



        //KW: Surface mesh

        fprintf(fp,"\tsurface_mesh={\n");
        fprintf(fp,"number_of_vertices = %d\n",roi->mesh[0].n_vertices);
        fprintf(fp,"number_of_triangles = %d\n",roi->mesh[0].n_triangles);
        fprintf(fp,"\tvertices={\n");
        //printf("\n KW: TEST 2");

        for(int i = 0; i < roi->mesh[0].n_vertices; i++)
        {
            fprintf(fp,"%3.3f %3.3f %3.3f\n",roi->mesh[0].vertex[i].x,roi->mesh[0].vertex[i].y,roi->mesh[0].vertex[i].z);

        } // end of vertices
        fprintf(fp,"\t\t};\n"); // end of vertices

        fprintf(fp,"\ttriangles={\n");
        for(int i = 0; i < roi->mesh[0].n_triangles; i++)
        {
            fprintf(fp,"%i %i %i\n",roi->mesh[0].triangle[i].x,roi->mesh[0].triangle[i].y,roi->mesh[0].triangle[i].z);

        } // end of triangles
        fprintf(fp,"\t\t};\n"); // end of triangles
        fprintf(fp,"\t\t};\n"); // end of Surface mesh


        //KW: Mean mesh
        fprintf(fp,"\tmean_mesh={\n");
        fprintf(fp,"number_of_vertices = %d\n",roi->mesh[1].n_vertices);
        fprintf(fp,"number_of_triangles = %d\n",roi->mesh[1].n_triangles);
        fprintf(fp,"\tvertices={\n");
        //printf("\n KW: TEST 2");

        for(int i = 0; i < roi->mesh[1].n_vertices; i++)
        {
            fprintf(fp,"%3.3f %3.3f %3.3f\n",roi->mesh[1].vertex[i].x,roi->mesh[1].vertex[i].y,roi->mesh[1].vertex[i].z);

        } // end of vertices
        fprintf(fp,"\t\t};\n"); // end of vertices

        fprintf(fp,"\ttriangles={\n");
        for(int i = 0; i < roi->mesh[1].n_triangles; i++)
        {
            fprintf(fp,"%i %i %i\n",roi->mesh[1].triangle[i].x,roi->mesh[1].triangle[i].y,roi->mesh[1].triangle[i].z);

        } // end of triangles
        fprintf(fp,"\t\t};\n"); // end of triangles
        fprintf(fp,"\t\t};\n"); // end of mean mesh
        fprintf(fp,"};\n"); // end of roi
        fclose(fp);
    }
    else
    {
        return(FAIL);
    }
    return(OKFlag);
}
/* ***************************************************************************************************** */
