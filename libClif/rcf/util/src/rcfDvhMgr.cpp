#include <string.h>
#include <stdio.h>
#include "rcfDvhMgr.hpp"

// private function basicInit is a placeholder for 
// initializations and defaults

void rcfDvhMgr::basicInit()
{
   dvh_nbins = 0;
   dvh = NULL;
   ddvh = NULL;
   compute_dvh = 1;
   valid_dvh = 0;
   compute_ddvh = 1;
   valid_ddvh = 0;
   ddvh_underflow = 0.0;
   ddvh_overflow = 0.0;
   voxel_volume_buf = NULL;
   manage_voxel_volume_buf = false;
   voxel_length = 0;
   voxel_volume = 1.0;
   dose_grid = NULL;
   grid_length = 0;
   mask_buffer = NULL;
   mask = 0;
   mask_buffer_length = 0;
   autorange = 1;
   userrange = 0;
   userrange_min = 0.0;
   userrange_max = 0.0;
   userfrac = 0.0;
   min_dose = 0.0;
   max_dose = 0.0;
   roi_name = NULL;
   output_path = NULL;
   outp_dvh = 1;
   outp_ddvh = 1;
   working_dose = NULL;
   working_dose_length = 0;
   working_dose_allocated = 0;
   working_volume = NULL;
   working_volume_length = 0;
   working_volume_allocated = 0;
   dose_and_volume_ready = 0;
   debug_flag = 0;
   rel_volume = 0;
   compute_complex_stats = true;
   isKurtosis = false;
   isSkewness = false;
   userIndex.clear();
   userIndexIntegral.clear();
   leftIndex.clear();
   rightIndex.clear();
   indexIntegral.clear();
   enableGeud = false;
   geudExponent = 0.0;
   bin_size = 0.0;
   is_bin_size_set = false;
   
}

rcfDvhMgr::rcfDvhMgr()
{
   basicInit();
}


int rcfDvhMgr::setBins(int nbins)
{

   if(dvh_nbins > 0)clearDvh();

   if(nbins <= 0)
   {
      fprintf(stderr,"ERROR rcfDvhMgr::rcfDvhMgr(int) illegal value for the number of bins (less or equal zero): %d\n",nbins);
      return(CF_FAIL);
   }
   // allocate memory
   dvh = new float[nbins];
   if(NULL == dvh)
   {
      fprintf(stderr,"ERROR rcfDvhMgr::rcfDvhMgr(int) Failure to allocate memory for the cumulative dvh. Size of request: %d\n",(int)(nbins*sizeof(float)));
      return(CF_FAIL);
   }
   ddvh = new float[nbins];
   if(NULL == dvh)
   {
      fprintf(stderr,"ERROR rcfDvhMgr::rcfDvhMgr(int) Failure to allocate memory for the differential dvh. Size of request: %d\n",(int)(nbins*sizeof(float)));
      return(CF_FAIL);
   }
   compute_dvh = 1;
   compute_ddvh = 1;
   dvh_nbins = nbins;
   // make sure memory is set to zero
   memset(dvh,0,nbins*sizeof(float));
   memset(ddvh,0,nbins*sizeof(float));
   return(CF_OK);
}


rcfDvhMgr::rcfDvhMgr(int nbins)
{
   basicInit();

   int rtn;

   rtn = setBins(nbins);
   if(CF_OK != rtn)
   {
      fprintf(stderr,"ERROR rcfDvhMgr::rcfDvhMgr failure to initialize the rcfDvhMgr object with the requested number of bins: %d\n",nbins);
      return;
   }
}


rcfDvhMgr::~rcfDvhMgr()
{
   if(NULL != dvh)
   {
       delete[] dvh;
       dvh = NULL;
   }   
   if(NULL != ddvh)
   {
       delete[] ddvh;
       ddvh = NULL;
   }
   if(NULL != roi_name)
   {
       delete[] roi_name;
       roi_name = NULL;
   }
   if(NULL != output_path)
   {
       delete[] output_path;
       output_path = NULL;
   }
   if(NULL != working_dose)
   {
       delete[] working_dose;
       working_dose = NULL;
   }
   if(NULL != working_volume)
   {
       delete[] working_volume;
       working_volume = NULL;
   }      
}

void rcfDvhMgr::clearDvh()
{
   if(NULL != dvh)
   {
      delete[] dvh;
      dvh = NULL;
   }
   if(NULL != ddvh)
   {
      delete[] ddvh;
      ddvh = NULL;
   }
   compute_dvh = 0;
   compute_ddvh = 0;
   dvh_nbins = 0;
}


// post-initialization member functions

// this function will de-allocate memory for the dvh and
// re-allocate it (no de-allocation if memory is not there)
int rcfDvhMgr::setNumberOfBins(int nbins)
{
   return(setBins(nbins));
}
int rcfDvhMgr::getNumberOfBins(int *valid)
{
   if( (NULL == dvh) && (NULL == ddvh) )
   {
      *valid = 0;
      return(0);
   }
   *valid = 1;
   return(dvh_nbins);
}

int rcfDvhMgr::setBinSize(float ubinSize)
{
   // sanity check
   if(ubinSize <= 0)
   {
      fprintf(stderr,"rcfDvhMgr::setBinSize user attempts to set bin size to zero or negative value. Value detected: %f\n",ubinSize); 
      return(CF_FAIL);
   }
   if( (min_dose <= 0) || (max_dose <= 0) )
   {
      // cant allocate memory yet, just record the value and mark user request
      bin_size = ubinSize;
      is_bin_size_set = true;
      return(CF_OK);
   }
   else
   {
      // possibly we have all the information we need to allocate memory
      float dose_interval = max_dose - min_dose;
      if(dose_interval <= 0)
      {
         fprintf(stderr,"rcfDvhMgr::setBinSize min max dose values are detected, but appear to be inconsistent.  Dose interval computed: %f\n",dose_interval); 
         
         return(CF_FAIL);
      }
      // dose interval is legitimate, proceed
      int nbins = (int)(dose_interval/ubinSize);
      int rtn = setBins(nbins);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"rcfDvhMgr::setBinSize memory allocation failed. Number of bins: %d\n",nbins); 
         return(CF_FAIL);
      }
      return(CF_OK);
   }
   return(CF_OK);
}

int rcfDvhMgr::computeDvh()
{
   int rtn;

   // sanity checks
   if( (0==compute_dvh) && (0==compute_ddvh) )
   {
      // nothing to compute. Issue warning, exit with error
      fprintf(stderr,"rcfDvhMgr::computeDvh has been called, but object state indicates that no computation needs to be performed. compute_dvh: %d  compute_ddvh: %d\n",compute_dvh,compute_ddvh); 
      return(CF_FAIL);
   }

   if( (1==autorange) && (1==userrange) )
   {
      // contradictory condition. Either autorange is requested for the dvh, or user has already set a range.
      fprintf(stderr,"rcfDvhMgr::computeDvh Detects internal inconsistency. You can either request autoranging of the DVH, or you can define your own range, not both at once. autorange: %d userrange: %d\n",autorange,userrange); 
      return(CF_FAIL);
   }

   // prepare dose and volume
   rtn = prepareDoseAndVolume();
   if( CF_OK != rtn )
   {
      fprintf(stderr,"rcfDvhMgr::computeDvh Failure to prepare Dose and Volume with return: %d\n",rtn); 
      return(CF_FAIL);
   }

   if( (1 == autorange) && (userfrac <= 0.0) )
   {
      // This section computes dvh by automatically computing
      // dvh range, without any user interference

      // compute min and max of the dose
      if(CF_OK != computeRange())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute dose range\n"); 
         return(CF_FAIL);
      }
      // compute differential dvh 
      if(CF_OK != computeDiffDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Differential Dvh - autorange path\n"); 
         return(CF_FAIL);
      }
      // compute cumulative dvh 
      if(CF_OK != computeCumulDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Cumulative Dvh - autorange path\n"); 
         return(CF_FAIL);
      }
   }
   else if( (1 == autorange) && (userfrac > 0) )
   {
      // This section computes dvh by automatically computing
      // dvh range, and then re-computing the range by truncating
      // the tail of the cumulative dvh according to user supplied
      // fraction of the volume parameter (userfrac). This option
      // would be used most often with Brachytherapy DVH. The object will
      // maintain under and overflows for the differential DVH
      // compute min and max of the dose

      if(CF_OK != computeRange())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute dose range\n"); 
         return(CF_FAIL);
      }
      // compute differential dvh 
      if(CF_OK != computeDiffDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Differential Dvh - limited autorange path\n"); 
         return(CF_FAIL);
      }
      // compute cumulative dvh 
      if(CF_OK != computeCumulDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Cumulative Dvh - limited autorange path\n"); 
         return(CF_FAIL);
      }
      // Rescale the limits 
      if(CF_OK != computeRescaledDvhLimits())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to Rescale Dvh limits - limited autorange path\n"); 
         return(CF_FAIL);
      }
      // compute differential dvh 
      if(CF_OK != computeDiffDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Differential Dvh FOLLOWING RESCALING - limited autorange path\n"); 
         return(CF_FAIL);
      }
      // compute cumulative dvh 
      if(CF_OK != computeCumulDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Cumulative Dvh FOLLOWING RESCALING - limited autorange path\n"); 
         return(CF_FAIL);
      }
   }
   else
   {
      min_dose = userrange_min;
      max_dose = userrange_max;

      // compute differential dvh 
      if(CF_OK != computeDiffDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Differential Dvh - user defined limits path\n"); 
         return(CF_FAIL);
      }
      // compute cumulative dvh 
      if(CF_OK != computeCumulDvh())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute Cumulative Dvh - user defined limits path\n"); 
         return(CF_FAIL);
      }
   }

   if(enableGeud)
   {
      if(CF_OK != computeGeud())
      {
         fprintf(stderr,"rcfDvhMgr::computeDvh Failure to compute GEUD\n"); 
         return(CF_FAIL);
      }
   }


   if(1 == debug_flag)fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::computeDvh - Dvh computed completed\n");

   return(CF_OK);

}

// the function computeStandardDvh computes Standard Differential DVH,
// without any rescaling

int rcfDvhMgr::computeDiffDvh()
{
   float step;
   int i;
   float andex;
   int index;
   float l_min_dose,l_max_dose;
   int rtn;


   if(1 != dose_and_volume_ready)
   {
      fprintf(stderr,"rcfDvhMgr::computeDiffDvh You can not compute the dvh until dose and volume arrays are allocated and prepared\n");
      return(CF_FAIL);
   }


   // sanity checks
   if( dvh_nbins <= 0) 
   {
      fprintf(stderr,"rcfDvhMgr::computeDiffDvh detects illegal number of dvh bins. dvh_nbins: %d\n",dvh_nbins);
      return(CF_FAIL);
   }

   // zero the histogram buffer
   memset(ddvh,0,dvh_nbins*sizeof(float));
   valid_ddvh = 0;

   if( 1 == userrange)
   {
      l_min_dose = userrange_min;
      l_max_dose = userrange_max;
   }
   else
   {
      l_min_dose = min_dose;
      l_max_dose = max_dose;
   }

  
   step = (l_max_dose - l_min_dose)/((float)dvh_nbins);

   if( step <= 0.0 )
   {
      fprintf(stderr,"rcfDvhMgr::computeDiffDvh detects illegal computed step: %f max_dose: %f min_dose: %f dvh_nbins: %d\n",step,max_dose,min_dose,dvh_nbins);
      return(CF_FAIL);
   }

   // compute the standard case 
   ddvh_underflow = 0.0;
   ddvh_overflow = 0.0;
   for(i=0; i<working_dose_length; i++)
   {
     // handle underflow - overflow
      if(working_dose[i] < l_min_dose )
      {
         ddvh_underflow += working_volume[i];
         continue;
      } 
      if(working_dose[i] > l_max_dose )
      {
         ddvh_overflow += working_volume[i];
         continue;
      } 
      andex = working_dose[i] - l_min_dose;
      andex /= step;
      index = (int)andex; 
      if(index == dvh_nbins)index--; // handle upper boundary 
      if(index > dvh_nbins)
      {
         /* application error */
         fprintf(stderr,"ERROR: rcfDvhMgr::computeDiffDvh : apparent inconsistency between computed limits and data: index: %d  dvh_nbins: %d \n",index,dvh_nbins);
         return(CF_FAIL);
      }
      if(index <0)
      {
         /* application error */
         fprintf(stderr,"WARNING: rcfDvhMgr::computeDiffDvh: apparent inconsistency between computed limits and data with index: %d dose: %f  min_dose: %f \n",index,dose_grid[i],min_dose);
         index=0;
      }
      ddvh[index]+= working_volume[i];
   }
   if(compute_complex_stats)
   {
      rtn = computeDiffStats();
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::computeDiffDvh : Failure to compute differential statistics \n");
         return(CF_FAIL);
      }
   }
   valid_ddvh = 1;
   return(CF_OK);
}

int rcfDvhMgr::computeDiffStats()
{
   double ave = 0.0;
   double avesq = 0.0;
   double avecube = 0.0;
   double avefour = 0.0;
   double sigma;
   bool isIndex = false;
   bool isIndexIntegral = false;
   double frequency;

   if( userIndex.size() > 0 )isIndex = true;
   if( userIndexIntegral.size() > 0 )isIndexIntegral = true;

   if(1 != dose_and_volume_ready)
   {
      fprintf(stderr,"rcfDvhMgr::computeStandardDvh You can not compute the dvh until dose and volume arrays are allocated and prepared\n");
      return(CF_FAIL);
   }
   for(int i=0; i<working_dose_length; i++)
   {
      frequency = working_volume[i]/roi_volume;
      ave += working_dose[i]*frequency;
      avesq += (working_dose[i]*working_dose[i])*frequency;
   }
   sigma = avesq - ave*ave;
   if(sigma < 0.0)sigma = -sigma; // could happen numerically
   variance = sqrt(sigma);
   average = ave;

   if(isSkewness)
   {
      // calculate skewness
      avecube = 0.0;
      for(int i=0; i<working_dose_length; i++)
      {
         frequency = working_volume[i]/roi_volume;
         avecube += working_dose[i]*working_dose[i]*working_dose[i]*frequency;
      }
      skewness = (avecube - (3.0*avesq*ave) + (2*ave*ave*ave))/(variance*variance*variance);
   }
   if(isKurtosis)
   {
      // calculate kurtosis 
      avefour = 0.0;
      if(!isSkewness)
      {
         avecube = 0.0;
         for(int i=0; i<working_dose_length; i++)
         {
            frequency = working_volume[i]/roi_volume;
            avecube += working_dose[i]*working_dose[i]*working_dose[i]*frequency;
            avefour += working_dose[i]*working_dose[i]*working_dose[i]*working_dose[i]*frequency;
         }
      }
      else
      {
         for(int i=0; i<working_dose_length; i++)
         {
            frequency = working_volume[i]/roi_volume;
            avefour += working_dose[i]*working_dose[i]*working_dose[i]*working_dose[i]*frequency;
         }
      }
      kurtosis = (avefour - (4.0*avecube*ave) + (6.0*avesq*ave*ave) -(3.0*ave*ave*ave*ave))/(variance*variance*variance*variance);
   }

   if(isIndex)
   {
      double lastave = 0.0;
      double boundary;
      double delta;
      double ratio;
      float doselo,dosehi;
      ave = ddvh[0];
      leftIndex.clear();
      rightIndex.clear();
      float step = (max_dose - min_dose)/((float)dvh_nbins); 

      for(int i=1; i<dvh_nbins; i++)
      {
         lastave = ave;
         ave += ddvh[i];
         for(unsigned int ii=0; ii<userIndex.size(); ii++) 
         {
            boundary = userIndex[ii]*roi_volume;
            if(boundary <= ddvh[0])
            {
               //special case, a bit questionable, but best we can do
               leftIndex[userIndex[ii]] = min_dose;
               continue;
            }
            dosehi = min_dose + i*step + 0.5*step;
            doselo = min_dose + (i-1)*step + 0.5*step;
            if( (ave>boundary) && (boundary>lastave) )
            {
               //compute the left index via interpolation
               delta = dosehi - doselo;
               ratio = (boundary-lastave)/(ave-lastave);
               leftIndex[userIndex[ii]] = doselo + (delta*ratio);
            }
         }
         for( unsigned int ii=0; ii<userIndex.size(); ii++ ) 
         {
            boundary = (1.0-userIndex[ii])*roi_volume;
            if( (ave>boundary) && (boundary>lastave) )
            {
               //compute the left index via interpolation
               delta = dosehi - doselo;
               ratio = (boundary-lastave)/(ave-lastave);
               rightIndex[userIndex[ii]] = doselo + (delta*ratio);
            }
         }
      }
   }

   if(isIndexIntegral)
   {
      double limit;
      double dosehi,doselo;
      double sum,prevsum;
      float step = (max_dose - min_dose)/((float)dvh_nbins); 
      float deltadose,fracdose,deltasum;
      indexIntegral.clear();

      for( unsigned int ii=0; ii<userIndexIntegral.size(); ii++ )
      {
         limit = userIndexIntegral[ii];
         if(limit <= min_dose)
         {
            indexIntegral[limit] = 0.0;
            continue; // boundary condition, continue loop over indices
         }
         if(limit >= max_dose)
         {
            indexIntegral[limit] = 1.0;
            continue; // boundary condition, continue loop over indices
         }
         sum = dvh[0];
         for(int i=1; i<dvh_nbins; i++)
         {
            prevsum = sum;
            sum += ddvh[i];
            dosehi = min_dose + i*step;
            doselo = min_dose + (i-1)*step;
            if( dosehi >= limit)
            {
               deltadose = limit - doselo;
               fracdose = deltadose/step;
               deltasum = sum - prevsum;
               indexIntegral[limit] = prevsum + (fracdose*deltasum);
               break; // no point completing this inner loop
            }
         }
      }
   }

   return(CF_OK);
}

// calculate cumulative dvh, based on the differential dvh 
int rcfDvhMgr::computeCumulDvh()
{
   int i,ii;
   int rtn;
   // zero the histogram buffer
   memset(dvh,0,dvh_nbins*sizeof(float));

   valid_dvh = 0;

   for(i=0; i<dvh_nbins; i++)
   {
      for(ii=dvh_nbins-1; ii >= i; ii--)
      {
         dvh[i] += ddvh[ii];
      }
      /* add overflows in case differential histo was rescaled */
     dvh[i] += ddvh_overflow;
   }
   valid_dvh = 1; // cumulative dvh is ready if we are here

   if(compute_complex_stats)
   {
      rtn = computeCumulStats();
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::computeCumulDvh : Failure to compute cumulative statistics \n");
         return(CF_FAIL);
      }
   }
   return(CF_OK);
}

int rcfDvhMgr::computeCumulStats()
{
   if(0 == valid_dvh)
   {
      fprintf(stderr,"ERROR:rcfDvhMgr::computeCumulStats request to compute cumulative statistics, but the cumulative dvh has not been computed\n");
      return(CF_FAIL);
   }

   double lastave;
   double boundary;
   float dosehi,doselo;
   float delta,ratio;
   double ave = dvh[0];
   float step = (max_dose - min_dose)/((float)dvh_nbins); 

   for(int i=1; i<dvh_nbins; i++)
   {
      lastave = ave;
      ave = dvh[i];
      for( unsigned int ii=0; ii<userIndex.size(); ii++ )
      {
         boundary = userIndex[ii]*roi_volume;
         dosehi = min_dose + i*step + 0.5*step;
         doselo = min_dose + (i-1)*step + 0.5*step;
         if( (ave<boundary) && (boundary<lastave) )
         {
            //compute the left index via interpolation
            delta = dosehi - doselo;
            ratio = (lastave-boundary)/(lastave-ave);
            indexCumul[userIndex[ii]] = doselo + (delta*ratio);
         }
      }
   }
   return(CF_OK);
}

int rcfDvhMgr::computeRescaledDvhLimits()
{
   int i;
   float l_dvh_max;
   float step;

   if(0 == valid_dvh)
   {
      // the dvh has to be computed before rescaling limits can be computed
      fprintf(stderr,"ERROR: rcfDvhMgr::computeRescaledDvhLimits the dvh has to be computed before rescaling can be attempted \n");
      return(CF_FAIL);
   }
   l_dvh_max = dvh[0]*userfrac;
   step = (max_dose - min_dose)/((float)dvh_nbins);

   for(i=0; i<dvh_nbins; i++)
   {
      if( dvh[i] < l_dvh_max )
      {
         // we found our bin. Mark limits accordingly.
         userrange = 1;
         autorange = 0;
         userrange_min = min_dose;
         userrange_max = min_dose + (i*step); 
         if(is_bin_size_set)
         {
            // user wants fixed bin size, recompute number of bins
            int nbins = (int)((userrange_max - userrange_min)/bin_size);
            int rtn = setBins(nbins);
            if(CF_OK != rtn)
            {
               fprintf(stderr,"ERROR: rcfDvhMgr::computeRescaledDvhLimits failed attempt to recompute number of bins based on user defined bin size. Recomputed number of bins: %d \n",nbins);
               return(CF_FAIL);
            }
         }
         return(CF_OK);
      }
   }

   // if we are here, we have not found rescaling maximum
   return(CF_FAIL);
}

// compute min-max dose prior to dvh computation
int rcfDvhMgr::computeRange()
{
   float lmax = -1.;
   float lmin = 1.E+06;

   for(int i=0; i<working_dose_length; i++)
   {
      if(working_dose[i] > lmax)lmax = working_dose[i];
      if(working_dose[i] < lmin)lmin = working_dose[i];
   }

   if( (lmax == lmin) || (lmin > lmax) )
   {
      //error. Inconsistent range has been computed
      fprintf(stderr,"ERROR: rcfDvhMgr::computeRange computed illegal minimum maximum dose values. An illegal condition. Computed minimum: %f Computed Maximum: %f\n",lmin,lmax);
      return(CF_FAIL);
   }
   min_dose = lmin;
   max_dose = lmax;

   // if user set bin size, recompute bins and reallocate memory
   if(is_bin_size_set)
   {
      int nbins = (int)((max_dose-min_dose)/bin_size);
      int rtn = setBins(nbins);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::computeRange failure to reinitialze histogram after number of bins is recomputed following ranging with bin size requested by user. Number of bins computed: %d\n",nbins);
         return(CF_FAIL);
      }
   }

   return(CF_OK);
}

int rcfDvhMgr::prepareDoseAndVolume()
{
   int i;
   int length = 0;
   int rtn;
   int valid;

   rtn = testDataReady(&valid);
   if((CF_OK != rtn) || (0 == valid) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume DVH data apparently invalid or illegal\n");
      return(CF_FAIL);
   }

   //first calculate the length of needed buffer
   if( (NULL != mask_buffer) && (mask_buffer_length>0) && (mask>0) )
   {
      for(i=0; i<mask_buffer_length; i++)
      {
         if( (mask_buffer[i] & mask) > 0 )length++;
      }
   }
   else
   {
      length = grid_length;
   }


   if( 0 == length )
   {
      // complain, apparently no content for this ROI
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume:  The structure is apparently empty. Zero length detected: %d \n",length);
      if(NULL != roi_name)fprintf(stderr,"ROI NAme: %s\n",roi_name);
      return(CF_FAIL);
   }

   // allocate memory for dose
   working_dose = new float[length];
   if(NULL == working_dose)
   {
      // complain, memory allocation
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume: Failure to allocate memory for working dose. Size of request: %d\n",(int)(length*sizeof(float)));
      return(CF_FAIL);
   }
   working_dose_length = length;
   working_dose_allocated = 1;

   // allocate memory for volume, 
   working_volume = new float[length];
   if(NULL == working_volume)
   {
      // complain, memory allocation
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume: Failure to allocate memory for working volume. Size of request: %d\n",(int)(length*sizeof(float)));
      return(CF_FAIL);
   }
   working_volume_length = length;
   working_volume_allocated = 1;

   int run_index = 0;
   // construct buffers
   if( (NULL != mask_buffer) && (NULL != voxel_volume_buf) )
   {
      // volume correction buffer aligned with dose buffer, mask present 
      run_index = 0;
      roi_volume = 0;
      roi_length = 0;
      for(i=0; i<grid_length; i++)
      {
            if( (mask & mask_buffer[i]) > 0)
            {
              working_dose[run_index] = dose_grid[i];
              working_volume[run_index] = voxel_volume*voxel_volume_buf[i]; 
              roi_volume += working_volume[run_index];
              roi_length++;
              run_index++;
            }
      }
   }
   else if( (NULL != mask_buffer) && (NULL == voxel_volume_buf) )
   {
      // No volume correction 
      roi_volume = 0;
      roi_length = 0;
      run_index = 0;
      for(i=0; i<grid_length; i++)
      {
         if( (mask & mask_buffer[i]) > 0)
         {
           working_dose[run_index] = dose_grid[i];
           working_volume[run_index] = voxel_volume; 
           roi_volume += working_volume[run_index];
           roi_length++;
           run_index++;
         }
      }
   }
   else if( (NULL == mask_buffer) && (NULL != voxel_volume_buf ) )
   {
      // no mask, but volume correction is set, volume correction and dose buffers aligned
      roi_volume = 0;
      for(i=0; i<grid_length; i++)
      {
         working_dose[i] = dose_grid[i];
         working_volume[i] = voxel_volume*voxel_volume_buf[i]; 
         roi_volume += working_volume[i];
      }
      roi_length = grid_length;
   }
   else if( (NULL == mask_buffer) && (NULL == voxel_volume_buf) )
   {
      // No volume (or density) correction study set, and no mask
      // treat whole dose grid as a single ROI, apply single voxel volume
      roi_volume = 0;
      for(i=0; i<grid_length; i++)
      {
         working_dose[i] = dose_grid[i];
         working_volume[i] = voxel_volume; 
         roi_volume += working_volume[i];
      }
      roi_length = grid_length;
   }
   else
   {
      // complain, unsupported object state 
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume: reports an unsupported object state\n");
      return(CF_FAIL);
   }


   // compute range
   rtn = computeRange();
   if(CF_OK != rtn)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::prepareDoseAndVolume: Failure to compute range of the working dose buffer with return: %d\n",rtn);
      return(CF_FAIL);
   }

   dose_and_volume_ready = 1;

   return(CF_OK);
}

int rcfDvhMgr::testDataReady(int *valid)
{
   *valid = 0;
   if(NULL == dose_grid)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::testDataReady reports that the dose grid is NOT set. The dose grid MUST be set to compute the DVH\n");
      return(CF_FAIL);
   }
   if(voxel_volume <= 0)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::testDataReady reports that voxel volume is negative or zero\n");
      return(CF_FAIL);
   }

   if( (NULL != mask_buffer) && (NULL != voxel_volume_buf) )
   {
      if(grid_length != voxel_length)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::testDataReady reports that mask is present, voxel corrections is present, voxel index is ABSENT, and voxel length is NOT EQUAL to grid_length. Illegal data combination\n");
         return(CF_FAIL);
      }
      if(1 == debug_flag)
      {
         fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::testDataReady reports that mask is set, and the volume reference is set\n");
      }
      *valid = 1;
      return(CF_OK);
   }
   else if( (NULL == mask_buffer) && (NULL != voxel_volume_buf) )
   {
      // verify lengths
      if(grid_length != voxel_length)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::testDataReady reports that mask is ABSENT, voxel corrections is present and voxel length is NOT EQUAL to grid_length. Illegal data combination\n");
         return(CF_FAIL);
      }
      if(1 == debug_flag)
      {
         fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::testDataReady reports that mask is NOT set, and the volume reference IS set, The whole dose buffer will be treated as a single ROI, but volumes will be modified according to user supplied volume modification buffer\n");
      }
      *valid = 1;
      return(CF_OK);
   }
   else if( (NULL == mask_buffer) && (NULL == voxel_volume_buf) )
   {
      if(1 == debug_flag)
      {
         fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::testDataReady reports that mask is NOT set, and the volume reference is NOT set, The whole dose buffer will be treated as a single ROI without volume corrections\n");
      }
      *valid = 1;
      return(CF_OK);
   }
   else
   {
      if(1 == debug_flag)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::testDataReady reports an illegal combination of data. DVH can NOT be computed\n");
      }
      *valid = 0;
      return(CF_FAIL);
   }

   return(CF_OK);
}


int rcfDvhMgr::outputDvh()
{
  int nopath = 0;
  int noname = 0;
  char default_name[8] = "unknown";
  char diffend[6] = ".ddvh";
  char cumulend[5] = ".dvh";
  char stats[6] = ".stat";
  char *buffer = NULL;
  char *core_name = NULL;
  int length = 0;
  FILE *fid_dif = NULL;
  FILE *fid_cumul = NULL;
  FILE *fid_stats = NULL;

  if( (0 == outp_dvh) && (0 == outp_ddvh) )
  {
     // nothing to do, just get out
     return(CF_OK);
  }

  if(1 == rel_volume)
  {
     // sanity check on the roi_volume
     if(roi_volume <= 0.0 )
     {
        // this should have been calculated by default
        // during dvh calculation. Otherwise, object state error
        fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh detects uncomputed roi_volume. Value seen: %f\n",roi_volume);
        return(CF_FAIL);
     }
  }

  // check for explicit output path 
  if(NULL == output_path)
  {
     // path not defined, marked as such
     nopath = 1;
  }
  else if(0 == strlen(output_path))
  {
     // path not defined, marked as such
     nopath = 1;
  }
  else
  {
     length += strlen(output_path);
  }
  // check if name is defined
  if(NULL == roi_name)
  {
     // name not defined, marked as such
     noname = 1;
     length += 8;
  }
  else if(0 == strlen(roi_name))
  {
     // name not defined, marked as such
     noname = 1;
     length += 8;
  }
  else
  {
     length += strlen(roi_name);
  }

  // pad length generously for endings and slashes
  length += 20;

  // allocate memory for core name
  core_name = new char[length];
  if(NULL == core_name)
  {
     fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh failure to allocate memory. Size of request: %d\n",length);
     return(CF_FAIL);
  }
  if(0 == nopath)
  {
     strcpy(core_name,output_path);
     strcat(core_name,"/");
     if(0 == noname) strcat(core_name,roi_name);
     else strcat(core_name,default_name);
  }
  else
  {
     if(0 == noname) strcpy(core_name,roi_name);
     else strcpy(core_name,default_name);
  }

  length = strlen(core_name);
  length += 20;
  buffer = new char[length];
  if(NULL == buffer)
  {
     fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh failure to allocate memory. Size of request: %d\n",length);
     return(CF_FAIL);
  }

  // prepare files 
  if( 1 == outp_ddvh)
  {
    //output differential dvh
    strcpy(buffer,core_name);
    strcat(buffer,diffend);
    fid_dif = fopen(buffer,"w");
    if(NULL == fid_dif) 
    {
     fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh failure to open output file: %s Invalid Path?\n",buffer);
     if( NULL != buffer )
     {
        delete[] buffer;
        buffer = NULL;
     }
     if( NULL != core_name )
     {
        delete[] core_name; 
        core_name = NULL;
     }
     return(CF_FAIL);
    }
    else
    {
       if(1 == debug_flag)
       {
          fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::outputDvh opened output file for differential dvh: %s\n",buffer);
       }
    }
  }
  if( 1 == outp_dvh)
  {
    //output differential dvh
    strcpy(buffer,core_name);
    strcat(buffer,cumulend);
    fid_cumul = fopen(buffer,"w");
    if(NULL == fid_cumul) 
    {
     fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh failure to open output file: %s Invalid Path?\n",buffer);
     if( NULL != buffer )
     {
        delete[] buffer;
        buffer = NULL;
     }
     if( NULL != core_name )
     {
        delete[] core_name; 
        core_name = NULL;
     }
     return(CF_FAIL);
    }
    else
    {
       if(1 == debug_flag)
       {
          fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::outputDvh opened output file for cumulative dvh: %s\n",buffer);
       }
    }
  }

  //output statistics unconditionally 
  strcpy(buffer,core_name);
  strcat(buffer,stats);
  fid_stats = fopen(buffer,"w");
  if(NULL == fid_stats) 
  {
     fprintf(stderr,"ERROR: rcfDvhMgr::outputDvh failure to open statistics file: %s Invalid Path?\n",buffer);
     if( NULL != buffer )
     {
        delete[] buffer;
        buffer = NULL;
     }
     if( NULL != core_name )
     {
        delete[] core_name; 
        core_name = NULL;
     }
     return(CF_FAIL);
  }
  else
  {
     if(1 == debug_flag)
     {
        fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::outputDvh opened output file for dvh statistics: %s\n",buffer);
     }
  }

  // files opened, delete names
  if( NULL != buffer )
     {
        delete[] buffer;
        buffer = NULL;
     }
     if( NULL != core_name )
     {
        delete[] core_name; 
        core_name = NULL;
     }

  // files open, dump
  float step;
  int iloop;
  float dose;
  step = (max_dose - min_dose)/((float)dvh_nbins); 
  if( NULL != fid_dif )
  {
     for(iloop=0; iloop<dvh_nbins; iloop++)
     {
        dose = min_dose + iloop*step + 0.5*step;
        if(0 == rel_volume) fprintf(fid_dif,"%f \t %f\n",dose,ddvh[iloop]);
        else fprintf(fid_dif,"%f \t %f\n",dose,(ddvh[iloop])/roi_volume);
     }
     fclose(fid_dif);
  }
  if( NULL != fid_cumul )
  {
     for(iloop=0; iloop<dvh_nbins; iloop++)
     {
        dose = min_dose + iloop*step + 0.5*step;
        if(0 == rel_volume)fprintf(fid_cumul,"%f \t %f\n",dose,dvh[iloop]);
        else fprintf(fid_cumul,"%f \t %f\n",dose,(dvh[iloop])/roi_volume);
     }
     fclose(fid_cumul);
  }

  if(NULL != fid_stats)
  {
     fprintf(fid_stats,"General DVH data for ROI: %s\n",roi_name);
     if(NULL != voxel_volume_buf)
     {
        fprintf(fid_stats,"User provided inhomogenous voxel volume corrections \n");
     }
     else
     {
        fprintf(fid_stats,"Common Voxel Volume of: %f  was used in calculation \n",voxel_volume);
     }
     if(1 == autorange)
     {
        fprintf(fid_stats,"Histogram limits were autocomputed\n");
     }
     else if(userfrac > 0.0)
     {
        fprintf(fid_stats,"Histogram limits were autocomputed, and the upper limit of the DVH was truncated to total volume fraction of: %f (as specified by user)\n",userfrac);
     }
     else
     {
        fprintf(fid_stats,"Histogram limits were directly specified by the user\n");
     }
     fprintf(fid_stats,"Histogram Minimum: %f\n",min_dose);
     fprintf(fid_stats,"Histogram Maximum: %f\n",max_dose);
     if(1 == rel_volume)
     {
        fprintf(fid_stats,"DVH printed in units of RELATIVE VOLUME\n");
        fprintf(fid_stats,"Fractional volume of underflows: %f of ROI volume\n",ddvh_underflow/roi_volume);
        fprintf(fid_stats,"Fractional volume of overflows: %f of ROI volume\n",ddvh_overflow/roi_volume);
     }
     else
     {
        fprintf(fid_stats,"DVH printed in units of ABSOLUTE VOLUME\n");
        fprintf(fid_stats,"Volume of underflows: %f\n",ddvh_underflow);
        fprintf(fid_stats,"Volume of overflows: %f\n",ddvh_overflow);
     }
     fprintf(fid_stats,"Computed ROI length: %d\n",roi_length);
     fprintf(fid_stats,"Computed ROI volume: %f\n",roi_volume);
     if(enableGeud)fprintf(fid_stats,"Computed GEUD: %f\n",geudValue);
     if(compute_complex_stats)
     {
        fprintf(fid_stats,"\n Differential Histogram Advanced Statistics:\n");
        fprintf(fid_stats,"Average: %f\n",average);
        fprintf(fid_stats,"Variance: %f\n",variance);
        if(isSkewness)fprintf(fid_stats,"Skewness: %f\n",skewness);
        if(isKurtosis)fprintf(fid_stats,"Kurtosis: %f\n",kurtosis);
        if(leftIndex.size() > 0)
        {
           fprintf(stderr,"\n");
           std::map<float,float>::iterator it;
           for(it = leftIndex.begin(); it != leftIndex.end(); it++) fprintf(fid_stats,"LeftIndex: %f \t  Value: %f\n",(*it).first,(*it).second);
        }
        if(rightIndex.size() > 0)
        {
           fprintf(stderr,"\n");
           std::map<float,float>::iterator it;
           for(it = rightIndex.begin(); it != rightIndex.end(); it++) fprintf(fid_stats,"RightIndex: %f \t  Value: %f\n",(*it).first,(*it).second);
        }
        if(rightIndex.size() > 0)
        {
           fprintf(fid_stats,"\n Cumulative Histogram Advanced Statistics: \n");
           std::map<float,float>::iterator it;
           for(it = indexCumul.begin(); it != indexCumul.end(); it++) fprintf(fid_stats,"Cumulative Index: %f \t  Value: %f\n",(*it).first,(*it).second);
        }
     }
     fclose(fid_stats);
  }
  
  return(CF_OK);
}

int rcfDvhMgr::setUsrDvhLimits(float usrmin, float usrmax)
{
   userrange = 0;
   // sanity checks
   if( usrmax <= usrmin )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setUsrDvhLimits user attempts to set the DVH range, but the limits appear to be reversed. Maximum has to be greater than the minimum. Detected values: min: %f  max: %f\n",usrmin,usrmax); 
      return(CF_FAIL);
   }
   userrange = 1;
   autorange = 0;
   userrange_min = usrmin;
   userrange_max = usrmax;

   return(CF_OK);
}

int rcfDvhMgr::setUsrDvhLimits(float usrmin, float usrmax, float binSize)
{
   userrange = 0;
   // sanity checks
   if( usrmax <= usrmin )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setUsrDvhLimits user attempts to set the DVH range, but the limits appear to be reversed. Maximum has to be greater than the minimum. Detected values: min: %f  max: %f\n",usrmin,usrmax); 
      return(CF_FAIL);
   }
   if(binSize <= 0.0)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setUsrDvhLimits user attempts to set the the bin size to zero or negative value\n"); 
      return(CF_FAIL);
   }
   userrange = 1;
   autorange = 0;
   userrange_min = usrmin;
   userrange_max = usrmax;
   bin_size = binSize;
   is_bin_size_set = true;
   int nbins = (int)((userrange_max - userrange_min)/bin_size);
   int rtn = setBins(nbins);
   if(CF_OK != rtn)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setUsrDvhLimits Failure to allocate histogram memory whith computed bin number: %d\n",nbins); 
      return(CF_FAIL);
   }

   return(CF_OK);
}
int rcfDvhMgr::getUsrDvhLimits(float *usrmin, float *usrmax, int *valid)
{
   *valid = 0;
   // check if the limits have been set
   if(0 == userrange)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setUsrDvhLimits user attempts to get the DVH range, but the limits have not been set. \n"); 
      return(CF_FAIL);
   }
   *usrmin = userrange_min;
   *usrmax = userrange_max;
   *valid = 1;
   return(CF_OK);
}

int rcfDvhMgr::setDvhTruncationFactor(float factor)
{
   // sanity check
   if( (factor <= 0.0) || (factor >= 1.0) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setDvhTruncationFactor. The truncation factor must be greater than 0.0 and less than 1.0. Detected: %f  \n",factor); 
      return(CF_FAIL);
   }
   
   userfrac = factor;
   return(CF_OK);
}

float rcfDvhMgr::getDvhTruncationFactor()
{
  return(userfrac); 
}

int rcfDvhMgr::resetDvh()
{
   if(NULL != dvh)
   {
      memset(dvh,0,dvh_nbins*sizeof(float));
      valid_dvh = 0;
   }

   if(NULL != ddvh)
   {
      memset(ddvh,0,dvh_nbins*sizeof(float));
      valid_ddvh = 0;
   }

   autorange = 1;
   ddvh_underflow = 0.0;
   ddvh_overflow = 0.0;
   userfrac = 0.0;
   userrange = 0;
   userrange_min = 0.0;
   userrange_max = 0.0;

   return(CF_OK);
}

int rcfDvhMgr::setPointerToVoxelVolumeBuffer(float *usrbuf,int length)
{
   // sanity checks
   if(length <= 0 )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToVoxelBuffer reports illegal buffer length passed by user. The length must be positive. Detected: %d\n",length);
      return(CF_FAIL);
   }
   if(grid_length > 0)
   {
      if( grid_length != length )
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToVoxelBuffer reports that grid length has already been set and does not match volume buffer length being declared by user. Buffers must be matched. Grid Length %d \t This Length: %d\n",grid_length,length);
         return(CF_FAIL);
      }
   }
   if( NULL == usrbuf )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToVoxelBuffer reports NULL pointer passed in lieu of buffer.\n");
      return(CF_FAIL);
   }
   voxel_length = length;
   voxel_volume_buf = usrbuf;
   return(CF_OK);
}

int rcfDvhMgr::setPointerToMaskBuffer(unsigned long *usrbuf,int length)
{
   // sanity checks
   if(length <= 0 )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToMaskBuffer reports illegal buffer length passed by user. The length must be positive. Detected: %d\n",length);
      return(CF_FAIL);
   }
   if( NULL == usrbuf )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToMaskBuffer reports NULL pointer passed in lieu of buffer.\n");
      return(CF_FAIL);
   }
   if( grid_length > 0 )
   {
      if(length != grid_length)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToMaskBuffer reports that the declared buffer length differs from an already set dose grid length. Declared length: %d dose grid length: %d:\n",length,grid_length);
         return(CF_FAIL);
      }
   }
   mask_buffer_length = length;
   mask_buffer = usrbuf;
   return(CF_OK);
}
int rcfDvhMgr::setPointerToDoseBuffer(float *usrbuf,int length)
{
   // sanity checks
   if(length <= 0 )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToDoseBuffer reports illegal buffer length passed by user. The length must be positive. Detected: %d\n",length);
      return(CF_FAIL);
   }
   if( NULL == usrbuf )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToDoseBuffer reports NULL pointer passed in lieu of buffer.\n");
      return(CF_FAIL);
   }
   if( mask_buffer_length > 0 )
   {
      if(length != mask_buffer_length)
      {
         fprintf(stderr,"ERROR: rcfDvhMgr::setPointerToDoseBuffer reports that the declared buffer length differs from an already set mask buffer length. Declared length: %d mask buffer length: %d:\n",length,mask_buffer_length);
         return(CF_FAIL);
      }
   }
   dose_grid = usrbuf;
   grid_length = length;
   return(CF_OK);
}
int rcfDvhMgr::setVoxelVolume(float usrvolume)
{
   // sanity checks
   if(usrvolume <= 0 )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setVoxelVolume reports that the declared voxel volume is less or equal to zero.  Detected value: %f\n",usrvolume);
      return(CF_FAIL);
   }
   if(1 == debug_flag)
   {
      fprintf(stderr,"DEBUG_INFO: rcfDvhMgr::setVoxelVolume reports setting the voxel volume: %f\n",usrvolume);
   }
   voxel_volume = usrvolume;
   return(CF_OK);
}
void rcfDvhMgr::setRoiMask(unsigned long usrmask)
{
   mask = usrmask;
}
float* rcfDvhMgr::getCumulDvhBuffer(int *valid)
{
   *valid = 0;
   if(0 == valid_dvh)return(NULL);
   else
   {
      *valid = 1;
      return(dvh);
   }
}
float* rcfDvhMgr::getDiffDvhBuffer(int *valid)
{
   *valid = 0;
   if(0 == valid_ddvh)return(NULL);
   else
   {
      *valid = 1;
      return(ddvh);
   }
}
float rcfDvhMgr::getDvhUnderflows(int *valid)
{
   *valid = 0;
   if(0 == valid_ddvh)return(0.0);
   else
   {
      *valid = 1;
      return(ddvh_underflow);
   }
}
float rcfDvhMgr::getDvhOverflows(int *valid)
{
   *valid = 0;
   if(0 == valid_dvh)return(0.0);
   else
   {
      *valid = 1;
      return(ddvh_overflow);
   }
}
int rcfDvhMgr::setRoiName(char *name)
{
   int length;
   // sanity checks
   if(NULL == name)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setRoiName NULL pointer passed in as ROI name \n");
      return(CF_FAIL);
   }
   if(0 == (length = strlen(name)) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setRoiName empty string passed in as ROI name \n");
      return(CF_FAIL);
   }

   // allocate memory
   roi_name = NULL;
   roi_name = new char[length+1];
   if(NULL == roi_name)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setRoiName failure to allocate memory for the ROI name. Size of request: %d \n",length+1);
      return(CF_FAIL);
   }
   strcpy(roi_name,name);
   
   return(CF_OK);
}
int rcfDvhMgr::setOutputPath(char *name)
{
   int length;
   // sanity checks
   if(NULL == name)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setOutputPath NULL pointer passed in as Output Path \n");
      return(CF_FAIL);
   }
   if(0 == (length = strlen(name)) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setOutputPath empty string passed in as Output Path \n");
      return(CF_FAIL);
   }

   // allocate memory
   output_path = NULL;
   output_path = new char[length+1];
   if(NULL == output_path)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setOutputPath failure to allocate memory for the Output Path. Size of request: %d \n",length+1);
      return(CF_FAIL);
   }
   strcpy(output_path,name);

   return(CF_OK);
}
int rcfDvhMgr::addUserIndexRequest(float value)
{
   float tvalue;
   if(value <= 0.0)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::addUserIndexRequest User index request must be a positive value. Passed: %f \n",value);
      return(CF_FAIL);
   }
   if(value >= 1.0)tvalue = value/100.; //assume user means percent
   else tvalue = value;

   if(tvalue >= 1.0)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::addUserIndexRequest User index request must be less than 1.0 or 100 percent. Value seen %f\n",value);
      return(CF_FAIL);
   }

   for( unsigned int i=0; i<userIndex.size(); i++ )
   {
       if(tvalue == userIndex[i]) { return CF_OK; } // already have it, quit silently
   }
   userIndex.push_back(tvalue);
   return(CF_OK);
}
int rcfDvhMgr::addUserIndexIntegralRequest(float value)
{
   for(unsigned int i=0; i<userIndexIntegral.size(); i++)
   {
       if(value == userIndexIntegral[i]) { return CF_OK; } // already have it, quit silently   
   }
   userIndexIntegral.push_back(value);
   return(CF_OK);
}
void rcfDvhMgr::disableCumulDvhOutput()
{
   outp_dvh = 0;
}
void rcfDvhMgr::disableDiffDvhOutput()
{
   outp_ddvh = 0;
}
void rcfDvhMgr::enableDebug()
{
   debug_flag = 1;
}
void rcfDvhMgr::setVolumeToRelative()
{
   rel_volume = 1;
}

float rcfDvhMgr::getAverage(bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getAverage request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   *valid = true;
   return(average);
}
float rcfDvhMgr::getVariance(bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getVariance request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   *valid = true;
   return(variance);
}
float rcfDvhMgr::getKurtosis(bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getVariance request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   if(!isKurtosis)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getKurtosis request to get Kurtosis, but Kurtosis calculation has not been enabled\n");
      return(0.0);
   }
   *valid = true;
   return(kurtosis);
}
float rcfDvhMgr::getSkewness(bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getVariance request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   if(!isSkewness)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getKurtosis request to get Kurtosis, but Kurtosis calculation has not been enabled\n");
      return(0.0);
   }
   *valid = true;
   return(skewness);
}
float rcfDvhMgr::getUserLeftIndex(int index, bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserLeftIndex request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   if((unsigned int)index >= userIndex.size())
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserLeftIndex index must be positive and within range.  Index: %d  Maximum: %lu\n",index,userIndex.size()-1);
      return(0.0);
   }
   *valid = true;
   return(leftIndex[userIndex[index]]);
}
float rcfDvhMgr::getUserRightIndex(int index, bool *valid)
{
   *valid = false;
   if( (0==valid_ddvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserRightIndex request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   if((unsigned int)index >= userIndex.size())
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserRightIndex index must be positive and within range.  Index: %d  Maximum: %lu\n",index,userIndex.size()-1);
      return(0.0);
   }
   *valid = true;
   return(rightIndex[userIndex[index]]);
}
float rcfDvhMgr::getUserIntegralIndex(int index, bool *valid)
{
   *valid = false;
   if( (0==valid_dvh) || (!compute_complex_stats) )
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserCumulativeIndex request to get statistics, but statistics has not been computed\n");
      return(0.0);
   }
   if(index >= (int)userIndexIntegral.size())
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::getUserCumulativeIndex index must be positive and within range.  Index: %d  Maximum: %lu\n",index,(userIndex.size()-1));
      return(0.0);
   }
   *valid = true;
   return(indexIntegral[userIndexIntegral[index]]);
}
int rcfDvhMgr::setGeudExponent(float expo)
{
   if(0.0 == expo)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setGeudExponent user passed 0.0 as value of GEUD exponent. This value is illegal and cannot be accepted, GEUD computation has not been enabled\n");
      return(CF_FAIL);
   }
   geudExponent = expo;
   enableGeud = true;
   return(CF_OK);
}

int rcfDvhMgr::computeGeud()
{
   if(!enableGeud)
   {
      fprintf(stderr,"ERROR: rcfDvhMgr::setGeudExponent function called even though geud computation has not been enabled\n");
      return CF_FAIL;
   }
   double ave = 0.0;
   
   if(1 != dose_and_volume_ready)
   {
      fprintf(stderr,"rcfDvhMgr::computeGeud You can not compute the dvh until dose and volume arrays are allocated and prepared\n");
      return CF_FAIL;
   }

   double lvolume = 0.0;
   for(int i=0; i<working_dose_length; i++) 
   {
      ave += working_volume[i]*pow(working_dose[i],geudExponent);
      lvolume += working_volume[i];
   }

   if(lvolume > 0)
   {
      ave /= lvolume;
      geudValue = pow(ave,(double)(1/geudExponent));
   }

   return CF_OK;
}
