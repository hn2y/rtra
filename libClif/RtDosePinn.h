#ifndef RTDOSEPINN_H
#define RTDOSEPINN_H
#include <iostream>
#include<fstream>
#include <vector>
#include "jvsDefines.h"


class RtDosePinn
{
public:
    RtDosePinn();
    struct boundingBox
    {
        float maxX;
        float maxY;
        float maxZ;
        float minX;
        float minY;
        float minZ;

    };

    struct Header
    {
        int bytes_pix;
        int x_dim; // doseImageColumns
        int y_dim; // doseImageRows
        int z_dim; //
        float x_pixdim;
        float y_pixdim;
        float z_pixdim;
        float x_start;
        float y_start;
        float z_start;
        float X_offset; //
        float Y_offset; //  = 0.000000;
        float x_start_dicom;
        float y_start_dicom;
    };

    struct ImageHeader
    {
        float DoseGridVoxelSizeX;
        float DoseGridVoxelSizeY;
        float DoseGridVoxelSizeZ;
        int DoseGridDimensionX;
        int DoseGridDimensionY;
        int DoseGridDimensionZ;
        float DoseGridOriginX;
        float DoseGridOriginY;
        float DoseGridOriginZ;
        int DoseGridBinaryOrderEndian;
    };

    ImageHeader imageHeader;
    Header header;
    boundingBox doseArrayBoundingBox;
    /// A vector containing dose Array  // column major
    std::vector<float> doseArray;
    std::vector<float> doseArrayCoordinatesX;
    std::vector<float> doseArrayCoordinatesY;
    std::vector<float> doseArrayCoordinatesZ;

    std::vector<float> doseGridZvalues;
    /// TODO: dose value X values
    std::vector<float> doseGridXvalues;

    /// TODO: dose value y values
    std::vector<float> doseGridYvalues;

    //
    int getDoseArrayFromPinnacleDoseAndHeaderFiles();

    void setDoseHeaderFilePath(std::string _DoseHeaderFilePath);
    void setDoseImageFilePath(std::string _DoseImageFilePath);
    void setDoseImageHeaderFilePath(std::string _DoseImageHeaderFilePath);
    float getMaxDose();
    void updateDoseGridValues();
    std::string doseCalcImageSet;// Patient.ImageSetList[0].ImageName

private:

    // Total dose header file path
    std::string doseHeaderFilePath;
    std::string doseImageHeaderFilePath;
    std::string doseImageFilePath;
    float maxDose;

};

#endif // RTDOSEPINN_H
