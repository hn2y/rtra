#ifndef __rcfLineArgMgr
#define __rcfLineArgMgr

// Author: M.Fatyga
// Date Created: 03/14/2006

/**
    \class rcfLineArgMgr
    @ingroup UTIL
    Object rcfLineArgMgr parses the command line
    and stores pairs of flags and data items. It
    currently assumes strict flag-value ordering
    of the command line. User can use public interface
    to retrieve values by quering the flag name.
*/

class rcfLineArgMgr
{

   protected:

      int nargs;
      char** flags;
      char** data;

   public:

      rcfLineArgMgr(int iArgc, char *ppcArgv[]);
      ~rcfLineArgMgr();

   public:

      // public interface functions return CF_OK if the
      // flag is found, CF_FAIL if the flag is not found

      int getMessage(char *flag, int *data);
      int getMessage(char *flag, float *data);
      int getMessage(char *flag, char **data);

      // verify number of tokens in the line
      int getNumberOfTokens();
      bool IsValid();

   private:

      void basicInit();

      
};

#endif
