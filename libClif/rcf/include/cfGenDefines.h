#ifndef CF_GEN_DEFINES_H
#define CF_GEN_DEFINES_H
#include <math.h>

/*
 * Header cfGenDefines.h
 * Date Created: 01/04/05
 * Original Author: M.Fatyga
 * Purpose: contain definition
 * shared by all RTRCF code
 */

/**
    @file
    @ingroup INCLUDE
    @brief Contains definitions shared by all RCF code
*/

#define CF_OK 0
#define CF_FAIL -1

#define VALID 1
#define INVALID 0

/*
 * definitions below correspond to
 * SPARC storage representations.
 */

#define uint8 unsigned char
#define uint16 unsigned short
#define uint32 unsigned int
// HN
// To avoid interfereing with opencv header \opencv310\include\opencv2\core\hal\interface.h
#if defined _MSC_VER || defined __BORLANDC__
typedef unsigned __int64 uint64;
#else
//#define uint64 unsigned long int
//HN disabled in linux due to declaration error


// TODO: Not sure if we need to change it or not
// in opencv the following are defined for other compilers
typedef int64_t int64;
typedef uint64_t uint64;
//#  define CV_BIG_INT(n)   n##LL
//#  define CV_BIG_UINT(n)  n##ULL
#endif

#define float32 float
#define float64 double
namespace cf
{

enum e_cfType
{
    UINT8,
    UINT16,
    UINT32,
    UINT64,
    FLOAT32,
    FLOAT64
};

enum ImageModality
{
    FBCT,
    CBCT,
    MR,
    UNKNOWN_MODALITY
};
}

/** \def INDEX3D(index, DIM)
 * 	calculate 1-D index number of a 3-D index.
 *
 */
#define INDEX3D(index, DIM) ((index.z*DIM.x*DIM.y) + (index.y*DIM.x) + index.x)

#endif
