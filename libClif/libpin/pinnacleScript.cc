/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* pinnacleScript.cc
      Routines for doing Pinnacle Scripting on remote machines....

      April 25, 2002: JVS: Break off from dose_to_pinnacle.cc S
      March 10, 2005: JVS: Change rsh's to ssh's for all machines
      Nov   10, 2005: JVS: Add iPinnComm_IssueRootCommand for LINUX
      Nov   15, 2005: JVS: Finish with iPinnComm and vPinnComm commands
      Mar 28, 2006: JVS: Update output message about send_pinnacle_script..
                         No functionality change, just correct message
      April 4, 2006: JVS: Add in wait after sends pinnacle command
      August 30, 2007: JVS: Improve error message after have Spawning Error
      Nov 27, 2007: JVS: remove warning message
      Jan 18, 2015: JVS: improve LoadPinnacleScript

*/
/* *************************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h> // for strcpy


#include <sys/types.h> /* for fork */
#include <sys/syscall.h>
#include <unistd.h>    /* for fork */
#include <wait.h>      /* for wait */
#include <errno.h>

#include "utilities.h"
#include "myPinnacle.h"
#include "myPinnComm.h"
#include "pinnacleScript.h"
#include "wait_routines.h"
/* ******************************************************* */
#ifdef LINUX
#warning "----------Re Defining iPinnComm_* commands for LINUX -------------" 
void vPinnComm_Initialize(
                void *pvTopLevelWidget,  /* If NULL, will initialize X. */
                int *piArgc, 
                char *ppcArgv[])
{
  printf("\n ERROR: Cannot issue initialize PinnComm from LINUX");
  return;
}
void vPinnComm_SetRemoteWindowId(int iWindowID)
{
  printf("\n ERROR: Cannot set PinnComm Window from LINUX");
  return;
}

int iPinnComm_IssueRootCommand(char *pcMessage)
{
  printf("\n ERROR: Cannot issue Pinnacle root commands from LINUX");
  return 0;
}
int iPinnComm_QueryFloat(void *pvObject, char *pcMessage, float *value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_QueryInt(void *pvObject, char *pcMessage, int *value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_QueryObject(void *pvObject, char *pcMessage, void **ppvValue)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_QueryObject(char *pvObject, char *pcMessage, void **ppvValue)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_QueryString(char *pvObject, char *pcMessage, char value[PINN_COMM_STRING_LEN])
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_QueryString(void *pvObject, char *pcMessage, char *value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_SetString(void *pvObject, char *pcMessage, char *value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_SetObject(void *pvObject, char *pcMessage, void *ppvValue)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_SetFloat(void *pvObject, char *pcMessage, float value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}
int iPinnComm_SetInt(void *pvObject, char *pcMessage, int value)
{
  printf("\n ERROR: Cannot issue Pinnacle commands from LINUX");
  return 0;
}

#endif
/* ******************************************************* */
/* ********************************************************************************* */
int sendPinnacleCommand(long myPinnWindowID, //char* directory, 
                        char *pcMessage)
{
  if(myPinnWindowID > 0)
  {
     int iStatus = iPinnComm_IssueRootCommand(pcMessage);
     if (!iStatus) {
           fprintf(stderr, "\n ERROR: sendPinnacleCommand: Issuing Command %s", pcMessage);
           return(FAIL);
     }
  }
  else
  {
     /* Write the script to a file */
    char fileName[1024];
    // directory might not be needed
    // sprintf(fileName,"%s/tempScriptCreateBySendPinnacleCommand.Script", directory);
    sprintf(fileName,"tempScriptCreateBySendPinnacleCommand.Script");
    FILE *oStrm = fopen(fileName,"w");
    fprintf(oStrm,"%s\n\n",pcMessage);
    fclose(oStrm);
    sprintf(fileName,"tempScriptCreateBySendPinnacleCommand.Script");
    if(LoadPinnacleScript(fileName) != OK)
    {
      printf("\n ERROR: sendPinnacleCommand: Loading Script %s", fileName);
      return(FAIL);
    }
  }
  return(OK);
}
/* ********************************************************************************* */
int send_pinnacle_script(char *ExtPinnComputer, char *ExtPinnWindowID, char *script_fname)
{
  /* Get the remote path for mySsh, a csh script that runs ssh! */
   char *path = getenv("UVA_BIN");
   if(path == NULL){
      eprintf("\n ERROR: Environment Variable for UVA_BIN not set");
      return(FAIL);
   } 
   int wait_status = WAIT_FOR_CHILD; // When WAIT, then returns FAIL even when OK ???
#ifdef DEBUG
   printf ("\n Send Pinnacle Script to load %s...\n",script_fname);
#endif
   fflush(NULL);
   int sreturn;
   /* Spawn off process to load the Dose to Pinnacle */
   fflush(stdout);fflush(stderr);  // flush the input so not exist in daughter
   pid_t pid = fork();


   char remoteCommand[MAX_STR_LEN];
   sprintf(remoteCommand,"%s/mySsh",path);
   // const char *commandToRun= "${UVA_BIN}/${UNAME}/pinnacle_load_script";
   const char *commandToRun= "${UVA_BIN}/pinnacleLoadScript";


   switch (pid)
   {
      case -1:
             printf("\n ERROR: fork failed");
             exit(FAIL);
      case 0:
          char *display;
          display = getenv("DISPLAY");
          if(display == NULL )
          {
             display = getenv("UVA_DISPLAY");
             if(display == NULL)
	     {
                printf("\n ERROR: Getting DISPLAY or UVA_DISPLAY environment variable\n");
                perror("DISPLAY");
                return(FAIL);
             }
          }
#ifdef DEBUG
          printf ("\n DISPLAY = %s", display);
#endif
          char *MCVUser;
          MCVUser = getenv("UVA_USERNAME");
#ifdef DEBUG
          printf("\n send_pinnacle_script: Child process executing command %s -x %s %s %s %s %s %s %s %s\n", 
                     remoteCommand, 
                     ExtPinnComputer,
                     commandToRun,
                     "-window",
                     ExtPinnWindowID,
		     "-script",
                     script_fname,
		     "-display",
                      display);
#endif
          fflush(stdout);fflush(stderr);
          if(MCVUser == NULL)
	  {
#ifdef DEBUG
            printf("\n execlp being executed for %s", remoteCommand); fflush(NULL);
#endif
	    sreturn = execlp( remoteCommand, remoteCommand, "-x",
                        ExtPinnComputer,
                        commandToRun,
                        "-window",
                        ExtPinnWindowID,
			"-script",
                        script_fname,
			"-display",
                        display,
                        NULL);
          }
          else
          {
             sreturn = execlp( remoteCommand, remoteCommand, "-x", "-l", MCVUser,
                        ExtPinnComputer,
                        commandToRun,
                        "-window",
                        ExtPinnWindowID,
			"-script",
                        script_fname,
			"-display",
                        display,
                        NULL);
	  }
             if(sreturn == -1)
             {
               printf("\n ERROR: Spawning Process: %d",sreturn);
               printf("\n\t ERROR: Spawing: %s -x -l %s %s %s -window %s -script %s -display %s", 
                   remoteCommand, MCVUser, ExtPinnComputer, commandToRun, ExtPinnWindowID, script_fname, display);
               perror("execlp");
               return(FAIL);
             }
             printf("\n ^^^^^^^^ send_pinnacle_script Child is All Done ^^^^^^^^^^");
             _exit(OK);
	  default:
#ifdef DEBUG
	    printf("\n Child process %ld started by send_pinnacle_script",(long) pid);
#endif
	    if(wait_status != NO_WAIT_FOR_CHILD )
            {
	      printf("\n Waiting for process %d (%s) to finish\n", pid, script_fname);
              fflush(NULL);
              if(wait_for_child(pid) != OK) return(FAIL);
            }
	    
  } /* end of the switch */        
  return(OK);
}
/* **************************************************************************** */
// #define DEBUG_PINNACLE
int LoadPinnacleScript(char *ScriptFname){
  // Create the filename
  char ffilename[MAX_STR_LEN];
#ifdef DEBUG_PINNACLE
  printf("\n LoadPinnacleScript(%s)",ScriptFname);
#endif
  if( ScriptFname[0] == '/' ) {  // Check if fully qualified path
    strcpy(ffilename,ScriptFname);
  } else {                       // if not, then make it fully qualified
    char *pwdpath; 
    pwdpath = getenv("PWD");
    if(pwdpath == NULL ){
      perror("pwd");
      return(FAIL);
    }
    sprintf(ffilename,"%s/%s",pwdpath,ScriptFname);
  }
  // Check that the file exist
  if (OK != fileExists(ffilename) ) {
    printf("\n ERROR: LoadPinnacleScript: File not exist %s",ffilename);
  }
#ifdef DEBUG_PINNACLE
  printf("\n LoadPinnacleScript ==> Executing the Script File %s", ffilename);
#endif

  if(PinnWindowID > 0) {
#ifdef DEBUG_PINNACLE
    printf("\t Direct Loading");
#endif
    int iStatus;
    char loadString[MAX_STR_LEN];
    strcpy(loadString,"LoadNoCheckSum");
    iStatus = iPinnComm_SetString(NULL, loadString, ffilename);
    if(!iStatus){fprintf(stderr,"\nERROR: Setting LoadNoCheckSum"); return(FAIL);}
  } else {
#ifdef DEBUG_PINNACLE
    printf("\t send_pinnacle_script Loading");
#endif
    // See if the window ID is externally defined 
    char *ExtPinnWindowID = getenv("PINN_WINDOW_ID");
    char *ExtPinnComputer = getenv("PINN_COMPUTER");
    if(ExtPinnWindowID != NULL && ExtPinnComputer != NULL ){
      if( send_pinnacle_script(ExtPinnComputer, ExtPinnWindowID,ffilename) != OK){
	fprintf(stderr,"\n ERROR: Sending Script to Pinnacle"); return(FAIL);
      } 
    } else {
      printf("\n Run the Script %s to load into Pinnacle",ScriptFname);
    }
  }
  return(OK);
}
/* **************************************************************************** */
