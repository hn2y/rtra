#ifndef RIDGIDBODYMOTIONMODEL_H
#define RIDGIDBODYMOTIONMODEL_H
#include <iostream>
#include <QtXml>
#include <MotionModels/GumModel.h>

class RigidBodyMotionModel : public GumModel
{

public:
    RigidBodyMotionModel();
    RigidBodyMotionModel(std::ostream  &stream);
    ~RigidBodyMotionModel();
    RotationCenter rotCenter;
    Point3 sysRotErrorSigma;
    Point3 randRotErrorSigma;
    Point3 sysTranErrorSigma;
    Point3 randTranErrorSigma;

    Point3 intraFractionTranErrorSigma;
    Point3 intraFractionRotErrorSigma;




    bool DoseConvFlag;
    bool loadModelFromXMLFile(char* filename);
    bool loadModelFromXMLDocument(QDomDocument* domXmlDocument);
    bool writeXML();
    std::ostream  &logstream;
private:
    QDomDocument *domXmlDoc;
};

#endif // RIDGIDBODYMOTIONMODEL_H
