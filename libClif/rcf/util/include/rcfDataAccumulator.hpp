
// Author: M.Fatyga
// Date Created: 03/17/2008
// Object rcfDataAccumulator  
// This object manages data accumulation
// for PPG patients.

#ifndef __rcfDataAccumulator
#define __rcfDataAccumulator


#include <string>
#include <vector>
#include "rcfClifParser.hpp"

/**
    \class rcfDirectoryMgr
    @ingroup UTIL
*/

class rcfDataAccumulator
{

   protected:

      std::vector<std::string*> tgtdir;
      std::vector<std::string*> srcdir;
      std::vector<std::string*> corr_tgt;
      std::vector<std::string*> corr_src;
      std::vector<std::string*> comm_tgt;
      std::vector<std::string*> comm_src;
      std::vector<std::string*> drop_tgt;
      std::vector<std::string*> drop_src;
      std::vector<std::string*> file_tgt;
      std::vector<std::string*> file_src;
      std::vector<std::string*> patient_list;
      std::string root;
      rcfClifParser *request;
      char *func;
      FILE *logid;
      FILE *errlogid;

   public:

      rcfDataAccumulator();
      ~rcfDataAccumulator();

   public:

   int processRequest(char *file);

   protected:

   private:

   void basicInit();
   void reInitList();
   int buildDataListSelect(rcfClifParser *parser, int select);
   int setRoot(rcfClifParser *parser);
   int processPatient(unsigned int index);
   int buildCorrelatedDataList(rcfClifParser *parser);
   int buildCommonDataList(rcfClifParser *parser);
   int buildDropDataList(rcfClifParser *parser);
   int buildFileDataList(rcfClifParser *parser);
   int buildPatientList(rcfClifParser *parser);
   int processPatientList(rcfClifParser *parser);
   void reportError(const char *usrstr);
   void reportWarning(const char *usrstr);
   int setSystemLog(rcfClifParser *parser);

      
};

#endif
