#include "DeformationModel.h"

DeformationModel::DeformationModel():logstream(std::cout)
{
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="Deformation";
    DoseConvFlag=false;
    domXmlDoc=0;

}

DeformationModel::DeformationModel(std::ostream &stream):logstream(stream)
{
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="Deformation";
    DoseConvFlag=false;
    domXmlDoc=0;
}



DeformationModel::~DeformationModel()
{

}
DeformationModel DeformationModel::operator=(const DeformationModel &deformationModel)
{
    std::string class_member = "DeformationModel::operator=:";
    guModelType = deformationModel.guModelType;
    guModelFile = deformationModel.guModelFile;
    domXmlDoc   = deformationModel.domXmlDoc;
    DoseConvFlag = deformationModel.DoseConvFlag;

    //


    return *this;

}
