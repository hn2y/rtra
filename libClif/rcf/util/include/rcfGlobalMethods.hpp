#ifndef CF_GLOBALMETHODS_H
#define CF_GLOBALMETHODS_H

#include "cfGenDefines.h"
#include <string>
#include <algorithm>
#include <alloca.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <stdio.h>
#include <time.h>

void TrimString( std::string& str );
std::string GetCompileTime();
std::string GetExecutionStartTime();
void RemoveTrailingSlash( std::string& input );
std::string GetLibraryVersion();
bool CompareStrings(std::string string1, std::string string2 );

/// Removes double /'s from file paths
void RemoveDoubleSlashes( std::string& input );

void rcfPrintRunTimeInformation(int argc, char *argv[]);
void rcfPrintRunTimeInformation(int argc, char *argv[], const char *Prog_Name, const float Revision);
void rcf_print_runtime_info(int argc, char *argv[]);

inline char* mkdtemp(char *templ)
{
    char *t = (char*)alloca(strlen(templ) + 1);
    char *r;

    /* Save template */
    (void) strcpy(t, templ);
    for (; ; )
    {
        r = mktemp(templ);

        if (*r == '\0') { return NULL; }

        if (mkdir(templ, 0755) == 0) { return r; }

        /* Other errors indicate persistent conditions. */
        if (errno != EEXIST) { return NULL; }

        /* Reset template */
        (void) strcpy(templ, t);
    }
}

/**
 *  Returns the index of an (x,y,z) location. If the (x,y,z) location is
 *  invalid CF_FAIL is returned instead.
 */
inline int xyzToSingleValue(int x, int y, int z, int xSize, int ySize, int zSize)
{
    if( x < 0 || x >= xSize )
    {
        return CF_FAIL;
    }
    else if( y < 0 || y >= ySize )
    {
        return CF_FAIL;
    }
    else if( z < 0 || z >= zSize )
    {
        return CF_FAIL;
    }
    else
    {
        return (z * ySize * xSize + y * xSize + x);
    }
}

void StringToLowerCase(std::string& str);
void StringToUpperCase(std::string& str);

std::string CalculateChecksum(unsigned char* buffer, int byteCount);

#endif
