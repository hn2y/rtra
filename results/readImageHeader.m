function cTImageInfo=readImageHeader(filePath)
cTImageInfo=[];
fidHeader= fopen(filePath);
tline= fgetl(fidHeader);
while ischar(tline)
    disp(tline)
    try
        eval(['cTImageInfo.',tline]);
    catch
    end
    
    tline = fgetl(fidHeader);
    
end
fclose (fidHeader);
return 