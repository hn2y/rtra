/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/*  readPinnaclePatient.cc

    routines to read patient information from Pinnacle
  
    Created: Oct, 23, 1998: JVS

    Modification History:
       Feb 1, 1999: JVS: patient->directory is the plan directory
                         makes sure image is specified with the full path...
       Feb 4, 1999: JVS: patch image filename
       April 6, 1999: JVS: plan_fname now part of case_info_type
       Oct 27, JVS: Add #ifdef's around printf's
       June 21, 2000: change read_pinnacle_image_name, will get full path if ../ is
                      specified in the image name...
       August 29, 2000: JVS: readPinnaclePatient.cc...read from Patient...
   Feb 2, 2001: JVS: make sure all free before delete
   Dec 16, 2004: JVS: Allow Water Phantom as valid image type
   Nov 27, 2007: JVS: get rid of warning in readPinnacleImageName
   June 3, 2011: JVS: Add in reading of plan.Pinnacle to get StartWithDICOM value
   March 6, 2012: JVS: Add reading in of patient->ImageDBName =clean_name( image_header->db_name);
*/
// #define DEBUG_READ_PATIENT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isspace

#include "utilities.h"
#include "read_clif.h"
#include "case_info.h"
#include "ct_routines.h" // For image_header_type definition

// #define DEBUG_READ_PATIENT
/* *********************************************************************** */
int readPinnaclePlanPinnacleFile(case_info_type *patient) {
  FILE *strm;
  char fullFileName[MAX_STR_LEN];
  sprintf(fullFileName,"%s.Pinnacle",patient->PlanFileName);
  strm = fopen(fullFileName,"r");
  if(NULL == strm )  {
    printf ("\n ERROR: %s does not exist ",fullFileName); return(FAIL);
  } 
  else // found plan.Pinnacle file
  {
     /* read in the ClifObjects */
     clifObjectType *PinnacleInfoList = new(clifObjectType); // allocate memory
     // read in the Clif
     if( readClifFile(strm, PinnacleInfoList,"PinnacleInfoList") != OK)
     {
        printf("\n ERROR: Reading clif from file %s\n", fullFileName); return(FAIL);
     }
     if( readIntClifStructure  (PinnacleInfoList, "StartWithDICOM", &patient->StartWithDICOM) != OK  )	{
       // printf("\n ERROR: Reading StartWithDICOM from %s", fullFileName); return(FAIL);
       printf("\n WARNING: StartWithDICOM does not exist in file %s, setting to 0", fullFileName);
       patient->StartWithDICOM=0; 
     }
     //  printf("\n readPlanPinnacleFile reports StartWithDICOM = %d", patient->StartWithDICOM);
  }
  return(OK);
}
/* *********************************************************************************** */
int readPinnacleImageName(case_info_type *patient )
{
   char PlanDefaultsFileName[MAX_STR_LEN];
   strcpy(PlanDefaultsFileName,patient->PlanFileName);  // put first part on name
   strcat(PlanDefaultsFileName,".defaults");          // append .Trial to the name  
#ifdef DEBUG_READ_PATIENT
   printf("\n Reading In Image Name from file %s", PlanDefaultsFileName);
#endif
   FILE *fp = fopen(PlanDefaultsFileName,"r");
   if(fp==NULL){
      printf("\n ERROR: opening patient beam file %s", PlanDefaultsFileName);return(FAIL);
   }
   // read image_file from stream 
   char cstring[MAX_STR_LEN];
   // strcpy(cstring,"image_file      :"); // Nov 2007
   strcpy(cstring,"image_file");
   int sval = (int) strlen(cstring);
   char string[MAX_STR_LEN];
   do{
      if(fgets(string,MAX_STR_LEN,fp) == NULL)
      {
         printf("\n ERROR: readPinnacleImageName: getting string from file %s, string=%s", PlanDefaultsFileName,cstring);
         return(FAIL);
      }
   }while( strncmp(string,cstring,sval) );
   while(isspace(string[sval])) sval++;
   sval++; // increment past colon -- for Nov 2007
   /* full file name is string+sval, 
      but want to remove all of the path information */
   //   printf("\n string = %s", string+sval);
   while(isspace(string[sval])) sval++;
   strcpy(patient->ImageName, string+sval);
   sval = (int) strlen(patient->ImageName);
   patient->ImageName[sval-1]='\0'; // Null terminate the string
   // check to see if has the full path name or not 

   /* Version 4.2g writes ../ImageSet_1. 
      BUT we need the full path information */
#ifdef DEBUG_READ_PATIENT
   printf("\n A. patient->ImageName = %s", patient->ImageName);
#endif
   if(strncmp( patient->ImageName, "../", 3) == 0 )
   {
     if( strlen(patient->Directory) != 0 )
     {
	// Get the Image Directory
        char ImageDirectory[MAX_STR_LEN];
        strcpy(ImageDirectory,patient->Directory);
        // trim of the last part of the directory
        int iLen=(int)strlen(ImageDirectory)-1;
        if( ImageDirectory[iLen] == '/') iLen--; // (ended in "/")
        while( ImageDirectory[iLen] != '/' && iLen)
	{
           ImageDirectory[iLen] = 0; iLen--;
        }
        // make sure not have multiple /
        while( ImageDirectory[iLen] == '/' && iLen)
	{
           ImageDirectory[iLen] = 0; iLen--;
        }
#ifdef DEBUG_READ_PATIENT
        printf("\n ImageDirectory = %s", ImageDirectory);
#endif
        // remove the dots
        char tfilename[MAX_STR_LEN];
        strcpy(tfilename,patient->ImageName+3);
        sprintf(patient->ImageName,"%s/%s", ImageDirectory, tfilename);
#ifdef DEBUG_READ_PATIENT
        printf("\nB. ImageName = %s", patient->ImageName);
#endif
     }
   }
   /* Version 4.0 (without full path) starts with Institution */
   if( strncmp( patient->ImageName,"Institution", 11)==0)
   {
      char *ppath = getenv("PINN_PAT_DIR");
      if(ppath == NULL)
      {
         ppath = getenv("PINNLP_PATIENTS");
         if(ppath == NULL)
         {  
            printf("\n ERROR: Environment Variable for PINNLP_PATIENTS of PINN_PAT_DIR not set");
            printf("\n\tand image name does not have full path information");
            return(FAIL);
	 }
      }
      char tfilename[MAX_STR_LEN];
      sprintf(tfilename,"%s/%s",ppath,patient->ImageName);
      strcpy(patient->ImageName, tfilename);
   }
   fclose(fp);
#ifdef DEBUG_READ_PATIENT
   printf("\n Image File Name is %s", patient->ImageName);
#endif
   return(OK);
}
/* *********************************************************************************** */
int readPinnaclePlanInfo(case_info_type *patient)
{
  if (NULL == patient) {
    printf("\n ERROR: %s passed Null pointer\n",__FUNCTION__); return(FAIL);
  }
  // Read Plan Info 
  char planInfoFileName[MAX_STR_LEN];
  if (NULL == patient->PlanFileName)
    strcpy(patient->PlanFileName,"plan");
  strcpy(planInfoFileName,patient->PlanFileName);
  
  strcat(planInfoFileName,".PlanInfo");
  clifObjectType *PlanInfoClif = new(clifObjectType);
  FILE *istrm = fopen(planInfoFileName,"r");
  if(istrm == NULL)
  {
     printf("\n ERROR: Opening file %s", planInfoFileName); return(FAIL);
  }
  if (    readClifFile(istrm, PlanInfoClif, "PlanInfo") != OK 
       || readStringClifStructure(PlanInfoClif,"PatientName",patient->PatientName) != OK
       || readStringClifStructure(PlanInfoClif,"PlanName", patient->PlanName) != OK )
  {
     printf("\n ERROR: Reading Clif Information from %s", planInfoFileName); return(FAIL);
  }
#ifdef DEBUG_READ_PATIENT 
  printf("\n All Done with PlanInfo");
#endif
  fclose(istrm);
   // free the structure
   if(freeClifStructure(PlanInfoClif) != OK)
   {
      printf("\n ERROR: Freeing memory for clif object");
   }
  delete(PlanInfoClif);
  return(OK);
}
/* ********************************************************************************** */
int readPinnaclePatientFile(case_info_type *patient)
{
#ifdef DEBUG_READ_PATIENT
  printf("\n readPinnaclePatientFile: ");
#endif
  char patientFileName[MAX_STR_LEN];
  strcpy(patientFileName,patient->Directory);
  if(strlen(patientFileName)) strcat(patientFileName,"/");
  strcat(patientFileName,"../Patient");
  FILE *istrm = fopen(patientFileName,"r");
  if(istrm == NULL) {
     printf("\n ERROR: Opening file %s", patientFileName); return(FAIL);
  }
  /* read in the clifs */
  clifObjectType *PatientClif  = new(clifObjectType);
  if ( readClifFile(istrm, PatientClif, "Patient") != OK  ) {
      printf("\n ERROR: readPinnaclePatientFile: %s",patientFileName); return(FAIL);
  }  
  fclose(istrm);
#ifdef DEBUG_READ_PATIENT
  printf("\n readPinnaclePatientFile: completed readClifFile");
#endif
  /* Find the Plan in the List of Clifs */
  clifObjectType *PlanList;
  if(   getClifAddress(PatientClif,"PlanList", &PlanList)!= OK
    ||  checkIfObjectList(PlanList) != OK ) {
      printf("\n ERROR: Valid PlanList Cannot be Found in %s",patientFileName); return(FAIL);
  } 
#ifdef DEBUG_READ_PATIENT
  printf("\n readPinnaclePatientFile: completed getting PlanList");
#endif
  clifObjectType *PlanObject;
  if(getMatchingClifAddress(PlanList, "PlanName", patient->PlanName, &PlanObject) != OK ) {
     printf("\n ERROR: getMatchingClifAddress"); return(FAIL);
  }
#ifdef DEBUG_READ_PATIENT
  printf("\n readPinnaclePatientFile: completed getting PlanName (PlanObject) for %s", patient->PlanName);
#endif
  /* Need to determine Image Name */  
  char ImageType[MAX_STR_LEN];
  if( readStringClifStructure(PlanObject, "PrimaryImageType", ImageType) != OK) {
     printf("\n ERROR: Determining PrimaryImageType"); return(FAIL);
  }
  //
  // printf("\n --- ImageType == %s", ImageType);
  patient->ImageDBName[0]=0; // initialize name to null
  if(strcmp(ImageType,"Digitize Contours") == 0 ) {
    strcpy(patient->ImageName, ImageType);
    strcpy(patient->ImageDBName, "DigitizedContours");
  }
  else if(strcmp(ImageType,"Water Phantom") == 0 ) {
    printf("\n Water Phantom Image Type Detected\n");
    strcpy(patient->ImageName, ImageType);
    strcpy(patient->ImageDBName, "WaterPhantom");
  }
  else if( strcmp(ImageType,"Images") == 0 ) {
     /* here, we could concoct the name based upon the PrimaryCTImageSetID, PatientID, etc,
	but this is already done in the plan.defaults file, so will read if from there */
    if(readPinnacleImageName(patient) != OK)
    {
      printf("\n ERROR: Reading Patient Image File Name"); return(FAIL);
    }
    // get the patient->ImageDBName;
   {
     image_header_type imageHeader;
     if(OK != read_image_header(patient->ImageName, &imageHeader) ){
       printf("\n ERROR: read_image_header returned an error \n"); return(FAIL);
     }
     strcpy(patient->ImageDBName,imageHeader.db_name);
     clean_name(patient->ImageDBName);
   }
  }
  else {
   printf("\n ERROR: %s is undefined Image Type",ImageType); return(FAIL);
  }
  // printf("\n ----patient->ImageDBName = %s", patient->ImageDBName);
   // free the structure
   if(freeClifStructure(PatientClif) != OK) {
      printf("\n ERROR: Freeing memory for clif object");
   }
   delete (PatientClif);

   return(OK);
}
/* *********************************************************************************** */
/* *********************************************************************************** */
int readPinnaclePatient(case_info_type *patient)
{
   strcpy(patient->Directory, patient->PlanFileName);
   int len=(int)strlen(patient->Directory)-1;
   while(len >= 0 && patient->Directory[len] != '/')
   {
      patient->Directory[len]=0; 
      len--;
   }
   while( len >=1 && patient->Directory[len-1] == '/')
   {
      patient->Directory[len]=0; 
      len--;
   }
#ifdef DEBUG_READ_PATIENT
   printf("\n patient->Directory = %s (%d)",patient->Directory, strlen(patient->Directory));
   printf("\n (%c)", patient->Directory[len]);
#endif 
   if( readPinnaclePlanInfo(patient) != OK ||
       readPinnaclePatientFile(patient) != OK ||
       OK != readPinnaclePlanPinnacleFile(patient) )
   {
      printf("\n ERROR: Reading Pinnacle Patient Information\n"); return(FAIL);
   }
   return(OK);
}
/* ********************************************************************************** */
