// CUDA - C includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_vector.h>


#include <stdio.h>

extern "C"
void vecFloatOnDevice(int numElement, thrust::device_vector<float>& v );


void launchTransformPts(int numElement, thrust::device_vector<float>& v)
{
	
	thrust::device_vector<float> newVar(numElement);
	v = newVar;

}
