#ifndef CFCLIFPOINTS_HPP
#define CFCLIFPOINTS_HPP

#include "cfTypedefs.hpp"
#include <vector>
#include <string>
#include <list>

class cfClifPoints
{
    public:
        cfClifPoints();
        ~cfClifPoints();
        cfPoint3D<float> GetPoint( int index );
        void AddPoint( cfPoint3D<float> p );
        bool RemovePoint( unsigned int index );
        void ClearPoints();
        int GetNumberOfPoints() { return points.size(); }
    private:
        std::list< cfPoint3D<float> > points;
        std::list< cfPoint3D<float> >::iterator iterator;
};
#endif
