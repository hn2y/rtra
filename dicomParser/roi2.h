#ifndef ROI2_H
#define ROI2_H
#include <iostream>
#include "Utilities/someHelpers.h"
#include <vector>
#include <limits>       // std::numeric_limits

#ifdef UNICODE
#undef UNICODE
#endif

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmrt/drtimage.h"
#include "dcmtk/dcmrt/drtstrct.h"
#include "dicomParser/CTImage.h"
#include "roi_type.h"

class ROI2
{
public:
    ROI2();
    ROI2(std::ostream &stream);
    ~ROI2();

    /// will contain the bounding box coordinates of each OAR
    struct BoundingBox
    {
#ifdef __linux__

        double maxX=-std::numeric_limits<double>::max();
        double maxY=-std::numeric_limits<double>::max();
        double maxZ=-std::numeric_limits<double>::max();
        double minX=std::numeric_limits<double>::max();
        double minY=std::numeric_limits<double>::max();
        double minZ=std::numeric_limits<double>::max();
#elif defined _WIN32 || defined _WIN64

        double maxX=-(std::numeric_limits<double>::max)();
        double maxY=-(std::numeric_limits<double>::max)();
        double maxZ=-(std::numeric_limits<double>::max)();
        double minX=(std::numeric_limits<double>::max)();
        double minY=(std::numeric_limits<double>::max)();
        double minZ=(std::numeric_limits<double>::max)();
#else
#error "unknown platform"
#endif


    };


    /// ROI Name
    std::string RoiName;

    /// ROI Number in DICOM
    Sint32 RoiNumber;

    const char * rtStructFileName; // TODO: probably we don't need this

    //const char *associateCTDicomFile; // will be used to match the coordinates of point wrt CT files
    std::vector<CTImage> * ctImageSet;

    BoundingBox boundingBox;
    /// Is Target
    bool IsTargetVolume;

    /// Display Color
    std::vector<unsigned int> DisplayColor;


    // Flags
    bool isDataLoaded;
    bool isBitmapComputed;



    //// DICOM POINTERs --> these will be set at rtStructure Level
    // Pointer to RTStructure that owns this ROI
    DRTStructureSetIOD * rtstructIODPointer;
    /// the number of corresponding RoiContourSequenceItem in DICOM file
    Sint32 ItemNumberInRoiContourSequence;
    /// the number of corresponding StructureSetROISequence in DICOM file
    Sint32 ItemNumberInStructureSetROISequence;
    /// contourDICOMIndexForUniqueSlices[m][k]  m = UniqueSlice index , k
    std::vector<std::vector<int>> contourDICOMIndexForUniqueSlices;
    /// TODO: description
    std::vector<int>  correspondingUniqueSliceIdxForEachContourInDICOMList;


    //// contours --> curveVector
    std::vector<roi_curve_type> curveVector;
    std::vector<float> curveZlocations;
    int n_curves;

    /// Binary Bitmap params
    std::vector<bool> binaryBitmap;
    std::vector<unsigned int> indicesNonzerosElemInBinaryBitmap; // column major
    std::vector<float> partialVolumeOfBinaryBitmapVoxels;
    std::vector<unsigned int short> numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap;

    std::vector<float> imageSetSlicesZValuesInMM; // TODO: to be moved to Volume/imageSet
    int ImageSetXdim;                         // TODO: to be moved to Volume/imageSet
    int ImageSetYdim;                         // TODO: to be moved to Volume/imageSet
    int ImageSetZdim;                         // TODO: to be moved to Volume/imageSet
    std::vector<float> ImageSetXGridValuesInMM;  // TODO: to be moved to Volume/imageSet
    std::vector<float> ImageSetYGridValuesInMM; // TODO: to be moved to Volume/imageSet




    bool bVoxelBitStartState;





    //// functions
    bool createBitmap();
    // this function filles curveVector, curveZlocations, n_curves
    bool readCurveData();

    bool setCTImageSet(std::vector<CTImage> *_ctImageSet);


private:
    // debug / logging
    std::ostream &logstream;

    int closestNumberIndexInVectorLessThanOrEqualToValue(const std::vector<float> &vec, float value);
    int closestNumberIndexInVectorGreaterThanOrEqualToValue(const std::vector<float> &vec, float value);
    template<typename T>
    std::map<T, unsigned int> histogram(std::vector<T> &a);
};

#endif // ROI2_H

