#ifndef RotationAndTranslationModelMODEL_H
#define RotationAndTranslationModelMODEL_H
#include <iostream>
#include <QtXml>
#include <MotionModels/GumModel.h>



class RotationAndTranslationModel: public GumModel
{
public:
    RotationAndTranslationModel();
    RotationAndTranslationModel(std::ostream  &stream); // TODO :
    ~RotationAndTranslationModel();
    RotationCenter rotCenter;
    Point3 sysRotErrorSigma;
    Point3 randRotErrorSigma;
    Point3 sysTranErrorSigma;
    Point3 randTranErrorSigma;

    Point3 intraFractionTranErrorSigma;
    Point3 intraFractionRotErrorSigma;
    bool DoseConvFlag;

    // general functions
    // void clear();

    bool loadModelFromXMLFile(char* filename,std::string & roiID);
    bool loadModelFromXMLDocument(QDomDocument* domXmlDocument, std::string & roiID);
    bool writeXML();
    bool IsModelSet();
    std::ostream  &logstream;

    // operators
    RotationAndTranslationModel operator=(const RotationAndTranslationModel &rotAndTransModelData);
    //bool operator==(const RotationAndTranslationModel & moddata) const;
    //bool operator!=(const RotationAndTranslationModel & moddata) const;

    QDomDocument *domXmlDoc;
private:

};

#endif // RotationAndTranslationModelMODEL_H
