#ifndef RTDOSE_H
#define RTDOSE_H

#include <iostream>
#include <vector>

// The DCMTK headers
#ifdef UNICODE
#undef UNICODE
#endif

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmrt/drtdose.h"
#include "dcmtk/dcmrt/drtimage.h"
//#include "dcmtk/dcmrt/drtplan.h"
//#include "dcmtk/dcmrt/drtstrct.h"
//#include "dcmtk/dcmdata/dcfilefo.h"
//#include "dcmtk/dcmrt/drmstrct.h"
//#include "dcmtk/dcmrt/drttreat.h"


#include "someHelpers.h"


class RtDose
{
public:
    RtDose();
    struct boundingBox
    {
        double maxX;
        double maxY;
        double maxZ;
        double minX;
        double minY;
        double minZ;

    };

    const char * rtDicomDoseFile;
    const char * associateCTDicomFile;
    DcmFileFormat rtDoseDCMTKfileformat;


    /// A vector containing dose Array
    /// row major rearrangment  numRow x numCol x numFrame element saved in row major, i.e. it is indexed [k*rows*columns + i*columns + j]   j=0:columns-1 , i=0:rows-1 , k=0:numframes

    std::vector<double> doseArray;
    std::vector<double> doseArrayCoordinatesX;
    std::vector<double> doseArrayCoordinatesY;
    std::vector<double> doseArrayCoordinatesZ;

    /// dose Maximum value in gray
    double maxDose;

    /// dose object oreintation patient
    double DoseImageOrientation[6];

    /// dose object  DoseImagePixelSpacing
    double DoseImagePixelSpacing[2];

    /// dose object  DoseImagePixelSpacing
    double DoseImageSpacingBetweenSlices;

    /// dose object  patient position
    double doseImagePositionPatient[3];

    /// dose object number of columns
    Uint16 doseImageColumns;

    /// dose object number of Rows
    Uint16 doseImageRows;



    /// dose coordinates needs to be parsed from dicom dose file
    std::vector<Point3> doseArrayCoordinates;


    /// Dose image vertical Grid Interval // = -abs(DoseImagePixelSpacing[0])
    double verticalGridIntervalDoseImage;

    /// Dose image horizontal Grid Interval // = abs(DoseImagePixelSpacing[1])
    double horizontalGridIntervalDoseImage;

    /// dose image coord1OFFirstPoint
    double coord1OFFirstPoint;

    /// dose image coord1OFFirstPoint
    double coord2OFFirstPoint;

    /// dose number of Frames
    Uint16 doseImageNumOfFrames;


    /// TODO : notice here, we use dose Frame Increment pointer
    std::vector<double> doseGridZvalues;

    /// TODO: dose value X values
    std::vector<double> doseGridXvalues;

    /// TODO: dose value y values
    std::vector<double> doseGridYvalues;

    // Patient Position
    std::string patientPosition;

    // Patient CT xOffsetCT
    double xOffsetCT;

    // Patient CT yOffsetCT
    double yOffsetCT;

    // CT Image Position Patient
    double cTImagePositionPatient[2];

    //CT Image DoseImagePixelSpacing
    double cTImagePixelSpacing[2];


    // CT Image Orientation
    double cTImageOrientation[6];


    // dose Image number of columns
    Uint16 cTImageColumns;

    // dose Image number of Rows
    Uint16 cTImageRows;

    // functions
    /// dose values will be in gray
    bool getDoseArray();

    /// still experimental --> // TODO : need to figure out a way to make it faster, this is recommened way of parsing a RTDOSE DICOM files.
    bool getdoseArrayUsingDCMRT();

    /// Coordinates of the dose array voxels wrt patient frame
    bool  getDoseArrayCoordiantes();

    /// Dose array bounding box
    boundingBox doseArrayBoundingBox;

    /// Dose Cal ImageSet ID
    std::string doseCalcImageSet;

    bool getCTYoffset();
    bool getCTXoffset();
    bool getDoseArrayFromPinnacleTotalDoseAndHeaderFiles(std::string headerFilePath, std::string doseImageHeaderFilePath, std::string totalDoseBinaryFilePath);
};

#endif // RTDOSE_H
