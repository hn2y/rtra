// Author: M.Fatyga
// Date: 03/14/2006
// This class provides utilities for
// the management of byte ordering

#ifndef __rcfByteMgr
#define __rcfByteMgr

#include "cfGenDefines.h" // data types below are defined here

/**
    \class rcfByteMgr
    @ingroup UTIL
    This class provides byte swapping and endianness checking functions.
*/

class rcfByteMgr
{

   public:

      uint16 reverseByteOrder(uint16);
      uint32 reverseByteOrder(uint32);
      int reverseByteOrder(int);
      uint64 reverseByteOrder(uint64);
      float32 reverseByteOrder(float32);
      float64 reverseByteOrder(float64);
      bool isOrderBigEndian();
      bool isOrderLittleEndian();

   private:

      int check_byte_order();

      
};

#endif
