/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* binary.cc: binary file read routines
   created: 6/18/98: jvs
*/
#include <stdio.h>
#include <stdlib.h>
#include "utilities.h"

float *read_binary(char *image_fname, int image_size)
{
  /* Open the image stream */
  FILE *image_strm;
  image_strm = fopen(image_fname,"rb");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s \n",image_fname);
    return(NULL);
  }
  /* allocate memory for the image */
  float *image = (float *)calloc( image_size, sizeof(float) );

  if(image == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(NULL);
  }
  unsigned sread = (unsigned) (fread(image,sizeof(float),image_size,image_strm));
  /* close the data stream */
  fclose(image_strm);
#ifdef DEBUG
  printf("\n Read %d items from %s", sread, image_fname);
#endif
  if(sread != (unsigned) image_size) return(NULL);
  return(image);
}
/* ******************************************************************** */
