#!/home/hnourzad/apps/epd-7.3-2-SunOS_5.10-x86_64/bin/python
# -*- AddToRTRAQue.py -*-#
"""
SYNOPSIS
    it send the information to RTRA system to perform robustness analysis for a given pinnacle patient file(in patient folder),
     plan.roi, TotalDose files,  Uncertainty parameters, number of Fractions, number of SubFractions for intraFx motions, numSimulation parameters.

DESCRIPTION
    use AddToRTRAQue.py -h
    The data will be sent to the RTRA System via mcvque user

EXAMPLES
    Show some examples of how to use this script.

VERSION 0.0
AUTHOR
    hnourzad on 3/21/17

"""

import os, sys
import time
import numpy as np
import datetime
import argparse
import subprocess
import ntpath
import paramiko # used for ssh information sending
import datetime
import getpass # to get user information


dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path)

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail,head

# def parsePatientHeaderFile(patientFilePath):


def listRoiNames(planDotRoiFilePath):
    if not os.path.isfile(planDotRoiFilePath):
        raise ValueError('roiFile does not exists.')
    roiList = []
    roiVolumeList= []
    with open(planDotRoiFilePath, 'r') as fsrc:
        for line in fsrc:
            if 'roi={' in line:
                lineCurr = next(fsrc)
                roiName = lineCurr.rsplit('name: ')[-1]  # take string after name:
                roiName = roiName.rstrip('\n')
                roiList.append(roiName)
                while 'volume = ' not in lineCurr:
                    lineCurr = next(fsrc)
                roiVol = lineCurr.rsplit('volume = ')[-1]  # take string after volume:
                roiVol = float(roiVol.rstrip('\n').replace(';',' '))
                roiVolumeList.append(roiVol)
    fsrc.close()
    return roiList, roiVolumeList


##List of ROIs##
def standardizedRoiNamesDict():
    """!
    @brief:
    @ROI library in format of {standard name : [all standardized Names]}
    @return:
    :  d: the class library
    """
    d = dict()
    d['AnalCanal'] = ['AnalCanal']
    d['AnalSphincter'] = ['AnalSphincter']
    d['Aorta'] = ['Aorta']
    d['Bladder'] = ['Bladder']
    d['BowelBag'] = ['BowelBag']
    d['BrachialPlexus_Ipsi'] = ['BrachialPlexus_Ipsi']
    d['BrachialPlexus_L'] = ['BrachialPlexus_L']
    d['BrachialPlexus_R'] = ['BrachialPlexus_R']
    d['Brain'] = ['Brain']
    d['BrainStem'] = ['BrainStem','Brainstem']
    d['Breast_L'] = ['Breast_L']
    d['Breast_R'] = ['Breast_R']
    d['BronchialTree_Prox'] = ['BronchialTree_Prox','BronchialTree']
    d['BaseOfTongue'] = ['BaseOfTongue']
    d['Carina'] = ['Carina']
    d['CaudaEquina'] = ['CaudaEquina']
    d['Cerebellum'] = ['Cerebellum']
    d['Cerebrum'] = ['Cerebrum']
    d['ChestWall'] = ['ChestWall']
    d['Chiasm'] = ['Chiasm']
    d['Cochlea_L'] = ['Cochlea_L']
    d['Cochlea_R'] = ['Cochlea_R']
    d['ConstrMuscle'] = ['ConstrMuscle']
    d['Cornea'] = ['Cornea']
    d['Duodenum'] = ['Duodenum']
    d['Ear_L'] = ['Ear_L']
    d['Ear_R'] = ['Ear_R']
    d['Esophagus'] = ['Esophagus']
    d['External'] = ['External']
    d['Eye_L'] = ['Eye_L']
    d['Eye_R'] = ['Eye_R']
    d['FemoralHead_L'] = ['FemoralHead_L']
    d['FemoralHead_R'] = ['FemoralHead_R']
    d['Femur_L'] = ['Femur_L']
    d['Femur_R'] = ['Femur_R']
    d['FrontalLobe'] = ['FrontalLobe']
    d['Glottis'] = ['Glottis']
    d['GreatVessel'] = ['GreatVessel']
    d['Heart'] = ['Heart']
    d['Hippocampus'] = ['Hippocampus']
    d['Hypopharynx'] = ['Hypopharynx']
    d['Hypothalamus'] = ['Hypothalamus']
    d['Kidney_L'] = ['Kidney_L']
    d['Kidney_R'] = ['Kidney_R']
    d['Larynx'] = ['Larynx']
    d['LacrimalGland_L'] = ['LacrimalGland_L']
    d['LacrimalGland_R'] = ['LacrimalGland_R']
    d['Lens_L'] = ['Lens_L']
    d['Lens_R'] = ['Lens_R']
    d['Lips'] = ['Lips']
    d['Liver'] = ['Liver']
    d['Lung_L'] = ['Lung_L']
    d['Lung_R'] = ['Lung_R']
    d['Mandible'] = ['Mandible']
    d['OccipitalLobe'] = ['OccipitalLobe']
    d['OpticNerve_L'] = ['OpticNerve_L']
    d['OpticNerve_R'] = ['OpticNerve_R']
    d['OralCavity'] = ['OralCavity']
    d['Oropharynx'] = ['Oropharynx']
    d['Pancreas'] = ['Pancreas']
    d['Parotid_L'] = ['Parotid_L']
    d['Parotid_R'] = ['Parotid_R']
    d['PenileBulb'] = ['PenileBulb']
    d['Perineum'] = ['Perineum']
    d['Pharynx'] = ['Pharynx']
    d['PharynxConst'] = ['PharynxConst']
    d['Pituitary'] = ['Pituitary']
    d['Prostate'] = ['Prostate']
    d['PubicSymphysis'] = ['PubicSymphysis']
    d['Rectum'] = ['Rectum']
    d['Sacrum'] = ['Sacrum']
    d['SeminalVesicle'] = ['SeminalVesicle']
    d['Sigmoid'] = ['Sigmoid']
    #d['Skin'] = ['Skin']
    d['SpinalCord'] = ['SpinalCord']
    d['Stomach'] = ['Stomach']
    d['Supraglottis'] = ['Supraglottis']
    d['TemporalLobe_L'] = ['TemporalLobe_L']
    d['TemporalLobe_R'] = ['TemporalLobe_R']
    d['Thyroid'] = ['Thyroid']
    d['Trachea'] = ['Trachea']
    d['Urethra'] = ['Urethra']
    d['Uterus'] = ['Uterus']
    d['Vagina'] = ['Vagina']
    d['VocalCords'] = ['VocalCords']
    d['Vulva'] = ['Vulva']
    return d



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--patientfile", help="Path of the Patient file that exist in the patient folder!")
    parser.add_argument("-r", "--roifile", help="Path of the plan.roi file")
#    parser.add_argument("-l", "--roinames", help="A comma separated list of roi names to perform robustness analysis!")
    parser.add_argument("-f", "--fractionnumber", help="Number of Tx fractions")
    parser.add_argument("-sf", "--subFXnumber", help="Number of Tx fractions")
    parser.add_argument("-v", "--numVirSimulation", help="Number of virtual simulations")
    parser.add_argument("-d", "--destinationfolder", help=" The result files are stored in this folder.")
    parser.add_argument("-tdh", "--totaldoseheader", help=" Total dose header file path.")
    parser.add_argument("-tdih", "--totaldoseimageheader", help=" Total dose image header file path.")
    parser.add_argument("-tdi", "--totaldoseimage", help=" Total dose image file path.")
    parser.add_argument("-us", "--systematicuncertaitnySigmas", help=" A comma separated list of translation[cm] and rotation[Deg] STD for systematic. Expect 6 numbers [stX,stY,stZ,srX,srY,srZ]")
    parser.add_argument("-ur", "--randomuncertaitnySigmas", help="  A comma separated list of translation[cm] and rotation[Deg] STD for random error. Expect 6 numbers [rtX,rtY,rtZ,rrX,rrY,rrZ]")
    parser.add_argument("-ui", "--intraFxuncertaitnySigmas", help="  A comma separated list of translation[cm] and rotation[Deg] STD for random error. Expect 6 numbers [itX,itY,itZ,irX,irY,irZ]")

    parser.add_argument("-pn", "--planname", help="  Plan Name ")
    parser.add_argument("-tn", "--trialname", help="  Trial Name ")
    parser.add_argument("-pp","--plandotpinnacle", help="plan.Pinnacle file (in plan folder)")



    args = parser.parse_args()
    if args.patientfile is None:
        parser.error('No patient file path is provided')
    else:
        # print 'input patientfile = ' + args.patientfile
        if not os.path.isfile(args.patientfile):
            parser.error('Patient file does not exist at -->' + args.patientfile)
            sys.exit() #

    if args.roifile is None:
        parser.error('No roi file is defined. plan.roi file  are usually found in Plan_X folders.')
    else:
        if not (os.path.isfile(args.roifile)):
            parser.error('Roi file does not exist at->' + args.roifile)
            sys.exit() # TODO: should return something

    if args.destinationfolder is None:
         print ('No destination folder is defined. The result will be store where the totalDose files reside')
         # parser.error('No destination folder is defined.')  # TODO: should it be mandatory to define this function
    else:
        if not (os.path.isdir(args.destinationfolder)):
            parser.error('Desitination path is not a directory->' + args.destinationfolder)
            sys.exit()

    if args.totaldoseheader is None:
        parser.error('TotalDose header not provided. This file can be usually found in Plan_X/dose folders.')
    else:
        if not (os.path.isfile(args.totaldoseheader)):
            parser.error('TotalDose header does not exist at->' + args.totaldoseheader)
            sys.exit() # TODO: should return something

    if args.totaldoseimageheader is None:
        parser.error('TotalDose image header not provided. This file can be usually found in Plan_X/dose folders.')
    else:
        if not (os.path.isfile(args.totaldoseimageheader)):
            parser.error('TotalDose image header does not exist at->' + args.totaldoseimageheader)
            sys.exit() # TODO: should return something

    if args.totaldoseimage is None:
        parser.error('TotalDose image file not provided. This file can be usually found in Plan_X/dose folders.')
    else:
        if not (os.path.isfile(args.totaldoseimage)):
            parser.error('TotalDose image file does not exist at->' + args.totaldoseimage)
            sys.exit() # TODO: should return something

    if args.systematicuncertaitnySigmas is None:
        parser.error('STD parameters [stX,stY,stZ,srX,srY,srZ] for systematic error are not set. use -h to get more information.')
    else:
        sysUncerParamsList = args.systematicuncertaitnySigmas.split(',')
        stX = float(sysUncerParamsList[0])
        stY = float(sysUncerParamsList[1])
        stZ = float(sysUncerParamsList[2])
        srX = float(sysUncerParamsList[3])
        srY = float(sysUncerParamsList[4])
        srZ = float(sysUncerParamsList[5])
        print '[stX,stY,stZ,srX,srY,srZ] = ' + str([stX,stY,stZ,srX,srY,srZ])

    if args.randomuncertaitnySigmas is None:
        parser.error('STD parameters [rtX,rtY,rtZ,rrX,rrY,rrZ] for random error are not set. use -h to get more information.')
    else:
        randomUncerParamsList = args.randomuncertaitnySigmas.split(',')
        rtX = float(randomUncerParamsList[0])
        rtY = float(randomUncerParamsList[1])
        rtZ = float(randomUncerParamsList[2])
        rrX = float(randomUncerParamsList[3])
        rrY = float(randomUncerParamsList[4])
        rrZ = float(randomUncerParamsList[5])
        print '[rtX,rtY,rtZ,rrX,rrY,rrZ] = ' + str([rtX,rtY,rtZ,rrX,rrY,rrZ])

    if args.intraFxuncertaitnySigmas is None:
        itX = float(0.0)
        itY = float(0.0)
        itZ = float(0.0)
        irX = float(0.0)
        irY = float(0.0)
        irZ = float(0.0)
        print '[itX,itY,itZ,irX,irY,irZ] = ' + str([itX, itY, itZ, irX, irY, irZ])
    else:
        intraFxUncerParamsList = args.intraFxuncertaitnySigmas.split(',')
        itX = float(intraFxUncerParamsList[0])
        itY = float(intraFxUncerParamsList[1])
        itZ = float(intraFxUncerParamsList[2])
        irX = float(intraFxUncerParamsList[3])
        irY = float(intraFxUncerParamsList[4])
        irZ = float(intraFxUncerParamsList[5])
        print '[itX,itY,itZ,irX,irY,irZ] = ' + str([itX, itY, itZ, irX, irY, irZ])





    if args.planname is None:
        parser.error('PlanName is needed.')
    else:
        print 'planname = ' + args.planname

    if args.trialname is None:
        parser.error('trialname is needed.')
    else:
        print "trialname = " + args.trialname


    if args.plandotpinnacle is None:
        parser.error('plan.Pinnacle file path is not set')
    else:
        if not (os.path.isfile(args.plandotpinnacle)):
            parser.error('TotalDose header does not exist at->' + args.plandotpinnacle)
            sys.exit() # TODO: should return something
        print 'plan.Pinnacle path= ' + args.plandotpinnacle

    # if args.roinames is None:
    #     parser.error('No roi name is provided.')
    # else:
    #     roiList = args.roinames.split(',')
    #     if len(roiList)<1:
    #         parser.error('check roi lists->' + args.roinames)
    #
    # print roiList



    roiList, roiVolumeList = listRoiNames(args.roifile)
    print zip(roiList,roiVolumeList)


    # Filter these rois and find the ones that are meaningful --> 0<volume <ub , standardized names
    standRoiList =[]
    for key, values in standardizedRoiNamesDict().items():  # check each key in roiList()
         for val in values:
             standRoiList.append(val)
    maxVolume = 4000
    # Standardized OARs + GTV1 and filtering based on maximum volume# TODO: we could also filter the ROIs which are not drawn in the imageset the dose is computed for
    roiListToRunRTRA =[]
    print zip(roiList,roiVolumeList)
    for roi,volume in zip(roiList,roiVolumeList):
        if (roi in standRoiList) and (roi != 'External') and (volume < maxVolume):
            roiListToRunRTRA.append(roi)
        elif (('GTV' in roi) or ('CTV' in roi)) and (('-GTV' not in roi) or ('-' not in roi)):
            roiListToRunRTRA.append(roi)




    if len(roiListToRunRTRA)<1:
        raise ValueError('No standard roi name is found in the plan.roi. Please run standardization script.')
    print 'Standaridized ROI names'
    roiListToRunRTRACSV =  ",".join(roiListToRunRTRA)
    print roiListToRunRTRACSV




    if args.fractionnumber is None:
        parser.error('Number of Fractions is not provided.')
    else:
        print 'Number of fractions is : ' + str(int(args.fractionnumber))

    if args.numVirSimulation is None:
        parser.error('Number of Virtual Treatment Simulations is not provided.')
    else:
        print 'Number of virtual treatment simulation is : ' +  str(int(args.numVirSimulation))


    if args.subFXnumber is None:
        print 'number of subFractions for intrafraction motion is not provided. Setting it to zero'
        numSubFrac = 0
    else:
        numSubFrac = int(args.subFXnumber)



    #
    # # now need to send the Patient file with all the ct images and headers to the computation destination
    #RTRA= '10.4.157.63'
    RTRA = '10.4.157.67'
    #currentUser = getpass.getuser()
    currentUser = 'mcvque'
    print 'User:' + currentUser
    #
    #
    with open(args.patientfile) as f:
        for line in f:
            if "MedicalRecordNumber = " in line:
                index = line[0:-2].find('=')
                MedicalRecordNumber = line[index+3:-3]
                print "MedicalRecordNumber = " + str(MedicalRecordNumber)
            if "FirstName = " in line:
                index = line[0:-2].find('=')
                FirstName = line[index+3:-3]
                print "FirstName = " + str(FirstName)
            if "LastName = " in line:
                index = line[0:-2].find('=')
                LastName = line[index+3:-3]
                print "LastName= " + str(LastName)
    #
    #
    applicationPathInRTRA = '/home/mcvque/gitRepos/rtra/RTRA'
    # now make a new patient folder in RTRA TempFolder
    sshClient = paramiko.SSHClient()
    sshClient.load_system_host_keys()
    try:
        sshClient.connect(RTRA, username =currentUser)
        print 'This user is allowed to used RTRA!'
    except: #(paramiko.BadHostKeyException, paramiko.AuthenticationException,paramiko.SSHException, socket.error) as e:
        #print e
        raise ValueError('You are not allowed to connect to RTRA. Please contact HN/JVS/WTW/CH.')



    RTRASaveBaseFolderPath = '/home/mcvque/temp'
    PatientTempFolderName = FirstName.replace(" ", "")+'_'+ LastName.replace(" ", "") +'_'+ MedicalRecordNumber +'_'+ datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
    RTRAPatientFolderPath = RTRASaveBaseFolderPath + '/' + PatientTempFolderName
    stdin, stdout, stderr = sshClient.exec_command('cd ' + RTRASaveBaseFolderPath) # makeBinaryMaskTempFolder
    if len(stderr.readlines()) >0:
        sshClient.close()
        raise ValueError('No folder : ' + RTRASaveBaseFolderPath + ' in RTRA')
        sys.exit()

    stdin, stdout, stderr = sshClient.exec_command('mkdir ' + RTRAPatientFolderPath) # makeBinaryMaskTempFolder
    exit_status = stdout.channel.recv_exit_status()          # Blocking call
    if exit_status != 0:
        print("Error", exit_status)
        for line in stderr.readlines():
            print line
        sshClient.close()
        raise ValueError('cannot make patient folder at: '+ RTRAPatientFolderPath +' on RTRA machine! Notice it is extremely unlikely that your patient folder already exists their.')
        sys.exit()




    # first copy aise ValueErrorpatient file to the destination
    print 'Copying roifile to RTRA'
    cmdToRun = 'scp '+ args.patientfile +' '+currentUser+'@'+RTRA+':' +RTRAPatientFolderPath
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid

    # copy roi file over
    print 'Copying patient to RTRA '
    cmdToRun = 'scp '+ args.roifile +' '+currentUser+'@'+RTRA+':' +RTRAPatientFolderPath
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid

  # copy plan.Pinnacle file over
    print 'Copying patient to RTRA '
    cmdToRun = 'scp '+ args.plandotpinnacle +' '+currentUser+'@'+RTRA+':' +RTRAPatientFolderPath
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid

    # extract total dose image path
    planDotPinnacleFileName,jnk  = path_leaf(args.plandotpinnacle.replace(" ", ""))

    # extract total dose header file path
    tdhFileName,jnk = path_leaf(args.totaldoseheader.replace(" ", ""))

    # extract total dose image header path
    tdihFileName,jnk  = path_leaf(args.totaldoseimageheader.replace(" ", ""))

    # extract total dose image path
    tdiFileName,jnk  = path_leaf(args.totaldoseimage.replace(" ", ""))



    # copy total dose to RTRA
    print 'Copying total dose header to RTRA '
    cmdToRun = 'scp "'+ args.totaldoseheader +'" '+currentUser+'@'+RTRA+':' + RTRAPatientFolderPath + '/' + tdhFileName
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid

    # copy total dose header to RTRA
    print 'Copying total dose header to RTRA '
    cmdToRun = 'scp "'+ args.totaldoseimageheader +'" '+currentUser+'@'+RTRA+':' + RTRAPatientFolderPath + '/' + tdihFileName
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid


    # copy total dose header to RTRA
    print 'Copying total dose image header to RTRA '
    cmdToRun = 'scp "'+ args.totaldoseimage +'" '+currentUser+'@'+RTRA+':' + RTRAPatientFolderPath + '/' + tdiFileName
    subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid



    # now extract the patient filepath and folder
    patientFilename,patientFolderPath= path_leaf(args.patientfile)
    # now extract the roifile path and folder
    roiFilename,roiFolderPath= path_leaf(args.roifile)


    print patientFolderPath

    # loop through patient folder and copy img file and headers  to destinatioin folder
    for root, dirs, files in os.walk(patientFolderPath):
         for file in files:
            if file.endswith(".img") or file.endswith(".header"):
                 print('Copying patient '+ os.path.join(root, file) + '  to RTRA')
                 cmdToRun = 'scp '+ os.path.join(root, file) +' '+currentUser+'@'+RTRA+':'+ RTRAPatientFolderPath
                 subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid
         del dirs[:] # this breaks


    # now make a patient folder in temp result folder in RTRA
    RTRATempResultFolderPath = RTRASaveBaseFolderPath + '/Result/' + PatientTempFolderName

    stdin, stdout, stderr = sshClient.exec_command('mkdir ' + RTRATempResultFolderPath)
    exit_status = stdout.channel.recv_exit_status()          # Blocking call
    if exit_status != 0:
        print("Error", exit_status)
        for line in stderr.readlines():
            print line
        sshClient.close()
        raise ValueError('cannot make patient folder in: '+ RTRATempResultFolderPath +' in RTRA. Notice it is extremely unlikely that your patient folder already exists their.')
        sys.exit()


    patientFilePathInRTRA = RTRAPatientFolderPath + '/' + patientFilename
    roiFilePathInRTRA = RTRAPatientFolderPath + '/' + roiFilename
    planDotPinnaclePathInRTRA = RTRAPatientFolderPath + '/' + planDotPinnacleFileName
    tdhFilePathInRTRA = RTRAPatientFolderPath + '/' + tdhFileName
    tdihFilePathInRTRA = RTRAPatientFolderPath + '/' + tdihFileName
    tdiFilePathInRTRA = RTRAPatientFolderPath + '/' + tdiFileName

    desiredRoiNameSpaceSeparated= ' '.join('\"' + item + '\"' for item in roiListToRunRTRA)

    #applicationPathInRTRA +
    cmdToRun = ' -p \"' + patientFilePathInRTRA+'\" -r \"' + roiFilePathInRTRA+'\" '+ desiredRoiNameSpaceSeparated + \
               ' -resultfolder \"' + RTRATempResultFolderPath + '\"' + \
               ' -tdh \"' + tdhFilePathInRTRA + '\" -tdih \"' + tdihFilePathInRTRA + '\" -tdi \"' + tdiFilePathInRTRA +'\"'+\
               ' -planname \"' + args.planname + '\"' + ' -trialname \"' + args.trialname + '\"' +\
               ' -pp \"' + planDotPinnaclePathInRTRA + '\"' +\
               ' -stX ' + str(stX) + ' -stY ' + str(stY) + ' -stZ ' + str(stZ) + \
               ' -rtX ' + str(rtX) + ' -rtY ' + str(rtY) + ' -rtZ ' + str(rtZ) + \
               ' -itX ' + str(itX) + ' -itY ' + str(itY) + ' -itZ ' + str(itZ) + \
               ' -srX ' + str(srX) + ' -srY ' + str(srY) + ' -srZ ' + str(srZ) +\
               ' -rrX ' + str(rrX) + ' -rrY ' + str(rrY) + ' -rrZ ' + str(rrZ) + \
               ' -irX ' + str(irX) + ' -irY ' + str(irY) + ' -irZ ' + str(irZ) + \
               ' -subfrac ' + str(int(numSubFrac)) +  \
               ' -frac ' + str(int(args.fractionnumber)) + ' -numSim ' + str(int(args.numVirSimulation))

    print cmdToRun

    with open(args.destinationfolder+ "/runCommand.txt", "w") as text_file:
        text_file.write("%s" % cmdToRun)

    print 'Copying runCommand to RTRA '
    cmdToRunCp = 'scp "'+ args.destinationfolder +'/runCommand.txt" '+currentUser+'@'+RTRA+':' + RTRATempResultFolderPath + '/runCommand.txt'
    subprocess.call(cmdToRunCp, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid

    print ("Added the Job to the Que waiting for the report..........")
    timeOutTime = 300 # wait if report file is not ready just return
    secondsElapsed = 0
    flagReportIsReady = False
    cmdToRun = 'ls  '+ RTRATempResultFolderPath+'/Reports/*.pdf'
    while not flagReportIsReady:
        stdin,stdout,stderr = sshClient.exec_command(cmdToRun)
        exit_status = stdout.channel.recv_exit_status()

        if exit_status == 0:
            for line in stdout.readlines():
                print(line)
            print ("No error")
            flagReportIsReady =True
        else:
            time.sleep(1)
            secondsElapsed = secondsElapsed +1
            if (secondsElapsed>timeOutTime):
                print 'Timeout ... check location in RTRA system:' + RTRATempResultFolderPath+'/Reports/'
                sshClient.close()
                sys.exit()

    sshClient.close()
    print 'Report is generated! Loading report using pdf viewer'
    time.sleep(20)
#    subprocess.call('ssh -X ' +currentUser +'@'+RTRA + ' evince ' + RTRATempResultFolderPath+'/Reports/*.pdf', shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT,preexec_fn=os.setsid
    subprocess.call('ssh -X ' +currentUser +'@'+RTRA + '  /home/shared/apps/foxitsoftware/foxitreader/FoxitReader ' + RTRATempResultFolderPath+'/Reports/*.pdf', shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT,preexec_fn=os.setsid




    # # # X connection using paramiko fails in Solaris --> look at testX11Paramiko.py
    # # A simple method is to pass the
    # # now add this command to que file at RTRA
    # cmdToRun2  ='echo \"' + cmdToRun + '\" >> ' + RTRATempResultFolderPath + '/runCommand.txt'
    # # stdin,stdout,stderr = sshClient.exec_command('source ~/.bashrc')
    # stdin,stdout,stderr = sshClient.exec_command(cmdToRun2)
    # exit_status = stdout.channel.recv_exit_status()
    # if exit_status == 0:
    #     for line in stdout.readlines():
    #         print(line)
    #     print ("Added the Job to the Que waiting for the report..........")
    # else:
    #     print("Error", exit_status)
    #     for line in stderr.readlines():
    #         print line
    #     sshClient.close()
    #     raise ValueError('ERROR: in running command at RTRA.' + cmdToRun)
    #     sys.exit()

    # now check every 5 secends if the result is ready in RTRA result folder for this patient


    # this method fails  in app0 and ovdc system
    # cmdToRun3 = 'ssh -X mcvque@' + RTRA +' "source ~/.profile; '+ applicationPathInRTRA + cmdToRun + '"'
    # print cmdToRun3
    # subprocess.call(cmdToRun3, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid


    # # This method fails
    # stdin, stdout, stderr = sshClient.exec_command('\"source ~/.profile; '+ applicationPathInRTRA + cmdToRun + '\"')
    # exit_status = stdout.channel.recv_exit_status()          # Blocking call
    # if exit_status == 0:
    #     for line in stdout.readlines():
    #         print(line)
    #     print ("Process done!")
    # else:
    #     print("Error", exit_status)
    #     for line in stderr.readlines():
    #         print line
    #     sshClient.close()
    #     raise ValueError('ERROR: in running command at RTRA.' + applicationPathInRTRA + cmdToRun )
    #     sys.exit()





    # if args.destinationfolder is None:
    #     print 'Since the destination folder is not defined by user, the results are not saved on this machine, A temp copy of result could be found at ' + RTRATempResultFolderPath + 'in RTRA machine!'
    # else:
    #     print 'Copying the results to destination folder:' + args.destinationfolder
    #     cmdToRun = 'scp '+ '-r'+' '+currentUser+'@'+RTRA+':' +RTRATempResultFolderPath + ' ' + args.destinationfolder
    #     subprocess.call(cmdToRun, shell=True) # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid
    sshClient.close()
