#ifndef INTERPOLATIONALGORTIHMS_H
#define INTERPOLATIONALGORTIHMS_H
#include <vector>
#include <math.h> //nanf
// http://codereview.stackexchange.com/questions/120795/linear-interpolation-c


class InterpolationAlgortihms
{
public:
    InterpolationAlgortihms();
    template<typename T>
    T bilinear(const float &tx, const float &ty, const T &c00, const T &c10, const T &c01, const T &c11);

    void interp3(std::vector<float> &vOutput, const std::vector<float> &gridX, const std::vector<float> &gridY, const std::vector<float> &gridZ, const std::vector<float> &vGrid, const std::vector<float> &xq, const std::vector<float> &yq, const std::vector<float> &zq);
};

#endif // INTERPOLATIONALGORTIHMS_H
