#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


// Header needs for log
#include <boost/iostreams/stream_buffer.hpp>
#include <iostream>
#include <fstream>
#include "JJGDVCGuiDebugBuffer.h"
#include "Common/PatientInfo.h"


#define  OK_RTRA  true
#define  Failed_RTRA false

#include "qcustomplot/qcustomplot.h"
// Robustness analyszer
#include "Algorithm/RobustnessAnalyzer.h"


class MainWindow : public QMainWindow
{
Q_OBJECT

    void testInterpolationAlgorithm();

    void testRobustnessAnalyzerPinnacle();

    void testRobustnessAnalyzerPinnacleWithIntraFractionRigidMotion();

    void testUncertaintyModel();

    void testPinnacleFilesParsing();

    void testTotalDoseRead();

    void testBinaryRead();

    void testDICOMParser();

    void testrtFileParsing();

    void testIntersectionOfTwoSetsWithReconIndices();

    void setInitialParamsForApplication();


    // Loads
    QString patientCurrentFolderQString = QString();
    bool isPatientFolderSelected;
    bool isPatientFolderDicom;
    bool isPatientFolderPinnacle;


    // debug / logging
    std::string debug_capture;
    boost::iostreams::stream_buffer<JJGDvcGuiDebugBuffer> debug_buffer;
    std::ofstream logfile; // need to define a defalut constructor -  http://stackoverflow.com/questions/3997099/why-this-error-no-appropriate-default-constructor-available
    std::ostream logstream;
    std::string prefix;
    long verbose;

    QCustomPlot *customPlot;


public:
    MainWindow(QWidget *parent = 0);

    ~MainWindow();

    void listRoisForGivenPatient(QString patientPath, QString planDotRoiPath);

    void testPrintReport(RobustnessAnalyzer *robustnessAnalyzer, PinnacleFiles *myPinnacleFiles,
                         const QString &resultFolder, const QString &trialName, const QString &PlanName);


    void robustnessAnalyzerPinnacle(QString patientPath,
                                    QString planDotRoiPath,
                                    QString planDotPinnaclePath,
                                    const std::vector<std::string> &desiredRoiList,
                                    QString totalDoseHeaderFilePath,
                                    QString totalDoseImageFilePath,
                                    QString totalDoseImageHeaderFilePath,
                                    QString resultFolder,
                                    float sysTranSigmaX,
                                    float sysTranSigmaY,
                                    float sysTranSigmaZ,
                                    float randomTranSigmaX,
                                    float randomTranSigmaY,
                                    float randomTranSigmaZ,
                                    float sysRotSigmaX,
                                    float sysRotSigmaY,
                                    float sysRotSigmaZ,
                                    float randomRotSigmaX,
                                    float randomRotSigmaY,
                                    float randomRotSigmaZ,
                                    float intraFxTranSigmaX,
                                    float intraFxTranSigmaY,
                                    float intraFxTranSigmaZ,
                                    float intraFxRotSigmaX,
                                    float intraFxRotSigmaY,
                                    float intraFxRotSigmaZ,
                                    int numFrac,
                                    int numSimulation,
                                    int numSubFrac,
                                    QString trialName,
                                    QString planName,
                                    bool debug);


    void robustnessAnalyzerDICOM(QString dicomPath,
                                 QString dicomStructurePath,
                                 QString dicomDosePath,
                                 const std::vector<std::string> &desiredRoiList,
                                 QString resultFolder,
                                 float sysTranSigmaX,
                                 float sysTranSigmaY,
                                 float sysTranSigmaZ,
                                 float randomTranSigmaX,
                                 float randomTranSigmaY,
                                 float randomTranSigmaZ,
                                 float sysRotSigmaX,
                                 float sysRotSigmaY,
                                 float sysRotSigmaZ,
                                 float randomRotSigmaX,
                                 float randomRotSigmaY,
                                 float randomRotSigmaZ,
                                 float intraFxTranSigmaX,
                                 float intraFxTranSigmaY,
                                 float intraFxTranSigmaZ,
                                 float intraFxRotSigmaX,
                                 float intraFxRotSigmaY,
                                 float intraFxRotSigmaZ,
                                 int numFrac,
                                 int numSimulation,
                                 int numSubFrac,
                                 QString trialName,
                                 QString planName,
                                 bool debug);


    void plotColorMap();

private slots:

    bool specifyPatientFolder();


    void
    writeResults(const QString &resultFolder, const PinnacleFiles &myPinnacleFile,
                 RobustnessAnalyzer *robustnessAnalyzer) const;

    void runRobustnessAnalyzer(RobustnessAnalyzer *robustnessAnalyzer) const;

    void setRigidBodyMotion(ModelData *modelData, const std::string &RoiToRotateAroundCOMString,
                            float sysTranSigmaX, float sysTranSigmaY, float sysTranSigmaZ,
                            float sysRotSigmaX, float sysRotSigmaY, float sysRotSigmaZ,
                            float randomTranSigmaX, float randomTranSigmaY, float randomTranSigmaZ,
                            float randomRotSigmaX, float randomRotSigmaY, float randomRotSigmaZ,
                            int numSubFrac,
                            float intraFxTranSigmaX, float intraFxTranSigmaY, float intraFxTranSigmaZ,
                            float intraFxRotSigmaX, float intraFxRotSigmaY, float intraFxRotSigmaZ) const;

    void
    setIntraFracMotionError(const ModelData *modelData, int numSubFrac, float intraFxTranSigmaX,
                            float intraFxTranSigmaY, float intraFxTranSigmaZ, float intraFxRotSigmaX,
                            float intraFxRotSigmaY, float intraFxRotSigmaZ) const;

    void robustAnalyzer(PinnacleFiles &myPinnacleFile, RtDosePinn &rtDosePinnTotalDose, const QString &resultFolder,
                        float sysTranSigmaX, float sysTranSigmaY, float sysTranSigmaZ,
                        float randomTranSigmaX, float randomTranSigmaY, float randomTranSigmaZ,
                        float sysRotSigmaX, float sysRotSigmaY, float sysRotSigmaZ,
                        float randomRotSigmaX, float randomRotSigmaY, float randomRotSigmaZ,
                        float intraFxTranSigmaX, float intraFxTranSigmaY, float intraFxTranSigmaZ,
                        float intraFxRotSigmaX, float intraFxRotSigmaY, float intraFxRotSigmaZ,
                        int numFrac, int numSimulation, int numSubFrac,
                        const QString &trialName, const QString &planName, bool debug);
};

#endif // MAINWINDOW_H
