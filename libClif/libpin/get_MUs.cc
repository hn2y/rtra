/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* get_MUs_for_beam.cc
   Reads in monitor units interactively or non-interactively 
   Created: 11 Feb, 2000: PJK based on read_ct_scanner_name
   Modification History:
      26 Sept 2000: JVS: Surprise! Pinnacle actually does use/store floating point 
                         Monitor Units....add floating point versions of read_pinn_mus
                         NOTE ALSO DANGER.....OLD ROUTINES READ FROM .Current.
       Nov 27, 2007: JVS: Get rid of compiler warnings 
*********************************************************************************** */
#include <stdlib.h>      // for calloc
#include <stdio.h>
#include <string.h>      // for strcat and strcpy
#include "string_util.h" // for get_value
#include "utilities.h"   // for error routines
#include "typedefs.h"
//#include "dose_region.h" // for definition of dose regions structure and function prototypes
#include "read_clif.h"
#include "get_MUs.h"

#include "myPinnacle.h"
#include "myPinnComm.h"
/* *********************************************************************************** */
int read_pinn_MUs(char *plan_fname, char *trial_name, int *MUs, long myPinnWindowID)
{
  if(myPinnWindowID > 0 )  // active pinnacle session
  {
   // Get MUs
   int itmp = -100;
   int iStatus = 1;
   float MonitorUnits;
   char pinnCommString[MAX_STR_LEN];
   strcpy(pinnCommString, "TrialList.Current.PrescriptionList.Current.RequestedMonitorUnitsPerFraction");
   iStatus = iPinnComm_QueryFloat(NULL, pinnCommString, &MonitorUnits);
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: Querying MUs\n");
      return(FAIL);
   }
   strcpy(pinnCommString,"TrialList.Current.PrescriptionList.Current.RequestedMonitorUnitsPerFraction");
   iStatus = iPinnComm_QueryInt(NULL,pinnCommString, &itmp);
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: Querying MUs\n");
      return(FAIL);
   }

   *MUs = itmp;
   printf("\n (Interactive) MUs %d (from %d)",*MUs, itmp);
   printf("\n Floating Point MU's = %f", MonitorUnits);
  }

  else // non-active pinnacle session
  {
   if(read_pinn_MUs(plan_fname, trial_name, MUs) != OK)
   {
      eprintf("\n ERROR: (Non-Active) Getting CT scanner type"); 
      return(FAIL);
   }
  }
  return(OK);
}
/* ********************************************************************************* */
int read_pinn_MUs(char *fname_stem, char *Trial_Name, int *MUs)
{
  if(1){
   printf("\n ERROR: This code not checked yet"); 
   return(FAIL);
  }

   char pbeam_fname[MAX_STR_LEN];
   strcpy(pbeam_fname,fname_stem);  // put first part on name
   strcat(pbeam_fname,".Trial");    // append .Trial to the name  
   // open dose region file
   FILE *fp = fopen(pbeam_fname,"r");
   if(fp==NULL)
   {
      eprintf("\n ERROR: opening dose region file %s", pbeam_fname);
      return(FAIL);
   }
   int beam_found = 0;
   // get to the desired trial
   do{
      if(clif_get_to_line(fp,"Trial ={")!=OK)
      {
         eprintf("\n ERROR: finding Trial in file %s",pbeam_fname);
         return(FAIL);
      }
      char Current_Trial_Name[MAX_STR_LEN];
      // get and report Trial Name
      if(clif_string_read(fp,"Name =",Current_Trial_Name) != OK)  
         return(FAIL); 
      /* check if current trial matches */
      if(strcmp(Current_Trial_Name,Trial_Name) == 0) beam_found = 1;
      if(!beam_found)
      {  /* Close the Trial cliff */
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
            return(FAIL);
         }
      }
   }while(!beam_found);
#ifdef DEBUG
   printf("\n Reading In MUs for Trial %s", Trial_Name);
#endif
   int itmp = -100;
   itmp = (int) clif_get_value(fp, "RequestedMonitorUnitsPerFraction =");
   *MUs = itmp;
   printf("\n (Non-Interactive) MUs %d (from %d)",*MUs, itmp);

   // currently, read in only 3-D dose grid from Pinnacle
   // close context of Trial structure
   if(clif_get_to_line(fp,"};")!=OK)
   {
      eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
      return(FAIL);
   }
   // close dose region file
   fclose(fp);
   return(OK);
} 
/* ********************************************************************************* */
int readPinnacleMonitorUnits(char *plan_fname, char *trial_name, float *MonitorUnits, long myPinnWindowID)
{
  if(myPinnWindowID > 0 )  // active pinnacle session
  {
   // Get MUs
   int iStatus = 1;
   char pinnCommString[MAX_STR_LEN];
   strcpy(pinnCommString,"TrialList.Current.PrescriptionList.Current.RequestedMonitorUnitsPerFraction");
   iStatus = iPinnComm_QueryFloat(NULL, pinnCommString, MonitorUnits);
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: Querying MUs\n"); return(FAIL);
   }
   printf("\n (Interactive) Monitor Units %f ",*MonitorUnits);
  }
  else // non-active pinnacle session
  {
   if(readPinnacleMonitorUnits(plan_fname, trial_name, MonitorUnits) != OK)
   {
      printf("\n ERROR: readPinnacleMonitorUnits (non-interactive)"); 
      return(FAIL);
   }
  }
  return(OK);
}
/* ********************************************************************************* */
int readPinnacleMonitorUnits(char *fname_stem, char *TrialName, float *MonitorUnits)
{
  if(1) {
   printf("\n ERROR: This code not checked yet"); 
   return(FAIL);
  }
   // This code will be WRONG when more than 1 prescription exists, JVS

   char pbeamFilename[MAX_STR_LEN];
   strcpy(pbeamFilename,fname_stem);  // put first part on name
   strcat(pbeamFilename,".Trial");    // append .Trial to the name  
   FILE *istrm = fopen(pbeamFilename,"r");
   if(istrm==NULL)
   {
      printf("\n ERROR: opening patient beam file %s", pbeamFilename);return(FAIL);
   }
   clifObjectType *TrialList = new(clifObjectType);
   if(TrialList == NULL)
   {
      printf("\n ERROR: Allocating Memory for clifObjectType\n"); return(FAIL);
   }
   if( readSpecificClif(istrm, TrialList, "Trial", TrialName) != OK)
   {
      printf("\n ERROR: Reading clif from file %s\n", pbeamFilename); return(FAIL);
   }
   fclose(istrm); // close the input stream

   if(readFloatClifStructure(TrialList, "PrescriptionList.Prescription.RequestedMonitorUnitsPerFraction",MonitorUnits )!= OK ) 
   {
      printf("\n ERROR: Determining Monitor Units");
      return(FAIL);   
   }
   return(OK);
} 
/* ********************************************************************************* */
