#ifndef cf_read_clif_h_included
#define cf_read_clif_h_included

#define END_OF_CONTEXT 10
#define NOT_FOUND      20
#define END_OF_FILE    30
#define MAXDELIM 2

/* Define the "types" needed for these routines */
typedef struct clifDataType{
  char *Handle;
  char *Value; 
  char Delim[MAXDELIM];
  int isquote;
}clifDataType;

typedef struct clifArrayType{
  char *Handle;
  float *Value;
  int nValues;
}clifArrayType;

typedef struct clifObjectType{
  char *Handle;              /* name of the object     */
  clifDataType   *Data;      /* Array of Data Objects  */
  clifArrayType  *Array;     /* Array of Array Objects */
  clifObjectType *Object;    /* Array of Sub-objects   */
  int nDataObjects;          /* Number of each type of object */
  int nArrayObjects;
  int nObjects;
  int clifLevel;             /* for keeping track of the level of the clif */
}clifObjectType;


#endif
