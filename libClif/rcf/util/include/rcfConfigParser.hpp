#ifndef __rcfConfigParser
#define __rcfConfigParser

#define RCF_MAXCONFIG 100
#define RCF_MAXTAG 2048
#define RCF_MAXKEY 100

#include <stdio.h>
#include "rcfAdmin.hpp"
#include "rcfClifParser.hpp"
#include "string.h"
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>

typedef struct
{
  char key[RCF_MAXKEY];
  char rel_key[RCF_MAXKEY];
}cfRelPathTable;

/**
    \class rcfConfigParser
    @ingroup UTIL
*/

class rcfConfigParser
{

   private:
      char *func;
      FILE *fid;
      char *lines[RCF_MAXCONFIG+1];
      int is_ready;
      int nlines;
      int last_found;
      char last_tag[RCF_MAXTAG];
      char * parse_token;
      rcfAdmin *admin;
      cfRelPathTable *tables;
      int ntables;
      std::vector<std::string*> parser_labels;
      std::vector<bool> my_parsers;
      std::vector<rcfConfigParser*> parsers;
      bool write_as_clif;
      std::string databaseConfigPath;
      std::string resolvedConfigPath;
      bool keepSpacesFlag;

   public:

      rcfConfigParser();
      /** Creates parser and open config file */
      rcfConfigParser(char *file);
      ~rcfConfigParser();

      /** Verify that object is ready for read */
      int isReady(); 
      int loadConfigFile(char *file); // in case object is constructed
                                      // without loading
      int buildParserFromClif(rcfClifParser *clif);
      int setRcfAdmin(rcfAdmin *usr_admin);

      // get tag-value pairs
      // tag - tag string user is looking for
      // value - value associated with tag
      // maxsize - maximum size of the value string
      // valid - has tag been found and is value valid?
      // returns failure only in case of application error
      // search result is signalled through the valid flag
      // IMPORTANT NOTE: these functions will always find
      // the same FIRST OCCURENCE of the tag-value pair. 
      // If user wants to get tag-value with multiple occurences
      // of tag, getRepeatTag() functions should be used.

      int getTag(char *tag, char *value, int maxsize, int *valid);
      int getTag(char *tag, int *value, int *valid);
      int getTag(char *tag, float *value, int *valid);
      /** Returns the value associated with this tag. Returns empty string on failure to find the tag.*/
      std::string getTag( std::string tag );
      rcfConfigParser* getParser(std::string tag,bool *exception);
      int addReferenceParser(std::string tag, rcfConfigParser *parser);
      int addManagedReferenceParser(std::string tag, rcfConfigParser *parser);

      // Family of functions getRepeatTag are "working functions"
      // which underlie getTag family. This family can handle 
      // multiple lines with the same tag. If you search for
      // same tag twice (or n-times), the object will remember
      // location of past search and skip lines previously found.
      // The counter gets reset explicitely by calling member function
      // rewind() or implicitely by searching for a different tag. 
      // Consecutive searches for the same tag activate this feature
      // automatically. This may be confusing, as this is implicit
      // behavior. Use carefully.

      int getRepeatTag(char *tag, char *value, int maxsize, int *valid);
      int getRepeatTag(char *tag, int *value, int *valid);
      int getRepeatTag(char *tag, float *value, int *valid);
      std::string getRepeatTag( std::string tag );

      // prepare the object to write the config file
      int setUserLine(char *tag, char *value);
      int appendUserLine(char *tag, char *value);
      int appendUserLine(int index,char *tag, char *value);
      int writeConfigFile(char *path, char* name = NULL);

      //prepare the object for processing of relative paths
      int setNumberOfTriggerKeys(int nkeys);
      int setTriggerKey(int nkey, char *key);
      int addSubordinateKey(char *key,char *subkey);
      int addSubordinateKey(int nkey,char *subkey);

      // arms parser with key correlations according to current policy
      int armParser(); 

      /** Erases memory of past searches on this parser */
      void rewind();

      /** Overload assignment operator */
      rcfConfigParser& operator=(const rcfConfigParser& parser);

      /** Replaces a key/value pair */
      int replace(std::string tag, std::string value);

      /** Gets a key/pair map */
      std::map<std::string, std::string> getKeyValueMap();
      /** Returns the path used to load this config file. Returns NULL if this parser was not loaded from a file.*/
      std::string getDatabasePath(bool *verify); 
      std::string getResolvedPath(bool *verify); 

      friend std::ostream& operator<<(std::ostream& output, rcfConfigParser* parser);

      // these functions can be called to control space removal during parsing
      void keepSpaces();
      void removeSpaces();

      bool noCaseCompare(std::string s1, std::string s2);

  // service functions
  protected:

      void reportError(const char *str);
      void reportWarning(const char *str);
      int readConfigFile();
      int readConfigFileAsClif(char *path);
      int inspectConfigLine(int index);
      int getUniqueTag(char *tag, char *value, int maxsize, int *valid);
      int transferTables(rcfConfigParser *parser);
      int addReferenceParserSelect(std::string tag, rcfConfigParser *parser,bool manage);
      int writeDefaultConfigFile(std::ofstream & outfile);
      int writeClifConfigFile(std::ofstream & outfile);

  private:

      void basicInit();

      
};

#endif
