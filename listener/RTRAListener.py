#!/usr/bin/python
# -*- RTRAListener.py -*-#

"""

SYNOPSIS

    This file is intended to run in RTRA system as a service provider for clients systems that
    put request for robustness analysis.

DESCRIPTION

    Once there is change in the status RTRAAllResultFolder, This program finds runCommand.txt in each subfolder in  RTRAAllResultFolder. if completionFileName could not be found in a folder, the associated runCommand.txt will be added to the queue. 


EXAMPLES
   Just run it, it checks and makes sure that only one instance of this software is running.


VERSION 0.0

AUTHOR
    hnourzad on 3/21/17

MODIFICATION HISTORY:
    2020/11/23: JVS: Make compliant with python 3 for Stoat and git-based install, clean up
    2020/12/14: JVS: Refactor code and create start, stop, restart, status, and report commands

"""

import fcntl, sys, os
import platform
import argparse
from PyQt5 import QtCore
import datetime
import subprocess
import ntpath
import datetime
import getpass # to get user information
import signal
import sched
import threading
import time
import psutil
#-------------------------------------------------------------------------------
print(sys.argv)
pid_file = 'rtra_program.pid'
# print(f'{os.path.dirname(os.path.abspath(__file__))}')
# Set UVA_BIN and UNAME if not already set
UVA_BIN=os.getenv('UVA_BIN','/uva/bin')
UVA_CALC=os.getenv('UVA_CALC','/uva/calc')
UNAME=os.getenv('UNAME', f'{platform.machine()}-{platform.system().lower()}')
RTRA_exe = f'{UVA_BIN}/{UNAME}/RTRA' 
# '/home/shared/apps/RTRA/build/release/RTRA '
RTRAAllResultFolder = f'{UVA_CALC}/rtra/Result' # '/home/mcvque/temp/Result'
completionFileName = 'RTRALog.txt'
lock_filename = os.path.join(RTRAAllResultFolder, pid_file)
#
scheduler=None 
threadRunScheduler=None
lock_fp=None

# Make sure exe exists...
if not os.path.isfile(RTRA_exe) and not os.access(RTRA_exe, os.X_OK):
    print(f'ERROR: {RTRA_exe} not exist\n Exiting\n');
    sys.exit(0)

def report():
    print (f'\tRTRA_exe={RTRA_exe}')
    print (f'\tRTRAAllResultFolder={RTRAAllResultFolder}')
    print (f'\tlock_filename={lock_filename}')
#-------------------------------------------------------------------------------
# print('----------------------------')


# Set up a global to be modified by the threads
# counter = 0

def start_rtra():
    print(f'\tNull Start Function. ({__file__})')
   
def start_rtra_does_not_lock():
    print(f'\tInitiating RTRA service. ({__file__})')
    if check_if_running():
        print('\tNot restarting, RTRA already running...')
        sys.exit(0)
    else:
        print('\tStarting RTRA')
        if not os.path.isfile(lock_filename):
            os.chmod(lock_filename, 0o777)
        lock_fp = open(lock_filename, 'w')
        try:
            fcntl.lockf(lock_fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            print(f'\tLocking Failed!  Another RTRA service is running!')
            exit()
        scheduler = sched.scheduler(time.time, time.sleep)
        threadRunScheduler = threading.Thread(target=scheduler.run)

def stop_rtra():
    if check_if_running():
        print('\tStopping RTRA')
        #
        PROCNAME='RTRAListener.py'
        current_pid=os.getpid();
        #
        print(f'\tcurrent_pid == {current_pid}, {__file__}')
      
        for proc in psutil.process_iter():
            # check whether the process name matches
            # print(f"Name={proc.name()}, PID={proc.pid}, cmdline={proc.cmdline()}")
            if proc.name() == PROCNAME and proc.pid != current_pid:
                print(f"\t Killing: Name={proc.name()}, PID={proc.pid}, cmdline={proc.cmdline()}")
                proc.kill()
    else:
        print('\tRTRA is not running, no need to stop')

def restart_rtra():
    print('\tRestarting RTRA')
    stop_rtra()
    start_rtra()

def lock(lock_filename):
    print(f"\tlock checking for lockFile {lock_filename}")
    lock_fp = open(lock_filename, 'w')
    try:
        fcntl.lockf(lock_fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        return False
    return True

def check_if_running():
    print(f"\tChecking for lockFile {lock_filename}")
    if not lock(lock_filename):
        print(f'\tRTRA service is running! ({__file__})')
        return True
    else:
        print(f'\tRTRA service is not running')
        return False


def path_head_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head), tail


def getlines(filepath):
    with open(filepath) as f:
        lineList = []
        for line in f:
            lineList.append(line)
        return lineList

def idleJob(name):
    global flagExitIdle
    print('\tGoing to idle mode ... ')
    while not flagExitIdle:
        time.sleep(0.5)  # delays for 5 seconds
    print('\tExited from idle mode resetting flag in 0.5 second ')
    flagExitIdle = False

def scheduleIdleJob():
    global eIdle
    global flagExitIdle
    # global scheduler
    flagExitIdle = False
    eIdle = scheduler.enter(0, 1, idleJob, ('Idle',))

def comeOutFromIdle():
    print('\tExiting Idle mode ...')
    global flagExitIdle
    flagExitIdle = True

def processOneJob(cmdToRun,completionFolderPath):
    # global scheduler
    # first make sure that
    # global counter
    print('\tproccessOneJob EVENT:', time.time(), cmdToRun)
    pathForcompletionFile = os.path.join(completionFolderPath, completionFileName)
    print('\tprocessOneJob Writing RTRA Log file at :' + pathForcompletionFile)

    with open(pathForcompletionFile,"w") as logfile:
        subprocess.call(RTRA_exe + cmdToRun, shell=True, stdout=logfile)  # , stdout=subprocess.PIPE, stderr=subprocess.STDOUT, preexec_fn=os.setsid
    # while True:
    #     time.sleep(1)  # delays for 5 seconds
    #     break
    print('\tprocessOneJob: Finished: ' + cmdToRun)

    f = open("demofile.txt", "a")
    f.write("Now the file has one more line!")

    if len(scheduler.queue) ==0:
        scheduleIdleJob()  # this makes sure that the thread will be always busy


def scheduleOneJob(job,completionFolderPath):
    scheduler.enter(0.5, 1, processOneJob, (job,completionFolderPath))
    # if scheduler.queue[0].argument[0] == 'Idle':
    #     comeOutFromIdle()

def scheduleNewJobs(JobLists,completionFileFolderPathList):
    print('\tSchedule New Jobs')
    for iJob in range(0,len(JobLists)):
        scheduleOneJob(JobLists[iJob],completionFileFolderPathList[iJob])

    # print('Scheduler Thread is alive ?' + str(threadRunScheduler.is_alive())
    if (not threadRunScheduler.is_alive()):
        print('\tLaunching New Jobs via threadRunScheduler')
        threadRunScheduler.start()
    else: # now the thread is alive just check if the idle job is the first job in the queue if so comeout from idle job and
        comeOutFromIdle()
        print('\tAdded some more jobs to Queue')
    # print('Scheduler Thread is alive after start?' + str(threadRunScheduler.is_alive())
    # threadRunScheduler.join()


def listCreatedFoldersWithInASpecificTime(path, timeSec, NowInFloat):
    os.chdir(path)

    # all_subdirs = [d for d in os.listdir(".") if os.path.isdir(d)]
    folderList =[] # {}
    for file in os.listdir("."):
        if os.path.isdir(file):
            timestamp = os.path.getmtime(file)

            # get timestamp and directory name and store to dictionary
            if (NowInFloat- timestamp) < timeSec:
                folderList.append(os.path.join(path, file))
                #fileList[os.path.join(os.getcwd(), file)] = timestamp
    return folderList


def directory_changed(path):
    nowInFloat = time.time()
    print(f'\tDirectory Changed: {path} at {str(nowInFloat)}')
    time.sleep(0.5)
    # go five second back in time and get all the newly created folder here
    folderList = list(set(listCreatedFoldersWithInASpecificTime(str(path), 2e10, nowInFloat)))
    print('\tFolders changed in last 5 seconds ....')
    print(folderList)
    # grab the commands and put them in scheduler
    JobLists = []
    completionFileFolderPathList=[]

    for folderPath in folderList:
        cmdFile = [os.path.join(root, name)
                           for root, dirs, files in os.walk(folderPath)
                           for name in files
                           if (name.startswith('runCommand.txt'))]
        if len(cmdFile) > 0:
            # if log file exists in the folder ignore it and move on
            if os.path.isfile(os.path.join(folderPath,completionFileName)):
                continue

            with open(cmdFile[0], 'r') as f:
                cmd = f.readline()
            if any(cmd in s for s in scheduler.queue):
                continue
            else:
                JobLists.append(cmd)
                filePath, tail= path_head_tail(cmdFile[0])
                # print((cmdFile[0])
                completionFileFolderPathList.append(folderPath)
        else:
            print('\t\tWARNING: no runCommand.txt in ' + folderPath)


    if len(JobLists) != len(completionFileFolderPathList):
        raise ValueError('ERROR: len(JobLists) != len(completionFileFolderPathList) should not be the case!!!!!')
    print(f'\n{str(len(JobLists))} unscheduled jobs found : ')
    print(JobLists)
    # fs_watcher.addPaths()
    if len(JobLists) > 0:
        scheduleNewJobs(JobLists, completionFileFolderPathList)

def file_changed(path):
    print('\tFile Changed: %s' % path)

def main(app):   
    paths = [RTRAAllResultFolder]
    # read lines of the Que file
    fs_watcher = QtCore.QFileSystemWatcher(paths)
    fs_watcher.directoryChanged.connect(directory_changed)
    fs_watcher.fileChanged.connect(file_changed)

    print(f'\tIdle Mode observing changes in the result folder...{RTRAAllResultFolder}')
    app.exec_()
    return

def sigint_handler(*args):
    """Handler for the SIGINT signal."""
    QtCore.QCoreApplication.quit()


if __name__== "__main__":
    parser = argparse.ArgumentParser(description='RTRAListener')
    parser.add_argument("-start", help="Start listener if not already running", action="store_true")
    parser.add_argument('-stop', help='Stop listener if running', action="store_true")
    parser.add_argument('-restart', help='Restart the listener', action="store_true")
    parser.add_argument('-report', help='Report global variables', action="store_true")
    parser.add_argument('-status', help='Show status of RTRA', action="store_true")
    args = parser.parse_args()
    if args.report:
        report()
    if args.status:
        check_if_running()
        sys.exit(0)

    if args.start:
        start_rtra()
    elif args.stop:
        stop_rtra()
        exit()
    elif args.restart:
        restart_rtra()
    else:
        print('Exiting with no start, stop, restart action\n')
        parser.print_help(sys.stderr)
        exit()
    #
    if check_if_running():
        print('\tNot restarting, RTRA already running...')
        sys.exit(0)
    else:
        print('\tStarting RTRA')
        # Lock file in a sub-routine does not work because garbage collection
        # removes the lock
        if not os.path.isfile(lock_filename):
            os.chmod(lock_filename, 0o777)
        lock_fp = open(lock_filename, 'w')
        try:
            fcntl.lockf(lock_fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            print(f'\tLocking Failed!  Another RTRA service is running!')
            exit()

        scheduler = sched.scheduler(time.time, time.sleep)
        threadRunScheduler = threading.Thread(target=scheduler.run)
        #
        signal.signal(signal.SIGINT, sigint_handler)
        app = QtCore.QCoreApplication(sys.argv)
        timer = QtCore.QTimer()
        timer.start(500)  # You may change this if you wish.
        timer.timeout.connect(lambda: None)  # Let the interpreter run each 500 ms.
        # Your code here.
        main(app)
        app.quit()
