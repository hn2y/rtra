#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <algorithm>    // std::count_if
#include "../Utilities/gpusAndCpusInfo.h"



#ifndef calculateAllDiffAndCulmulativeDvhsForPinnacle_H
#define calculateAllDiffAndCulmulativeDvhsForPinnacle_H
typedef thrust::device_vector<float> dVec;
typedef dVec *pointerDVec;
bool IsTrue2(bool a)
{
    return a==true;
}


extern "C" void negateAll(float *vectorToNegate, int vecSize);
extern "C" void launchTransformPts(float *XqTransformed, float *YqTransformed, float *ZqTransformed, float *Xqn, float *Yqn, float *Zqn, int np, float *R_All, float *T_All, int numTrans, int devNum);
extern "C" void dvhCalcGPU(float *dvhs, float *doseBins, const int EachBinLength, const float *d_dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum);
extern "C" void dvhCalcWithPartialVolumeGPU(float *dvhs, float *doseBins, const int EachBinLength, const float *d_dose_in,const float *d_dosePartialVolume_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum);
extern "C" void sumUpFractionDosesOnGPU(float *d_doseOut, const float *d_doseIn, const int fractionsNum, const float *fractionsDoseWeights, const int numDoseSets,  const int NumoFDosePtsInEachDoseSet, const int devNum);
// interp3 perfroms triliniear interpolation in GPU,   the gridpoint coordinates must be monotonically increasing
// gridX, gridY and gridZ are device raw pointers to gridpoint coordinates
extern "C"  void interp3(
        float * vOutput,
        const int     nQueryPoints,
        const int     xSize,
        const int     ySize,
        const int     zSize,
        const float * gridX,
        const float * gridY,
        const float * gridZ,
        const float * vGrid,
        const float * xq,
        const float * yq,
        const float * zq);
#endif // calculateAllDiffAndCulmulativeDvhsForPinnacle_H
//
void allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPointsFloat(std::vector<pointerDVec>& d_inPolygonPointsXforROIiPointerVec,
                                                                             std::vector<pointerDVec> &d_inPolygonPointsYforROIiPointerVec,
                                                                             std::vector<pointerDVec> &d_inPolygonPointsZforROIiPointerVec,
                                                                             std::vector<float*>& d_inPolygonPointsXforROIiRawPointerVec,
                                                                             std::vector<float*>& d_inPolygonPointsYforROIiRawPointerVec,
                                                                             std::vector<float*>& d_inPolygonPointsZforROIiRawPointerVec,
                                                                             std::vector<pointerDVec>& d_inPolygonPointsPartialVolumeforROIiPointerVec,
                                                                             std::vector<float*>& d_inPolygonPointsPartialVolumeforROIiRawPointerVec,
                                                                             std::vector<pointerDVec>& d_doseGridXvaluesPointerVec,
                                                                             std::vector<pointerDVec>& d_doseGridYvaluesPointerVec,
                                                                             std::vector<pointerDVec>& d_doseGridZvaluesPointerVec,
                                                                             std::vector<float*> &d_doseGridXvaluesRawPointerVec,
                                                                             std::vector<float*>& d_doseGridYvaluesRawPointerVec,
                                                                             std::vector<float*> &d_doseGridZvaluesRawPointerVec,
                                                                             std::vector<pointerDVec>& d_doseArraryPointerVec,
                                                                             std::vector<float*> &d_doseArrayRawPointerVec,
                                                                             std::vector<pointerDVec> &d_rotationMatricesPointerVec,
                                                                             std::vector<float*>& d_rotationMatricesRawPointerVec,
                                                                             std::vector<pointerDVec> &d_translationVectorsPointerVec,
                                                                             std::vector<float*> &d_translationVectorsRawPointerVec,
                                                                             std::vector<float> doseArray,
                                                                             std::vector<float> doseGridXvalues,
                                                                             std::vector<float> doseGridYvalues,
                                                                             std::vector<float> doseGridZvalues,
                                                                             std::vector<float> inPolygonPointsX,
                                                                             std::vector<float> inPolygonPointsY,
                                                                             std::vector<float> inPolygonPointsZ,
                                                                             std::vector<float> inPolygonPointsPartialVolume,
                                                                             std::vector<float> RotationMatrices,
                                                                             std::vector<float> TranslationVectors,
                                                                             gpusAndCpusInfo devInfo,
                                                                             const bool verbose)
{
    //    bool verbose = false;
    int cntQualifiedDevices=-1;
    for (int idev = 0; idev < devInfo.numGPUs; idev++)
    {
        if (devInfo.IsDeviceSelectedForComputation[idev])
        {
            cntQualifiedDevices ++;
            cudaSetDevice(idev);
            //            if (iRoi==0) // TODO : Notice we don't need to delete dose information from devices we can do this when iRoi==numberRoi
            //            {
            d_doseArraryPointerVec[cntQualifiedDevices] = new dVec(doseArray.begin(), doseArray.end());
            if (verbose)
            {
                std::cout<< "d_doseArraryPointerVec is allocated for device :" << idev  << std::endl;
            }

            d_doseGridXvaluesPointerVec[cntQualifiedDevices] = new dVec(doseGridXvalues.begin(), doseGridXvalues.end());
            if (verbose)
            {
                std::cout<< "d_doseGridXvaluesPointerVec is allocated for device :" << idev  << std::endl;
            }
            d_doseGridYvaluesPointerVec[cntQualifiedDevices] = new dVec(doseGridYvalues.begin(), doseGridYvalues.end());
            if (verbose)
            {
                std::cout<< "d_doseGridYvaluesPointerVec is allocated for device :" << idev  << std::endl;
            }
            d_doseGridZvaluesPointerVec[cntQualifiedDevices] = new dVec(doseGridZvalues.begin(), doseGridZvalues.end());
            if (verbose)
            {
                std::cout<< "d_doseGridZvaluesPointerVec is allocated for device :" << idev  << std::endl;
            }

            // we need to creat the following resources for each ROI and delete them before doing calculation on the next ROI
            d_inPolygonPointsXforROIiPointerVec[cntQualifiedDevices] = new dVec(inPolygonPointsX.begin(), inPolygonPointsX.end());
            d_inPolygonPointsYforROIiPointerVec[cntQualifiedDevices] = new dVec(inPolygonPointsY.begin(), inPolygonPointsY.end());
            d_inPolygonPointsZforROIiPointerVec[cntQualifiedDevices] = new dVec(inPolygonPointsZ.begin(), inPolygonPointsZ.end());
            d_inPolygonPointsPartialVolumeforROIiPointerVec[cntQualifiedDevices] = new dVec(inPolygonPointsPartialVolume.begin(), inPolygonPointsPartialVolume.end());
            d_translationVectorsPointerVec[cntQualifiedDevices] = new dVec(TranslationVectors.begin(), TranslationVectors.end());
            d_rotationMatricesPointerVec[cntQualifiedDevices] = new dVec(RotationMatrices.begin(), RotationMatrices.end());
            // float* d_doseArrayRawPointer = thrust::raw_pointer_cast(&d_doseArrary[0]);
            d_doseArrayRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseArraryPointerVec[cntQualifiedDevices]->data());

            d_doseGridXvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridXvaluesPointerVec[cntQualifiedDevices]->data());
            d_doseGridYvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data());
            d_doseGridZvaluesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsXforROIiRawPointerVec[cntQualifiedDevices]= thrust::raw_pointer_cast(d_inPolygonPointsXforROIiPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsYforROIiRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_inPolygonPointsYforROIiPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsZforROIiRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_inPolygonPointsZforROIiPointerVec[cntQualifiedDevices]->data());
            d_inPolygonPointsPartialVolumeforROIiRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_inPolygonPointsPartialVolumeforROIiPointerVec[cntQualifiedDevices]->data());
            d_rotationMatricesRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_rotationMatricesPointerVec[cntQualifiedDevices]->data());
            d_translationVectorsRawPointerVec[cntQualifiedDevices] = thrust::raw_pointer_cast(d_translationVectorsPointerVec[cntQualifiedDevices]->data());





            if ((doseGridXvalues[doseGridXvalues.size() - 1] -doseGridXvalues[0]) < 0)
            {
                //thrust::transform(d_doseGridXvalues.begin(), d_doseGridXvalues.end(), d_doseGridXvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsXforROIi.begin(), d_inPolygonPointsXforROIi.end(), d_inPolygonPointsXforROIi.begin(), thrust::negate<float>());
                // https://groups.google.com/forum/#!topic/thrust-users/1ttLbwE3H5Q
                if (verbose)
                {
                    std::cout << "negating dose grid X values" << std::endl;
                }
                thrust::transform(d_doseGridXvaluesPointerVec[cntQualifiedDevices]->begin(), d_doseGridXvaluesPointerVec[cntQualifiedDevices]->end(), d_doseGridXvaluesPointerVec[cntQualifiedDevices]->begin(),thrust::negate<float>());
                if (verbose)
                {
                    std::cout << "negating dose grid X values done" << std::endl;
                }
                //negateAll(d_doseGridXvaluesRawPointerVec[cntQualifiedDevices], d_doseGridXvaluesPointerVec[cntQualifiedDevices]->size());



                //negateAll(d_inPolygonPointsXforROIiRawPointer, d_inPolygonPointsXforROIi.size());


            }

            if ((doseGridYvalues[doseGridYvalues.size() - 1] - doseGridYvalues[0]) < 0)
            {
                //thrust::transform(d_doseGridYvalues.begin(), d_doseGridYvalues.end(), d_doseGridYvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsYforROIi.begin(), d_inPolygonPointsYforROIi.end(), d_inPolygonPointsYforROIi.begin(), thrust::negate<float>());
                //std::cout << "\nbefor negation:" << std::endl;
                //   logstream << class_member <<  "\n before negation applied d_doseGridYvaluesPointerVec values of 200 pts :  " << std::endl;

                // thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));

                if (verbose)
                {
                    std::cout << "negating dose grid Y values" << std::endl;
                }
                thrust::transform(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->begin(), d_doseGridYvaluesPointerVec[cntQualifiedDevices]->end(), d_doseGridYvaluesPointerVec[cntQualifiedDevices]->begin(),thrust::negate<float>());
                if (verbose)
                {
                    std::cout << "negating dose grid Y values done" << std::endl;
                }
                // negateAll(d_doseGridYvaluesRawPointerVec[cntQualifiedDevices],  d_doseGridYvaluesPointerVec[cntQualifiedDevices]->size());



                //negateAll(d_inPolygonPointsYforROIiRawPointer, d_inPolygonPointsYforROIi.size());
                //std::cout << "\nAfter negation:" << std::endl;
                // thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(), 10, std::ostream_iterator<float>(std::cout, ","));

                //  logstream << class_member <<  "\n after negation applied d_doseGridYvaluesPointerVec values of 200 pts :  " << std::endl;

                //   thrust::copy_n(d_doseGridYvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));




            }
            if ((doseGridZvalues[doseGridZvalues.size() - 1] - doseGridZvalues[0]) < 0)
            {

                //logstream << class_member <<  "\n before negation applied d_doseGridZvaluesPointerVec values of 200 pts :  " << std::endl;

                //thrust::copy_n(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));

                //thrust::transform(d_doseGridZvalues.begin(), d_doseGridZvalues.end(), d_doseGridZvalues.begin(), thrust::negate<float>());
                //thrust::transform(d_inPolygonPointsZforROIi.begin(), d_inPolygonPointsZforROIi.end(), d_inPolygonPointsZforROIi.begin(), thrust::negate<float>());



                if (verbose)
                {
                    std::cout << "negating dose grid Z values" << std::endl;
                }
                thrust::transform(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->begin(), d_doseGridZvaluesPointerVec[cntQualifiedDevices]->end(), d_doseGridZvaluesPointerVec[cntQualifiedDevices]->begin(),thrust::negate<float>());
                if (verbose){
                    std::cout << "negating dose grid Z values done" << std::endl;
                }
                // negateAll(d_doseGridZvaluesRawPointerVec[cntQualifiedDevices],  d_doseGridZvaluesPointerVec[cntQualifiedDevices]->size());
                //negateAll(d_inPolygonPointsZforROIiRawPointer, d_inPolygonPointsZforROIi.size());
                // logstream << class_member <<  "\n after negation applied d_doseGridZvaluesPointerVec values of 200 pts :  " << std::endl;
                //   thrust::copy_n(d_doseGridZvaluesPointerVec[cntQualifiedDevices]->data(),10,std::ostream_iterator<float>(std::cout, ","));


            }
        }// (IsDeviceSelectedForComputation[idev])
    }
}


//extern "C" cudaError_t calculateAllDiffAndCulmulativeDvhs(float* cDVHForTrial,
//                                                          float* diffDVHForTrial,
//                                                          float* voxelDose95thP,
//                                                          float* voxelDose5thP,
//                                                          float* voxelDoseMedian,
//                                                          float* voxelDoseMean,
//                                                          float* voxelDoseVariance,
//                                                          const float* fractionsDoseWeights,
//                                                          const float* doseArray,
//                                                          const float* doseGridX,
//                                                          const float* doseGridY,
//                                                          const float* doseGridZ,
//                                                          const float* PointsCloudX,
//                                                          const float* PointsCloudY,
//                                                          const float* PointsCloudZ,
//                                                          const float* RotationMatrices,
//                                                          const float* TranlationVectors,
//                                                          const int numFractions,
//                                                          const int numDoseBins,
//                                                          const int doseGridXSize,
//                                                          const int doseGridYSize,
//                                                          const int doseGridZSize,
//                                                          const int numPoints,
//                                                          const int numTransformations)
extern "C" cudaError_t calculateAllDiffAndCulmulativeDvhsForPinnacle(std::vector<float> &cDVHForTrial,
                                                                     std::vector<float> &diffDVHForTrial,
                                                                     std::vector<float> &voxelDoseMeanAllTrials,
                                                                     std::vector<float> &voxelDoseVarianceAllTrials,
                                                                     std::vector<float> fractionsDoseWeights,
                                                                     std::vector<float> doseArray,
                                                                     std::vector<float> doseGridX,
                                                                     std::vector<float> doseGridY,
                                                                     std::vector<float> doseGridZ,
                                                                     std::vector<float> PointsCloudX,
                                                                     std::vector<float> PointsCloudY,
                                                                     std::vector<float> PointsCloudZ,
                                                                     std::vector<float> inPolygonPointsPartialVolume,
                                                                     std::vector<float> RotationMatrices,
                                                                     std::vector<float> TranlationVectors,
                                                                     const int numDoseBins,
                                                                     const float doseBinWidth,
                                                                     const bool isVerbose)

{
    std::string class_member = "calculateAllDiffAndCulmulativeDvhs.cu -->";

    if (isVerbose)
    {
        std::cout << class_member<< "Calculating memory requirement for ROI On GPU (float each number 4 bytes)" << std::endl;
    }
    int numFractions= fractionsDoseWeights.size();
    int numTransformations= TranlationVectors.size()/3;
    size_t numNominalQueryPts = PointsCloudX.size();
    size_t numTrials = numTransformations/numFractions;
    size_t memoryForNominalQueryinBytes = numNominalQueryPts * 4 * 4; // each points needs four (coordinate(3) + partial volume(1))
    double memoryForNominalQueryPtsinMB = static_cast<double>(memoryForNominalQueryinBytes) / 1024.0 / 1024.0;
    size_t  memoryRequiredForDoseMatrixinMB = doseArray.size() * 4 * 4 / 1024 / 1024;  // 4 bytes times 4 number for each pt (x,v,z,v) note x,y,z are in
    size_t memoryRequiredForBinedDoseAfterTransformation = numDoseBins*numTrials*numFractions * 4 / 1024 / 1024;  //  Make a vector with this size at Host side  then transfer all device date to this host vector

    size_t minmunMemRequiredwith = 20 * memoryForNominalQueryPtsinMB * 1024 * 1024 +
            memoryRequiredForDoseMatrixinMB * 1024 * 1024;  // to be safe and probably be benificial in terms of performance, we put a minimum required memory for GPU , here is 20*memoryrequired for query pts + Memory required for dose coordinates and its values (x,y,z,v)
    gpusAndCpusInfo deviceInfo = gpusAndCpusInfo();
    deviceInfo.getNumCpuAndGpus();
    if (deviceInfo.numGPUs>1)
    {
        deviceInfo.numGPUs=1; //TODO: Fix the multigpu case
    }
    deviceInfo.getAvilableMemoryOnDevices();

    if (isVerbose)
    {

        std::cout << class_member<<  "minmunMemRequiredwith = " << minmunMemRequiredwith << " Bytes"<< std::endl;
    }
    // Select the Devices for computation which have avialable memory more than minmunMemRequiredwith
    std::vector<bool> temp2(deviceInfo.numGPUs,false);
    deviceInfo.IsDeviceSelectedForComputation= temp2; //=   TODO: to be changed in future
    for (int idev = 0; idev < deviceInfo.numGPUs; ++idev)
    {
        if (deviceInfo.freeMemOnEachDevice[idev]>minmunMemRequiredwith)
        {
            deviceInfo.IsDeviceSelectedForComputation[idev] = true;
            if (isVerbose)
            {
                std::cout << class_member <<  "device " << idev << " is selected for computation." << std::endl;
            }
        }
    }
    // TODO: consider gpu to gpu memory transfer for other devices http://stackoverflow.com/questions/628041/how-to-copy-memory-between-different-gpus-in-cuda
    deviceInfo.numQualifiedDevices= std::count_if (deviceInfo.IsDeviceSelectedForComputation.begin(), deviceInfo.IsDeviceSelectedForComputation.end(), IsTrue2);
    deviceInfo.transformationJobDistributionVec.clear();
    deviceInfo.indicesOfQualifiedDevices.clear();
    for (unsigned int iDev = 0; iDev < deviceInfo.IsDeviceSelectedForComputation.size(); ++iDev)
    {
        if (deviceInfo.IsDeviceSelectedForComputation[iDev])
        {
            deviceInfo.indicesOfQualifiedDevices.push_back(iDev);
            deviceInfo.transformationJobDistributionVec.push_back(std::vector<unsigned int>(0));
        }
    }

    // memory allocation for dose matrix and its coordinates on selected GPU for computation
    std::vector<pointerDVec> d_doseArraryPointerVec(deviceInfo.numQualifiedDevices); // every single pointer might point to resources on a different device
    std::vector<float*> d_doseArrayRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_doseGridXvaluesPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_doseGridXvaluesRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_doseGridYvaluesPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_doseGridYvaluesRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_doseGridZvaluesPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_doseGridZvaluesRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_inPolygonPointsXforROIiPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_inPolygonPointsXforROIiRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_inPolygonPointsYforROIiPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_inPolygonPointsYforROIiRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_inPolygonPointsZforROIiPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_inPolygonPointsZforROIiRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_inPolygonPointsPartialVolumeforROIiPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*>      d_inPolygonPointsPartialVolumeforROIiRawPointerVec(deviceInfo.numQualifiedDevices);


    std::vector<pointerDVec> d_translationVectorsPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*>  d_translationVectorsRawPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<pointerDVec> d_rotationMatricesPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*>  d_rotationMatricesRawPointerVec(deviceInfo.numQualifiedDevices);

    std::vector<pointerDVec> d_transformedPtsXVec(deviceInfo.numQualifiedDevices);
    std::vector<pointerDVec> d_transformedPtsYVec(deviceInfo.numQualifiedDevices);
    std::vector<pointerDVec> d_transformedPtsZVec(deviceInfo.numQualifiedDevices);
    std::vector<pointerDVec> d_DoseValueTransformedPtsVec(deviceInfo.numQualifiedDevices);
    std::vector<pointerDVec> d_DoseValueForTransformedPtsFractionsCombinedVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_transformedPtsXRawPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_transformedPtsYRawPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_transformedPtsZRawPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_DoseValueForTransformedPtsRawPointerVec(deviceInfo.numQualifiedDevices);
    std::vector<float*> d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec(deviceInfo.numQualifiedDevices);

    if (isVerbose)
    {
        std::cout << class_member<<  "pointer for device vectors has been set" << std::endl;
    }
    // memory allocation for dose matrix and its coordinates on selected GPU for computation
    allocateDeviceMemAndTransferDoseArrayDoseCoordinatesAndQueryPointsFloat(d_inPolygonPointsXforROIiPointerVec,
                                                                            d_inPolygonPointsYforROIiPointerVec,
                                                                            d_inPolygonPointsZforROIiPointerVec,
                                                                            d_inPolygonPointsXforROIiRawPointerVec,
                                                                            d_inPolygonPointsYforROIiRawPointerVec,
                                                                            d_inPolygonPointsZforROIiRawPointerVec,
                                                                            d_inPolygonPointsPartialVolumeforROIiPointerVec,
                                                                            d_inPolygonPointsPartialVolumeforROIiRawPointerVec,
                                                                            d_doseGridXvaluesPointerVec,
                                                                            d_doseGridYvaluesPointerVec,
                                                                            d_doseGridZvaluesPointerVec,
                                                                            d_doseGridXvaluesRawPointerVec,
                                                                            d_doseGridYvaluesRawPointerVec,
                                                                            d_doseGridZvaluesRawPointerVec,
                                                                            d_doseArraryPointerVec,
                                                                            d_doseArrayRawPointerVec,
                                                                            d_rotationMatricesPointerVec,
                                                                            d_rotationMatricesRawPointerVec,
                                                                            d_translationVectorsPointerVec,
                                                                            d_translationVectorsRawPointerVec,
                                                                            doseArray,
                                                                            doseGridX,
                                                                            doseGridY,
                                                                            doseGridZ,
                                                                            PointsCloudX,
                                                                            PointsCloudY,
                                                                            PointsCloudZ,
                                                                            inPolygonPointsPartialVolume,
                                                                            RotationMatrices,
                                                                            TranlationVectors,
                                                                            deviceInfo,
                                                                            isVerbose);

    if (isVerbose)
    {
        std::cout << class_member<<  "Memory for device vectors has been allocated" << std::endl;
    }

    // Check available memory after allocation the memory for inPolygonPoint, dose array, and dose coordinates
    if (!deviceInfo.getAvilableMemoryOnDevices())
    {
        return cudaGetLastError(); // TODO: change it
    }

    deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch.clear();

    for (int iQualDev = 0; iQualDev < deviceInfo.indicesOfQualifiedDevices.size(); ++iQualDev)
    {
        int tempNumTrans = floor(static_cast<double>(deviceInfo.freeMemOnEachDevice[deviceInfo.indicesOfQualifiedDevices[iQualDev]]) / (static_cast<double>(memoryForNominalQueryPtsinMB) * 1024 * 1024 * 4.0 / 3.0)* 0.68);// 0.98 should be adjusted to have a free space on each device to make sure that transformation is done properly.
        int tempNumTransDividedByNumFrac= tempNumTrans/numFractions;
        int numTrans_MultipleOfNumOfFractionForEachTrial=tempNumTransDividedByNumFrac*numFractions;
        deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch.push_back(numTrans_MultipleOfNumOfFractionForEachTrial);
        if (isVerbose)
        {
            std::cout <<"deviceInfo.freeMemOnEachDevice[deviceInfo.indicesOfQualifiedDevices[iQualDev]]= " << deviceInfo.freeMemOnEachDevice[deviceInfo.indicesOfQualifiedDevices[iQualDev]] <<std::endl;
            std::cout << "tempNumTrans = " << tempNumTrans << std::endl;
            std::cout << "memoryForNominalQueryPtsinMB = "<< memoryForNominalQueryPtsinMB<< std::endl;


            std::cout << "tempNumTransDividedByNumFrac = " << tempNumTransDividedByNumFrac<< std::endl;


            std::cout << "numTrans_MultipleOfNumOfFractionForEachTrial = " << numTrans_MultipleOfNumOfFractionForEachTrial<< std::endl;

            std::cout << "deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch["<< iQualDev <<"] = " << deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch[iQualDev] << std::endl;
        }
    }

    // Schedule transformation jobs on the available qualifiled Devices
    if (isVerbose)
    {

        std::cout << class_member << "Scheduling tranformation jobs on the available qualified devices for ROI  " << std::endl;

        std::cout << class_member << "numFractions = " << numFractions<<std::endl;
        std::cout << class_member << "numTrials = " << numTrials<<std::endl;
        std::cout << class_member << "PointsCloudX.size() = " << PointsCloudX.size()<<std::endl;
    }
    size_t currentScheduledTranJob=0;
    while (currentScheduledTranJob<numFractions*numTrials-1)
    {

        for (int iQualDev = 0; iQualDev < deviceInfo.indicesOfQualifiedDevices.size(); ++iQualDev)
        {
            if (deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]>numFractions*numTrials-1 -currentScheduledTranJob)
            {
                deviceInfo.transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob); // starting transformation index for   iQualDev
                deviceInfo.transformationJobDistributionVec[iQualDev].push_back(numFractions*numTrials-1); // end transformation index for  iQualDev
                currentScheduledTranJob= numFractions*numTrials;
                break;
            }
            else
            {

                deviceInfo.transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob); // starting transformation index for   iQualDev
                deviceInfo.transformationJobDistributionVec[iQualDev].push_back(currentScheduledTranJob + deviceInfo.numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]-1); // end transformation index for  iQualDev
                currentScheduledTranJob= currentScheduledTranJob +deviceInfo. numberOfPossibleTransfomationInEachKernelLaunch[iQualDev]; // Should point to the next undone index
            }
        } // iQualDev
    } // while ends here
    if (isVerbose)
    {

        std::cout << class_member << "tranformation jobs are scheduled " << std::endl;
    }
    cudaError_t cudaStatus;

    //    // perform transformation jobs
    //    if (deviceInfo. transformationJobDistributionVec.size()==1)
    //    {
    //        std::cout << class_member << "Only one GPU has been detected with CUDA computational capability " << std::endl;

    //        //http://stackoverflow.com/questions/21616395/multi-gpu-cuda-thrust
    //        for (int iBatchLaunch = 0; iBatchLaunch < deviceInfo. transformationJobDistributionVec[0].size()/2; ++iBatchLaunch)
    //        {
    //            unsigned int iQualDev=0;
    //            cudaStatus= cudaSetDevice(deviceInfo. indicesOfQualifiedDevices[iQualDev]);

    //            if (cudaStatus != cudaSuccess)
    //            {
    //                std::cout <<class_member << "cudaSetDevice failed! or " << deviceInfo. indicesOfQualifiedDevices[iQualDev] <<  "  Do you have a CUDA-capable GPU installed?" << std::endl;
    //                //  return false;
    //            }

    //            size_t startIndexOfTransformationsToLaunch= deviceInfo. transformationJobDistributionVec[iQualDev][iBatchLaunch*2];
    //            size_t stopIndexOfTransformationsToLaunch= deviceInfo. transformationJobDistributionVec[iQualDev][iBatchLaunch*2+1];
    //            // create the resultant tranformation vectors  on device along with its device_ptr
    //            size_t numTransToLaunch=stopIndexOfTransformationsToLaunch-startIndexOfTransformationsToLaunch+1;
    //            if (numTransToLaunch%numFractions!=0)
    //            {
    //                std::cout << "ERROR: number of Transformations is expected to be multiple of number of Fractions" << std::endl;
    //                // return false;
    //            }
    //            const size_t numElem = numTransToLaunch*PointsCloudX.size();
    //            const size_t numOfTrialForThisLaunch  = numTransToLaunch/numFractions;
    //            const size_t numOfElemAfterSummingUpFractionDosesForThisLaunch  = numOfTrialForThisLaunch*PointsCloudX.size();


    //        }

    //    }


    //    else

    //    {
    //        omp_set_num_threads(deviceInfo. transformationJobDistributionVec.size());  // transformationJobDistributionVec.size() gives us number of qualified gpu devices




    //    }

    omp_set_num_threads(deviceInfo. transformationJobDistributionVec.size());  // transformationJobDistributionVec.size() gives us number of qualified gpu devices


    //http://stackoverflow.com/questions/21616395/multi-gpu-cuda-thrust
    for (int iBatchLaunch = 0; iBatchLaunch < deviceInfo. transformationJobDistributionVec[0].size()/2; ++iBatchLaunch)
    {

#pragma omp parallel
        {
            unsigned int iQualDev = omp_get_thread_num();
            if (deviceInfo. transformationJobDistributionVec[iQualDev].size()>iBatchLaunch*2)
            {
                cudaStatus= cudaSetDevice(deviceInfo. indicesOfQualifiedDevices[iQualDev]);
                if (cudaStatus != cudaSuccess) {
                    std::cout <<class_member << "cudaSetDevice failed! or " << deviceInfo. indicesOfQualifiedDevices[iQualDev] <<  "  Do you have a CUDA-capable GPU installed?" << std::endl;
                    //  return false;
                }
                //  numberOfPossibleTransfomationInEachKernelLaunch is a multiple of numFractions so startIndexOfTransformationsToLaunch is also a multiple of numFractions
                size_t startIndexOfTransformationsToLaunch= deviceInfo. transformationJobDistributionVec[iQualDev][iBatchLaunch*2];
                size_t stopIndexOfTransformationsToLaunch= deviceInfo. transformationJobDistributionVec[iQualDev][iBatchLaunch*2+1];


                // create the resultant tranformation vectors  on device along with its device_ptr
                size_t numTransToLaunch=stopIndexOfTransformationsToLaunch-startIndexOfTransformationsToLaunch+1;
                if (numTransToLaunch%numFractions!=0)
                {
                    std::cout << "ERROR: number of Transformations is expected to be multiple of number of Fractions" << std::endl;
                    // return false;
                }
                size_t numElem = numTransToLaunch*PointsCloudX.size();
                size_t numOfTrialForThisLaunch  = numTransToLaunch/numFractions;
                size_t numOfElemAfterSummingUpFractionDosesForThisLaunch  = numOfTrialForThisLaunch*PointsCloudX.size();
                //  const size_t numElemForThisLaunch= numOfTrialForThisLaunch*PointsCloudX.size();
                // thrust::host_vector<float> tempArray1(numElem); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu
                //           //     //thrust::device_ptr<float> dp = thrust::device_malloc<float>(numElem);
                //            //    //thrust::device_vector<float> v3(dp, dp + numElem);
                //thrust::host_vector<float> tempArray2(numOfElemAfterSummingUpFractionDosesForThisLaunch); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu
                //  std::cout << "Host vectors have been allocated "<< std::endl;
                if (isVerbose)
                {

                    std::cout << "numElem = " << numElem<< std::endl;
                    std::cout << "numTransToLaunch = " << numTransToLaunch << std::endl;
                }

                // thrust::copy_n(d_transformedPtsXVec[iQualDev]->data(),10,std::ostream_iterator<float>(std::cout, ","));
                d_transformedPtsXVec[iQualDev] = new dVec(numElem); // new dVec(tempArray1);
                if (isVerbose)
                {
                    std::cout << "d_transformedPtsXVec have been allocated for device: "<< deviceInfo. indicesOfQualifiedDevices[iQualDev] << std::endl;
                }
                d_transformedPtsYVec[iQualDev] = new dVec(numElem);
                if (isVerbose)
                {
                    std::cout << "d_transformedPtsYVec have been allocated for device: "<< deviceInfo. indicesOfQualifiedDevices[iQualDev] << std::endl;
                }
                d_transformedPtsZVec[iQualDev] = new dVec(numElem);
                if (isVerbose)
                {

                    std::cout << "d_transformedPtsZVec have been allocated for device: "<< deviceInfo. indicesOfQualifiedDevices[iQualDev] << std::endl;
                }
                d_DoseValueTransformedPtsVec[iQualDev] = new dVec(numElem);
                d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev] = new dVec(numOfElemAfterSummingUpFractionDosesForThisLaunch);

                d_transformedPtsXRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsXVec[iQualDev]->data());
                d_transformedPtsYRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsYVec[iQualDev]->data());
                d_transformedPtsZRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_transformedPtsZVec[iQualDev]->data());
                d_DoseValueForTransformedPtsRawPointerVec[iQualDev]= thrust::raw_pointer_cast(d_DoseValueTransformedPtsVec[iQualDev]->data());
                d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev] = thrust::raw_pointer_cast(d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev]->data());
                if (isVerbose)
                {

                    std::cout << class_member << " Applying overall transformation (rotations and translations) from #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch  << " on Device " << deviceInfo.indicesOfQualifiedDevices[iQualDev]<< std::endl;
                }
                launchTransformPts(d_transformedPtsXRawPointerVec[iQualDev],
                                   d_transformedPtsYRawPointerVec[iQualDev],
                                   d_transformedPtsZRawPointerVec[iQualDev],
                                   d_inPolygonPointsXforROIiRawPointerVec[iQualDev],
                                   d_inPolygonPointsYforROIiRawPointerVec[iQualDev],
                                   d_inPolygonPointsZforROIiRawPointerVec[iQualDev],
                                   d_inPolygonPointsXforROIiPointerVec[iQualDev]->size(),
                                   d_rotationMatricesRawPointerVec[iQualDev]+startIndexOfTransformationsToLaunch * 9,
                                   d_translationVectorsRawPointerVec[iQualDev]+startIndexOfTransformationsToLaunch * 3,
                                   numTransToLaunch,
                                   deviceInfo.indicesOfQualifiedDevices[iQualDev]);


                // Make sure that grid values are monotonically increasing
                if ((doseGridX[doseGridX.size() - 1] - doseGridX[0]) < 0)
                {
                    //thrust::transform(d_doseGridXvalues.begin(), d_doseGridXvalues.end(), d_doseGridXvalues.begin(), thrust::negate<float>());
                    //thrust::transform(d_inPolygonPointsXforROIi.begin(), d_inPolygonPointsXforROIi.end(), d_inPolygonPointsXforROIi.begin(), thrust::negate<float>());
                    // https://groups.google.com/forum/#!topic/thrust-users/1ttLbwE3H5Q

                    // thrust::transform(d_transformedPtsXVec[iQualDev].begin(),d_transformedPtsXVec

                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsXVec values..." << std::endl;
                    }
                    thrust::transform(d_transformedPtsXVec[iQualDev]->begin(), d_transformedPtsXVec[iQualDev]->end(), d_transformedPtsXVec[iQualDev]->begin(),thrust::negate<float>());
                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsXVec done" << std::endl;
}
                    //negateAll(d_transformedPtsXRawPointerVec[iQualDev], d_transformedPtsXVec[iQualDev]->size());



                    //negateAll(d_transformedPtsXRawPointerVec[iQualDev], numElem);

                    //negateAll(d_inPolygonPointsXforROIiRawPointer, d_inPolygonPointsXforROIi.size());


                }
                if ((doseGridY[doseGridY.size() - 1] - doseGridY[0]) < 0)
                {
                    //thrust::transform(d_doseGridYvalues.begin(), d_doseGridYvalues.end(), d_doseGridYvalues.begin(), thrust::negate<float>());
                    //thrust::transform(d_inPolygonPointsYforROIi.begin(), d_inPolygonPointsYforROIi.end(), d_inPolygonPointsYforROIi.begin(), thrust::negate<float>());
                    // cout << "d_transformedPtsY[0] before negation " << d_transformedPtsY[0] << endl;
                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsYVec values..." << std::endl;
                    }
                    thrust::transform(d_transformedPtsYVec[iQualDev]->begin(), d_transformedPtsYVec[iQualDev]->end(), d_transformedPtsYVec[iQualDev]->begin(),thrust::negate<float>());
                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsYVec values done" << std::endl;
}
                    //negateAll(d_transformedPtsYRawPointerVec[iQualDev], d_transformedPtsYVec[iQualDev]->size());

                    //negateAll(d_transformedPtsYRawPointerVec[iQualDev], numElem);

                    // cout << "d_transformedPtsY[0] after negation " << d_transformedPtsY[0] << endl;
                    //negateAll(d_inPolygonPointsYforROIiRawPointer, d_inPolygonPointsYforROIi.size());

                }
                if ((doseGridZ[doseGridZ.size() - 1] - doseGridZ[0]) < 0)
                {
                    //thrust::transform(d_doseGridZvalues.begin(), d_doseGridZvalues.end(), d_doseGridZvalues.begin(), thrust::negate<float>());
                    //thrust::transform(d_inPolygonPointsZforROIi.begin(), d_inPolygonPointsZforROIi.end(), d_inPolygonPointsZforROIi.begin(), thrust::negate<float>());
                    //cout << "d_transformedPtsZ[0] before negation " << d_transformedPtsZ[0] << endl;
                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsZVec values..." << std::endl;
                    }
                    thrust::transform(d_transformedPtsZVec[iQualDev]->begin(), d_transformedPtsZVec[iQualDev]->end(), d_transformedPtsZVec[iQualDev]->begin(),thrust::negate<float>());
                    if (isVerbose)
                    {

                    std::cout << "negating d_transformedPtsZVec values done" << std::endl;
}

                    //negateAll(d_transformedPtsZRawPointerVec[iQualDev], d_transformedPtsZVec[iQualDev]->size());
                    //negateAll(d_transformedPtsZRawPointerVec[iQualDev], numElem);

                    //cout << "d_transformedPtsZ[0] after negation " << d_transformedPtsZ[0] << endl;
                    //negateAll(d_inPolygonPointsZforROIiRawPointer, d_inPolygonPointsZforROIi.size());

                }

                if (isVerbose)
                {

                std::cout << class_member << "Calculating dose at tranformed point clouds from transformation #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch << " for ROI:  on Device " << deviceInfo.indicesOfQualifiedDevices[iQualDev]<< std::endl;
}
                // Notice since in pinnacle data is stored in column major format in order to have correct answer we need to swap y and x
                // in interp3 function
                interp3(d_DoseValueForTransformedPtsRawPointerVec[iQualDev],
                        numElem,
                        d_doseGridYvaluesPointerVec[iQualDev]->size(),
                        d_doseGridXvaluesPointerVec[iQualDev]->size(),
                        d_doseGridZvaluesPointerVec[iQualDev]->size(),
                        d_doseGridYvaluesRawPointerVec[iQualDev],
                        d_doseGridXvaluesRawPointerVec[iQualDev],
                        d_doseGridZvaluesRawPointerVec[iQualDev],
                        d_doseArrayRawPointerVec[iQualDev],
                        d_transformedPtsYRawPointerVec[iQualDev],
                        d_transformedPtsXRawPointerVec[iQualDev],
                        d_transformedPtsZRawPointerVec[iQualDev]);

                //                // for debug purpose
                //                if (!(rtStructure->roiVector[iRoi].RoiName).compare("CTV"))
                //                {
                //                    //thrust::host_vector<float> tempArray2(d_DoseValueTransformedPtsVec[iQualDev]->size()); // TODO: find a better way other thatn host_vector copying to allocate memory on gpu
                if (isVerbose)
                {

                std::cout << class_member << "\n \n \n \n d_DoseValueTransformedPtsVec[iQualDev]->operator[](0): "<< d_DoseValueTransformedPtsVec[iQualDev]->operator[](0) <<std::endl; //TODO: don't forget this operator!
                //                    //thrust::copy_n(&tempArray2[0],1,std::ostream_iterator<float>(logstream, ","));
                //                    //   thrust::copy_n(&d_transformedPtsYVec[iQualDev][0],1,std::ostream_iterator<float>(logstream, ","));
                //                    //  thrust::copy_n(&d_transformedPtsZVec[iQualDev][0],1,std::ostream_iterator<float>(logstream, ","));

                std::cout << "x[0]=" <<  d_transformedPtsXVec[iQualDev]->operator[](0) << ",  y=" <<  d_transformedPtsYVec[iQualDev]->operator[](0) << ",   z=" <<  d_transformedPtsZVec[iQualDev]->operator[](0) <<  "\n \n \n "<< std::endl;
}


                //                    //std::cout << "Dose is  = " <<  d_DoseValueTransformedPtsVec[iQualDev][0] << std::endl;
                //                }



                // For each trial,  we need to compute the sum of the dose recive by each point in the point could in all the fractions
                // d_DoseValueForTransformedPtsRawPointerVec[iQualDev] --> d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev]

                sumUpFractionDosesOnGPU(d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev],
                                        d_DoseValueForTransformedPtsRawPointerVec[iQualDev],
                                        numFractions,
                                        &fractionsDoseWeights[0],
                        numTransToLaunch,
                        PointsCloudX.size(),
                        deviceInfo.indicesOfQualifiedDevices[iQualDev]);

                if (isVerbose)
                {

                std::cout << class_member << "\n DVH calculation for dose at cloud points from transportation #" << startIndexOfTransformationsToLaunch << "  to  #"  << stopIndexOfTransformationsToLaunch << " for ROI  on Device " << deviceInfo.indicesOfQualifiedDevices[iQualDev]<< std::endl;
}

                //                dvhCalcGPU(&cDVHForTrial[startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
                //                        &diffDVHForTrial[startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
                //                        numDoseBins,
                //                        d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev],
                //                        doseBinWidth,
                //                        PointsCloudX.size(),
                //                        numTransToLaunch/numFractions, // we made sure that numTransToLaunch is integer multiple of numFractions --> so this gives number of Trials in this launch
                //                        deviceInfo.indicesOfQualifiedDevices[iQualDev]);


                dvhCalcWithPartialVolumeGPU(&cDVHForTrial[startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
                        &diffDVHForTrial[startIndexOfTransformationsToLaunch/numFractions*numDoseBins],
                        numDoseBins,
                        d_DoseValueForTransformedPtsFractionsCombinedRawPointerVec[iQualDev],
                        d_inPolygonPointsPartialVolumeforROIiRawPointerVec[iQualDev],
                        doseBinWidth,
                        PointsCloudX.size(),
                        numTransToLaunch/numFractions, // we made sure that numTransToLaunch is integer multiple of numFractions --> so this gives number of Trials in this launch
                        deviceInfo.indicesOfQualifiedDevices[iQualDev]);


                // if (iQualDev==1)
                // {
                //            thrust::copy_n(&cDVHForTransformedPts[iRoi][startIndex*numDoseBins],100,std::ostream_iterator<float>(logstream, ","));
                // }

                delete(d_transformedPtsXVec[iQualDev]);
                delete(d_transformedPtsYVec[iQualDev]);
                delete(d_transformedPtsZVec[iQualDev]);
                delete(d_DoseValueTransformedPtsVec[iQualDev]);
                delete(d_DoseValueForTransformedPtsFractionsCombinedVec[iQualDev]);

                //                    thrust::device_free(d_transformedPtsXVec[iQualDev]);
                //                    thrust::device_free(d_transformedPtsYVec[iQualDev]);
                //                    thrust::device_free(d_transformedPtsZVec[iQualDev]);
                //                    thrust::device_free(d_DoseValueTransformedPtsVec[iQualDev]);



            } // if transformationJobDistributionVec[iQualDev].size() ends here
        } // #pragma omp parallel



    }// iBatchLaunch





    //        if (iRoi==rtStructure->roiVector.size()) // TODO : Notice we don't need to delete dose information from devices we can do this when iRoi==numberRoi
    //        {
    for (int iQualDev = 0; iQualDev < deviceInfo.transformationJobDistributionVec.size(); ++iQualDev)
    {
        delete(d_doseArraryPointerVec[iQualDev]);
        delete(d_doseGridXvaluesPointerVec[iQualDev]);
        delete(d_doseGridYvaluesPointerVec[iQualDev]);
        delete(d_doseGridZvaluesPointerVec[iQualDev] );
    }
    //        }

    // we need to creat the following resources for each ROI and delete them before doing calculation on the next ROI
    for (int iQualDev = 0; iQualDev < deviceInfo.transformationJobDistributionVec.size(); ++iQualDev)
    {
        delete(d_inPolygonPointsXforROIiPointerVec[iQualDev]);
        delete(d_inPolygonPointsYforROIiPointerVec[iQualDev]);
        delete(d_inPolygonPointsZforROIiPointerVec[iQualDev]);
        delete(d_translationVectorsPointerVec[iQualDev]);
        delete(d_rotationMatricesPointerVec[iQualDev]);
    }



    return cudaGetLastError();
}

