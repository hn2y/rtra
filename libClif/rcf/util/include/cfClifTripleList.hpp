/* 
 * File:   cfClifTripleList.hpp
 * Author: fsleeman
 *
 * Created on April 26, 2011, 2:56 PM
 */

#ifndef CFCLIFTRIPLELIST_HPP
#define	CFCLIFTRIPLELIST_HPP

#include "cfTypedefs.hpp"
#include <string>
#include <vector>

struct StringTriple
{
    std::string a;
    std::string b;
    std::string c;
};

class cfClifTripleList
{
    public:
        cfClifTripleList();
        ~cfClifTripleList();
        bool AddTriple(std::string line);
        int GetNumberOfTriples() { return list.size(); }
        std::string GetHandle() { return handle; }
        void SetHandle( std::string tripleListHandle ) { handle = tripleListHandle; }
        StringTriple GetTriple(unsigned int index);
        void GetTripleVector(std::vector<cfPoint3D<int> >& tripleVector);
        void GetTripleVector(std::vector<cfPoint3D<float> >& tripleVector);
    private:
        std::string handle;
        std::vector< StringTriple > list;
};

#endif	/* CFCLIFTRIPLELIST_HPP */

