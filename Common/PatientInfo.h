#include <QString>
#include <QVarLengthArray>
//#include "opencv/cv.h"
#include <QVector>

#ifndef ENUMS_H
#define ENUMS_H
enum ContourGeometricType{UNKNOWNGEOMETRICTYPE =0,IsPOINT =1,IsCLOSEDPLANAR =2/* Close_planer is used in dcm tags */};
enum RTStructureType {UNKNOWNSTRUCTURETYPE =0,PINNACLEFILE =1,DICMFILE =2};
enum Modality {UNKOWNIMAGE =0,CTImageModality =1 /* CT in dicom tags */,PETImage =2};
enum PatientPositionDescriptor{HFP,HFS,HFDR,HFDL,FFP,FFS,FFDR,FFDL};
#endif

#ifndef POINT3D_H
#define POINT3D_H
struct Point3D
{
    float x,y,z;
};
#endif

#ifndef CONTOUR_H
#define CONTOUR_H
struct Contour
{
    ContourGeometricType geometricType =UNKNOWNGEOMETRICTYPE;
    QVarLengthArray< QVector<Point3D>,1 > contourDataQVarLengthArrayOfQVector;
};
#endif

#ifndef ROI_H
#define ROI_H
struct RegionOfInterest
{
    QString sRoiName;
    signed int nRoiNumber;
    unsigned int nDisplayColor[3];
    bool isTargetVolume;
    QString sReferencedFrameOfReferenceUID;
    signed int ItemNumberInRoiContourSequence; /* the number of corresponding RoiContourSequenceItem in DICOM file */
    float fCenterOfMass[3];
    float fBoundingBox_XYZ_MxMn[3][2]; // [x y z][max min]

    /* Bit Map representation of ROI */
    bool bVoxelBitStartState;
    QVector<Contour> contourSequenceQVector;
    QVector<int> voxelBitStateChangeQVector;

};
#endif // ROI_H

#ifndef RTSTRUCTURESET_H
#define RTSTRUCTURESET_H
struct RTStructureSet
{
    QString sRTStructureFileName;
    RTStructureType rTStructureType =UNKNOWNSTRUCTURETYPE;
    // TODO : throws error 'QVector' : invalid template argument for 'T', type expected
    // after adding cfTypedefs.hpp from rcf
      QVector<RegionOfInterest> ROIsSequenceQVector;
};
#endif // RTSTRUCTURESET_H

#ifndef CT_H
#define CT_H
struct CT
{
    float fCTsSlope = 0.0f;
    float fCTsIntercept = 0.0f;
    float fPixelSpacing[3];
    float fOffSet[2];
    float fSliceLocation =0;
    int nNumberOfFrames =0; //(0028,0008)

    qint16 PixelRepresentation;
    qint16 BitsAllocated;
    qint16 BitsStored;
    qint16 HighBit;
    qint16 SamplesPerPixel;

    float fImagePositionPatient[3];
    int nImageOrientation[6];
    //cv::Mat cvGreyCTImage;
};
#endif // CT_H

#ifndef CTIMAGESET_H
#define CTIMAGESET_H
struct CTImageSet
{
    Modality modality =UNKOWNIMAGE;
    PatientPositionDescriptor patientPositionDescriptor;
    QVector<RTStructureSet> RTStructureSequenceQVector; // TODO: this should be in the Plan file or has a different Structure for it

};
#endif // CTIMAGESET_H

#ifndef PATIENT_H
#define PATIENT_H
struct Patient
{
    QString sPatientID;
    QString sPatientName;
    QVector<CTImageSet> cTImageSetSequenceQVector;

};

#endif // PATIENT_H
