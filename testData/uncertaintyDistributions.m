close all force
clear variables
clc
numSamples=1e7;
rStd=5.7;
rVec=rStd* randn(numSamples,1);
dV=zeros(3,numSamples);
binEdges= linspace(-rStd*5,rStd*5,30);

% scenario 1 : for each sample one of the dx,dy,dz = rVec
subSample=randi(3,numSamples,1);
indices= sub2ind([3,numSamples], subSample,(1:numSamples)');
dV(indices)=rVec;
subplot(2,2,1)
plot3(dV(1,:), dV(2,:), dV(3,:),'.')
subplot(2,2,2)
histogram(dV(1,:),binEdges )
subplot(2,2,3)
histogram(dV(2,:),binEdges )
subplot(2,2,4)
histogram(dV(3,:),binEdges )

%%  senario 2 : sample dx dy and dz independently
figure
chiPdf = @(x,k) x.^(k-1).*exp(-x.^2/2)/2^(k/2-1)/gamma(k/2);
chiCdf =@ (x,k) gammainc(k/2,x.^2/2,'upper') / gamma(k/2);
chiSquaredPdf = @(x,k) x.^(k/2-1).*exp(-x.^2/2)/2^(k/2)/gamma(k/2);
% subStd=rStd/sqrt(3);
edge2binCenter = @(edges) edges(1:end-1)+ diff(edges)/2;

dV = zeros(3,numSamples);
subStdVec=[3];


for i=1:length(subStdVec)
    subStd=subStdVec(i);
    dV(1,:)=subStd* randn(1,numSamples);
    dV(2,:)=subStd* randn(1,numSamples);
    dV(3,:)=subStd* randn(1,numSamples);
    
    % [theta,rho,z] = cart2pol(dV(1,:),dV(2,:),dV(3,:));
    [azimuth,elevation,r] = cart2sph(dV(1,:),dV(2,:),dV(3,:));
    %     subplot(2,2,1)
    %     plot3(dV(1,:), dV(2,:), dV(3,:),'.')
    %
    %     subplot(2,2,2)
    %     histogram(dV(1,:),binEdges )
    %     subplot(2,2,3)
    %     histogram(dV(2,:),binEdges )
    %     subplot(2,2,4)
    %     histogram(dV(3,:),binEdges )
    figure
%     histogram(r) % confirmed that the mean is sqrt(2)*gamma((k+1)/2)/gamma(k/2) where k=3
    edges= linspace(0,max(r)+.1,60);
    [N,edges] = histcounts(r,edges);
    xData=edge2binCenter(edges)';
    yData= N'/trapz(xData,N);
    plot(xData,yData)
    hold on
    plot(xData,chiPdf(xData/subStd,3)/subStd)
    meanAnalytical= sqrt(2)*gamma( (3+1)/2)/gamma(3/2);
    meanAnalyticalScaled = subStd*meanAnalytical;
    stdAnalytical=subStd*sqrt( 3-meanAnalytical^2)
    std(r)
%     plot(xData,chiSquaredPdf(xData/subStd,3)/subStd)

    % rSampled with the second method
    figure
    rSampled= elevation;%sqrt(sum(dV.^2));
    edges= linspace(min(rSampled)-.1,max(rSampled)+.1,60);
    % histogram(rSampled,edges)
    [N,edges] = histcounts(rSampled,edges);
    xData=edge2binCenter(edges)';
    yData= N'/trapz(xData,N);
    
    hold on
    plot(xData,yData)
    title('r: second scenario')
    yhat = fit(xData,yData,'a*cos(x)');
    plot(yhat,xData,yData)
    subStdVec(i)=subStd;
    estimatedSigmaVec(i)=(yhat.a)^.5;
end
% the result indicate that to reproduce square root of sumation of squares of three independent normal distributions
% with std of sigma follows chiPdf(xData/sigma,3)/sigma
% chiPdf=  @(x,k) x.^(k-1).*exp(-x.^2/2)/2^(k/2-1)/gamma(k/2);
% Azimuth from uniform distribution [-pi , pi]
% Elevation from 0.5*cos(x)  and abs(x)>pi/2 -->x=0
%     meanAnalytical= sqrt(2)*gamma( (3+1)/2)/gamma(3/2);
%     meanAnalyticalScaled = sigma*meanAnalytical;
%     stdAnalytical=sqrt( 3-meanAnalytical^2)
%     stdAnalyticalScaled = sigma *stdAnalytical;
% Now if the std of magnitude of is given (stdAnalyticalScaled), we can
% find the corresponding sigma for each  .











