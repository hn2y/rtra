/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/*
$$::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$$
$$     FILE:  PinnComm.h
$$    TITLE:  Inter-process communication with a Pinnacle process.
$$  PROJECT:  General
$$
$$  ADAC/Geometrics 
$$  6510 Grand Teton Plaza
$$  Suite 310
$$  Madison WI 53719
$$
$$  Copyright 1997 ADAC/Geometrics 
$$  All Rights Reserved
$$
$$  DESIGN BY:  Gehring
$$
$$::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*/

#ifndef PINN_COMM_H
#define PINN_COMM_H

/* Buffer length for QueryString routine. */
#ifndef PINN_COMM_STRING_LEN
#define PINN_COMM_STRING_LEN 1024
#endif

/* Initialize all message passing data. 
 * This function initializes the top-level shell and all X atoms.
 * If the specified top level shell is NULL, a top level shell is
 * created along with a DrawingArea widget.
 */
extern 
#ifdef myCC
"C"
#endif
void vPinnComm_Initialize(
                void *pvTopLevelWidget,  /* If NULL, will initialize X. */
                int *piArgc, 
                char *ppcArgv[]);

/* Install the window ID of the remote (Pinnacle) process. 
 * This is typically used by the non-Pinnacle process to
 * install a window ID which has been specified on the command line.
 */
extern
#ifdef myCC
"C"
#endif
void vPinnComm_SetRemoteWindowId(int iWindowId);

/* Install the window ID of the local process.
 * This is typically used by the non-Pinnacle process to
 * install a window ID when the process has X windows of its own.
 * If a process relies on vPinnComm_Initialize() to create the X windows,
 * a call to vPinnComm_SetLocalWindowId() is not necessary.
 */
extern 
#ifdef myCC
"C"
#endif
void vPinnComm_SetLocalWindowId(int iWindowId);

/* Send a command to the root object.  If the command has an equal sign,
 * the right-hand side can be a value or a query.
 */
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_IssueRootCommand(char *pcString);

/* Send a value to the specified object.  Specify NULL for the root object. */
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_SetFloat(void *pvObject, char *pcMessage, float fValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_SetInt(void *pvObject, char *pcMessage, int iValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_SetObject(void *pvObject, char *pcMessage, void *pvValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_SetString(void *pvObject, char *pcMessage, char *pcString);

/* Query a value from the specified object (NULL for the root object.) */
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_QueryFloat(void *pvObject, char *pcMessage,
               float *pfValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_QueryInt(void *pvObject, char *pcMessage,
               int *piValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_QueryObject(void *pvObject, char *pcMessage,
               void **ppvValue);
extern 
#ifdef myCC
"C"
#endif
int iPinnComm_QueryString(void *pvObject, char *pcMessage,
               char acString[PINN_COMM_STRING_LEN]);


#endif  /* PINN_COMM_H */

