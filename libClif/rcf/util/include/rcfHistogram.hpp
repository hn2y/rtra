#ifndef RCFHISTOGRAM_HPP
#define RCFHISTOGRAM_HPP

#include "cfImage.hpp"
#include "cfDose.hpp"
#include <iostream>

/**
    \class rcfHistogram
    @ingroup UTIL

    This classes generates histograms for cfImage and cfDose.
 */

/// Modes for the histogram scaling
enum HistogramScaling
{
    BINCOUNT,
    BINSIZE,
    AUTOSCALE
};

class rcfHistogram
{
   public:
        rcfHistogram();
        /// Reset all of the histogram data and parameters
        void Reset();
        /// Sets the bounds for the histogram, inclusive
        bool SetBounds(float lower, float upper);
        /// Sets the number of bins, sets HistogramScaling to BINCOUNT
        bool SetNumberOfBins(int count);
        /// Sets the size of the bins, sets HistogramScaling to BINSIZE
        bool SetSizeOfBins(float size);
        /// Selects the method used to set up the histogram
        bool SetHistogramScalingMethod( HistogramScaling method );
        /// Sets whether or not zero (0.0) values should be automatically counted as underflow
        void MakeZerosUnderflow(bool areUnderflow) { areZerosUnderflow = areUnderflow; }
        /// Returns true is zeros (0.0) are being counted as underflow
        bool AreZerosUnderflow() { return areZerosUnderflow; }
        /// Return the number of bins used int the histogram
        int GetNumberOfBins() { return binCount; }
        /// Return the size of each bin used in the histogram
        float GetSizeOfBins() { return binSize; }
        /// Return a pointer to the histogram data with a size of binSize
        int* GetHistogramBuffer() { return histogramBuffer; }
        /// Populates the rcfHistogram with the histogram results
        bool GenerateHistogram(cfImage* image);
        /// Populates the rcfHistogram with the histogram results
        bool GenerateHistogram(cfDose* dose);
        /// Populates the rcfHistogram with the histogram results
        bool GenerateHistogram(float* buf, int length);
        /// Saved the histogram data in an ASCII text file in the specified location
        bool SaveHistogram(std::string path, std::string filename);
        /// Saved the histogram data in an ASCII text file int the specified location
        bool SaveHistogram(std::string fullFilePath);
        /// Sets the data lower bound, everything under is considered underflow
        void SetLowerBound(float lower);
        /// Sets the data upper bound, everything over is considered overflow
        void SetUpperBound(float upper);
        /// Returns the underflow lower bound
        float GetLowerBound() { return lowerBound; }
        /// Returns the overflow upper bound
        float GetUpperBound() { return upperBound; }
   private:
        /// Set up parameters used to set up the histogram
        bool SetUpHisogramParameters(void* dataBuffer, int bufferSize, float min, float max);        
        /// Fills the histogram buffer bins
        bool FillHistogramBuffer(void* buffer, bool removeUpperTail);
        /// Sets parameters when AUTOSCALE is selected
        bool SetAutoParameters();        
        bool isLowerBoundUserSelected; ///< True if the user specified a lower bound
        bool isUpperBoundUserSelected; ///< True if the user specified a upper bound
        bool areZerosUnderflow; ///< Selects of zeros (0.0) are counted as underflow
        int bufferSize; ///< Size of the histogram buffer
        int binCount; ///< Number of bins used for the histogram
        float binSize; ///< Size of each bin in the histogram        
        int underflow; ///< Number of elements above the upper limit        
        int overflow; ///< Number of element below the lower limit        
        float lowerBound; ///< Lower bound of the histogram, inclusive        
        float upperBound; ///< Lower bound of the histogram, inclusive                
        int* histogramBuffer; ///< Pointer to the array holding the histogram bins        
        e_cfType dataType; ///< Data type of the histogram buffer
        /// Enum for the scaling method of the histogram (bin count, bin size, auto scaling)
        HistogramScaling scalingMethod;
};

#endif
