#include "CTImage.h"
#include "Utilities/someHelpers.h"

CTImage::CTImage()
{

    ImagePositionPatient = std::vector<double>(3,0);
}

bool CTImage::parseCTDicomfile()
{
    status = ctDcmFileFormat.loadFile(dicomFilePath.c_str());
    if (status.bad())
    {
        std::cout << "ERROR in reading dicom file at path: " << dicomFilePath << std::endl;

        return false;
    }

    //            DRTImageIOD rtimage;
    //            status = rtimage.read(*fileformat.getDataset());
    //            DcmPixelData myPixelData = rtimage.getPixelData();
    //            myPixelData.getUint16Array()


    status=ctDcmFileFormat.getDataset()->findAndGetSint32(DCM_NumberOfFrames,NumberOfFrames);
    if (status.good())
    {
        std::cout << "NumberOfFrames: " << NumberOfFrames << std::endl;
        if (NumberOfFrames>1)
        {
            IsMultiFrame=true;
            //            if (mo.FilePathVec.size()>1)
            //            {
            //                std::cout << "ERROR: Multiple multiframe image: not supported yet " << std::endl;
            //            }
        }
    }
    else
    {
//        std::cout << "ERROR in reading NumberOfFrames:" << status.text()<< std::endl;
        if (strcmp(status.text(),"Tag not found")==0)
        {
            IsMultiFrame=false;
        }
        else
        {
            return false;
        }
    }

    OFString patientIdOFString;
    status=ctDcmFileFormat.getDataset()->findAndGetOFString(DCM_PatientID,patientIdOFString);
    if (status.bad())
    {
        std::cout << "ERROR in reading PatientID:" << status.text()<< std::endl;
        return false;
    }

    PatientID= patientIdOFString.c_str();

    OFString studyIDOFString;

    status=ctDcmFileFormat.getDataset()->findAndGetOFString(DCM_StudyID,studyIDOFString);
    if (status.bad())
    {
        std::cout << "ERROR in reading StudyID:" << status.text()<< std::endl;
        return false;
    }
    StudyID= studyIDOFString.c_str();


    OFString SeriesNumberOFString;

    status=ctDcmFileFormat.getDataset()->findAndGetOFString(DCM_SeriesNumber,SeriesNumberOFString);
    if (status.bad())
    {
        std::cout << "ERROR in reading SeriesNumber:" << status.text()<< std::endl;
        return false;
    }
    SeriesNumber= SeriesNumberOFString.c_str();


    OFString ModalityOFString;

    status=ctDcmFileFormat.getDataset()->findAndGetOFString(DCM_Modality,ModalityOFString);
    if (status.bad())
    {
        std::cout << "ERROR in reading Modality:" << status.text()<< std::endl;
        return false;
    }
    Modality= ModalityOFString.c_str();










    if (~IsMultiFrame)
    {

        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_Rows,Rows);
        if (status.bad())
        {
            std::cout << "ERROR in Rows:" << status.text()<< std::endl;
            return false;
        }

        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_Columns,Columns);
        if (status.bad())
        {
            std::cout << "ERROR in DCM_Columns:" << status.text()<< std::endl;
            return false;
        }
        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_PixelRepresentation,PixelRepresentation);
        if (status.bad())
        {
            std::cout << "ERROR in DCM_PixelRepresentation:" << status.text()<< std::endl;
            return false;
        }
        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_BitsAllocated,BitsAllocated);
        if (status.bad())
        {
            std::cout << "ERROR in DCM_BitsAllocated:" << status.text()<< std::endl;
            return false;
        }
        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_HighBit,HighBit);
        if (status.bad())
        {
            std::cout << "ERROR in DCM_HighBit:" << status.text()<< std::endl;
            return false;
        }
        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_BitsStored,BitsStored);
        if (status.bad())
        {
            std::cout << "ERROR in DCM_BitsStored:" << status.text()<< std::endl;
            return false;
        }
        status=ctDcmFileFormat.getDataset()->findAndGetUint16(DCM_SamplesPerPixel,SamplesPerPixel);

        if (status.bad())
        {
            std::cout << "ERROR in DCM_SamplesPerPixel:" << status.text()<< std::endl;
            return false;
        }



        //Pixel Spacing
        double pixelSpacingx;
        if (ctDcmFileFormat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingx, 0).good())// (0028,0030)
        {
            PixelSpacing[0] = pixelSpacingx;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing X in CT ? " << std::endl;
            return false;

        }

        double pixelSpacingy;
        if (ctDcmFileFormat.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingy, 1).good())// (0028,0030)
        {
            PixelSpacing[1] = pixelSpacingy;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing Y in CT " << std::endl;
            return false;

        }



        OFString pPosOFString;
        if (ctDcmFileFormat.getDataset()->findAndGetOFString(DCM_PatientPosition, pPosOFString).good())
        {

            //std::cout << "Patient's Pos: " << pPosOFString << std::endl;
        }
        else
        {
            std::cout << "Error: cannot access patient position!" << std::endl;
            return false;
        }
        if (pPosOFString.size() > 0)
        {
//            std::cout << "Patient's Pos: " << pPosOFString << std::endl;
            OFString pPosHFS = "HFS";

          //  std::cout << pPosOFString.compare(pPosHFS) << std::endl;
            if (pPosOFString.compare("HFS") == 0)				// HFS --> +x, -y, -z
            {
                PatientPosition = "HFS";
            }
            else if (pPosOFString.compare("HFP") == 0)		// "HFP" --> +x, +y, -z
            {
                PatientPosition = "HFP";

            }
            else if (pPosOFString.compare("FFS") == 0)		// FFS --> +x, -y, -z
            {
                PatientPosition = "FFS";

            }
            else if (pPosOFString.compare("FFP") == 0)		// FFP --> -x, +y, -z
            {
                PatientPosition = "FFP";
            }

        }
        else
        {
            std::cout << "Patient's Pos is not in the dcm file provided !" << std::endl;
            return false;
        }



        for (int i = 0; i < 3; ++i)
        {
            if (ctDcmFileFormat.getDataset()->findAndGetFloat64(DCM_ImagePositionPatient, ImagePositionPatient[i], i).good()) // (00200032)
            {
               // std::cout << "Read CT's DCM_ImagePositionPatient[" << i << "], " << ImagePositionPatient[i] << std::endl;
            }
            else
            {
                std::cout << "ERROR in reading CT's DCM_ImagePositionPatient[" << i << "]" << std::endl;
                return false;
            }
        }
        sliceZvalue = ImagePositionPatient[2];




        // reading ImageOrientationPatient
        for (int i = 0; i < 6; i++)
        {
            double value = 0.0;
            if (ctDcmFileFormat.getDataset()->findAndGetFloat64(DCM_ImageOrientationPatient, value, i).good()) // (0020,0037) image orientation
            {
                ImageOrientationPatient[i] = value;
            }
            else
            {
                std::cout << "Error in parsing CT image orientation " << std::endl;
                return false;
            }

        }



        // Computing yOffsetCT and xOffsetCT --> pixel in middle of the CT Volume
        if (abs(ImageOrientationPatient[4] - 1) < 1e-5)
        {
            yOffsetCT = ImagePositionPatient[1] + (PixelSpacing[0] * (Rows - 1) / 2);

        }
        else if (abs(ImageOrientationPatient[4] + 1) < 1e-5)
        {

            yOffsetCT = ImagePositionPatient[1] - (PixelSpacing[0] * (Rows - 1) / 2);

        }
        else
        {
            yOffsetCT = ImagePositionPatient[1];
        }



        if (PatientPosition.compare("HFS") == 0)
        {
            yOffsetCT = -yOffsetCT;
        }
        else if (PatientPosition.compare("HFP") == 0)
        {
            yOffsetCT = yOffsetCT;
            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;
        }
        else if (PatientPosition.compare("FFS") == 0)
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position other than FFS, it is supported but not tested extensively" << std::endl;
        }
        else if (PatientPosition.compare("FFP") == 0)
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position is " << PatientPosition << " which is not supported using defalut position (HFS)" << std::endl;
        }
      //  std::cout << "yoffsetCT = " << yOffsetCT << std::endl;


        if (abs(ImageOrientationPatient[0] - 1) < 1e-5)
        {
            xOffsetCT = ImagePositionPatient[0] + (PixelSpacing[1] * (Columns - 1) / 2);

        }
        else if (abs(ImageOrientationPatient[0] + 1) < 1e-5)
        {

            xOffsetCT = ImagePositionPatient[0] - (PixelSpacing[1]* (Columns - 1) / 2);

        }
        else
        {
            xOffsetCT = ImagePositionPatient[0];
        }



        if (PatientPosition.compare("HFS") == 0)
        {
            xOffsetCT = xOffsetCT;
        }
        else if (PatientPosition.compare("HFP") == 0)
        {
            xOffsetCT = -xOffsetCT;
            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;
        }
        else if (PatientPosition.compare("FFS") == 0)
        {
            xOffsetCT = xOffsetCT;
            std::cout << " Patient position other than FFS, it is supported but not tested extensively" << std::endl;
        }
        else if (PatientPosition.compare("FFP") == 0)
        {
            xOffsetCT = -xOffsetCT;
            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            xOffsetCT = xOffsetCT;
            std::cout << " Patient position is " << PatientPosition << " which is not supported using defalut position (HFS)" << std::endl;
        }

//        std::cout << "xoffsetCT = " << xOffsetCT << std::endl;

        double xStart= xOffsetCT-PixelSpacing[0]*static_cast<double>(Columns-1)/2;
        double xEnd= xOffsetCT+PixelSpacing[0]*static_cast<double>(Columns-1)/2;


        double yStart= yOffsetCT-PixelSpacing[1]*static_cast<double>(Rows-1)/2;
        double yEnd= yOffsetCT+PixelSpacing[1]*static_cast<double>(Rows-1)/2;


        gridXvalues = generateFloatRange(xStart,PixelSpacing[0],xEnd);
        gridYvalues = generateFloatRange(yStart,PixelSpacing[1],yEnd);







        if (!readPixelData())
        {
            return false;
        }






    }
    else
    {
        std::cout << "ERROR: multiframe CT image: not supported yet " << std::endl;

    }

}

bool CTImage::readPixelData()
{


    if (BitsAllocated > 16)
    {
        std::cout << "Upto 16 bits per scan pixel are supported"<< std::endl;
        return false;
    }

    pixelDataVectorUint16.clear();
    const Uint16 *pixelDataUint16=NULL;




    switch (PixelRepresentation)
    {
    case 0:
        if (BitsAllocated == 16)
        {


            if (ctDcmFileFormat.getDataset()->findAndGetUint16Array(DCM_PixelData, pixelDataUint16, &pixelCount).good())
            {
//                    std::cout << "number of pixels of Uint8 in this CT:" << pixelCount << std::endl;
//                    std::cout << "pixelDataUint16[0] "<< (int)pixelDataUint16[0]  << "pixelDataUint16[1] " << (int)pixelDataUint16[1] << "pixelDataUint16[2]=  "  << (int)pixelDataUint16[2]  << std::endl;
            }
            else
            {
                std::cout << "ERROR in running findAndGetUint16Array !" << std::endl;
                return false;
            }

            pixelDataVectorUint16.resize(Rows*Columns,0);
            int indexCTRead=0; // from pixelDataUint16  row major
            int indexCTWrite=0; // we write it into pixelDataVecorUnit16 in col major
            for (int j = 0; j < Columns; j++)
            {
                for (int i = 0; i < Rows; i++)
                {
                    indexCTRead =  i*Columns + j; // row major , dose data is saved in raw major order
                    //indexCTWrite = j*Rows + i; // col major, we save the dose in col major order
                    indexCTWrite = indexCTRead;
                    pixelDataVectorUint16[indexCTWrite]=pixelDataUint16[indexCTRead];
                }
            }
        }
        maxValue = static_cast<int>(*std::max_element(pixelDataVectorUint16.begin(), pixelDataVectorUint16.end()));

        minValue = static_cast<int>(*std::min_element(pixelDataVectorUint16.begin(), pixelDataVectorUint16.end()));

        break;
    case 1:
        // this is how it is done in CERR not sure if this is the best way
        //                    if bitsAllocated == 16
        //                        if strcmpi(class(sliceV),'int32')
        //                            sliceV = typecast(sliceV,'int16');
        //                            sliceV = sliceV(1:2:end);
        //                        else
        //                            sliceV = typecast(sliceV,'int16');
        //                        end
        //                    end
        //                otherwise
        //                    sliceV = typecast(sliceV,'int16');



        std::cout <<"ERROR : PixelRepresentation 1 is still not supported fully" <<std::endl;
        return false;
        break;
    default:
        std::cout << "PixelRepresentation is not 0 or 1: reading it as int16"<< std::endl;
        return false;
        break;


    } // switch ends


    return true;
}

bool CTImage::setCTDicomFilePath(const std::string  ctDicomFilePath)
{


    if (FILE *file = fopen(ctDicomFilePath.c_str(), "r")) {
        fclose(file);
    } else {
        std::cout << "ERROR: file does not exist at path " <<  ctDicomFilePath<< std::endl;

        return false;
    }
    dicomFilePath= ctDicomFilePath;
    return true;
}

bool CTImage::savePixelDataToASCIIFile(std::string filePath)
{

    std::ofstream FILE(filePath);
    if (FILE.is_open())
    {
        if (pixelDataVectorUint16.size()>0)
        {
            for (int count = 0; count < pixelDataVectorUint16.size(); count++){
                FILE << pixelDataVectorUint16[count] << "\n";
            }
            FILE.close();
        }
        else if (pixelDataVectorUint32.size()>0)
        {
            for (int count = 0; count < pixelDataVectorUint32.size(); count++){
                FILE << pixelDataVectorUint32[count] << "\n";
            }
            FILE.close();
        }
    }
    else
    {
        std::cout << "Unable to open file " << filePath;
        return false;
    }

    return true;
}


