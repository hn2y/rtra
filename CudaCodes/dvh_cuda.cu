
// __device__ double fmod	(	double 	x, double 	y)	
//http://developer.download.nvidia.com/compute/cuda/4_1/rel/toolkit/docs/online/group__CUDA__MATH__DOUBLE.html
//http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH__SINGLE.html#group__CUDA__MATH__SINGLE
#include <cuda.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <iostream>


#include <thrust/scan.h>
#include <thrust/iterator/reverse_iterator.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>


// volIndoseBins contains dvh results of all dose sets --> volIndoseBins =[volInDoseBin1,volInDoseBin2, ..., volInDoseBinN]
//  volInDoseBinN of size ( NumPtsInEachSet) which contains dvh result for does set N -- > where N =1:numDoseSets
extern "C" void dvhCalcWithPartialVolumeGPU(float *dvhs, float *volIndoseBins, const int EachDoseSetBinNumbers, const float *d_dose_in, const float *d_dosePartialVolume_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum);
extern "C" void dvhCalcGPU(float *dvhs, float *volIndoseBins, const int EachDoseSetBinNumbers, const float *d_dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int deviceNum);


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		//fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}


// volIndoseBins contains dvh results of all dose sets --> volIndoseBins =[volInDoseBin1,volInDoseBin2, ..., volInDoseBinN]
//  volInDoseBinN of size ( NumPtsInEachSet) which contains dvh result for does set N -- > where N =1:numDoseSets
__global__ void dvh_cuda(float *volIndoseBins, const int EachDoseSetBinNumbers, const float *dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets)
{

	int id_x = blockIdx.x * blockDim.x + threadIdx.x;
	//     int id_y = blockidx.y * blockDim.y + threadIdx.y ;
	//      int absoulteId = id_y*Nby2 + id_x ;
	if (id_x < NumPtsInEachSet*numDoseSets)
	{

		float myItem = dose_in[id_x];
		int N = id_x / NumPtsInEachSet; // DVH set number
        int myBin = floorf(myItem / DoseBinWidth); // we assume  that myBin is not greater than EachDoseSetBinNumbers
        if (myBin < EachDoseSetBinNumbers)
		{
            atomicAdd(&volIndoseBins[N*EachDoseSetBinNumbers + myBin], 1.0); // Atomic tends to be slow  for large number of points but since we have several different bins here the ratio between data points ( NumPtsInEachSet*numDoseSets) and bins (EachDoseSetBinNumbers*numDoseSets) is going to be in order of 100  --> for an OARwith 50000 points and EachDoseSetBinNumbers=500 we get the ratio is 100
		}
		
	}
}

__global__ void dvh_cuda_parVol(float *volIndoseBins, const int EachDoseSetBinNumbers, const float *dose_in, const float *dose_in_PartVol, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets)
{

    int id_x = blockIdx.x * blockDim.x + threadIdx.x;
    //     int id_y = blockidx.y * blockDim.y + threadIdx.y ;
    //      int absoulteId = id_y*Nby2 + id_x ;
    if (id_x < NumPtsInEachSet*numDoseSets)
    {

        float myItem = dose_in[id_x];
        int N = id_x / NumPtsInEachSet; // DVH set number
        int pointIndexInDoseSet = id_x%NumPtsInEachSet;
        float partialVol=dose_in_PartVol[pointIndexInDoseSet];
        int myBin = floorf(myItem / DoseBinWidth); // we assume  that myBin is not greater than EachDoseSetBinNumbers

        if (myBin < EachDoseSetBinNumbers)
        {
            atomicAdd(&volIndoseBins[N*EachDoseSetBinNumbers + myBin], partialVol); // Atomic tends to be slow  for large number of points but since we have several different bins here the ratio between data points ( NumPtsInEachSet*numDoseSets) and bins (EachDoseSetBinNumbers*numDoseSets) is going to be in order of 100  --> for an OARwith 50000 points and EachDoseSetBinNumbers=500 we get the ratio is 100
        }

    }
}



__global__ void castIntToFloat(float *output, const int *in, const int numElmem)
{

	int id_x = blockIdx.x * blockDim.x + threadIdx.x;
	//     int id_y = blockidx.y * blockDim.y + threadIdx.y ;
	//      int absoulteId = id_y*Nby2 + id_x ;
	if (id_x <numElmem)
	{

		output[id_x] = (float) in[id_x];
	}
}

//
//__global__ void divideByConstant(float *in,  const int divisor, const float numElem)
//{
//
//	int id_x = blockIdx.x * blockDim.x + threadIdx.x;
//	//     int id_y = blockidx.y * blockDim.y + threadIdx.y ;
//	//      int absoulteId = id_y*Nby2 + id_x ;
//	if ((id_x <numElmem) & (divisor!=0))
//	{
//
//		in[id_x] = in[id_x]/divisor;
//	}
//}

// functor with different argument 
// http://stackoverflow.com/questions/30749426/what-is-the-optimal-way-to-use-additional-data-fields-in-functors-in-thrust



void dvhCalcGPU(float *dvhs, float *volIndoseBins, const int EachDoseSetBinNumbers, const float *d_dose_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum)
{
	if (numDoseSets == 0)
	{
		std::cout << "dvhCalcGPU : numDoseSets is zero !!!!!!!!!!!" << std::endl;
		return;
	}
    gpuErrchk(cudaSetDevice(devNum));
    cudaDeviceProp prop;

    gpuErrchk(cudaGetDeviceProperties(&prop,devNum));

	//  number of kenel to lanuch  --> np
	int n_threads_per_block = 1 << 10;  // TODO : 1024 threads per block // note: optimal value is harware dependent
	int n_blocks = ceil((double)NumPtsInEachSet*(double)numDoseSets / (double)n_threads_per_block);
	std::cout << "number blocks for dvhCalcGPU= " << n_blocks << std::endl;
	std::cout << "threads per blocks in dvhCalcGPu = " << n_threads_per_block << std::endl;
    float * d_volIndoseBins;
    gpuErrchk(cudaMalloc((void**)&d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets*sizeof(float)));
    gpuErrchk(cudaMemset(d_volIndoseBins, 0, sizeof(float)*EachDoseSetBinNumbers*numDoseSets));

	float * d_dvhsRawPointer;
    gpuErrchk(cudaMalloc((void**)&d_dvhsRawPointer, EachDoseSetBinNumbers*numDoseSets*sizeof(float)));
    gpuErrchk(cudaMemset(d_dvhsRawPointer, 0, sizeof(float)*EachDoseSetBinNumbers*numDoseSets));


	// wrap raw pointer with a device_ptr 
	thrust::device_ptr<float> d_dvhsDevicePtr = thrust::device_pointer_cast(d_dvhsRawPointer);

    //thrust::device_vector<float> d_dvhs(d_dvhsDevicePtr, d_dvhsDevicePtr + EachDoseSetBinNumbers*numDoseSets);

    dvh_cuda << <n_blocks, n_threads_per_block >> >(d_volIndoseBins, EachDoseSetBinNumbers, d_dose_in, DoseBinWidth, NumPtsInEachSet, numDoseSets);
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());

    // TODO: find a better way (Like in-place converstion that doesnot require this copying and  casting (waste of memory)

    //castIntToFloat << <n_blocks, n_threads_per_block >> >(d_dvhsRawPointer, d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets);
    //gpuErrchk(cudaPeekAtLastError());
    //gpuErrchk(cudaDeviceSynchronize());
    gpuErrchk(cudaMemcpy(d_dvhsRawPointer,d_volIndoseBins , EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToDevice));


	typedef thrust::device_vector<float>::iterator Iterator;


	//thrust::host_vector<float> hv(4); 
	//hv[0] = 1;
	//hv[1] = 2;
	//hv[2] = 3;
	//hv[3] = 4;

	//thrust::device_vector<float> dv = hv;
	//thrust::reverse_iterator<Iterator> rI(dv.end());
	//std::cout << "rI[0]= " << rI[0] << std::endl;
	//std::cout << "rI[1]= " << rI[1] << std::endl;
	//std::cout << "rI[2]= " << rI[2] << std::endl;
	//thrust::inclusive_scan(rI, rI + 4, rI);
	//std::cout << "dv[0]= " << dv[0] << std::endl;
	//std::cout << "dv[1]= " << dv[1] << std::endl;
	//std::cout << "dv[2]= " << dv[2] << std::endl;


    /*thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<int>(std::cout, ","));
	std::cout << std::endl;
*/

    //thrust::copy_n(d_dvhs.begin(), EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<int>(std::cout, ","));
	//std::cout << std::endl;
	// // note that we point the iterator to the *end* of the device_vector
    thrust::reverse_iterator<Iterator> reverseIter = make_reverse_iterator(d_dvhsDevicePtr + EachDoseSetBinNumbers*numDoseSets); // note that we point the iterator to the "end" of the device pointer area
	//std::cout << "reverseIter[0]= " << reverseIter[0] << std::endl;
	//std::cout << "reverseIter[1]= " << reverseIter[1] << std::endl;
	//std::cout << "reverseIter[2]= " << reverseIter[2] << std::endl;



    // compute cumulative sum of each dose set
	for (int k = 0; k < numDoseSets; k++) 
	{
        thrust::inclusive_scan(reverseIter , reverseIter + EachDoseSetBinNumbers, reverseIter);
        reverseIter += EachDoseSetBinNumbers;


    //thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<float>(std::cout, ","));
	//std::cout << std::endl;
	}

    thrust::device_vector<float> d_tempV(EachDoseSetBinNumbers);

	// normalizing dvhs
	for (size_t k = 0; k < numDoseSets; k++)
	{
        thrust::fill(d_tempV.begin(), d_tempV.end(), d_dvhsDevicePtr[k*EachDoseSetBinNumbers]);
        //thrust::copy_n(d_tempV.begin(), EachDoseSetBinNumbers, std::ostream_iterator<float>(std::cout, ","));
		//std::cout << std::endl;
        thrust::transform(d_dvhsDevicePtr + k*EachDoseSetBinNumbers, d_dvhsDevicePtr + (k + 1)*EachDoseSetBinNumbers, d_tempV.begin(), d_dvhsDevicePtr + k*EachDoseSetBinNumbers,
			thrust::divides<float>());
        //thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<float>(std::cout, ","));
		//std::cout << std::endl;
	}
	




    gpuErrchk(cudaMemcpy(dvhs, d_dvhsRawPointer, EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(volIndoseBins, d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaFree(d_volIndoseBins)); // free d_volIndoseBins
    //for (size_t i = 0; i < EachDoseSetBinNumbers*numDoseSets; i++)
	//{
    //	std::cout << "volIndoseBins[" << i << "] = " << volIndoseBins[i] << "			d_dvhsDevicePtr[" << i << "]= " << d_dvhsDevicePtr[i] << std::endl;
	//}
    gpuErrchk(cudaFree(d_dvhsRawPointer));
}

void dvhCalcWithPartialVolumeGPU(float *dvhs, float *volIndoseBins, const int EachDoseSetBinNumbers, const float *d_dose_in,const float *dosePartialVolume_in, const float DoseBinWidth, const int NumPtsInEachSet, const int numDoseSets, const int devNum)
{
    if (numDoseSets == 0)
    {
        std::cout << "dvhCalcWithPartialVolumeGPU : numDoseSets is zero !!!!!!!!!!!" << std::endl;
        return;
    }
    bool verbose =false;
    gpuErrchk(cudaSetDevice(devNum));
    cudaDeviceProp prop;

    gpuErrchk(cudaGetDeviceProperties(&prop,devNum));

    //  number of kenel to lanuch  --> np
    int n_threads_per_block = 1 << 10;  // TODO : 1024 threads per block // note: optimal value is harware dependent
    int n_blocks = ceil((double)NumPtsInEachSet*(double)numDoseSets / (double)n_threads_per_block);
    if (verbose)
    {
    std::cout << "number blocks for dvhCalcGPU= " << n_blocks << std::endl;
    std::cout << "threads per blocks in dvhCalcGPu = " << n_threads_per_block << std::endl;
    }
    float * d_volIndoseBins;
    gpuErrchk(cudaMalloc((void**)&d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets*sizeof(float)));
    gpuErrchk(cudaMemset(d_volIndoseBins, 0, sizeof(float)*EachDoseSetBinNumbers*numDoseSets));

    float * d_dvhsRawPointer;
    gpuErrchk(cudaMalloc((void**)&d_dvhsRawPointer, EachDoseSetBinNumbers*numDoseSets*sizeof(float)));
    gpuErrchk(cudaMemset(d_dvhsRawPointer, 0, sizeof(float)*EachDoseSetBinNumbers*numDoseSets));


    // wrap raw pointer with a device_ptr
    thrust::device_ptr<float> d_dvhsDevicePtr = thrust::device_pointer_cast(d_dvhsRawPointer);

    //thrust::device_vector<float> d_dvhs(d_dvhsDevicePtr, d_dvhsDevicePtr + EachDoseSetBinNumbers*numDoseSets);

    dvh_cuda_parVol << <n_blocks, n_threads_per_block >> >(d_volIndoseBins, EachDoseSetBinNumbers, d_dose_in, dosePartialVolume_in, DoseBinWidth, NumPtsInEachSet, numDoseSets);
    gpuErrchk(cudaPeekAtLastError());
    gpuErrchk(cudaDeviceSynchronize());

    // TODO: find a better way (Like in-place converstion that doesnot require this copying and  casting (waste of memory)

    //castIntToFloat << <n_blocks, n_threads_per_block >> >(d_dvhsRawPointer, d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets);
    //gpuErrchk(cudaPeekAtLastError());
    //gpuErrchk(cudaDeviceSynchronize());
    gpuErrchk(cudaMemcpy(d_dvhsRawPointer,d_volIndoseBins , EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToDevice));


    typedef thrust::device_vector<float>::iterator Iterator;


    //thrust::host_vector<float> hv(4);
    //hv[0] = 1;
    //hv[1] = 2;
    //hv[2] = 3;
    //hv[3] = 4;

    //thrust::device_vector<float> dv = hv;
    //thrust::reverse_iterator<Iterator> rI(dv.end());
    //std::cout << "rI[0]= " << rI[0] << std::endl;
    //std::cout << "rI[1]= " << rI[1] << std::endl;
    //std::cout << "rI[2]= " << rI[2] << std::endl;
    //thrust::inclusive_scan(rI, rI + 4, rI);
    //std::cout << "dv[0]= " << dv[0] << std::endl;
    //std::cout << "dv[1]= " << dv[1] << std::endl;
    //std::cout << "dv[2]= " << dv[2] << std::endl;


    /*thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<int>(std::cout, ","));
    std::cout << std::endl;
*/

    //thrust::copy_n(d_dvhs.begin(), EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<int>(std::cout, ","));
    //std::cout << std::endl;
    // // note that we point the iterator to the *end* of the device_vector
    thrust::reverse_iterator<Iterator> reverseIter = make_reverse_iterator(d_dvhsDevicePtr + EachDoseSetBinNumbers*numDoseSets); // note that we point the iterator to the "end" of the device pointer area
    //std::cout << "reverseIter[0]= " << reverseIter[0] << std::endl;
    //std::cout << "reverseIter[1]= " << reverseIter[1] << std::endl;
    //std::cout << "reverseIter[2]= " << reverseIter[2] << std::endl;



    // compute cumulative sum of each dose set
    for (int k = 0; k < numDoseSets; k++)
    {
        thrust::inclusive_scan(reverseIter , reverseIter + EachDoseSetBinNumbers, reverseIter);
        reverseIter += EachDoseSetBinNumbers;


    //thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<float>(std::cout, ","));
    //std::cout << std::endl;
    }

    thrust::device_vector<float> d_tempV(EachDoseSetBinNumbers);

    // normalizing dvhs
    for (size_t k = 0; k < numDoseSets; k++)
    {
        thrust::fill(d_tempV.begin(), d_tempV.end(), d_dvhsDevicePtr[k*EachDoseSetBinNumbers]);
        //thrust::copy_n(d_tempV.begin(), EachDoseSetBinNumbers, std::ostream_iterator<float>(std::cout, ","));
        //std::cout << std::endl;
        thrust::transform(d_dvhsDevicePtr + k*EachDoseSetBinNumbers, d_dvhsDevicePtr + (k + 1)*EachDoseSetBinNumbers, d_tempV.begin(), d_dvhsDevicePtr + k*EachDoseSetBinNumbers,
            thrust::divides<float>());
        //thrust::copy_n(d_dvhsDevicePtr, EachDoseSetBinNumbers*numDoseSets, std::ostream_iterator<float>(std::cout, ","));
        //std::cout << std::endl;
    }





    gpuErrchk(cudaMemcpy(dvhs, d_dvhsRawPointer, EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(volIndoseBins, d_volIndoseBins, EachDoseSetBinNumbers*numDoseSets*sizeof(float), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaFree(d_volIndoseBins)); // free d_volIndoseBins
    //for (size_t i = 0; i < EachDoseSetBinNumbers*numDoseSets; i++)
    //{
    //	std::cout << "volIndoseBins[" << i << "] = " << volIndoseBins[i] << "			d_dvhsDevicePtr[" << i << "]= " << d_dvhsDevicePtr[i] << std::endl;
    //}
    gpuErrchk(cudaFree(d_dvhsRawPointer));
}
