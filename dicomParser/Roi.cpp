#include "Roi.h"
#include <map>
Roi::Roi():logstream(std::cout)
{
    resolutionPointClouds=1.5;
    rtStructFileName=0;
    rtstructIODPointer=0;
    associateCTDicomFile=0;
    DisplayColor=std::vector<unsigned int>(3);
    IsTargetVolume=false;
    //parentRtstructure=0;

}

Roi::Roi(std::ostream &stream):logstream(stream)
{
    resolutionPointClouds=1.5;
    rtStructFileName=0;
    rtstructIODPointer=0;
    associateCTDicomFile=0;
    DisplayColor=std::vector<unsigned int>(3);
    IsTargetVolume=false;
    // parentRtstructure=0;
}

//Roi::Roi(std::ostream &stream,RtStructure *parent):logstream(stream)
//{
//    resolutionPointClouds=1.5;
//    rtStructFileName=0;
//    parentRtstructure=parent;
//}


Roi::~Roi()
{

}

bool Roi::generatePointCloud()
{
    std::string class_member = "Roi::generatePointCloud:";


    if (rtStructFileName==0)
    {
        logstream << class_member << "RT Structure file (rtStructFileName) has not been set yet." << std::endl;
        return false;
    }


    if (!calculatePoint2DIndicesInsideContoursOfEachSlice())
    {
        logstream << class_member << "Error --> calculatePoint2DIndicesInsideContoursOfEachSlice" << std::endl;
        return false;
    }

    if (!calcPoint3DIndicesInsideROI())
    {
        logstream << class_member << "Error --> calcPoint3DIndicesInsideROI" << std::endl;
        return false;
    }


    if (!calculateInpolygonPoints())
    {
        logstream << class_member << "Error --> calcUniqueContourZValuesAndRecontructIndices" << std::endl;
        return false;
    }


    if (!adjustPolygonPointsWRTPatientPosInCT())
    {
        logstream << class_member << "Error --> in adjustPolygonPointsWRTPatientPosInCT" << std::endl;
        return false;
    }
    return true;
}


bool Roi::adjustPolygonPointsWRTPatientPosInCT()
{
    std::string class_member = "Roi::adjustPolygonPointsWRTPatientPosInCT:";
    if (associateCTDicomFile==0)
    {
        logstream << class_member<< " ERROR --> associateCTDicomFile has not been set yet" << std::endl;

        return false;
    }
    DcmFileFormat fileformat1;
    OFCondition status1 = fileformat1.loadFile(associateCTDicomFile);
    OFString pPos = "";
    std::string patientPosition;
    OFString patientsID;

    if (status1.bad())
    {
        logstream << class_member<< "Error: cannot read associateCTDicomFile DICOM file (" << status1.text() << ")" << std::endl;
        return false;
    }

    //OFString patientPosition;
    if (fileformat1.getDataset()->findAndGetOFString(DCM_PatientPosition, pPos).good())
    {

        logstream << class_member<< "PatientPosition: " << pPos << std::endl;
    }
    else
    {
        logstream << class_member<< "Error: cannot access patient position!" << std::endl;
    }

    if (pPos.size() > 0)
    {
        //OFString pPosHFS = "HFS";
        //cout << pPos.compare(pPosHFS) << endl;

        if (pPos.compare("HFS") == 0)				// HFS --> +x, -y, -z
        {
            patientPosition = "HFS";
            std::vector<bool> selectedCoordinate = { false, true, true };

            // negateSelectedCoordinates(inPolygonPoints[cntROI], selectedCoordinate);
            negateSelectedCoordinates(inPolygonPointsX, inPolygonPointsY, inPolygonPointsZ, selectedCoordinate);
        }
        else if (pPos.compare("HFP") == 0)		// "HFP" --> +x, +y, -z
        {
            patientPosition = "HFP";
            std::vector<bool> selectedCoordinate = { false, false, true };

            // negateSelectedCoordinates(inPolygonPoints[cntROI], selectedCoordinate);
            negateSelectedCoordinates(inPolygonPointsX, inPolygonPointsY, inPolygonPointsZ, selectedCoordinate);
        }
        else if (pPos.compare("FFS") == 0)		// FFS --> +x, -y, -z
        {
            patientPosition = "FFS";
            std::vector<bool> selectedCoordinate = { false, true, true };
            //negateSelectedCoordinates(inPolygonPoints[cntROI], selectedCoordinate);
            negateSelectedCoordinates(inPolygonPointsX, inPolygonPointsY, inPolygonPointsZ, selectedCoordinate);

        }
        else if (pPos.compare("FFP") == 0)		// FFP --> -x, +y, -z
        {
            patientPosition = "FFP";
            std::vector<bool> selectedCoordinate = { true, false, true };
            //negateSelectedCoordinates(inPolygonPoints[cntROI], selectedCoordinate);
            negateSelectedCoordinates(inPolygonPointsX, inPolygonPointsY, inPolygonPointsZ, selectedCoordinate);
        }

    }
    else
    {
        logstream << class_member<< "Patient's Pos is not in the dcm file provided !" << std::endl;
        return false;
    }




    //% if isstr(pPos)
    //	% switch upper(pPos)
    //	% case 'HFS' %+x, -y, -z
    //	%             data(:, 2) = -data(:, 2);
    //%             %data(:, 2) = 2 * yOffsetCT * 10 - data(:, 2);
    //%         case 'HFP' %-x, +y, -z
    //	%             data(:, 1) = -data(:, 1);
    //%             data(:, 1) = 2 * xOffsetCT * 10 - data(:, 1);
    //%         case 'FFS' %+x, -y, -z
    //	%             data(:, 2) = -data(:, 2);
    //%             %data(:, 2) = 2 * yOffsetCT * 10 - data(:, 2);
    //%         case 'FFP' %-x, +y, -z
    //	%             data(:, 1) = -data(:, 1);
    //%             data(:, 1) = 2 * xOffsetCT * 10 - data(:, 1);
    //%     end
    //	% else
    //	%     data(:, 2) = -data(:, 2); %Default it to HFS
    //	% end


    return true;
}

std::vector<Point3> Roi::calculatePointsInPolygonFromIndices(const std::vector<int> &vI, std::vector<double> &xgridPts, std::vector<double> &ygridPts, std::vector<double> &zgridPts)
{
    std::string class_member = "Roi::calculatePointsInPolygonFromIndices:";

    Point3 point;
    point.x = 0.0;
    point.y = 0.0;
    point.z = 0.0;

    std::vector <Point3> points(vI.size(), point); // Initialize

    auto xLen = xgridPts.size();
    auto yLen = ygridPts.size();
    //	auto zLen = zgridPts.size();

    for (int i = 0; i < vI.size(); i++)
    {
        points[i].z = zgridPts[floor(vI[i] / xLen / yLen)];
        int xyInd = vI[i] % (xLen*yLen);
        points[i].y = ygridPts[xyInd% yLen];
        points[i].x = xgridPts[floor(xyInd / yLen)];
    }

    return points;
}

void Roi::calculatePointsInPolygonFromIndices(const std::vector<int>& vI,
                                              std::vector<double>& xgridPts, std::vector<double>& ygridPts, std::vector<double>& zgridPts,
                                              std::vector<double>& InPolygonPtsX, std::vector<double>& InPolygonPtsY, std::vector<double>& InPolygonPtsZ)
{
    /*Point3 point;
    point.x = 0.0;
    point.y = 0.0;
    point.z = 0.0;
    */
    //vector <Point3> points(vI.size(), point); // Initialize
    InPolygonPtsX = std::vector<double>(vI.size(), 0);// initialize
    InPolygonPtsY = std::vector<double>(vI.size(), 0);// initialize
    InPolygonPtsZ = std::vector<double>(vI.size(), 0);// initialize




    auto xLen = xgridPts.size();
    auto yLen = ygridPts.size();
    //	auto zLen = zgridPts.size();

    for (int i = 0; i < vI.size(); i++)
    {
//        if (zgridPts[0]<0) // TODO : Fix this for the cases --> when ???????????
//        {
//        InPolygonPtsZ[i] = zgridPts[zgridPts.size()-floor(vI[i] / xLen / yLen)-1];  //	points[i].z = zgridPts[floor(vI[i] / xLen / yLen)];
//        }
//        else //
//        {
//          InPolygonPtsZ[i] = zgridPts[floor(vI[i] / xLen / yLen)];  //	points[i].z = zgridPts[floor(vI[i] / xLen / yLen)];
//        }

        InPolygonPtsZ[i] = zgridPts[floor(vI[i] / xLen / yLen)];  //	points[i].z = zgridPts[floor(vI[i] / xLen / yLen)];
        int xyInd = vI[i] % (xLen*yLen);
        InPolygonPtsY[i] = ygridPts[xyInd% yLen]; //points[i].y = ygridPts[xyInd% yLen];
        InPolygonPtsX[i] = xgridPts[floor(xyInd / yLen)];		//points[i].x = xgridPts[floor(xyInd / yLen)];
    }

    //return points;
    //ptsInZsub = floor(point3DIndicesInsideROI / xLen / yLen);
    // xyInd= point3DIndicesInsideROI%xLen*yLen;
    //ptsInYsub = xyInd% yLen
    //ptsInXsub = floor(xyInd / yLen);
    //ptsInXpos = xPos(ptsInXsub);
    //ptsInYpos = yPos(ptsInYsub);
    //ptsInZpos = zPos(ptsInZsub);


}



/// generates gridXpos, gridYpos, gridZpos from resolutionPointClouds and boundingBoxForROI
void Roi::generateGridPoints()
{
    std::string class_member = "Roi::generateGridPoints:";

    gridXpos=generateRange(boundingBoxForROI.minX-resolutionPointClouds/ 2.0,resolutionPointClouds,
                           boundingBoxForROI.maxX+resolutionPointClouds/ 2.0);
    gridYpos=generateRange(boundingBoxForROI.minY-resolutionPointClouds/ 2.0,resolutionPointClouds,
                           boundingBoxForROI.maxY+resolutionPointClouds/ 2.0);
    gridZpos=generateRange(boundingBoxForROI.minZ-resolutionPointClouds/ 2.0,resolutionPointClouds,
                           boundingBoxForROI.maxZ+resolutionPointClouds/ 2.0);
}

bool Roi::calculateCOM()
{
    std::string class_member = "Roi::calculateCOM:";

    logstream << class_member<< " ROI's Center of Mass (COM) is calculated based on the generated point cloud" << std::endl;
    if (inPolygonPointsX.size()==0 | inPolygonPointsY.size()==0 | inPolygonPointsZ.size()==0)
    {
        logstream << class_member<< " ERROR --> inPolygonPoints vectors are empty. Forgot to run generatePointCloud?" << std::endl;
        return false;
    }

    RoiCOM.x=0;
    RoiCOM.y=0;
    RoiCOM.z=0;
    for (int iPts = 0; iPts < inPolygonPointsX.size(); ++iPts)
    {
        RoiCOM.x =  RoiCOM.x + inPolygonPointsX[iPts];
        RoiCOM.y =  RoiCOM.y + inPolygonPointsY[iPts];
        RoiCOM.z =  RoiCOM.z + inPolygonPointsZ[iPts];
    }
    RoiCOM.x =RoiCOM.x /inPolygonPointsX.size();
    RoiCOM.y =RoiCOM.y /inPolygonPointsY.size();
    RoiCOM.z =RoiCOM.z /inPolygonPointsZ.size();
    //
    return true;

}

bool Roi::getPatientName(std::string pName)
{
    std::string class_member = "Roi::getPatientName:";
    if (rtstructIODPointer==0)
    {
        logstream << class_member<< " ERROR --> rtstructIODPointer has not been set yet" << std::endl;

        return false;
    }
    OFCondition status;
    OFString pNameOFString;

    status= rtstructIODPointer->getPatientName(pNameOFString);
    if (!status.good())
    {
        logstream << class_member<< " ERROR --> OFCondition is bad" << std::endl;
        return false;
    }

    logstream << class_member << " Patient name for this ROI is  " <<  pNameOFString<< std::endl;
    return true;
}

bool Roi::findMatchingROIContourSequenceMatchingWithROINumber()
{
    std::string class_member = "Roi::findMatchingROIContourSequenceMatchingWithROINumber:";
    if (rtstructIODPointer==0)
    {
        logstream << class_member<< " ERROR --> rtstructIODPointer has not been set yet" << std::endl;

        return false;
    }
    OFCondition status;
    DRTROIContourSequence::Item * roiContourSequenceItem;
    DRTROIContourSequence roiContourSequence=rtstructIODPointer->getROIContourSequence();
    int num_items = roiContourSequence.getNumberOfItems();
    // search in ROI contour Sequence and find the item that satisfies ReferencedROINumber=RoiNumber
    for (int i=0;i<num_items;i++)
    {
        roiContourSequence.getItem(i,roiContourSequenceItem);
        Sint32 referenceRoiNum;
        roiContourSequenceItem->getReferencedROINumber(referenceRoiNum);
        if (referenceRoiNum==RoiNumber)
        {
            ItemNumberInRoiContourSequence=i;

            break;
        }
        else if(i==num_items-1)
        {
            logstream << class_member << "No matching roiContourSequenceItem for referenceRoiNum = " << RoiNumber << std::endl;

            return false;
        }
    }
    return true;
}

bool Roi::calculatePoint2DIndicesInsideContoursOfEachSlice()
{
    std::string class_member = "Roi::calculatePoint2DIndicesInsideContoursOfEachSlice:";

    if (rtstructIODPointer==0)
    {
        logstream << class_member<< " ERROR --> rtstructIODPointer has not been set yet" << std::endl;

        return false;
    }
    auto meshgridPts = xyMeshGrid(gridXpos, gridYpos);
    std::vector<int> associatedUniqueSlice(gridZpos.size(), -1);
    CorrespondingUniqueContourPlaneIdxForGridZpos = associatedUniqueSlice;


    // Find Contour Planes with Unique Z values
    auto uZvaluesAndReconIndices = uniqueContourZValuesAndRecontructIndices(zValuesVector);
    UniqueZvalueInEachContourSet = uZvaluesAndReconIndices.first;
    correspondingUniqueSliceIdxForEachContourInDICOMList = uZvaluesAndReconIndices.second;

    std::vector<double> slicesThicknessVec(UniqueZvalueInEachContourSet.size(), 0.0);
    thicknessOfSlicesWithUniqueZValue= slicesThicknessVec;
    std::vector<std::vector<int>> temp2(0, std::vector<int>(0));
    contourDICOMIndexForUniqueSlices =temp2;

    std::vector<std::vector<std::vector<int>>> temp3(0, std::vector<std::vector<int>>(0));
    point2DIndicesInsideContoursOfEachSlice= temp3;
    if (!findMatchingROIContourSequenceMatchingWithROINumber()) // this updates ItemNumberInRoiContourSequence
    {
        return false;
    }

    DRTROIContourSequence::Item * roiContourSequenceItem=0;
    DRTROIContourSequence roiContourSequenceLocal=rtstructIODPointer->getROIContourSequence();
    roiContourSequenceLocal.getItem(ItemNumberInRoiContourSequence,roiContourSequenceItem);
    OFCondition contourDataLoadStatus;
    DRTContourSequence contourSeq = roiContourSequenceItem->getContourSequence();
    OFVector<Float64> contourData;
    std::vector < std::vector<int>> inPolygonPtIndicesOnThisUniqueSlice;

    for (int idxUniqueSlice = 0; idxUniqueSlice < UniqueZvalueInEachContourSet.size(); idxUniqueSlice++)
    {

        //  region -->  calculate thicknessOfSlicesWithUniqueZValue, CorrespondingUniqueContourPlaneIdxForGridZpos
        //*******************************************************************************************************//
        //*******************************************************************************************************//
        if (idxUniqueSlice == 0)
        {
            thicknessOfSlicesWithUniqueZValue[idxUniqueSlice] = (UniqueZvalueInEachContourSet[1] - UniqueZvalueInEachContourSet[0]) / 2.0;
            //idx = find(zPos >= uniqueZvals(1) & zPos<(uniqueZvals(1) + sliceThickness(1)));
            // idx= find(gridZposROIs >= UniqueZvalueInEachContourSet[0] & gridZposROIs < UniqueZvalueInEachContourSet[0] + thicknessOfSlicesWithUniqueZValue[idxUniqueSlice]);
            // CorrespondingUniqueContourPlaneIdxForGridZpos[idx] = 0;

            // find Grid point z levels which are between first slice (0th) and the  2nd slice(indexed 1)
            auto a = UniqueZvalueInEachContourSet[idxUniqueSlice];
            auto b = thicknessOfSlicesWithUniqueZValue[idxUniqueSlice];
            auto idx = findSatisfiyingIndices(gridZpos, [a, b](double localx){return (localx >= a) &  (localx < a + b); });


            //crospondingUniqueContourPlaneIdx(idx) = 1;
            for (int iIdx = 0; iIdx < idx.size(); iIdx++)
            {
                CorrespondingUniqueContourPlaneIdxForGridZpos[idx[iIdx]] = idxUniqueSlice;
            }
        }
        else if (idxUniqueSlice == UniqueZvalueInEachContourSet.size() - 1)
        {
            //sliceThickness(i) = (uniqueZvals(i) - uniqueZvals(i - 1)) / 2;
            //idx = find((zPos >= (uniqueZvals(numUniquePlanes) - sliceThickness(numUniquePlanes)))&  zPos <= uniqueZvals(numUniquePlanes));
            //crospondingUniqueContourPlaneIdx(idx) = numUniquePlanes;
            thicknessOfSlicesWithUniqueZValue[idxUniqueSlice] = (UniqueZvalueInEachContourSet[idxUniqueSlice] - UniqueZvalueInEachContourSet[idxUniqueSlice - 1]) / 2.0;

            // find Grid point z levels which are between last slice and its adjacent slice
            auto a = UniqueZvalueInEachContourSet[idxUniqueSlice];
            auto b = thicknessOfSlicesWithUniqueZValue[idxUniqueSlice];
            auto idx = findSatisfiyingIndices(gridZpos, [a, b](double localx){return (localx >= a - b) &  (localx <= a); });

            //crospondingUniqueContourPlaneIdx(idx) = 1;
            for (int iIdx = 0; iIdx < idx.size(); iIdx++)
            {
                CorrespondingUniqueContourPlaneIdxForGridZpos[idx[iIdx]] = idxUniqueSlice;
            }

        }
        else
        {
            //sliceThickness(i) = (uniqueZvals(i) - uniqueZvals(i - 1));
            //idx = find(zPos >= (uniqueZvals(i) - sliceThickness(i) / 2) & zPos<(uniqueZvals(i) + sliceThickness(i) / 2));
            //crospondingUniqueContourPlaneIdx(idx) = i;
            thicknessOfSlicesWithUniqueZValue[idxUniqueSlice] = (UniqueZvalueInEachContourSet[idxUniqueSlice] - UniqueZvalueInEachContourSet[idxUniqueSlice - 1]);

            // find Grid point z levels which are between slice numbers idxUniqueSlice  and idxUniqueSlice-1
            auto a = UniqueZvalueInEachContourSet[idxUniqueSlice];
            auto b = thicknessOfSlicesWithUniqueZValue[idxUniqueSlice];
            auto idx = findSatisfiyingIndices(gridZpos, [a, b](double localx){return (localx >= a - b / 2) &  (localx <= a + b / 2); });

            //crospondingUniqueContourPlaneIdx(idx) = 1;
            for (int iIdx = 0; iIdx < idx.size(); iIdx++)
            {
                CorrespondingUniqueContourPlaneIdxForGridZpos[idx[iIdx]] = idxUniqueSlice;
            }

        }
        //#######################################################################################################//
        //#######################################################################################################//
        //  end region<--calculate thicknessOfSlicesWithUniqueZValue, CorrespondingUniqueContourPlaneIdxForGridZpos

        logstream << class_member <<"idxUniqueSlice = " << idxUniqueSlice << std::endl;

        contourDICOMIndexForUniqueSlices.push_back(findSatisfiyingIndices(correspondingUniqueSliceIdxForEachContourInDICOMList, [idxUniqueSlice](double localx){return localx == idxUniqueSlice; }));
        logstream <<  class_member<<  "For contour : " << RoiName << " the number of countour for unique slice: " << idxUniqueSlice << " = " << contourDICOMIndexForUniqueSlices[idxUniqueSlice].size() << std::endl;;


        //  region -->  fill inPolygonPtIndicesOnThisUniqueSlice
        //*******************************************************************************************************//
        //*******************************************************************************************************//
        inPolygonPtIndicesOnThisUniqueSlice.clear();



        DRTContourSequence::Item *contourItem;
        for (int iContour = 0; iContour < contourDICOMIndexForUniqueSlices[idxUniqueSlice].size(); iContour++)
        {
            contourSeq.getItem(contourDICOMIndexForUniqueSlices[idxUniqueSlice][iContour], contourItem); //TODO:: double check make sure we are choosing the right contour
            contourDataLoadStatus = contourItem->getContourData(contourData);
            logstream << class_member << "Index of Contour for unique slice  "<< idxUniqueSlice << " in DICOM Contour Seq. = " << contourDICOMIndexForUniqueSlices[idxUniqueSlice][iContour] << " :" <<std::endl;
            Point P;
            P.x = 0.0;
            P.y = 0.0;
            std::vector<Point> polygon(contourData.size() / 3, P);

            int cnt = 0;
            for (auto p = contourData.begin(); p != contourData.end(); p = p + 3)
            {
                // x --> *p
                // y--> *(p + 1)
                // z --> *(p + 2)
                // xVecContour / xVec Array
                // yVecContour
                polygon[cnt].x = *(p);
                polygon[cnt].y = *(p + 1);
                cnt++;
            }


            //inPolygonPtIndicesOnThisUniqueSlice[iContour] = inPolygonIndices(meshgridPts, polygon);
            inPolygonPtIndicesOnThisUniqueSlice.push_back(inPolygonIndices2(meshgridPts, polygon));

            // To test :
            //cout << "Points in Contour number " << contourDICOMIndexForUniqueSlices[idxUniqueSlice][iContour] << " : " << endl;
            //		for (int iPts = 0; iPts < inPolygonPtIndicesOnThisUniqueSlice[iContour].size(); iPts++)
            //	{
            //			auto idx = inPolygonPtIndicesOnThisUniqueSlice[iContour][iPts];
            //cout << " Point index = " << idx << " = (" << meshgridPts[idx].x << " , " << meshgridPts[idx].y << " )" << endl;

            //	}


        }

        point2DIndicesInsideContoursOfEachSlice.push_back(inPolygonPtIndicesOnThisUniqueSlice);

        //#######################################################################################################//
        //#######################################################################################################//
        //  end region<--fill inPolygonPtIndicesOnThisUniqueSlice


    }
    return true;


}

// find unique 3D indices of uniqueContourZValuesAndRecontructIndices
bool Roi::calculateInpolygonPoints()
{
    std::string class_member = "Roi::calcUniqueContourZValuesAndRecontructIndices:";

    auto sortedUniquePointCloudIndicesAndSortIndices = sortValuesAndSortIndices(point3DIndicesInsideROI);

    std::map<int, int> histUniquePointClouds; // <int--> index of the 3d point, int --> number of occurances>
    for (const int& key : sortedUniquePointCloudIndicesAndSortIndices.first)
    {
        ++histUniquePointClouds[key];
    }
    //	http://stackoverflow.com/questions/10038985/remove-a-key-from-a-c-map

    std::vector<int> indicesToDelete;// TODO: We don't need this just for QA
    std::vector<int> indicesToKeep;

    for (auto & p : histUniquePointClouds)
    {
        if (p.second > 1)
        {
            //histUniquePointClouds.erase(p.first); // This is a very bad IDEA don't do this ever :)
            indicesToDelete.push_back(p.first); // TODO: We don't need this just for QA
        }
        else
        {
            // make a new list of all in points
            indicesToKeep.push_back(p.first);
        }
    }
    point3DIndicesInsideROI.clear();
    point3DIndicesInsideROI = indicesToKeep;
    for (auto & iDel : indicesToDelete) // TODO: We don't need this just for QA
    {
        histUniquePointClouds.erase(iDel);
    }

    // Initialize inPolygonPoints // TODO: this is for easy debuging and comparison we are gonna get rid of inPolygonPoints due to its inefficeiy

    //    inPolygonPoints = calculatePointsInPolygonFromIndices(point3DIndicesInsideROI,
    //        gridXpos, gridYpos, gridZpos);


    calculatePointsInPolygonFromIndices(point3DIndicesInsideROI,
                                        gridXpos, gridYpos, gridZpos, inPolygonPointsX, inPolygonPointsY, inPolygonPointsZ);



    return true;
}


bool Roi::calcPoint3DIndicesInsideROI()
{
    std::string class_member = "Roi::calcPoint3DIndicesInsideROI:";

    //initialize point3DIndicesInsideContourAtGridZposlevel
    std::vector<std::vector<int>> temp5(0, std::vector<int>(0));
    point3DIndicesInsideContourAtGridZposlevel = temp5;
    //initialize point3DIndicesInsideROI
    //point3DIndicesInsideROI= temp6;

    for (int nz = 0; nz < gridZpos.size(); nz++)
    {
        //initialize point3DIndicesInsideContourAtGridZposlevel[nz]
        std::vector<int> temp(0);
        point3DIndicesInsideContourAtGridZposlevel.push_back(temp);

        auto idxCorrespUniqueContourSlice = CorrespondingUniqueContourPlaneIdxForGridZpos[nz];
        if (idxCorrespUniqueContourSlice == -1)
        {
            logstream << class_member << "Cloud points on  z= " << gridZpos[nz] << " are outside the bounding box for this ROI" << std::endl;
        }
        else  // so we will have a contour segment that contains this z-level points
        {

            // Convert the precomputed  point2DIndicesInsideContoursOfEachSlice to
            for (int ik = 0; ik < point2DIndicesInsideContoursOfEachSlice[idxCorrespUniqueContourSlice].size(); ik++)
            {
                std::vector<int> inPolyPtsIndicesSegK = point2DIndicesInsideContoursOfEachSlice[idxCorrespUniqueContourSlice][ik];



                for (int& d : inPolyPtsIndicesSegK) // Covert the local indices of grid points in contour ik to global indices
                    d += nz*gridXpos.size() * gridYpos.size();

                point3DIndicesInsideContourAtGridZposlevel[nz].insert(point3DIndicesInsideContourAtGridZposlevel[nz].end(), inPolyPtsIndicesSegK.begin(), inPolyPtsIndicesSegK.end());

                point3DIndicesInsideROI.insert(point3DIndicesInsideROI.end(), inPolyPtsIndicesSegK.begin(), inPolyPtsIndicesSegK.end());
                // std::for_each(inPolyPtsIndicesSegK.begin(), inPolyPtsIndicesSegK.end(), [nz,xsize,ysize](int& d) { d += nz*xsize*ysize; }); // This method will be ugly since we need to pass nz,gridXposROIs.size() and gridYposROIs.size() to the labmda expression


                //	point3DIndicesInsideContourAtGridZposlevel.push_back = nz*gridXposROIs.size() * gridYposROIs.size() + point2DIndicesInsideContoursOfEachSlice[idxCorrespUniqueContourSlice][k];

            }		//contourDICOMIndexForUniqueSlices[idxCorrespUniqueContourSlice].size()

        }

    } // nz loop
    return true;
}

std::vector<int> Roi::inPolygonIndices(const std::vector<Point>& pVec, const std::vector<Point>& polygon)
{
    PointInPolygon pInPolygon;
    /*for (auto it = pVec.begin(); it != pVec.end(); ++it)
    {

    pInPolygon.calculate(p, polygon);
    }*/
    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {

        if (pInPolygon.calculate(pVec[i], polygon))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}

std::vector<int> Roi::inPolygonIndices2(const std::vector<Point>& pVec, const std::vector<Point>& polygon)
{
    int nvert = polygon.size();
    std::vector<double> vertx(nvert,0);
    std::vector<double> verty(nvert,0);

    for (int iVert=0; iVert <nvert; iVert++)
    {
        vertx[iVert]= polygon[iVert].x;
        verty[iVert]= polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert,vertx, verty,pVec[i].x,pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}
bool Roi::pnpoly(int nvert, std::vector<double>& vertx, std::vector<double>& verty, double testx, double testy)
{
  int i, j;
  bool c = false;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}



std::pair<std::vector<double>, std::vector<int>> Roi::uniqueContourZValuesAndRecontructIndices(std::vector<double>& v)
{
    auto outputSortedAndIndices = sortValuesAndSortIndices(v);

    std::vector<double> vUniqueSorted(outputSortedAndIndices.first);
    vUniqueSorted.erase(std::unique(vUniqueSorted.begin(), vUniqueSorted.end(), [](double l, double r) { return std::abs(l - r) < 0.01; }), vUniqueSorted.end());
    /*std::cout << "\nUniqueSorted: " << ' ';
    for (auto var : vUniqueSorted)
    std::cout << var << ' ' << ' ';
    */

    // find indices of vUniqueSorted elements to reconstruct v  -->  vUniqueSorted(u) = outputSortedAndIndices.first
    std::vector<int> u;
    u.reserve(outputSortedAndIndices.first.size());
    std::transform(outputSortedAndIndices.first.begin(), outputSortedAndIndices.first.end(), std::back_inserter(u),
                   [&](double xp)
    {
        return (std::distance(vUniqueSorted.begin(),
                              std::lower_bound(vUniqueSorted.begin(), vUniqueSorted.end(), xp)));
    });


    std::vector<int> t(outputSortedAndIndices.first.size(),0);

    for (int ivar = 0; ivar < t.size(); ++ivar)
    {
        t[ivar]=outputSortedAndIndices.second.at(u[ivar]);

    }


    std::pair < std::vector<double>, std::vector<int> >  output;
    output.first = vUniqueSorted;
    output.second = t;
    return output;
}


std::vector<double> &splitString(const std::string &s,char delim, std::vector<double> &elems) // used in LoadPointCloudFromCSVFile
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        std::stringstream conv(item);
        double number;
        conv >> number;
        elems.push_back(number);
    }
    return elems;
}


bool Roi::LoadPointCloudFromCSVFile(std::string FilePath)
{
    try
    {
        inPolygonPointsX.clear();
        inPolygonPointsY.clear();
        inPolygonPointsZ.clear();

        std::ifstream in(FilePath.c_str(), std::ifstream::in);
        std::string line;
        std::vector< double > numbers;

        while (std::getline(in, line, '\n'))
        {
            numbers.clear();
            splitString(line, ',', numbers);
            inPolygonPointsX.push_back(numbers[0]);
            inPolygonPointsY.push_back(numbers[1]);
            inPolygonPointsZ.push_back(numbers[2]);


        }

    }
    catch (int t) // TODO: handle it properly
    {

        return false;
    }

    return true;
}
