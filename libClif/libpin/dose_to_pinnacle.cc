/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* dose_to_pinnacle.cc
   Borrowed heavily from dose_to_pinn from mcvmc
   
   Modification History:
      Dec. 7, 1998: JVS: Created
      Dec 15, 1998: JVS: Write dose grid too.
      Jan 26, 1999: JVS: Moved to library
      May 5,  1999: PJK: Adding lines to automatically update on screen
                         thanks to some Wu inspiration
      May 26, 1999: JVS: fixing PJK's lines from may 5,
                         want to change for ibeam, not beam 9 :>
      Jan 20/21, 2000: JVS: Hack in ability to write dose back to pinnacle externally
                         (send_pinnacle_script needs to be moved elsewhere)
                         Also need to get correct path for pinnacle_load_script
      Feb 1, 2000: JVS: Pass -display via the rsh command! Now can send scripts to Pinnacle from Linux
      May 31, 2000: JVS: Split off LoadPinnacleScript
                         Add doseToPinnacle() that uses trial name....
      June 15:2000: PJK: Changed %f to %.3f when writing back dose grid to Pinnacle 
      Sept 18, 2000: JVS: add UVA_USER to dens_pinnacle_script
      Oct 4, 2000: JVS: Add warning about dose_to_pinn!!!!
      April 2, 2001: JVS: Add output of child PID when runs send_pinnacle_script
      April 25, 2002: JVS: Break off Pinnacle Scripting Stuff to pinnacleScript.cc
      Sept 5, 2002: JVS: Break out createPinnacleLoadDoseScript
      Jan 8, 2004: JVS: Modify how writes Pinnacle Script...
                        Theory that nesting in { }; is what causes 
                        plans to fail to load.....
                        _And_ modify so uses CreateVersion (which is defined) since
                        sometimes WriteVersion is an empty string
                        Ideally, would use PlanInfo....PinnacleVersionDescription, but don't know how to
                        extract it
      Feb 13, 2004: JVS: Use AppVersionAndRevision via a store string...this should set
                         to current version (rather than create or write version)
      Feb 17, 2004: JVS: Change to AppVersion
      June 7, 2005: JVS: Change order to write VoxelSize first in createPinnacleLoadDoseScript
                         ...Ran into very rare with "bug" with JKG that, within a script, resetting the
                         voxelSize after changing dimensions reset the number of voxels....This happens
                         in interactive mode, but never before in non-interactive mode
      April 4, 2006: JVS: Get rid of toggling of dose engine to invalidate the dose. Replace with changing status
                          TrialList.#"$trialName".BeamList.#"*".DoseStatus = "Uncomputed";
      June 11, 2007: JVS: Add echo out of name of beam that is being loaded so it appears in Pinnacle Calling window (and can see if script was run)
      Mar 17, 2010: JVS: routines so can write all beams to one script
      April 24, 2010: writeLoadAllBeams set to work when beam name lengths differ
      May 16, 2012: JVS: Remove unused LocalPinnWindowID
      Feb 11, 2013: JVS: Add SKIP_LOAD so can use with dtransform
*/
/* *************************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h> // for strcmp


#include <sys/types.h> /* for fork */
#include <sys/syscall.h>
#include <unistd.h>    /* for fork */
#include <wait.h>      /* for wait */
#include <errno.h>

#include "utilities.h"
#include "dose_region.h"
#include "myPinnacle.h"
#include "myPinnComm.h"
#include "pinnacleScript.h"
/* **************************************************************************** */
int dose_to_pinnacle(char *name, volume_region_type *dose_region, int ibeam) // , long LocalPinnWindowID)
{
   /* get current directory */
   char *pwdpath; 
   pwdpath = getenv("PWD");
   if(pwdpath == NULL )
   {
      perror("pwd");
      return(FAIL);
   }
   char dosefile[MAX_STR_LEN];
   sprintf(dosefile,"%s/dose_%s.bin",pwdpath,name);
   if( dose_to_pinn( name, dosefile, dose_region, ibeam) != OK)
     //if( dose_to_pinn( name, dosefile, dose_region, ibeam, LocalPinnWindowID) != OK)
   {
      eprintf("\n ERROR: Writing dose to Pinnacle"); return(FAIL);
   }
   return(OK);
}
/* *********************************************************************************************** */
int doseToPinnacleWithType(const char *calcType, char *TrialName,  char *BeamName,  char *doseFile,  volume_region_type *dose_region)
{
   /* Create Script File Name for Pinnacle */
   char scriptFname[MAX_STR_LEN];
   sprintf(scriptFname,"%s.%s%s.Script",calcType, TrialName,BeamName);
   clean_name(scriptFname);
   return(doseToPinnacle(scriptFname, TrialName, BeamName, doseFile, dose_region));
}
/* *********************************************************************************************** */
int doseToPinnacle(char *TrialName, char *BeamName, char *doseFile, volume_region_type *dose_region)
{
   /* Create Script File Name for Pinnacle */
   char scriptFname[MAX_STR_LEN];
   sprintf(scriptFname,"%s%s.Script",TrialName,BeamName);
   clean_name(scriptFname);
   return(doseToPinnacle(scriptFname, TrialName, BeamName, doseFile, dose_region));
}
/* *********************************************************************************************** */
// March 17, 2010: JVS:
// Need version that creates script to load all available beams
// This could be done by appending the beam info to the file
// and by breaking out the dose grid part from the beam list part
//
int writePinnacleDoseRegionToScript(const char *scriptFname, const char *TrialName, 
                                  const volume_region_type *dose_region, const char *fileMode)
{
#ifdef DEBUG_PINNACLE
  printf("\nwritePinnacleDoseRegionToScript: Writing Script file %s\n",scriptFname);
#endif
  // fileMode = "w" or "a", for write or append
  if(strcmp(fileMode,"w") != 0 && strcmp(fileMode,"a") != 0) {
    printf("\n ERROR: writePinnacleDoseRegionScript: fileMode = %s is invalid\n",fileMode); return(FAIL);
  }
  FILE *pstrm = fopen(scriptFname, fileMode);
  if (pstrm == NULL){
      printf("\n ERROR: Can't open pinnacle load dose script file %s \n", scriptFname);
      char cwd[1024];
      if (getcwd(cwd, sizeof(cwd)) != NULL)
	printf("\n\tCurrent working dir: %s\n", cwd);
      else
	perror("ERROR: Cannot determine current directory using getcwd(), error ="); 
      return(FAIL);
  }

  // June 11, 2007: Add echo out so appears in Pinnacle Calling window (and can see if script was run)
  fprintf(pstrm,"Echo = \"Loading dose for trial %s\";\n",TrialName);
  
  // JVS: Set voxel size first since Pinnacle sometimes (once in 8 years) will reset the
  //      Dimensions when VoxelSize is changed via a script....This change occurs when NOT scripting,
  //      but now, this dimension change seems to occur during scripting too.....
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.VoxelSize.X = %.3f;\n",TrialName,dose_region->voxel_size.x);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.VoxelSize.Y = %.3f;\n",TrialName,dose_region->voxel_size.y);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.VoxelSize.Z = %.3f;\n",TrialName,dose_region->voxel_size.z);
  
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Dimension.X = %d;\n",TrialName,dose_region->n.x);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Dimension.Y = %d;\n",TrialName,dose_region->n.y);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Dimension.Z = %d;\n",TrialName,dose_region->n.z);

  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Origin.X = %.3f;\n",TrialName,dose_region->origin.x);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Origin.Y = %.3f;\n",TrialName,dose_region->origin.y);
  fprintf(pstrm,"TrialList.#\"%s\".DoseGrid.Origin.Z = %.3f;\n",TrialName,dose_region->origin.z);

  fclose(pstrm); 
  return(OK);
}
/* *********************************************************************************************** */
int writePinnacleBeamDoseToScript(const char *scriptFname, const char *TrialName, const char *BeamName,
                                  const char *dosefile,  const char *fileMode)
{
  // Allows appending so can make one script that loads all dose files
  // fileMode = "w" or "a", for write or append
  if(strcmp(fileMode,"w") != 0 && strcmp(fileMode,"a") != 0) {
    printf("\n ERROR: writePinnacleDoseRegionScript: fileMode = %s is invalid\n",fileMode); return(FAIL);
  }
  FILE *pstrm = fopen(scriptFname, fileMode);
  if (pstrm == NULL){
      printf("\n ERROR: Can't open pinnacle load dose script file %s \n", scriptFname);
      char cwd[1024];
      if (getcwd(cwd, sizeof(cwd)) != NULL)
	printf("\n\tCurrent working dir: %s\n", cwd);
      else
	perror("ERROR: Cannot determine current directory using getcwd(), error ="); 
      return(FAIL);
  }
  fprintf(pstrm,"Echo = \"Loading dose for trial %s beam %s\";\n",TrialName,BeamName);

  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseStatus = \"Uncomputed\";\n",TrialName,BeamName);  // Invalidate the dose....
  // fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseEngine.TypeName = \"Fast Convolve\";\n",TrialName,BeamName);
  // fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseEngine.TypeName = \"Adaptive Convolve\";\n",TrialName,BeamName);
  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseVolume = \\BOB{B}:%s\\;\n",TrialName,BeamName,dosefile);
  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".MonitorUnitsValid = 1;\n",TrialName,BeamName);
#ifdef DEBUG_DOSE
  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseStatus;\n",TrialName,BeamName);
#endif
  // 8/21/01: JVS: Set version same as the Pinnacle Write Version
  // fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".ComputationVersion = TrialList.#\"%s\".ObjectVersion.CreateVersion;\n", TrialName, BeamName, TrialName);
  fprintf(pstrm,"Store.At.CurrentVersionNumber = SimpleString {};\n");
  fprintf(pstrm,"Store.At.CurrentVersionNumber.AppendString = \"Pinnacle v\";\n");
  // fprintf(pstrm,"Store.At.CurrentVersionNumber.AppendString = AppVersionAndRevision;\n");
  // AppVersionAndRevision has a space at the end of it (problem after save plan), AppVersion does not!!
  fprintf(pstrm,"Store.At.CurrentVersionNumber.AppendString = AppVersion;\n");
  // fprintf(pstrm,"Store.At.CurrentVersionNumber.AppendString = \"\"\";\n");
  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".ComputationVersion = Store.At.CurrentVersionNumber.String;\n",TrialName,BeamName);
  fprintf(pstrm,"Store.FreeAt.CurrentVersionNumber = \"\";\n");
#ifdef DEBUG_DOSE
  fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".DoseStatus;\n",TrialName,BeamName);
#endif
  fprintf(pstrm,"Echo = \"Completed loading dose for trial %s beam %s\";\n",TrialName,BeamName);
  
  fclose(pstrm); 
  return(OK);  
}
/* *********************************************************************************************** */
int writeLoadAllBeamsPinnacleScript(const char *scriptFileName, const char *trialName, const char *beamName, 
                                    const char *doseFileName, const volume_region_type *doseRegion)
{
   // Add in writing one script file for all beams
   static int nBeamsWritten=0;
   if( 0==nBeamsWritten &&  OK != writePinnacleDoseRegionToScript(scriptFileName, trialName, doseRegion, "w") ) {
      printf("\n ERROR: writeLoadAllBeamsPinnacleScript::writePinnacleDoseRegionToScript "); return(FAIL);
   }
   // Write out the beam section
   if(OK != writePinnacleBeamDoseToScript(scriptFileName, trialName, beamName, doseFileName,"a") ) {
     printf("\n ERROR: writeLoadAllBeamsPinnacleScript::writePinnacleDoseRegionToScript "); return(FAIL);
   }
   nBeamsWritten++;
   return(OK);
}
/* *********************************************************************************************** */
int writeLoadAllBeamsPinnacleScript(const char *scriptFileName, const char *ptFolder, const char *trialName, const char *beamName)
{
  static int nBeamsWritten=0;
  //
   // Create name of script that would load that beam
   char beamScriptFname[MAX_STR_LEN];
   sprintf(beamScriptFname,"%s/%s%s.Script",ptFolder,trialName,beamName);
   // append it to the file
   char fileMode[MAX_STR_LEN];
   strcpy(fileMode,"a");   // Default file mode
   if( 0==nBeamsWritten ) {
       strcpy(fileMode,"w");   // open file for write on first pass
   } 
   FILE *pstrm = fopen(scriptFileName, fileMode);
   if (pstrm == NULL){
     printf("\n ERROR: Can't open pinnacle load dose script file %s \n", scriptFileName);
     char cwd[1024];
     if (getcwd(cwd, sizeof(cwd)) != NULL)
       printf("\n\tCurrent working dir: %s\n", cwd);
     else
       perror("ERRPR: Cannot determine current directory using getcwd(), error ="); 
     return(FAIL);
   }
   // Script.ExecuteNow sometimes is not recognized--LoadNoCheckSum always seems to work.
   // fprintf(pstrm,"Script.ExecuteNow = \"%s\";\n",beamScriptFname);
   fprintf(pstrm,"LoadNoCheckSum = \"%s\";\n",beamScriptFname);
  
   nBeamsWritten++;
   fclose(pstrm);    
   return(OK);
}
/* *********************************************************************************************** */
int writeLoadAllBeamsPinnacleScript(const char *beamScriptFileName, const char *compositeScriptFileName)
{
  static int nBeamsWritten=0;
  //
  // append it to the file
  char fileMode[MAX_STR_LEN];
  strcpy(fileMode,"a");   // Default file mode
  if( 0==nBeamsWritten ) {
    strcpy(fileMode,"w");   // open file for write on first pass
  } 
  FILE *pstrm = fopen(compositeScriptFileName, fileMode);
  if (pstrm == NULL){
    printf("\n ERROR: Cannot open file %s \n", compositeScriptFileName);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
      printf("\n\tCurrent working dir: %s\n", cwd);
    else
      perror("ERRPR: Cannot determine current directory using getcwd(), error ="); 
    return(FAIL);
  }
  // Script.ExecuteNow sometimes is not recognized--LoadNoCheckSum always seems to work.
  // fprintf(pstrm,"Script.ExecuteNow = \"%s\";\n",beamScriptFname);
  fprintf(pstrm,"LoadNoCheckSum = \"%s\";\n",beamScriptFileName);
  
  nBeamsWritten++;
  fclose(pstrm);
  
  return(OK);
}
/* *********************************************************************************************** */
int createPinnacleLoadDoseScript(const char *scriptFname, const char *TrialName, const char *BeamName, const char *dosefile, const volume_region_type *dose_region)
{
  const char *functionName="createPinnacleLoadDoseScript";
  /* Write Script File for Pinnacle */
#ifdef DEBUG_PINNACLE
  printf("\n %s: Pinnacle Script file name is %s", functionName, scriptFname);
#endif
  // Write the dose region section
  if(OK != writePinnacleDoseRegionToScript(scriptFname, TrialName, dose_region, "w") ) {
    printf("\n ERROR: %s ::writePinnacleDoseRegionToScript ",functionName); return(FAIL);
  }
  // Write out the beam section
  if(OK != writePinnacleBeamDoseToScript(scriptFname, TrialName, BeamName, dosefile, "a") ) {
    printf("\n ERROR: %s : writePinnacleBeamDoseToScript ",functionName); return(FAIL);
  }
  return(OK);
}
/* *********************************************************************************************** */
int OriginalcreatePinnacleLoadDoseScript(char *scriptFname, char *TrialName, char *BeamName, char *dosefile, volume_region_type *dose_region)
{
  /* Write Script File for Pinnacle */
#ifdef DEBUG_PINNACLE
  printf("\n Pinnacle Script file name is %s", scriptFname);
#endif
   FILE *pstrm = fopen(scriptFname,"w");
   if (pstrm == NULL)
   {
      eprintf("\n ERROR: Can't open pinnacle load dose script file %s! \n", scriptFname);
      return(FAIL);
   }
#ifdef DEBUG_PINNACLE
   printf("Opened file %s\n",scriptFname);
#endif

   fprintf(pstrm,"TrialList.#\"%s\" = {\n", TrialName);
#ifndef SKIP_DOSE_GRID
   fprintf(pstrm,"   DoseGrid = {\n");
   fprintf(pstrm,"               Dimension.X = %d;\n",dose_region->n.x);
   fprintf(pstrm,"               Dimension.Y = %d;\n",dose_region->n.y);
   fprintf(pstrm,"               Dimension.Z = %d;\n",dose_region->n.z);
   fprintf(pstrm,"               VoxelSize.X = %.3f;\n",dose_region->voxel_size.x);
   fprintf(pstrm,"               VoxelSize.Y = %.3f;\n",dose_region->voxel_size.y);
   fprintf(pstrm,"               VoxelSize.Z = %.3f;\n",dose_region->voxel_size.z);
   fprintf(pstrm,"               Origin.X = %.3f;\n",dose_region->origin.x);
   fprintf(pstrm,"               Origin.Y = %.3f;\n",dose_region->origin.y);
   fprintf(pstrm,"               Origin.Z = %.3f;\n",dose_region->origin.z);
   fprintf(pstrm,"              };\n");
#endif
   fprintf(pstrm,"   BeamList.#\"%s\" = {\n",BeamName);
   fprintf(pstrm,"               DoseEngine.TypeName = \"Fast Convolve\";\n");
   fprintf(pstrm,"               DoseEngine.TypeName = \"Adaptive Convolve\";\n");
   fprintf(pstrm,"               DoseVolume = \\BOB{B}:%s\\;\n",dosefile);
   fprintf(pstrm,"               MonitorUnitsValid = 1;\n");
   fprintf(pstrm,"              };\n");
   fprintf(pstrm,"};\n");
   // 8/21/01: JVS: Set version same as the Pinnacle Write Version
   fprintf(pstrm,"TrialList.#\"%s\".BeamList.#\"%s\".ComputationVersion = TrialList.#\"%s\".ObjectVersion.WriteVersion;\n", TrialName, BeamName, TrialName);
   fclose(pstrm); 
   return(OK);
}
/* *********************************************************************************************** */
int doseToPinnacle(char *scriptFname, char *TrialName, char *BeamName, char *dosefile, volume_region_type *dose_region)
{
   if(createPinnacleLoadDoseScript(scriptFname,TrialName,BeamName, dosefile, dose_region)!= OK) 
   {
     printf("\n ERROR: createPinnacleLoadDoseScript for %s, %s, %s", scriptFname,TrialName,BeamName);
   }
#ifndef SKIP_LOAD
   if(LoadPinnacleScript(scriptFname) != OK)
   {
      printf("\n ERROR: Loading Pinnacle Script"); return(FAIL);
   }
#else
   printf("\n SKIP_LOAD is set.   Must manually load %s", scriptFname);
#endif
   return(OK);
}
/* ************************************************************************************************** */
int dose_to_pinn(char *name, char *dosefile, volume_region_type *dose_region, int ibeam)
  //, long LocalPinnWindowID)
{
   /* Write Script File for Pinnacle */
   FILE *pstrm;

   printf("\n \7\7\7 **************************************************************");
   printf("\n ********** DANGER --dose_to_pinn uses .Current in its scripts");
   printf("\n ********** Should use doseToPinnacle instead  ");
   printf("\n **************************************************************");
   char script[MAX_STR_LEN]; 
   sprintf(script,"%s.Script",name);
   char script_fname[MAX_STR_LEN];
   clean_name((const char*) script,script_fname);
#ifdef DEBUG_PINNACLE
  printf("\n Pinnacle Script file name is %s", script_fname);
#endif
   pstrm = fopen(script_fname,"w");
   if (pstrm == NULL){
      printf("\n ERROR: Can't open pinnacle load dose script file %s \n", script_fname);
      char cwd[1024];
      if (getcwd(cwd, sizeof(cwd)) != NULL)
	printf("\n\tCurrent working dir: %s\n", cwd);
      else
	perror("ERROR: Cannot determine current directory using getcwd(), error ="); 
      return(FAIL);
   }
#ifdef DEBUG_PINNACLE
   printf("Opened file %s\n",script_fname);
#endif

#ifndef SKIP_DOSE_GRID
   fprintf(pstrm,"        TrialList.Current.DoseGrid = {\n");
   fprintf(pstrm,"                Dimension.X = %d;\n",dose_region->n.x);
   fprintf(pstrm,"                Dimension.Y = %d;\n",dose_region->n.y);
   fprintf(pstrm,"                Dimension.Z = %d;\n",dose_region->n.z);
   fprintf(pstrm,"                VoxelSize.X = %.3f;\n",dose_region->voxel_size.x);
   fprintf(pstrm,"                VoxelSize.Y = %.3f;\n",dose_region->voxel_size.y);
   fprintf(pstrm,"                VoxelSize.Z = %.3f;\n",dose_region->voxel_size.z);
   fprintf(pstrm,"                Origin.X = %.3f;\n",dose_region->origin.x);
   fprintf(pstrm,"                Origin.Y = %.3f;\n",dose_region->origin.y);
   fprintf(pstrm,"                Origin.Z = %.3f;\n",dose_region->origin.z);
   fprintf(pstrm,"        };\n");
#endif

   fprintf(pstrm,"TrialList.Current.BeamList.#\"#%d\".DoseEngine.TypeName = \"Fast Convolve\";\n",ibeam);
   fprintf(pstrm,"TrialList.Current.BeamList.#\"#%d\".DoseEngine.TypeName = \"Adaptive Convolve\";\n",ibeam);
 
   fprintf(pstrm,"TrialList.Current.BeamList.#\"#%d\".DoseVolume = \\BOB{B}:%s\\;\n",ibeam,dosefile);
   fprintf(pstrm,"TrialList.Current.BeamList.#\"#%d\".MonitorUnitsValid = 1;\n",ibeam);
   fclose(pstrm); 
   /* Write Dose to Pinnacle DIRECTLY if Pinnacle is Running */
#ifndef SKIP_LOAD
   if(LoadPinnacleScript(script_fname) != OK)
   {
      printf("\n ERROR: Loading Pinnacle Script"); return(FAIL);
   }
#else
   printf("\n SKIP_LOAD is set.   Must manually load %s", script_fname);
#endif


#ifdef DEBUG_PINNACLE
   printf("\n dose_to_pinn finished \n");
#endif
  return(OK);
   
}
/* ***************************************************************************** */
