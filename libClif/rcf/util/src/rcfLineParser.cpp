
#include "rcfLineParser.hpp"
#include <string.h>
#include <stdio.h>
#include <iostream>


void rcfLineParser::basicInit()
{
   
   tkn_head.clear();
   ready = false;
   delim_set = false;
   head_set = false;
   delim.clear();
   full = false;
   curtokens = 0;
   ntokens = 0;
   func = NULL;
   keepSpacesFlag = false;
}

rcfLineParser::rcfLineParser()
{
   basicInit();
}


rcfLineParser::rcfLineParser(unsigned int utokens)
{
   func = (char*)"rcfLineParser::rcfLineParser";
   basicInit();
   // set limit on the number of tokens
   ntokens = utokens;

}

rcfLineParser::rcfLineParser(char *del_str)
{
   func = (char*)"rcfLineParser::rcfLineParser";
   // create head
   basicInit();
   if( NULL != del_str)
   {
      int length = strlen(del_str);
      if(length > 0)
      {
         delim = del_str;
         delim_set = true;
      }
   }
}
rcfLineParser::rcfLineParser(unsigned int utokens, char *del_str)
{
   func = (char*)"rcfLineParser::rcfLineParser";
   // create head
   basicInit();

   if( utokens > 0) 
   {
      ntokens = utokens;
      head_set = true;
   }

   if( NULL != del_str)
   {
      int length = strlen(del_str);
      if(length > 0)
      {
         delim = del_str;
         delim_set = true;
      }
   }

   if( (true==delim_set) && (true == head_set) )ready = true;
}

rcfLineParser::~rcfLineParser()
{
}

int rcfLineParser::setDelimiter(char *del_str)
{
   func = (char*)"rcfLineParser::setDelimiter";
   ready = false;
   delim_set = false;

   if( NULL != del_str)
   {
      int length = strlen(del_str);
      if(length > 0)
      {
         delim = del_str;
         delim_set = true;
      }
      else
      {
         reportError("Empty string passed as delimiter");
         return(CF_FAIL);
      }
   }
   else 
   {
      reportError("NULL string passed as delimiter");
      return(CF_FAIL);
   }


   if( (true==delim_set) && (true == head_set) )ready = true;
   return(CF_OK);
}

int rcfLineParser::setTokens(unsigned int utokens)
{
   func = (char*)"rcfLineParser::setTokens";
   if(tkn_head.size() > 0)tkn_head.clear();
   
   ready = false;
   head_set = false;

   if( utokens > 0)
   {
      ntokens = utokens;
      head_set = true;
   }
   if( (true==delim_set) && (true == head_set) )ready = true;
   return(CF_OK);
}

int rcfLineParser::parseLine(char *line)
{
   func = (char*)"rcfLineParser::parseLine";
   clearLine();

   if(false == ready)
   {
      reportError("Attempt to parse line, while the object is not in a ready state");
      return(CF_FAIL);
   }
   if(NULL == line)
   {
      reportError("Invalid string passed in as string to parse");
      return(CF_FAIL);
   }
   int length = strlen(line);
   if(length <= 0)
   {
      reportError("Empty string passed in as string to parse");
      return(CF_FAIL);
   }

   return(tokenizeLine(std::string(line)));
}
int rcfLineParser::parseLine(std::string line)
{
   return(tokenizeLine(line));
}

int rcfLineParser::tokenizeLine(std::string lline)
{
   func = (char*)"rcfLineParser::tokenizeLine_str";
   size_t found= 0;
   std::vector<size_t> indices;
   size_t low,length;
   int skip = delim.size(); 
   int cntr = 0;
   std::string result;

   // sanity checks
   if(lline.size() <= 0 )
   {
      reportError("Empty string passed in lieu of input data");
      return(CF_FAIL);
   }
   if( (false==delim_set) || (false==head_set) || (false == ready) )
   {
      reportError("Line can not be parsed until the object is initialized");
      return(CF_FAIL);
   }

   if(full)clearLine();

   // object initialized and input seems valid, proceed to parse
   do
   {
      found = lline.find(delim,found);
      if(found != std::string::npos)
      {
         indices.push_back(found);
         found++;
         cntr++;
      }
      else break;
   }while(cntr<ntokens);

   int nind = indices.size();
   if(nind <= 0)
   {
      reportError("Tokenization failed, no tokens found");
      return(CF_FAIL);
   }
   for(int i=0; i<nind; i++)
   {
      if(0 == i)
      {
         low = 0;
         length = indices[0];
         if(0 == length)continue;
      }
      else
      {
         low = indices[i-1]+skip;
         length = indices[i] - low;
      }
      result = lline.substr(low,length);
      if(!keepSpacesFlag)removeSpaces(result);
      tkn_head.push_back(result);

   }
   if(indices[nind-1] != (lline.size() - skip) )
   {
      low = indices[nind-1]+skip;
      length = lline.size() - low;
      result = lline.substr(low,length);
      removeSpaces(result);
      tkn_head.push_back(result);
   }

   if( tkn_head.size() > 0 )
   {
      full = true;
      curtokens = tkn_head.size();
   }
   else
   {
      full = false;
   }
   return(CF_OK);
}

int rcfLineParser::getNumberOfTokens(int *utokens)
{
   func = (char*)"rcfLineParser::getNumberOfTokens";
   if( (false==ready) || (false == full) )
   {
      *utokens = 0;
      reportError("User inquired about the number of tokens, but the module is either not full or not ready");
      return(CF_FAIL);
   }
   *utokens = curtokens;
   return(CF_OK);
}

int rcfLineParser::getToken(int index, char** usr)
{
   func = (char*)"rcfLineParser::getToken";
   if( (false == ready) || (false == full) || (index >= curtokens) || (index<0) )
   {
      *usr = NULL;
      if( false == ready) reportError("User requested a token, but the module is not ready");
      if( false == full) reportError("User requested a token, but no line has been parsed");
      if( index > curtokens) reportError("User requested a token with index greater than the range of valid tokens");
      if( index < 0) reportError("User requested a token with negative index ");
      return(CF_FAIL);
   }
   *usr = (char*)((tkn_head[index]).c_str());
   return(CF_OK);
}

std::string rcfLineParser::getToken(int index, int *valid)
{
   func = (char*)"rcfLineParser::getToken_str";
   std::string nothing;
   nothing.clear();
   *valid = 0;
   if( (false == ready) || (false == full) || (index >= curtokens) || (index<0) )
   {
      if( false == ready) reportError("User requested a token, but the module is not ready");
      if( false == full) reportError("User requested a token, but no line has been parsed");
      if( index > curtokens) reportError("User requested a token with index greater than the range of valid tokens");
      if( index < 0) reportError("User requested a token with negative index ");
      return(nothing);
   }
   *valid = 1;
   return(tkn_head[index]);
}

void rcfLineParser::clearLine()
{
   if( (false == ready) || (false == full) )return; 
   if(tkn_head.size() <= 0)return;
   tkn_head.clear();
   full = false;
}

void rcfLineParser::removeSpaces(std::string & ustr)
{
   std::string sep = " ";
   size_t found = 0;

   do
   {
      found = ustr.find(sep,found);
      if(found != std::string::npos) ustr.erase(found,1);
      else break;
   }while(1);

   sep = "\n";
   found = 0;
   do
   {
      found = ustr.find(sep,found);
      if(found != std::string::npos) ustr.erase(found,1);
      else break;
   }while(1);
}

void rcfLineParser::reportError(const char *str)
{
   fprintf(stderr,"%s reports that %s\n",func,str);
}
void rcfLineParser::keepSpaces()
{
   keepSpacesFlag = true;
}
