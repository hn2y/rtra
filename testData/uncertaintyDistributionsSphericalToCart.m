close all force
clear variables
clc
numSamples=1e5;

chiPdf = @(x,k) x.^(k-1).*exp(-x.^2/2)/2^(k/2-1)/gamma(k/2);
chiCdf =@ (x,k) gammainc(k/2,x.^2/2,'upper') / gamma(k/2);
chiSquaredPdf = @(x,k) x.^(k/2-1).*exp(-x.^2/2)/2^(k/2)/gamma(k/2);
edge2binCenter = @(edges) edges(1:end-1)+ diff(edges)/2;
gaussPdf = @(x,mu,sigma) 1/sqrt(2*pi)/sigma*exp(-(x-mu).^2/2/sigma^2);
subStd=5.7;
myPDF= @(x) exp(-x/(subStd/5));

xSamplesNormal= subStd/sqrt(3)*randn(1,numSamples);
r=subStd* rand(1,numSamples);
% r=sampleDist(myPDF,2,numSamples,[0 subStd],true)';
% r = 2*subStd*abs(randn(1,numSamples));
azi= 2*pi*rand(1,numSamples)-pi;

ele = sampleDist(@(x)0.5*cos(x),...
    0.5,numSamples,[-pi/2,pi/2],true)';
% ele = 2*pi*rand(1,numSamples)-pi;
edges= linspace(min(r)-.1,max(r)+.1,60);
[Nr,edges] = histcounts(r,edges);
xDatar=edge2binCenter(edges)';
% yData= N'/trapz(xData,N);
[Nazi,edgesAzi] = histcounts(azi,linspace(-pi-.2,pi+.2,50));
xDataAzi=edge2binCenter(edgesAzi)';

[Nele,edgesEle] = histcounts(ele,linspace(-pi/2-.2,pi/2+.2,50));
xDataEle=edge2binCenter(edgesEle)';


[x,y,z]=sph2cart(azi,ele,r);

[Nx,edgesX] = histcounts(x,linspace(-2*subStd,2*subStd,50));
xDataX=edge2binCenter(edgesX)';

[Ny,edgesY] = histcounts(y,linspace(-2*subStd,2*subStd,50));
xDataY=edge2binCenter(edgesY)';

[Nz,edgesZ] = histcounts(z,linspace(-2*subStd,2*subStd,50));
xDataZ=edge2binCenter(edgesZ)';


subplot(2,3,1)
% histogram(r)
plot(xDatar,Nr);

subplot(2,3,2)
% histogram(azi)
plot(xDataAzi,Nazi)

subplot(2,3,3)
% histogram(ele)
plot(xDataEle,Nele)

subplot(2,3,4)
% histogram(x)
plot(xDataX,Nx)
hold on
[NxSampleNormal,edgesX] = histcounts(xSamplesNormal,linspace(-2*subStd,2*subStd,50));
plot(xDataX,NxSampleNormal);
% gausDist= gaussPdf(xDataX,0,subStd/sqrt(3));
% plot(xDataX,gausDist/max(gausDist)*max(Nx))


subplot(2,3,5)
% histogram(y)
plot(xDataY,Ny)
subplot(2,3,6)
% histogram(z)
plot(xDataZ,Nz)

figure
plot3(x, y, z,'.')
axis equal

% comparing dx distribution due to uniform magitude sampling,
% resulted in less conservative uncertainty when compared to dx distribution 
% drawn from gaussian with std/sqrt(3).
% This tells that it is safe to use gaussian distribution while keeping in mind that this approximation
% is overestimating the situation of uniform magnnitude sampling.
