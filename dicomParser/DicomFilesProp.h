#ifndef DICOMFILESPROP_H
#define DICOMFILESPROP_H
/*!
 *  \brief     The DicomFilesProp class has some helper methods to extract information such as  Patient,Study,Connection, ...  about the DICOM files in a specified folder.
 *  \details   This class is used to demonstrate a number of section commands.
 *  \author    Hamidreza Nourzadeh
 *  \version   1.0
 *  \date      Feb, 2016
 *  \pre       DicomFilesProp class requires DCMTK library.
 *  \bug       Not Known Yet.
 *  \warning   TODO.
 *  \copyright TODO.
 */
#include <iostream>
#include <string>
#include <vector>
#include<QList>


#include <sys/types.h> // to check if directory exists
#include <sys/stat.h>  // to check if directory exists  relies on types


// The DCMTK headers
#ifdef UNICODE
#undef UNICODE
#endif


#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmrt/drtdose.h"
#include "dcmtk/dcmrt/drtimage.h"
#include "dcmtk/dcmrt/drtplan.h"
#include "dcmtk/dcmrt/drtstrct.h"
#include "dicomParser/patientstruct.h"
//#include "dcmtk/dcmdata/dcfilefo.h"
//#include "dcmtk/dcmrt/drmstrct.h"
//#include "dcmtk/dcmrt/drttreat.h"



class DicomFilesProp
{

private:
    //!
    //! \brief dicomFolderPath will hold the base path for the DICOM folder
    //!
    std::string dicomFolderPath;

public:
    //!
    //! \brief DicomFilesProp constructore
    //!
    DicomFilesProp();


    //!
    //! \brief DicomFilesProp constructore
    //! \param stream   : output stream
    //!
    DicomFilesProp( std::ostream &stream);


   std::vector <DicomPatient> PatientsVec;

    //enum dicomtype{ CT,DOSE,STRUCTURE,PLAN,OTHER};

    void setDicomFolderPath(std::string _dicomFolderPath);

    //!
    //! \brief dicomFilesList contains all the dicom files inside dicomFolderPath
    //!
    std::vector <std::string> dicomFilesVec;

    //!
    //! \brief modality contian type of each DICOM file in dicomFilesVec
    //!
    std::vector <std::string> modalityVec;

    //!
    //! \brief patientsIDList contains all the patients lists in the dicomFolderPath folder
    //!
    std::vector<std::string> patientsIDVec;


    //!
    //! \brief studyIDList the DICOM plan
    //!
    std::vector<std::string> studyIDVec;

    //!
    //! \brief planList all the DICOM plan files in dicomFolderPath folder, the size of the vector is equal to dicomFilesList.
    //!
    std::vector<std::string> planIDVec;


    //!
    //! \brief CTfilesInEachStudy // TODO:
    //!
    std::vector<std::vector <int>>  CTfilesInEachStudy;

    //!
    //! \brief planInEachStudy
    //!
    std::vector<std::vector<int>> planInEachStudy;


    //!
    //! \brief SOPInstanceUID InstanceUID of each dicom file in dicomFolderPath
    //!
    std::vector<std::string> SOPInstanceUID;

    //!
    //! \brief SOPClassUID of each dicom file in dicomFolderPath
    //!
    std::vector<std::string> SOPClassUID;


    //!
    //! \brief StudyInstanceUID of each dicom file in dicomFolderPath
    //!
    std::vector<std::string> StudyInstanceUID;

    //!
    //! \brief FrameOfReferenceUID of each dicom file in dicomFolderPath
    //!
    std::vector<std::string> FrameOfReferenceUID;


    //!
    //! \brief SeriesNumber of each dicom file in dicomFolderPath
    //!
    std::vector<std::string> SeriesNumberVec;



    //!
    //! \brief parseFolder  reads in the files  dicomFilesList folder and fills the relation tree
    //!
    bool parseFolder();

    bool verbose;

    void printParsedFolderInfo();



    std::ostream &logstream;













};

#endif // DICOMFILESPROP_H
