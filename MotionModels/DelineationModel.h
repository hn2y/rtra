#ifndef DELINEATIONMODEL_H
#define DELINEATIONMODEL_H
#include <iostream>
#include <QtXml>
#include <MotionModels/GumModel.h>


class DelineationModel:public GumModel
{
public:
    DelineationModel();
    DelineationModel(std::ostream  &stream);
    ~DelineationModel();
    QDomDocument *domXmlDoc;
    std::ostream  &logstream;

    // operators
    DelineationModel operator=(const DelineationModel &delineationModel);

    int verbose; // TODO : define different level for verbosity
private:

};

#endif // DELINEATIONMODEL_H
