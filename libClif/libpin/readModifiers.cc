/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* read_modifiers.cc
   Reads beam modifiers from Pinnacle
   Modification History:
      Nov. 4, 1999: JVS: Created
      April 24, 2000: JVS: Allow EXPOSE EXPOSE apertures (these are invalid apertures) needed
                           for IMRT program so can use aperture outline
      June 7, 2000: JVS: Using new readClif routines....
      June 23, 2000:PJK: Added check whether autoblocking is on 
                         (and thus block structure may not be up to date)
      Jan 31, 2003: JVS: Break out readPinnacleCompensator(Compensator, filename, compensator_type
                         so can more easily access just compensator for convolution code
      Feb 3, 2015: JVS: Move closePinnacleContour to here -- only place every used
      May 7, 2015: JVS: Allow non-zero auto-block with MLC (will use particleDMLC to transport through the MLC)
     Oct 13, 2015: JVS: Add in As Is combinations for block
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> /* for fabs */
#include "string_util.h"
#include "utilities.h"
#include "typedefs.h"
#include "defined_values.h"
#include "apertures.h"
#include "beams.h"
#include "read_clif.h"
#include "modifiers.h"
#include "lib.h" /* for definition of TRUE */
#include "myPinnacle.h"
#include "myPinnComm.h"
#include "readPinnacle.h" // prototypes for all routines
/* ****************************************************************** */
int closePinnacleContour(TwoDContourType *contour)
{
   return(closePinnacleCurve(&contour->npts, &contour->fvert));
}
/* ************************************************************************************* */
int readPinnacleAperture(clifObjectType *Aperture, aperture_type *ap)
{  // read in aperture from pinnacle (from CLIF) 
   ap->valid = 0;
   ap->n_contours = 0; // initialize to zero contours in the aperture 
   // check whether autoblocking is on (and thus block structure may not be up to date)
   int AutoBlock; 
   if( readIntClifStructure  (Aperture, "AutoBlock",  &AutoBlock) != OK ){
      printf("\n ERROR: Reading AutoBlock"); return(FAIL);
   }
   //   if (AutoBlock != 0){
   //   printf("\n ERROR: AutoBlock (currently %d) must be 0", AutoBlock); return(FAIL);
   //}

   // figure out if aperture is a "hole" or not
   char OutsideMode[MAX_STR_LEN];
   char InsideMode[MAX_STR_LEN];
  
   if(   readStringClifStructure(Aperture, "InsideMode",InsideMode)!= OK 
      || readStringClifStructure(Aperture, "OutsideMode",OutsideMode)!= OK 
     ){ 
      printf("\n ERROR: Reading Aperture Modes"); return(FAIL);
   }
   if(      (strcmp(InsideMode,"Expose") == 0) && (strcmp(OutsideMode,"Block" ) == 0)){ ap->hole = 1; ap->valid = 1;}
   else if( (strcmp(InsideMode,"Expose") == 0) && (strcmp(OutsideMode,"Expose") == 0)){ ap->hole = 2; ap->valid = 0;}
   else if( (strcmp(InsideMode,"Expose") == 0) && (strcmp(OutsideMode,"As Is" ) == 0)){ ap->hole = 2; ap->valid = 0;}

// Oct 13, 2015: JVS: Block, block results in full field being blocked -- this makes no sense to have a segment that does this
// else if( (strcmp(InsideMode,"Block" ) == 0) && (strcmp(OutsideMode,"Block" ) == 0)){ ap->hole = 0; ap->valid = 1;} 
   else if( (strcmp(InsideMode,"Block" ) == 0) && (strcmp(OutsideMode,"Expose") == 0)){ ap->hole = 0; ap->valid = 1;} 
   else if( (strcmp(InsideMode,"Block" ) == 0) && (strcmp(OutsideMode,"As Is" ) == 0)){ ap->hole = 0; ap->valid = 1;}
 
   else if( (strcmp(InsideMode,"As Is" ) == 0) && (strcmp(OutsideMode,"Block" ) == 0)){ ap->hole = 1; ap->valid = 1;}
   else if( (strcmp(InsideMode,"As Is" ) == 0) && (strcmp(OutsideMode,"As Is" ) == 0)){ ap->hole = 2; ap->valid = 0;}
   else if( (strcmp(InsideMode,"As Is" ) == 0) && (strcmp(OutsideMode,"Expose") == 0)){ ap->hole = 2; ap->valid = 0;}
   else{
      printf("\n ERROR: InsideMode = %s, OutsideMode = %s invalid",InsideMode,OutsideMode); return(FAIL);
   }
   // get to level that describes the aperture
#ifdef DEBUG_READ_BEAMS
   printf("\n   Reading Contour List for Aperture (n_contours = %d)",ap->n_contours);
#endif
   clifObjectType *ContourList;
   if(getClifAddress(Aperture,"ContourList", &ContourList)!= OK){
      printf("\n ERROR: Getting Address"); return(FAIL);
   } 
   if(readPinnacleContourList(ContourList, &ap->n_contours, &ap->contour)!=OK){
      printf("\n ERROR: reading Pinnacle RawData"); return(FAIL);
   }
   for(int iContour = 0; iContour<ap->n_contours; iContour++){
      if( closePinnacleContour( &ap->contour[iContour] ) !=OK ){
        printf("\n ERROR: closing Pinnacle Contour"); return(FAIL);
      }
   }
   return(OK);
}
/* ********************************************************************************* */
int readPinnacleCompensator(clifObjectType *Compensator, char *fileName, compensator_type *comp)
{
      // IsValid was not set in this function this has been fixed.
      char tmpString[MAX_STR_LEN];
      if(   readFloatClifStructure (Compensator, "SourceToCompensatorDistance",&comp->SourceToCompensatorDistance) != OK
         || readFloatClifStructure (Compensator, "Width",       &comp->Width) != OK
         || readFloatClifStructure (Compensator, "Height",      &comp->Height) != OK
         || readFloatClifStructure (Compensator, "Resolution",  &comp->Resolution) != OK
         || readFloatClifStructure (Compensator, "OutputFactor",&comp->OutputFactor) != OK
         || readFloatClifStructure (Compensator, "Density",     &comp->Density) != OK
         || readIntClifStructure   (Compensator, "CompensatorHangsDown", &comp->CompensatorHangsDown) != OK
         || readIntClifStructure   (Compensator, "ResampleUsingLinearInterpolation", &comp->Interpolation) != OK
         || readStringClifStructure(Compensator, "Type",      comp->Type) != OK
         || readIntClifStructure   (Compensator, "IsValid", &comp->IsValid) != OK
         || readStringClifStructure(Compensator, "Thickness", tmpString) != OK
      )
      {
         printf("\n ERROR: Reading Compensator Information"); return(FAIL);
      }
      // read thickness from file
      int idNum=0;
      if( readPinnacleXDRId(tmpString, &idNum) != OK )
      {
         printf("\n ERROR: Cannot get compensator file ID number from %s", tmpString);
         return(FAIL);
      }
      sprintf(comp->Fname,"%s.binary.%03d",fileName,idNum);
#ifdef DEBUG
      printf("\n Reading in compensator file %s", comp->Fname);
#endif
      comp->nx = (int) (comp->Width/comp->Resolution)  +1;
      comp->ny = (int) (comp->Height/comp->Resolution) +1;
      comp->thickness = read_binary(comp->Fname, comp->nx*comp->ny);
      if(comp->thickness == NULL)
      {
         printf("\n ERROR: reading compensator thickness");
         return(FAIL);
      }
      return(OK);
}
/* ********************************************************************************* */
int readPinnacleCompensator(clifObjectType *Compensator, beam_type *pbeam)
{  
   if( readIntClifStructure  (Compensator, "IsValid",  &pbeam->intensity_modulator_defined ) != OK )
   {
      printf("\n ERROR: Determining if Compensator Exists"); return(FAIL);
   }
   if( pbeam->intensity_modulator_defined )
   {
      // Allocate Memory For This
      pbeam->comp = (compensator_type *)calloc(1,sizeof(compensator_type));
      if(pbeam->comp == NULL)
      {
         eprintf("\n ERROR: allocating memory for compensator"); 
         return(FAIL);
      }
      if(readPinnacleCompensator(Compensator, pbeam->fname, pbeam->comp) != OK)
      {
	eprintf("\n ERROR: Reading Pinnacle Compensator"); return(FAIL);
      }
      // can read compensator from Pinnacle File or from the intensity.nml file
      // for reading from intensity.nml file, use qwu code
      // eprintf("\n ERROR: Compensators are not currently supported");
      //  return(FAIL);
   }
   return(OK);
}
/* ***************************************************************** */
int readPinnaclePbeamModifiers(clifObjectType *Modifier, beam_type *pbeam)
{
 if(checkIfObjectList(Modifier) != OK)
 {
   printf("\n ERROR: Invalid Object List"); return(FAIL);
 }
 /* Allocate memory for ALL modifiers at the start */
 pbeam->n_apertures = Modifier->nObjects;
 if(pbeam->n_apertures > 0 )
 {
    pbeam->aperture = (aperture_type *) calloc(pbeam->n_apertures, sizeof(aperture_type));
    if(pbeam->aperture == NULL)
    {
       printf("\n ERROR: allocating memory for %d apertures",pbeam->n_apertures);
       return(FAIL);
    }
    for(int iAp=0; iAp < pbeam->n_apertures; iAp++)
    {
       if(readPinnacleAperture(&Modifier->Object[iAp],&pbeam->aperture[iAp])!=OK)
       {
          printf("\n ERROR: Reading aperture %d", iAp+1);return(FAIL);
       }
    }
 }
 return(OK);
}
/* ********************************************************* */
int readPinnacleTrayParameters(clifObjectType *Beam, beam_type *pbeam)
{
   // then has BlockingMaskPixelSize...TrayFactor,CutoutFactor...
   if(pbeam->n_apertures)
   {
      if(   readFloatClifStructure (Beam, "BlockAndTrayFactor",  &pbeam->aperture[0].transmission) != OK 
         || readFloatClifStructure (Beam, "TrayFactor",  &pbeam->aperture[0].hole_transmission) != OK 
        )
      {
         printf("\n ERROR: reading Aperture Parameters"); return(FAIL);
      }       
      // the BlockAndTrayFactor and TrayFactor are the same for ALL of the apertures
      for(int iap=1;iap<pbeam->n_apertures; iap++)
      {
         pbeam->aperture[iap].transmission =pbeam->aperture[0].transmission;
         pbeam->aperture[iap].hole_transmission =pbeam->aperture[0].hole_transmission;
      }
   }
 return(OK);
}
/* ************************************************************************************ */
int readPinnacleCompensatorFile(char *compFileName, compensator_type *compensator)
{
  /* Read in the compensator information */
  // Open the file
  FILE *istrm = fopen(compFileName,"r");
  if(NULL==istrm)
  {
    printf("\n ERROR: Opening file %s", compFileName); return(FAIL);
  }
  // 
  #ifdef DEBUG
  printf("\n The file %s exists and is open for read access\n",compFileName);
  #endif

  // create Clif Object to read it into
  clifObjectType *CompensatorObject = new(clifObjectType);
  if( readClifFile(istrm, CompensatorObject,"CompensatorObject") != OK)
  {
      printf("\n ERROR: Reading clif from file %s\n", compFileName); return(FAIL);
  }
  fclose(istrm);
  //dumpClifStructure(CompensatorObject);
  /* Read in the compensator from Pinnacle */
  if( readPinnacleCompensator(CompensatorObject, compFileName, compensator) != OK )
  {
    printf("\n ERROR: Reading Pinnacle Compensator File\n"); return(FAIL);
  }
  /// \todo Probably should free all the things in the object first
  delete(CompensatorObject);
  return(OK);
}
/* *********************************************************************** */
/// \brief reads Pinnacle compensator from a given trial and beam using clif objects
/// \param trialName a string indicating the trial to read from
/// \param beamNum an integer indicating the number of the beam to retrieve the compensator from
/// \param compensator a pointer to the location to write the compensator to
/// \pre Pinnacle MUST be initialized before this can run.
int readPinnacleCompensatorFile(char* trialName, int beamNum, compensator_type* compensator)
{
   int oStatus = OK;
   int iStatus = 1;
   char pcMessage[256];
   //char* buffer;
   // Oct 2, 2012: JVS: Eliminate tmpName since causes compiler warning
   // based on example at http://www.cmiss.org/cmgui/wiki/TmpnamVsMkstemp
   // buffer = tmpnam(NULL);
   char tmpBufferName[MAX_STR_LEN];
   sprintf(tmpBufferName,"tmpCompensator-XXXXXX");
   //int tmp_fd=mkstemp(tmpBufferName); // 1/12/2015: JVS: Was not used

   /// Note, if Pinnacle with Save commands, you DO NOT want quotes around the file name
   /// Pinnacle will report a "ptCliffStread_CreateOnWriteFile: Failed" message, but
   /// iPinnComm_IssueRootCommand() will still return a 1 (Not a zero, like would be expected)
   sprintf(pcMessage, "TrialList.%s.BeamList.#%d.Compensator.Save=%s\n",trialName,beamNum,tmpBufferName);
   #ifdef DEBUG
   printf("\n Issuing Command %s", pcMessage);
   printf("\n pcMessageLenght = %d", strlen(pcMessage));
   #endif
   iStatus = iPinnComm_IssueRootCommand(pcMessage);
   #ifdef DEBUG
   printf("\n iStatus = %d", iStatus);
   #endif
   if (!iStatus) { printf("ERROR: Issuing command %s/n",pcMessage); oStatus=FAIL;}
   #ifdef DEBUG
   printf("\n Buffer = %s", buffer);
   #endif
   if( OK != readPinnacleCompensatorFile(tmpBufferName, compensator))
      { printf("ERROR: readPinnacleCompensator from %s\n", tmpBufferName); oStatus=FAIL;}
   return oStatus;
}
   
   
