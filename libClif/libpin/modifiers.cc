/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* modifiers.cc
   Misc Routines for beam modifiers in Pinnacle
   Modification History:
      Nov. 5, 1999: JVS: Created
      May 20, 2002: JVS: Add ScaleWidthHeight to compensatorToPinnacle
      April 21, 2004: IBM: Overload int compensator_to_pinnacle(compensator_type *c, char *trialName, char *beamName)
                           to be compatible with ibmParticleDmlc
      Nov 27, 2007: JVS: remove compiler warnings
      
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> /* for fabs */
#include "string_util.h"
#include "utilities.h"
#include "typedefs.h"
#include "defined_values.h"
#include "apertures.h"
#include "beams.h"
#include "read_clif.h"
#include "modifiers.h"
#include "lib.h" /* for definition of TRUE */
#include "myPinnacle.h"
#include "myPinnComm.h"
#include "pinnacleScript.h"

#ifndef MACHINE_SSD
#define MACHINE_SSD 100.0
#endif
/* ******************************************************************** */

int compensator_to_pinnacle(compensator_type *c, char *trialName, char *beamName)
{
   char filename[MAX_STR_LEN];

  
   char *pwdpath; 
   pwdpath = getenv("PWD");
   if(pwdpath == NULL )
   {
       perror("pwd");
       return(FAIL);
   }
   char prefix[MAX_STR_LEN];
   strcpy(prefix,"\\BOB{B}");   
   sprintf(filename,"%s.%s.loadCompensator.Script",trialName,beamName);
   FILE *strm = fopen(filename,"w");
   if(strm == NULL)
   {
      eprintf("\n ERROR: opening file %s", filename); return(FAIL);
   }
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.ResampleUsingLinearInterpolation = %d;\n",trialName,beamName,c->Interpolation);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".SetBeamType = \"%s\";\n",trialName,beamName,"Static");
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".UseMLC = %d;\n",trialName,beamName,0);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.IsValid = %d;\n",trialName,beamName,c->IsValid);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Density = %f;\n",trialName,beamName,c->Density);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Width = %f;\n",trialName,beamName,c->Width);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Height = %f;\n",trialName,beamName,c->Height);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Resolution = %f;\n",trialName,beamName,c->Resolution);
   fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Type = \"%s\";\n",trialName,beamName,c->Type);
 
   //
   // fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.IsValid = %d;\n",trialName,beamName,0);
   //   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Type = \"%s\";\n",ibeam,c->Type);
   if(c->IsValid == 1 )
   {
      fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.SourceToCompensatorDistance = %f;\n",trialName,beamName,c->SourceToCompensatorDistance);
      fprintf(strm,"TrialList.#\"%s\".BeamList.#\"%s\".Compensator.Thickness =\n %s:%s/%s\\;\n",trialName,beamName,prefix, pwdpath,c->filename);
        
//      sprintf(doseFileName, "%s/%s.Trial.binary.%03d.mc",pwdpath,plan_name, pbeam->dose_grid_id);
   }
   else 
   {
//      fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.SourceToCompensatorDistance = TrialList.Current.BeamList.#\"#%d\".Machine.SourceToBlockTrayDistance;\n",ibeam,ibeam);
       printf("\n ERROR: Compensator is invalid");
       return(FAIL);
   }
   fclose(strm);
   /* Command Pinnacle to read the Script file */
   if(LoadPinnacleScript(filename)!=OK) {
      printf ("\n ERROR: Loading Script %s",filename);return(FAIL);
   }
   return(OK);
}


int compensator_to_pinnacle(compensator_type *c, int ibeam)
{
   char filename[MAX_STR_LEN];
   sprintf(filename,"compensator%d.Script",ibeam);
   FILE *strm = fopen(filename,"w");
   if(strm == NULL)
   {
      eprintf("\n ERROR: opening file %s", filename); return(FAIL);
   }
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.IsValid = %d;\n",ibeam,c->IsValid);
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Density = %f;\n",ibeam,c->Density);
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Width = %f;\n",ibeam,c->Width);
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Height = %f;\n",ibeam,c->Height);
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Resolution = %f;\n",ibeam,c->Resolution);
   //
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.ScaleWidthHeight = %d;\n",ibeam,0);
   //   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Type = \"%s\";\n",ibeam,c->Type);
   if(c->IsValid == 1 )
   {
      fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.SourceToCompensatorDistance = %f;\n",ibeam,c->SourceToCompensatorDistance);
      fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.Thickness = \\BOB{B}:%s\\;\n",ibeam,c->filename);
   }
   else 
   {
      fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".Compensator.SourceToCompensatorDistance = TrialList.Current.BeamList.#\"#%d\".Machine.SourceToBlockTrayDistance;\n",ibeam,ibeam);
   }
   fclose(strm);
   /* Command Pinnacle to read the Script file */
   if(PinnWindowID > 0)
   {
      char *pwdpath; 
      pwdpath = getenv("PWD");
      if(pwdpath == NULL )
      {
         perror("pwd");
         return(FAIL);
      }
       char ffilename[MAX_STR_LEN];
       sprintf(ffilename,"%s/%s",pwdpath,filename); 
#ifdef DEBUG_PINNACLE
       printf("\n Executing the Script File %s", ffilename);
#endif
       int iStatus;
       char pinQuery[MAX_STR_LEN];
       strcpy(pinQuery,"LoadNoCheckSum");
       iStatus = iPinnComm_SetString(NULL, pinQuery, ffilename);
       if(!iStatus){fprintf(stderr,"\nERROR: Setting LoadNoCheckSum"); return(FAIL);}   
   }
   return(OK);
}
/* **************************************************************************** */
int reset_pinnacle_block(int ibeam, int iblock)
{
   char filename[MAX_STR_LEN];
   sprintf(filename,"resetblock%d.Script",ibeam);
   FILE *strm = fopen(filename,"w");
   if(strm == NULL)
   {
      eprintf("\n ERROR: opening file %s", filename); return(FAIL);
   }
   /* Set the block to "As Is" */
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".ModifierList.#\"#%d\".OutsideMode = \"As Is\"; \n", ibeam, iblock);
   fprintf(strm,"TrialList.Current.BeamList.#\"#%d\".ModifierList.#\"#%d\".InsideMode = \"As Is\"; \n", ibeam, iblock);
   fclose(strm);
   /* Command Pinnacle to read the Script file */
   if(PinnWindowID > 0)
   {
      char *pwdpath; 
      pwdpath = getenv("PWD");
      if(pwdpath == NULL )
      {
         perror("pwd");
         return(FAIL);
      }
       char ffilename[MAX_STR_LEN];
       sprintf(ffilename,"%s/%s",pwdpath,filename); 
#ifdef DEBUG_PINNACLE
       printf("\n Executing the Script File %s", ffilename);
#endif
       int iStatus;
       char pinQuery[MAX_STR_LEN];
       strcpy(pinQuery,"LoadNoCheckSum");
       iStatus = iPinnComm_SetString(NULL, pinQuery, ffilename);
       if(!iStatus){fprintf(stderr,"\nERROR: Setting LoadNoCheckSum"); return(FAIL);}   
   }
   return(OK);
}
/* *********************************************************************** */
int create_gnuplot_compensator(compensator_type *c, int ibeam)
{
   char filename[MAX_STR_LEN];
   sprintf(filename,"compensator_%d.gbin",ibeam);
   FILE *ostrm;
   ostrm = fopen(filename,"wb");
   if(ostrm == NULL)
   {
      eprintf("\n ERROR: Opening Output Stream");
      return(FAIL);
   }
   Point_2d_float origin;
   /* set initial coordinates of the point */
   /* may need to be reversed if orientation is incorrect */
   float mag_factor = c->SourceToCompensatorDistance/MACHINE_SSD;
   // printf("\n mag factor = %f", mag_factor);
   origin.x = -0.5*c->Width;
   origin.y = -0.5*c->Height;
   /* Output is in format as follows
      N+1 y0 y1 y2 ,,, yN 
      x0  z0 ..........zN
      x1  z1 ..........zN
      ...............
   */
   float ftmp = c->nx;
   // printf("\n %f values ", ftmp);
   fwrite(&ftmp, sizeof(float), 1, ostrm);  // N+1
   for(int j=0; j< c->nx; j++)  // y0 ... yN
   {
      float y_n = (origin.x+j*c->Resolution)*mag_factor;
      fwrite(&y_n, sizeof(float), 1, ostrm);
   }
   int index=0;
   for(int i=0; i< c->ny; i++)
   {
      float x_n = (origin.y+i*c->Resolution)*mag_factor;
      fwrite(&x_n, sizeof(float), 1, ostrm);
      for(int j=0; j<c->nx; j++, index++)
        fwrite(&c->thickness[index], sizeof(float), 1, ostrm);
   }
   fclose(ostrm);
   return(OK);
}
/* ******************************************************************* */
