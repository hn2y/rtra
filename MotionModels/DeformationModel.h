#ifndef DEFORMATIONMODEL_H
#define DEFORMATIONMODEL_H
#include <iostream>
#include <MotionModels/GumModel.h>
#include <QtXml>

class DeformationModel:public GumModel
{
public:
    DeformationModel();
    DeformationModel(std::ostream  &stream);
    ~DeformationModel();
    DeformationModel operator =(const DeformationModel &deformationModel);
    bool DoseConvFlag;
    QDomDocument *domXmlDoc;
    std::ostream  &logstream;
private:

};

#endif // DEFORMATIONMODEL_H
