#ifndef POINTINPOLYGON
#define POINTINPOLYGON

#include <vector>
#include "Utilities/someHelpers.h"

class PointInPolygon
{
public:

	PointInPolygon();
	~PointInPolygon();
	//std::ostream& operator<<(std::ostream& os, const Point& p);
        bool intersectionTest(const Point& p1, const Point& p2, const Point& p3, const Point& p4);
        bool calculate(const Point& p, const std::vector<Point>& polygon);
        bool onSegment(const Point& p1, const Point& p2, const Point& q);
        int orientation(const Point& p1, const Point& p2, const Point& q1);


};

#endif // POINTINPOLYGON
