#ifndef GPUSANDCPUSINFO_H
#define GPUSANDCPUSINFO_H
#include <cuda_runtime.h>
#include <omp.h>
#include <iostream>     // std::cout, std::ostream,
#include <vector>
#include <string>
class gpusAndCpusInfo
{
public:
    gpusAndCpusInfo(std::ostream &stream);
    gpusAndCpusInfo();
    int numGPUs;
    int numCPUs;
    int numQualifiedDevices;
    std::vector<size_t> freeMemOnEachDevice;
    bool getNumCpuAndGpus();
    bool getAvilableMemoryOnDevices();
    std::ostream &logstream;
    std::vector< std::vector<unsigned int> >  transformationJobDistributionVec;
    std::vector<unsigned int>  indicesOfQualifiedDevices;
    std::vector<unsigned int> numberOfPossibleTransfomationInEachKernelLaunch;
    std::vector<bool> IsDeviceSelectedForComputation;
    bool isVerbose;
};

#endif // GPUSANDCPUSINFO_H


