// CUDA-C includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <iostream>
#include "math_constants.h"



extern "C"  void interp3(
	float * vOutput,
	const int     nQueryPoints,
	const int     xSize,
	const int     ySize,
	const int     zSize,
	const float * gridX,
	const float * gridY,
	const float * gridZ,
	const float * vGrid,
	const float * xq,
	const float * yq,
	const float * zq);

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		//fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}



__global__ void interp3_cuda(
	float * vOutput,
	const int     nQueryPoints,
	const int     xSize,
	const int     ySize,
	const int     zSize,
	const float * gridX,
	const float * gridY,
	const float * gridZ,
	const float * vGrid,
	const float * xq,
	const float * yq,
	const float * zq)
{
	//int idx = blockDim.x * (gridDim.x * blockIdx.y + blockIdx.x) + threadIdx.x;
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx >= nQueryPoints)
	{
		return;
	}


	float x = xq[idx];
	float y = yq[idx];
	float z = zq[idx];

//# if __CUDA_ARCH__>=200
//	printf("x[%d] = %4.2f \n", idx,x);
//	printf("y[%d] = %4.2f \n", idx,y);
//	printf("z[%d] = %4.2f \n", idx, z);
//	printf("xSize = %d \n", xSize);
//	printf("ySize = %d \n", ySize);
//	printf("zSize = %d \n", zSize);
//
//
//	printf("gridX[0] = %4.2f \n", gridX[0]);
//	printf("gridX[xSize - 1] = %4.2f \n", gridX[xSize-1]);
//	
//	printf("gridY[0] = %4.2f \n", gridY[0]);
//	printf("gridY[ySize - 1] = %4.2f \n", gridY[ySize - 1]);
//
//	printf("gridZ[0] = %4.2f \n", gridZ[0]);
//	printf("gridZ[zSize - 1] = %4.2f \n", gridZ[zSize - 1]);
//
//#endif  
	//extern __shared__ float s[];
	//float* gridXshared = s;
	//float* gridYshared = (float*)&gridXshared[xSize];
	//float* gridZshared = (float*)&gridYshared[ySize];
	//
	//// check if the input is monotonically increasing 
	//if (gridX[xSize - 1] - gridX[0] < 0)
	//{
	//	for (int i = 0; i < xSize; i++)
	//	{
	//		gridXshared[i] = - gridX[i];
	//		x = -x;
	//	}
	//}
	//else
	//{
	//	for (int i = 0; i < xSize; i++)
	//	{
	//		gridXshared[i] = gridX[i];
	//	}
	//}

	//// check if the input is monotonically increasing 
	//if (gridY[ySize - 1] - gridY[0] < 0)
	//{
	//	for (int i = 0; i < ySize; i++)
	//	{
	//		gridYshared[i] = -gridY[i];
	//		y = -y;
	//	}
	//}
	//else
	//{
	//	for (int i = 0; i < ySize; i++)
	//	{
	//		gridYshared[i] = gridY[i];
	//	}
	//}

	//// check if the input is monotonically increasing 
	//if (gridZ[zSize - 1] - gridZ[0] < 0)
	//{
	//	for (int i = 0; i < zSize; i++)
	//	{
	//		gridZshared[i] = -gridZ[i];
	//		z = -z;
	//	}
	//}
	//else
	//{
	//	for (int i = 0; i < zSize; i++)
	//	{
	//		gridZshared[i] = gridZ[i];
	//	}
	//}




	//if (x < gridXshared[0] || x > gridXshared[xSize - 1] ||
	//	y < gridYshared[0] || y > gridYshared[ySize - 1] ||
	//	z < gridZshared[0] || z > gridZshared[zSize - 1])
	//{
	//	vOutput[idx] = CUDART_NAN_F;
	//	return;
	//}

	//float x0, y0, z0, x1, y1, z1;
	//int ibx, itx, iby, ity, ibz, itz, im;

	//ibx = 0;
	//itx = xSize - 1;
	//while (ibx < (itx - 1))
	//{
	//	im = ((ibx + itx) >> 1);
	//	if (x <= gridXshared[im])
	//	{
	//		itx = im;
	//	}
	//	else
	//	{
	//		ibx = im;
	//	}
	//}
	//x0 = gridXshared[ibx];
	//x1 = gridXshared[itx];

	//iby = 0;
	//ity = ySize - 1;
	//while (iby < (ity - 1))
	//{
	//	im = ((iby + ity) >> 1);
	//	if (y <= gridYshared[im])
	//	{
	//		ity = im;
	//	}
	//	else
	//	{
	//		iby = im;
	//	}
	//}
	//y0 = gridYshared[iby];
	//y1 = gridYshared[ity];

	//ibz = 0;
	//itz = zSize - 1;
	//while (ibz < (itz - 1))
	//{
	//	im = ((ibz + itz) >> 1);
	//	if (z <= gridZshared[im])
	//	{
	//		itz = im;
	//	}
	//	else
	//	{
	//		ibz = im;
	//	}
	//}
	//z0 = gridZshared[ibz];
	//z1 = gridZshared[itz];






	// comment this region if using shared memory 




	

	if (x < gridX[0] || x > gridX[xSize - 1] ||
		y < gridY[0] || y > gridY[ySize - 1] ||
		z < gridZ[0] || z > gridZ[zSize - 1])
	{
		vOutput[idx] = CUDART_NAN_F;
		return;
	}

	float x0, y0, z0, x1, y1, z1;
	int ibx, itx, iby, ity, ibz, itz, im;

	ibx = 0;
	itx = xSize - 1;
	while (ibx < (itx - 1))
	{
		im = ((ibx + itx) >> 1);
		if (x <= gridX[im])
		{
			itx = im;
		}
		else
		{
			ibx = im;
		}
	}
	x0 = gridX[ibx];
	x1 = gridX[itx];

	iby = 0;
	ity = ySize - 1;
	while (iby < (ity - 1))
	{
		im = ((iby + ity) >> 1);
		if (y <= gridY[im])
		{
			ity = im;
		}
		else
		{
			iby = im;
		}
	}
	y0 = gridY[iby];
	y1 = gridY[ity];

	ibz = 0;
	itz = zSize - 1;
	while (ibz < (itz - 1))
	{
		im = ((ibz + itz) >> 1);
		if (z <= gridZ[im])
		{
			itz = im;
		}
		else
		{
			ibz = im;
		}
	}
	z0 = gridZ[ibz];
	z1 = gridZ[itz];

	int sliceDim = xSize * ySize;
	int zOff0 = sliceDim * ibz;
	int zOff1 = zOff0 + sliceDim;
	int yOff0 = ySize * ibx;
	int yOff1 = yOff0 + ySize;

	float ax0 = (x - x0) / (x1 - x0);
	float ay0 = (y - y0) / (y1 - y0);
	float az0 = (z - z0) / (z1 - z0);
	float ax1 = 1.0f - ax0;
	float ay1 = 1.0f - ay0;

	float v000 = vGrid[zOff0 + yOff0 + iby];
	float v001 = vGrid[zOff0 + yOff0 + ity];
	float v010 = vGrid[zOff0 + yOff1 + iby];
	float v011 = vGrid[zOff0 + yOff1 + ity];
	float v100 = vGrid[zOff1 + yOff0 + iby];
	float v101 = vGrid[zOff1 + yOff0 + ity];
	float v110 = vGrid[zOff1 + yOff1 + iby];
	float v111 = vGrid[zOff1 + yOff1 + ity];

	float v00 = v000 * ay1 + v001 * ay0;
	float v01 = v010 * ay1 + v011 * ay0;
	float v10 = v100 * ay1 + v101 * ay0;
	float v11 = v110 * ay1 + v111 * ay0;

	float v0 = v00 * ax1 + v01 * ax0;
	float v1 = v10 * ax1 + v11 * ax0;

	vOutput[idx] = v0 * (1.0f - az0) + v1 * az0;
}


void interp3(	float * vOutput,
	const int     nQueryPoints,
	const int     xSize,
	const int     ySize,
	const int     zSize,
	const float * gridX,
	const float * gridY,
	const float * gridZ,
	const float * vGrid,
	const float * xq,
	const float * yq,
	const float * zq)
{
	// Trilinear interpolation 
	//  interp3 returns nan if a query point is out of the input grid.
	// note it is assumed the VGrid is obtained by meshgrid-ing over gridX, gridY, gridZ (see xyzMeshGrid for more detail)
	//  number of kenel to lanuch  --> nQueryPoints
	int n_threads_per_block = 1 << 10;  // 1024 threads per block // TODO: note: optimal value is harware dependent
	int n_blocks = ceil((double)nQueryPoints / (double)n_threads_per_block);

    if (false)
    {
	std::cout << "number blocks used in interp3 = " << n_blocks << std::endl;
	std::cout << "threads per blocks  used in interp3= " << n_threads_per_block << std::endl;
    }
	//std::cout << "number of transformation which is about to be performed = " << numTrans << std::endl;
	
	//TODO:  implement interp3 using shared memory don't forget to check if shared memory is enough for gridX,  gridY and gridZ // http://devblogs.nvidia.com/parallelforall/using-shared-memory-cuda-cc/
	// interp3_cuda << <n_blocks, n_threads_per_block,(xSize+ySize+zSize)*sizeof(float) >> >(vOutput, nQueryPoints, xSize, ySize, zSize, gridX, gridY, gridZ, vGrid, xq, yq, zq);
	
	interp3_cuda << <n_blocks, n_threads_per_block>> >(vOutput, nQueryPoints, xSize, ySize, zSize, gridX, gridY, gridZ, vGrid, xq, yq, zq);
	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());
}
