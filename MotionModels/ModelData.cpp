#include "ModelData.h"

ModelData::ModelData(std::ostream &stream,const std::vector<Roi>& roiVector):logstream(stream), rigidBodyMotionExist(false), deformableBodyMotionExist(false)
{
    rigidBodyMotionModel=0;
    deformableBodyMotionModel=0;
    verbose=0;

    SetupGUMModelForROIs(roiVector);

}

ModelData::ModelData(std::ostream &stream, const std::vector<roi_prop> &roiVector):logstream(stream), rigidBodyMotionExist(false), deformableBodyMotionExist(false)
{
    rigidBodyMotionModel=0;
    deformableBodyMotionModel=0;
    SetupGUMModelForROIs(roiVector);
    verbose=0;
}
ModelData::ModelData(std::ostream &stream):logstream(stream), rigidBodyMotionExist(false), deformableBodyMotionExist(false)
{
    rigidBodyMotionModel=0;
    deformableBodyMotionModel=0;
    verbose=0;

}


ModelData::ModelData():logstream(std::cout), rigidBodyMotionExist(false), deformableBodyMotionExist(false)
{

    rigidBodyMotionModel=0;
    deformableBodyMotionModel=0;

}


ModelData::~ModelData()
{
    // TODO: clean stuff
    if (rigidBodyMotionModel!=0)
    {
        delete(rigidBodyMotionModel);
    }

    if (deformableBodyMotionModel!=0)
    {
        delete(deformableBodyMotionModel);
    }
}


void ModelData::SetupGUMModelForROIs(const std::vector<Roi>& roiVector)
{
    std::string class_member = "ModelData::SetupGUMModelForROI:";
    if (!verbose)
    {
        logstream << class_member<< "Setting up an instance of RotationAndTranslationModel,DeformationModel and DelineationModel." << std::endl;
    }
    for (int var = 0; var < roiVector.size(); ++var)
    {
        auto rotAndTranModel = RotationAndTranslationModel(logstream);
        auto defModel =  DeformationModel(logstream);
        auto delModel = DelineationModel(logstream);
        boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel> modelTuple = boost::make_tuple(rotAndTranModel,defModel,delModel);
        gumMap[roiVector[var].RoiName] = modelTuple;
    }
    //gumMapSimple[roi] = rotAndTranModel;


    //  auto rotModel= boost::get<0>(gumMap[roi]);

    return;
}

void ModelData::SetupGUMModelForROIs(const std::vector<roi_prop> &roiVector)
{
    std::string class_member = "ModelData::SetupGUMModelForROI:";
    if (!verbose)
    {
    logstream << class_member<< "Setting up an instance of RotationAndTranslationModel,DeformationModel and DelineationModel." << std::endl;
    }
    for (int var = 0; var < roiVector.size(); ++var)
    {
        auto rotAndTranModel = RotationAndTranslationModel(logstream);
        auto defModel =  DeformationModel(logstream);
        auto delModel = DelineationModel(logstream);
        boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel> modelTuple = boost::make_tuple(rotAndTranModel,defModel,delModel);
        gumMap[roiVector[var].name] = modelTuple;
    }
    //gumMapSimple[roi] = rotAndTranModel;

    //  auto rotModel= boost::get<0>(gumMap[roi]);

    return;
}


bool ModelData::parseRigidBodyMotonFromXMLConfigDocRoot(const QDomElement& root,const std::vector<Roi>& roiVector)
{
    std::string class_member = "ModelData::parseRigidBodyMotonFromRoot:";

    // Search For RigidBodyMotion
    QDomNodeList rigidBodyMotionAllNodes = root.elementsByTagName("RidgidBodyMotion");

    if (rigidBodyMotionAllNodes.count()!=1)
    {
        logstream << class_member<< "ERROR --> Either there is no or more than one rigidBodyMotion is defined in the input config XML file"<< std::endl;
        return false;
    }
    else
    {
        logstream << class_member<< "Found rigidBodyMotion in the input config file" <<std::endl;
        QDomElement rigidBodyMotionNode =  rigidBodyMotionAllNodes.at(0).toElement();
        rigidBodyMotionModel = new RigidBodyMotionModel(logstream);



        auto randseedString =rigidBodyMotionNode.elementsByTagName("RandSeed").at(0).firstChild().nodeValue();
        logstream << class_member <<  " Parsing RigidBodyMotionNode : RandSeed ---> = " << randseedString.toStdString() << std::endl;
        // TODO :: rand seed is not used yet

        auto doseConvString =rigidBodyMotionNode.elementsByTagName("DoseConv").at(0).firstChild().nodeValue();
        logstream << class_member << " Parsing RigidBodyMotionNode : DoseConv --> = " << doseConvString.toStdString() << std::endl;

        rigidBodyMotionModel->DoseConvFlag=(doseConvString.toInt()==1);


        QDomElement rotCenter =rigidBodyMotionNode.elementsByTagName("RotCenter").at(0).toElement();
        QString rotCenterTypeString = rotCenter.elementsByTagName("Type").at(0).firstChild().nodeValue();
        QString rotCenterRoiNameString = rotCenter.elementsByTagName("RoiName").at(0).firstChild().nodeValue();
        logstream << class_member <<" Parsing RigidBodyMotionNode : RotCenter  --> Type = " << rotCenterTypeString.toStdString() << std::endl;
        logstream << class_member << " Parsing RigidBodyMotionNode : RotCenter  --> RoiName = " << rotCenterRoiNameString.toStdString() << std::endl;

        int isrotTypeCOM = QString::compare(rotCenterTypeString, "ROICOM",Qt::CaseSensitive);
        if (isrotTypeCOM==0)
        {
            rigidBodyMotionModel->rotCenter.RotationCenterType=RotationCenter::ROICenterOfMass;

        }
        else
        {
            logstream << class_member << "ERORR -- > Only ROI Center of MASS (ROICOM) is supported as Rotation Center Type" << std::endl;
            return false;
        }

        // Check if the provided ROI for Rotation Center is in the ROI list
        int testFlag=-1;
        for (int iRoi=0; iRoi< roiVector.size(); iRoi++)
        {
            testFlag = QString::compare(rotCenterRoiNameString,  roiVector[iRoi].RoiName.c_str(),Qt::CaseSensitive);
            if (testFlag ==0)
            {
                logstream << class_member << " Found the requested ROI name to rotation about in the roiVector" << std::endl;
                rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM = roiVector[iRoi].RoiName;
                break;
            }
            if (iRoi==roiVector.size()-1)
            {
                logstream << class_member << "ERROR --> Could not found the requested ROI name to rotation about in the roiVector" << std::endl;
                return false;
            }
        }






        QDomElement sysRotSigma =rigidBodyMotionNode.elementsByTagName("SystRotSigma").at(0).toElement();
        QString sysRotSigmaXString = sysRotSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString sysRotSigmaYString = sysRotSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString sysRotSigmaZString = sysRotSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        logstream << class_member << " Parsing RigidBodyMotionNode: SystRotSigma -- > x = " << sysRotSigmaXString.toStdString() <<  "   y = " << sysRotSigmaYString.toStdString() << "   z = " << sysRotSigmaZString.toStdString() <<std::endl;
        rigidBodyMotionModel->sysRotErrorSigma.x=sysRotSigmaXString.toDouble();
        rigidBodyMotionModel->sysRotErrorSigma.y=sysRotSigmaYString.toDouble();
        rigidBodyMotionModel->sysRotErrorSigma.z=sysRotSigmaZString.toDouble();


        QDomElement randRotSigma =rigidBodyMotionNode.elementsByTagName("RandRotSigma").at(0).toElement();
        QString randRotSigmaXString = randRotSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString randRotSigmaYString = randRotSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString randRotSigmaZString = randRotSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        rigidBodyMotionModel->randRotErrorSigma.x=randRotSigmaXString.toDouble();
        rigidBodyMotionModel->randRotErrorSigma.y=randRotSigmaYString.toDouble();
        rigidBodyMotionModel->randRotErrorSigma.z=randRotSigmaZString.toDouble();

        logstream << class_member <<" Parsing RigidBodyMotionNode: RandRotSigma -- > x = " << randRotSigmaXString.toStdString() <<  "   y = " << randRotSigmaYString.toStdString() << "   z = " << randRotSigmaZString.toStdString() <<std::endl;

        QDomElement systTrnSigma =rigidBodyMotionNode.elementsByTagName("SystTrnSigma").at(0).toElement();
        QString systTrnSigmaXString = systTrnSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString systTrnSigmaYString = systTrnSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString systTrnSigmaZString = systTrnSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        rigidBodyMotionModel->sysTranErrorSigma.x=systTrnSigmaXString.toDouble();
        rigidBodyMotionModel->sysTranErrorSigma.y=systTrnSigmaYString.toDouble();
        rigidBodyMotionModel->sysTranErrorSigma.z=systTrnSigmaZString.toDouble();

        logstream << class_member <<" Parsing RigidBodyMotionNode: SystTrnSigma -- > x = " << systTrnSigmaXString.toStdString() <<  "   y = " << systTrnSigmaYString.toStdString() << "   z = " << systTrnSigmaZString.toStdString() <<std::endl;


        QDomElement randTrnSigma =rigidBodyMotionNode.elementsByTagName("RandTrnSigma").at(0).toElement();
        QString randTrnSigmaXString = randTrnSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString randTrnSigmaYString = randTrnSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString randTrnSigmaZString = randTrnSigma.elementsByTagName("z").at(0).firstChild().nodeValue();

        rigidBodyMotionModel->randTranErrorSigma.x=randTrnSigmaXString.toDouble();
        rigidBodyMotionModel->randTranErrorSigma.y=randTrnSigmaYString.toDouble();
        rigidBodyMotionModel->randTranErrorSigma.z=randTrnSigmaZString.toDouble();

        logstream << class_member <<" Parsing RigidBodyMotionNode: RandTrnSigma -- > x = " << randTrnSigmaXString.toStdString() <<  "   y = " << randTrnSigmaYString.toStdString() << "   z = " << randTrnSigmaZString.toStdString() <<std::endl;


        rigidBodyMotionExist=true;
    }
    return true;
}

bool ModelData::parseAllRotationAndTranslationFromXmlCofingDocRoot(const QDomElement& root,const std::vector<Roi>& roiVector)
{
    std::string class_member = "ModelData::parseAllRotationAndTranslationFromXmlCofingDocRoot:";

    if (gumMap.size()<1)
    {
        logstream << class_member<<  "No gumMap has been setup, constructing gumMap . . . " <<std::endl;
        SetupGUMModelForROIs(roiVector);
    }

    // Search For RigidBodyMotion
    QDomNodeList rotationAndTranslationAllNodes = root.elementsByTagName("RotationAndTranslation");

    if (rotationAndTranslationAllNodes.count()<1)
    {
        logstream << class_member<< "ERROR --> No RotationAndTranslation Model is defined in the input config XML file"<< std::endl;
        return false;
    }

    logstream << class_member<< "Found  "<<  rotationAndTranslationAllNodes.count()  << " RotationAndTranslation model(s) in the input config file. " <<std::endl;




    // Parse first one by one
    for (int iTranRot=0;iTranRot<rotationAndTranslationAllNodes.count() ; iTranRot++)
    {
        QDomElement rotTransElement = rotationAndTranslationAllNodes.at(iTranRot).toElement();

        // get ID first
        QString roiIDString= rotTransElement.attribute("ID");
        // Check if the provided ROI Name is in the ROI list
        int testFlag=-1;
        int idIndexInRoiVector=-1;
        for (int iRoi=0; iRoi< roiVector.size(); iRoi++)
        {
            testFlag = QString::compare(roiIDString,  roiVector.at(iRoi).RoiName.c_str(),Qt::CaseSensitive);
            if (testFlag ==0)
            {
                logstream << class_member << " Found the requested ROI name to rotation about in the roiVector" << std::endl;
                idIndexInRoiVector=iRoi;
                break;

            }
            if (iRoi==roiVector.size()-1)
            {
                logstream << class_member << "ERROR --> Could not found the requested ROI name to rotation about in the roiVector" << std::endl;
                return false;
            }
        }
        // now we update  gumMap model -->  gumMap[roiVector.at(idIndexInRoiVector).RoiName];
        RotationAndTranslationModel newRotTransModel = RotationAndTranslationModel(logstream);
        newRotTransModel.associatedRoiID=roiVector.at(idIndexInRoiVector).RoiName;

        QString guModelFileString= rotTransElement.attribute("guModelFile");
        newRotTransModel.guModelFile=guModelFileString.toStdString();


        auto randseedString =rotTransElement.elementsByTagName("RandSeed").at(0).firstChild().nodeValue();
        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << " : RandSeed ---> = " << randseedString.toStdString() << std::endl;
        // TODO :: rand seed is not used yet

        auto doseConvString =rotTransElement.elementsByTagName("DoseConv").at(0).firstChild().nodeValue();
        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << " : DoseConv --> = " << doseConvString.toStdString() << std::endl;

        newRotTransModel.DoseConvFlag=(doseConvString.toInt()==1);


        QDomElement rotCenter =rotTransElement.elementsByTagName("RotCenter").at(0).toElement();
        QString rotCenterTypeString = rotCenter.elementsByTagName("Type").at(0).firstChild().nodeValue();
        QString rotCenterRoiNameString = rotCenter.elementsByTagName("RoiName").at(0).firstChild().nodeValue();
        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": RotCenter  --> Type = " << rotCenterTypeString.toStdString() << std::endl;
        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": RotCenter  --> Rotate around RoiName = " << rotCenterRoiNameString.toStdString() << std::endl;



        int isrotTypeCOM = QString::compare(rotCenterTypeString, "ROICOM",Qt::CaseSensitive);
        if (isrotTypeCOM==0)
        {
            newRotTransModel.rotCenter.RotationCenterType=RotationCenter::ROICenterOfMass;

        }
        else
        {
            logstream << class_member << "ERORR -- > Only ROI Center of MASS (ROICOM) is supported as Rotation Center Type" << std::endl;
            return false;
        }

        // Check if the provided ROI for Rotation Center is in the ROI list
        testFlag=-1;
        for (int iRoi=0; iRoi< roiVector.size(); iRoi++)
        {
            testFlag = QString::compare(rotCenterRoiNameString,  roiVector[iRoi].RoiName.c_str(),Qt::CaseSensitive);
            if (testFlag ==0)
            {
                logstream << class_member << " Found the requested ROI name to rotation about in the roiVector" << std::endl;
                newRotTransModel.rotCenter.RoiIDForRotationAroundCOM = roiVector[iRoi].RoiName;
                break;
            }
            if (iRoi==roiVector.size()-1)
            {
                logstream << class_member << "ERROR --> Could not found the requested ROI name to rotation about in the roiVector" << std::endl;
                return false;
            }
        }



        QDomElement sysRotSigma =rotTransElement.elementsByTagName("SystRotSigma").at(0).toElement();
        QString sysRotSigmaXString = sysRotSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString sysRotSigmaYString = sysRotSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString sysRotSigmaZString = sysRotSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": SystRotSigma -- > x = " << sysRotSigmaXString.toStdString() <<  "   y = " << sysRotSigmaYString.toStdString() << "   z = " << sysRotSigmaZString.toStdString() <<std::endl;
        newRotTransModel.sysRotErrorSigma.x=sysRotSigmaXString.toDouble();
        newRotTransModel.sysRotErrorSigma.y=sysRotSigmaYString.toDouble();
        newRotTransModel.sysRotErrorSigma.z=sysRotSigmaZString.toDouble();


        QDomElement randRotSigma =rotTransElement.elementsByTagName("RandRotSigma").at(0).toElement();
        QString randRotSigmaXString = randRotSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString randRotSigmaYString = randRotSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString randRotSigmaZString = randRotSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        newRotTransModel.randRotErrorSigma.x=randRotSigmaXString.toDouble();
        newRotTransModel.randRotErrorSigma.y=randRotSigmaYString.toDouble();
        newRotTransModel.randRotErrorSigma.z=randRotSigmaZString.toDouble();

        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": RandRotSigma -- > x = " << randRotSigmaXString.toStdString() <<  "   y = " << randRotSigmaYString.toStdString() << "   z = " << randRotSigmaZString.toStdString() <<std::endl;

        QDomElement systTrnSigma =rotTransElement.elementsByTagName("SystTrnSigma").at(0).toElement();
        QString systTrnSigmaXString = systTrnSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString systTrnSigmaYString = systTrnSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString systTrnSigmaZString = systTrnSigma.elementsByTagName("z").at(0).firstChild().nodeValue();
        newRotTransModel.sysTranErrorSigma.x=systTrnSigmaXString.toDouble();
        newRotTransModel.sysTranErrorSigma.y=systTrnSigmaYString.toDouble();
        newRotTransModel.sysTranErrorSigma.z=systTrnSigmaZString.toDouble();

        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": SystTrnSigma -- > x = " << systTrnSigmaXString.toStdString() <<  "   y = " << systTrnSigmaYString.toStdString() << "   z = " << systTrnSigmaZString.toStdString() <<std::endl;


        QDomElement randTrnSigma =rotTransElement.elementsByTagName("RandTrnSigma").at(0).toElement();
        QString randTrnSigmaXString = randTrnSigma.elementsByTagName("x").at(0).firstChild().nodeValue();
        QString randTrnSigmaYString = randTrnSigma.elementsByTagName("y").at(0).firstChild().nodeValue();
        QString randTrnSigmaZString = randTrnSigma.elementsByTagName("z").at(0).firstChild().nodeValue();

        newRotTransModel.randTranErrorSigma.x=randTrnSigmaXString.toDouble();
        newRotTransModel.randTranErrorSigma.y=randTrnSigmaYString.toDouble();
        newRotTransModel.randTranErrorSigma.z=randTrnSigmaZString.toDouble();

        logstream << class_member <<  " Parsing RotationAndTranslation for RoiName " <<  roiVector.at(idIndexInRoiVector).RoiName << ": RandTrnSigma -- > x = " << randTrnSigmaXString.toStdString() <<  "   y = " << randTrnSigmaYString.toStdString() << "   z = " << randTrnSigmaZString.toStdString() <<std::endl;




        //
        if (gumMap.find(roiVector.at(idIndexInRoiVector).RoiName)==gumMap.end())
        {
            // not found // This should not happen since we check the roiVector when parsing XML config file
            logstream << class_member << "ERROR --> This should not happen since we check the roiVector when parsing XML config file" << std::endl;
            return false;
        }

        std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator  iter =gumMap.find(roiVector.at(idIndexInRoiVector).RoiName);

        boost::get<0>(iter->second) = newRotTransModel;


        //  newRotTransModel.guModelFile=



    } // iTranRot







    return true;
}

bool ModelData::FillModelDataFromXmlConfigDoc(QDomDocument *domXmlDocument, std::vector<Roi>& roiVector)
{
    std::string class_member = "ModelData::FillModelDataFromXmlConfigDoc:";
    logstream << class_member<< " Filling Model Data ... " << std::endl;

    // Getting root element
    QDomElement root = domXmlDocument->firstChildElement();

    if (!parseRigidBodyMotonFromXMLConfigDocRoot(root,roiVector))
    {
        return false;
    }


    if (!parseAllRotationAndTranslationFromXmlCofingDocRoot(root,roiVector))
    {
        return false;
    }


    //    retrievElements(root, "StructureSet", "ID",logstream);
    //    QDomNodeList structureSetsAllNodes = root.elementsByTagName("StructureSetsAll");
    //    logstream << class_member <<   " Number of Structure Sets :  "  <<structureSetsAllNodes.count() << std::endl;
    //    for ( int i =0 ; i <structureSetsAllNodes.count();i++)
    //    {
    //        QDomNode strucruesetNode = structureSetsAllNodes.at(i);
    //        if (strucruesetNode.isElement())
    //        {
    //            QDomElement structureSetElem = strucruesetNode.toElement();
    //            retrievElements(structureSetElem, "ROI", "ID",logstream);
    //        }

    //    }
    return true;
}


//void ModelData::retrievElements(QDomElement root, QString tag, QString att)
//{
//    QDomNodeList nodes = root.elementsByTagName(tag);

//    logstream << "# of nodes with tag  "  << tag.toStdString()  << " is : "  << nodes.count() << std::endl;
//    for(int i = 0; i < nodes.count(); i++)
//    {
//        QDomNode elm = nodes.at(i);
//        if(elm.isElement())
//        {
//            QDomElement e = elm.toElement();
//            logstream << e.attribute(att).toStdString() << std::endl;
//        }
//    }
//}


