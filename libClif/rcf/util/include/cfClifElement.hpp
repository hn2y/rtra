#ifndef CFCLIFELEMENT_HPP
#define CFCLIFELEMENT_HPP

#include <string>

namespace clif_element
{
    enum ClifElementStyle
    {
        ASSIGNMENT,
        COLON
    };
}

class cfClifElement
{
    public:
        cfClifElement();
        cfClifElement( std::string line );
        ~cfClifElement();
        void SetElement( std::string line );
        void SetValue( std::string v ) { value = v; }
        void SetHandle( std::string h ) { handle = h; }
        // Add more Set function to use types instead of just strings
        void SetStyle( clif_element::ClifElementStyle s ) { style = s; }
        std::string GetHandle() { return handle; }
        std::string GetValue( bool suppressQuotes = true );
        clif_element::ClifElementStyle GetStyle() { return style; }
        cfClifElement& operator=(const cfClifElement& element);
    private:
        std::string handle;
        std::string value;
        clif_element::ClifElementStyle style;
};

#endif
