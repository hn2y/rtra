// Author: M.Fatyga
// Date: 1/9/2007
// This class provides utilities for
// the management of byte ordering

#ifndef __rcfDvhMgr
#define __rcfDvhMgr

#include "cfGenDefines.h" // data types below are defined here
#include <map>
#include <vector>

/**
    \class rcfDvhMgr
    @ingroup UTIL
*/

class rcfDvhMgr
{

   protected:

      int dvh_nbins;    ///< Number of bins for the dvh
      float bin_size;    ///< User-defined bin size 
      bool is_bin_size_set;    ///< User-defined bin size 
      float *dvh;       ///< Storage for cumulative dvh
      float *ddvh;      ///< Storage for differential dvh
      int compute_dvh;  ///< If set, compute cumulative dvh (default is yes) 
      int valid_dvh;    ///< If set, the cumulative dvh has been computed and is valid 
      int compute_ddvh; ///< If set, compute differential dvh (default is yes) 
      int valid_ddvh;   ///< If set, differential dvh has been computed and is valid  

      // storage for uder-overflow of the dvh. These will occur if the
      // range is modified by the user (as opposed to autocomputed)
      float ddvh_underflow;
      float ddvh_overflow; 

      // voxel volume definition. 
      // NOTE: voxel_volume_buf and dose_grid MUST be of the same length, defined by grid_length
      float *voxel_volume_buf; /**<  If defined, this stores the voxel base which extends 
                                    the dvh to a case of inhomogenous voxel base, as in dose 
                                    mass histogram. This is a pointer to an external buffer.
                               */
      bool manage_voxel_volume_buf;
      float voxel_volume; ///< This is baseline uniform volume for all voxels.
      
      int voxel_length;  ///< The length of the voxel reference study

      // dose grid storage 
      /// This buffer must be defined, it is the dose buffer that has to be histogrammed. 
      float *dose_grid; 

      /// Definition of dose grid length, shared by dose_grid and voxel_volume_buf 
      int grid_length;

      // managed working buffers, extracted on the basis of the ROI mask
      float *working_dose; 
      int working_dose_length;
      int working_dose_allocated;
      float *working_volume; 
      int working_volume_length;
      int working_volume_allocated;

      // add the optional ability to mask, specifically designed
      // to work with composite bitmap structures.
      // NOTE: if this buffer is attached, it is REQUIRED to be of the same
      // length as the dose buffer

      unsigned long *mask_buffer; 
      unsigned long mask;
      int mask_buffer_length;

      // control of the DVH range. 
      // NOTE: autorange is the default. If the range is modified, under-overflows are stored
      // and made available to the user.

      int autorange; /**<   if this flag is set, limits of the dvh will be auto-computed 
                            (all renormalizations do not apply). This is a default setting.
                     */
      float userfrac; /**<  This number acts as a value and a flag.
                            If this number is set, user asks that
                            the range is re-computed to truncate
                            a portion of the DVH volume. */

      int userrange;        ///< If this flag is set, user supplies both limits of the dvh.
      float userrange_min;  ///< If this value is set, user supplies minimum range of the DVH
      float userrange_max;  ///< If this value is set, user supplies maximum range of the DVH
      float min_dose;
      float max_dose;

      // control the output (if user desires)
      char *roi_name;       ///< Name of the roi, will be incorporated into the output file name
      char *output_path;    ///< Explicit output path.
      int outp_dvh;         ///< User requests automatic output of cumulative dvh on computation. Defaults to no
      int outp_ddvh;        ///< User requests automatic output of cumulative dvh on computation. Defaults to no
      int rel_volume;       ///< When this flag is set, dvh will be written as relative volume. Default is off (absolute volume)

      // computed ROI characteristics
      float roi_volume;
      int roi_length;

      // administrative variables
      int dose_and_volume_ready;
      int debug_flag;

      bool compute_complex_stats;
      double average;
      double variance;
      double skewness;
      bool isSkewness;
      double kurtosis;
      bool isKurtosis;

      std::vector<float> userIndex;
      std::map<float,float> leftIndex;
      std::map<float,float> rightIndex;
      std::map<float,float> indexCumul;

      std::map<float,float> indexIntegral;
      std::vector<float> userIndexIntegral;

      bool enableGeud;
      float geudExponent;
      float geudValue;


      // Public interface member functions
      public:

      // constructors
      rcfDvhMgr(); ///< Create a place-holder with defaults
      rcfDvhMgr(int nbins); ///< Declare dvh length
      // destructors
      ~rcfDvhMgr();

      // management of the dvh
      int setNumberOfBins(int nbins); /**<  Set DVH length. Allocates memory
                                            will fail if nbins is illegal, or memory 
                                            allocation fails. */
      int getNumberOfBins(int *valid); /**< Returns number of bins defined by user. Valid 
                                            flag is 1 if all is well, 0 if user has not yet 
                                            defined the dvh length */

      int setBinSize(float ubin); /**<  Set bin size. If min/max is already known will 
                                            allocate memory, otherwise just sets data 
                                            memory will be allocated later */

      // set-report user defined min-max for the dvh. Valid flag is 1 when 
      // the limit has been defined. The set function will fail if the limit
      // is illegal, or user is making an inconsistent request; 

      int setUsrDvhLimits(float min, float max);
      int setUsrDvhLimits(float min, float max,float binSize);
      int getUsrDvhLimits(float *min, float *max, int *valid);

      // set user renormalization
      // user asks for renormalization of the histogram by cutting off the high dose tail
      // The tail is cut off in proportion to the total volume 
      // (This option would be used most often with Brachy DVHs).
      // NOTE: this option is mutually exclusive with manually set limits

      int setDvhTruncationFactor(float factor);
      float getDvhTruncationFactor();

      // set pointers to input data buffers. Returns failure 
      // if NULL pointer passed
      int setPointerToDoseBuffer(float *buffer, int length);

      // set voxel volume, if the same volume should be assumed for each voxel.
      // this option is mutually exclusive with voxel reference buffer being set 
      int setVoxelVolume(float volume); 

      // this segment sets voxel reference buffer and voxel mask buffer. If these two
      // buffers are NOT set, and the Voxel Volume IS set, the module will compute
      // traditional Dvh. Otherwise, a modified DVH will be computed (example: Dose Mass Histogram).

      /// Set pointer to voxel reference buffer
      int setPointerToVoxelVolumeBuffer(float *buffer, int length);

      // if the mask buffer is not defined, the module will assume that
      // the entire image set represents an ROI. A WARNING will be issued,
      // as this is probably an error
      int setPointerToMaskBuffer(unsigned long *buffer, int length);
      void setRoiMask(unsigned long mask); // mask defaults to 1 (single ROI mask file)


      /// A request to compute the dvh
      int computeDvh();
      /// Request to output dvh
      int outputDvh();
      // NOTE: pre-requisites must be completed
      // (buffers attached, output paths defined)

      // This function resets the DVH by setting dvh buffers
      // to zero, and reseting range control parameters to
      // the autorange default.
      int resetDvh();

      // get DVH data buffers. Valid flag is set to 1 if buffers are valid, set to 0
      // otherwise.
      float *getCumulDvhBuffer(int *valid);
      float *getDiffDvhBuffer(int *valid);
      float getDvhUnderflows(int *valid);
      float getDvhOverflows(int *valid);

      // output control
      int setRoiName(char *name);
      int setOutputPath(char *path);
      void disableCumulDvhOutput();
      void disableDiffDvhOutput();
      void setVolumeToRelative();

      /// Control GEUD computation
      // if this function is used, GEUD will be automatically computed
      // and listed with DVH statistics
      int setGeudExponent(float expo);

      /// Debug control
      void enableDebug();

      //control histo stats
      void disableComplexStats(){ compute_complex_stats = false; };
      void enableKurtosis(){ isKurtosis = true; };
      void enableSkewness(){ isSkewness = true; };
      int addUserIndexRequest(float value);
      int addUserIndexIntegralRequest(float value);

      //programmatically extract statistics
      float getAverage(bool *valid);
      float getVariance(bool *valid);
      float getKurtosis(bool *valid);
      float getSkewness(bool *valid);
      int getNumberOfUserIndices(){ return(userIndex.size()); };
      int getNumberOfIntegralIndices(){ return(userIndexIntegral.size()); };
      float getUserLeftIndex(int index, bool *valid);
      float getUserRightIndex(int index, bool *valid);
      float getUserIntegralIndex(int index, bool *valid);

      


      // Private functions
      private:
      void basicInit();         ///< Place holder for default settings
      void reInit();            ///< Clear the DVH, user wants to recompute. Deallocates memory
      int setBins(int nbins);   ///< Allocates memory for the dvh
      void clearDvh();
      int computeRange();
      int computeDiffStats();
      int computeDiffDvh();
      int computeCumulDvh();
      int computeCumulStats();
      int computeRescaledDvhLimits();
      int prepareDoseAndVolume();
      int testDataReady(int *valid);                          
      int computeGeud();

};

#endif
