#ifndef PINNACLEFUNCWRAPPER_H
#define PINNACLEFUNCWRAPPER_H

// Header needs for log
//#include <boost/iostreams/stream_buffer.hpp>
//#include "JJGDVCGuiDebugBuffer.h"
#include <iostream>
#include <fstream>
#include <QString>
#include <vector>


class pinnacleFuncWrapper
{
public:
    pinnacleFuncWrapper();
    void listRoisForGivenPatient(QString patientPath, QString planDotRoiPath);
    void setInitialParamsForApplication();
    void robustnessAnalyzerPinnacle(QString patientPath, QString planDotRoiPath, const std::vector<std::string>& desiredRoiList, QString totalDoseHeaderFilePath, QString totalDoseImageFilePath, QString totalDoseImageHeaderFilePath, QString resultFolder, float sysTranSigmaX, float sysTranSigmaY, float sysTranSigmaZ, float randomTranSigmaX, float randomTranSigmaY, float randomTranSigmaZ, float sysRotSigmaX, float sysRotSigmaY, float sysRotSigmaZ, float randomRotSigmaX, float randomRotSigmaY, float randomRotSigmaZ, int numFrac, int numSimulation, bool debug);

    // Loads
    QString patientCurrentFolderQString = QString();
    bool isPatientFolderSelected;
    bool isPatientFolderPinnacle;
    bool verbose;

//    std::ostream  logstream;
//    boost::iostreams::stream_buffer<JJGDvcGuiDebugBuffer> debug_buffer;
//    std::string debug_capture;



};

#endif // PINNACLEFUNCWRAPPER_H
