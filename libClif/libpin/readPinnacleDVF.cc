//  Virginia Commonwealth University
//  department of Medical Physics

//   WTW 04 2012
/*
 * Constructors:
 * PinnDVF TrialDVF = PinnDVF(char *dvfPathName, PinnDose TrialDose);
 * This constructor retrieves the dvf file and interpolates onto the TrialDose grid defined by Trial dose
 * 
 * PinnDVF has elements
 * nx, ny, nz - int dimensions
 * ox, oy, oz - float origins
 * rx, ry, rz - float resolutions
 * arrayX , arrayY, arrayZ - the dvf data
 *
 * Functions:
 *
 * wtwGetDvfPathName(char *dvfPath,char *avName,char *ivName)
 *    avName: accumulated volume name (reference)
 *    ivName: iterating volume name
 *
 * wtwReadCompressedDVF(char *compressedDVFpath, int refIndex,float *dvfArray)
 *
 *    refIndex is a meant to be a volume List index, to guide interpolation
 *
 *
 * NOTE: if DVF does not exist, code returns.
 */


// Put your C++ code here.


#include <iostream>
#include <cassert>
#include <sstream>
#include "string.h"

#ifdef WAS_INCLUDED    // 1/12/2015: Includes were not found.  Evidentally, they were not used
#include "dTransform.h"
// A few of the useful Pinnacle Includes
// See PinnaclePluginPackage_9.100/Pinnacle/GeoPlugins/include
#include "CPPTrial.h"
#include "CPPBeam.h"
#include "CPPRoi.h"
#include "Volume.h"
#include "CPPVolume.h"
#include "CPPObjectList.h"
#include "ITKAndDVFInterface.h"
#endif
/* ********************************************************************************************************* */
dTransform::PinnDVF::PinnDVF(CPPTrial* ptTrial, int fixedTrialIndex, int movingTrialIndex, char *compressedDVFpath) {
    const float MIN_RESOLUTION = 0.004;

    //cout << "Reading DVF" << endl;

    float* xMap = NULL;
    float* yMap = NULL;
    float* zMap = NULL;
    float* xInvMap = NULL;
    float* yInvMap = NULL;
    float* zInvMap = NULL;
 
    //bool debug = true;
    bool debug = false;
    char PinnacleCmd[255];

    GeoWrapper *wrapper = new GeoWrapper();
    // Get the volume/image information for the moving trial
    // This is used to define the DVF starts
    char *cvName = new char[50];
    memset(cvName, 0, sizeof (char) *50);
    sprintf(PinnacleCmd, "TrialList.#%d.PatientRepresentation.PatientVolumeName", movingTrialIndex);
    string currentVolumeName = wrapper->sQuery(PinnacleCmd);
    strcpy(cvName, currentVolumeName.c_str());

    char *ivName = new char[50];
    memset(ivName, 0, sizeof (char) *50);
    // Find the volume name in the volume list
    int numVolumes = (int) wrapper->iQuery((char *) "VolumeList.Count");
    int findCurrentVolume = 1;
    int correctVolumeIndex = NULL;

    for (int iv = 0; iv < numVolumes; iv++) {
        sprintf(PinnacleCmd, "VolumeList.#%d.Name", iv);
        string iVolumeName = wrapper->sQuery((char *) PinnacleCmd);
        strcpy(ivName, iVolumeName.c_str());

        findCurrentVolume = strcmp(ivName, cvName);

        if (findCurrentVolume == 0)
            correctVolumeIndex = iv;
    }
    delete[] ivName;
    delete[] cvName;

    //  Open the volume, get the starts
    CPPVolume *imageVolume = NULL;
    sprintf(PinnacleCmd, "VolumeList.#%d.Address", correctVolumeIndex);
    imageVolume = new CPPVolume(wrapper->pvQuery(PinnacleCmd));
    if (imageVolume == NULL) {
      cout << "ERROR: imageVolume is NULL in DVF" << flush; return;
    }
    float xVolStart = imageVolume->getXStart();
    float yVolStart = imageVolume->getYStart();
    float zVolStart = imageVolume->getZStart();

    delete imageVolume;
    delete wrapper;
    // -------------File Reader-------------
    // Make sure DVF exists and is Readable
    std::ifstream is(compressedDVFpath, ios::in | ios::binary);
    if (!is.is_open()) {
        cout << "\r\n -- ERROR: No DVF found on the path -- ";
        cout << "-- Please Create " << compressedDVFpath << " -- \r\n" << endl;
        wrapper->iQuery((char *) " -- Please Create DVF -- ");
        delete wrapper;
        return;
    }
    ///               /// nBytes   index  what
    int isLittle = 0; ///   4        0    little endian=0, bigEndian=1
    int isPair   = 0; ///   4        4    0 = only forward DVF in file, 1 = forward-inverse (pair) DVF is in file
    int fSec     = 0; ///   4        8    fixed image flag:  0=primary, 1=fusion image
    int mSec     = 0; ///   4        12   moving image flag: 0=primary, 1=fusion image
    is.read((char *) &isLittle, sizeof (int));
    is.read((char *) &isPair, sizeof (int));
    is.read((char *) &fSec, sizeof (int));
    is.read((char *) &mSec, sizeof (int));

    // code currently does not check byte order wrt machine byte order...thus, will not work on opposite endian machine.

    if (debug == true) {
      cout << " DVF starts : " << xVolStart << ", " << yVolStart << ", " << zVolStart << endl;
      cout << "\r\nReporting isLittle as : " << isLittle << endl;
      cout << "Reporting isPair as   : " << isPair << endl;
      cout << "reporting fSec as     : " << fSec << endl;
      cout << "Reporting mSec as     : " << mSec << endl;
      cout << "\r\n";
    }
    ///                /// nBytes   index  what
    float fTx = 0.0f;  ///   4        16   xTranslation of fixed image IF nonzero 
    float fTy = 0.0f;  ///   4        20   yTranslation of fixed image IF nonzero
    float fTz = 0.0f;  ///   4        24   zTranslation of fixed image IF nonzero
    float fRx = 0.0f;  ///   4        28   xRotation of fixed image IF nonzero 
    float fRy = 0.0f;  ///   4        32   yRotation of fixed image IF nonzero
    float fRz = 0.0f;  ///   4        36   zRotation of fixed image IF nonzero
    if (fSec == 1) {
      //cout << "User Selected Fixed Volume is Secondary" << endl;
      is.read((char *) &fTx, sizeof (float));   ///
      is.read((char *) &fTy, sizeof (float));
      is.read((char *) &fTz, sizeof (float));
      is.read((char *) &fRx, sizeof (float));
      is.read((char *) &fRy, sizeof (float));
      is.read((char *) &fRz, sizeof (float));
    }

    ///                /// nBytes   index  what
    float mTx = 0.0f;  ///   4        16,40   xTranslation of moving image IF nonzero 
    float mTy = 0.0f;  ///   4        20,44   yTranslation of moving image IF nonzero
    float mTz = 0.0f;  ///   4        24,48   zTranslation of moving image IF nonzero
    float mRx = 0.0f;  ///   4        28,52   xRotation of moving image IF nonzero 
    float mRy = 0.0f;  ///   4        32,56   yRotation of moving image IF nonzero
    float mRz = 0.0f;  ///   4        36,60   zRotation of moving image IF nonzero
    if (mSec == 1) {
      //cout << "User Selected Moving Volume is Secondary" << endl;
      is.read((char *) &mTx, sizeof (float));
      is.read((char *) &mTy, sizeof (float));
      is.read((char *) &mTz, sizeof (float));
      is.read((char *) &mRx, sizeof (float));
      is.read((char *) &mRy, sizeof (float));
      is.read((char *) &mRz, sizeof (float));
    }
    ///               /// nBytes   index  what    
    int bbStartX = 0; ///   4      40,64  #voxels to minimum x corner of bounding box relative to the fixed image 
    int bbStartY = 0; ///   4      44,68  #voxels to minimum y corner of bounding box relative to the fixed image 
    int bbStartZ = 0; ///   4      48,72  #voxels to minimum z corner of bounding box relative to the fixed image 
    int bbEndX = 0;   ///   4      52,76  #voxels to maximum x corner of bounding box relative to the fixed image 
    int bbEndY = 0;   ///   4      56,80  #voxels to maximum x corner of bounding box relative to the fixed image 
    int bbEndZ = 0;   ///   4      60,84  #voxels to maximum x corner of bounding box relative to the fixed image 
    is.read((char *) &bbStartX, sizeof (int));
    is.read((char *) &bbStartY, sizeof (int));
    is.read((char *) &bbStartZ, sizeof (int));
    is.read((char *) &bbEndX, sizeof (int));
    is.read((char *) &bbEndY, sizeof (int));
    is.read((char *) &bbEndZ, sizeof (int));

    ///               /// nBytes   index  what    
    int dvfX = 0;     ///   4      64,88   number of DVF voxels in x direction
    int dvfY = 0;     ///   4      68,92   number of DVF voxels in y direction
    int dvfZ = 0;     ///   4      72,96   number of DVF voxels in z direction
    is.read((char *) &dvfX, sizeof (int));
    is.read((char *) &dvfY, sizeof (int));
    is.read((char *) &dvfZ, sizeof (int));

    ///                    /// nBytes   index  what    
    double spacingX = 0.0; ///   8      76,100  voxel size in X direction in mm
    double spacingY = 0.0; ///   8      84,108  voxel size in Y direction in mm
    double spacingZ = 0.0; ///   8      92,116  voxel size in Z direction in mm
    is.read((char *) &spacingX, sizeof (double));
    is.read((char *) &spacingY, sizeof (double));
    is.read((char *) &spacingZ, sizeof (double));

    // Check the voxel spacing
    // 
    if( dvfX != (bbEndX - bbStartX) ||
        dvfY != (bbEndY - bbStartY) ||
        dvfZ != (bbEndZ - bbStartZ)  ) {
      cout << "ERROR: number of voxels != difference in bounding box"<<endl; return;
    }


    long voxelCount = (dvfX) * (dvfY) * (dvfZ);
    if (debug == true) {
        cout << "\r\n DVF DIMENSIONS " << dvfX << " x " << dvfY << " x " << dvfZ << endl;
        cout << "With " << voxelCount << " voxels " << endl;

    }
    // Allocate memory for the dvf values (in x,y,z directions)
    xMap = (float*) malloc(voxelCount * sizeof (float));
    yMap = (float*) malloc(voxelCount * sizeof (float));
    zMap = (float*) malloc(voxelCount * sizeof (float));

    if (xMap == NULL || yMap == NULL || zMap == NULL) {
        cout << "ERROR: Failed to allocate memory for DVFs " << flush;
        return;
    }

    // Allocate memory for the compressed DVF components that are saved in the file
    signed char *xBufferHigh = new signed char[voxelCount];
    signed char *yBufferHigh = new signed char[voxelCount];
    signed char *zBufferHigh = new signed char[voxelCount];
    unsigned char *xBufferLow = new unsigned char[voxelCount];
    unsigned char *yBufferLow = new unsigned char[voxelCount];
    unsigned char *zBufferLow = new unsigned char[voxelCount];

    //cout << " Allocated all DVF buffers" << endl;

    // Read in the DVF buffers
    is.read((char *) xBufferHigh, voxelCount);
    is.read((char *) yBufferHigh, voxelCount);
    is.read((char *) zBufferHigh, voxelCount);
    is.read((char *) xBufferLow, voxelCount);
    is.read((char *) yBufferLow, voxelCount);
    is.read((char *) zBufferLow, voxelCount);

    // Do the image decompression
    for (int i = 0; i < voxelCount; i++) {
        xMap[i] = xBufferHigh[i] + (MIN_RESOLUTION * xBufferLow[i]);
        yMap[i] = yBufferHigh[i] + (MIN_RESOLUTION * yBufferLow[i]);
        zMap[i] = zBufferHigh[i] + (MIN_RESOLUTION * zBufferLow[i]);

    }
    // read in the second (inverse) DVF if it exists.
    if (isPair == 1) {
        //cout << "READING INVERSE DVF" << endl;
      cout << " WARNING: inverse map is currently read in, but is never used " << endl;
        is.read((char *) xBufferHigh, voxelCount);
        is.read((char *) yBufferHigh, voxelCount);
        is.read((char *) zBufferHigh, voxelCount);
        is.read((char *) xBufferLow, voxelCount);
        is.read((char *) yBufferLow, voxelCount);
        is.read((char *) zBufferLow, voxelCount);
        // Allocate memory for this inverse map
        xInvMap = (float*) malloc(voxelCount * sizeof (float));
        yInvMap = (float*) malloc(voxelCount * sizeof (float));
        zInvMap = (float*) malloc(voxelCount * sizeof (float));     
        if(NULL == xInvMap || NULL==yInvMap || NULL==zInvMap) {
	  cout << " ERROR: Allocating memory for inverse maps " << flush;return; 
	}
        // decompress the inverse map
        for (int i = 0; i < voxelCount; i++) {
            xInvMap[i] = xBufferHigh[i] + (MIN_RESOLUTION * xBufferLow[i]);
            yInvMap[i] = yBufferHigh[i] + (MIN_RESOLUTION * yBufferLow[i]);
            zInvMap[i] = zBufferHigh[i] + (MIN_RESOLUTION * zBufferLow[i]);
        }
    }
    // close out the input stream
    is.close();
    //cout << " Closed the DVF file " << endl;
    float xRes = 0.01f * spacingX; // Compute resolutions in cm
    float yRes = 0.01f * spacingY;
    float zRes = 0.01f * spacingZ;

    float xStart = 1.0f * bbStartX * xRes + xVolStart; /// I(JVS) changed this to xRes so that it is in units of cm (xVolStart is). WTW had spacingX here
    float yStart = 1.0f * bbStartY * yRes + yVolStart;
    float zStart = 1.0f * bbStartZ * zRes + zVolStart;

    if (debug == true) {
        cout << "DVF interpolation on Grid:" << endl;
        cout << " Starts: " << xStart << ", " << yStart << ", " << zStart << endl;
        cout << " Resolution: " << xRes << ", " << yRes << ", " << zRes << endl;
    }
    
    /// delete the allocated memory....
    if (xBufferHigh) {
        delete [] xBufferHigh;
        xBufferHigh = NULL;
    }
    if (yBufferHigh) {
        delete [] yBufferHigh;
        yBufferHigh = NULL;
    }
    if (zBufferHigh) {
        delete [] zBufferHigh;
        zBufferHigh = NULL;
    }
    if (xBufferLow) {
        delete [] xBufferLow;
        xBufferLow = NULL;
    }
    if (yBufferLow) {
        delete [] yBufferLow;
        yBufferLow = NULL;
    }
    if (zBufferLow) {
        delete [] zBufferLow;
        zBufferLow = NULL;
    }
    // ---------------------- This should be the end of this routine which reads the DVF
    //  These should be seperated into different sub-routines (which then can be
    // reused in future code.....
    //  Next steps interpolate the DVF onto the dose matrix points.   
    //   a. Get resolution of the dose matrix
    //   b. Foreach DVF direction
    //      i.  saves the DVF into a Pinnacle volume
    //      ii. interpolated DVF onto that volume
 
    PinnDose *DoseData = new PinnDose(ptTrial, fixedTrialIndex);
    // Get the dose array information for interpolation
    //
    /// Number of voxels, 1 added since below will be getting voxel boundaries
    nx = DoseData->nx + 1;
    ny = DoseData->ny + 1;
    nz = DoseData->nz + 1;
    int nElem = nx * ny * nz;
    /// Origin of voxels 
    ox = DoseData->ox;
    oy = DoseData->oy;
    oz = DoseData->oz;
    /// resolution of voxels
    rx = DoseData->rx;
    ry = DoseData->ry;
    rz = DoseData->rz;

    delete DoseData;

    if (debug == true) {
        cout << " Setting up DVF arrays" << endl;
        cout << " Starts: " << ox << ", " << oy << ", " << oz << endl;
        cout << " Resolu: " << rx << ", " << ry << ", " << rz << endl;
        cout << " Dimens: " << nx << ", " << ny << ", " << nz << endl;
    }

    /// Save the DVF into a Pinnacle Volume
    // initialize the Pinnacle Volume
    Volume* dvfVolume = NULL;
    // initialize the dvf arrays
    float *xDVFdata = new float[nElem];
    memset(xDVFdata, 0, sizeof (float) *nElem);

    float *yDVFdata = new float[nElem];
    memset(yDVFdata, 0, sizeof (float) *nElem);

    float *zDVFdata = new float[nElem];
    memset(zDVFdata, 0, sizeof (float) *nElem);

    for (int iDim = 0; iDim < 3; iDim++) {
        // Write the DVF buffers as read from the file to Pinnacle Volumes
        if (iDim == 0) {// x
            dvfVolume = new BasicVolume<float>(xMap, dvfX, dvfY, dvfZ);
        }
        if (iDim == 1) {// y
            dvfVolume = new BasicVolume<float>(yMap, dvfX, dvfY, dvfZ);
        }
        if (iDim == 2) {// z
            dvfVolume = new BasicVolume<float>(zMap, dvfX, dvfY, dvfZ);
        }
        dvfVolume->setXStart(xStart);
        dvfVolume->setYStart(yStart);
        dvfVolume->setZStart(zStart);

        dvfVolume->setXVoxelSize((float) (xRes));
        dvfVolume->setYVoxelSize((float) (yRes));
        dvfVolume->setZVoxelSize((float) (zRes));



        if (debug == true) {
            //report some DVF info
            float vdvfXstart = dvfVolume->getXStart();
            float vdvfXres = dvfVolume->getXVoxelSize();
            float xp = xStart + dvfX / 2.0 * xRes;
            float yp = yStart + dvfY / 2.0 * yRes;
            float zp = zStart + dvfZ / 2.0 * zRes;
            double dvfTestValue = dvfVolume->getValue(zp, yp, xp);
            cout << "in DVF volume, reporting " << endl;
            cout << " xstart = " << vdvfXstart << endl;
            cout << " x res  = " << vdvfXres << endl;
            cout << " dvf va = " << dvfTestValue << endl;
        }

        int j = 0;
        // Interpolate the DVF onto the Dose Grid Resolution
        // These values start at the
        for (int iz = 0; iz < nz; iz++) {
	  float zPos = (float) (oz + iz * rz) - 0.5*rz; /// JVS: Change shift from -1 voxel to -0.5 voxel (so am boundary) for (x,y,z)
            for (int iy = 0; iy < ny; iy++) {
                float yPos = (float) (oy + iy * ry) - 0.5*ry;
                for (int ix = 0; ix < nx; ix++) {
                    // Find the current position
                    float xPos = (float) (ox + ix * rx) - 0.5*rx;
                
                    double dvfValue = dvfVolume->getValue(zPos, yPos, xPos);
                    float fDvfValue = (float) 1.0f * dvfValue / 10.0f;  // convert from mm to cm  (better off being *0.1f

                    if (iDim == 0) {
                        xDVFdata[j] = fDvfValue;
                    }
                    if (iDim == 1) {
                        yDVFdata[j] = fDvfValue;
                    }
                    if (iDim == 2) {
                        zDVFdata[j] = fDvfValue;
                    }
                    j++;
                }
            }
        }
        delete dvfVolume;  /// delete the dvfVolume since it is no longer needed (must do for each direction since it was saved as such
        dvfVolume = NULL;
    }

    arrayX = xDVFdata;
    arrayY = yDVFdata;
    arrayZ = zDVFdata;

    if (debug == true) {
        string headerFileName = "dvfTestForDT.hdr";
        string binaryFileName = "dvfTestForDT.bin";
        FILE* fid;
        fid = fopen(headerFileName.c_str(), "wt");
        fprintf(fid, "\r\n DVF Float");
        fprintf(fid, "\r\n dimension = (%d, %d, %d)", nx, ny, nz);
        fprintf(fid, "\r\n origins   = (%5.5f ,  %5.5f , %5.5f)", ox, oy, oz);
        fprintf(fid, "\r\n resolute  = (%5.5f ,  %5.5f , %5.5f)", rx, ry, rz);
        fclose(fid);

        float* buffer = new float[3 * nx * ny * nz];
        memset(buffer, 0, sizeof (float) *3 * nx * ny * nz);
        for (int k = 0; k < nx * ny * nz; k++) {
            buffer[k] = arrayX[k];
            buffer[nx * ny * nz + k] = arrayY[k];
            buffer[2 * nx * ny * nz + k] = arrayZ[k];
        }


        fid = fopen(binaryFileName.c_str(), "wb");
        float writeSize = fwrite(buffer, sizeof (float), 3 * nx * ny*nz, fid);
        fclose(fid);
        cout << "Wrote " << writeSize << " DVF elements" << endl;
        delete[] buffer;
    }

    free(xMap);
    free(yMap);
    free(zMap);
    free(xInvMap);
    free(yInvMap);
    free(zInvMap);
}
/* ********************************************************************************************************* */
bool dTransform::wtwGetDvfPathName(char *dvfPath, char *avName, char *ivName) {
    // avName --> fixed in pinnacle
    // ivName --> moving in pinnacle
    int j = 0;
    char *dvfFileName = new char[1024];
    memset(dvfFileName, 0, sizeof (char) *1024);

    cout << " Opening DVF file : ";
    for (int i = 0; i < 50; i++) {
        if (avName[i] != '^' && avName[i] != ' ' && avName[i] != '%' && avName[i] != 0) {
            // cout << "Accumulated volume " << avName[i];
            dvfFileName[j] = avName[i];
            // cout << " j = " << j << "  " << dvfFileName[j]<< endl;
            j++;
            // cout << " j = " << j << endl;
        }
    }
    int correctJ = strlen(dvfFileName);
    j = correctJ;
    // cout << "correct J is " << correctJ << endl;

    dvfFileName[j] = '_';
    // cout << " j = " << j << "  " <<  dvfFileName[j]<< endl;
    j++;

    for (int i = 0; i < 50; i++) {
        if (ivName[i] != '^' && ivName[i] != ' ' && ivName[i] != '%' && ivName[i] != 0) {
            dvfFileName[j] = ivName[i];
            // cout << " j = " << j << "  " <<  dvfFileName[j] << endl;
            j++;

        }
    }

    // cout << "DVF file NAME = " << dvfFileName << endl;

    // string curName = string(dvfFileName);
    //string appName = "Demons_10.dvf";
    string appName = "Demons_01.dvf";
    string fullName = string(dvfFileName) + "_" + appName;

    cout << fullName << endl;


    // FileName is done, now path

    char PinnacleCmd[255];
    char PinnacleCmd2[255];
    GeoWrapper *wrapper = new GeoWrapper();
    if (wrapper == NULL) {
        cout << "ERROR: FindDVFfile : wrapper is NULL " << flush;
        //return NULL;
    }

    sprintf(PinnacleCmd, "GetEnv.PATIENTS");
    sprintf(PinnacleCmd2, "PatientDirectory");

    string initPatientPath = wrapper->sQuery(PinnacleCmd) + "/" + wrapper->sQuery(PinnacleCmd2);
    memset(dvfFileName, 0, sizeof (char) *1024);

    strcpy(dvfFileName, initPatientPath.c_str());
    int currLength = strlen(dvfFileName);

    // cout << "Initial path is " << dvfFileName << endl;
    // cout << " with " << currLength << " characters" << endl;

    // Need to Move back 1 directory
    //
    int foundPath = 0;
    int i = currLength - 1;
    dvfFileName[i] = 0;

    while (foundPath <= 0) {
        if (dvfFileName[i] != '/') {
            dvfFileName[i] = 0;
        } else {
            foundPath++;
        }
        i--;
    }

    string fullPath = string(dvfFileName) + fullName;



    // cout << " Patient Path is " << fullPath << endl;
    strcpy(dvfPath, fullPath.c_str());
    delete wrapper;
    delete[] dvfFileName;
    //return fullPath;
    return true;

}
/* ********************************************************************************************************* */
void dTransform::wtwReadCompressedDVF(char *compressedDVFpath, int refIndex, float *dvfArray) {
    const float MIN_RESOLUTION = 0.004;

    cout << "Reading DVF" << endl;

    float* xMap = NULL;
    float* yMap = NULL;
    float* zMap = NULL;
    float* xInvMap = NULL;
    float* yInvMap = NULL;
    float* zInvMap = NULL;
    int bbStartX = 0;
    int bbStartY = 0;
    int bbStartZ = 0;
    int bbEndX = 0;
    int bbEndY = 0;
    int bbEndZ = 0;
    int dvfX = 0;
    int dvfY = 0;
    int dvfZ = 0;
    double spacingX = 0;
    double spacingY = 0;
    double spacingZ = 0;
    int fSec = 0;
    float fTx = 0;
    float fTy = 0;
    float fTz = 0;
    float fRx = 0;
    float fRy = 0;
    float fRz = 0;
    int mSec = 0;
    float mTx = 0;
    float mTy = 0;
    float mTz = 0;
    float mRx = 0;
    float mRy = 0;
    float mRz = 0;
    int isLittle = 0;
    int isPair = 0;
    //bool debug = true;
    bool debug = false;

    std::ifstream is(compressedDVFpath, ios::in | ios::binary);
    if (!is.is_open()) {
        cout << "\r\n ERROR: No DVF found on the path \r\n";
        return;
    }

    is.read((char *) &isLittle, sizeof (int));

    is.read((char *) &isPair, sizeof (int));

    is.read((char *) &fSec, sizeof (int));

    is.read((char *) &mSec, sizeof (int));

    if (debug == true) {
        cout << "\r\nReporting isLittle as : " << isLittle << endl;
        cout << "Reporting isPair as   : " << isPair << endl;
        cout << "reporting fSec as     : " << fSec << endl;
        cout << "Reporting mSec as     : " << mSec << endl;
        cout << "\r\n";

    }



    if (fSec == 1) {
        cout << "User Selected Fixed Volume is Secondary" << endl;
        is.read((char *) &fTx, sizeof (float));
        is.read((char *) &fTy, sizeof (float));
        is.read((char *) &fTz, sizeof (float));
        is.read((char *) &fRx, sizeof (float));
        is.read((char *) &fRy, sizeof (float));
        is.read((char *) &fRz, sizeof (float));
    }

    if (mSec == 1) {
        cout << "User Selected Moving Volume is Secondary" << endl;
        is.read((char *) &mTx, sizeof (float));
        is.read((char *) &mTy, sizeof (float));
        is.read((char *) &mTz, sizeof (float));
        is.read((char *) &mRx, sizeof (float));
        is.read((char *) &mRy, sizeof (float));
        is.read((char *) &mRz, sizeof (float));
    }

    is.read((char *) &bbStartX, sizeof (int));
    is.read((char *) &bbStartY, sizeof (int));
    is.read((char *) &bbStartZ, sizeof (int));
    is.read((char *) &bbEndX, sizeof (int));
    is.read((char *) &bbEndY, sizeof (int));
    is.read((char *) &bbEndZ, sizeof (int));

    is.read((char *) &dvfX, sizeof (int));
    is.read((char *) &dvfY, sizeof (int));
    is.read((char *) &dvfZ, sizeof (int));

    is.read((char *) &spacingX, sizeof (double));
    is.read((char *) &spacingY, sizeof (double));
    is.read((char *) &spacingZ, sizeof (double));

    // These are not correct
    // int xDim = bbEndX - bbStartX;
    // int yDim = bbEndY - bbStartY;
    // int zDim = bbEndZ - bbStartZ;

    long voxelCount = (dvfX) * (dvfY) * (dvfZ);

    if (debug == true) {
        cout << "\r\n DVF DIMENSIONS " << dvfX << " x " << dvfY << " x " << dvfZ << endl;
        cout << "With " << voxelCount << " voxels " << endl;

    }

    xMap = (float*) malloc(voxelCount * sizeof (float));
    yMap = (float*) malloc(voxelCount * sizeof (float));
    zMap = (float*) malloc(voxelCount * sizeof (float));

    if (xMap == NULL || yMap == NULL || zMap == NULL) {
        cout << "ERROR: Failed to allocate memory for DVFs " << flush;
        return;
    }

    signed char *xBufferHigh = new signed char[voxelCount];
    signed char *yBufferHigh = new signed char[voxelCount];
    signed char *zBufferHigh = new signed char[voxelCount];
    unsigned char *xBufferLow = new unsigned char[voxelCount];
    unsigned char *yBufferLow = new unsigned char[voxelCount];
    unsigned char *zBufferLow = new unsigned char[voxelCount];

    is.read((char *) xBufferHigh, voxelCount);
    is.read((char *) yBufferHigh, voxelCount);
    is.read((char *) zBufferHigh, voxelCount);
    is.read((char *) xBufferLow, voxelCount);
    is.read((char *) yBufferLow, voxelCount);
    is.read((char *) zBufferLow, voxelCount);

    float xStart = 1.0 * bbStartX*spacingX;
    float yStart = 1.0 * bbStartY*spacingY;
    float zStart = 1.0 * bbStartZ*spacingZ;

    float xRes = 1.0 * spacingX;
    float yRes = 1.0 * spacingY;
    float zRes = 1.0 * spacingZ;

    for (int i = 0; i < voxelCount; i++) {
        xMap[i] = xBufferHigh[i] + (MIN_RESOLUTION * xBufferLow[i]);
        yMap[i] = yBufferHigh[i] + (MIN_RESOLUTION * yBufferLow[i]);
        zMap[i] = zBufferHigh[i] + (MIN_RESOLUTION * zBufferLow[i]);

    }


    if (isPair == 1) {
        cout << "READING INVERSE DVF" << endl;
        xInvMap = (float*) malloc(voxelCount * sizeof (float));
        yInvMap = (float*) malloc(voxelCount * sizeof (float));
        zInvMap = (float*) malloc(voxelCount * sizeof (float));

        is.read((char *) xBufferHigh, voxelCount);
        is.read((char *) yBufferHigh, voxelCount);
        is.read((char *) zBufferHigh, voxelCount);
        is.read((char *) xBufferLow, voxelCount);
        is.read((char *) yBufferLow, voxelCount);
        is.read((char *) zBufferLow, voxelCount);

        for (int i = 0; i < voxelCount; i++) {
            xInvMap[i] = xBufferHigh[i] + (MIN_RESOLUTION * xBufferLow[i]);
            yInvMap[i] = yBufferHigh[i] + (MIN_RESOLUTION * yBufferLow[i]);
            zInvMap[i] = zBufferHigh[i] + (MIN_RESOLUTION * zBufferLow[i]);
        }
    }

    is.close();


    if (xBufferHigh) {
        delete [] xBufferHigh;
        xBufferHigh = NULL;
    }
    if (yBufferHigh) {
        delete [] yBufferHigh;
        yBufferHigh = NULL;
    }
    if (zBufferHigh) {
        delete [] zBufferHigh;
        zBufferHigh = NULL;
    }
    if (xBufferLow) {
        delete [] xBufferLow;
        xBufferLow = NULL;
    }
    if (yBufferLow) {
        delete [] yBufferLow;
        yBufferLow = NULL;
    }
    if (zBufferLow) {
        delete [] zBufferLow;
        zBufferLow = NULL;
    }


    // Get information on the reference volume
    //

    char PinnacleCmd[255];
    GeoWrapper *wrapper = new GeoWrapper();
    if (wrapper == NULL) {
        cout << "ERROR: GetDoseArray : wrapper is NULL " << flush;
        return;
    }
    CPPVolume *refVolume = NULL;
    sprintf(PinnacleCmd, "VolumeList.#%d.Address", refIndex);
    refVolume = new CPPVolume(wrapper->pvQuery(PinnacleCmd));

    if (refVolume == NULL) {
        cout << "ERROR: DVF reader : reference volume is NULL " << flush;
        return;
    }
    // Starts
    double xS = 1.0 * refVolume->getXOrigin();
    double yS = 1.0 * refVolume->getYOrigin();
    double zS = 1.0 * refVolume->getZOrigin();

    // Resolutions
    double xR = 1.0 * refVolume->getXVoxelSize();
    double yR = 1.0 * refVolume->getYVoxelSize();
    double zR = 1.0 * refVolume->getZVoxelSize();

    // Dimensions
    int xD = refVolume -> getXDim();
    int yD = refVolume -> getYDim();
    int zD = refVolume -> getZDim();

    if (debug == true) {
    cout << " Reference Volume parameters from within DVF" << endl;
    cout << " Dimensions (x,y,z) = (" << xD << ", " << yD << ", " << zD << ")" << endl;
    cout << " Start Pos. (x,y,z) = (" << xS << ", " << yS << ", " << zS << ")" << endl;
    cout << " Resolution (x,y,z) = (" << xR << ", " << yR << ", " << zR << ")" << endl;

    itk_ResampleAndReAllocateDVFBuffers(xMap, yMap, zMap, dvfX, dvfY, dvfZ, spacingX, spacingY, spacingZ,
            xD, yD, zD, xS, yS, zS);

    }



    delete refVolume;
    delete wrapper;


    free(xMap);
    free(yMap);
    free(zMap);
    free(xInvMap);
    free(yInvMap);
    free(zInvMap);



}
