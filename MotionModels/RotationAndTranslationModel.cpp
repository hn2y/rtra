#include "RotationAndTranslationModel.h"

RotationAndTranslationModel::RotationAndTranslationModel(std::ostream &stream):
    logstream(stream)
{
    domXmlDoc=0;
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="RotationAndTransition";
    rotCenter.RotationCenterType =  RotationCenter::ROICenterOfMass;
    rotCenter.RoiIDForRotationAroundCOM=associatedRoiID;
    sysRotErrorSigma.x =0;
    sysRotErrorSigma.y =0;
    sysRotErrorSigma.z =0;

    randRotErrorSigma.x =0;
    randRotErrorSigma.y =0;
    randRotErrorSigma.z =0;

    sysTranErrorSigma.x =0;
    sysTranErrorSigma.y =0;
    sysTranErrorSigma.z =0;

    randTranErrorSigma.x =0;
    randTranErrorSigma.y =0;
    randTranErrorSigma.z =0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;






    DoseConvFlag=false;
}



RotationAndTranslationModel::RotationAndTranslationModel():logstream(std::cout)
{
    domXmlDoc=0;
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="RotationAndTransition";
    rotCenter.RotationCenterType =  RotationCenter::ROICenterOfMass;
    rotCenter.RoiIDForRotationAroundCOM=associatedRoiID;

    sysRotErrorSigma.x =0;
    sysRotErrorSigma.y =0;
    sysRotErrorSigma.z =0;

    randRotErrorSigma.x =0;
    randRotErrorSigma.y =0;
    randRotErrorSigma.z =0;

    sysTranErrorSigma.x =0;
    sysTranErrorSigma.y =0;
    sysTranErrorSigma.z =0;

    randTranErrorSigma.x =0;
    randTranErrorSigma.y =0;
    randTranErrorSigma.z =0;



    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    DoseConvFlag=false;


}

RotationAndTranslationModel::~RotationAndTranslationModel()
{

}

//void RotationAndTranslationModel::clear()
//{
//    domXmlDoc=0;
//    associatedRoiID= "";
//    guModelFile ="";
//    guModelType ="RotationAndTransition";
//}

bool RotationAndTranslationModel::loadModelFromXMLFile(char *filename, std::string & roiID)
{
    // TODO : Implement this
    std::string class_member = "RotationAndTransitionModel::loadModelFromFile:";


    //TODO: change it to the default stream logstream
    logstream << class_member<< "Failed to load the config file for reading." << std::endl;
    return false;
}

/// the input for this function should be input config document which contains
/// all the information about the  RotationAndTransitionModel GUM model.
bool RotationAndTranslationModel::loadModelFromXMLDocument(QDomDocument *domXmlDocument, std::string & roiID)
{
    std::string class_member = "RotationAndTransitionModel::loadModelFromXMLDocument:";
    logstream << class_member<< "Loading the RotationAndTransitionModel information from XML document." << std::endl;


    //sysRotErrorSigma.x = ;
    //randRotErrorSigma;
    //sysTranErrorSigma;
    //randTranErrorSigma;
    //DoseConvFlag=flase;


    return false;
}

bool RotationAndTranslationModel::writeXML()
{
    // TODO : implement to write a xml file
    std::string class_member = "RotationAndTransitionModel::writeXML:";

    if (guModelFile.empty())
    {
        logstream << class_member<< "ERORR --> the guModelFile is not set yet!"<<  std::endl;
        return false;
    }


    logstream << class_member<< "Not implemented yet." << std::endl;
    return false;
}

bool RotationAndTranslationModel::IsModelSet()
{
    if (sysRotErrorSigma.x ==0 &&  sysRotErrorSigma.y ==0 &&  sysRotErrorSigma.z ==0 &&
        randRotErrorSigma.x ==0 && randRotErrorSigma.y ==0 && randRotErrorSigma.z ==0 &&
        sysTranErrorSigma.x ==0 && sysTranErrorSigma.y ==0 && sysTranErrorSigma.z ==0 &&
        randTranErrorSigma.x ==0 && randTranErrorSigma.y ==0 && randTranErrorSigma.z ==0)
    {
        return false;

    }
    else
    {
        return true;
    }
}

RotationAndTranslationModel RotationAndTranslationModel::operator=(const RotationAndTranslationModel &rotAndTransModelData)
{
     std::string class_member = "RotationAndTranslationModel::operator=:";
     guModelType = rotAndTransModelData.guModelType;
     guModelFile = rotAndTransModelData.guModelFile;
     associatedRoiID=rotAndTransModelData.associatedRoiID;
     domXmlDoc   = rotAndTransModelData.domXmlDoc;
     DoseConvFlag = rotAndTransModelData.DoseConvFlag;
     sysRotErrorSigma = rotAndTransModelData.sysRotErrorSigma;
     sysTranErrorSigma = rotAndTransModelData.sysTranErrorSigma;
     randRotErrorSigma = rotAndTransModelData.randRotErrorSigma;
     randTranErrorSigma =rotAndTransModelData.randTranErrorSigma;
     rotCenter = rotAndTransModelData.rotCenter;
     return *this;

}
