#ifndef RTSTRUCTURE_H
#define RTSTRUCTURE_H


#define NOMINMAX // avoids windows min and max macro defination


#include <iostream>
#include <vector>
#include <map>

#include "Utilities/colors.h"




// The DCMTK headers
#ifdef UNICODE
#undef UNICODE
#endif

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
//#include "dcmtk/dcmrt/drtdose.h"
#include "dcmtk/dcmrt/drtimage.h"
//#include "dcmtk/dcmrt/drtplan.h"
#include "dcmtk/dcmrt/drtstrct.h"
//#include "dcmtk/dcmdata/dcfilefo.h"
//#include "dcmtk/dcmrt/drmstrct.h"
//#include "dcmtk/dcmrt/drttreat.h"

#include "dicomParser/Roi.h"


class RtStructure
{
public:
    RtStructure();
    RtStructure(std::ostream &stream);
    ~RtStructure();

#ifndef ROISPROPS
#define ROISPROPs
struct ROIBasicProp{
    std::string RoiName;
    Color::rgb Color;
    bool IsSelectedForComputation;
    int roiNumberInStructureSetRoiSeq;
    int roiNumberInRoiContourSequence;
    double DesiredPointCloudResolution;
    double Volume;
};

#endif    // ROISPROPS



    /* RT structure set from DCMTK  */
    DRTStructureSetIOD rtstructIOD;
    DcmFileFormat fileFormatStructure;
    OFCondition statusRTStructureSetIOD;
    const char * rtStructFileName;
    const char * associateCTDicomFile;

    /// A map from ROI number in ROI contour sequence(ROIContourSequence) and its corresponding ROI ID (ROIName)
    /// ROIContourSequence contains ROI Display Color (ROIDisplayColor) , Contour Sequence (ContourSequence) , and ReferencedROINumber that point to ROINumber in an Item in StructureSetROISequence
    std::map <std::string,int > allRoisNameNumberMap;

    /// A map from ROIName in ROI contour sequence(ROIContourSequence) and ROIBasicProp
   std::map <std::string,ROIBasicProp> allRoisBasicProperties;


    /// ROI Contour Sequence
    /// ROIContourSequence contains ROI Display Color (ROIDisplayColor) , Contour Sequence (ContourSequence) , and ReferencedROINumber that point to ROINumber in an Item in StructureSetROISequence
    DRTROIContourSequence roiContourSequence;



    /// Structure Set ROI Sequence (StructureSetROISequence)
    /// StructureSetROISequence contain ROINumber, ROIName, ReferencedFrameOfReferenceUID, ROIGenerationAlgorithm
    DRTStructureSetROISequence structureSetRoiSeq;

    /// Pointer to an Item in StructureSetROISequence
    DRTStructureSetROISequence::Item *roiStructureSetSeqItem;

    /// Pointer to an Item in ROIContourSequence
    DRTROIContourSequence::Item *roiContourSequenceItem;

    /// Requested and Verified ROI Name vector and its corresponding ROI Number in structureSetRoiSeq for computation
    std::map <std::string,int > roiNameListForComputation;

    /// Requested and Verified PointCloud resolution (roiNameListForComputation and  pointCloudVector) should have the same size
    std::map <std::string,double> pointCloudResolutionVector;


    /// Requested and Verified IsTargetVolume (roiNameListForComputation and  isTargetVolumeVector) should have the same size
    std::map <std::string,int >  isTargetVolumeVector;

    /// a Vector of ROIs
    std::vector<Roi> roiVector;

    OFVector<OFVector<Float64>>  contourDataList;

    /// Patient ID in DICOM File
    std::string patientID;

    /// Structure
    // function
    /// Fills roiNumNameMap
    bool constructRoiNumIDMap();

    /// Checks the provided input list and adds populate them into roiNameListForComputation
    bool verifyAndAddRoiNameListForComputation(std::vector<std::string>& inputList, std::vector<double> &inputPointCloudResList, std::vector<int> &inputIsTargetVolumeFlasList);


    /// Construct Roi Object Vector from verified list of roi Names
    bool createROIModel();


    /// Compute Point Clouds for the ROIs in the


    ///  Wirtes ROIs point clouds
    bool WriteROIsPointCloudsToCSVFile();

    // debug / logging
    std::ostream &logstream;

    /// reads all ROis basic properties
    bool ReadAllRoisProp();


private:
    bool getROIDisplayColorVec(std::vector<unsigned int> &displayColorVec);

};

#endif // RTSTRUCTURE_H
