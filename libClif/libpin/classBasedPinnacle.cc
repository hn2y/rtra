//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <math.h> // for fabs 
//#include <ctype.h> // for isspace
#include "iostream"
//#include "string_util.h"
#include "utilities.h"
//#include "typedefs.h"
//#include "defined_values.h"
//#include "apertures.h"
//#include "beams.h"
//#include "read_clif.h"
//#include "modifiers.h"
#include "classBasedPinnacle.h"
//#include "lib.h" // for definition of TRUE 
//#include "myPinnacle.h"
#include "readPinnacle.h" // prototypes for all routines
using namespace std;
/* *********************************************************************************** */
bool planClass::readSpecificTrial(const char *myTrialName) {
  //char *filename="plan.Trial";
  string filename=planDir+"/plan.Trial";
  // Open plan.Trial file and read the CLIF
  FILE *istrm = fopen(filename.c_str(),"r");
  if(istrm == NULL){
    printf("\n ERROR: Opening File %s\n", filename.c_str()); return false;
  }
  clifObjectType *TrialList = new(clifObjectType);
  if(TrialList == NULL){
    printf("\n ERROR:  Allocating Memory for clifObjectType\n"); return false; 
  } 
  if(readSpecificClif(istrm, TrialList, "Trial", myTrialName) != OK){
    printf("\n ERROR: Reading clif from file %s\n", filename.c_str()); return false;
  }
  fclose(istrm);
  // Fill up the Trial structure 
  clifObjectType *BeamList;
  if(getClifAddress(TrialList,"BeamList", &BeamList)!= OK){
    printf("\n ERROR: Getting Address for BeamList"); return false;
  }
  if(checkIfObjectList(BeamList) != OK){
    printf("\n ERROR: Invalid Object List"); return false;
  }
  int nBeams = BeamList->nObjects;
  Trial = new trialClass[1];
  Trial->nBeams = nBeams;
  Trial->Name = myTrialName;
  
  Trial->Beam= new beamClass[nBeams];
  for(int iBeam=0; iBeam<nBeams; iBeam++) {
    if (!Trial->Beam[iBeam].readPinnacleBeam(&BeamList->Object[iBeam])) {
      printf("\nERROR: %s: readPinnacleBeamData for beam %d",__FUNCTION__, iBeam); return false;
    }
  }
  freeClifStructure(TrialList);
  nTrials=1; // specific=1
  
  delete TrialList;
  return true;
}
//
bool planInfoClass::readPinnaclePlanInfo(){
  return(readPinnaclePlanInfo("plan.PlanInfo"));
}
bool planInfoClass::readPinnaclePlanInfo(const char *planInfoFileName) {
  if(NULL == planInfoFileName) {
    printf("\n ERROR: %s passed null pointer for file name ",__FUNCTION__ ); return false;
  }
  string filename=planDir+"/"+planInfoFileName;
  clifObjectType *PlanInfoClif = new(clifObjectType);
  FILE *istrm = fopen(filename.c_str(),"r");
  if(istrm == NULL){
    printf("\nERROR:%s Opening file %s",__FUNCTION__ ,filename.c_str()); return false;
  }
  if ( readClifFile(istrm, PlanInfoClif, "PlanInfo") != OK 
       || !readStringClifStructure(PlanInfoClif,"PatientName",PatientName) 
       || !readStringClifStructure(PlanInfoClif,"Institution",Institution)
       || !readStringClifStructure(PlanInfoClif,"PlanName", PlanName)
       || !readStringClifStructure(PlanInfoClif,"PatientID", PatientID)
       || !readStringClifStructure(PlanInfoClif,"LastName",LastName )
       || !readStringClifStructure(PlanInfoClif,"FirstName", FirstName)
       || !readStringClifStructure(PlanInfoClif,"MiddleName",MiddleName )
       || !readStringClifStructure(PlanInfoClif,"MedicalRecordNumber",MedicalRecordNumber )
       || !readStringClifStructure(PlanInfoClif,"PlanPath", PlanPath)
       || !readStringClifStructure(PlanInfoClif,"PrimaryImageType", PrimaryImageType) ){
    printf("\n ERROR: %s Reading Clif Information from %s", __FUNCTION__, planInfoFileName); return false;
  }
  fclose(istrm);
  // free the structure
  if(freeClifStructure(PlanInfoClif) != OK){
    printf("\n ERROR: Freeing memory for clif object"); return false;
  }
  delete(PlanInfoClif);
  return true;
}
/* *********************************************************************************** */
controlPointClass::controlPointClass() { // default constructor
  Gantry=0.0f;
  Couch=0.0f;
  Collimator=0.0f;;
  LeftJawPosition=0.0f;
  RightJawPosition=0.0f;
  TopJawPosition=0.0f;
  BottomJawPosition=0.0f;
  Weight=-1.0f;
  WeightLocked=0;
  PercentOfArc=0.0f;
  DoseRate=0.0f;
  DeliveryTime=0.0f; 
  mlc = NULL;
}
controlPointClass::~controlPointClass() { // default constructor
  if(NULL != mlc) delete(mlc);
}
controlPointClass::controlPointClass(clifObjectType *cpSegment) {
  if(!readCPSegment(cpSegment)) {
    throw("ERROR: controlPointClass Constructor");
  }
}
bool controlPointClass::readCPSegment(clifObjectType *cpSegment) {
  if( readFloatClifStructure (cpSegment, "Gantry", &Gantry) != OK
      || readFloatClifStructure (cpSegment, "Couch", &Couch) != OK
      || readFloatClifStructure (cpSegment, "Collimator", &Collimator) != OK
      || readFloatClifStructure (cpSegment, "LeftJawPosition",  &LeftJawPosition) != OK // x1
      || readFloatClifStructure (cpSegment, "RightJawPosition", &RightJawPosition) != OK // x2
      || readFloatClifStructure (cpSegment, "TopJawPosition",   &TopJawPosition) != OK // Pinnacle Y2
      || readFloatClifStructure (cpSegment, "BottomJawPosition",&BottomJawPosition) != OK // Y1
      || readFloatClifStructure (cpSegment, "PercentOfArc",&PercentOfArc) != OK
      || readFloatClifStructure (cpSegment, "DoseRate",&DoseRate) != OK
      || readFloatClifStructure (cpSegment, "DeliveryTime",&DeliveryTime) != OK
      || readFloatClifStructure (cpSegment, "Weight",&Weight) != OK
      || readIntClifStructure (cpSegment, "WeightLocked",&WeightLocked) != OK) {
    printf("\n ERROR: readCPSegment: Reading Beam Parameters from CLIF"); return false;
  }
  clifObjectType *cpMLCPositions;
  char MLCMessage[MAX_STR_LEN];
  if(checkForClifObject(cpSegment,"MLCSegmentList") == OK)
    sprintf(MLCMessage,"MLCSegmentList.BeamSegment.MLCLeafPositions");
  else
    sprintf(MLCMessage,"MLCLeafPositions");
  // get the address of the MLC leaf positions
  if(getClifAddress(cpSegment,MLCMessage, &cpMLCPositions)!= OK){
    printf("\n ERROR: Getting Address"); return false;
  }
  mlc = new mlcApertureClass;
  if (!readCPMLCPositions(cpMLCPositions) ){
    printf("\nERROR: readCPSegment: readCPMLCPositions"); return false;
  }
  return true;
}
bool controlPointClass::readCPMLCPositions(clifObjectType *cpMLCPositions) {
  if( readPinnacleCurve(cpMLCPositions, &mlc->nLeafPairs, &mlc->leaf) != OK){
    printf("\n ERROR: readPinnacleMLC: readPinnacleCurve for MLC"); return false;
  }
  return true;
}
/* *********************************************************************************** */
cpManagerClass::cpManagerClass() { // default constructor
  IsGantryStartStopLocked=-1;
  IsCouchStartStopLocked=-1;
  IsLeftRightIndependentLocked=-1;
  IsLeftRightIndependent=-1;
  IsTopBottomIndependent=-1;
  NumberOfControlPoints=0;
  CP=NULL;
}
cpManagerClass::~cpManagerClass() { // default destructor
  if(NULL != CP) delete[] CP;
}
cpManagerClass::cpManagerClass(clifObjectType *cpManager) {
  if(!readPinnacleCPManager(cpManager)) {
    printf("\nERROR: cpManagerClass constructor");
    throw("ERROR: cpManagerClass constructor");
  }
}
bool cpManagerClass::readPinnacleCPManager(clifObjectType *cpManagerMain){

  clifObjectType *cpManager = cpManagerMain;
  if( checkForClifObject(cpManagerMain,"CPManagerObject") == OK){
    if(getClifAddress(cpManagerMain,"CPManagerObject", &cpManager)!= OK){
            printf("\n ERROR: Getting Address of cpManager"); return false;
    }
  } 

  if( readIntClifStructure (cpManager, "NumberOfControlPoints", &NumberOfControlPoints ) != OK
      || readIntClifStructure (cpManager, "IsGantryStartStopLocked", &IsGantryStartStopLocked ) != OK
      || readIntClifStructure (cpManager, "IsCouchStartStopLocked", &IsCouchStartStopLocked ) != OK
      || readIntClifStructure (cpManager, "IsCollimatorStartStopLocked", &IsCollimatorStartStopLocked ) != OK ) {
    printf("\n\n ERROR: Reading from CPManager\n"); 
    // dumpClifStructure(cpManager);
    return false;
  }
  CP = new controlPointClass[NumberOfControlPoints];
  for(int iSeg=0; iSeg<NumberOfControlPoints; iSeg++) {
    clifObjectType *cpSegment;
    char segString[MAX_STR_LEN];
    sprintf(segString,"ControlPointList.#%d", iSeg);
    if(getClifAddress(cpManager,segString, &cpSegment)!= OK){
      printf("\n ERROR: Getting Address of cpSegment %s",segString); return false;
    }
    if(!CP[iSeg].readCPSegment(cpSegment)) {
      printf("\n  ERROR: readPinnacleCPManager: readCPSegment " );  return false;
    }
  }
  return true;
}
/* *********************************************************************************** */
beamClass::beamClass() {
}
beamClass::beamClass(clifObjectType *Beam) {
  try {
    if(!readPinnacleBeam(Beam)){
      printf("\n ERROR: %s returned false ",__FUNCTION__ );
    }
  }
  catch(...){
    printf("\n ERROR: %s error caught ",__FUNCTION__ );
  }
}
bool beamClass::readPinnacleBeam(clifObjectType *Beam) {
  // Things common to all versions of Pinnacle (that I know of)
  if(!readStringClifStructure(Beam, "Name",Name) 
     || !readStringClifStructure(Beam, "SetBeamType",SetBeamType) 
     || !readStringClifStructure(Beam, "IsocenterName",IsocenterName)
     || !readStringClifStructure(Beam, "MachineNameAndVersion",MachineNameAndVersion)
     || !readStringClifStructure(Beam, "Modality",Modality)
     || !readStringClifStructure(Beam, "MachineEnergyName",MachineEnergyName)
     || OK != readIntClifStructure   (Beam, "UseMLC",&UseMLC) 
     || OK != readFloatClifStructure (Beam, "Weight",&Weight) ) {
    printf("\n ERROR: %s Reading Beam Parameters from Beam Clif", __FUNCTION__ );
    throw("ERROR: beamClass constructor");
  }
  clifObjectType *cpManagerClif;
  // sprintf(commandString,"CPManager",iBeam);
  if(getClifAddress(Beam,"CPManager", &cpManagerClif)!= OK){
    printf("\nERROR: %s Getting Address of cpManager", __FUNCTION__ ); return false;
  }
  if(!CPManager.readPinnacleCPManager(cpManagerClif)) {
    printf("\nERROR: cpManagerClass constructor"); return false;
    //    throw("ERROR: cpManagerClass constructor");
  }
  /*  try {
    CPManager = new cpManagerClass(cpManagerClif);
  }
  catch (...) {
    printf("\n ERROR: initializing  cpManager for beam " ); return false;
  }
  */
  return true;
}
/* *********************************************************************************** */
