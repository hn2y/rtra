#ifndef ROIMODEL_H
#define ROIMODEL_H
#include <iostream>
#include "Utilities/someHelpers.h"
#include <vector>
#include "dicomParser/pointInPolygon.h"
#include <limits>       // std::numeric_limits

#ifdef UNICODE
#undef UNICODE
#endif
#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmrt/drtimage.h"
#include "dcmtk/dcmrt/drtstrct.h"
//#include "dcmtk/dcmrt/drtplan.h"
//#include "dcmtk/dcmrt/drtdose.h"
//#include "dcmtk/dcmdata/dcfilefo.h"
//#include "dcmtk/dcmrt/drmstrct.h"
//#include "dcmtk/dcmrt/drttreat.h"


// #include "dicomParser/RtStructure.h"




class Roi
{
public:
    Roi();
    Roi(std::ostream &stream);
    //Roi(std::ostream &stream, RtStructure *parent);

    ~Roi();


    /// will contain the bounding box coordinates of each OAR
    struct boundingBox
    {
#ifdef __linux__

        double maxX=-std::numeric_limits<double>::max();
        double maxY=-std::numeric_limits<double>::max();
        double maxZ=-std::numeric_limits<double>::max();
        double minX=std::numeric_limits<double>::max();
        double minY=std::numeric_limits<double>::max();
        double minZ=std::numeric_limits<double>::max();
#elif defined _WIN32 || defined _WIN64



        double maxX=-(std::numeric_limits<double>::max)();
        double maxY=-(std::numeric_limits<double>::max)();
        double maxZ=-(std::numeric_limits<double>::max)();
        double minX=(std::numeric_limits<double>::max)();
        double minY=(std::numeric_limits<double>::max)();
        double minZ=(std::numeric_limits<double>::max)();
#else
#error "unknown platform"
#endif


    };



    /// ROI Name
    std::string RoiName;

    /// ROI Number in DICOM
    Sint32 RoiNumber;

    const char * rtStructFileName; // TODO: probably we don't need this

    const char *associateCTDicomFile; // will be used to match the coordinates of point wrt CT files


    /// Is Target
    bool IsTargetVolume;

    /// the number of corresponding RoiContourSequenceItem in DICOM file
    Sint32 ItemNumberInRoiContourSequence;

    ///the bounding box coordinates of each OAR
    boundingBox boundingBoxForROI;

    /// ROI's Center of mass
    Point3 RoiCOM;


    /// dose Array Bounding box
    boundingBox doseArrayBoundingBox;

    /// PointCloud resolution for the ROI
    double resolutionPointClouds;


    ///  vector of grid points (X position) for this ROI
    std::vector<double> gridXpos;
    ///  vector of grid points (Y position) for this ROI
    std::vector<double> gridYpos;
    ///  vector of grid points (Z position) for this ROI
    std::vector<double> gridZpos;

    /// contourDICOMIndexForUniqueSlices[m][k]  m = UniqueSlice index , k
    std::vector<std::vector<int>> contourDICOMIndexForUniqueSlices;

    /// TODO: description
    std::vector<int>  correspondingUniqueSliceIdxForEachContourInDICOMList;

    /// sliceThickness --> gives  thickness of all unique slices associated for this ROI, the length of sliceThickness[n] vector is equal to number of unique slices in this roi
    std::vector<double> thicknessOfSlicesWithUniqueZValue;

    /// pointIndicesInsideContoursOfEachSlice[m][k] --> it contains indices of grid points inside contours kth which share unique slice m  , k =1:number of counters on unique slice m
    std::vector<std::vector<std::vector<int>>> point2DIndicesInsideContoursOfEachSlice;


    /// CorrespondingUniqueContourPlaneIdxForGridZpos[m] --> gives Cooresponding Unique slice index of this Roi for grid points at gridZposROIs[m] z-level
    std::vector<int> CorrespondingUniqueContourPlaneIdxForGridZpos;

    /// pointIndicesInsideContoursOfEachSlice[n][m]-->  it contains global indices of grid points inside polygon at z-level = gridZposROIs[m]
    std::vector<std::vector<int>>  point3DIndicesInsideContourAtGridZposlevel;

    /// pointIndicesInsideContoursOfEachSlice--> All the 3D indices of points inside the ROI
    std::vector<int> point3DIndicesInsideROI;

    /// Unique z values in each ROI Contour sets
    std::vector<double> UniqueZvalueInEachContourSet;


    /// pointIndicesOnContours[m] --> it contains indices of grid points on the Contours which are sharing unique plane m
    std::vector<int> pointIndicesOnContours;

    /// Points inside the ROI
    std::vector<Point3> inPolygonPoints;
    std::vector<double> inPolygonPointsX;
    std::vector<double> inPolygonPointsY;
    std::vector<double> inPolygonPointsZ;

    /// Volume of each voxel
    std::vector<double> inPolygonPointsVol;

    /// Z values of the contours
    std::vector<double> zValuesVector;

    /// Contour Sequence for this ROI
    DRTContourSequence contourSeq;

    /// Display Color
    std::vector<unsigned int> DisplayColor;


    // http://stackoverflow.com/questions/23047970/trying-to-merely-simulate-the-matlab-unique-function-in-c
    // http://en.cppreference.com/w/cpp/algorithm/unique_copy
    // http://www.openframeworks.cc/tutorials/c++%20concepts/001_stl_vectors_basic.html
    // http://infoscience.epfl.ch/record/133543/files/RTSTRUCT_Exporter.pdf


    //function

    bool generatePointCloud();



    /// generates gridXpos, gridYpos, gridZpos from resolutionPointClouds and boundingBoxForROI
    void generateGridPoints();

    /// calculates center of mass based on generated point cloud
    bool calculateCOM();


    bool getPatientName(std::string pName);
    bool findMatchingROIContourSequenceMatchingWithROINumber();

    DRTStructureSetIOD * rtstructIODPointer;


    std::vector<int> inPolygonIndices(const std::vector<Point> &pVec, const std::vector<Point> &polygon);



    bool LoadPointCloudFromCSVFile(std::string FilePath);
    bool pnpoly(int nvert, std::vector<double> &vertx, std::vector<double> &verty, double testx, double testy);
    std::vector<int> inPolygonIndices2(const std::vector<Point> &pVec, const std::vector<Point> &polygon);
private:

    /* Parent RT structure set*/
    // RtStructure * parentRtstructure;




    // debug / logging
    std::ostream &logstream;

    // functions
    std::vector<Point3> calculatePointsInPolygonFromIndices(const std::vector<int>& vI, std::vector<double>& xgridPts, std::vector<double>& ygridPts, std::vector<double>& zgridPts);
    void calculatePointsInPolygonFromIndices(const std::vector<int>& vI, std::vector<double>& xgridPts, std::vector<double>& ygridPts, std::vector<double>& zgridPts, std::vector<double>& InPolygonPtsX, std::vector<double>& InPolygonPtsY, std::vector<double>& InPolygonPtsZ);
    bool calculatePoint2DIndicesInsideContoursOfEachSlice();
    std::pair<std::vector<double>, std::vector<int> > uniqueContourZValuesAndRecontructIndices(std::vector<double> &v);
    bool calcPoint3DIndicesInsideROI();
    bool calculateInpolygonPoints();
    bool adjustPolygonPointsWRTPatientPosInCT();

};

#endif // ROIMODEL_H
