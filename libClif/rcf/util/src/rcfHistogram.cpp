#include "rcfHistogram.hpp"
#include <limits.h>
#include <float.h>
#include <fstream>
#include <iostream>

using namespace std;

#define HISTOGRAM_AUTO_SIZE 100 ///< Default number of bins used for auto scaling
#define HISTOGRAM_MIN_SIZE 10   ///< A minimum number of bins required for certain operations
#define HISTOGRAM_EXCLUSION_PERCENT 0.001 ///< Percent of points to treat a part of the tails, range [0, 100]

rcfHistogram::rcfHistogram()
{
    Reset();
}

/**
 *  Resets the histogram data and parameters
 */
void rcfHistogram::Reset()
{
    binCount = 0;
    binSize = 0.0;
    lowerBound = 0;
    upperBound = 0;
    underflow = 0;
    overflow = 0;
    bufferSize = 0;
    histogramBuffer = NULL;
    isLowerBoundUserSelected = false;
    isUpperBoundUserSelected = false;
    areZerosUnderflow = false;
    scalingMethod = AUTOSCALE;
    dataType = UINT8;
}

/**
 *  Sets the upper and lower bounds of the histogram
 */
bool rcfHistogram::SetBounds(float lower, float upper)
{
    if( upper < lower )
    {
        cerr << "ERROR: Lower bound is greater than the upper bound, bounds not set" << endl;
        return false;
    }
    else
    {
        lowerBound = lower;
        upperBound = upper;
        isLowerBoundUserSelected = true;
        isUpperBoundUserSelected = true;
        return true;
    }        
}

/**
 *  Sets the number of bins to be used in the histogram
 */
bool rcfHistogram::SetNumberOfBins(int count)
{
    if( count <= 0 )
    {
        cerr << "ERROR: rcfHistogram::SetNumberOfBins: Number of bins is not greater than zero" << endl;
        return false;
    }
    else
    {
        binCount = count;
        scalingMethod = BINCOUNT;
        return true;
    }
}

/**
 * Sets the size of the bins for the histrogram and sets the scaling method to BINSIZE if
 *  the operation was successful.
 */
bool rcfHistogram::SetSizeOfBins(float size)
{
    if( size <= 0.0 )
    {
        cerr << "ERROR:rcfHistogram::SetSizeOfBins: The provided bin size: " << size
            << " is less then or equal to zero" << endl;
        return false;
    }
    else
    {
        binSize = size;
        scalingMethod = BINSIZE;
        return true;
    }   
}

/**
 *  Returns true if the scaling method was set, false if an unknown type was attempted to be applied
 */
bool rcfHistogram::SetHistogramScalingMethod( HistogramScaling method )
{
    if( (BINCOUNT == method) || ( BINSIZE == method ) || ( AUTOSCALE == method ) )
    {
        scalingMethod = method;
        if( AUTOSCALE == method )
        {
            isLowerBoundUserSelected = false;
            isUpperBoundUserSelected = false;
        }

        return true;
    }   
    else
    {
        cerr << "ERROR: Setting histogram scaling method with an unknown method: " << method << endl;
        return false;
    }
}

/**
 *  Sets up histogram parameters (i.e. bin size/count, bounds)
 */
bool rcfHistogram::SetUpHisogramParameters(void* dataBuffer, int bufferSize, float min, float max )
{
    // initialize min/max values to opposite extremes
    float minValue = max;
    float maxValue = min;

    int tempValue = 0;    
    for( int i = 0; i < bufferSize; i++ )
    {
        switch(dataType)
        {
            case UINT8:
                tempValue = ((uint8*)dataBuffer)[i];
                break;
            case UINT16:
                tempValue = ((uint16*)dataBuffer)[i];
                break;
            case UINT32:
                tempValue = ((uint32*)dataBuffer)[i];
                break;
            case FLOAT32:
                tempValue = ((float32*)dataBuffer)[i];
                break;
            default:
                cerr << "ERROR:rcfHistogram::GetHistogram: Unsupported data type, not UINT8, UINT16, UINT32, or FLOAT32" << endl;
                return false;
                break;
        }

        if( tempValue < minValue )
        {
            minValue = tempValue;
        }
        if( tempValue > maxValue )
        {
            maxValue = tempValue;
        }
    }

    // set the upper and lower histogram bounds if they were not selected by the user
    if( !isLowerBoundUserSelected )
    {
        lowerBound = minValue;
    }
    if( !isUpperBoundUserSelected )
    {
        upperBound = maxValue;
    }

    // set bin count and sized
    if( BINCOUNT == scalingMethod )
    {
        binSize = (upperBound - lowerBound) / (float)binCount;
        cout << upperBound << " " << lowerBound << " " << binCount << " " << binSize << endl;
    }
    else if( BINSIZE == scalingMethod )
    {
        binCount = (upperBound - lowerBound) / binSize;
    }
    else if( AUTOSCALE == scalingMethod )
    {
        if(!SetAutoParameters())
        {
            cerr << "ERROR:rcfHistogram::SetUpHisogramParameters: Could not set parameters" << endl;
            return false;
        }
    }
    else
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: The scaling method: " << scalingMethod
            << " is an unknown type" << endl;
        return false;
    }

    return true;
}

/**
 *  Creates a histogram using an image
 */
bool rcfHistogram::GenerateHistogram(cfImage* image)
{
    int nx, ny, nz;
    if( CF_OK != image->GetImageSpatialDimensions(&nx, &ny, &nz) )
    {
        cerr << "ERROR:rcfHistogram::GenerateHistogram: Could not get image dimensions" << endl;
        return false;
    }
    bufferSize = nx * ny *nz;

    int valid = 0;
    void* imageBuffer = image->GetImageHandle(&valid);
    if( VALID != valid )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not get the image buffer pointer" << endl;
        return false;
    }

    int byteSize = 0;
    if( CF_OK != image->GetBytesPerPixel(&byteSize) )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not get image byte size" << endl;
        return false;
    }
    switch(byteSize)
    {
        case 1:
            dataType = UINT8;
            break;
        case 2:
            dataType = UINT16;
            break;
        case 4:
            dataType = UINT32;
            break;       
        default:
            cerr << "ERROR:rcfHistogram::GetHistogram: Unsupported byte count per word (bytes_pix: " << byteSize << "). Supports 1,2,4 only" << endl;
            return false;
            break;
    }

    if(!SetUpHisogramParameters(imageBuffer, bufferSize, (float)INT_MIN, (float)INT_MAX ) )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not set up parameters" << endl;
        return false;
    }

    if( AUTOSCALE == scalingMethod )
    {
        if(!FillHistogramBuffer(imageBuffer, true))
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
            return false;
        }
    }
    else
    {
        if(!FillHistogramBuffer(imageBuffer, false))
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
            return false;
        }
    }

    return true;
}

/**
 *  Creates a histogram using a dose
 */
bool rcfHistogram::GenerateHistogram(cfDose* dose)
{
    int nx, ny, nz;
    if( CF_OK != dose->getSizeOfGrid(&nx, &ny, &nz) )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not get dose grid size" << endl;
        return false;
    }
    bufferSize = nx * ny *nz;

    int valid = 0;
    void* doseBuffer = dose->getDose(&valid);
    if( VALID != valid )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not get the dose buffer pointer" << endl;
        return false;
    }
    
    dataType = FLOAT32;
    if( !SetUpHisogramParameters(doseBuffer, bufferSize, FLT_MIN, FLT_MAX ) )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not set up the histogram parameters" << endl;
        return false;
    }

    if( AUTOSCALE == scalingMethod )
    {
        if( !FillHistogramBuffer(doseBuffer, true) )
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
        }
    }
    else
    {
        if( !FillHistogramBuffer(doseBuffer, false) )
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
        }
    }

    return true;
}

/**
 *  Creates a histogram using a floating point buffer 
 */
bool rcfHistogram::GenerateHistogram(float* buf, int length)
{
    if( NULL == buf )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: NULL pointer passed by user in lieu of buffer" << endl;
        return false;
    }
    
    dataType = FLOAT32;
    if( !SetUpHisogramParameters((void*)buf, length, FLT_MIN, FLT_MAX ) )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: Could not set up the histogram parameters" << endl;
        return false;
    }

    if(length <= 0 )
    {
        cerr << "ERROR:rcfHistogram::GetHistogram: User passed invalid length of buffer (negative or zero)" << endl;
        return false;
    }

    bufferSize = length;

    if( AUTOSCALE == scalingMethod )
    {
        if( !FillHistogramBuffer((void*)buf, true) )
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
        }
    }
    else
    {
        if( !FillHistogramBuffer((void*)buf, false) )
        {
            cerr << "ERROR:rcfHistogram::GetHistogram: Could not fill the histogram buffer" << endl;
        }
    }

    return true;
}

/**
 *  Sets the binCount and binSize for automatic scaling
 */
bool rcfHistogram::SetAutoParameters()
{
    binCount = HISTOGRAM_AUTO_SIZE;
    binSize = (upperBound - lowerBound) / (float)binCount;
    
    if( binSize <= 0 )
    {
        cerr << "ERROR:rcfHistogram::SetAutoParameters: binSize <= 0.0" << endl;
        return false;
    }
    return true;
}

/**
 *  Sets the lower bound for the histogram
 */
void rcfHistogram::SetLowerBound(float lower)
{
    lowerBound = lower;
    isLowerBoundUserSelected = true;
}

/**
 *  Sets the upper bound for the histogram
 */
void rcfHistogram::SetUpperBound(float upper)
{
    upperBound = upper;
    isUpperBoundUserSelected = true;
}

/**
 *  Save tab delineated histogram file using seperate path and file names
 */
bool rcfHistogram::SaveHistogram(string path, string filename)
{
    string filePath = path + "/" + filename;
    return SaveHistogram(filePath);
}

/**
 *  Save tab delineated histogram file using a combined path and filename string
 */
bool rcfHistogram::SaveHistogram(string fullFilePath)
{
    ofstream fileWriter;
    fileWriter.open((char*)fullFilePath.c_str());
    if( !fileWriter.is_open() )
    {
        cerr << "ERROR:rcfHistogram::SaveHistogram: Could not open file: " << fullFilePath << endl;
        return false;
    }

    fileWriter << "Underflow\t" << underflow << endl;
    fileWriter << "Overflow\t" << overflow << endl;

    for( int i = 0; i < binCount; i++ )
    {
        fileWriter << (lowerBound + (i * binSize)) << "\t" << histogramBuffer[i] << endl;
    }
    fileWriter.close();
    return true;
}

/**
 *  Fills the histogram buffer's bins
 */
bool rcfHistogram::FillHistogramBuffer(void* imageBuffer, bool removeUpperTail)
{
    float calculateRange = binCount * binSize;

    // update bounds to fit the (binCount * binSize) range
    if( calculateRange != (upperBound - lowerBound) )
    {
        cerr << "Adjusting the upper bounds to fit the (bin count * bin size) range" << endl;
        float difference = (upperBound - lowerBound) - calculateRange;        
        upperBound -= difference;
    }    

    float tempValue = 0;
    // clear overflow/underflow
    overflow = 0;
    underflow = 0;

    // clear the histogram buffer
    if( NULL != histogramBuffer)
    {
        delete[] histogramBuffer;
        histogramBuffer = NULL;
    }

    // check memory for the histogram can be allocated
    if( NULL == (histogramBuffer = new int[binCount]) )
    {
        cerr << "ERROR:rcfHistogram::FillHistogramBuffer: Could not allocate memory for the histogram" << endl;
        return false;
    }
    else
    {
        memset(histogramBuffer, 0, binCount * sizeof(int));
    }

    // assign values to the historgram bins
    for( int i = 0; i < bufferSize; i++ )
    {        
        switch(dataType)
        {
            case UINT8:
                tempValue = ((uint8*)imageBuffer)[i];
                break;
            case UINT16:
                tempValue = ((uint16*)imageBuffer)[i];
                break;
            case UINT32:
                tempValue = ((uint32*)imageBuffer)[i];
                break;
            case FLOAT32:
                tempValue = ((float32*)imageBuffer)[i];
                break;
            default:
                cerr << "ERROR:rcfHistogram::FillHistogramBuffer: Unsupported data type, not UINT8, UINT16, UINT32, or FLOAT32" << endl;
                return false;
                break;
        }


        // assign values to bins
        if( tempValue < lowerBound )
        {
            underflow++;
        }
        else if( tempValue >= upperBound )
        {
            overflow++;
        }
        else if( areZerosUnderflow && (0.0 == tempValue) )
        {
            underflow++;
        }
        else
        {                     
            if( (int)((tempValue - lowerBound) / binSize) >= binCount )
            {
               return false;
            }
            histogramBuffer[(int)((tempValue - lowerBound) / binSize)]++;
        }
    }
    
    // Check if we should remove the tails and if there are enough bins to do so
    if( removeUpperTail && (binCount > HISTOGRAM_MIN_SIZE) )
    {
        // factor to scale percent for multiplication
        const float scaleFactor = 0.01;
        int excludeCount = bufferSize * HISTOGRAM_EXCLUSION_PERCENT * scaleFactor;

        // find upper limit of values to exclude
        int upperCount = 0;
        for( int i = binCount - 1; i >= 0; i-- )
        {
            upperCount += histogramBuffer[i];
            if( upperCount > excludeCount )
            {
                upperBound = ((i + lowerBound) * binSize);
                if( upperBound < lowerBound )
                {
                    cerr << "ERROR:rcfHistogram::FillHistogramBuffer: Auto generated upper bound is less than user specified lower bound" << endl;
                    return false;
                }
                break;
            }
        }
        // set new histogram bounds
        if(!SetAutoParameters())
        {
            cerr << "ERROR:rcfHistogram::FillHistogramBuffer: Could not set histogram parameters" << endl;
            return false;
        }

        // re-fill the buffer with new the new bounds applied
        if(!FillHistogramBuffer(imageBuffer, false))
        {
            cerr << "ERROR:rcfHistogram::FillHistogramBuffer: Could not fill the histogram buffer" << endl;
            return false;
        }
    }
    return true;
}
