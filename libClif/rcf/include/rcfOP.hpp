#ifndef RCF_OP_HPP
#define RCF_OP_HPP

#include "cfTypedefs.hpp"

/** \brief namespace rcfOP
 *
 * The namspace contains all common RCF algorithms.
 */
namespace rcfOP
{
	/** \brief Trilinear interpolation of a single point enclosed inside a cube
	 *
	 * \param p0 the left-bottom point of the cube
	 * \param p1 the right-top point of the cube
	 * \param v  the values on the eight vertices of the cube
	 * \param p  the point whose value to be interpolated
	 * \return the value of the point, p
	 *
	 */
	template<class C, class D>
	inline D trilinearInterpolate(const cfPoint3D<C>& p0, const cfPoint3D<C>& p1,
			D v[2][2][2], const cfPoint3D<C>& p)
	{
		C x, y, z;
		x = (C)(p.x - p0.x)/(C)(p1.x - p0.x);
		y = (C)(p.y - p0.y)/(C)(p1.y - p0.y);
		z = (C)(p.z - p0.z)/(C)(p1.z - p0.z);

		D value = D(v[0][0][0] * (1 - x) * (1 - y) * (1 - z) +
		        v[0][0][1] * (1 - x) * (1 - y) * z       +

		        v[0][1][0] * (1 - x) * y       * (1 - z) +
		        v[0][1][1] * (1 - x) * y       * z       +

		        v[1][0][0] * x       * (1 - y) * (1 - z) +
		        v[1][0][1] * x       * (1 - y) * z       +

		        v[1][1][0] * x       * y       * (1 - z) +
		        v[1][1][1] * x       * y       * z);
                return value;
	}

	/** \brief Trilinear interpolation of a single point enclosed inside a cube
	 *
	 * \param p0 the left-bottom point of the cube
	 * \param p1 the right-top point of the cube
	 * \param v  the values on the eight vertices of the cube
	 * \param p  the point whose value to be interpolated
	 * \return the value of the point, p
	 *
	 */
	template<class C, class D>
	inline D nearestNeighborInterpolate(const cfPoint3D<C>& p0, const cfPoint3D<C>& p1,
			D v[2][2][2], const cfPoint3D<C>& p)
	{
		double x, y, z;
		int xIndex, yIndex, zIndex;
		x = (double)(p.x - p0.x)/(double)(p1.x - p0.x);
		y = (double)(p.y - p0.y)/(double)(p1.y - p0.y);
		z = (double)(p.z - p0.z)/(double)(p1.z - p0.z);

		if(x<0.5) xIndex = 0;
		else  xIndex = 1;

		if(y<0.5) yIndex = 0;
		else  yIndex = 1;

		if(z<0.5) zIndex = 0;
		else  zIndex = 1;

		return v[xIndex][yIndex][zIndex];
	}

	/** \brief test system's endianness
	 * \return true if little endian; false if big endian
	 */
	inline bool isLittleEndian()
	{
	    union
	    {
	       long i;
	       char c[sizeof(long)];
	    };
	    i = 1;
	    return c[0] == 1;
	}

	/** \brief swap byte order
	 * \param b the byte array (input and output)
	 * \param n the size of the byte array
	 */
	inline void byteSwap(unsigned char *b, int n)
	{
		register int i = 0;
		register int j = n-1;

		unsigned char c;
		while(i<j)
		{
			c = b[i];
			b[i] = b[j];
			b[j] = c;
			i++, j--;
		}
	}

	/** \brief array byte array in Sun XDF format
	 * \param b the byte array (input and output)
	 * \param n the size of the byte array
	 *
	 */
	inline void xdrFormat(unsigned char *b, int n)
	{
		if(isLittleEndian()) byteSwap(b, n);
	}
}

#endif
