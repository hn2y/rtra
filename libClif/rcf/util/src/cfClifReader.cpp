#include "cfClif.hpp"
#include "cfClifElement.hpp"
#include "cfClifReader.hpp"
#include "cfClif.hpp"
#include "rcfGlobalMethods.hpp"
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>

using namespace std;

cfClifReader::cfClifReader()
{
    isFileOpen = false;
}

cfClifReader::cfClifReader( string path )
{
    isFileOpen = false;
    OpenClif( path );
    ReadClif();
}

cfClifReader::~cfClifReader()
{
    if( reader.is_open() )
    {
        reader.close();
    }
}

bool cfClifReader::OpenClif( string path )
{
    reader.open( path.c_str() );
    if( reader.is_open() )
    {
        isFileOpen = true;
        return isFileOpen;
    }
    else
    {
        cout << "ERROR:cfClifReader::OpenClif: Failed to open file " << path << endl << flush;
        isFileOpen = false;
        return isFileOpen;
    }
}

void cfClifReader::CloseClif()
{
    if( reader.is_open() )
    {
        reader.close();
    }    
}

bool cfClifReader::ReadClif()
{
    if( !isFileOpen )
    {
        cerr << "ERROR:cfClifReader::ReadClif: Could not read because the file is not open" << endl;
        return false;
    }
    else
    {
        return ReadClif(this);
    }
}

bool cfClifReader::ReadClif( cfClif* clif )
{
    if( !IsOpen() )
    {
        cerr << "ERROR:cfClifReader::ReadClif: Clif file is not open, cannot read" << endl;
        return false;
    }
    
    string line;    
    while (!reader.eof() )
    {
        line = ReadLine();        

        if( string::npos != line.find("[] ={") )
        {
            cfClifArray* tempArray = new cfClifArray();
            string handle = line.substr(0, line.find("[] ={"));
            TrimString(handle);
            tempArray->SetHandle(handle);

            string openBrace = "[] ={";
            string closingBrace = "};";
            unsigned int openBraceIndex = line.find((char*)openBrace.c_str());             
            if( string::npos != openBraceIndex )
            {
                line = line.substr(openBraceIndex + openBrace.length(), line.length() - openBraceIndex + openBrace.length());
            }
            
            bool endingBraceFound = false;
            while( !endingBraceFound )
            {
                TrimString(line);
                if( 0 == line.compare(closingBrace) )
                {
                    endingBraceFound = true;
                    break;
                }
                               
                while( line.length() > 0 )
                {
                    TrimString(line);
                    unsigned long commaIndex = line.find(",");
                    if( string::npos == commaIndex )
                    {
                        if( 0 == line.compare(closingBrace) )
                        {
                            endingBraceFound = true;
                            break;
                        }
                        else
                        {
                            unsigned long closingIndex = string::npos;
                            if( (closingIndex = (string::npos != line.find(closingBrace))) )
                            {
                                line = line.substr(0, closingIndex);
                            }
                            else
                            {
                              //  do nothing
                            }
                            tempArray->AddElement(line);
                            break;
                        }
                    }
                    else
                    {
                        string tempElement = line.substr(0, commaIndex);
                        tempArray->AddElement(tempElement);
                        if( line.size() - commaIndex >= commaIndex + 1 )
                        {
                            line = line.substr(commaIndex + 1, line.size() - commaIndex);
                        }
                        else
                        {
                            line = "";
                        }
                    } 
                }
                if( !reader.eof() )
                {
                    line = ReadLine();
                }
            }
            
            clif->AddArray( tempArray );
        }
        else if( string::npos != line.find("={") )
        {
            cfClif* currentClif = new cfClif();
            string handle = line.substr(0,line.find("={"));   
            TrimString(handle);            
            currentClif->SetHandle( handle );
            clif->clifList.push_back(currentClif);            
            ReadClif( currentClif );            
        }
        // if we got here the current line is not a clif or an array, it must be an element
        else if( string::npos != line.find("=") || string::npos != line.find(":") )
        {
            cfClifElement* currentElement = new cfClifElement(line);
            clif->data.push_back(currentElement);
        }
        // maybe make this a more generic process w/o clif names
        else if( 0 == clif->GetHandle().compare("points")
                || 0 == clif->GetHandle().compare("triangles") 
                || 0 == clif->GetHandle().compare("vertices") )
        {
            cfClifTripleList* tmpList = new cfClifTripleList();           
            clif->tripleList = tmpList;            
            while( string::npos == line.find("};") )
            {                
                if(! tmpList->AddTriple(line) )
                {
                    cout << "ERROR: Could not add triple with line: " << line << endl;                    
                }
                if( !reader.eof() )
                {
                    line = ReadLine();
                }
            }
            
            if( string::npos != line.find("};") )
            {
                return true;
            }
        }
        else if( string::npos != line.find("};") )
        {
            return true;
        } 
    }

    return true;
}

string cfClifReader::ReadLine()
{    
    string line;
    if( reader.eof() )
    {
        return "";
    }
    getline(reader, line);
           
    TrimString(line);
    
    // what are the rules about comments?
    string commentString;
    if( line.length() > 1 )
    {
        commentString = line.substr(0, 2);
        if( commentString == "//" )
        {
            line = "";
        }
        else if( commentString == "/*")
        {
            while( line.find("*/") == string::npos )
            {
                getline(reader, line);
            } 
            line = "";
        }
    }
    
    string currentLine = line;
    if( 0 == currentLine.length() && !reader.eof() )
    {        
        currentLine = ReadLine();
    }
    
    return currentLine;
}
