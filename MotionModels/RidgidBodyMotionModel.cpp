#include "RidgidBodyMotionModel.h"
RigidBodyMotionModel::RigidBodyMotionModel(std::ostream &stream):logstream(stream)
{
    domXmlDoc=0;
    associatedRoiID= "َ";
    guModelFile ="";
    guModelType ="RigidBodyMotion";
    rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    rotCenter.RoiIDForRotationAroundCOM=associatedRoiID;
    sysRotErrorSigma.x =0;
    sysRotErrorSigma.y =0;
    sysRotErrorSigma.z =0;

    randRotErrorSigma.x =0;
    randRotErrorSigma.y =0;
    randRotErrorSigma.z =0;

    sysTranErrorSigma.x =0;
    sysTranErrorSigma.y =0;
    sysTranErrorSigma.z =0;

    randTranErrorSigma.x =0;
    randTranErrorSigma.y =0;
    randTranErrorSigma.z =0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    DoseConvFlag=false;
}

RigidBodyMotionModel::RigidBodyMotionModel():logstream(std::cout)
{
    domXmlDoc=0;
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="RigidBodyMotion";
    rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    rotCenter.RoiIDForRotationAroundCOM=associatedRoiID;
    sysRotErrorSigma.x =0;
    sysRotErrorSigma.y =0;
    sysRotErrorSigma.z =0;

    randRotErrorSigma.x =0;
    randRotErrorSigma.y =0;
    randRotErrorSigma.z =0;

    sysTranErrorSigma.x =0;
    sysTranErrorSigma.y =0;
    sysTranErrorSigma.z =0;

    randTranErrorSigma.x =0;
    randTranErrorSigma.y =0;
    randTranErrorSigma.z =0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;

    intraFractionRotErrorSigma.x=0;
    intraFractionRotErrorSigma.y=0;
    intraFractionRotErrorSigma.z=0;


    DoseConvFlag=false;
}
RigidBodyMotionModel::~RigidBodyMotionModel()
{

}

bool RigidBodyMotionModel::loadModelFromXMLFile(char *filename )
{
    // TODO : Implement this
    std::string class_member = "RidgidBodyMotionModel::loadModelFromFile:";

    //TODO: change it to the default stream logstream
    logstream << class_member<< "Failed to load the config file for reading." << std::endl;
    return false;
}

/// the input for this function should be input config document which contains
/// all the information about the RidgidBodyMotionModel GUM models that will be applied to all the ROIs.
bool RigidBodyMotionModel::loadModelFromXMLDocument(QDomDocument* document)
{
    std::string class_member = "RidgidBodyMotionModel::loadModelFromXMLDocument:";
    logstream << class_member<< "Loading the RidgidBody information from XML document." << std::endl;


    //sysRotErrorSigma.x = ;
    //randRotErrorSigma;
    //sysTranErrorSigma;
    //randTranErrorSigma;
    //DoseConvFlag=flase;


    return false;
}

bool RigidBodyMotionModel::writeXML()
{
    // TODO : implement to write a xml file
    std::string class_member = "RidgidBodyMotionModel::writeXML:";
    logstream << class_member<< "Not implemented yet." << std::endl;
    return false;

}

