#ifndef CF_DVF3D_HPP
#define CF_DVF3D_HPP

#include "cfObject3d.hpp"
#include "rcfOP.hpp"

/** \class cfDvf3d
 * \brief Represent a 3-D DVF object on a rectilinear grid box.
 *
 * \author Baoshe Zhang
 */
template<class C>
class cfDvf3d: public cfObject3d
{
public:
	/** \brief constructor
	 *
	 * \warning DVF and coordinate system are not set. use setCoord to set coordinate sytem and use setDvf to set DVF
	 */
	cfDvf3d(): _isAlloc(false), _isPtrSet(false) { }

	/** \brief constructor
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \warning DVF is not set. use setDvf to set DVF
	 */
	cfDvf3d(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
					: _isAlloc(false), _isPtrSet(false)
	{
		_coord.setCoordAxis(x, y, z);
	}

	/** \brief constructor
	 *
	 * \param coord a 3-D cfCoord3d coordinate system
	 * \warning DVF is not set. use setDvf or setDvfPtr to set DVF
	 */
	cfDvf3d(const cfCoord3d<C>& coord)
					: _isAlloc(false), _isPtrSet(false)
	{
		_coord = coord;
	}

	/** \brief constructor
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \param pDvf a DVF pointer of data array containing all DVF values.
	 */
	cfDvf3d(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z,
			const cfPoint3D<C*>& pDvf) : _isAlloc(false), _isPtrSet(false)
	{
		_coord = cfCoord3d<C>(x, y, z);
		setDvf(pDvf);
	}

	/** \brief constructor
	 *
	 * \param coord a cfCoord3d coordinate system
	 * \param pDvf a DVF pointer of data array containing all DVF values
	 * \param pDvf a DVF pointer of data array containing all DVF values.
	 */
	cfDvf3d(const cfCoord3d<C>& coord, const cfPoint3D<C*>& pDvf) : _isAlloc(false), _isPtrSet(false)
	{
		_coord = coord;
		setDvf(pDvf);
	}

	/** \brief copy constructor (deep)
	 *
	 * \param dvf a cfDvf3d
	 */
	cfDvf3d(const cfDvf3d<C>& dvf): _isAlloc(false), _isPtrSet(false)
	{
		*this = dvf;
	}

	/** \brief deconstructor
	 * if memory is allocated to DVF, it will be deleted here
	 */
	~cfDvf3d()
	{
		if(_isAlloc)
		{
			delete[] _pDvf.x;
			delete[] _pDvf.y;
			delete[] _pDvf.z;
			_isAlloc = false;
		}
	}

	/** \brief operator =
	 * \param dvf a cfDvf3d
	 */
	void operator=(const cfDvf3d<C>& dvf)
	{
		_coord = dvf.getCoord3d();
		setDvf(dvf.getDvfPtr());
	}

	/** \brief set the coordinate system for the DVF
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \warning no DVF is set. use setDvf or setDvfPtr to assign DVF.
	 */
	void setCoordAxis(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
	{
            if(_isAlloc)
            {
                delete[] _pDvf.x;
                _pDvf.x = NULL;
                delete[] _pDvf.y;
                _pDvf.y = NULL;
                delete[] _pDvf.z;
                _pDvf.z = NULL;
            }
            _isAlloc = false;
            _isPtrSet = false;
            _coord.setCoordAxis(x, y, z);
	}

	/** \brief set the coordinate system for the DVF
	 *
	 * \param coord a cfCoord3d coordinate system
	 * \warning no DVF is set. use setDvf or setDvfPtr to assign DVF.
	 */
	void setCoordAxis(const cfCoord3d<C>& coord)
	{
		if(_isAlloc)
		{
			delete[] _pDvf.x;
			delete[] _pDvf.y;
			delete[] _pDvf.z;
		}
		_isAlloc = false;
		_isPtrSet = false;
		_coord = coord;
	}

	/** \brief set the values of the current volume
	 *
	 * \param pDvf a DVF pointer.
	 * \warning if no memory allocated for the volume, first allocate memory
	 */
	void setDvf(const cfPoint3D<C*>& pDvf)
	{
            if(!_coord.isValid())
            {
                    std::cerr << "Error: The 3-d coordinate system has not been set(cfDvf3d::setDvf)!!!" << std::endl;
                    return;
            }

            int numPoints = _coord.getNumPoint();

            if(!_isAlloc) // if no memory allocated, allocated memory.
            {
                try
                {
                    _pDvf.x = new C[numPoints];
                    _pDvf.y = new C[numPoints];
                    _pDvf.z = new C[numPoints];
                    _isAlloc = true;
                }
                catch(std::bad_alloc&) // if memory allocate fails, exit
                {
                    _isAlloc = false;
                    std::cerr << "Error: Memory Allocation for DVF(cfDvf3d::setDvf)!!!" << std::endl;
                    return;
                }
            }

            int i;
            for(i=0; i<numPoints; i++)
            {
                _pDvf.x[i] = pDvf.x[i];
                _pDvf.y[i] = pDvf.y[i];
                _pDvf.z[i] = pDvf.z[i];
            }

            _isPtrSet = _isAlloc;    // _isPtrSet means _isAlloc
	}

	/** \brief reset DVF to a specific value, and allocate memory
	 *
	 * \param dvf the new DVF value
	 */
	void resetDvf(const cfPoint3D<C>& dvf)
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The 3-d coordinate system has not been set(cfDvf3d::resetDvf)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            if(!_isAlloc && !_isPtrSet) // if no memory allocated and no pointer is set, allocated memory.
            {
                try
                {
                    _pDvf.x = new C[numPoints];
                    _pDvf.y = new C[numPoints];
                    _pDvf.z = new C[numPoints];
                    _isAlloc = true;
                    _isPtrSet = _isAlloc;    // _isPtrSet means _isAlloc
                }
                catch(std::bad_alloc&) // if memory allocate fails, exit
                {
                    _isAlloc = false;
                    std::cerr << "Error: Memory Allocation for value of cfVolume3d(cfDvf3d::resetDvf)!!!" << std::endl;
                    return;
                }
            }

            int i;
            for(i=0; i<numPoints; i++)
            {
                _pDvf.x[i] = dvf.x;
                _pDvf.y[i] = dvf.y;
                _pDvf.z[i] = dvf.z;
            }
        }

	/** \brief set a DVF value to a coordinate point specified by an index
	 *
	 * \param index the index to specify a coordinate point
	 * \param dvf the dvf to be set
	 */
	void setDvfAt(const cfPoint3D<int>& index, const cfPoint3D<C>& dvf) const
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The 3-d coordinate system has not been set(cfDvf3d::setDvfAt)!!!" << std::endl;
                return;
            }

            if(!_isPtrSet)  // if no memory allocated, exit (illegal memory access).
            {
                std::cerr << "Error: Memory has not allocated for cfVolume3d(cfDvf3d::setDvfAt)"
                             "or DVF pointer is not set!!!" << std::endl;
                return;
            }

            if(!_coord.isInRange(index))
            {
                std::cerr << "Error: Invalid index(cfDvf3d::setDvfAt)" << std::endl;
                return;
            }

            cfPoint3D<int> dim = _coord.getNDim();

            int vIndex = INDEX3D(index, dim);

            _pDvf.x[vIndex] = dvf.x;
            _pDvf.y[vIndex] = dvf.y;
            _pDvf.z[vIndex] = dvf.z;
	}

	/** \brief assign three pointers to the DVF data arrays of the volume
	 *
	 * \param pDvf a DVF pointer.
	 * \warning if the memory is allocated to the volume, it will be deleted
	 */
	void setDvfPtr(const cfPoint3D<C*>& pDvf)
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The 3-d coordinate system has not been set(cfDvf3d::setDvfPtr)!!!" << std::endl;
                return;
            }

            if(_isAlloc) // if memory is allocated for DVF, delete them
            {
                delete[] _pDvf.x;
                delete[] _pDvf.y;
                delete[] _pDvf.z;
                _isAlloc = false;
            }
            _pDvf = pDvf;
            _isPtrSet = true;
	}

	/** \brief get the values of DVF of the volume
	 *
	 * \param pDvf a DVF pointer, specify a data array store the DVF values.
	 * \warning use INDEX3D(index, DIM) to get a 1-D index number
	 */
	void getDvf(cfPoint3D<C*> pDvf) const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The DVF values or pointer have not been set(cfDvf3d::getDvf)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

	    int i;

	    for(i=0; i<numPoints; i++)
            {
                *(pDvf.x+i) = *(_pDvf.x+i);
                *(pDvf.y+i) = *(_pDvf.y+i);
                *(pDvf.z+i) = *(_pDvf.z+i);
            }
	}

	/** \brief get the pointer to the DVF value arrays of the current volume
	 *
	 * \return the DVF pointer
	 * \warning. use INDEX3D(index, DIM) to access the DVF value of an index
	 */
	cfPoint3D<C*> getDvfPtr() const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The DVF pointers have not been set(cfDvf3d::getDvfPtr)!!!" << std::endl;
                return cfPoint3D<C*>(NULL, NULL, NULL);
            }
            return _pDvf;
	}

	/** \brief get the DVF value of a coordinate point specified by index
	 *
	 * \param index the index
	 */
	cfPoint3D<C> getDvfAt(const cfPoint3D<int>& index) const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The value pointer has not been set(cfDvf3d::getDvfAt)!!!" << std::endl;               
                return cfPoint3D<C>((C)0, (C)0, (C)0);
            }

            if(!_coord.isInRange(index))
            {
                std::cerr << "Error: Invalid index(cfDvf3d::getDvfAt)!!!" << std::endl;
                return cfPoint3D<C>((C)0, (C)0, (C)0);
            }

            cfPoint3D<int> dim = _coord.getNDim();

            int vIndex = INDEX3D(index, dim);

            cfPoint3D<C> dvf;
            dvf.x = _pDvf.x[vIndex];
            dvf.y = _pDvf.y[vIndex];
            dvf.z = _pDvf.z[vIndex];

            return dvf;
	}

	/** \brief get the DVF value of a point
	 *
	 * When the point is not inside the coordinate system, dvf = 0.
	 * \param tgtPoint the point
	 */
	cfPoint3D<C> getDvf(const cfPoint3D<C>& tgtPoint) const
	{
            cfPoint3D<C> point0, point1;
            cfPoint3D<int> index;
            cfPoint3D<C> value, v[2][2][2];

            if(_coord.isInBox(tgtPoint)) // if tgtPoint is inside DVF
            {
                _coord.getCoordIndex(tgtPoint, index);  // get index of the above point in DVF
                _coord.getCoordAt(index, point0); // get the coordinate point of the above index
                _coord.getCoordAt(index+cfPoint3D<int>(1,1,1), point1); // get the diagonal point
                getBoxVertex(index, v);           // get the values of 8 points
                value = dvfTrilinearInterpolate(point0, point1, v, tgtPoint); // triliear interpolation
            }
            else         // if tgtPoint is not inside DVF, value is set to 0
            {
                value = cfPoint3D<C>((C)0, (C)0, (C)0);
            }
            return value;
	}

    cfPoint3D<C> getDvf(const cfPoint3D<C>& tgtPoint, bool& valid) const
	{
        cfPoint3D<C> point0, point1;
        cfPoint3D<int> index;
        cfPoint3D<C> value, v[2][2][2];

        if(_coord.isInBox(tgtPoint)) // if tgtPoint is inside DVF
        {
            _coord.getCoordIndex(tgtPoint, index);  // get index of the above point in DVF
            _coord.getCoordAt(index, point0); // get the coordinate point of the above index
            _coord.getCoordAt(index+cfPoint3D<int>(1,1,1), point1); // get the diagonal point
            getBoxVertex(index, v);           // get the values of 8 points
            value = dvfTrilinearInterpolate(point0, point1, v, tgtPoint); // triliear interpolation
            valid = true;            
        }
        else         // if tgtPoint is not inside DVF, value is set to 0
        {
            value = cfPoint3D<C>((C)0, (C)0, (C)0);
            valid = false;            
        }        
        return value;
	}

	/** \brief get the 3-D coordinate system of the current volume
	 *
	 */
	const cfCoord3d<C>& getCoord3d() const
	{
            return _coord;
	}

	/** \brief save cfDvf3d to an AVS FLD file
	 * \param outfilename the file name of the AVS FLD file
	 *
	 */
	void save(const std::string outfilename)
	{
            int i;  //index
            const bool isUniform = _coord.isUniform();

            /* pointer to the dvf vector of dvf3d */
            cfPoint3D<C*> pDvf;

            /* the data type of value */
            std::string dataType;

            const cfPoint3D<int> dim = _coord.getNDim();
            const int numPoint = _coord.getNumPoint();

            /* set Value */
            if(typeid(C) == typeid(float))
            {
                pDvf = getDvfPtr();
                dataType = "xdr_float";
            }
            else if(typeid(C) == typeid(char) || typeid(C) == typeid(unsigned char))
            {
                pDvf = getDvfPtr();
                dataType = "xdr_byte";
            }
            else if(typeid(C) == typeid(double))
            {
                pDvf = getDvfPtr();
                dataType = "xdr_double";
            }
            else if(typeid(C) == typeid(int) || typeid(C) == typeid(unsigned int))
            {
                pDvf = getDvfPtr();
                dataType = "xdr_integer";
            }
            else if(typeid(C) == typeid(short) || typeid(C) == typeid(unsigned short))
            {
                pDvf = getDvfPtr();
                dataType = "xdr_short";
            }
            else
            {
                std::cerr << "data type not supported!!!" << std::endl;
                return;
            }

            /* create a AVS FLD file */
            std::ofstream out(outfilename.c_str(), std::ios::out | std::ios::binary);
            if(!out.is_open())
            {
                std::cerr << "can not open the file!!!" << std::endl;
                return;
            }
            /* create header */
            out << "# AVS" << std::endl;
            out << "ndim=3" << std::endl;
            out << "dim1=" << dim.x << std::endl;
            out << "dim2=" << dim.y << std::endl;
            out << "dim3=" << dim.z << std::endl;
            out << "nspace=3" << std::endl;
            out << "veclen=3" << std::endl;
            if(dataType == "xdr_short")
            {
                out << "data=xdr_integer" << std::endl;
            }
            else
            {
                out << "data=" << dataType << std::endl;
            }

            if(isUniform) /* uniform field */
            {
                out << "field=uniform" << std::endl;
            }
            else /*rectilinear field */
            {
                out << "field=rectilinear" << std::endl;
            }
            out << "\f\f";


            /* set binary data area */
            /* value */
            if(dataType == "xdr_short")
            {
                int tValue;

                for(i=0; i<numPoint; i++)
                {
                    /* x-component */
                    tValue = pDvf.x[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));

                    /* y-component */
                    tValue = pDvf.y[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));

                    /* z-component */
                    tValue = pDvf.z[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));
                }
            }
            else
            {
                C tValue;

                for(i=0; i<numPoint; i++)
                {
                    /* x-component */
                    tValue = pDvf.x[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));

                    /* y-component */
                    tValue = pDvf.y[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));

                    /* z-component */
                    tValue = pDvf.z[i];
                    rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
                    out.write((char*)&tValue, sizeof(tValue));
                }
            }

            /* coordinate */
            if(isUniform) /* uniform field */
            {
                float tCoord;
                /* Axis X */
                tCoord = _coord.getCoordAxisX().getFirstCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

                tCoord = _coord.getCoordAxisX().getLastCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

                /* Axis Y */
                tCoord = _coord.getCoordAxisY().getFirstCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

                tCoord = _coord.getCoordAxisY().getLastCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

                /* Axix Z */
                tCoord = _coord.getCoordAxisZ().getFirstCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

                tCoord = _coord.getCoordAxisZ().getLastCoord();
                rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
            }
            else /*rectilinear field */
            {
                float tCoord;
                /* Axis X */
                cfCoordAxis<C> axisX = _coord.getCoordAxisX();
                for(i=0; i<dim.x; i++)
                {
                        tCoord = (float)axisX.getCoordAt(i);
                        rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                        out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
                }
                /* Axis Y */
                cfCoordAxis<C> axisY = _coord.getCoordAxisY();
                for(i=0; i<dim.y; i++)
                {
                        tCoord = (float)axisY.getCoordAt(i);
                        rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                        out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
                }
                /* Axix Z */
                cfCoordAxis<C> axisZ = _coord.getCoordAxisZ();
                for(i=0; i<dim.z; i++)
                {
                        tCoord = (float)axisZ.getCoordAt(i);
                        rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
                        out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
                }
            }
            /* close the file */
            out.close();
	}

	/** \brief chain together two dvfs to fill up the current dvf volume
	 *
	 * The transform principle: choose a coordinate point (or grid point) from dvf1, p0. If p0
	 * is inside the composite (output) DVF space, calculate the corresponding DVF, dvf1, to get a new point, p1 = p0+dvf1;
	 * otherwise, dvf1=0, i.e., p1 = p0. If p1 is inside dvf2, get the dvf2 at p1; otherwise, the value is equal to zero.
	 * Calculate p2 = p1 + dvf2 = p0 + dvf1 + dvf2. Here, when p0 is not a grid point of dvf1, or p1 is not a grid point of dvf2,
	 * a fitting method will be used.
	 * \param dvf3d_1 the first 3-D DVF
	 * \param dvf3d_2 the second 3-D DVF
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
	void chain(const cfDvf3d<C>& dvf3d_1, const cfDvf3d<C>& dvf3d_2, const std::string interpMethod)
	{

            int i, j, k;

            cfPoint3D<C> zip(0,0,0);
            resetDvf(zip); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> p0, p1, p2;
            cfPoint3D<int> p0Index, p1Index, p2Index;
            cfPoint3D<C> dvf1, dvf2;

            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        p0Index = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(p0Index, p0);			// get the coordinate point from current composite DVF
                        dvf1 = dvf3d_1.getDvf(p0);				// get DVF at the current point
                        p1 = p0 + dvf1;							// get the deformed point, p1, after dvf1

                        dvf2 = dvf3d_2.getDvf(p1);				// get the DVF at the new point p1
                        p2 = p1 + dvf2;							// calculate the new point p2

                        setDvfAt(p0Index, p2-p0);				// new composite dvf is difference between point 2 and 0.
                    }
                }
            }
	}
	
	/** \brief chain together two dvfs to fill up the current dvf volume, including rigid transforms
	 *
	 * The transform principle: choose a coordinate point (or grid point) from dvf1, p0. If p0
	 * is inside the composite (output) DVF space, calculate the corresponding DVF, dvf1, to get a new point, p1 = p0+dvf1;
	 * otherwise, dvf1=0, i.e., p1 = p0. If p1 is inside dvf2, get the dvf2 at p1; otherwise, the value is equal to zero.
	 * Calculate p2 = p1 + dvf2 = p0 + dvf1 + dvf2. Here, when p0 is not a grid point of dvf1, or p1 is not a grid point of dvf2,
	 * a fitting method will be used.
	 * \param dvf3d_1 the first 3-D DVF
	 * \param mat_1 the first DVF rigid transform matrix
	 * \param isXformFirst_1 forward or reverse transform for first DVF
	 * \param dvf3d_2 the second 3-D DVF
	 * \param mat_2 the second DVF rigid transform matrix
	 * \param isXformFirst_2 forward or reverse transform for second DVF
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
	void chain(const cfDvf3d<C>& dvf3d_1, const cfMatrix4<C>& mat_1, bool isXformFirst_1, const cfDvf3d<C>& dvf3d_2, const cfMatrix4<C>& mat_2, bool isXformFirst_2, const std::string interpMethod)
	{

            int i, j, k;

            cfPoint3D<C> zip(0,0,0);
            resetDvf(zip); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> p0, p0_rigid, p1, p1_rigid, p2;
            cfPoint3D<int> p0Index, p1Index, p2Index;
            cfPoint3D<C> dvf1, dvf2;

			bool valid_1 = false;
			bool valid_2 = false;

            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        p0Index = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(p0Index, p0);			// get the coordinate point from current composite DVF
                        
						cfMatrix4<C> p, Tp;
                        p.null();                        
                        if( isXformFirst_1 )
                        {
                            //use cf_matrix4D to apply transformation
                            //assign p0 to first column of a matrix p
                            p.element[0][0] = p0.x;
                            p.element[1][0] = p0.y;
                            p.element[2][0] = p0.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = mat_1 * p;
                            p0_rigid.x = (C)Tp.element[0][0];
                            p0_rigid.y = (C)Tp.element[1][0];
                            p0_rigid.z = (C)Tp.element[2][0];

                            dvf1 = dvf3d_1.getDvf(p0_rigid, valid_1);
                            p1 = p0_rigid + dvf1;
                        }
                        else
                        {
                            dvf1 = dvf3d_1.getDvf(p0, valid_1);
                            p0_rigid = p0 + dvf1;

                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = p0_rigid.x;
                            p.element[1][0] = p0_rigid.y;
                            p.element[2][0] = p0_rigid.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = mat_1 * p;
                            p1.x = (C)Tp.element[0][0];
                            p1.y = (C)Tp.element[1][0];
                            p1.z = (C)Tp.element[2][0];
                        }
						
						if( isXformFirst_2 )
                        {
                            //use cf_matrix4D to apply transformation
                            //assign p0 to first column of a matrix p
                            p.element[0][0] = p1.x;
                            p.element[1][0] = p1.y;
                            p.element[2][0] = p1.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = mat_2 * p;
                            p1_rigid.x = (C)Tp.element[0][0];
                            p1_rigid.y = (C)Tp.element[1][0];
                            p1_rigid.z = (C)Tp.element[2][0];

                            dvf2 = dvf3d_2.getDvf(p1_rigid, valid_2);
                            p2 = p1_rigid + dvf2;
                        }
                        else
                        {
                            dvf2 = dvf3d_2.getDvf(p1, valid_2);
                            p1_rigid = p1 + dvf2;

                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = p1_rigid.x;
                            p.element[1][0] = p1_rigid.y;
                            p.element[2][0] = p1_rigid.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = mat_2 * p;
                            p2.x = (C)Tp.element[0][0];
                            p2.y = (C)Tp.element[1][0];
                            p2.z = (C)Tp.element[2][0];
                        }
						
						if(valid_1 && valid_2)
						{
							setDvfAt(p0Index, p2-p0);				// new composite dvf is difference between point 2 and 0.
						}
						else
						{
							setDvfAt(p0Index, 0.0F);
						}
						
                    }
                }
            }
	}

        void fill(const cfDvf3d<C>& srcDvf3d)
	{
            int i, j, k;

            resetDvf(cfPoint3D<C>(0,0,0)); // reset the values of the current volume to 0
            
            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint;
            cfPoint3D<int> tgtIndex, srcIndex;
            cfPoint3D<C> dvf;
            cfPoint3D<float> value;
            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);
                        //dvf = getDvf(srcPoint);  
                        //srcPoint = tgtPoint + dvf;
                        value = srcDvf3d.getDvf(tgtPoint); 
                        setDvfAt(tgtIndex, value);
                    }
                }
            }
	}

        void fillWithTransformation(const cfDvf3d<C>& srcDvf, const cfMatrix4<C>& tform, bool isXformFirst)
        {
            std::cout << "fillWithTransformation: " << tform << std::endl;
            std::cout << "isXformFirst: ";
            if( isXformFirst )
            {
                std::cout << "true" << std::endl;
            }
            else
            {
                std::cout << "false" << std::endl;
            }
            int i, j, k;

            resetDvf(cfPoint3D<C>(0,0,0)); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint, tgtPoint2, transformPoint;
            cfPoint3D<int> tgtIndex, srcIndex;
            cfPoint3D<C> dvf;
            cfPoint3D<C> value;
            bool valid = false;
            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        srcIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(srcIndex, srcPoint);

                        cfMatrix4<C> p, Tp;
                        p.null();
                        if( isXformFirst )
                        {
                            //use cf_matrix4D to apply transformation
                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = srcPoint.x;
                            p.element[1][0] = srcPoint.y;
                            p.element[2][0] = srcPoint.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = tform * p;
                            transformPoint.x = (C)Tp.element[0][0];
                            transformPoint.y = (C)Tp.element[1][0];
                            transformPoint.z = (C)Tp.element[2][0];
                            valid = true;
                            dvf = srcDvf.getDvf(srcPoint, valid);
                            dvf += (transformPoint - srcPoint);

                        }
                        else
                        {
                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = srcPoint.x;
                            p.element[1][0] = srcPoint.y;
                            p.element[2][0] = srcPoint.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = tform * p;
                            transformPoint.x = (C)Tp.element[0][0];
                            transformPoint.y = (C)Tp.element[1][0];
                            transformPoint.z = (C)Tp.element[2][0];
                            valid = true;
                            dvf = srcDvf.getDvf(srcPoint, valid);
                            dvf += (transformPoint - srcPoint);
                        }

                        if(valid)
                        {
                            //value = srcVolume.getValueAt(srcPoint, interpMethod);   // get the value for the new point
                            value = dvf;

                        }
                        else
                        {
                            value = 0;
                        }

                        setDvfAt(srcIndex, value);   // assign interpolated value to tgtVolume
                    }
                }
            }
        }

private:

	/** \brief Trilinear interpolation of a single point enclosed inside a cube
	 *
	 * \param p0 the left-bottom point of the cube
	 * \param p1 the right-top point of the cube
	 * \param v  the values on the eight vertices of the cube
	 * \param p  the point whose value to be interpolated
	 * \return the value of the point, p
	 *
	 */
	cfPoint3D<C> dvfTrilinearInterpolate(const cfPoint3D<C>& p0, const cfPoint3D<C>& p1,
				const cfPoint3D<C> dvf[2][2][2], const cfPoint3D<C>& p) const
	{
            int i, j, k;
            cfPoint3D<C> value;
            C xV[2][2][2], yV[2][2][2], zV[2][2][2];

            for(i=0; i<2; i++)
            {
                for(j=0; j<2; j++)
                {
                    for(k=0; k<2; k++)
                    {
                        xV[i][j][k] = dvf[i][j][k].x;
                        yV[i][j][k] = dvf[i][j][k].y;
                        zV[i][j][k] = dvf[i][j][k].z;
                    }
                }
            }

            value.x = rcfOP::trilinearInterpolate(p0, p1, xV, p);
            value.y = rcfOP::trilinearInterpolate(p0, p1, yV, p);
            value.z = rcfOP::trilinearInterpolate(p0, p1, zV, p);
            return value;
	}

	/** \brief extract the values on eight vertices of a cube of the current volume3d
	 *
	 * \param index the left-bottom point specified by index of the cube box
	 * \param v the values of eight vertices (returned)
	 */
	void getBoxVertex(const cfPoint3D<int>& index, cfPoint3D<C> v[2][2][2]) const
	{
            int i, j, k;
            for(i=0; i<2; i++)
            {
                for(j=0; j<2; j++)
                {
                    for(k=0; k<2; k++)
                    {
                        v[i][j][k] = getDvfAt(index + cfPoint3D<int>(i,j,k));
                    }
                }
            }
	}
       
private:
	cfCoord3d<C> _coord;  // coordinate system
	bool _isAlloc;        // true, memory is allocated
	bool _isPtrSet;       // true, a pointer is set to _xDvf, _yDvf, _zDvf or _isAlloc = true
	cfPoint3D<C*> _pDvf;  // a DVF pointer
}; // cfDvf3d

#endif
