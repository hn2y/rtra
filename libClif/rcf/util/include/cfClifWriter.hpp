#ifndef CFCLIFWRITER_HPP
#define CFCLIFWRITER_HPP

#include "cfClif.hpp"
#include <string>
#include <fstream>

class cfClifWriter
{
    public:
        cfClifWriter();
        cfClifWriter( cfClif* clifToSave );
        ~cfClifWriter();
        int WriteClif();
        void SetClif( cfClif* clifToSave ) { clif = clifToSave; }
        void SetPath( std::string clifPath ) { path = clifPath; }
        void SetFilename( std::string clifFilename ) { filename = clifFilename; }
    private:
        std::string InsertTabs( int level );
        void WriteClifRecursive( cfClif* currentClif, int* stackDepth );
        void WriteArray( cfClif* currentClif, int* stackDepth, int arrayIndex );
        std::string path;
        std::string filename;
        cfClif* clif;
        std::ofstream outputClif;
        int stackDepth;
};
#endif
