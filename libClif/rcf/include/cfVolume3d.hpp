#ifndef CF_VOLUME3D_HPP
#define CF_VOLUME3D_HPP

#include "cfDvf3d.hpp"

/** \class cfVolume3d
 * \brief Represent a 3-D object on a rectilinear grid box.
 *
 * class C is used to represent the data type of the coordinate system, class D to represent the data type of the value
 * on each space point.
 *
 * \author Baoshe Zhang
 */
template<class C, class D>
class cfVolume3d: public cfObject3d
{
public:

	/** \brief constructor
	 *
	 * \warning no coordinate system is set and no values are be assigned to the volume.
	 * use setCoord to set coordinate system and use setValue and setValuePtr to assign values.
	 */
	cfVolume3d(): _isAlloc(false), _isPtrSet(false) {}

	/** \brief constructor
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \warning no values are be assigned to the volume. use setValue or setValuePtr to assign values.
	 */
	cfVolume3d(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
					: _isAlloc(false), _isPtrSet(false)
	{
		_coord.setCoordAxis(x, y, z);
	}

	/** \brief constructor
	 *
	 * \param coord a 3-D cfCoord3d coordinate system
	 * \warning no values are be assigned to the volume. use setValue or setValuePtr to assign values.
	 */
	cfVolume3d(const cfCoord3d<C>& coord)
					: _isAlloc(false), _isPtrSet(false)
	{
		_coord = coord;
	}

	/** \brief constructor
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \param value the values to be assigned to the volume
	 */
	cfVolume3d(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z, const D *value)
					: _isAlloc(false), _isPtrSet(false)
	{
            setCoord(x, y, z);
            setValue(value);
	}

	/** \brief copy constructor (deep)
	 *
	 * \param vol a cfVolume3d
	 */
	cfVolume3d(const cfVolume3d<C,D>& vol): _isAlloc(false), _isPtrSet(false)
	{
            *this = vol;
	}

	/** \brief copy constructor from cfVolumeClsss(deep)
	 *
	 * \param volClass a cfVolume
	 */
	cfVolume3d(const cfVolume<D>& volClass): _isAlloc(false), _isPtrSet(false)
	{
            cfCoordAxis<float> xAxis, yAxis, zAxis;
            if(volClass.volume.isUniform.x)
            {
                xAxis.setCoord(volClass.volume.origin.x, volClass.volume.voxelSize.x,
                    volClass.volume.nElements.x);
            }
            else
            {
                xAxis.setCoord(volClass.volume.slicePositionX, volClass.volume.nElements.x);
            }

            if(volClass.volume.isUniform.y)
            {
                yAxis.setCoord(volClass.volume.origin.y, volClass.volume.voxelSize.y,
                     volClass.volume.nElements.y);
            }
            else
            {
                yAxis.setCoord(volClass.volume.slicePositionY, volClass.volume.nElements.y);
            }

            if(volClass.volume.isUniform.z)
            {
                zAxis.setCoord(volClass.volume.origin.z, volClass.volume.voxelSize.z,
                    volClass.volume.nElements.z);
            }
            else
            {
                zAxis.setCoord(volClass.volume.slicePositionZ, volClass.volume.nElements.z);
            }
            setCoordAxis(xAxis, yAxis, zAxis);

            this->setValuePtr(volClass.value);
	}



	/** \brief operator =
	 *
	 * \param vol a cfVolume3d
	 */
	void operator=(const cfVolume3d<C,D>& vol)
	{
		_coord = vol.getCoord3d();
		setValue(vol.getValuePtr());
	}

	/** \brief operator *=
	 *
	 * \param scale the scaling factor
	 */
	void operator*=(double scale)
	{
            if(!isValid())
            {
                std::cerr << "Error: The value has not been set(cfVolume3d::*=)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();            
            {
		int i;

		for(i=0; i<numPoints; i++)
                {
                    *(_value+i) = (D)(*(_value+i) * scale);
                }
            }
	}

	/** \brief operator +=
	 *
	 * \param vol a cfVolume3d
	 */
	void operator+=(const cfVolume3d<C,D>& vol)
	{
            if(!isValid() || !vol.isValid())
            {
                std::cerr << "Error: The value has not been set(cfVolume3d::+=)!!!" << std::endl;
                return;
            }

            if(!(vol.getCoord3d() == _coord))
            {
                std::cerr << "These two cfVolume3ds have different coordinate system!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            D *value = vol.getValuePtr();
            for(int i=0; i<numPoints; i++)
            {
                *(_value+i) += *(value+i);
            }
	}

	/** \brief operator +=
	 *
	 * \param value a number of type D
	 */
	void operator+=(const D value)
	{
            if(!isValid())
            {
                std::cerr << "Error: The value has not been set(cfVolume3d::+=)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            for(int i=0; i<numPoints; i++)
            {
                *(_value+i) += value;
            }
	}

	/** \brief replace each value by its absolute
	 *
	 */
	void abs()
	{
            if(!isValid())
            {
                std::cerr << "Error: The value has not been set(cfVolume3d::+=)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            for(int i=0; i<numPoints; i++)
            {
                D t;
                t = *(_value+i);
                if( t < 0 ) t = -t;
                *(_value+i) = t;
            }
	}

	/** \brief destructor
	 * if memory is allocated to the volume, it will be deleted here
	 */
	~cfVolume3d()
	{
            if(_isAlloc)
            {
                delete[] _value;
                _value = NULL;
                _isAlloc = false;
        }
	}

	/** \brief set the coordinate system for the volume
	 *
	 * \param x x-axis
	 * \param y y-axis
	 * \param z z-axis
	 * \warning no values are be assigned to the volume. use setValue to assign values.
	 */
	void setCoordAxis(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
	{
            if(_isAlloc)
            {
                delete[] _value;
                _value = NULL;
            }
            _isAlloc = false;
            _isPtrSet = false;

            _coord.setCoordAxis(x, y, z);
	}

	/** \brief set the coordinate system for the volume
	 *
	 * \param coord a cfCoord3d coordinate system
	 * \warning no values are be assigned to the volume. use setValue or setValuePtr to assign values.
	 */
	void setCoordAxis(const cfCoord3d<C>& coord)
	{
            if(_isAlloc)
            {
                delete[] _value;
                _value = NULL;
            }
            _isAlloc = false;
            _isPtrSet = false;

            _coord = coord;
	}

	/** \brief set the values of the current volume
	 *
	 * \param value a pointer to a data array. The values will replace the values of the current volume.
	 * \warning if no memory allocated for the volume, first allocate memory
	 */
	void setValue(const D* value)
	{
            if(!_coord.isValid())
            {
                std::cerr << "The 3-d coordinate system has not been set(cfVolume3d::setValue)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            if(!_isAlloc) // if no memory allocated, allocate memory
            {
                try
                {
                    _value = new D[numPoints];
                    _isAlloc = true;
                }
                catch(std::bad_alloc&)  // if memory allocation fails, exit
                {
                    _isAlloc = false;
                    std::cerr << "Error: Memory Allocation for the value(cfVolume3d::setValue)!!!" << std::endl;
                    return;
                }
            }
            
		int i;
		for(i=0; i<numPoints; i++)
                {
                    *(_value+i) = *(value+i);
                }

            _isPtrSet = _isAlloc;    // _isSet means _isAlloc
	}

	/** \brief set the values of the current volume to a particular value
	 *
	 * \param value the value to be set
	 * \warning if no memory allocated for the volume, first allocate memory
	 */
	void resetValue(D value)
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The 3-d coordinate system has not been set(cfVolume3d::resetValue)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            if(!_isAlloc && !_isPtrSet) // if no memory allocated and no pointer is set, allocate memory
            {
                try
                {
                    _value = new D[numPoints];
                    _isAlloc = true;
                    _isPtrSet = _isAlloc;    // _isSet means _isAlloc
                }
                catch(std::bad_alloc&)  // if memory allocation fails, exit
                {
                    _isAlloc = false;
                    std::cerr << "Error: Memory Allocation for the value(cfVolume3d::resetValue)!!!" << std::endl;
                    return;
                }
            }
            
            int i;
            for(i=0; i<numPoints; i++)
            {
                *(_value+i) = (D)value;
            }
	}

	/** \brief set a value to a coordinate point specified by an index
	 *
	 * \param index the index to specify a coordinate point
	 * \param value the value to be set
	 */
	void setValueAt(const cfPoint3D<int>& index, const D value)
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The coordinate system has not been set(cfVolume3d::setValueAt)!!!" << std::endl;
                return;
            }

            if(!_isPtrSet)  // if no memory pointer set, exit (illegal memory access)
            {
                std::cerr << "Error: Memory has not allocated(cfVolume3d::setValueAt)!!!" << std::endl;
                return;
            }

            if(!_coord.isInRange(index))
            {
                std::cerr << "Error: Invalid index(cfVolume3d::setValueAt)!!!" << std::endl;
                return;
            }

            cfPoint3D<int> dim = _coord.getNDim();

            int vIndex = INDEX3D(index, dim);

            _value[vIndex] = value;
	}


	/** \brief assign a pointer to the data array of the volume
	 *
	 * \param value the pointer. From now on, the volume will use the values pointed by this pointer.
	 * \warning if the memory is allocated to the volume, it will be deleted
	 */
	void setValuePtr(const D* value)
	{
            if(!_coord.isValid())
            {
                std::cerr << "Error: The 3-d coordinate system has not been set(cfVolume3d::setValuePtr)!!!" << std::endl;
                return;
            }

            if(_isAlloc) // if memory is allocated for value, will be deleted
            {
                delete[] value;
                value = NULL;
                _isAlloc = false;
            }

            _value = const_cast<D*>(value);
            _isPtrSet = true;
	}

	/** \brief get the values of the current volume
	 *
	 * \param value a data array used to store the values of the current volume.
	 * \warning before using this function, make sure that value is allocated memory of Nx*Ny*Nz*sizeof(D)
	 */
	void getValue(D* value) const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The value has not been set(cfVolume3d::getValue)!!!" << std::endl;
                return;
            }

            int numPoints = _coord.getNumPoint();

            int i;

            for(i=0; i<numPoints; i++)
            {
                *(value+i) = *(_value+i);
            }
	}

	/** \brief get the value of a coordinate grid point.
	 *
	 * \param index the index of a grid point
	 * \return the value corresponding to the point
	 */
	D getValueAt(cfPoint3D<int>& index) const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The value pointer has not been set(cfVolume3d::getValueAt)!!!" << std::endl;
                return D(0);
            }

            if(!_coord.isInRange(index))
            {
                std::cerr << "Error: Invalid index(cfVolume3d::getValueAt)" << std::endl;
                return D(0);
            }

            cfPoint3D<int> dim = _coord.getNDim();

            int vIndex = INDEX3D(index, dim);

            return _value[vIndex];
	}

	/** \brief get the value of a point.
	 *
	 * use interpolation to get the value for a point. When the point is not inside
	 * the coordinate system, return 0.
	 * \param tgtPoint the space point
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 * \return the value corresponding to the point
	 */
	D getValueAt(const cfPoint3D<C>& tgtPoint, const std::string interpMethod) const
	{
            cfPoint3D<C> point0, point1;
            cfPoint3D<int> index;
            D value, v[2][2][2];

            if(_coord.isInBox(tgtPoint)) // if tgtPoint is inside srcVolume
            {
                _coord.getCoordIndex(tgtPoint, index);  // get index of the above point in source
                _coord.getCoordAt(index, point0); // get the coordinate point of the above index              
                _coord.getCoordAt(index+cfPoint3D<int>(1,1,1), point1); // get the diagonal point
                getBoxVertex(index, v);           // get the values of 8 points
                if(interpMethod == "TRILINEAR")
                {
                    value = rcfOP::trilinearInterpolate(point0, point1, v, tgtPoint); // triliear interpolation
                }
                else if(interpMethod == "NEARESTNEIGHBOR")
                {
                    value = rcfOP::nearestNeighborInterpolate(point0, point1, v, tgtPoint); // triliear interpolation
                }
                else
                {
                    std::cerr << "Interpolation method--" << interpMethod << " is not supported!!!" << std::endl;
                    value = (D)0;
                }
            }
            else         // if tgtPoint is not inside srcVolume, value is set to 0
            {
                value = (D)0;
            }

            return value;
	}

	/** \brief get the pointer to the value array of the current volume
	 *
	 * \return the pointer. use INDEX3D(index, DIM) to access the value of an index
	 */
	D* getValuePtr() const
	{
            if(!_isPtrSet)
            {
                std::cerr << "Error: The value pointer has not been set(cfVolume3d::getValuePtr)!!!" << std::endl;
                return NULL;
            }
            return _value;
	}


	/** \brief use a source volume to fill up the current volume
	 *
	 * \param srcVolume the source volume to fill up the current volume
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
        void fill(const cfVolume3d<C,D>& srcVolume, const std::string interpMethod)
	{
            if( 0 != interpMethod.compare("TRILINEAR") && 0 != interpMethod.compare("NEARESTNEIGHBOR") )
            {
                std::cerr << "ERROR:cfVolume3d::fill: Unknown interpolation method: " << interpMethod << std::endl;
                return;
            }

		int i, j, k;

		/* if tgt/src have the same coordinate, just copy */
		resetValue((D)0);  // reset the current volume to zero, and allocate memory

		cfPoint3D<int> dim = _coord.getNDim();
		const int Nx = dim.x;
		const int Ny = dim.y;
		const int Nz = dim.z;

		cfPoint3D<int> tgtIndex;
		cfPoint3D<C> tgtPoint;
		D value;
		for(i = 0; i < Nx; i++)
		{
                    for(j=0; j < Ny; j++)
                    {
                        for(k=0; k < Nz; k++)
                        {
                            tgtIndex = cfPoint3D<int>(i, j, k);
                            _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume
                            value = srcVolume.getValueAt(tgtPoint, interpMethod); // get the value of the point
                            setValueAt(tgtIndex, value); // assign interpolated value to tgtVolume
                        }
                    }
		}
	}

	/** \brief use a source volume and a 3-D DVF to fill up the current volume
	 *
	 * The relation between the current volume and the source volume is
	 * current V(x, y, z) = source V(x+dvfX, y+dvfY, z+dvfZ)
	 * \param dvf3d the 3-D DVF
	 * \param srcVolume the source volume to fill up the current volume
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
        void fill(const cfDvf3d<C>& dvf3d, const cfMatrix4<C>& tform, const cfVolume3d<C,D>& srcVolume, bool isXformFirst, const std::string interpMethod)
        {
            int i, j, k;

            resetValue((D)0); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint, tgtPoint2;
            cfPoint3D<int> tgtIndex, srcIndex;
            cfPoint3D<C> dvf;
            D value;
            bool valid = false;
            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume

                        cfMatrix4<C> p, Tp;
                        p.null();                        
                        if( isXformFirst )
                        {
                            //use cf_matrix4D to apply transformation
                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = tgtPoint.x;
                            p.element[1][0] = tgtPoint.y;
                            p.element[2][0] = tgtPoint.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = tform * p;
                            srcPoint.x = (C)Tp.element[0][0];
                            srcPoint.y = (C)Tp.element[1][0];
                            srcPoint.z = (C)Tp.element[2][0];

                            dvf = dvf3d.getDvf(srcPoint, valid);
                            srcPoint += dvf;
                        }
                        else
                        {
                            tgtPoint2 = tgtPoint;
                            dvf = dvf3d.getDvf(tgtPoint, valid);
                            tgtPoint2 += dvf;

                            //assign tgtPoint to first column of a matrix p
                            p.element[0][0] = tgtPoint2.x;
                            p.element[1][0] = tgtPoint2.y;
                            p.element[2][0] = tgtPoint2.z;
                            p.element[3][0] = 1;

                            //apply transform to p, and write out result to srcPoint
                            Tp = tform * p;
                            srcPoint.x = (C)Tp.element[0][0];
                            srcPoint.y = (C)Tp.element[1][0];
                            srcPoint.z = (C)Tp.element[2][0];
                        }

                        if(valid)
                        {
                            value = srcVolume.getValueAt(srcPoint, interpMethod);   // get the value for the new point
                        }
                        else
                        {
                            value = 0;
                        }

                        setValueAt(tgtIndex, value);   // assign interpolated value to tgtVolume
                    }
                }
            }
        }
        
	void fill(const cfDvf3d<C>& dvf3d, const cfVolume3d<C,D>& srcVolume, const std::string interpMethod)
	{
            int i, j, k;

            resetValue((D)0); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint;
            cfPoint3D<int> tgtIndex, srcIndex;
            cfPoint3D<C> dvf;
            D value;
            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume
                        dvf = dvf3d.getDvf(tgtPoint);  // get DVF of the point
                        srcPoint = tgtPoint + dvf;     // get the new pointer after DVF                        
                        value = srcVolume.getValueAt(srcPoint, interpMethod);   // get the value for the new point                        
                        setValueAt(tgtIndex, value);   // assign interpolated value to tgtVolume                        
                    }
                }
            }
	}

	/** \brief use a source volume and a constant 3-D DVF to fill up the current volume
	 *
	 * The relation between the current volume and the source volume is
	 * current V(x, y, z) = source V(x+dvfX, y+dvfY, z+dvfZ)
	 * \param shift the const 3-D vector (cfPoint3D)
	 * \param srcVolume the source volume to fill up the current volume
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
	void fill(const cfPoint3D<C>& shift, const cfVolume3d<C,D>& srcVolume, const std::string interpMethod)
	{
            int i, j, k;

            cfCoord3d<C> srcCoord3d = srcVolume.getCoord3d();
            srcCoord3d.shift(-shift);
            /* if tgt/src-shifted have the same coordinate, just copy */
            /* otherwise, interpolate */
            resetValue((D)0); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;            

            cfPoint3D<C> tgtPoint, srcPoint;
            cfPoint3D<int> tgtIndex, srcIndex;
            
            D value;
            for(i = 0; i < Nx; i++)
            {
                for(j=0; j < Ny; j++)
                {
                    for(k=0; k < Nz; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume
                        srcPoint = tgtPoint + shift;   // get the new pointer after DVF                        
                        value = srcVolume.getValueAt(srcPoint, interpMethod);   // get the value for the new point
                        setValueAt(tgtIndex, value);   // assign interpolated value to tgtVolume
                    }
                }
            }
	}

	/** \brief use a source volume and a transformation matrix to fill up the current volume
	 *
	 * The relation between the current volume and the source volume is
	 * current V(p) = source V(T(p)), where T(p)=T*p, T is an rigid transformation composed of
	 * translation and rotation, and p is a point in the volume V.
	 * \param tform the transformation matrix
	 * \param srcVolume the source volume to fill up the current volume
	 * \param interpMethod the interpolation method. Current Values: TRILINEAR, NEARESTNEIGHBOR
	 */
	void fill(const cfMatrix4<C>& tform, const cfVolume3d<C,D>& srcVolume, const std::string interpMethod)
	{

            int i, j, k;

            resetValue((D)0); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint;
            cfPoint3D<int> tgtIndex, srcIndex;
            D value;
            for(i = 0; i < Nx-1; i++)
            {
                for(j=0; j < Ny-1; j++)
                {
                    for(k=0; k < Nz-1; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume

                        //use cf_matrix4D to apply transformation
                        cfMatrix4<C> p, Tp;
                        p.null();
                        //assign tgtPoint to first column of a matrix p
                        p.element[0][0] = tgtPoint.x;
                        p.element[1][0] = tgtPoint.y;
                        p.element[2][0] = tgtPoint.z;
                        p.element[3][0] = 1;

                        //apply transform to p, and write out result to srcPoint
                        Tp = tform * p;
                        srcPoint.x = (C)Tp.element[0][0];
                        srcPoint.y = (C)Tp.element[1][0];
                        srcPoint.z = (C)Tp.element[2][0];

                        value = srcVolume.getValueAt(srcPoint, interpMethod);   // get the value for the new point
                        setValueAt(tgtIndex, value);   // assign interpolated value to tgtVolume
                    }
                }
            }
	}

        void fillDistance(const cfMatrix4<C>& tform, const cfVolume3d<C,D>& srcVolume, const std::string interpMethod)
	{
            int i, j, k;

            resetValue((D)0); // reset the values of the current volume to 0

            cfPoint3D<int> dim = _coord.getNDim();
            const int Nx = dim.x;
            const int Ny = dim.y;
            const int Nz = dim.z;

            cfPoint3D<C> tgtPoint, srcPoint;
            cfPoint3D<int> tgtIndex;
            D value;
            for(i = 0; i < Nx-1; i++)
            {
                for(j=0; j < Ny-1; j++)
                {
                    for(k=0; k < Nz-1; k++)
                    {
                        tgtIndex = cfPoint3D<int>(i, j, k);
                        _coord.getCoordAt(tgtIndex, tgtPoint);  // get the coordinate point from current volume

                        //use cf_matrix4D to apply transformation
                        cfMatrix4<C> p, Tp;
                        p.null();
                        //assign tgtPoint to first column of a matrix p
                        p.element[0][0] = tgtPoint.x;
                        p.element[1][0] = tgtPoint.y;
                        p.element[2][0] = tgtPoint.z;
                        p.element[3][0] = 1;

                        //apply transform to p, and write out result to srcPoint
                        Tp = tform * p;
                        srcPoint.x = (C)Tp.element[0][0];
                        srcPoint.y = (C)Tp.element[1][0];
                        srcPoint.z = (C)Tp.element[2][0];
                        
                        value = srcVolume.getValueAt(srcPoint, "TRILINEAR");
                        setValueAt(tgtIndex, value);   // assign interpolated value to tgtVolume
                    }
                }
            }
	}

	/** \brief get the 3-D coordinate system of the current volume
	 *
	 */
	const cfCoord3d<C>& getCoord3d() const
	{
		return _coord;
	}

	/** \brief save cfVolume3d to an AVS FLD file
	 * \param outfilename the file name of the AVS FLD file
	 *
	 */
	void save(const std::string outfilename)
	{
		int i;  //index
		const bool isUniform = _coord.isUniform();

		/* pointer to the value block of cfVolume3d */
		D* pValue;

		/* the data type of value */
		std::string dataType;

		const cfPoint3D<int> dim = _coord.getNDim();
		const int numPoint = _coord.getNumPoint();

		/* set Value */
		if(typeid(D) == typeid(float))
		{
			pValue = getValuePtr();
			dataType = "xdr_float";
		}
		else if(typeid(D) == typeid(char) || typeid(D) == typeid(unsigned char))
		{
			pValue = getValuePtr();
			dataType = "xdr_byte";
		}
		else if(typeid(D) == typeid(double))
		{
			pValue = getValuePtr();
			dataType = "xdr_double";
		}
		else if(typeid(D) == typeid(int) || typeid(D) == typeid(unsigned int))
		{
			pValue = getValuePtr();
			dataType = "xdr_integer";
		}
		else if(typeid(D) == typeid(short) || typeid(D) == typeid(unsigned short))
		{
			pValue = getValuePtr();
			dataType = "xdr_short";
		}
		else
		{
			std::cerr << "data type not supported!!!" << std::endl;
                        return;
		}

		/* create a AVS FLD file */
		std::ofstream out(outfilename.c_str(), std::ios::out | std::ios::binary);
		if(!out.is_open())
		{
			std::cerr << "can not open the file!!!" << std::endl;
			return;
		}
		/* create header */
		out << "# AVS" << std::endl;
		out << "ndim=3" << std::endl;
		out << "dim1=" << dim.x << std::endl;
		out << "dim2=" << dim.y << std::endl;
		out << "dim3=" << dim.z << std::endl;
		out << "nspace=3" << std::endl;
		out << "veclen=1" << std::endl;
		if(dataType == "xdr_short")
		{
			out << "data=xdr_integer" << std::endl;
		}
		else
		{
			out << "data=" << dataType << std::endl;
		}
		if(isUniform) /* uniform field */
		{
			out << "field=uniform" << std::endl;
		}
		else /*rectilinear field */
		{
			out << "field=rectilinear" << std::endl;
		}
		out << "\f\f";


		/* set binary data area */
		/* value */
		if(dataType == "xdr_short")
		{
			int tValue;
			for(i=0; i<numPoint; i++)
			{
				tValue = pValue[i];
				rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
				out.write((char*)&tValue, sizeof(tValue));
			}
		}
		else
		{
			D tValue;
			for(i=0; i<numPoint; i++)
			{
				tValue = pValue[i];
				rcfOP::xdrFormat((unsigned char*)&tValue, sizeof(tValue));
				out.write((char*)&tValue, sizeof(tValue));
			}
		}

		/* coordinate */
		if(isUniform) /* uniform field */
		{
			float tCoord;
			/* Axis X */
			tCoord = _coord.getCoordAxisX().getFirstCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

			tCoord = _coord.getCoordAxisX().getLastCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

			/* Axis Y */
			tCoord = _coord.getCoordAxisY().getFirstCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

			tCoord = _coord.getCoordAxisY().getLastCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

			/* Axix Z */
			tCoord = _coord.getCoordAxisZ().getFirstCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));

			tCoord = _coord.getCoordAxisZ().getLastCoord();
			rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
			out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
		}
		else /*rectilinear field */
		{
			float tCoord;
			/* Axis X */
			cfCoordAxis<C> axisX = _coord.getCoordAxisX();
			for(i=0; i<dim.x; i++)
			{
				tCoord = (float)axisX.getCoordAt(i);
				rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
				out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
			}
			/* Axis Y */
			cfCoordAxis<C> axisY = _coord.getCoordAxisY();
			for(i=0; i<dim.y; i++)
			{
				tCoord = (float)axisY.getCoordAt(i);
				rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
				out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
			}
			/* Axix Z */
			cfCoordAxis<C> axisZ = _coord.getCoordAxisZ();
			for(i=0; i<dim.z; i++)
			{
				tCoord = (float)axisZ.getCoordAt(i);
				rcfOP::xdrFormat((unsigned char*)&tCoord, sizeof(tCoord));
				out.write(reinterpret_cast<char*>(&tCoord), sizeof(tCoord));
			}
		}
		/* close the file */
		out.close();
	}
	/** \brief check if the value or value pointer is set
	 *
	 * \return true, valid; false, not valid
	 */
	bool isValid() const
	{
		return _isPtrSet;
	}


private:
	/** \brief extract the values on eight vertices of a cube of the current volume3d
	 *
	 * \param index the left-bottom point specified by index of the cube box
	 * \param v the values of eight vertices (returned)
	 */
	void getBoxVertex(const cfPoint3D<int>& index, D v[2][2][2]) const
	{
            int i, j, k;
            cfPoint3D<int> index0;
            for(i=0; i<2; i++)
            {
                for(j=0; j<2; j++)
                {
                    for(k=0; k<2; k++)
                    {
                        index0 = index + cfPoint3D<int>(i,j,k);
                        v[i][j][k] = getValueAt(index0);
                    }
                }
            }
	}

private:
	bool _isAlloc;        // true, memory is allocated
	bool _isPtrSet;       // true, a pointer is set to _value or _isAlloc = true
	D* _value;            // the values
	cfCoord3d<C> _coord;  // coordinate system
};

#endif
