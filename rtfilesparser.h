#ifndef RTFILESPARSER_H
#define RTFILESPARSER_H

#include <QObject>
#include "Common/PatientInfo.h"
#include <QDebug>

// Pinnacle and Clifreader Headers
#include "libClif/libjvs/utilities.h"
#include "libClif/libjvs/option.h"
#include "libClif/include/read_clif.h"
#include "roi_type.h" // libClif/include/roi_type.h // This  contains structure for ROIs
#include "case_info.h" //libClif/include/case_info.h // Contains structure for Patient file

// Dicom reader and parder Headers
#include "dicomParser/DicomFilesProp.h"
#include "dicomParser/CTImage.h"
#include "dicomParser/RtStructure.h"
#include "dicomParser/RtDose.h"
#include "Roi.h"




class rtFilesParser: public QObject // TODO: why it is not working
{

    Q_OBJECT

private:

    QString patientFolder;

    QVector<Patient> patientQVector;

    DicomFilesProp* dicomFilesProp;// TODO: should be removed in future, similar fucntion can be writtein inside readDicomPatientFolder


public:
    explicit rtFilesParser(QObject *parent);
    ~rtFilesParser();

    // flags
    bool isDicomFolder;
    bool isPinnaclePatientFolder;

    // Pinnacle Functions
    // functions for patients
    bool readPinnaclePatient(QString sPinnPatientFilePath, Patient * oPatient);

    // Reads  structureSets for a given patients
    bool readPinnacleStructureSetsForAGivenPatient(Patient *oPatient, QVector<RTStructureSet> & oRtStructureSetQVector);

    // Reads ROIs for a given structureSet
    bool readPinnacleRigionOfInterestForAGivenRtStructure(RTStructureSet * oRtStructure, QVector<RegionOfInterest> & ROIsQVector);

    // Reads DOSE from a given Plan


    // Reads Total Dose file from a Plan



    // TODO :need  a function to r9ead plan





    // DICOM Functions
    bool readDicomPatientFolder(QString sPatientFolderPath, QVector<Patient> & _patientQVector);
    bool readDicomPatientFromSelectedFiles(QVector<QString>& sPatientDicomFilesSelectedPath, QVector<Patient> & _patientQVector);

    bool readDicomRTStructueSetsForAGivenPatient(Patient *oPatient, QVector<RTStructureSet> & oRtStructureSetQVector);
    bool readDicomROISForAGivenRtStructure(RTStructureSet * oRtStructure, QVector<RegionOfInterest> & ROIsQVector);




    bool detectPatientFolderType();

    bool setPatientFolder(QString _patientFolder);





signals:

public slots:

};

#endif // RTFILESPARSER_H
