/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy
   of these files.
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input

   Please contact us if you have any questions
*/
/*  read_patient.cc

    routines to read patient information from Pinnacle

    Created: Oct, 23, 1998: JVS

    Modification History:
       Feb 1, 1999: JVS: patient->directory is the plan directory
                         makes sure image is specified with the full path...
       Feb 4, 1999: JVS: patch image filename
       April 6, 1999: JVS: plan_fname now part of case_info_type
       Oct 27, JVS: Add #ifdef's around printf's
       June 21, 2000: change read_pinnacle_image_name, will get full path if ../ is
                      specified in the image name...
       Nov 27, 2007: JVS get rid of compiler warning

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isspace

#include <iostream>
#include "utilities.h"
#include "read_clif.h"
#include "case_info.h"


void splitFilename (const std::string& str, std::string &baseName, std::string & folderPath )
{
    size_t found;
    //std::cout << "Splitting: " << str << std::endl;
    found=str.find_last_of("/\\");
    folderPath = str.substr(0,found);
    baseName= str.substr(found+1);

}
/* *********************************************************************************** */

int read_pinnacle_patient_full_prop(char *patientFliename,
                                    patient_type & patient)
{
    char patient_fname[MAX_STR_LEN];
    strcpy(patient_fname,patientFliename);  // put first part on name

    FILE *fp = fopen(patient_fname,"r");
    if(fp==NULL)
    {
        std::cout << " ERROR: opening patient  file " <<  patient_fname <<std::endl;
        return(FAIL);
    }

    patient.PatientFilePath = std::string(patient_fname);

    splitFilename(patient.PatientFilePath,patient.PatientFileName,patient.PatientFolderPath);
    //std::cout <<  " base name is = " <<  patient.PatientFileName << " PatientFolder is "  << patient.PatientFolderPath<< std::endl;



    clifObjectType patientClif = clifObjectType{};

    // read the entire clif object in....
    if ( OKFlag != readClifFile(fp,&patientClif,"patientClif")) {
        std::cout << "ERROR: read_pinnacle_patient_full_prop readClifFile" << std::endl; return FAIL;
    }
    fclose(fp);

    if(  !readStringClifStructure(&patientClif,"PatientPath",patient.PatientPath))
    {
        std::cout << "ERROR: read_pinnacle_patient_full_prop: reading info from %s " << std::endl; return FAIL;

    }

    // Parse out the object of interest....
    if( OKFlag != readIntClifStructure(&patientClif,"PatientID",&patient.PatientID) ||
            !readStringClifStructure(&patientClif,"PatientPath",patient.PatientPath) ||
            !readStringClifStructure(&patientClif,"LastName",patient.LastName) ||
            !readStringClifStructure(&patientClif,"FirstName",patient.FirstName) ||
            !readStringClifStructure(&patientClif,"MiddleName",patient.MiddleName) ||
            !readStringClifStructure(&patientClif,"MedicalRecordNumber",patient.MedicalRecordNumber) ||
            !readStringClifStructure(&patientClif,"EncounterNumber",patient.EncounterNumber) ||
            !readStringClifStructure(&patientClif,"PrimaryPhysician",patient.PrimaryPhysician) ||
            !readStringClifStructure(&patientClif,"AttendingPhysician",patient.AttendingPhysician) ||
            !readStringClifStructure(&patientClif,"ReferringPhysician",patient.ReferringPhysician) ||
            !readStringClifStructure(&patientClif,"RadiationOncologist",patient.RadiationOncologist) ||
            !readStringClifStructure(&patientClif,"Oncologist",patient.Oncologist) ||
            !readStringClifStructure(&patientClif,"Radiologist",patient.Radiologist) ||
            !readStringClifStructure(&patientClif,"Prescription",patient.Prescription) ||
            !readStringClifStructure(&patientClif,"Disease",patient.Disease) ||
            !readStringClifStructure(&patientClif,"Diagnosis",patient.Diagnosis) ||
            !readStringClifStructure(&patientClif,"Comment",patient.Comment) ||
            OKFlag != readIntClifStructure(&patientClif,"NextUniquePlanID",&patient.NextUniquePlanID) ||
            OKFlag != readIntClifStructure(&patientClif,"NextUniqueImageSetID",&patient.NextUniqueImageSetID) ||
            !readStringClifStructure(&patientClif,"Gender",patient.Gender) ||
            !readStringClifStructure(&patientClif,"DateOfBirth",patient.DateOfBirth) ||
            OKFlag != readFloatClifStructure(&patientClif,"DirSize",&patient.DirSize)
            ) {
        std::cout << "ERROR: read_pinnacle_patient_full_prop: reading info from %s " << std::endl; return FAIL;
    }

    // Read ImageSetList
    clifObjectType *imageSetListClif =  new(clifObjectType);
//    clifObjectType imageSetListClif = clifObjectType{};

    if(OKFlag != getClifAddress(&patientClif, "ImageSetList",&imageSetListClif)) {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for imageSetClass " << std::endl; return FAIL;
    }
    // check object just to make sure
    if(OKFlag != checkIfObjectList(imageSetListClif)) {
        std::cout << " ERROR: This should never happen " << std::endl; return FAIL;
    }

    patient.ImageSetList.clear();
    int nImageSets= imageSetListClif->nObjects;
    for (int iImageSetClif = 0; iImageSetClif < nImageSets; ++iImageSetClif)
    {
        patient.ImageSetList.push_back(ImageSet_type{});
        patient.ImageSetList[iImageSetClif].isStartWithDICOM = patient.isStartWithDICOM;

        if (OKFlag != readIntClifStructure(&imageSetListClif->Object[iImageSetClif],"ImageSetID",&patient.ImageSetList.back().ImageSetID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ImageSetID " << std::endl; return FAIL;
        }

        if (OKFlag != readIntClifStructure(&imageSetListClif->Object[iImageSetClif],"PatientID",&patient.ImageSetList.back().PatientID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PatientID " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"ImageName",patient.ImageSetList.back().ImageName))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ImageName " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"NameFromScanner",patient.ImageSetList.back().NameFromScanner))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for NameFromScanner " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"ExamID",patient.ImageSetList.back().ExamID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ExamID " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"StudyID",patient.ImageSetList.back().StudyID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for StudyID " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"Modality",patient.ImageSetList.back().Modality))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for Modality " << std::endl;
            return FAIL;
        }

        if (OKFlag != readIntClifStructure(&imageSetListClif->Object[iImageSetClif],"NumberOfImages",&patient.ImageSetList.back().NumberOfImages))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for NumberOfImages " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"ScanTimeFromScanner",patient.ImageSetList.back().ScanTimeFromScanner))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ScanTimeFromScanner " << std::endl;
            return FAIL;
        }
        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"FileName",patient.ImageSetList.back().FileName))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for FileName " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"SeriesDescription",patient.ImageSetList.back().SeriesDescription))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for SeriesDescription " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&imageSetListClif->Object[iImageSetClif],"MRN",patient.ImageSetList.back().MRN))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for MRN " << std::endl;
            return FAIL;
        }



    }


    // Read PlanList
    clifObjectType *planListClif =  new(clifObjectType);

    if(OKFlag != getClifAddress(&patientClif, "PlanList",&planListClif)) {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PlanList " << std::endl; return FAIL;
    }
    // check object just to make sure
    if(OKFlag != checkIfObjectList(planListClif)) {
        std::cout << " ERROR: This should never happen " << std::endl; return FAIL;
    }

    patient.PlanList.clear();
    int nPlans= planListClif->nObjects;

    for (int iPlan = 0; iPlan < nPlans; ++iPlan)
    {
        patient.PlanList.push_back(Plan_type{});

        if (OKFlag != readIntClifStructure(&planListClif->Object[iPlan],"PlanID",&patient.PlanList.back().PlanID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PlanID " << std::endl; return FAIL;
        }

        if (!readStringClifStructure(&planListClif->Object[iPlan],"ToolType",patient.PlanList.back().ToolType))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ToolType " << std::endl;
            return FAIL;
        }
        if (!readStringClifStructure(&planListClif->Object[iPlan],"PlanName",patient.PlanList.back().PlanName))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PlanName " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&planListClif->Object[iPlan],"Physicist",patient.PlanList.back().Physicist))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for Physicist " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&planListClif->Object[iPlan],"Comment",patient.PlanList.back().Comment))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for Comment " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&planListClif->Object[iPlan],"Dosimetrist",patient.PlanList.back().Dosimetrist))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for Dosimetrist " << std::endl;
            return FAIL;
        }

        if (OKFlag != readIntClifStructure(&planListClif->Object[iPlan],"PrimaryCTImageSetID",&patient.PlanList.back().PrimaryCTImageSetID))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PrimaryCTImageSetID " << std::endl;
            return FAIL;
        }

        // filling FusionIDArray
        patient.PlanList.back().FusionIDArray.clear();

        clifObjectType *fusionIDArrayClif =  new(clifObjectType);
        if(OKFlag != getClifAddress(&planListClif->Object[iPlan], "FusionIDArray",&fusionIDArrayClif)) {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for imageSetClass " << std::endl; return FAIL;
        }
        // check object just to make sure
        if(OKFlag != checkIfObjectList(fusionIDArrayClif)) {
            std::cout << " ERROR: This should never happen " << std::endl; return FAIL;
        }
        patient.PlanList.back().FusionIDArray.clear();


        int nFusionArray= fusionIDArrayClif->nObjects;
        for (int iFusionID = 0; iFusionID < nFusionArray; ++iFusionID)
        {
            patient.PlanList.back().FusionIDArray.push_back(Float_type{});

            if (OKFlag != readIntClifStructure(&fusionIDArrayClif->Object[iFusionID],"Value",&patient.PlanList.back().FusionIDArray.back().Value))
            {
                std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for Value " << std::endl; return FAIL;
            }
            //            std::cout << "iFusionID = " << patient.PlanList.back().FusionIDArray.back().Value << std::endl;
        }

       // delete(fusionIDArrayClif);














        if (!readStringClifStructure(&planListClif->Object[iPlan],"PrimaryImageType",patient.PlanList.back().PrimaryImageType))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PrimaryImageType " << std::endl;
            return FAIL;
        }

        if (!readStringClifStructure(&planListClif->Object[iPlan],"PinnacleVersionDescription",patient.PlanList.back().PinnacleVersionDescription))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PinnacleVersionDescription " << std::endl;
            return FAIL;
        }


        if (OKFlag != readIntClifStructure(&planListClif->Object[iPlan],"IsNewPlanPrefix",&patient.PlanList.back().IsNewPlanPrefix))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for IsNewPlanPrefix " << std::endl;
            return FAIL;
        }

        if (OKFlag != readIntClifStructure(&planListClif->Object[iPlan],"PlanIsLocked",&patient.PlanList.back().PlanIsLocked))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for PlanIsLocked " << std::endl;
            return FAIL;
        }

        if (OKFlag != readIntClifStructure(&planListClif->Object[iPlan],"OKForSyntegraInLaunchpad",&patient.PlanList.back().OKForSyntegraInLaunchpad))
        {
            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for OKForSyntegraInLaunchpad " << std::endl;
            return FAIL;
        }



        // read Object Version  //TODO: need to read ObjectVersion inside Plan

        //        clifObjectType *objectVersionClif2 =  new(clifObjectType);
        //        if(OK != getClifAddress(&planListClif->Object[iPlan], "ObjectVersion",&objectVersionClif2)) {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ObjectVersion " << std::endl;
        //            return FAIL;
        //        }
        //        // check object just to make sure
        //        if(OK != checkIfObjectList(objectVersionClif2))
        //        {
        //            std::cout << " ERROR: This should never happen " << std::endl; return FAIL;
        //        }


        //        if (!readStringClifStructure(objectVersionClif,"WriteVersion",patient.PlanList.back().ObjectVersion.WriteVersion))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for WriteVersion " << std::endl;
        //            return FAIL;
        //        }


        //        if (!readStringClifStructure(objectVersionClif,"CreateVersion",patient.PlanList.back().ObjectVersion.CreateVersion))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for CreateVersion " << std::endl;
        //            return FAIL;
        //        }

        //        if (!readStringClifStructure(objectVersionClif,"LoginName",patient.PlanList.back().ObjectVersion.LoginName))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for LoginName " << std::endl;
        //            return FAIL;
        //        }

        //        if (!readStringClifStructure(objectVersionClif,"CreateTimeStamp",patient.PlanList.back().ObjectVersion.CreateTimeStamp))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for CreateTimeStamp " << std::endl;
        //            return FAIL;
        //        }


        //        if (!readStringClifStructure(objectVersionClif,"WriteTimeStamp",patient.PlanList.back().ObjectVersion.WriteTimeStamp))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for WriteTimeStamp " << std::endl;
        //            return FAIL;
        //        }


        //        if (!readStringClifStructure(objectVersionClif,"LastModifiedTimeStamp",patient.PlanList.back().ObjectVersion.LastModifiedTimeStamp))
        //        {
        //            std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for LastModifiedTimeStamp " << std::endl;
        //            return FAIL;
        //        }



    } // iPlan




    // read Object Version
    clifObjectType* objectVersionClif =  new(clifObjectType);
    if(OKFlag != getClifAddress(&patientClif, "ObjectVersion",&objectVersionClif)) {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for ObjectVersion " << std::endl;
        return FAIL;
    }
    // check object just to make sure
    if(OKFlag != checkIfObjectList(objectVersionClif))
    {
        std::cout << " ERROR: This should never happen " << std::endl; return FAIL;
    }


    if (!readStringClifStructure(objectVersionClif,"WriteVersion",patient.ObjectVersion.WriteVersion))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for WriteVersion " << std::endl;
        return FAIL;
    }


    if (!readStringClifStructure(objectVersionClif,"CreateVersion",patient.ObjectVersion.CreateVersion))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for CreateVersion " << std::endl;
        return FAIL;
    }

    if (!readStringClifStructure(objectVersionClif,"LoginName",patient.ObjectVersion.LoginName))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for LoginName " << std::endl;
        return FAIL;
    }

    if (!readStringClifStructure(objectVersionClif,"CreateTimeStamp",patient.ObjectVersion.CreateTimeStamp))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for CreateTimeStamp " << std::endl;
        return FAIL;
    }


    if (!readStringClifStructure(objectVersionClif,"WriteTimeStamp",patient.ObjectVersion.WriteTimeStamp))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for WriteTimeStamp " << std::endl;
        return FAIL;
    }


    if (!readStringClifStructure(objectVersionClif,"LastModifiedTimeStamp",patient.ObjectVersion.LastModifiedTimeStamp))
    {
        std::cout << " ERROR: read_pinnacle_patient_full_prop: getting address for LastModifiedTimeStamp " << std::endl;
        return FAIL;
    }


    //delete(imageSetListClif); //TODO : get rid of pointer --> here valgrind will complain it is a mem leak to be addressed.

    // Read in imageSet Header files
    for (int iImageSet = 0; iImageSet < patient.ImageSetList.size(); ++iImageSet) {

        std::string iImageSetHeaderPath = patient.PatientFolderPath + "/"+ patient.ImageSetList[iImageSet].ImageName + ".header";
        if (OKFlag != readPinnacleImageSetHeader(iImageSetHeaderPath,patient.ImageSetList[iImageSet].imageHeader))
        {
            //return FAIL; // TODO : it does tolarate any missing imageSets
            patient.ImageSetList[iImageSet].isImageHeaderLoaded = false;
            continue;
        }

        patient.ImageSetList[iImageSet].isImageHeaderLoaded = true;

        // check if binary file exist
        std::string iImageSetDataFilePath = patient.PatientFolderPath + "/"+ patient.ImageSetList[iImageSet].ImageName + ".img";

        if (OKFlag == fileExists(iImageSetDataFilePath.c_str()))
        {
            patient.ImageSetList[iImageSet].dataFilePath =iImageSetDataFilePath;
        }
        else
        {
            std::cout << "Image Data file do not exist in the patient" <<iImageSetDataFilePath << std::endl;
            return FAIL;
        }

    }

    // Now reading Volume for imageSets

    for (int iImageSet = 0; iImageSet < patient.ImageSetList.size(); ++iImageSet) {

        patient.ImageSetList[iImageSet].isVolumeDataLoaded = false;
        if (!patient.ImageSetList[iImageSet].isImageHeaderLoaded)
        {
            continue;
        }

        if (OKFlag != read_pinnacle_volume(patient.ImageSetList[iImageSet]))
        {
            std::cout << "ERROR in reading data volume for  imageset " << iImageSet << std::endl;
            return FAIL;
        }
    }
    return(OKFlag);


}

/* *********************************************************************************** */
int read_pinnacle_image_name(char *pbeam_fname_stem, char *filename)
{
    char pbeam_fname[MAX_STR_LEN];
    strcpy(pbeam_fname,pbeam_fname_stem);  // put first part on name
    strcat(pbeam_fname,".defaults");          // append .Trial to the name
#ifdef DEBUG
    printf("\n Reading In Image Name from file %s",pbeam_fname);
#endif
    FILE *fp = fopen(pbeam_fname,"r");
    if(fp==NULL)
    {
        printf("\n ERROR: opening patient beam file %s", pbeam_fname);
        return(FAIL);
    }




    /* read image_file from stream */
    char cstring[MAX_STR_LEN];
    strcpy(cstring,"image_file      :");
    int sval = (int) strlen(cstring);
    char string[MAX_STR_LEN];
    do{
        if(fgets(string,MAX_STR_LEN,fp) == NULL)
        {
            printf("\n ERROR: read_pinnacle_image_name: getting string from file %s, string=%s", pbeam_fname,cstring);
            return(FAIL);
        }
    }while( strncmp(string,cstring,sval) );

    /* full file name is string+sval,
      but want to remove all of the path information */
    //   printf("\n string = %s", string+sval);
    sval++; // increment past colon
    while(isspace(string[sval])) sval++;
    strcpy(filename, string+sval);
    sval = (int) strlen(filename);
    filename[sval-1]='\0'; // Null terminate the string
    /* check to see if has the full path name or not */

    /* Version 4.2g writes ../ImageSet_1.
      BUT we need the full path information */
    // printf("\n filename = %s", filename);
    if(strncmp( filename, "../", 3) == 0 )
    {
        // remove the dots
        char tfilename[MAX_STR_LEN];
        sprintf(tfilename,"%s",filename+3);
        // determine full path name
        char fullPathName[MAX_STR_LEN];
        strcpy(fullPathName,pbeam_fname);  // copy over pbeam_fname
        int iLen = (int) (strlen(fullPathName) -1);
        while( fullPathName[iLen] != '/' && iLen )
        {
            fullPathName[iLen] = 0; iLen--;
        }
        // make sure not have double //
        while( fullPathName[iLen] == '/' && iLen )
        {
            fullPathName[iLen] = 0; iLen--;
        }
        sprintf(filename,"%s/%s", fullPathName, tfilename);
        fullPathName[iLen] = 0; iLen--;
        while( fullPathName[iLen] != '/' && iLen )
        {
            fullPathName[iLen] = 0; iLen--;
        }
        // make sure not have double //
        while( fullPathName[iLen] == '/' && iLen )
        {
            fullPathName[iLen] = 0; iLen--;
        }
        sprintf(filename,"%s/%s", fullPathName, tfilename);
    }
    /* Version 4.0 (without full path) starts with Institution */
    if( strncmp( filename,"Institution", 11)==0)
    {
        char *ppath = getenv("PINN_PAT_DIR");
        if(ppath == NULL)
        {
            ppath = getenv("PINNLP_PATIENTS");
            if(ppath == NULL)
            {
                printf("\n ERROR: Environment Variable for PINNLP_PATIENTS of PINN_PAT_DIR not set");
                printf("\n\tand image name does not have full path information");
                return(FAIL);
            }
        }
        /* for some odd reason, sprintf(filename,"%s/%s",ppath,filename); gives result of ppath/ppath/...
         ust tmp string to overcome this */
        char tfilename[MAX_STR_LEN];
        sprintf(tfilename,"%s/%s",ppath,filename);
        strcpy(filename, tfilename);
    }
    fclose(fp);
    printf("\n Image File Name is %s", filename);
    return(OKFlag);
}
/* *********************************************************************************** */
/* *********************************************************************************** */
int read_pinnacle_patient_name(case_info_type *patient)
{
    char pbeam_info_fname[MAX_STR_LEN];
    strcpy(pbeam_info_fname,patient->PlanFileName);  // put first part on name
    strcat(pbeam_info_fname,".PlanInfo");          // append .Trial to the name
#ifdef DEBUG
    printf("\n Reading In Plan Info from file %s",pbeam_info_fname);
#endif
    FILE *fp = fopen(pbeam_info_fname,"r");
    if(fp==NULL)
    {
        printf("\n ERROR: opening patient beam file %s",pbeam_info_fname);
        return(FAIL);
    }
    /* Read In Patient and Plan Information */
    if(clif_string_read(fp,"PatientName =",patient->PatientName) != OKFlag)
        return(FAIL);
    if(clif_string_read(fp,"PlanName =",patient->PlanName) != OKFlag)
        return(FAIL);
    fclose(fp);
    return(OKFlag);
}
/* *********************************************************************************** */
int read_pinnacle_patient(case_info_type *patient)
{

    if(read_pinnacle_image_name(patient->PlanFileName, patient->ImageName) != OKFlag)
    {
        printf("\n ERROR: Reading Patient Image File Name");
        return(FAIL);
    }
#ifdef DEBUG
    printf("\n Image Name %s Located",patient->ImageName);
#endif
    if(read_pinnacle_patient_name(patient) != OKFlag)
    {
        printf("\n ERROR: Reading Patient Name");
        return(FAIL);
    }
    strcpy(patient->Directory, patient->PlanFileName);
    int len=(int) (strlen(patient->Directory)-1);
    while(len > 0 && patient->Directory[len] != '/')
    {
        patient->Directory[len]=0;
        len--;
    }
#ifdef DEBUG
    printf("\n patient->Directory = %s",patient->Directory);
#endif
    return(OKFlag);
}
/* *********************************************************************************** */
