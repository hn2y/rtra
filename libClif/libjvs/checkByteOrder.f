c      LOGICAL FUNCTION BIGENDIAN()
      IMPLICIT NONE
c     Declare a single integer with length equal to 4 bytes
      Integer*4 i4
c     Declare a 4-length byte array
      Byte b(4)
c     Equivalence them in memory
      EQUIVALENCE (i4,b)
c     Set the integer equal to 1.000
      i4 = 1
c
      print *, b(1),b(2),b(3),b(4)     
c
      if (b(1).EQ.1 .AND. b(4).EQ.0) then
         print *, 'Little Endian'
c         bigendian = 0
      elseif (b(1).EQ.0.AND.b(4).EQ.1) then
         print *, 'Big Endian'
c         bigendian = 1
      else
         print *, 'Indeterminate Byte Order'
      endif
      print *, 'Hello World'
      end
