#include <list>
#include <iostream>
#include "cfClifPoints.hpp"

using namespace std;

cfClifPoints::cfClifPoints()
{
    
}

cfClifPoints::~cfClifPoints()
{
    ClearPoints();
}

void cfClifPoints::AddPoint( cfPoint3D<float> p )
{    
    points.insert(points.end(), 1, p);
}

bool cfClifPoints::RemovePoint( unsigned int index )
{
    if( index >= points.size() )
    {
        cerr << "ERROR: cfClifPoints::RemovePoint: Index: " << index << " is out of bounds."
            << " Valid range is [0, " << points.size() << "]." << endl;
        return false;
    }
    else
    {
        cerr << "Opps, this is not implemented yet" << endl;
        return true;
    }
}

void cfClifPoints::ClearPoints()
{
    points.clear();
}
