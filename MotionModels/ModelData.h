#ifndef MODELDATA_H
#define MODELDATA_H
#include <list>
#include <map>
#include <MotionModels/GumModel.h>
#include <MotionModels/RidgidBodyMotionModel.h>
#include <MotionModels/DeformableBodyMotionModel.h>
#include <MotionModels/RotationAndTranslationModel.h>
#include <MotionModels/DeformationModel.h>
#include <MotionModels/DelineationModel.h>
#include <dicomParser/Roi.h>
#include <QtXml>
#include "libClif/include/roi_type.h"
#include <boost/tuple/tuple.hpp>

class ModelData
{

public:
    ModelData();
    ModelData(std::ostream &stream);
    ModelData(std::ostream &stream,const std::vector<Roi>& roiVector);
    ModelData(std::ostream &stream,const std::vector<roi_prop>& roiVector);
    ~ModelData();
    RigidBodyMotionModel *rigidBodyMotionModel;
    DeformableBodyMotionModel *deformableBodyMotionModel;
    bool rigidBodyMotionExist;
    bool deformableBodyMotionExist;
   // std::vector<bool> flagRotationAndTransitionModelExist;
   // std::vector<bool> flagDeformationModelExist;
//    std::vector<bool> flagDelinationModelExist;

    void SetupGUMModelForROIs(const std::vector<Roi> &roiVector);
    void SetupGUMModelForROIs(const std::vector<roi_prop> &roiVector);
    bool FillModelDataFromXmlConfigDoc(QDomDocument* domXmlDocument, std::vector<Roi>& roiVector);
    //std::map<std::string, RigidBodyMotionModel> gumMap;
    //bool loadModelFromXMLFile(char* filename,std::string & roiID);
    // bool loadModelFromXMLDocument(QDomDocument* domXmlDocument, std::string & roiID);


    //TODO: we extend the map to multiple uncertainty model for an ROI using the following pattern
    //std::map<std::string,tuple<RigidBodyMotionModel&,DeformableBodyMotionModel&,DelineationModel&>>* gumMap;
    std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>> gumMap;
   // std::map<std::string, RotationAndTranslationModel> gumMapSimple;

    int verbose;
    QDomDocument *modelDataXmlDoc;

    //void retrievElements(QDomElement root, QString tag, QString att);
    bool parseRigidBodyMotonFromXMLConfigDocRoot(const QDomElement &root, const std::vector<Roi> &roiVector);
    bool parseAllRotationAndTranslationFromXmlCofingDocRoot(const QDomElement &root, const std::vector<Roi> &roiVector);
private:
    std::ostream &logstream;

};

#endif // MODELDATA_H
