/* 
 * File:   cfDicomDirectoryMgr.hpp
 * Author: fsleeman
 *
 * Created on April 29, 2011, 1:34 PM
 */

#ifndef CFDICOMDIRECTORYMGR_HPP
#define	CFDICOMDIRECTORYMGR_HPP

#include "cfDicomSlice.hpp"
#include "cfDicomSliceReader.hpp"
#include <string>
#include <vector>

namespace DICOM_MGR
{
    struct DicomHeaderTags
    {
        std::string studyDate;          // 0x0008 0x0020
        std::string studyTime;          // 0x0008 0x0030
        std::string modality;           // 0x0000 0x0060
        std::string patientName;        // 0x0010 0x0010
        std::string patientID;          // 0x0010 0x0020
        std::string studyInstanceUID;   // 0x0020 0x000D
        std::string seriesInstanceUID;  // 0x0020 0x000E
        std::string studyID;            // 0x0020 0x0010
        std::string seriesNumber;       // 0x0020 0x0011
    };
}

class cfDicomDirectoryMgr
{
    public:
        cfDicomDirectoryMgr();    
        int ReadDirectory(std::string directory);
        int GetNumberOfSets() { return tagVector.size(); }        
        std::vector<DICOM_MGR::DicomHeaderTags> GetSetInformation() { return tagVector; }
        DICOM_MGR::DicomHeaderTags GetSetInformation(unsigned int index);
    private:
        std::string dicomDirectory;
        std::vector<DICOM_MGR::DicomHeaderTags> tagVector;
        std::vector<int> numberOfSlices;
};

#endif	/* CFDICOMDIRECTORYMGR_HPP */
