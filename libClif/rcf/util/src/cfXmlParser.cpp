/*
 * cfXMLParser.cpp
 *
 *  Created on: Sep 16, 2009
 *      Author: bzhang
 */

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <typeinfo>
#include <exception>
#include <stdexcept>

#include "cfXmlParser.hpp"

cfXmlParser::cfXmlParser()
{
	m_xmlTree = NULL;
}

cfXmlParser::~cfXmlParser()
{
	deleteXmlTree();
}

xmlNode* cfXmlParser::getRootNode() const
{
	if(m_xmlTree)
	{
		return m_xmlTree->getChildNode(0);
	}
	return NULL;
}

xmlNode *cfXmlParser::parseNodeBlock(std::string xmlNodeStr)
{
	xmlNode *childmultimap = new xmlNode();

	size_t t_npos = -1;

	int node_count = 0;

	int sibling_node_count = 0;
	// find <
	while((t_npos = xmlNodeStr.find_first_of("<", t_npos+1)) != xmlNodeStr.npos)
	{
		if(xmlNodeStr.at(t_npos+1) == '/') // ending tag: </childnode
		{
			node_count--;
		}
		else  // beginning tag: <childnode
		{
			node_count++;
		}

		// the beginning/ending tags match
		if(node_count == 0)
		{
			// get the position of close >
			size_t t_blk_end = xmlNodeStr.find_first_of(">", t_npos+1);

			// get the child xml string
			std::string xmlNode_t = xmlNodeStr.substr(0, t_blk_end+1);

			// remove current node from xmlNode
			xmlNodeStr.erase(0, t_blk_end+1);

			// get node name
			std::string nodeName = xmlNode_t.substr(t_npos+2, t_blk_end-t_npos-2);
			std::ostringstream oss;
			oss << "xmlnode";
			oss << sibling_node_count;
			oss << "_" + nodeName;

			std::string nodeName_alias = oss.str();

			// check nodeName (beginning/ending tags)
			if(nodeName == xmlNode_t.substr(1, nodeName.length()))
			{
				xmlNode_t.erase(t_npos, nodeName.length()+3);

				xmlAttrMap* attrMap =
								parseNodeAttr(xmlNode_t.substr(0, xmlNode_t.find_first_of(">")+1));

				xmlNode_t.erase(0, xmlNode_t.find_first_of(">")+1);

				// check if containing child node block
				if(xmlNode_t.length()>0 && xmlNode_t.at(0) == '<' && xmlNode_t.at(xmlNode_t.length()-1) == '>')
				{
					xmlNode* xmlNodeMap_t = parseNodeBlock(xmlNode_t);
					if(attrMap->size() > 0)
						xmlNodeMap_t->insert(std::pair<std::string, void*>("xmlattr", (void*)attrMap));
					childmultimap->insert(std::pair<std::string, void*>(nodeName_alias, (void*)xmlNodeMap_t));
				}
				else // no more child node block
				{
					std::string nodeValue = xmlNode_t;
					size_t v_npos;
					// remove redundant white space
					//while((v_npos=nodeValue.find("  ")) != nodeValue.npos)
					//{
					//	nodeValue.erase(v_npos, 1);
					//}
					while((v_npos = nodeValue.find("&lt;")) != nodeValue.npos)
					{
						nodeValue.replace(v_npos, 4, "<");
					}
					while((v_npos = nodeValue.find("&gt;")) != nodeValue.npos)
					{
						nodeValue.replace(v_npos, 4, ">");
					}

					xmlAttrMap *xmlValueMap = new xmlAttrMap();
					xmlValueMap->insert(std::pair<std::string, std::string>("xmlvalue", nodeValue));
					xmlValueMap->insert(attrMap->begin(), attrMap->end());

					xmlNode* xmlNodeMap_t = new xmlNode;
					if(attrMap->size() > 0)
						xmlNodeMap_t->insert(std::pair<std::string, void*>("xmlattr", (void*)attrMap));
					xmlNodeMap_t->insert(std::pair<std::string, void*>("xmlvalue", (void*)new std::string(nodeValue)));

					childmultimap->insert(std::pair<std::string, void*>(nodeName_alias, (void*)xmlNodeMap_t));
				}
			}
			else
			{
				std::cerr << "Error: beginning/ending tags do not match!!!" << std::endl;
				return NULL;
			}
			sibling_node_count++;
			t_npos = -1; // reset search position
		}
	}
	return childmultimap;
}

void cfXmlParser::loadXmlFile(std::string xmlFileName)
{
	std::ifstream xmlFile(xmlFileName.c_str());
	if(xmlFile.is_open())
	{
		// read xml file
		xmlFile >> std::noskipws;
		std::stringstream t_ss;
		std::copy(std::istream_iterator<char>(xmlFile), std::istream_iterator<char>(), std::ostream_iterator<char>(t_ss));
		std::string xmlStr = t_ss.str();
		xmlFile.close();

		// parse xml string
		parse(xmlStr);
	}
	else
	{
		std::cerr << "cannot open xml file: " + xmlFileName + "!!!" << std::endl;
	}

}

void cfXmlParser::parse(std::string xmlStr)
{
	deleteXmlTree();

	size_t t_npos = 0;
	// change \r|\n|\t into a white space
	while((t_npos = xmlStr.find_first_of("\r\n\t", t_npos)) != xmlStr.npos)
	{
		xmlStr.replace(t_npos, 1, " ");
	}

	// remove declaration
	t_npos = 0;
	while((t_npos = xmlStr.find("<?", t_npos)) != xmlStr.npos)
	{
		size_t npos_end = xmlStr.find("?>");
		if(npos_end != xmlStr.npos)
		{
			xmlStr.erase(t_npos, npos_end-t_npos+2);
		}
		else
		{
			std::cerr << "xml has incorrect format!!!" << std::endl;
			return;
		}
	}

	// remove comment
	t_npos = 0;
	while((t_npos = xmlStr.find("<!--", t_npos)) != xmlStr.npos)
	{
		size_t npos_end = xmlStr.find("-->", t_npos);
		if(npos_end != xmlStr.npos)
		{
			xmlStr.erase(t_npos, npos_end - t_npos+3);
		}
		else
		{
			std::cerr << "xml has incorrect format!!!" << std::endl;
			return;
		}
	}

	// remove DTD
	t_npos = 0;
	while((t_npos = xmlStr.find("<!", t_npos)) != xmlStr.npos)
	{
		size_t npos_end = xmlStr.find_first_of(">", t_npos);
		if(npos_end != xmlStr.npos)
		{
			xmlStr.erase(t_npos, npos_end - t_npos+3);
		}
		else
		{
			std::cerr << "xml has incorrect format!!!" << std::endl;
			return;
		}
	}

	// change <x/> into <x></x>
	t_npos = 0;
	while((t_npos = xmlStr.find("/>", t_npos)) != xmlStr.npos)
	{
		size_t npos_start = xmlStr.find_last_of("<", t_npos);
		size_t npos_end = xmlStr.find_first_of(" ", npos_start);
		if(npos_end == xmlStr.npos || npos_end > t_npos || npos_end < npos_start)
		{
			npos_end = t_npos;
		}
		std::string nodeName = xmlStr.substr(npos_start+1, npos_end - npos_start-1);
		xmlStr.replace(t_npos, 2, "></"+nodeName+">");
		t_npos += nodeName.length() + 1;
	}

	// remove white space before >
	t_npos = 0;
	while((t_npos = xmlStr.find_first_of("><", t_npos)) != xmlStr.npos)
	{
		try
		{
			bool pre_sp = false;
			if(t_npos > 0)
			{
				pre_sp = (xmlStr.at(t_npos-1) == ' ');
			}
			bool suf_sp = (xmlStr.at(t_npos+1) == ' ');
			if(!pre_sp && !suf_sp) // no white space surrounding < or >
			{
				t_npos++;
			}
			else  // white space exists
			{
				if(pre_sp) // prefix white space
				{
					xmlStr.erase(t_npos-1, 1);
					t_npos--;
				}
				if(suf_sp) // suffix white space
				{
					xmlStr.erase(t_npos+1, 1);
				}
			}
		}
		catch(std::out_of_range& e)
		{
			break;
		}
	}

	m_xmlTree = parseNodeBlock(xmlStr);
}

void cfXmlParser::showXmlTree() const
{
	showChildNode(m_xmlTree, 0);
}

void cfXmlParser::showChildNode(xmlNode *xmlChildNode, int depth) const
{
	if(xmlChildNode->find("xmlvalue") == xmlChildNode->end())
	{
		xmlNode::const_iterator it;
		for(it=xmlChildNode->begin(); it!=xmlChildNode->end();it++)
		{
			for(int i=0; i<depth; i++) std::cout << "     ";

			if(it->first != "xmlattr") // process child node
			{
				std::cout << it->first.substr(it->first.find_first_of('_')+1) << std::endl;
				showChildNode((xmlNode *)((*it).second), depth+1);
			}
			else // show attributes
			{
				std::cout << it->first << std::endl;
				xmlAttrMap *attrMap = (xmlAttrMap *)(it->second);
				xmlAttrMap::const_iterator it_attr;

				if(attrMap->size() > 0)
				{
					for(it_attr=attrMap->begin(); it_attr!=attrMap->end();it_attr++)
					{
						for(int i=0; i<depth; i++) std::cout << "     ";
						std::cout << "     ";
						std::cout << it_attr->first << ":" << it_attr->second << std::endl;
					}
				}
			}
		}
	}
	else
	{
		xmlNode *xmlValueMap = (xmlNode *)(xmlChildNode);
		xmlNode::const_iterator it = xmlValueMap->find("xmlvalue");
		for(int i=0; i<depth; i++) std::cout << "     ";
		std::cout << it->first << ":" << *((std::string*)(it->second)) << std::endl;
		if((it = xmlValueMap->find("xmlattr")) != xmlValueMap->end())
		{
			xmlAttrMap *attrMap = (xmlAttrMap *)(it->second);
			xmlAttrMap::const_iterator it_attr;

			if(attrMap->size() > 0)
			{
				for(int i=0; i<depth; i++) std::cout << "     ";
				std::cout << "xmlattr:" << std::endl;

				for(it_attr=attrMap->begin(); it_attr!=attrMap->end();it_attr++)
				{
					for(int i=0; i<depth; i++) std::cout << "     ";
					std::cout << "     ";
					std::cout << it_attr->first << ":" << it_attr->second << std::endl;
				}
			}
		}
	}
}

void cfXmlParser::deleteXmlTree()
{
	if(m_xmlTree == NULL) return;
	deleteChildNode(m_xmlTree);
	m_xmlTree = NULL;
}

void cfXmlParser::deleteChildNode(xmlNode *xmlChildNode)
{
	if(xmlChildNode->find("xmlvalue") == xmlChildNode->end())
	{
		xmlNode::const_iterator it;
		for(it=xmlChildNode->begin(); it!=xmlChildNode->end();it++)
		{
			if(it->first != "xmlattr")
			{
				deleteChildNode((xmlNode *)((*it).second));
			}
			else
			{
				xmlAttrMap *attrMap = (xmlAttrMap *)(it->second);
				delete attrMap;
			}
		}
		delete xmlChildNode;
	}
	else
	{
		xmlNode *xmlValueMap = (xmlNode *)(xmlChildNode);
		xmlNode::const_iterator it = xmlValueMap->find("xmlvalue");
		delete (std::string*)(it->second);

		if((it = xmlValueMap->find("xmlattr")) != xmlValueMap->end())
		{
			xmlAttrMap *attrMap = (xmlAttrMap *)(it->second);
			delete attrMap;
		}
		delete xmlValueMap;
	}
	xmlChildNode = NULL;
}

xmlAttrMap* cfXmlParser::parseNodeAttr(std::string nodeAttr)
{
	xmlAttrMap* attrMap = new xmlAttrMap();

	if(nodeAttr.find_first_of(' ') != nodeAttr.npos)  // check if attribute(s) exists.
	{
		xmlAttrMap valueMap;

		int attr_num = 0;

		// double quote
		size_t npos_t_begin = 0;
		while((npos_t_begin = nodeAttr.find_first_of('\"', npos_t_begin)) != nodeAttr.npos)
		{
			size_t npos_t_end = nodeAttr.find_first_of('\"', npos_t_begin+1);
			if(npos_t_end == nodeAttr.npos) break;

			std::ostringstream stm;
			stm << "xmlattr_";
			stm << attr_num;
			valueMap.insert(std::pair<std::string, std::string>(stm.str(),
					nodeAttr.substr(npos_t_begin+1, npos_t_end-npos_t_begin-1)));
			nodeAttr.replace(npos_t_begin, npos_t_end-npos_t_begin+1, stm.str());

			npos_t_begin = npos_t_end+1;
			attr_num++;
		}

		// single quote
		npos_t_begin = 0;
		while((npos_t_begin = nodeAttr.find_first_of('\'', npos_t_begin)) != nodeAttr.npos)
		{
			size_t npos_t_end = nodeAttr.find_first_of('\'', npos_t_begin+1);
			if(npos_t_end == nodeAttr.npos) break;

			std::ostringstream stm;
			stm << "xmlattr_";
			stm << attr_num;
			valueMap.insert(std::pair<std::string, std::string>(stm.str(),
					nodeAttr.substr(npos_t_begin+1, npos_t_end-npos_t_begin-1)));
			nodeAttr.replace(npos_t_begin, npos_t_end-npos_t_begin+1, stm.str());

			npos_t_begin = npos_t_end+1;
			attr_num++;
		}

		// remove whitespace
		while((npos_t_begin = nodeAttr.find(" =")) != nodeAttr.npos)
		{
			nodeAttr.erase(npos_t_begin, 1);
		}
		while((npos_t_begin = nodeAttr.find("  ")) != nodeAttr.npos)
		{
			nodeAttr.erase(npos_t_begin, 1);
		}
		while((npos_t_begin = nodeAttr.find("= ")) != nodeAttr.npos)
		{
			nodeAttr.erase(npos_t_begin+1, 1);
		}

		// remove heading/tailing
		nodeAttr.erase(nodeAttr.length()-1, 1);
		nodeAttr.erase(0, nodeAttr.find_first_of(' ')+1);

		std::istringstream iss(nodeAttr);
		do
		{
			std::string keyValue;
			iss >> keyValue;
			if(keyValue.length() > 0)
			{
				size_t npos_key = keyValue.find_first_of("=");
				std::string keyStr, valueStr;
				keyStr = keyValue.substr(0, npos_key);
				valueStr = keyValue.substr(npos_key+1);
				size_t valueStr_pos;
				while((valueStr_pos = valueStr.find("&lt;")) != valueStr.npos)
				{
					valueStr.replace(valueStr_pos, 4, "<");
				}
				while((valueStr_pos = valueStr.find("&gt;")) != valueStr.npos)
				{
					valueStr.replace(valueStr_pos, 4, ">");
				}
				if(valueMap.find(valueStr) != valueMap.end()) valueStr = valueMap[valueStr];
				attrMap->insert(std::pair<std::string, std::string>(keyStr, valueStr));
			}
		} while(iss);

	}
	return attrMap;
}
