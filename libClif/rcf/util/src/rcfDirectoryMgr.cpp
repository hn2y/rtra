
#include "rcfDirectoryMgr.hpp"
#include "rcfLineParser.hpp"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include <iostream>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include "config.h"

using namespace std;

void rcfDirectoryMgr::basicInit()
{
   nlevels = 0;
   root = NULL;
   func = NULL;
   object_failed = 0;
   memset(tokens,0,sizeof(tokens));
   usr_buffer = NULL;
   leading_separator = 0;
   memset(separator,0,sizeof(separator));
   if(0 == strcmp("UNAME_SYSTEM","SunOS"))
   {
      separator[0] = '/';
   }
   else if(0 == strcmp("UNAME_SYSTEM","Linux"))
   {
      separator[0] = '/';
   }
   else if(0 == strcmp("UNAME_SYSTEM","Windows"))
   {
      separator[0] = '\\';
   }
   else
   {
      // default to UNIX
      separator[0] = '/';
   }
   logid = NULL;
   create_depth = 0;   
}

rcfDirectoryMgr::rcfDirectoryMgr()
{
   basicInit();
}

int rcfDirectoryMgr::setRoot(char *usr_root)
{
   func = (char*)"rcfDirectoryMgr::setRoot";
   DIR *pdir = NULL;
   int length=0;

   if(NULL == usr_root)
   {
      reportError("Invalid or empty string passed as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if(0 == (length = strlen(usr_root)))
   {
      reportError("Invalid or empty string passed as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   pdir = opendir(usr_root);
   if(NULL != pdir )
   {
      // all is well
      closedir(pdir);
   }
   else
   {
      fprintf(stderr,"ERROR: %s user declared root directory %s does not exist \n",func,usr_root);
      object_failed = 1;
      return(CF_FAIL);
   }

   // all is well, save the root

   root = new char[length+1];
   if(NULL == root)
   {
      reportError("Failure to allocate memory\n");
      return(CF_FAIL);
   }
   strcpy(root,usr_root);
   return(CF_OK);
}

rcfDirectoryMgr::rcfDirectoryMgr(char *usr_root)
{
   basicInit();
   setRoot(usr_root);
}

rcfDirectoryMgr::~rcfDirectoryMgr()
{
   if(NULL != root)
   {
       delete[] root;
       root = NULL;
   }
   if(nlevels > 0)
   {
      for(int i=0; i<nlevels; i++)
      {
          if(NULL != tokens[i])
          {
              delete[] tokens[i];
              tokens[i] = NULL;              
          }
      }
   }
   if(NULL != usr_buffer)
   {
       delete[] usr_buffer;
       usr_buffer = NULL;
   }
   for(unsigned int i=0; i<dirnames.size(); i++)
   {
       if(NULL != dirnames[i])
       {
           delete dirnames[i];
           dirnames[i] = NULL;
       }
   }
   for(unsigned int i=0; i<filnames.size(); i++)
   {
       if(NULL != filnames[i])
       {
           delete filnames[i];
           filnames[i] = NULL;
       }
   }
   dirnames.clear();
   filnames.clear();
   for(unsigned int i=0; i<wrkdirs.size(); i++)
   {
       if(NULL != wrkdirs[i])
       {
           delete wrkdirs[i];
           wrkdirs[i] = NULL;
       }
   }
   for(unsigned int i=0; i<wrkfils.size(); i++)
   {
       if(NULL != wrkfils[i])
       {
           delete wrkfils[i];
           wrkfils[i] = NULL;
       }
   }
   dirnames.clear();
   filnames.clear();
   
}

int rcfDirectoryMgr::loadLine(char *line)
{
   return(loadLine(line,0));
}

int rcfDirectoryMgr::loadLine(char *line, int clip)
{
   func = (char*)"rcfDirectoryMgr::loadLine";
   int length=0;
   char *lline = NULL;
   char *wline = line;

   if(1 == object_failed)
   {
      reportError("This is a failed object. Can not execute functional command\n");
      return(CF_FAIL);
   }

   if(NULL == line)
   {
      reportError("NULL pointer passed as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if(0 == (length = strlen(line)))
   {
      reportError("Empty string passed as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if(clip < 0)
   {
      reportError("Clip request can not be negative\n");
      object_failed = 1;
      return(CF_FAIL);
   }

   // check if the last character is OS separator, if not,
   // create temporary buffer and append it
   if(line[length-1] != separator[0])
   {
      lline = new char[length+2];
      if(NULL == lline)
      {
         reportError("Failure to allocate temporary buffer");
         return(CF_FAIL);
      }
      strcpy(lline,line);
      strcat(lline,separator);
      wline = lline;
   }

   // check for the leading separator
   if( wline[0] == separator[0] )
   {
      leading_separator = 1;
   }
   

   

   // put the line parser on the stack
   rcfLineParser lparser(MAXTOKENS,separator);

   int rtn = lparser.parseLine(wline);
   if(CF_OK != rtn)
   {
      reportError("Failure to parse the line by the line parser\n");
      object_failed = 1;
      return(CF_FAIL);
   }

   // parse successful. Check how many tokens we got
   rtn = lparser.getNumberOfTokens(&nlevels);
   if(CF_OK != rtn)
   {
      reportError("Failure to parse the line (obtain number of levels from the line parser)\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if( nlevels >= MAXTOKENS)
   {
      reportError("Number of levels too large. Please rebuild this object and update the maximum number\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if(clip >= nlevels)
   {
      reportError("Clip request exceeds number of levels\n");
      fprintf(stderr,"Clip request: %d  Number of levels: %d\n",clip,nlevels);
      object_failed = 1;
      return(CF_FAIL);
   }
   nlevels -= clip;

   // fill the token array
   char *tmpbuf;

   int realindex = 0;

   for(int i=0; i<nlevels; i++)
   {
      tmpbuf = NULL;
      rtn = lparser.getToken(i,&tmpbuf);
      if((CF_OK != rtn) || (NULL == tmpbuf) )
      {
         fprintf(stderr,"ERROR: rcfDirectoryMgr::loadLine failure to obtain parsed token at index %d of %d\n",i,nlevels-1);
         if(CF_OK != rtn)fprintf(stderr,"REASON: failure returned when querying for token\n");
         if(NULL == tmpbuf)fprintf(stderr,"REASON: NULL pointer returned when querying for token\n");
         object_failed = 1;
         return(CF_FAIL);
      }
      if(0 == (length = strlen(tmpbuf)) )
      {
         // not an error, could be empty element due to typo, like double slash in unit, just skip
         continue;
      }
      rtn = cleanStringToken(tmpbuf);
      if(CF_OK != rtn)
      {
         fprintf(stderr,"ERROR: rcfDirectoryMgr::loadLine failure to clean buffer for token %d out of %d\n",i,nlevels-1);
         object_failed = 1;
         return(CF_FAIL);
      }
      // allocate memory
      tokens[realindex] = new char[length+1];
      if(NULL == tokens[realindex])
      {
         fprintf(stderr,"ERROR: rcfDirectoryMgr::loadLine failure to allocate memory for token %d out of %d\n",i,nlevels-1);
         object_failed = 1;
         return(CF_FAIL);
      }
      strcpy(tokens[realindex],tmpbuf);
      realindex++;
   }
   if(realindex <= 0)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::loadLine The line was processed succesfully, but all tokens were empty. This is not a valid line in any operating system\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   else
   {
      //reset to actual number of tokens, after deletion of empty tokens.
      nlevels = realindex;
   }
   if(NULL != lline)
   {
       delete[] lline;
       lline = NULL;
   }

   return(CF_OK);
}

int rcfDirectoryMgr::appendLine(char *token)
{
   func = (char*)"rcfDirectoryMgr::appendLine";

   if(NULL == token)
   {
      reportError("NULL pointer passed in as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if(0 == strlen(token) )
   {
      reportError("Empty string passed in as data\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   if( nlevels >= MAXTOKENS )
   {
      reportError("This object is full, can not be appended\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   int length = strlen(token);
   tokens[nlevels] = new char[length+1];
   if(NULL == tokens[nlevels])
   {
      reportError("Failure to allocate memory for a new token\n");
      object_failed = 1;
      return(CF_FAIL);
   }
   strcpy(tokens[nlevels],token);
   nlevels++;

   return(CF_OK);
}

// this function tests that a partial directory
// tree exists
int rcfDirectoryMgr::operateOnLine(int select, int ulevel, int *verify)
{
   func = (char*)"rcfDirectoryMgr::operateOnLine";
   DIR *pdir = NULL;
   *verify = 0;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(CF_FAIL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(CF_FAIL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(CF_FAIL);
     }
   }
   // build the string

   // compute necessary length
   int length=0;
   if(NULL != root)length = strlen(root);
   for(int i=0; i<level; i++)
   {
      length += strlen(tokens[i]);
   }
   // allocate memory for temporary buffer
   char *tbuf = NULL;
   tbuf = new char[length+level+30];
   if(NULL == tbuf)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::operateOnLine failure to allocate memory for the command buffer. Size of request: %d\n",length+level+30);
      return(CF_FAIL);
   }

   switch(select)
   {
      case 0:
         strcpy(tbuf,"");
         break;
      case 1:
         strcpy(tbuf,"rm -rf ");
         break;
      case 2:
         strcpy(tbuf,"rm -rf ");
         // adjust level if needed
         if( (create_depth > 1) && (ulevel == nlevels) )
         {
            // this condition means that the user wants to delete full
            // created line
            int tlevel = nlevels - create_depth + 1;
            if(tlevel < level)level = tlevel;
         }
         break;
      default:
         reportError("Illegal Selector value. Must be 0 or 1");
         return(CF_FAIL);
         break;
   }
         

   if(NULL != root)
   {
      strcat(tbuf,root);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,separator);
         strcat(tbuf,tokens[i]);
      }
   }
   else
   {
      if(1 == leading_separator)strcat(tbuf,separator);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,tokens[i]);
         strcat(tbuf,separator);
      }
   }


   // finally do the test
   switch(select)
   {
      case 0:
         pdir = opendir(tbuf);
         if(NULL != pdir)
         {
            *verify = 1;
            closedir(pdir); 
         }
         break;
      case 1:
      case 2:
         if(0 == system(tbuf))*verify = 1;
         break;
      default:
         reportError("Illegal Selector value. Must be 0 or 1");
         return(CF_FAIL);
         break;
   }
   if(NULL != tbuf)
   {
       delete[] tbuf;
       tbuf = NULL;
   }

   return(CF_OK);
}

int rcfDirectoryMgr::testFile(char *file,int level, int *verify)
{
   return(operateOnFile(file,0,level,verify));
}

int rcfDirectoryMgr::testFile(char *file,int *verify)
{
   return(testFile(file,nlevels,verify));
}

int rcfDirectoryMgr::testName(char *file,int level, int *verify)
{
   return(operateOnFile(file,2,level,verify));
}

int rcfDirectoryMgr::testName(char *file,int *verify)
{
   return(testName(file,nlevels,verify));
}

int rcfDirectoryMgr::deleteFile(char *file,int level, int *verify)
{
   return(operateOnFile(file,1,level,verify));
}

int rcfDirectoryMgr::deleteFile(char *file,int *verify)
{
   return(deleteFile(file,nlevels,verify));
}

int rcfDirectoryMgr::deleteDirectory(char *file,int level, int *verify)
{
   return(operateOnFile(file,3,level,verify));
}

int rcfDirectoryMgr::deleteDirectory(char *file,int *verify)
{
   return(deleteDirectory(file,nlevels,verify));
}

int rcfDirectoryMgr::clearFilesOnLine()
{
   return(clearLineContents(0));
}
int rcfDirectoryMgr::clearDirectoriesOnLine()
{
   return(clearLineContents(1));
}
int rcfDirectoryMgr::clearLine()
{
   return(clearLineContents(2));
}

int rcfDirectoryMgr::clearLineContents(int select)
{
   func = (char*)"rcfDirectoryMgr::clearLineContents";
   int rtn;
   int verify;
   int length;

   rtn = examineLine(nlevels);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine line");
      return(CF_FAIL);
   }

   switch(select)
   {
      case 0:
         length = filnames.size();
         if(length > 0)
         {
            for(int i=0; i<length; i++)
            {
               rtn = deleteFile((char*)(filnames[i]->c_str()),&verify);
               if( (CF_OK != rtn) || (0 == verify) )
               {
                  reportError("Failure to delete existing file");
                  fprintf(stderr,"Failed delete on file: %s\n",filnames[i]->c_str());
                  return(CF_FAIL);
               }
            }
         }
         break;
      case 1:
         length = dirnames.size();
         if(length > 0)
         {
            for(int i=0; i<length; i++)
            {
               rtn = deleteDirectory((char*)(dirnames[i]->c_str()),&verify);
               if( (CF_OK != rtn) || (0 == verify) )
               {
                  reportError("Failure to delete existing file");
                  fprintf(stderr,"Failed delete on file: %s\n",dirnames[i]->c_str());
                  return(CF_FAIL);
               }
            }
         }
         break;
      case 2:
         length = filnames.size();
         if(length > 0)
         {
            for(int i=0; i<length; i++)
            {
               rtn = deleteFile((char*)(filnames[i]->c_str()),&verify);
               if( (CF_OK != rtn) || (0 == verify) )
               {
                  reportError("Failure to delete existing file");
                  fprintf(stderr,"Failed delete on file: %s\n",filnames[i]->c_str());
                  return(CF_FAIL);
               }
            }
         }
         length = dirnames.size();
         if(length > 0)
         {
            for(int i=0; i<length; i++)
            {
               rtn = deleteDirectory((char*)(dirnames[i]->c_str()),&verify);
               if( (CF_OK != rtn) || (0 == verify) )
               {
                  reportError("Failure to delete existing file");
                  fprintf(stderr,"Failed delete on file: %s\n",dirnames[i]->c_str());
                  return(CF_FAIL);
               }
            }
         }
         break;
      default:
         reportError("Illegal selector value");
         fprintf(stderr,"selector seen: %d\n",select);
         break;
   }

   return(CF_OK);
}

int rcfDirectoryMgr::filterFilesBySubstring(std::string usr, int *nfiles)
{
   func = (char*)"rcfDirectoryMgr::filterFilesBySubstring";
   int rtn;
   *nfiles = 0;

   // sanity check
   if(usr.size() <= 0)
   {
      reportError("User asks to filter files by empty substring");
      return(CF_FAIL);
   }
   if( (filnames.size() <=0) && (dirnames.size() <= 0) )
   {
      rtn = examineLine(nlevels);
      if(CF_OK != rtn)
      {
         reportError("Failure to examine line");
         return(CF_FAIL);
      }
   }
   if(filnames.size() <= 0)
   {
      // nothing to do
      return(CF_OK);
   }

   for(unsigned int i=0; i<wrkfils.size(); i++)
   {
       if(NULL != wrkfils[i])
       {
           delete wrkfils[i];
           wrkfils[i] = NULL;
       }
   }
   wrkfils.clear();

   size_t found;
   for(unsigned int i=0; i<filnames.size(); i++)
   {
     if(NULL != filnames[i])
     {
        found = filnames[i]->find(usr);
        if(found != std::string::npos)
        {
            wrkfils.push_back(filnames[i]);
        }
     }
   }
   *nfiles = wrkfils.size();
   return(CF_OK);
}

int rcfDirectoryMgr::filterFilesByType(std::string usr, int *nfiles)
{
   func = (char*)"rcfDirectoryMgr::filterFilesBySubstring";
   int rtn;
   *nfiles = 0;

   // sanity check
   if(usr.size() <= 0)
   {
      reportError("User asks to filter files by empty substring");
      return(CF_FAIL);
   }
   if( (filnames.size() <=0) && (dirnames.size() <= 0) )
   {
      rtn = examineLine(nlevels);
      if(CF_OK != rtn)
      {
         reportError("Failure to examine line");
         return(CF_FAIL);
      }
   }
   if(filnames.size() <= 0)
   {
      // nothing to do
      return(CF_OK);
   }

   for(unsigned int i=0; i<wrkfils.size(); i++) 
   {
       if(NULL != wrkfils[i])
       {
           delete wrkfils[i];
           wrkfils[i] = NULL;
       }
   }
   wrkfils.clear();

   std::string tmp = std::string(".") + usr;

   size_t found;
   //int usrlength = tmp.size();
   //int clength;
   size_t clength;

   for(unsigned int i=0; i<filnames.size(); i++)
   {
     if(NULL != filnames[i])
     {
        clength = filnames[i]->size() - tmp.size();
        found = filnames[i]->find(tmp);
        if(found == clength)
        {
            wrkfils.push_back(filnames[i]);
        }
     }
   }
   *nfiles = wrkfils.size();
   return(CF_OK);
}

int rcfDirectoryMgr::filterDirectoriesBySubstring(std::string usr, int *ndirs)
{
   func = (char*)"rcfDirectoryMgr::filterDirectoriesBySubstring";
   int rtn;
   *ndirs = 0;

   // sanity check
   if(usr.size() <= 0)
   {
      reportError("User asks to filter files by empty substring");
      return(CF_FAIL);
   }
   if( (filnames.size() <=0) && (dirnames.size() <= 0) )
   {
      rtn = examineLine(nlevels);
      if(CF_OK != rtn)
      {
         reportError("Failure to examine line");
         return(CF_FAIL);
      }
   }
   if(dirnames.size() <= 0)
   {
      // nothing to do
      return(CF_OK);
   }

   for(unsigned int i=0; i<wrkdirs.size(); i++) 
   {
       if(NULL != wrkdirs[i])
       {
           delete wrkdirs[i];
           wrkdirs[i] = NULL;
       }
   }
   wrkdirs.clear();

   size_t found;
   for(unsigned int i=0; i<dirnames.size(); i++)
   {
     if(NULL != dirnames[i])
     {
        found = dirnames[i]->find(usr);
        if(found != std::string::npos)
        {
            wrkdirs.push_back(dirnames[i]);
        }
     }
   }
   *ndirs = wrkdirs.size();
   return(CF_OK);
}

int rcfDirectoryMgr::statLine(int *ndirs, int *nfiles, int ulevel)
{
   func = (char*)"rcfDirectoryMgr::statFilesInLine";
   int rtn = examineLine(ulevel);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine line");
      return(CF_FAIL);
   }
   *nfiles = filnames.size();
   *ndirs = dirnames.size();
   return(CF_OK);
}

int rcfDirectoryMgr::statLine(int *ndirs, int *nfiles)
{
   func = (char*)"rcfDirectoryMgr::statFilesInLine";
   int rtn = examineLine(nlevels);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine line");
      return(CF_FAIL);
   }
   *nfiles = filnames.size();
   *ndirs = dirnames.size();
   return(CF_OK);
}

std::string rcfDirectoryMgr::getFileNameInLine(unsigned int index, int *verify)
{
   func = (char*)"rcfDirectoryMgr::getFileNameInLine";
   *verify = 0;

   if( index >= filnames.size() )
   {
      reportError("Illegal index value");
      return(std::string(""));
   }
   *verify = 1;
   return(*(filnames[index]));
}

std::string rcfDirectoryMgr::getDirectoryNameInLine(unsigned int index, int *verify)
{
   func = (char*)"rcfDirectoryMgr::getDirectoryNameInLine";
   *verify = 0;

   if( index >= dirnames.size() )
   {
      reportError("Illegal index value");
      return(std::string(""));
   }
   *verify = 1;
   return(*(dirnames[index]));
}

std::string rcfDirectoryMgr::getFilteredFileName(unsigned int index, int *verify)
{
   func = (char*)"rcfDirectoryMgr::getFilteredFileNameInLine";
   *verify = 0;

   //if( (index < 0) || (index >= wrkfils.size()) )
   if( index >= wrkfils.size() )
   {
      reportError("Illegal index value");
      return(std::string(""));
   }
   *verify = 1;
   return(*(wrkfils[index]));
}

std::string rcfDirectoryMgr::getFilteredDirectoryName(unsigned int index, int *verify)
{
   func = (char*)"rcfDirectoryMgr::getFilteredDirectoryNameInLine";
   *verify = 0;

   if( index >= wrkdirs.size() )
   {
      reportError("Illegal index value");
      return(std::string(""));
   }
   *verify = 1;
   return(*(wrkdirs[index]));
}

int rcfDirectoryMgr::examineLine(int ulevel)
{
   func = (char*)"rcfDirectoryMgr::examineLine";
   DIR *pdir = NULL;
   struct dirent *pent = NULL;
   struct stat stbuf;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(CF_FAIL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(CF_FAIL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(CF_FAIL);
     }
   }
   // build the string

   // compute necessary length
   int length=0;
   if(NULL != root)
   {
       length = strlen(root);
   }
   for(int i=0; i<level; i++)
   {
      length += strlen(tokens[i]);
   }
   // allocate memory for temporary buffer
   char *tbuf = NULL;
   tbuf = new char[length+level+30];
   if(NULL == tbuf)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::operateOnLine failure to allocate memory for the command buffer. Size of request: %d\n",length+level+30);
      return(CF_FAIL);
   }

   strcpy(tbuf,"");

   if(NULL != root)
   {
      strcat(tbuf,root);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,separator);
         strcat(tbuf,tokens[i]);
      }
   }
   else
   {
      if(1 == leading_separator)strcat(tbuf,separator);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,tokens[i]);
         strcat(tbuf,separator);
      }
   }

   // clear lists, if they do exist
   for(unsigned int i=0; i<dirnames.size(); i++)
   {
       if(NULL != dirnames[i])
       {
           delete dirnames[i];
           dirnames[i] = NULL;
       }
   }
   for(unsigned int i=0; i<filnames.size(); i++)
   {
       if(NULL != filnames[i])
       {
           delete filnames[i];
           filnames[i] = NULL;
       }
   }
   dirnames.clear();
   filnames.clear();


   // finally, build lists 
   pdir = opendir(tbuf);
   if(NULL != pdir)
   {
      errno = 0;
      while( NULL != (pent = readdir(pdir)) ) 
      {
         if(0 == errno)
         {
            std::string qname = std::string(tbuf) +std::string(separator) + std::string(pent->d_name);
            stat(qname.c_str(),&stbuf);
            if(S_IFDIR == (stbuf.st_mode & S_IFMT) )
            {
               // this is a directory
               dirnames.push_back(new std::string(pent->d_name));
            }
            else if(S_IFREG == (stbuf.st_mode & S_IFMT) )
            {
               // this is a file 
               filnames.push_back(new std::string(pent->d_name));
            }
            else
            {
               //reportWarning("unhandled file type");
            }
      
         }
         else errno = 0;
      }

      closedir(pdir); 
      delete[] tbuf;
      tbuf = NULL;
   }

   return(CF_OK);
}

int rcfDirectoryMgr::operateOnFile(char *file,int select, int ulevel, int *verify)
{
   func = (char*)"rcfDirectoryMgr::operateOnFile";
   DIR *pdir = NULL;
   struct dirent *pent = NULL;
   struct stat stbuf;
   *verify = 0;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(CF_FAIL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(CF_FAIL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(CF_FAIL);
     }
   }
   if(NULL == file)
   {
      reportError("NULL pointer in lieu of file name\n");
      return(CF_FAIL);
   }
   if(0 == strlen(file) )
   {
      reportError("Empty string in lieu of file name\n");
      return(CF_FAIL);
   }
   // build the string

   // compute necessary length
   int length=0;
   if(NULL != root)length = strlen(root);
   for(int i=0; i<level; i++)
   {
      length += strlen(tokens[i]);
   }

   length += strlen(file);

   // allocate memory for temporary buffer
   char *tbuf = NULL;
   tbuf = new char[length+level+30];
   if(NULL == tbuf)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::operateOnLine failure to allocate memory for the command buffer. Size of request: %d\n",length+level+30);
      return(CF_FAIL);
   }

   switch(select)
   {
      case 0:
      case 2:
         strcpy(tbuf,"");
         break;
      case 1:
         strcpy(tbuf,"rm ");
         break;
      case 3:
         strcpy(tbuf,"rm -rf ");
         break;
      default:
         reportError("Illegal Selector value. Must be 0 or 1");
         return(CF_FAIL);
         break;
   }
         

   if(NULL != root)
   {
      strcat(tbuf,root);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,separator);
         strcat(tbuf,tokens[i]);
      }
   }
   else
   {
      if(1 == leading_separator)strcat(tbuf,separator);
      for(int i=0; i<level; i++)
      {
         strcat(tbuf,tokens[i]);
         strcat(tbuf,separator);
      }
   }

   // finally do the test
   switch(select)
   {
      case 0:
         pdir = opendir(tbuf);
         if(NULL != pdir)
         {
            while( NULL != (pent = readdir(pdir)) ) {
               if(0 == errno)
               {
                  std::string qname = std::string(tbuf) +std::string(separator) + std::string(pent->d_name);
                  stat(qname.c_str(),&stbuf);
                  stat(pent->d_name,&stbuf);
                  if(S_IFREG == (stbuf.st_mode & S_IFMT))
                  {
                     if(0 == strcmp(pent->d_name,file))
                     {
                        // we found it, report accordingly
                        *verify = 1;
                        closedir(pdir);
                        delete[] tbuf;
                        tbuf = NULL;
                        return(CF_OK);
                     }
                  }
               }
            }
         }
         else
         {
            *verify = 0;
            delete[] tbuf;
            tbuf = NULL;
            return(CF_OK);
         }
         break;
      case 1:
      case 3:
         strcat(tbuf,separator);
         strcat(tbuf,file);
         if(0 == system(tbuf))*verify = 1;
         delete[] tbuf;
         tbuf = NULL;
         return(CF_OK);
         break;
      case 2:
         pdir = opendir(tbuf);
         if(NULL != pdir)
         {
            while( NULL != (pent = readdir(pdir)) ) 
            {
               if(0 == errno)
               {
                  if(0 == strcmp(pent->d_name,file))
                  {
                     // we found it, report accordingly
                     *verify = 1;
                     closedir(pdir);
                     delete[] tbuf;
                     tbuf = NULL;
                     return(CF_OK);
                  }
               }
            }
         }
         else
         {
            *verify = 0;
            delete[] tbuf;
            tbuf = NULL;
            return(CF_OK);
         }
         break;
      default:
         reportError("Illegal Selector value. Must be 0 or 1");
         return(CF_FAIL);
         break;
   }

   if(NULL != tbuf)
   {
       delete[] tbuf;
       tbuf = NULL;
   }

   return(CF_OK);
}

int rcfDirectoryMgr::testLine(int level, int *verify)
{
   return(operateOnLine(0,level,verify));
}

int rcfDirectoryMgr::testLine(int *verify)
{
   return(testLine(nlevels,verify));
}

int rcfDirectoryMgr::deleteLine(int level, int *verify)
{
   return(operateOnLine(1,level,verify));
}

int rcfDirectoryMgr::deleteLine(int *verify)
{
   return(operateOnLine(2,nlevels,verify));
}

int rcfDirectoryMgr::createLine(int ulevel,int *verify)
{
   func = (char*)"rcfDirectoryMgr::createLine_int";
   *verify = 0;
   int create = 0;
   DIR *pdir = NULL;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(CF_FAIL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(CF_FAIL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(CF_FAIL);
     
     }
   }
   // build the string

   // compute necessary length
   int length=0;
   if(NULL != root)length = strlen(root);
   for(int i=0; i<level; i++)
   {
      length += strlen(tokens[i]);
   }
   // allocate memory for temporary buffer
   char *tbuf = NULL;
   tbuf = new char[length+level+30];
   if(NULL == tbuf)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::createLine failure to allocate memory for the command buffer. Size of request: %d\n",length+level+20);
      return(CF_FAIL);
   }
   char *tbufm = NULL;
   tbufm = new char[length+level+30];
   if(NULL == tbufm)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::createLine failure to allocate memory for the command buffer. Size of request: %d\n",length+level+20);
      return(CF_FAIL);
   }

   strcpy(tbuf,"");
   strcpy(tbufm,"mkdir -p ");

   if(NULL != root)
   {
      strcat(tbuf,root);
      strcat(tbuf,separator);
      strcat(tbufm,root);
      strcat(tbufm,separator);
   }
   else
   {
      if(1 == leading_separator)
      {
         strcat(tbuf,separator);
         strcat(tbufm,separator);
      }
   }

   for(int i=0; i<level; i++)
   {
      strcat(tbuf,tokens[i]);
      strcat(tbuf,separator);
      strcat(tbufm,separator);
      strcat(tbufm,tokens[i]);
      if(NULL != (pdir = opendir(tbuf)) )
      {
         // do nothing
         closedir(pdir);
         pdir = NULL;
         continue;
      }
      else
      {
         if(0 != system(tbufm) )
         {
            fprintf(stderr,"ERROR: rcfDirectoryMgr::createLine failure to create directory %s\n",tbufm);
            return(CF_FAIL);
         }
         else create++;
      }
   }

   if(0 == create)*verify = 2; //no create, tree there already
   else *verify = 1; // we created something

   create_depth = create;

   delete[] tbuf;
   tbuf = NULL;
   delete[] tbufm;
   tbufm = NULL;

   return(CF_OK);
}

int rcfDirectoryMgr::createLine(int *verify)
{
   return(createLine(nlevels,verify));
}

int rcfDirectoryMgr::getNumberOfLevels(int *usr_nlevels)
{
   func = (char*)"rcfDirectoryMgr::getNumberOfLevels";

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(CF_FAIL);
   }
   *usr_nlevels = nlevels;
   return(CF_OK);
}
char* rcfDirectoryMgr::getLevel(int ulevel, int *verify)
{
   func = (char*)"rcfDirectoryMgr::getLevel";
   *verify = 0;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(NULL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(NULL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(NULL);
     }
   }

   return(tokens[level]);
}

char* rcfDirectoryMgr::makePathBufferSelect(int select, int ulevel, int *verify, char *fname)
{
   func = (char*)"rcfDirectoryMgr::makePathBuffer_int";
   *verify = 0;
   int level;

   if(1 == object_failed)
   {
      reportError("This is a failed object, it will not perform operations\n");
      return(NULL);
   }

   if((ulevel > nlevels) || (0 == ulevel) )
   {
      reportError("Illegal level request\n");
      fprintf(stderr,"level request: %d \t is positive and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
      return(NULL);
   }
   if(ulevel > 0)
   {
      level = ulevel;
   }
   else
   {
     level = nlevels + ulevel; 
     if(level <= 0)
     {
         reportError("Illegal level request");
         fprintf(stderr,"level request: %d \t is negative and exceeds the number of levels in the object which is: %d\n",ulevel,nlevels);
         return(NULL);
     }
   }

   if(NULL != usr_buffer)
   {
       delete[] usr_buffer;
       usr_buffer = NULL;
   }
   
   // build the string

   // compute necessary length
   int length=0;
   if(NULL != root)length = strlen(root);
   for(int i=0; i<level; i++)
   {
      length += strlen(tokens[i]);
   }
   if(NULL != fname)length += (strlen(fname) +5);
   // allocate memory for temporary buffer
   usr_buffer = new char[length+level+20];
   if(NULL == usr_buffer)
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::makePathBuffer failure to allocate memory for the command buffer. Size of request: %d\n",length+level+20);
      return(NULL);
   }


   if((NULL != root) && (0 == select) )
   {
      strcpy(usr_buffer,root);
      strcat(usr_buffer,separator);
      strcat(usr_buffer,tokens[0]);
   }
   else
   {
      if(0 == leading_separator)
      {
         strcpy(usr_buffer,tokens[0]);
      }
      else
      {
         strcpy(usr_buffer,separator);
         strcat(usr_buffer,tokens[0]);
      }
   }

   for(int i=1; i<level; i++)
   {
      strcat(usr_buffer,separator);
      strcat(usr_buffer,tokens[i]);
   }
   if(NULL != fname)
   {
      strcat(usr_buffer,separator);
      strcat(usr_buffer,fname);
   }

   *verify = 1;
   return(usr_buffer);
}

char* rcfDirectoryMgr::makePathBuffer(int level, int *verify, char *fname)
{
   return(makePathBufferSelect(0,level,verify,fname));
}
std::string rcfDirectoryMgr::makePathBuffer(int level, int *verify, std::string fname)
{

   char *buf = NULL;
   buf = makePathBufferSelect(0,level,verify,(char*)(fname.c_str()));
   if((NULL == buf) || (0 == *verify) )
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::makePathBuffer_string Failure to to execute underlying function\n");
      return(std::string(""));
   }
   std::string lpath(buf);
   delete[] usr_buffer;
   usr_buffer = NULL;
   return(lpath);
}

char* rcfDirectoryMgr::makePathBuffer(int *verify, char *fname)
{
   return(makePathBuffer(nlevels,verify,fname));
}

std::string rcfDirectoryMgr::makePathBuffer(int *verify, std::string fname)
{

   char *buf = NULL;
   buf = makePathBuffer(nlevels,verify,(char*)(fname.c_str()));
   if((NULL == buf) || (0 == *verify) )
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::makePathBuffer_string Failure to to execute underlying function\n");
      return(std::string(""));
   }
   std::string lpath(buf);
   delete[] usr_buffer;
   usr_buffer = NULL;
   return(lpath);
}

char* rcfDirectoryMgr::makeRelPathBuffer(int level, int *verify, char *fname)
{
   return(makePathBufferSelect(1,level,verify,fname));
}
std::string rcfDirectoryMgr::makeRelPathBuffer(int level, int *verify, std::string fname)
{

   char *buf = NULL;
   buf = makePathBufferSelect(1,level,verify,(char*)(fname.c_str()));
   if((NULL == buf) || (0 == *verify) )
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::makePathBuffer_string Failure to to execute underlying function\n");
      return(std::string(""));
   }
   std::string lpath(buf);
   delete[] usr_buffer;
   usr_buffer = NULL;
   return(lpath);
}

char* rcfDirectoryMgr::makeRelPathBuffer(int *verify, char *fname)
{
   return(makeRelPathBuffer(nlevels,verify,fname));
}
std::string rcfDirectoryMgr::makeRelPathBuffer(int *verify, std::string fname)
{

   char *buf = NULL;
   buf = makeRelPathBuffer(nlevels,verify,(char*)(fname.c_str()));
   if((NULL == buf) || (0 == *verify) )
   {
      fprintf(stderr,"ERROR: rcfDirectoryMgr::makePathBuffer_string Failure to to execute underlying function\n");
      return(std::string(""));
   }
   std::string lpath(buf);
   delete[] usr_buffer;
   usr_buffer = NULL;
   return(lpath);
}

int rcfDirectoryMgr::accumulateFilteredFiles(rcfDirectoryMgr &usrdir)
{
   func = (char*)"rcfDirectoryMgr::accumulateFilteredFiles";
   std::string command = "cp ";
   char *topath;
   char *frompath;
   int rtn,verify;

   // sanity checks
   if( !(usrdir.isObjectValid()) )
   {
      reportError("invalid directory object passed as argument");
      return(CF_FAIL);
   }
   if( 0 == usrdir.wrkfils.size() )
   {
      // nothing to do
      return(CF_OK);
   }

   int ifound = 0;
   for(unsigned int i=0; i<usrdir.wrkfils.size(); i++)
   {
      ifound = 0;
      for(unsigned int j=0; j<wrkfils.size(); j++)
      {
         if( (0 == wrkfils[j]->compare(*(usrdir.wrkfils[i]))) )
         {
            // we found it, say so
            ifound = 1;
         }
      }
      if(0 == ifound)
      {
         // copy
         topath = makePathBuffer(&verify,NULL); 
         if( (NULL == topath) || (0 == verify) )
         {
            reportError("Failure to obtain source path");
            return(CF_FAIL);
         }
         frompath = usrdir.makePathBuffer(&verify,(char*)(usrdir.wrkfils[i]->c_str())); 
         if( (NULL == frompath) || (0 == verify) )
         {
            reportError("Failure to obtain target path");
            return(CF_FAIL);
         }
         std::string stfrompath(frompath);
         std::string sttopath(topath);
         //if((0 == strcmp("RCF_OSTYPE","UNIX")) || (0 == strcmp("RCF_OSTYPE","LINUX")) )
         //{
            rtn = repairUnixCommandString(&stfrompath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair frompath string");
               return(CF_FAIL);
            }
            rtn = repairUnixCommandString(&sttopath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair topath string");
               return(CF_FAIL);
            }
         //}
         std::string lbuf = command + stfrompath + std::string(" ") + sttopath + std::string("/."); 

         // do the deed
         if(CF_OK == systemAndLog(lbuf.c_str()) )
         {
            // fine, keep going
         }
         else
         {
            // we failed, lets bail out
            reportError("Failure to execute file copy");
            fprintf(stderr,"Command that failed: %s\n",lbuf.c_str());
            fflush(stderr);
            return(CF_FAIL);
         }
      }
   } 

   return(CF_OK);
}

int rcfDirectoryMgr::compare(rcfDirectoryMgr & usrdir,bool recurse,bool *result)
{
   return(compare(nlevels,usrdir,usrdir.nlevels,recurse,result));
}
int rcfDirectoryMgr::compare(rcfDirectoryMgr & usrdir,bool *result)
{
   return(compare(usrdir,false,result));
}

int rcfDirectoryMgr::compare(int level,rcfDirectoryMgr & usrdir,int usrlvl,bool recurse,bool *result)
{
   func = (char*)"rcfDirectoryMgr::verify";
   string lbuf;
   int rtn;
   int verify;
   *result = false;
   char *topath = NULL;
   char *frompath = NULL;
   int ifound = 0;


   // sanity checks
   if( !(usrdir.isObjectValid()) )
   {
      reportError("invalid directory object passed as argument");
      return(CF_FAIL);
   }


   rtn = examineLine(level);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine Line for this object");
      return(CF_FAIL);
   }
   rtn = usrdir.examineLine(usrlvl);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine Line for the argument object");
      return(CF_FAIL);
   }
   if( (0 == usrdir.dirnames.size()) && (0 == usrdir.filnames.size()) )
   {
      // nothing to do, and we will call it a positive match for now
      *result = true;
      return(CF_OK);
   }

   // first check the files
   for(unsigned int i=0; i<usrdir.filnames.size(); i++)
   {
      ifound = 0;
      for(unsigned int j=0; j<filnames.size(); j++)
      {
         if( (0 == filnames[j]->compare(*(usrdir.filnames[i]))) )
         {
            // we found it, say so
            ifound = 1;
         }
      }
      if(0 == ifound)
      {
         // no match in file names 
         *result = false;
         return(CF_OK);
      }
   }

   // now check directories
   for(unsigned int i=0; i<usrdir.dirnames.size(); i++)
   {
      ifound = 0;
      for(unsigned int j=0; j<dirnames.size(); j++)
      {
         if( (0 == dirnames[j]->compare(*(usrdir.dirnames[i]))) )
         {
            // we found it, say so
            ifound = 1;
         }
      }
      if(0 == ifound)
      {
         // no match in directory names 
         *result = false;
         return(CF_OK);
      }
   }

   // now a more tricky job of recursive check
   if(true == recurse)
   {
      for(unsigned int i=0; i<usrdir.dirnames.size(); i++)
      {
         if( 0 == usrdir.dirnames[i]->compare(std::string(".")) )continue;
         if( 0 == usrdir.dirnames[i]->compare(std::string("..")) )continue;
         ifound = 0;
         for(unsigned int j=0; j<dirnames.size(); j++)
         {
            if( (0 == dirnames[j]->compare(*(usrdir.dirnames[i]))) )
            {
               // we found it, now examine contents recursively 
               topath = makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
               if( (NULL == topath) || (0 == verify) )
               {
                  reportError("Failure to obtain source path");
                  return(CF_FAIL);
               }
               frompath = usrdir.makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
               if( (NULL == frompath) || (0 == verify) )
               {
                  reportError("Failure to obtain target path");
                  return(CF_FAIL);
               }
               rcfDirectoryMgr toMgr;
               rcfDirectoryMgr fromMgr;
               rtn = toMgr.loadLine(topath);
               if(CF_OK != rtn)
               {
                  reportError("Failure to load line");
                  fprintf(stderr,"Line load attempted: %s\n",topath);
                  return(CF_FAIL);
               }
               rtn = fromMgr.loadLine(frompath);
               if(CF_OK != rtn)
               {
                  reportError("Failure to load line");
                  fprintf(stderr,"Line load attempted: %s\n",frompath);
                  return(CF_FAIL);
               }
               bool match;
               rtn = toMgr.compare(fromMgr,true,&match);
               if( (CF_OK == rtn) && (true == match) ) ifound = 1;
               else 
               {
                  *result = false;
                  return(rtn);
               }
            }
         }
         if(0 == ifound)
         {
            // mismatch in files
            *result = false;
            return(CF_OK);
         }
      }
   }

   *result = true;
   return(CF_OK);
}

int rcfDirectoryMgr::accumulate(rcfDirectoryMgr & usrdir)
{
   return(accumulate(nlevels,usrdir,usrdir.nlevels));
}


int rcfDirectoryMgr::accumulate(int level,rcfDirectoryMgr & usrdir,int usrlvl)
{
   func = (char*)"rcfDirectoryMgr::accumulate";
   char buf_cpf[4] = "cp ";
   char buf_cpd[7] = "cp -r ";
   char buf_rsync[10] = "rsync -r ";
   string lbuf;
   char *topath;
   char *frompath;
   int rtn;
   int verify;

   // sanity checks
   if( !(usrdir.isObjectValid()) )
   {
      reportError("invalid directory object passed as argument");
      return(CF_FAIL);
   }


   rtn = examineLine(level);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine Line for this object");
      return(CF_FAIL);
   }
   rtn = usrdir.examineLine(usrlvl);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine Line for the argument object");
      return(CF_FAIL);
   }

   if( (0 == usrdir.dirnames.size()) && (0 == usrdir.filnames.size()) )
   {
      // nothing to do
      return(CF_OK);
   }

   int ifound = 0;
   for(unsigned int i=0; i<usrdir.filnames.size(); i++)
   {
      ifound = 0;
      for(unsigned int j=0; j<filnames.size(); j++)
      {
         if( (0 == filnames[j]->compare(*(usrdir.filnames[i]))) )
         {
            // we found it, say so
            ifound = 1;
         }
      }
      if(0 == ifound)
      {
         // copy
         topath = makePathBuffer(&verify,(char*)(usrdir.filnames[i]->c_str())); 
         if( (NULL == topath) || (0 == verify) )
         {
            reportError("Failure to obtain source path");
            return(CF_FAIL);
         }
         frompath = usrdir.makePathBuffer(&verify,(char*)(usrdir.filnames[i]->c_str())); 
         if( (NULL == frompath) || (0 == verify) )
         {
            reportError("Failure to obtain target path");
            return(CF_FAIL);
         }
         std::string stfrompath(frompath);
         std::string sttopath(topath);
         //if((0 == strcmp("RCF_OSTYPE","UNIX")) || (0 == strcmp("RCF_OSTYPE","LINUX")) )
         //{
            rtn = repairUnixCommandString(&stfrompath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair frompath string");
               return(CF_FAIL);
            }
            rtn = repairUnixCommandString(&sttopath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair topath string");
               return(CF_FAIL);
            }
         //}
         lbuf = std::string(buf_cpf) + stfrompath +std::string("/") + std::string(" ") + sttopath + std::string("/");; 

         // do the deed
         if(CF_OK == systemAndLog(lbuf.c_str()) )
         {
            // fine, keep going
         }
         else
         {
            // we failed, lets bail out
            reportError("Failure to execute file copy");
            fprintf(stderr,"Command that failed: %s\n",lbuf.c_str());
            fflush(stderr);
            return(CF_FAIL);
         }
      }
   } 

   for(unsigned int i=0; i<usrdir.dirnames.size(); i++)
   {

      if( 0 == usrdir.dirnames[i]->compare(std::string(".")) )continue;
      if( 0 == usrdir.dirnames[i]->compare(std::string("..")) )continue;

      ifound = 0;
      for(unsigned int j=0; j<dirnames.size(); j++)
      {
         if( (0 == dirnames[j]->compare(*(usrdir.dirnames[i]))) )
         {
            // we found it, say so
            topath = makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
            if( (NULL == topath) || (0 == verify) )
            {
               reportError("Failure to obtain source path");
               return(CF_FAIL);
            }
            frompath = usrdir.makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
            if( (NULL == frompath) || (0 == verify) )
            {
               reportError("Failure to obtain target path");
               return(CF_FAIL);
            }
            rcfDirectoryMgr toMgr;
            rcfDirectoryMgr fromMgr;
            rtn = toMgr.loadLine(topath);
            if(CF_OK != rtn)
            {
               reportError("Failure to load line");
               fprintf(stderr,"Line load attempted: %s\n",topath);
               return(CF_FAIL);
            }
            rtn = fromMgr.loadLine(frompath);
            if(CF_OK != rtn)
            {
               reportError("Failure to load line");
               fprintf(stderr,"Line load attempted: %s\n",frompath);
               return(CF_FAIL);
            }
            bool match;
            rtn = toMgr.compare(fromMgr,true,&match);
            if( (CF_OK == rtn) && (true == match) ) ifound = 1;
         }
      }
      if(0 == ifound)
      {
         // copy
         topath = makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
         if( (NULL == topath) || (0 == verify) )
         {
            reportError("Failure to obtain source path");
            return(CF_FAIL);
         }
         frompath = usrdir.makePathBuffer(&verify,(char*)(usrdir.dirnames[i]->c_str())); 
         if( (NULL == frompath) || (0 == verify) )
         {
            reportError("Failure to obtain target path");
            return(CF_FAIL);
         }
         rcfDirectoryMgr toMgr;
         rtn = toMgr.loadLine(topath);
         if(CF_OK != rtn)
         {
            reportError("Failure to load line");
            fprintf(stderr,"Line load attempted: %s\n",topath);
            return(CF_FAIL);
         }
         std::string stfrompath(frompath);
         std::string sttopath(topath);
        // if((0 == strcmp("RCF_OSTYPE","UNIX")) || (0 == strcmp("RCF_OSTYPE","LINUX")) )
        // {
            rtn = repairUnixCommandString(&stfrompath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair frompath string");
               return(CF_FAIL);
            }
            rtn = repairUnixCommandString(&sttopath);
            if(CF_OK != rtn)
            {
               reportError("Failure to repair topath string");
               return(CF_FAIL);
            }
         //}
         // check if the target directory line exists
         rtn = toMgr.testLine(&verify);
         if(CF_OK != rtn)
         {
            reportError("Failure to test line");
            fprintf(stderr,"Line tested: %s\n",topath);
            return(CF_FAIL);
         } 
         if(0 == verify)
         {
            lbuf = std::string(buf_cpd) + stfrompath + std::string("/") + std::string(" ") + sttopath + std::string("/"); 
         }
         else
         {
            lbuf = std::string(buf_rsync) + stfrompath + std::string("/") + std::string(" ") + sttopath + std::string("/"); 
         }

         // do the deed
         if(CF_OK == systemAndLog(lbuf.c_str()) )
         {
            // fine, keep going
         }
         else
         {
            // we failed, lets bail out
            reportError("Failure to execute file copy");
            fprintf(stderr,"Command that failed: %s\n",lbuf.c_str());
            fflush(stderr);
            return(CF_FAIL);
         }
      }
   } 

   return(CF_OK);
}

int rcfDirectoryMgr::dropData( rcfDirectoryMgr & usrdir)
{
   func = (char*)"rcfDirectoryMgr::dropData";
   int rtn,verify;

   // sanity checks
   if( !(usrdir.isObjectValid()) )
   {
      reportError("invalid directory object passed as argument");
      return(CF_FAIL);
   }

   rtn = clearFilesOnLine();
   if(CF_OK != rtn)
   {
      reportError("Failure to clear Line for this object");
      return(CF_FAIL);
   }
   rtn = usrdir.examineLine(usrdir.nlevels);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine Line for the argument object");
      return(CF_FAIL);
   }

   std::string topath;
   std::string frompath;
   std::string tmpath;
   std::string command("cp ");
   std::string fcommand;
   std::string tbuf;
   rcfDirectoryMgr *tmgr;


   topath = makePathBuffer(&verify,std::string("."));
   if(0 == verify)
   {
      reportError("Failure to create target path buffer");
      return(CF_FAIL);
   }

   for(unsigned int i=0; i<usrdir.dirnames.size(); i++)
   {
      tbuf = *(usrdir.dirnames[i]);
      if( (0 == tbuf.compare(std::string("."))) || (0 == tbuf.compare(std::string(".."))))continue;
      tmpath = usrdir.makePathBuffer(&verify,tbuf );
      tmgr = new rcfDirectoryMgr;
      if(NULL == tmgr)
      {
         reportError("Failure to allocate memory for directory manager");
         return(CF_FAIL);
      }
      rtn = tmgr->loadLine((char*)(tmpath.c_str()));
      if(CF_OK != rtn)
      {
         reportError("Failure to Load Line");
         fprintf(stderr,"Line: %s\n",tmpath.c_str());
         return(CF_FAIL);
      }
      // additional precautionary test
      rtn = tmgr->testLine(&verify);
      if((CF_OK != rtn) || (0 == verify))
      {
         reportError("Failure to test temporary line");
         fprintf(stderr,"Line: %s\n",tmpath.c_str());
         fprintf(stderr,"rtn: %d  verify: %d\n",rtn,verify);
         return(CF_FAIL);
      }
      rtn = tmgr->examineLine(tmgr->nlevels);
      if(CF_OK != rtn)
      {
         reportError("Failure to examine temporary line");
         fprintf(stderr,"Line: %s\n",tmpath.c_str());
         return(CF_FAIL);
      }
      for(unsigned int j=0; j<tmgr->filnames.size(); j++)
      {
         frompath = tmpath + std::string("/") + (*(tmgr->filnames[j]));
         rtn = repairUnixCommandString(&frompath);
         if(CF_OK != rtn)
         {
            reportError("Failure to repair tmpath string");
            return(CF_FAIL);
         }
         fcommand = command + frompath + std::string("  ") + topath;
         if(CF_OK == systemAndLog(fcommand.c_str()) )
         {
             //fine, keep going
         }
         else
         {
            reportError("Failure to copy an existing file");
            fprintf(stderr,"Failed Command: %s\n",fcommand.c_str());
            return(CF_FAIL);
         }
      }
      delete tmgr;
      tmgr = NULL;
   }

   return(CF_OK);
}

bool rcfDirectoryMgr::isObjectValid()
{
   if(1 == object_failed)return(false);
   else return(true);
}

int rcfDirectoryMgr::printContent(int level)
{
   func = (char*)"rcfDirectoryMgr::printContent";
   int rtn;

   rtn = examineLine(level);
   if(CF_OK != rtn)
   {
      reportError("Failure to examine line");
      return(CF_FAIL);
   }
   cout<<"FILES: \n";
   for(unsigned int i=0; i<filnames.size(); i++) 
   {
       fprintf(stderr,"%s \n",filnames[i]->c_str());
   }
   cout<<"DIRECTORIES: \n";
   for(unsigned int i=0; i<dirnames.size(); i++)
   {
       fprintf(stderr,"%s \n",dirnames[i]->c_str());
   }
   return CF_OK;
}
int rcfDirectoryMgr::printContent()
{
   return(printContent(nlevels));
}


void rcfDirectoryMgr::reportError(const char *string)
{
   if(NULL == string)
   {
      if(NULL == func)
      {
         fprintf(stderr,"ERROR: Anonymous Function called error report with empty data string  \n");
      }
      else
      {
         fprintf(stderr,"ERROR: %s  called error report with empty data string \n",func);
      }
   }

   if(NULL == func)
   {
      fprintf(stderr,"ERROR: Anonymous Function reports  %s \n",string);
   }
   else
   {
      fprintf(stderr,"ERROR: %s  %s \n",func,string);
   }
}

//this function repairs a string that has to be used
// in a unix command. It puts escape character to precede
// spaces. Needed when one operates on directories transfered
// from Windows
int rcfDirectoryMgr::repairUnixCommandString(std::string *usr_string)
{
   func = (char*)"rcfDirectoryMgr::repairUnixCommandString";
   if(NULL == usr_string)
   {
      reportError("NULL pointer passed in lieu of string");
      return(CF_FAIL);
   }

   int pos = 0;
   do
   {
      pos = usr_string->find(' ',pos+1);
      if(pos>0)
      {
          usr_string->insert(pos,1,'\\');
      }
      pos++;
   }while(pos > 0);

   return(CF_OK);
}

int rcfDirectoryMgr::cleanStringToken(char *usrbuf)
{
   func = (char*)"rcfDirectoryMgr::cleanStringToken";
   if(NULL == usrbuf)
   {
      //nothing to do
      reportError("NULL pointer passed as data");
      return(CF_FAIL);
   }
   int length = strlen(usrbuf);
   if(0 == length)
   {
      // nothing to do
      return(CF_OK);
   }
   char *tbuf = new char[length+1];
   if(NULL == tbuf)
   {
      reportError("Failure to allocate memory");
      fprintf(stderr,"size of request: %d\n",length+1);
      return(CF_FAIL);
   }
   memset(tbuf,0,length+1);
   int actlength = 0;
   for(int i=0; i<length; i++)
   {
      if(usrbuf[i] != ' ')
      {
         tbuf[actlength] = usrbuf[i];
         actlength++;
      }
   }
   strcpy(usrbuf,tbuf);
   delete[] tbuf;
   tbuf = NULL;
   return(CF_OK);
}

std::string rcfDirectoryMgr::appendTerminalSeparator(std::string user)
{
   std::string rtn = user;
   int length = rtn.length();
   if(length > 0)
   {
      const char *buf = rtn.c_str();
      if( buf[length-1] != separator[0] )
      {
        rtn.append(separator); 
      }
   }
   return(rtn);
}

int rcfDirectoryMgr::systemAndLog(const char *usrbuf)
{   
   if(NULL == usrbuf)
   {
      fprintf(stderr,"rcfDirectoryMgr: WARNING: system call with NULL command");
      if(NULL != logid)fprintf(logid,"Empty command detected in system call\n");
      return(CF_FAIL);
   }
   if(0 == strlen(usrbuf))
   {
      fprintf(stderr,"rcfDirectoryMgr: WARNING: system call with NULL command");
      if(NULL != logid)fprintf(logid,"Empty command detected in system call\n");
      return(CF_FAIL);
   } 

   if(0 == system(usrbuf))
   {
      if(NULL != logid)
      {
         fprintf(logid,"rcfDirectoryMgr:: issued successful system command: >>>>  %s\n",usrbuf);
         fflush(logid);
      }
      return(CF_OK);
   }
   else
   {
      if(NULL != logid)
      {
         fprintf(logid,"rcfDirectoryMgr:: issued FAILED system command: >>>> %s\n",usrbuf);
         fflush(logid);
      }
      return(CF_FAIL);
   }
   fprintf(stderr, "ERROR: rcfDirectoryMgr:: impossible logic ");
   return(CF_FAIL); // This should be impossible to get to, but put this just in case
}
int rcfDirectoryMgr::setSystemLog(FILE *usrlogid)
{
   if(NULL == usrlogid)
   {
      fprintf(stderr,"WARNING: attempt to set system log file id to NULL, ignored\n");
      return(CF_FAIL);
   }
   logid = usrlogid;
   return(CF_OK);
}
std::string rcfDirectoryMgr::getSeparator()
{
   return(std::string(separator));
}
