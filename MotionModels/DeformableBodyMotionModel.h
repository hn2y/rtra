#ifndef DEFORMABLEBODYMOTIONMODEL_H
#define DEFORMABLEBODYMOTIONMODEL_H
#include <MotionModels/GumModel.h>


class DeformableBodyMotionModel:public GumModel
{
public:
    DeformableBodyMotionModel();
    ~DeformableBodyMotionModel();
};

#endif // DEFORMABLEBODYMOTIONMODEL_H
