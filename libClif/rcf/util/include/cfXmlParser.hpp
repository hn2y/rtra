/*
 * cfXMLParser.hpp
 *
 *  Created on: Sep 16, 2009
 *      Author: bzhang
 */

#ifndef CFXMLPARSER_HPP_
#define CFXMLPARSER_HPP_

#include <map>
#include <string>

typedef std::map<std::string, std::string> xmlAttrMap;

/** \struct xmlAttr
 * \brief a structure representing a xml attribute
 *
 * \author Baoshe Zhang
 */
struct xmlAttr
{
	std::string name;
	std::string value;
};

/** \struct xmlNode
 * \brief a structure representing a xml node
 *
 * \author Baoshe Zhang
 */
struct xmlNode: public std::map<std::string, void*>
{
	/** \brief get the number of child nodes
	 *
	 */
	unsigned int getChildCount() const
	{
		if(this->find("xmlvalue") != this->end())
		{
			return 0;
		}

		if(this->find("xmlattr") != this->end())
		{
			return this->size() - 1;
		}

		return this->size();
	}

	/** \brief get a child node
	 * \param index the index number for the child node
	 */
	xmlNode* getChildNode(unsigned int index) const
	{
		unsigned int count = 0;
		xmlNode::const_iterator it;
		for(it=this->begin(); it!=this->end(); it++)
		{
			if(it->first != "xmlattr" && it->first != "xmlvalue")   // skip xmlattr and xmlvalue
			{
				if(count == index)
				{
					xmlNode* childNode = (xmlNode*)it->second;
					childNode->name = it->first.substr(it->first.find_first_of('_')+1);
					return childNode;
				}
				count++;
			}
		}
		return NULL;
	}

	/** \brief get the value of a child node
	 */
	std::string value() const
	{
		xmlNode::const_iterator it = this->find("xmlvalue");
		if(it != this->end())
		{
			return *(std::string*)it->second;
		}
		return "";
	}

	/** \brief get the attributes of the current node
	 */
	xmlAttrMap* getAttrMap() const
	{
		xmlNode::const_iterator it = this->find("xmlattr");

		if(it != this->end())
		{
			xmlNode::const_iterator it_attr = this->find("xmlattr");
			if(it_attr != this->end())
			{
				return (xmlAttrMap*)(it_attr->second);
			}
		}
		return NULL;
	}

	/** \brief get the value of an attribute of the current node
	 * \param keyAttr the name of the attribute
	 */
	std::string getAttr(std::string keyAttr) const
	{
		xmlAttrMap* attrMap = getAttrMap();

		if(attrMap != NULL)
		{
			if(attrMap->find(keyAttr) != attrMap->end())
			{
				return (*attrMap)[keyAttr];
			}
		}
		return "";
	}

	/** \brief get the value of an attribute of the current node
	 * \param keyAttr the name of the attribute
	 */
	xmlAttr getAttr(unsigned int index) const
	{
		xmlAttrMap* attrMap = getAttrMap();

		xmlAttr attr;
		attr.name = "";
		attr.value = "";
		if(attrMap != NULL && index>=0 && index<attrMap->size())
		{
			xmlAttrMap::const_iterator it_attr = attrMap->begin();
			for(unsigned int i=0; i<index; i++) it_attr++;

			attr.name = it_attr->first;
			attr.value = it_attr->second;
		}
		return attr;
	}

	/** \brief get the number of attributes of the current node
	 */
	int getAttrNum() const
	{
		xmlAttrMap* attrMap = getAttrMap();
		if(attrMap != NULL)
		{
			return attrMap->size();
		}
		return 0;
	}

	std::string name;
};

/** \class cfXmlParser
 * \brief parse a xml string or load a xml file to parse
 *
 * \author Baoshe Zhang
 */

class cfXmlParser {
public:

	/** \brief constructor
	 *
	 */
	cfXmlParser();

	/** \brief destructor
	 *
	 */
	virtual ~cfXmlParser();

	/** \brief parse a xml string
	 * \param xmlStr the xml string
	 *
	 */
	void parse(std::string xmlStr);

	/** \brief read a xml string from a xml file
	 * \param xmlFileName the path of a xml file
	 *
	 */
	void loadXmlFile(std::string xmlFileName);

	/** \brief show the structure of the xml string
	 *
	 */
	void showXmlTree() const;

	/** \brief get the root node of the xml tree
	 * \return a pointer to the root node
	 */
	xmlNode* getRootNode() const;



private:
	/** \brief parse a child node block
	 * \param xmlNode a xml string represent a child node
	 * \return a pointer to the child node
	 */
	xmlNode *parseNodeBlock(std::string xmlNodeStr);

	/** \brief show the structure of a child node block
	 * \param xmlChildNode a xml string represent a child node
	 * \param depth the depth of the child node
	 *
	 */
	void showChildNode(xmlNode *xmlChildNode, int depth) const;

	/** \brief delete a child node from the current xml tree
	 * \param xmlChildNode a pointer to the child node
	 *
	 */
	void deleteChildNode(xmlNode *xmlChildNode);

	/** \brief parse the attributes of a node
	 * \param nodeAttr the attribute string
	 * \return a pointer to the attribute.
	 */
	xmlAttrMap* parseNodeAttr(std::string nodeAttr);

	/** \brief delete the whole xml tree
	 *
	 */
	void deleteXmlTree();


	/**
	 * a private variable to store the xml tree
	 */
	xmlNode *m_xmlTree;
};

#endif /* CFXMLPARSER_HPP_ */
