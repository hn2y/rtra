#include <cuda.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <iostream>

extern "C" void allocateMemOnDevice(float* d_allocated, size_t numElement);

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        //fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}
//__global__ void allocateMemOnDeviceKernel(float *XqTransformed, float *YqTransformed, float *ZqTransformed, float *Xqn, float *Yqn, float *Zqn, int np, float *R_All, float *T_All, int numTrans)
//{
//    //float *X, float *Y, float *Z, float *V
//    int idx = blockIdx.x * blockDim.x + threadIdx.x;
//    if (idx < np)
//    {
//        float Xi = Xqn[idx];
//        float Yi = Yqn[idx];
//        float Zi = Zqn[idx];


//        for (int j = 0; j<numTrans; j++)
//        {
//            XqTransformed[idx + j*np] = R_All[j * 9] * Xi + R_All[j * 9 + 3] * Yi + R_All[j * 9 + 6] * Zi + T_All[j * 3];


//            YqTransformed[idx + j*np] = R_All[j * 9 + 1] * Xi + R_All[j * 9 + 4] * Yi + R_All[j * 9 + 7] * Zi + T_All[j * 3 + 1];


//            ZqTransformed[idx + j*np] = R_All[j * 9 + 2] * Xi + R_All[j * 9 + 5] * Yi + R_All[j * 9 + 8] * Zi + T_All[j * 3 + 2];

//        }
//    }
//}

void allocateMemOnDevice(float *d_allocated, size_t numElement)
{
    //  number of kenel to lanuch  --> np
 gpuErrchk(cudaMalloc((void**)&d_allocated, numElement * sizeof(float)))

}
