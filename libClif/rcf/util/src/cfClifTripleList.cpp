#include <iostream>
#include <string>
#include <vector>
#include "cfClifTripleList.hpp"
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include "rcfGlobalMethods.hpp"

using namespace std;

cfClifTripleList::cfClifTripleList()
{
    
}
    
cfClifTripleList::~cfClifTripleList()
{
    
}
    
bool cfClifTripleList::AddTriple(std::string line)
{    
    StringTriple triple;
    int spaceIndex1 = line.find_first_of(" ");
    triple.a =  line.substr(0, spaceIndex1);
    int spaceIndex2 = line.find(" ", spaceIndex1 + 1);
    triple.b = line.substr(spaceIndex1 + 1, spaceIndex2 - spaceIndex1 - 1);
    triple.c =  line.substr(spaceIndex2 + 1, line.length() - spaceIndex2 - 1);
    
    TrimString(triple.a);
    TrimString(triple.b);
    TrimString(triple.c);
    
    if( triple.a.size() == 0 || triple.b.size() == 0 || triple.c.size() == 0 )
    {       
        return false;
    }
    
    list.push_back(triple);           
    return true;
}

StringTriple cfClifTripleList::GetTriple(unsigned int index)
{        
    if( index >= list.size() )
    {
        StringTriple empty;
        return empty;
    }
    else
    {
        return list[index];
    }
}

void cfClifTripleList::GetTripleVector(std::vector<cfPoint3D<int> >& tripleVector)
{
    tripleVector.clear();
    for( unsigned int i = 0; i < list.size(); i ++ )
    {
        cfPoint3D<int> currentTriple;
        currentTriple.x = atoi((char*)list[i].a.c_str());
        currentTriple.y = atoi((char*)list[i].b.c_str());
        currentTriple.z = atoi((char*)list[i].c.c_str());
        tripleVector.push_back(currentTriple);                
    }
}

void cfClifTripleList::GetTripleVector(std::vector<cfPoint3D<float> >& tripleVector)
{
    tripleVector.clear();
    for( unsigned int i = 0; i < list.size(); i++ )
    {
        cfPoint3D<float> currentTriple;
        currentTriple.x = atof((char*)list[i].a.c_str());
        currentTriple.y = atof((char*)list[i].b.c_str());
        currentTriple.z = atof((char*)list[i].c.c_str());
        tripleVector.push_back(currentTriple);                
    }
}
