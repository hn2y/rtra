/** \file
    \author Various
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* dose_regions.cc
   Reads in Dose Region (from Pinnacle Format)
   Created: 6/9/98: JVS
   Modification History:
      Jan 26, 1999: JVS: Add get_dose_region calls
      Feb 1,  1999: JVS: Pinn Comm Stuff Added to read selected dose distribution from Pinnacle
      Feb 4,  1999: JVS: Add read_pinnacle_dose_region(dr) for reading dr from ACTIVE Pinnacle session
      April 6, 1999: JVS: Add smarts so can tell if active Pinnacle session or not...
      April 22, 1999: JVS: Bug fix: Now reads dose grid for specific trial (uses read_clif now)
      June 29, 1999: JVS: Add read_pin_dose_grid and write_mcv_dose_header
      Oct 27,1999:  JVS: Add #ifdef's around printf's
      Oct 4, 2000: JVS: Specify TrialName so not use "Current"
      May 2, 2001: JVS: Change tmpname to mktemp so will work cross-platform..(i.e. Pinnacle will write to same location)
      July 19, 2001: JVS: Input proper getting of beam number for dose files...(XDR Number)
      Aug 21, 2001: JVS: mktemp not work on linux...get rid of it...
      Sept 4, 2002: JVS: Add readBigEndianPinnacleDoseFile...this automatically swab's.
                         Add writeBigEndianPinnacleDoseFile
      Sept 5, 2002: JVS: get_dose_region now checks for active Pinnacle session before reading from file
      April 18, 2002: JVS: Change error message in read_dose_file so more generic 
                           since routine also used to read in other pinnacle binaries.
      Feb 18, 2004: JVS: writeBigEndianPinnacleDoseFile calls writeBigEndianBinaryFile
      June 4, 2008: JVS: Updates for sun CC compiler
      June 10, 2008: JVS: use readPinnacleBigEndianDoseFile quite a bit more
                          I hope this does not cause problems in other codes....
      Feb 25, 2010: JVS: If read_pinnacle_dose writes tmpFile before reading, read in in current machine byteorder
       Mar 1, 2010: JVS: Default of read_pinnacle_dose when reading from live session is to figure out the 
                         endianness from the file contents....
			

*********************************************************************************** */
#include <stdio.h>
#include <stdlib.h>      // for calloc
#include <string.h>      // for strcat and strcpy
#include "string_util.h" // for get_value
#include "utilities.h"   // for error routines
#include "typedefs.h"
#include "dose_region.h" // for definition of dose regions structure and function prototypes
#include "read_clif.h"
#include "readWriteEndianFile.h"
// #include "readPinnacle.h" // for prototypes XDR

#include "myPinnacle.h"
#include "myPinnComm.h"

/* ********************************************************************************* */
int readPinnacleXDRId(char *iString, int *Value);
/* ***************************************************************** */
/* ********************************************************************************* */
int read_pinnacle_dose(char *PlanFileName, char *TrialName, int DoseGridID, int ibeam, volume_region_type *dr, float **dose, long localPinnWindowID)
{
  if(localPinnWindowID > 0 )  // active pinnacle session
  {
     if(read_pinnacle_dose(TrialName, ibeam, dr, dose) != OK)
     {
        printf("\n ERROR: read_pinnacle_dose returned error"); return(FAIL);
     }
  }
  else // inactive, read from dose files
  {
     if(dr == NULL)
     {
        if(get_dose_region(PlanFileName,TrialName, &dr) != OK)
	{
           printf("\n ERROR: read_pinnacle_dose: Getting dose region"); return(FAIL);
        }
     }
     else
     {
        /* read in the dose grid */
        if(read_pinnacle_dose_region(PlanFileName, TrialName, dr) != OK)
	{
           printf("\n ERROR: read_pinnacle_dose: Reading dose region for trial %s", TrialName); return(FAIL);
        }
     }
     /* compute the size of the dose region */
     int DoseGridSize = dr->n.x*dr->n.y*dr->n.z;
     /* read in the dose file */
     char DoseFileName[MAX_STR_LEN];
     sprintf(DoseFileName,"%s.Trial.binary.%03d",PlanFileName, DoseGridID);
#ifdef DEBUG
     printf("\n Reading file %s", DoseFileName);
#endif
     if(readBigEndianPinnacleDoseFile(DoseFileName, DoseGridSize, dose) != OK)
     {
        printf("\n ERROR: read_pinnacle_dose: Reading Pinnacle Dose File");return(FAIL);
     }
  }
  return(OK);
}
/* *************************************************************************************** */
int read_pinnacle_dose(char *TrialName, int ibeam, volume_region_type *dr, float **dose)
{  /* reads in displayed pinnacle dose distribution for a beam for an ACTIVE pinnacle session */
   /* ******************** */
   /* read in the size of the dose grid from Pinnacle here */
   if( read_pinnacle_dose_region( TrialName, dr ) != OK)
   {
      printf("\n ERROR: read_pinnacle_dose: Reading Dose Grid From Pinnacle");return(FAIL);
   }
   int DoseGridSize = dr->n.x*dr->n.y*dr->n.z;

   int iStatus = 1;
   /* write Pinnacle Dose out to a temporary file */
   char tmp_file[MAX_STR_LEN];
   //tmpnam(tmp_file);
   {
     char *pwdpath; 
     pwdpath = getenv("PWD");
     if(pwdpath == NULL ){
         perror("pwd");return(FAIL);
     }
     sprintf(tmp_file,"%s/tempPinnacleDoseFile%d",pwdpath,ibeam);
     //      mktemp(tmp_file); // this not work for some reason...
   }
#ifdef DEBUG
   printf("\n Writing Pinnacle dose to file %s", tmp_file);
#endif
   char command[MAX_STR_LEN];
   sprintf(command,"TrialList.%s.BeamList.#%d.Save = %s\n",TrialName,ibeam,tmp_file);
   iStatus = iPinnComm_IssueRootCommand(command); 
   if(!iStatus){fprintf(stderr,"\nERROR: Executing Command %s",command); return(FAIL);} 
   /* Determine the ID of the dose file */
   clifObjectType *Beam = new(clifObjectType);
   FILE *iStrm = fopen(tmp_file,"r");
   if(iStrm == NULL){
     printf("\n ERROR: Opening File %s\n", tmp_file); return(FAIL);
   }
   if( readClifFile(iStrm, Beam,"Beam") != OK) {
     printf("\n ERROR: Reading clif from file %s\n", tmp_file); return(FAIL);
   }
   fclose(iStrm);
   char DoseVolume[MAX_STR_LEN];
   int doseGridID=0;
   if( readStringClifStructure(Beam, "DoseVolume", DoseVolume) != OK 
       || readPinnacleXDRId(DoseVolume, &doseGridID )  != OK ) {
     printf("\n ERROR: Determining DoseVolume ID"); return(FAIL);
   }      
   /* ********************************* */   
   char dosefile[MAX_STR_LEN];
   sprintf(dosefile,"%s.binary.%03d", tmp_file,doseGridID);
   printf ("\n Dose File is %s", dosefile);
#define UNKNOWN_BYTE_ORDER
 
#ifdef UNKNOWN_BYTE_ORDER
   if(readUnknownEndianBinaryDataFromFile(dosefile, DoseGridSize, dose) != OK){
     printf("\n ERROR: read_pinnacle_dose: Reading Pinnacle Dose File %s", dosefile);return(FAIL);
   }
#else 
   switch (check_byte_order()) 
     {
     case BIG_ENDIAN:
       printf("\n\t Local architecture is BIG ENDIAN ");
       break;
     case LITTLE_ENDIAN:
       printf("\n\t Local architecture is LITTLE ENDIAN ");
       break;
     default:
       printf("\n\t Local architecture is of indeterminite byte order"); return(FAIL);
     }  
#ifndef USE_MACHINE_BYTE_ORDER
   printf(", reading in BigEndian file written from Pinnacle ");
   if(readBigEndianBinaryDataFromFile(dosefile, DoseGridSize, dose) != OK){
     printf("\n ERROR: read_pinnacle_dose: Reading Pinnacle Dose File %s", dosefile);return(FAIL);
   }
#else
   switch (check_byte_order()) 
     {
     case BIG_ENDIAN:
       /* read in the file */
       // if(readBigEndianPinnacleDoseFile(dosefile, DoseGridSize, dose) != OK){
       printf(", reading BigEndian file ");
       if(readBigEndianBinaryDataFromFile(dosefile, DoseGridSize, dose) != OK){
	   printf("\n ERROR: read_pinnacle_dose: Reading Pinnacle Dose File %s", dosefile);return(FAIL);
       }
       break;
     case LITTLE_ENDIAN:
       printf("\n, reading littleEndian file ");
       if(readLittleEndianBinaryDataFromFile(dosefile, DoseGridSize, dose) != OK){
	   printf("\n ERROR: read_pinnacle_dose: Reading Pinnacle Dose File %s", dosefile);return(FAIL);
       }
       break;
     default:
       printf("\n ERROR: unknown byte order.  Will not read Pinnacle Dose File %s", dosefile);return(FAIL);
     }
#endif // USE_MACHINE_BYTE_ORDER
#endif // UNKNOWN_BYTE_ORDER

   /* delete the temporary dose files */
   if( remove(tmp_file) != 0){
      printf("\n ERROR: Removing temporary dose file %s", tmp_file); return(FAIL);
   }
   for(int i=0;i<3;i++){
      sprintf(dosefile, "%s.binary.%03d",tmp_file,i);
      if( remove(dosefile) != 0){
         printf("\n ERROR: Removing temporary dose file %s", dosefile); return(FAIL);
      }
   }
   if(OK != qaDoseValues(*dose,DoseGridSize) ) {
     printf("\n ERROR: read_pinnacle_dose: doseValues out of range.  Probable byte order problem\n"); return(FAIL);
   }

   return(OK);
}
/* ********************************************************************************* */
/* ********************************************************************************* */
int read_pinnacle_dose_region(char *fname_stem, char *TrialName, volume_region_type *dose_grid)
{
   char pbeam_fname[MAX_STR_LEN];
   strcpy(pbeam_fname,fname_stem);  // put first part on name
   strcat(pbeam_fname,".Trial");    // append .Trial to the name  
   // open dose region file
   FILE *fp = fopen(pbeam_fname,"r");
   if(fp==NULL)
   {
      printf("\n ERROR: opening dose region file %s", pbeam_fname);
      return(FAIL);
   }
   int beam_found = 0;
   // get to the desired trial
   do{
      if(clif_get_to_line(fp,"Trial ={")!=OK)
      {
         printf("\n ERROR: finding Trial ={ in file %s",pbeam_fname);
         return(FAIL);
      }
      char Current_TrialName[MAX_STR_LEN];
      // get and report Trial Name
      if(clif_string_read(fp,"Name =",Current_TrialName) != OK)  
         return(FAIL); 
      /* check if current trial matches */
      if(strcmp(Current_TrialName,TrialName) == 0) beam_found = 1;
      if(!beam_found)
      {  /* close the Trial cliff */
         if(clif_get_to_line(fp,"};")!=OK)
         {
            printf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
            return(FAIL);
         }
      }
   }while(!beam_found);
#ifdef DEBUG
  printf("\n Reading In Dose Grid for Trial %s", TrialName);
#endif
   if(read_pin_dose_grid(fp, dose_grid) != OK)
   {
      printf("\n ERROR: Reading In Pinnacle Dose Grid");
      return(FAIL);
   }
   // currently, read in only 3-D dose grid from Pinnacle
   // close context of Trial structure
   if(clif_get_to_line(fp,"};")!=OK)
   {
      printf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
      return(FAIL);
   }
   // close dose region file
   fclose(fp);
   return(OK);
} 
/* ********************************************************************************* */
#ifdef LINUX
int read_pinnacle_dose_region(char *TrialName, volume_region_type *dg)
{
  dg->n.x=dg->n.y=dg->n.z=0;
  printf("\n ERROR: read_pinnacle_dose_region cannot be executed from LINUX for %s",TrialName);
  return(FAIL);
}
#else
int read_pinnacle_dose_region(char *TrialName, volume_region_type *dg) // gets dose region from Active Pinnacle Session
{
   /* Reads dose region from ACTIVE Pinnacle Session */
   int iStatus;

   /* in Pinnacle, dose grid are same for all beams */
   char pcMessage[PINN_COMM_STRING_LEN];
   char qString[PINN_COMM_STRING_LEN];
   void *pvTrial;
   // Get memory address of Dose Grid in Current Trial
   sprintf(pcMessage, "TrialList.%s.DoseGrid.Address",TrialName);
   iStatus = iPinnComm_QueryObject(NULL, pcMessage, &pvTrial); 
   if (!iStatus) {
      fprintf(stderr, "Error query object %s\n", pcMessage);
      return(FAIL);
   }

   // Get the dose grid parameters
   int itmp;
   strcpy(qString,"Dimension.X");
   iStatus = iPinnComm_QueryInt(pvTrial, qString, &itmp);
   if (!iStatus) {
      fprintf(stderr, "Error query int DoseGrid Dimension.X for current Trial\n");
      return (FAIL);
   }
   dg->n.x = itmp;
   strcpy(qString,"Dimension.Y");
   iStatus = iPinnComm_QueryInt(pvTrial, qString, &itmp);
   if (!iStatus) {
      fprintf(stderr, "Error query int DoseGrid Dimension.Y for current Trial\n");
      return (FAIL);
   }
   dg->n.y = itmp;
   strcpy(qString,"Dimension.Z");
   iStatus = iPinnComm_QueryInt(pvTrial,qString, &itmp);
   if (!iStatus) {
      fprintf(stderr, "Error query int DoseGrid Dimension.Z for current Trial\n");
      return (FAIL);
   }
   dg->n.z = itmp;
   strcpy(qString,"VoxelSize.X");
   iStatus = iPinnComm_QueryFloat(pvTrial, qString, &(dg->voxel_size.x));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid VoxelSize.X for current Trial\n");
       return (FAIL);
   }
   strcpy(qString,"VoxelSize.Y");
   iStatus = iPinnComm_QueryFloat(pvTrial, qString, &(dg->voxel_size.y));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid VoxelSize.Y for current Trial\n");
       return (FAIL);
   }
   strcpy(qString,"VoxelSize.Z");
   iStatus = iPinnComm_QueryFloat(pvTrial, qString, &(dg->voxel_size.z));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid VoxelSize.Z for current Trial\n");
       return (FAIL);
   }
   strcpy(qString,"Origin.X");
   iStatus = iPinnComm_QueryFloat(pvTrial, qString, &(dg->origin.x));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid Origin.X for current Trial\n");
       return (FAIL);
   }
   strcpy(qString,"Origin.Y");
   iStatus = iPinnComm_QueryFloat(pvTrial,  qString, &(dg->origin.y));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid Origin.Y for current Trial\n");
       return (FAIL);
   }
   strcpy(qString,"Origin.Z");
   iStatus = iPinnComm_QueryFloat(pvTrial, qString, &(dg->origin.z));
   if (!iStatus) {
       fprintf(stderr, "Error query int DoseGrid Origin.Z for current Trial\n");
       return (FAIL);
   }

#ifdef DEBUG
  printf("\n Dose Grid: vs: %f %f %f",
     dg->voxel_size.x,dg->voxel_size.y,dg->voxel_size.z);
  printf("\n Dose Grid:  o: %f %f %f",
     dg->origin.x,dg->origin.y,dg->origin.z);
  printf("\n Dose Grid:  n: %d %d %d",
     dg->n.x,dg->n.y,dg->n.z);
#endif
   return(OK);
}
#endif
/* ************************************************************************* */
int getVoxelVolume(case_info_type *patient)
{
  volume_region_type *dose_region;
  if(get_dose_region( patient->PlanFileName, patient->TrialName, &dose_region) != OK)
  {
     printf("\n ERROR: Getting dose region"); return(FAIL);
  }
  /* get voxel size for determining nrcycl in dosxyz */
  patient->voxel_volume = dose_region->voxel_size.x * dose_region->voxel_size.y
                            * dose_region->voxel_size.z;
  if(patient->voxel_volume <= 0.0) {
      printf("\n ERROR: voxel_volume < 0 (x %f y %f z %f", dose_region->voxel_size.x, dose_region->voxel_size.y, dose_region->voxel_size.z);
      return(FAIL);
  }
  // printf("\n\tVoxelVolume = %f", patient->voxel_volume);
  if(dose_region != NULL) free(dose_region);
  return(OK);
}
/* ************************************************************************ */
int get_dose_region(char *dose_region_fname, volume_region_type **dose_region,
             case_info_type *patient )
{
  if(get_dose_region( dose_region_fname, patient->TrialName, dose_region) != OK)
  {
     printf("\n ERROR: Getting dose region"); return(FAIL);
  }

  /* get voxel size for determining nrcycl in dosxyz */
  /* Sept 5, 2002: JVS: This moved to getVoxelVolume (stand alone) */
  /* volume_region_type *ldose_region;
  ldose_region = *dose_region;
  patient->voxel_volume = ldose_region->voxel_size.x * ldose_region->voxel_size.y
                            * ldose_region->voxel_size.z;
  if(patient->voxel_volume <= 0.0) {
      printf("\n ERROR: voxel_volume < 0 (x %f y %f z %f", ldose_region->voxel_size.x, ldose_region->voxel_size.y, ldose_region->voxel_size.z);
      return(FAIL);
  }
  printf("\n VoxelVolume = %f", patient->voxel_volume); */
  return(OK);
}
int get_dose_region(char *dose_region_fname, char *TrialName, volume_region_type **dose_region)
{
  volume_region_type *ldose_region; /* local copy of dose region */
  ldose_region = (volume_region_type *)calloc(1,sizeof(volume_region_type));
  if(ldose_region == NULL)
  {
     printf("\n ERROR: allocating memory for dose_region");
     return(FAIL);
  }

  if( PinnWindowID > 0 ) // read from active Pinnacle session
  {
     if( (read_pinnacle_dose_region(TrialName, ldose_region)!=OK))
     {
        printf("\n ERROR: getting dose regions\n");return(FAIL);
     }
  }
  else 
  {
     if( (read_pinnacle_dose_region(dose_region_fname,TrialName, ldose_region)!=OK))
     {
        printf("\n ERROR: getting dose regions\n");return(FAIL);
     }
  }

#ifdef DEBUG
  printf("\n Dose Grid: vs: %f %f %f",
     ldose_region->voxel_size.x,ldose_region->voxel_size.y,ldose_region->voxel_size.z);
  printf("\n Dose Grid:  o: %f %f %f",
     ldose_region->origin.x,ldose_region->origin.y,ldose_region->origin.z);
  printf("\n Dose Grid:  n: %d %d %d",
     ldose_region->n.x,ldose_region->n.y,ldose_region->n.z);
#endif
  *dose_region = ldose_region; /* assign dose region */
  return(OK);
}
/* ************************************************************************************************* */
