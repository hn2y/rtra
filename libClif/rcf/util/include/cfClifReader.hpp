#ifndef CFCLIFREADER_HPP
#define CFCLIFREADER_HPP

#include "cfClifArray.hpp"
#include "cfClif.hpp"
#include <string>
#include <fstream>

class cfClifReader : public cfClif
{
    public:
        cfClifReader();
        cfClifReader( std::string path );
        ~cfClifReader();
        bool OpenClif( std::string path );
        void CloseClif();
        bool ReadClif(cfClif* clif);
        bool ReadClif();
        void ReadRoi( std::string path );
        std::ifstream reader;
        bool IsOpen() { return isFileOpen; }
    private:
        std::string ReadLine();
        bool isFileOpen;
};
#endif
