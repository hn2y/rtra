#include "DelineationModel.h"

DelineationModel::DelineationModel(std::ostream &stream):logstream(stream)
{
    domXmlDoc=0;
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="Delineation";
    verbose=10;
}
DelineationModel::DelineationModel():logstream(std::cout)
{
    domXmlDoc=0;
    associatedRoiID= "";
    guModelFile ="";
    guModelType ="Delineation";
}

DelineationModel::~DelineationModel()
{

}

DelineationModel DelineationModel::operator=(const DelineationModel &delineationModel)
{
    std::string class_member = "DelineationModel::operator=:";
    guModelType = delineationModel.guModelType;
    guModelFile = delineationModel.guModelFile;
    associatedRoiID=delineationModel.associatedRoiID;
    domXmlDoc   = delineationModel.domXmlDoc;

    return *this;
}

