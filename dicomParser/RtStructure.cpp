#include "RtStructure.h"

RtStructure::RtStructure():logstream(std::cout), rtStructFileName(0)
{
    roiContourSequenceItem=0;
}

RtStructure::RtStructure(std::ostream &stream):logstream(stream), rtStructFileName(0)
{
    roiContourSequenceItem=0;
}

RtStructure::~RtStructure()
{

}

bool RtStructure::ReadAllRoisProp()
{
    std::string class_member = "RtStructure::ReadAllRoisColor:";

    if (rtStructFileName==0)
    {
        logstream << class_member<< "RT Structure file (rtStructFileName) has not been set yet." << std::endl;
        return false;
    }
    //  statusRTStructureSetIOD has been set and can be used to read in the desired information

    if (allRoisNameNumberMap.size()<1)
    {
        if (!constructRoiNumIDMap())
        {
            return false;
        }

    }
    allRoisBasicProperties.clear();
    roiContourSequence = rtstructIOD.getROIContourSequence();
    int num_items = roiContourSequence.getNumberOfItems();

    if (num_items!=allRoisNameNumberMap.size())
    {
        logstream << class_member << "ERROR : Number of items in roiContourSequence does not match to allRoisNameNumberMap.size()" << std::endl;
        return false;
    }

    for (auto& roiPointer: allRoisNameNumberMap)
    {

          auto roiName =  roiPointer.first; // roiName
          auto roiNumber = roiPointer.second;// roiNumber

        // search in ROI contour Sequence and find the item that satisfies ReferencedROINumber=RoiNumber
        for (int i=0;i<num_items;i++)
        {
            roiContourSequence.getItem(i,roiContourSequenceItem);
            Sint32 referenceRoiNum;
            roiContourSequenceItem->getReferencedROINumber(referenceRoiNum);

//            auto searchMap = allRoisNameNumberMap.find(inputList[i]); // searchMap->second --> roinumber and

            if (referenceRoiNum==roiNumber)
            {
                ROIBasicProp roiProp;
                roiProp.RoiName = roiName;
                roiProp.roiNumberInRoiContourSequence= i;
                roiProp.roiNumberInStructureSetRoiSeq= roiNumber;
                roiProp.DesiredPointCloudResolution =1.5; // this value is default value for point cloud resolution

                std::vector<unsigned int> tempDisplayColorVec(3,0);
                getROIDisplayColorVec(tempDisplayColorVec); // get the color for the current roiContourSequenceItem
                roiProp.Color.r=static_cast<double>(tempDisplayColorVec[0])/255.0;
                roiProp.Color.g=static_cast<double>(tempDisplayColorVec[1])/255.0;
                roiProp.Color.b=static_cast<double>(tempDisplayColorVec[2])/255.0;
                roiProp.IsSelectedForComputation=false;
                allRoisBasicProperties[roiName]=roiProp;
                break;
            }
            else if(i==num_items-1)
            {
                logstream << class_member << "No matching roiContourSequence for referenceRoiNum = " << roiNumber << std::endl;

                return false;
            }
        }
    }
    return true;

}

bool RtStructure::constructRoiNumIDMap()
{
    std::string class_member = "RtStructure::constructRoiNumIDMap:";

    if (rtStructFileName==0)
    {
        logstream << "RT Structure file (rtStructFileName) has not been set yet." << std::endl;
        return false;
    }
    allRoisNameNumberMap.clear();
    OFFilename rtStructOFFileName(rtStructFileName);

    statusRTStructureSetIOD = fileFormatStructure.loadFile(rtStructOFFileName);
    if (statusRTStructureSetIOD.good())
    {
        statusRTStructureSetIOD = rtstructIOD.read(*fileFormatStructure.getDataset());
        if (statusRTStructureSetIOD.bad())
        {
            logstream << class_member << "ERROR in Reading RT Structure Set:" << statusRTStructureSetIOD.text() << std::endl;

            return false;
        }

        logstream << class_member << " Reading RT Structure Set:" << statusRTStructureSetIOD.text() << std::endl;
        OFString patientIDOFString;
        rtstructIOD.getPatientID(patientIDOFString);
        std::string tempString(patientIDOFString.c_str());
        patientID=tempString;


        logstream << class_member <<  "Patient ID in Structure Set = " << patientID << std::endl;

        ////DRTStructureSetROISequence ssRoiSeq;
        //// DRTStructureSet ss; // TODo: causes stackflow error

        structureSetRoiSeq = rtstructIOD.getStructureSetROISequence();
        Uint16 num_items = structureSetRoiSeq.getNumberOfItems();
        logstream << class_member <<  "This structure dicom file has " << num_items << " ROI sequence(s)" << std::endl;
        DRTStructureSetROISequence::Item *roiitem2;
        OFString roiName;
        //map < int, OFString> roiNumRoiNameMap; // roiNameRoiNumMap;

        for (int i = 0; i < num_items; i++)
        {
            structureSetRoiSeq.getItem(i, roiitem2);
            roiitem2->getROIName(roiName);
            Sint32 roiNum;
            roiitem2->getROINumber(roiNum);
            std::string tempString2(roiName.c_str());
            allRoisNameNumberMap[tempString2] = roiNum;
        }
    }
    else
    {
        logstream << " ERROR : Could not open the RT Structure file " << rtStructFileName << std::endl;
        return false;
    }

    return true;
}

bool RtStructure::verifyAndAddRoiNameListForComputation(std::vector<std::string>& inputList, std::vector<double> & inputPointCloudResList, std::vector<int> & inputIsTargetVolumeFlasList)
{
    std::string class_member = "RtStructure::verifyAndAddRoiNameListForComputation:";
    // if roi number name map is not constructued construct it
    if (allRoisNameNumberMap.size()<1)
    {
        if (!constructRoiNumIDMap())
        {
            return false;
        }

    }

    if (inputList.size()!=inputPointCloudResList.size())
    {
        logstream<<  class_member<< " ERROR : Both ROI name vector and point cloud resolution should have the same length. " << rtStructFileName << std::endl;

        return false;
    }
    pointCloudResolutionVector.clear();
    isTargetVolumeVector.clear();

    // go through each item in input list and verify that they are actually in the provided RT Structure file
    for (int i=0; i< inputList.size(); i++)
    {
        auto searchMap = allRoisNameNumberMap.find(inputList[i]);
        if(searchMap == allRoisNameNumberMap.end())
        {

            logstream << class_member << "ERROR/WARNING --> Requested ROI Name = " <<inputList[i] <<  " not found, it is going to be ignored. " <<  std::endl;
        }
        else
        {
            logstream << class_member <<" Found the requested ROI Name = " <<inputList[i]  << " in the MAP " << std::endl;

            roiNameListForComputation[inputList[i]] = searchMap->second;
            pointCloudResolutionVector[inputList[i]] = inputPointCloudResList[i];
            isTargetVolumeVector[inputList[i]] = inputIsTargetVolumeFlasList[i];

        }

    }

    if (roiNameListForComputation.size()<1)
    {
        logstream << class_member <<"ERROR --> No match has been found for requested ROI list in the RT structure " << std::endl;
        return false;
    }

    return true;
}


bool RtStructure::getROIDisplayColorVec(std::vector<unsigned int> & displayColorVec)
{
    std::string class_member = "RtStructure::getROIDisplayColor:";

    if (roiContourSequenceItem==0)
    {
        logstream << "roiContourSequenceItem has not been set yet." << std::endl;
        return false;
    }




    Sint32 displayColorR;
    Sint32 displayColorG;
    Sint32 displayColorB;
    OFCondition testCondition;
    testCondition=roiContourSequenceItem->getROIDisplayColor(displayColorR,0);
    if(testCondition.bad())
    {
        logstream << class_member <<"ERROR --> in reading ROI display color " << std::endl;
        return false;

    }

    testCondition=roiContourSequenceItem->getROIDisplayColor(displayColorG,1);
    if(testCondition.bad())
    {
        logstream << class_member <<"ERROR --> in reading ROI display color" << std::endl;
        return false;

    }
    testCondition=roiContourSequenceItem->getROIDisplayColor(displayColorB,2);
    if(testCondition.bad())
    {
        logstream << class_member <<"ERROR --> in reading ROI display color " << std::endl;
        return false;
    }
    displayColorVec[0] = displayColorR;
    displayColorVec[1] = displayColorG;
    displayColorVec[2] = displayColorB;


    return true;
}

bool RtStructure::createROIModel()
{
    std::string class_member = "RtStructure::createROIModel:";
    // if roi number name map is not constructued inform the user
    if (allRoisNameNumberMap.size()<1)
    {
        logstream << class_member <<"There is no ROI Name inside roiNameNumberMap. Call constructRoiNumIDMap first if the RT structure file has been set." << std::endl;
        return false;

    }
    // if roi number name map is not constructued inform the user
    if (roiNameListForComputation.size()<1)
    {
        logstream << class_member <<"No verified ROI Name inside roiNameListForComputation " << std::endl;
        return false;
    }

    roiVector.clear();

    int cnt=0;
    //   for (int iStrucName=0;iStrucName<roiNameListForComputation.size();i++)
    for (auto& m: roiNameListForComputation)
    {
        Roi tempRoi= Roi(logstream);
        tempRoi.rtStructFileName=rtStructFileName;
        tempRoi.RoiName = m.first;
        tempRoi.RoiNumber = m.second;
        tempRoi.resolutionPointClouds=pointCloudResolutionVector[tempRoi.RoiName];
        tempRoi.IsTargetVolume=isTargetVolumeVector[tempRoi.RoiName];
        tempRoi.rtstructIODPointer=&rtstructIOD;
        tempRoi.associateCTDicomFile=this->associateCTDicomFile;

        //       std::string tempString;
        //       if (!tempRoi.getPatientName(tempString))
        //       {

        //           return false;
        //       }

        roiContourSequence = rtstructIOD.getROIContourSequence();
        int num_items = roiContourSequence.getNumberOfItems();

        // search in ROI contour Sequence and find the item that satisfies ReferencedROINumber=RoiNumber
        for (int i=0;i<num_items;i++)
        {
            roiContourSequence.getItem(i,roiContourSequenceItem);
            Sint32 referenceRoiNum;
            roiContourSequenceItem->getReferencedROINumber(referenceRoiNum);

            if (referenceRoiNum==tempRoi.RoiNumber)
            {
                tempRoi.ItemNumberInRoiContourSequence=i;

                break;
            }
            else if(i==num_items-1)
            {
                logstream << class_member << "No matching roiContourSequenceItem for referenceRoiNum = " << tempRoi.RoiNumber << std::endl;

                return false;
            }
        }



        // filling contours information --> display color , ...
//        if (!getROIDisplayColorVec(tempRoi.DisplayColor))
//        {
//            return false; // no further error message needed
//        }
            tempRoi.DisplayColor[0]= round(255.0*allRoisBasicProperties[tempRoi.RoiName].Color.r);

            tempRoi.DisplayColor[1]= floor(255.0*allRoisBasicProperties[tempRoi.RoiName].Color.g);

            tempRoi.DisplayColor[2]= floor(255.0*allRoisBasicProperties[tempRoi.RoiName].Color.b);



        OFCondition contourDataLoadStatus;
        DRTContourSequence contourSeq = roiContourSequenceItem->getContourSequence();
        unsigned long int  contNumItem = contourSeq.getNumberOfItems();
        DRTContourSequence::Item *contourItem;
        OFVector<Float64> contourData;
        logstream << class_member << tempRoi.RoiName <<  " -> Contour  sequence size = " << contNumItem << std::endl;
        for (unsigned long int iContour = 0; iContour < contNumItem; iContour++)
        {
            contourSeq.getItem(iContour, contourItem);

            contourDataLoadStatus = contourItem->getContourData(contourData);
            logstream << class_member << " Contour number " << iContour << " :" << std::endl;
            if (contourDataLoadStatus.good())
            {
                // 	cout << "Contour data load status is good" << endl;
                logstream << class_member <<  tempRoi.RoiName << " ----> Contour data size for contour " << iContour << "  = " << contourData.size() << std::endl;

                auto mytmpZpointer = contourData.end();
                logstream << class_member << tempRoi.RoiName << " ----> Contour z value is " << contourData.back() << std::endl;
                tempRoi.zValuesVector.push_back(contourData.back());

                for (auto p = contourData.begin(); p != contourData.end(); p = p + 3)
                {
                    tempRoi.boundingBoxForROI.maxX = std::max(*p, tempRoi.boundingBoxForROI.maxX);
                    tempRoi.boundingBoxForROI.maxY = std::max(*(p + 1), tempRoi.boundingBoxForROI.maxY);
                    tempRoi.boundingBoxForROI.maxZ = std::max(*(p + 2), tempRoi.boundingBoxForROI.maxZ);
                    tempRoi.boundingBoxForROI.minX = std::min(*p, tempRoi.boundingBoxForROI.minX);
                    tempRoi.boundingBoxForROI.minY = std::min(*(p + 1), tempRoi.boundingBoxForROI.minY);
                    tempRoi.boundingBoxForROI.minZ = std::min(*(p + 2), tempRoi.boundingBoxForROI.minZ);
                    /*					std::cout <<" x[ " << cnt << " ]=  " <<  *p << ' ';
                                       std::cout << " y[ " << cnt << " ]=  " << *(p+1) << ' ';
                                       std::cout << " z[ " << cnt << " ]=  " << *(p+2) << ' ' << endl;*/
                }
            }
            else
            {
                logstream << class_member  << "          " << "ERORR --> Failed to read  contour data " << std::endl;
            }
        } // end for j

        // Construct the grid points based on the boundingbox coordinates
        tempRoi.generateGridPoints();

        if (!tempRoi.generatePointCloud())
        {
            return false;
        }

        if (!tempRoi.calculateCOM())
        {
            return false;
        }
        // auto meshgridPts = xyMeshGrid(gridXposROIs[cntROI], gridYposROIs[cntROI]);




        roiVector.push_back(tempRoi);
        cnt++;

    }


    return true;

}


bool RtStructure::WriteROIsPointCloudsToCSVFile()
{
    std::string class_member = "RtStructure::WritePointCloudsToCSVFile:";

    // if roi number name map is not constructued inform the user
    if (allRoisNameNumberMap.size()<1)
    {
        logstream << class_member <<"There is no ROI Name inside roiNameNumberMap. Call constructRoiNumIDMap first if the RT structure file has been set." << std::endl;
        return false;

    }
    // if roi number name map is not constructued inform the user
    if (roiNameListForComputation.size()<1)
    {
        logstream << class_member <<"No verified ROI Name inside roiNameListForComputation " << std::endl;
        return false;
    }

    // inform user if no roivector exist
    if (roiVector.size()<1)
    {
        logstream << class_member <<"size of roiVector is zero. " << std::endl;
        return false;
    }

    for (int iRoi = 0; iRoi < roiVector.size(); ++iRoi)
    {

        //    roiVector[iRoi].inPolygonPointsX

        std::string filePathXValues= "PointCloudFile_Xvals_" + roiVector[iRoi].RoiName +".txt";
        std::string filePathYValues= "PointCloudFile_Yvals_" + roiVector[iRoi].RoiName +".txt";
        std::string filePathZValues= "PointCloudFile_Zvals_" + roiVector[iRoi].RoiName +".txt";
        writeVectorToAsciiFile(filePathXValues,roiVector[iRoi].inPolygonPointsX);
        writeVectorToAsciiFile(filePathYValues,roiVector[iRoi].inPolygonPointsY);
        writeVectorToAsciiFile(filePathZValues,roiVector[iRoi].inPolygonPointsZ);
    }

    return true;

}



