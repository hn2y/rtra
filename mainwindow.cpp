#include "mainwindow.h"
#include "rtfilesparser.h"

#include "libClif/libpin/classBasedPinnacle.h"
#include "roisworker.h"
#include "someHelpers.h"
#include "GuiException.h"

// test dicom parser
#include "JJGDVCGuiDebugBuffer.h" // needs stream_buffer.hpp from boost library
#include "dicomParser/DicomFilesProp.h"
#include "dicomParser/RtDose.h"
#include "dicomParser/RtStructure2.h"
#include "dicomParser/Roi.h"

// Pinnacle file
#include "libClif/PinnacleFiles.h"

#include "ITKUtils.h"

// Uncertainty models
// Model Data
#include "MotionModels/ModelData.h"

// Robustness analyszer
#include "Algorithm/RobustnessAnalyzer.h"


// interpolation algorithms
#include "Algorithm/InterpolationAlgortihms.h"

#include <QFileDialog>
#include <QStyle>


// To make report
#include <QPrinter>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), debug_buffer(debug_capture), logstream(&debug_buffer)
{
    setInitialParamsForApplication();
    customPlot = NULL;
    //    testIntersectionOfTwoSetsWithReconIndices();
    //testInterpolationAlgorithm();
    //testRobustnessAnalyzerPinnacle();
//    testRobustnessAnalyzerPinnacleWithIntraFractionRigidMotion();
    //     testUncertaintyModel();
    // testPinnacleFilesParsing();
    // testTotalDoseRead();
    // testBinaryRead();
    // testrtFileParsing();
    // testDICOMParser();
}

MainWindow::~MainWindow()
{

    if (customPlot != NULL)
    {
        std::cout << "\n deleting custom plot........\n";
        delete (customPlot);
    }
}


void MainWindow::testInterpolationAlgorithm()
{

    //            std::vector<float> & vOutput,
    //            const std::vector<float> & gridX,
    //            const std::vector<float> & gridY,
    //            const std::vector<float> & gridZ,
    //            const std::vector<float> & vGrid,
    //            const std::vector<float> & xq,
    //            const std::vector<float> & yq,
    //            const std::vector<float> & zq)
    std::string homePath = std::string(std::getenv("HOME"));


    InterpolationAlgortihms interpAlgs = InterpolationAlgortihms();
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    std::string doseHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.header";
    std::string doseImageFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img";
    std::string doseImageHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img.header";

    rtDosePinnTotalDose.setDoseHeaderFilePath(doseHeaderFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageFilePath(doseImageFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(doseImageHeaderFilePathString.c_str());

    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }
    std::vector<float> xq;
    std::vector<float> yq;
    std::vector<float> zq;
    xq.push_back(23.7);
    yq.push_back(-25.5);
    zq.push_back(17.1);

    std::vector<float> output = std::vector<float>(1, 0);
    thrust::transform(rtDosePinnTotalDose.doseGridYvalues.begin(), rtDosePinnTotalDose.doseGridYvalues.end(),
                      rtDosePinnTotalDose.doseGridYvalues.begin(), thrust::negate<float>());

    //rtDosePinnTotalDose.doseArrayCoordinatesX()
    interpAlgs.interp3(output,
                       rtDosePinnTotalDose.doseGridYvalues,
                       rtDosePinnTotalDose.doseGridXvalues,
                       rtDosePinnTotalDose.doseGridZvalues,
                       rtDosePinnTotalDose.doseArray,
                       yq, xq, zq);

    std::cout << "Output[0] = " << output[0] << std::endl;
    //    std::vector<float> gridX =linspace((float)4.5,(float)42.3,127);
    //    std::vector<float> gridY =linspace((float)40.2,(float)12.00,95);
    //    std::vector<float> gridZ=linspace((float)5.4,(float)19.8,49);
    //    std::vector<float> vGrid;




}

void MainWindow::testRobustnessAnalyzerPinnacle()
{
    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setVebose(false);
    std::string homePath = std::string(std::getenv("HOME"));
    std::string patientFilePathString = homePath + "/gitRepos/NKI/patient_319/Patient";

    // 1) read a patinet filea and its imagesSets
    myPinnacleFile.setPatientFilePath(patientFilePathString.c_str());
    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }
    writeVectorToBinaryFileQT(
            "./results/ImageSetXGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeXGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetYGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeYGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetZGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeZGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetPixelData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeDataUint16);


    // 2) read ROIs for plan.roi
    std::string roiFilePathString = homePath + "/gitRepos/NKI/patient_319/Plan_2/plan.roi";

    myPinnacleFile.setRoiFilePath(roiFilePathString.c_str());
    myPinnacleFile.readRoisProperties();
    // Loading data for an specific ROI with a given name

    std::vector<int> roiIndicesToBeExcluded;

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {

        if (                           // myPinnacleFile.roisPropVector[iRoi].name.compare("PTV_00")!=0 &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("Bladder_00")!=0 &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("FemurL_00")!=0  &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("FemurR_00")!=0  &&
                myPinnacleFile.roisPropVector[iRoi].name.compare("GTV1_00") != 0 &&
                // myPinnacleFile.roisPropVector[iRoi].name.compare("Rectum_00")!=0  &&
                myPinnacleFile.roisPropVector[iRoi].name.compare("Sigmoid_00") != 0
                )
        {
            //            std::cout << "Roi will be excluded .. > " + myPinnacleFile.roisPropVector[iRoi].name <<std::endl;
            roiIndicesToBeExcluded.push_back(iRoi);
            continue;
        }

        if (OKFlag != myPinnacleFile.readRoiDataForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.createBitmapForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.calculateCenterOfMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesX_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesX);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesY_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesY);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesZ_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesZ);


        //        if (myPinnacleFile.roisPropVector[iRoi].name.compare("GTV1_00")==0 )
        //        {
        //            std::cout << "Roi[" << std::to_string(iRoi) << "]:" << myPinnacleFile.roisPropVector[iRoi].name << " will be modified!"  << std::endl;

        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX.clear();
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY.clear();
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ.clear();

        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX.push_back(23.7);
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY.push_back(25.5);
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ.push_back(17.1);

        //        }


        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesX_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesY_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesZ_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ);

    }
    myPinnacleFile.roisPropVector.erase(ToggleIndices(myPinnacleFile.roisPropVector, std::begin(roiIndicesToBeExcluded),
                                                      std::end(roiIndicesToBeExcluded)),
                                        myPinnacleFile.roisPropVector.end());


    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if (OKFlag != myPinnacleFile.createBitmapVoxelMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            std::cout << "ERROR:  in  running createBitmapVoxelMassForGivenRoiName for ROI: "
                      << myPinnacleFile.roisPropVector[iRoi].name << std::endl;

        }
        std::cout << "Roi[" << myPinnacleFile.roisPropVector[iRoi].name << "].mass = "
                  << myPinnacleFile.roisPropVector[iRoi].mass << std::endl;
    }


    // 2.5 decudeRoiTypeFromName

    if (OKFlag != myPinnacleFile.deduceRoiTypeFromName())
    {
        std::cout << "ERROR: This is not what we expected" << std::endl;
        return;
    }

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        std::cout << "is Roi[" << iRoi << "] = " << myPinnacleFile.roisPropVector[iRoi].name << " a target? "
                  << myPinnacleFile.roisPropVector[iRoi].IsTargetVolume << std::endl;
    }


    // 3) Setting up uncertainty model
    std::cout << "... Instantiating ModelData ... " << std::endl;
    ModelData *modelData = new ModelData(logstream,
                                         myPinnacleFile.roisPropVector); // // It automatically creates GUMMODELMap
    std::string ROIName = std::string("GTV1_00");
    std::string RoiToRotateAroundCOMString = std::string("GTV1_00");
    // 4) setting up rigidbody motion
    modelData->rigidBodyMotionModel = new RigidBodyMotionModel(std::cout);
    modelData->rigidBodyMotionModel->DoseConvFlag = false;
    modelData->rigidBodyMotionModel->rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM = RoiToRotateAroundCOMString;
    modelData->rigidBodyMotionExist = true;

    modelData->rigidBodyMotionModel->sysTranErrorSigma.x = .3;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.y = .3;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.z = .3;

    modelData->rigidBodyMotionModel->randTranErrorSigma.x = 0.3;
    modelData->rigidBodyMotionModel->randTranErrorSigma.y = 0.3;
    modelData->rigidBodyMotionModel->randTranErrorSigma.z = 0.3;

    modelData->rigidBodyMotionModel->sysRotErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.z = 0;

    modelData->rigidBodyMotionModel->randRotErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->randRotErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->randRotErrorSigma.z = 0;

    // 5) setting up Rotation and transformation of indiviual ROI
    RotationAndTranslationModel rotTransModel = RotationAndTranslationModel(std::cout);
    rotTransModel.sysRotErrorSigma.x = 0;
    rotTransModel.sysRotErrorSigma.y = 0;
    rotTransModel.sysRotErrorSigma.z = 0;
    rotTransModel.sysTranErrorSigma.x = 0;
    rotTransModel.sysTranErrorSigma.y = 0;
    rotTransModel.sysTranErrorSigma.z = 0;
    rotTransModel.rotCenter.RoiIDForRotationAroundCOM = ROIName;
    rotTransModel.rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    std::map<std::string, boost::tuple<RotationAndTranslationModel, DeformationModel, DelineationModel>>::iterator iter = modelData->gumMap.find(
            ROIName);
    if (iter != modelData->gumMap.end())
    {
        boost::get<0>(iter->second) = rotTransModel;
    }
    else
    {
        logstream << "ERROR/Warning rotTranModel for " << ROIName << " is not created, since it was not in the gumap"
                  << std::endl;

    }

    // 6) Reading a dose file
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    std::string doseHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.header";
    std::string doseImageFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img";
    std::string doseImageHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img.header";

    rtDosePinnTotalDose.setDoseHeaderFilePath(doseHeaderFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageFilePath(doseImageFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(doseImageHeaderFilePathString.c_str());


    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }

    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesX);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesY);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesZ);

    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridXvalues);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridYvalues);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridZvalues);

    writeVectorToBinaryFileQT(
            "./results/TotalDoseArray_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            rtDosePinnTotalDose.doseArray);


    // 7) assign the dose to the pinnacle patient
    myPinnacleFile.setRtDosePinn(&rtDosePinnTotalDose);



    // 8) Initialze RobustAnalyzer
    // Calculation engine is instantiated here !
    RobustnessAnalyzer *robustnessAnalyzer = new RobustnessAnalyzer(std::cout);
    robustnessAnalyzer->setPinnacleFlag(true);
    robustnessAnalyzer->setModelData(modelData);
    robustnessAnalyzer->setPinnacleFiles(&myPinnacleFile);

    robustnessAnalyzer->setNumFractions(39);

    robustnessAnalyzer->setNumDoseBins(350);
    robustnessAnalyzer->setNumTrials(1000);
    robustnessAnalyzer->setGPUUsage(true);
    if (!robustnessAnalyzer->generateTransformationsParamsPinnacle())
    {
        std::cout << " Error in generateTransformationsParams ." << std::endl;

        return;
    }
    //     std::cout << "Patient" << std::to_string(patientNum) << " transformations has been created in robustnessAnalyzer  for dose  " <<  rtDose.doseCalcImageSet <<  " plan and Scan_" + std::to_string(scanFolderNumber) + " at " + nowInString() << std::endl;
    //if gpu device is chosen for computation
    if (!robustnessAnalyzer->calculateAllDVHSonGpuForPinnaclePatient())
    {

        std::cout << " Error in Calculation PDVHs on GPU(s)." << std::endl;
        return;
    }
    //    if (!robustnessAnalyzer->calculateAllDVHsOnCpuForPinnaclePatient())
    //    {

    //        std::cout  <<  " Error in Calculation PDVHs on CPU(s)." << std::endl;
    //        return;
    //    }


    if (!robustnessAnalyzer->calculateDchs())
    {
        logstream << " Error in Calculation Dchs." << std::endl;
        return;
    }

    logstream << " all Dchs has been calculated in robustnessAnalyzer " << std::endl;
    logstream << "Initialize Confidence Level for PDVHs based on the ROI's' type " << std::endl;
    if (!robustnessAnalyzer->calculatePDVHsConfLevelInitialValues())
    {
        return; // no further comment is needed
    }

    logstream << "Writing DCHs to file... " << std::endl;
    if (!robustnessAnalyzer->writeAllDchsToFile())
    {
        logstream << "ERORR --> in writing DCHs to file" << std::endl;
        return;
    }

    logstream << "calculateDVHsMeanAndVarianceMedianPercentiles... " << std::endl;
    if (!robustnessAnalyzer->calculateDVHsMeanAndVarianceMedianPercentiles())
    {
        logstream << "ERORR --> in calculateDVHsMeanAndVarianceMedianPercentiles  " << std::endl;
        return;
    }

    logstream << "Writing Planed Dvhs to file... " << std::endl;
    if (!robustnessAnalyzer->WritePlannedDVHS())
    {
        logstream << "ERORR --> in writing DCHs to file" << std::endl;
        return;
    }
    writeVectorToBinaryFileQT(
            "./results/DoseBinCenters_Patient_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            myPinnacleFile.rtDosePinn->doseCalcImageSet + ".bin", robustnessAnalyzer->doseBinCenters);

    for (int iRoi = 0; iRoi < robustnessAnalyzer->pinnacleFiles->roisPropVector.size(); ++iRoi)
    {
        writeVectorToBinaryFileQT("./results/cDMH_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                                  myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                                  robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                                  robustnessAnalyzer->cDMHForTrial[iRoi]);
        writeVectorToBinaryFileQT("./results/diffDMH_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                                  myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                                  robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                                  robustnessAnalyzer->diffDMHForTrial[iRoi]);

    }


    delete (modelData);
    delete (robustnessAnalyzer);
    std::cout << "Done !" << std::endl;
}

void MainWindow::testRobustnessAnalyzerPinnacleWithIntraFractionRigidMotion()
{

    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setVebose(false);
    std::string homePath = std::string(std::getenv("HOME"));
    std::string patientFilePathString = homePath + "/gitRepos/NKI/patient_319/Patient";

    // 1) read a patinet filea and its imagesSets
    myPinnacleFile.setPatientFilePath(patientFilePathString.c_str());
    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }
    writeVectorToBinaryFileQT(
            "./results/ImageSetXGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeXGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetYGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeYGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetZGridData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeZGridDataVectorInCm);
    writeVectorToBinaryFileQT(
            "./results/ImageSetPixelData_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            myPinnacleFile.Patient.ImageSetList[0].volumeDataUint16);


    // 2) read ROIs for plan.roi
    std::string roiFilePathString = homePath + "/gitRepos/NKI/patient_319/Plan_2/plan.roi";

    myPinnacleFile.setRoiFilePath(roiFilePathString.c_str());
    myPinnacleFile.readRoisProperties();
    // Loading data for an specific ROI with a given name

    std::vector<int> roiIndicesToBeExcluded;

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {

        if (                           // myPinnacleFile.roisPropVector[iRoi].name.compare("PTV_00")!=0 &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("Bladder_00")!=0 &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("FemurL_00")!=0  &&
            // myPinnacleFile.roisPropVector[iRoi].name.compare("FemurR_00")!=0  &&
                myPinnacleFile.roisPropVector[iRoi].name.compare("GTV1_00") != 0 &&
                // myPinnacleFile.roisPropVector[iRoi].name.compare("Rectum_00")!=0  &&
                myPinnacleFile.roisPropVector[iRoi].name.compare("Sigmoid_00") != 0
                )
        {
            //            std::cout << "Roi will be excluded .. > " + myPinnacleFile.roisPropVector[iRoi].name <<std::endl;
            roiIndicesToBeExcluded.push_back(iRoi);
            continue;
        }

        if (OKFlag != myPinnacleFile.readRoiDataForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.createBitmapForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.calculateCenterOfMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesX_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesX);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesY_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesY);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointIndicesZ_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointIndicesZ);


        //        if (myPinnacleFile.roisPropVector[iRoi].name.compare("GTV1_00")==0 )
        //        {
        //            std::cout << "Roi[" << std::to_string(iRoi) << "]:" << myPinnacleFile.roisPropVector[iRoi].name << " will be modified!"  << std::endl;

        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX.clear();
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY.clear();
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ.clear();

        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX.push_back(23.7);
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY.push_back(25.5);
        //            //            myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ.push_back(17.1);

        //        }


        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesX_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesX);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesY_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesY);
        writeVectorToBinaryFileQT(
                "./results/binaryBitmapPointCoordinatesZ_" + myPinnacleFile.roisPropVector[iRoi].name + '_' +
                std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
                myPinnacleFile.roisPropVector[iRoi].binaryBitmapPointCoordinatesZ);

    }
    myPinnacleFile.roisPropVector.erase(ToggleIndices(myPinnacleFile.roisPropVector, std::begin(roiIndicesToBeExcluded),
                                                      std::end(roiIndicesToBeExcluded)),
                                        myPinnacleFile.roisPropVector.end());


    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if (OKFlag != myPinnacleFile.createBitmapVoxelMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            std::cout << "ERROR:  in  running createBitmapVoxelMassForGivenRoiName for ROI: "
                      << myPinnacleFile.roisPropVector[iRoi].name << std::endl;

        }
        std::cout << "Roi[" << myPinnacleFile.roisPropVector[iRoi].name << "].mass = "
                  << myPinnacleFile.roisPropVector[iRoi].mass << std::endl;
    }


    // 2.5 decudeRoiTypeFromName

    if (OKFlag != myPinnacleFile.deduceRoiTypeFromName())
    {
        std::cout << "ERROR: This is not what we expected" << std::endl;
        return;
    }

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        std::cout << "is Roi[" << iRoi << "] = " << myPinnacleFile.roisPropVector[iRoi].name << " a target? "
                  << myPinnacleFile.roisPropVector[iRoi].IsTargetVolume << std::endl;
    }


    // 3) Setting up uncertainty model
    std::cout << "... Instantiating ModelData ... " << std::endl;
    ModelData *modelData = new ModelData(logstream,
                                         myPinnacleFile.roisPropVector); // // It automatically creates GUMMODELMap
    std::string ROIName = std::string("GTV1_00");
    std::string RoiToRotateAroundCOMString = std::string("GTV1_00");
    // 4) setting up rigidbody motion
    modelData->rigidBodyMotionModel = new RigidBodyMotionModel(std::cout);
    modelData->rigidBodyMotionModel->DoseConvFlag = false;
    modelData->rigidBodyMotionModel->rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM = RoiToRotateAroundCOMString;
    modelData->rigidBodyMotionExist = true;

    // systematic error

    modelData->rigidBodyMotionModel->sysTranErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.z = 0;

    modelData->rigidBodyMotionModel->sysRotErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.z = 0;

    // random error
    modelData->rigidBodyMotionModel->randTranErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->randTranErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->randTranErrorSigma.z = 0;

    modelData->rigidBodyMotionModel->randRotErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->randRotErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->randRotErrorSigma.z = 0;



    // intra fraction error

    modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x = 0.2;
    modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y = 0.2;
    modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z = 0.2;

    modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x = 0;
    modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y = 0;
    modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z = 0;




    // 5) setting up Rotation and transformation of indiviual ROI
    RotationAndTranslationModel rotTransModel = RotationAndTranslationModel(std::cout);
    rotTransModel.sysRotErrorSigma.x = 0;
    rotTransModel.sysRotErrorSigma.y = 0;
    rotTransModel.sysRotErrorSigma.z = 0;
    rotTransModel.sysTranErrorSigma.x = 0;
    rotTransModel.sysTranErrorSigma.y = 0;
    rotTransModel.sysTranErrorSigma.z = 0;
    rotTransModel.rotCenter.RoiIDForRotationAroundCOM = ROIName;
    rotTransModel.rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    std::map<std::string, boost::tuple<RotationAndTranslationModel, DeformationModel, DelineationModel>>::iterator iter = modelData->gumMap.find(
            ROIName);
    if (iter != modelData->gumMap.end())
    {
        boost::get<0>(iter->second) = rotTransModel;
    }
    else
    {
        logstream << "ERROR/Warning rotTranModel for " << ROIName << " is not created, since it was not in the gumap"
                  << std::endl;

    }

    // 6) Reading a dose file
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    std::string doseHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.header";
    std::string doseImageFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img";
    std::string doseImageHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img.header";

    rtDosePinnTotalDose.setDoseHeaderFilePath(doseHeaderFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageFilePath(doseImageFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(doseImageHeaderFilePathString.c_str());


    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }

    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesX);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesY);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseArrayCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseArrayCoordinatesZ);

    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesX_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridXvalues);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesY_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridYvalues);
    writeVectorToBinaryFileQT(
            "./results/TotalDoseGridCoordinatesZ_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            ".bin", rtDosePinnTotalDose.doseGridZvalues);

    writeVectorToBinaryFileQT(
            "./results/TotalDoseArray_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" + ".bin",
            rtDosePinnTotalDose.doseArray);


    // 7) assign the dose to the pinnacle patient
    myPinnacleFile.setRtDosePinn(&rtDosePinnTotalDose);



    // 8) Initialze RobustAnalyzer
    // Calculation engine is instantiated here !
    RobustnessAnalyzer *robustnessAnalyzer = new RobustnessAnalyzer(std::cout);
    robustnessAnalyzer->setPinnacleFlag(true);
    robustnessAnalyzer->setModelData(modelData);
    robustnessAnalyzer->setPinnacleFiles(&myPinnacleFile);
    int numFraction = 2;
    int numSubFraction = 25;
    robustnessAnalyzer->setNumSubFraction(numSubFraction);
    robustnessAnalyzer->IsIntraFxRandomGaussian = false;


    robustnessAnalyzer->setNumFractions(numFraction * numSubFraction);
    robustnessAnalyzer->setNumDoseBins(350);
    robustnessAnalyzer->setNumTrials(10000);
    robustnessAnalyzer->setGPUUsage(true);
    if (!robustnessAnalyzer->generateTransformationsParamsWithIntraFractionPinnacle())
    {
        std::cout << " Error in generateTransformationsParams ." << std::endl;

        return;
    }


    for (int iRoi = 0; iRoi < robustnessAnalyzer->pinnacleFiles->roisPropVector.size(); ++iRoi)
    {

        writeVectorToBinaryFileQT(
                "./results/TranslationVector_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                robustnessAnalyzer->simulationModelParams[iRoi].TranslationVectors);
    }
    //     std::cout << "Patient" << std::to_string(patientNum) << " transformations has been created in robustnessAnalyzer  for dose  " <<  rtDose.doseCalcImageSet <<  " plan and Scan_" + std::to_string(scanFolderNumber) + " at " + nowInString() << std::endl;
    //if gpu device is chosen for computation
    if (!robustnessAnalyzer->calculateAllDVHSonGpuForPinnaclePatient())
    {

        std::cout << " Error in Calculation PDVHs on GPU(s)." << std::endl;
        return;
    }
    //    if (!robustnessAnalyzer->calculateAllDVHsOnCpuForPinnaclePatient())
    //    {

    //        std::cout  <<  " Error in Calculation PDVHs on CPU(s)." << std::endl;
    //        return;
    //    }


    if (!robustnessAnalyzer->calculateDchs())
    {
        logstream << " Error in Calculation Dchs." << std::endl;
        return;
    }

    logstream << " all Dchs has been calculated in robustnessAnalyzer " << std::endl;
    logstream << "Initialize Confidence Level for PDVHs based on the ROI's' type " << std::endl;
    if (!robustnessAnalyzer->calculatePDVHsConfLevelInitialValues())
    {
        return; // no further comment is needed
    }

    logstream << "Writing DCHs to file... " << std::endl;
    if (!robustnessAnalyzer->writeAllDchsToFile())
    {
        logstream << "ERORR --> in writing DCHs to file" << std::endl;
        return;
    }

    logstream << "calculateDVHsMeanAndVarianceMedianPercentiles... " << std::endl;
    if (!robustnessAnalyzer->calculateDVHsMeanAndVarianceMedianPercentiles())
    {
        logstream << "ERORR --> in calculateDVHsMeanAndVarianceMedianPercentiles  " << std::endl;
        return;
    }

    logstream << "Writing Planed Dvhs to file... " << std::endl;
    if (!robustnessAnalyzer->WritePlannedDVHS())
    {
        logstream << "ERORR --> in writing DCHs to file" << std::endl;
        return;
    }
    writeVectorToBinaryFileQT(
            "./results/DoseBinCenters_Patient_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
            myPinnacleFile.rtDosePinn->doseCalcImageSet + ".bin", robustnessAnalyzer->doseBinCenters);

    for (int iRoi = 0; iRoi < robustnessAnalyzer->pinnacleFiles->roisPropVector.size(); ++iRoi)
    {
        writeVectorToBinaryFileQT("./results/cDMH_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                                  myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                                  robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                                  robustnessAnalyzer->cDMHForTrial[iRoi]);
        writeVectorToBinaryFileQT("./results/diffDMH_" + std::to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                                  myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                                  robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                                  robustnessAnalyzer->diffDMHForTrial[iRoi]);

    }


    delete (modelData);
    delete (robustnessAnalyzer);
    std::cout << "Done !" << std::endl;
}

void MainWindow::testTotalDoseRead()
{
    PinnacleFiles myPinnacleFile;

    std::string homePath = std::string(std::getenv("HOME"));
    std::string patientFilePathString = homePath + "/gitRepos/NKI/patient_319/Patient";
    myPinnacleFile.setPatientFilePath(patientFilePathString.c_str());


    myPinnacleFile.setVebose(false);
    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }

    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    std::string doseHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.header";
    std::string doseImageFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img";
    std::string doseImageHeaderFilePathString =
            homePath + "/gitRepos/NKI/patient_319/Plan_2/dose/autoPlan7Beam00.totalDose.binary.img.header";

    rtDosePinnTotalDose.setDoseHeaderFilePath(doseHeaderFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageFilePath(doseImageFilePathString.c_str());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(doseImageHeaderFilePathString.c_str());

    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }


}


void MainWindow::testUncertaintyModel()
{
    PinnacleFiles myPinnacleFile;

    std::string homePath = std::string(std::getenv("HOME"));
    std::string patientFilePathString = homePath + "/gitRepos/NKI/patient_319/Patient";
    myPinnacleFile.setPatientFilePath(patientFilePathString.c_str());

    myPinnacleFile.setVebose(true);
    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }

    std::string roiFilePathString = homePath + "/gitRepos/NKI/patient_319/Plan_2/plan.roi";
    myPinnacleFile.setRoiFilePath(roiFilePathString.c_str());
    myPinnacleFile.readRoisProperties();




    // pointCloudVector
    std::cout << "... Instantiating ModelData ... " << std::endl;
    ModelData *modelData = new ModelData(std::cout,
                                         myPinnacleFile.roisPropVector); // // It automatically creates GUMMODELMap


    std::string ROIName = std::string("BLADDER5");
    std::string RoiToRotateAroundCOMString = std::string("External");
    // setting up rigidbody motion
    modelData->rigidBodyMotionModel = new RigidBodyMotionModel(std::cout);
    modelData->rigidBodyMotionModel->DoseConvFlag = false;
    modelData->rigidBodyMotionModel->rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM = RoiToRotateAroundCOMString;
    modelData->rigidBodyMotionExist = true;

    modelData->rigidBodyMotionModel->sysTranErrorSigma.x = 2.6;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.y = 2.4;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.z = 2.4;

    modelData->rigidBodyMotionModel->randTranErrorSigma.x = 2.0;
    modelData->rigidBodyMotionModel->randTranErrorSigma.y = 1.8;
    modelData->rigidBodyMotionModel->randTranErrorSigma.z = 1.7;

    modelData->rigidBodyMotionModel->sysRotErrorSigma.x = 1.1;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.y = 0.6;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.z = 0.5;

    modelData->rigidBodyMotionModel->randRotErrorSigma.x = 1.1;
    modelData->rigidBodyMotionModel->randRotErrorSigma.y = 0.6;
    modelData->rigidBodyMotionModel->randRotErrorSigma.z = 0.5;


    RotationAndTranslationModel rotTransModel = RotationAndTranslationModel(std::cout);
    rotTransModel.sysRotErrorSigma.x = 0;
    rotTransModel.sysRotErrorSigma.y = 0;
    rotTransModel.sysRotErrorSigma.z = 0;
    rotTransModel.sysTranErrorSigma.x = 0;
    rotTransModel.sysTranErrorSigma.y = 0;
    rotTransModel.sysTranErrorSigma.z = 0;
    rotTransModel.rotCenter.RoiIDForRotationAroundCOM = ROIName;
    rotTransModel.rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    std::map<std::string, boost::tuple<RotationAndTranslationModel, DeformationModel, DelineationModel>>::iterator iter = modelData->gumMap.find(
            ROIName);
    if (iter != modelData->gumMap.end())
    {
        boost::get<0>(iter->second) = rotTransModel;
    }
    else
    {
        std::cout << "ERROR/Warning rotTranModel for " << ROIName << " is not created, since it was not in the gumap"
                  << std::endl;

    }


}

void MainWindow::testPinnacleFilesParsing()
{


    PinnacleFiles myPinnacleFile;
    myPinnacleFile.usage();

    //   myPinnacleFile.setPatientFilePath("F:/UVA/gitRepos/NKI/patient_301/Patient");

    //     myPinnacleFile.setPatientFilePath("F:/UVA/gitRepos/NKI/patient_319/Patient");
    //    myPinnacleFile.setPatientFilePath("/home/hamid/gitRepos/NKI/patient_319/Patient");

    std::string homePath = std::string(std::getenv("HOME"));
    std::string patientFilePathString = homePath + "/gitRepos/NKI/patient_319/Patient";
    myPinnacleFile.setPatientFilePath(patientFilePathString.c_str());


    //  myPinnacleFile.setPatientFilePath("C:/xData/Dropbox/UVaResearch/Codes/nki_patient_319/Patient");

    myPinnacleFile.setVebose(true);
    // 'C:\xData\Dropbox\UVaResearch\Codes\nki_patient_301'

    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }


    // Example to read the plan.Trail file
    //    myPinnacleFile.setTrialFilePath("C:/xData/Dropbox/UVaResearch/Codes/nki_patient_301/Plan_2/plan.Trial");

    //    if (myPinnacleFile.ReadTrialList("autoPlan7Beam00")!=0)
    //    {
    //        std::cout << "ERROR:  in reading patient file" << std::endl;
    //    }


    std::cout << "We got here " << std::endl;

    //    // no need to define the extension
    //myPinnacleFile.setRoiFilePath("F:/UVA/gitRepos/NKI/patient_301/Plan_2/plan.roi");
    //    myPinnacleFile.setRoiFilePath("F:/UVA/gitRepos/NKI/patient_319/Plan_4/plan.roi");
    //    myPinnacleFile.setRoiFilePath("/home/hamid/gitRepos/NKI/patient_319/Plan_4/plan.roi");
    std::string roiFilePathString = homePath + "/gitRepos/NKI/patient_319/Plan_2/plan.roi";
    myPinnacleFile.setRoiFilePath(roiFilePathString.c_str());

    //        myPinnacleFile.setRoiFilePath("C:/xData/Dropbox/UVaResearch/Codes/nki_patient_301/Plan_2/plan.roi");

    myPinnacleFile.readRoisProperties();
    ////    myPinnacleFile.getRoiNames();



    std::cout << "\n roiPropVector has " << myPinnacleFile.roisPropVector.size() << "elements" << std::endl;
    //char stringToFind[] = "NKIVCU^PA^S00^I0002";
    char stringToFind[] = "NKIVCU^PS^S00^I0002";

    ////    std::vector<roi_prop>::iterator it =  std::find_if(roiPropVector.begin(), roiPropVector.end(),
    ////                 [] (const roi_prop& rp) { return 0 ==strcmp(rp.volume_name,"NKIVCU^PA^S00^I0002"); });


    std::vector<roi_prop> matchingProps;
    std::copy_if(myPinnacleFile.roisPropVector.begin(), myPinnacleFile.roisPropVector.end(),
                 std::back_inserter(matchingProps),
                 [&stringToFind](const roi_prop &rp)
                 { return 0 == strcmp(rp.volume_name.c_str(), stringToFind); });

    for (int iMatchingRoi = 0; iMatchingRoi < matchingProps.size(); ++iMatchingRoi)
    {
        std::cout << matchingProps[iMatchingRoi].name << std::endl;
    }


    // Loading data for an specific ROI with a given name
    std::string roiNameToLoadData = "BLADDER";
    if (OKFlag != myPinnacleFile.readRoiDataForGivenRoiName(roiNameToLoadData))
    {

        return;
    }

    //    if (OK != myPinnacleFile.printRoisDataForGivenRoiName(roiNameToLoadData))
    //    {
    //        return;
    //    }



    if (OKFlag != myPinnacleFile.createBitmapForGivenRoiName(roiNameToLoadData))
    {
        return;
    }

    auto itRoi = std::find_if(myPinnacleFile.roisPropVector.begin(), myPinnacleFile.roisPropVector.end(),
                              [&roiNameToLoadData](const roi_prop &rp)
                              { return 0 == rp.name.compare(roiNameToLoadData); });

    int roiIndexInRoisPropVector = std::distance(myPinnacleFile.roisPropVector.begin(), itRoi);

    if (roiIndexInRoisPropVector >= myPinnacleFile.roisPropVector.size())
    {
        std::cout << "No ROI in RoisPropVector with Name= " << roiNameToLoadData << std::endl;
        return;

    }
    else
    {
        std::cout << "ROI index = " << roiIndexInRoisPropVector << std::endl;

    }


    std::string RoiBitmapNameToSave = std::string("c:\\xData\\ROI_") +
                                      myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].name +
                                      "ImageSet" + myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].volume_name +
                                      ".binary.mask";

    if (OKFlag != myPinnacleFile.saveBitmapForGivenRoiName(RoiBitmapNameToSave, roiNameToLoadData))
    {
        return;
    }

    std::string partialVolVecNameToSave = std::string("c:\\xData\\ROI_") +
                                          myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].name +
                                          "ImageSet" +
                                          myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].volume_name +
                                          ".partialVol.bin";

    if (OKFlag != myPinnacleFile.savePartialVolumeOFBitmapForGivenRoiName(partialVolVecNameToSave, roiNameToLoadData))
    {
        return;
    }

    std::string indicesOfNonzerosToSave = std::string("c:\\xData\\ROI_") +
                                          myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].name +
                                          "ImageSet" +
                                          myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].volume_name +
                                          ".indices.bin";

    if (OKFlag !=
        myPinnacleFile.saveIndicesNonzerosElemInBinaryBitmapForGivenRoiName(indicesOfNonzerosToSave, roiNameToLoadData))
    {
        return;
    }

    std::string numCornersInsideRoiNameToSave = std::string("c:\\xData\\ROI_") +
                                                myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].name +
                                                "ImageSet" +
                                                myPinnacleFile.roisPropVector[roiIndexInRoisPropVector].volume_name +
                                                ".numCornerInside.bin";

    if (OKFlag != myPinnacleFile.savenumCornerInsidePolygonForNonzeroPointsOfBinaryBitmapForGivenRoiName(
            numCornersInsideRoiNameToSave, roiNameToLoadData))
    {
        return;
    }





    // Loading data for ROIs associated with an specific ImageSet
    //    if(OK != myPinnacleFile.readRoiDataForAllRoisOnGivenImageSetName(std::string("NKIVCU^PA^S00^I0002")))

    //    {
    //        return;
    //    }

    // To read RoiData
    //    if (myPinnacleFile.readRoisData() != OK)
    //    {

    //        return;
    //    }




}


void MainWindow::testBinaryRead()
{

    std::string testDataFolderPath = "F:/UVA/gitRepos/roiscreator/testData/";

    std::vector<double> vectorDoubleBigEndian;
    readBinayFileQt(testDataFolderPath + std::string("ADoubleBigEndian.bin"), BigEndian, 4, vectorDoubleBigEndian);

    std::vector<unsigned int> vectorUint32BigEndian;
    readBinayFileQt(testDataFolderPath + std::string("AUint32BigEndian.bin"), BigEndian, 4, vectorUint32BigEndian);

    std::vector<unsigned short int> vectorUint16BigEndian;
    readBinayFileQt(testDataFolderPath + std::string("AUint16BigEndian.bin"), BigEndian, 4, vectorUint16BigEndian);

    std::vector<double> vectorDoubleLittleEndian;
    readBinayFileQt(testDataFolderPath + std::string("ADoubleLittleEndian.bin"), LitteEndian, 4,
                    vectorDoubleLittleEndian);

    std::vector<unsigned int> vectorUint32LittleEndian;
    readBinayFileQt(testDataFolderPath + std::string("AUint32LittleEndian.bin"), LitteEndian, 4,
                    vectorUint32LittleEndian);

    std::vector<unsigned short int> vectorUint16LittleEndian;
    readBinayFileQt(testDataFolderPath + std::string("AUint16LittleEndian.bin"), LitteEndian, 4,
                    vectorUint16LittleEndian);


    for (int i = 0; i < 4; ++i)
    {
        qDebug() << "DoubleNumbersBigEndian[" << i << "] = " << vectorDoubleBigEndian[i];
        qDebug() << "Uint32NumbersBigEndian[" << i << "] = " << vectorUint32BigEndian[i];
        qDebug() << "Uint16NumbersBigEndian[" << i << "] = " << vectorUint16BigEndian[i];
        qDebug() << "DoubleNumbersLittleEndian[" << i << "] = " << vectorDoubleLittleEndian[i];
        qDebug() << "Uint32NumbersLittleEndian[" << i << "] = " << vectorUint32LittleEndian[i];
        qDebug() << "Uint16NumbersLittleEndian[" << i << "] = " << vectorUint16LittleEndian[i];
    }

}


void MainWindow::testDICOMParser()
{
    // Test reading dose dicom files
    if (Failed_RTRA == specifyPatientFolder())
    {
        logstream << "Either cancled or Failed" << std::endl;
        return;
    }



    // Check if this a dicom folder
    DicomFilesProp dicomPropFiles(logstream);
    dicomPropFiles.setDicomFolderPath(patientCurrentFolderQString.toStdString());
    if (!dicomPropFiles.parseFolder())
    {
        logstream << "Error in Parsing the folder one or more dicom files have problems" << std::endl;
        return;
    }

    //dicomPropFiles.PatientsVec.
    //DicomPatient.StudyVec
    //      DicomStudy.SeriesVec
    //        DicomSeries.ModalityVec
    //DicomModality.ModalityID
    //DicomModality.FileNameVec
    //DicomModality.FilePathVec
    //        DicomSeries.PatientID
    //        DicomSeries.StudyID
    //        DicomSeries.SeriesNumber

    // first Patient

    std::vector<string> ctFilePathList;
    std::vector<string> structureFilePath;
    for (int iSeries = 0; iSeries < dicomPropFiles.PatientsVec[0].StudyVec[0].SeriesVec.size(); ++iSeries)
    {
        if (dicomPropFiles.PatientsVec[0].StudyVec[0].SeriesVec[iSeries].ModalityID.compare("CT") == 0)
        {

            ctFilePathList = dicomPropFiles.PatientsVec[0].StudyVec[0].SeriesVec[iSeries].FilePathVec;

        }

        if (dicomPropFiles.PatientsVec[0].StudyVec[0].SeriesVec[iSeries].ModalityID.compare("RTSTRUCT") == 0)
        {
            structureFilePath = dicomPropFiles.PatientsVec[0].StudyVec[0].SeriesVec[iSeries].FilePathVec;
        }
    }

    std::vector<CTImage> ctImageSet = std::vector<CTImage>(ctFilePathList.size());

    for (int iCT = 0; iCT < ctFilePathList.size(); ++iCT)
    {
        ctImageSet[iCT].setCTDicomFilePath(ctFilePathList[iCT]);
        ctImageSet[iCT].parseCTDicomfile();

    }


    RtStructure2 rtStructure = RtStructure2();
    rtStructure.associateCTDicomFile = ctFilePathList[0].c_str();
    rtStructure.setCTImageSet(&ctImageSet);

    rtStructure.rtStructFileName = structureFilePath[0].c_str();

    if (!rtStructure.constructRoiNumIDMap())
    {
        logstream << "Error in ROIName Numuber map between StructureSetROISeq and RoiContourseq in dicom file"
                  << std::endl;
        return;

    }


    if (!rtStructure.ReadAllRoisProp())
    {
        logstream << " ERORR in reading ROIs basic properties." << std::endl;
        return;
    }

    std::vector<std::string> selectedRoiVec;

    std::vector<int> selectedRoiIsTargetVolumeFlagVec;

    selectedRoiVec.push_back(std::string("Bladder"));
    selectedRoiIsTargetVolumeFlagVec.push_back(0);
    selectedRoiVec.push_back(std::string("Femur_L"));
    selectedRoiIsTargetVolumeFlagVec.push_back(0);
    selectedRoiVec.push_back(std::string("CTV"));
    selectedRoiIsTargetVolumeFlagVec.push_back(1);


    std::vector<double> selectedRoiPointCloudResolutionVec = std::vector<double>(selectedRoiVec.size(), 1.5);

    if (!rtStructure.verifyAndAddRoiNameListForComputation(selectedRoiVec, selectedRoiPointCloudResolutionVec,
                                                           selectedRoiIsTargetVolumeFlagVec)) // verify selectedRoiVec

    {
        logstream
                << " Check input XML file for the request ROI name. Or check RT structre path and make sure it is pointing to the right RT structure DICOM file."
                << std::endl;
        return;
    }

    if (!rtStructure.createROIModel())
    {
        logstream << "Error in creating ROI model of rtStructure" << std::endl;
        return;

    }




    // Check if this is a Pinncle Folder






    //    RtDose rtDose =  RtDose();
    //    rtDose.rtDicomDoseFile = "";
    logstream << "------------Testing DICOM Structure---------- " << std::endl;


}

void MainWindow::testrtFileParsing()
{
    if (Failed_RTRA == specifyPatientFolder())
    {
        logstream << "Either cancled or Failed" << std::endl;
        return;
    }

    rtFilesParser *oRtFileParser = new rtFilesParser(this);
    if (!oRtFileParser->setPatientFolder(patientCurrentFolderQString))
    {
        logstream << "Failed setting patient folder" << std::endl;
    }

    if (!oRtFileParser->detectPatientFolderType())
    {
        return;
    }


}

void MainWindow::testIntersectionOfTwoSetsWithReconIndices()
{
    std::vector<std::string> v1;
    std::vector<std::string> v2;

    v1.push_back("ROI1");
    v1.push_back("CTV");
    v1.push_back("ROI3");

    v2.push_back("ROI5");
    v2.push_back("CTV");
    v2.push_back("ROI6");

    std::vector<std::string> v3 = instersection(v1, v2);

    std::cout << "v1: " << std::endl;
    for (const auto &i : v1) // access by const reference
        std::cout << i << ' ';

    std::cout << "\nv2: " << std::endl;
    for (const auto &i : v2) // access by const reference
        std::cout << i << ' ';


    std::cout << "\n intersection: " << std::endl;
    for (int iV = 0; iV < v3.size(); ++iV)
    {
        std::cout << v3[iV] << std::endl;
    }


    return;
}

void mydrawTextFun(QPainter &painter, qreal x, qreal y, Qt::Alignment flags,
                   const QString &text, QRectF *boundingRect = 0)
{
    const qreal size = 32767.0;
    QPointF corner(x, y - size);
    if (flags & Qt::AlignHCenter) corner.rx() -= size / 2.0;
    else if (flags & Qt::AlignRight) corner.rx() -= size;
    if (flags & Qt::AlignVCenter) corner.ry() += size / 2.0;
    else if (flags & Qt::AlignTop) corner.ry() += size;
    else flags |= Qt::AlignBottom;
    QRectF rect(corner, QSizeF(size, size));
    painter.drawText(rect, flags, text, boundingRect);
}

void mydrawText(QPainter &painter, const QPointF &point, Qt::Alignment flags,
                const QString &text, QRectF *boundingRect = 0)
{
    mydrawTextFun(painter, point.x(), point.y(), flags, text, boundingRect);
}

void MainWindow::plotColorMap()
{
    customPlot = new QCustomPlot;
    // configure axis rect:
    customPlot->setInteractions(
            QCP::iRangeDrag | QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox(true);
    customPlot->xAxis->setLabel("x");
    customPlot->yAxis->setLabel("y");

    // set up the QCPColorMap:
    QCPColorMap *colorMap = new QCPColorMap(customPlot->xAxis, customPlot->yAxis);
    int nx = 200;
    int ny = 200;
    colorMap->data()->setSize(nx, ny); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(-4, 4), QCPRange(-4,
                                                         4)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
    // now we assign some data, by accessing the QCPColorMapData instance of the color map:
    double x, y, z;
    for (int xIndex = 0; xIndex < nx; ++xIndex)
    {
        for (int yIndex = 0; yIndex < ny; ++yIndex)
        {
            colorMap->data()->cellToCoord(xIndex, yIndex, &x, &y);
            double r = 3 * qSqrt(x * x + y * y) + 1e-2;
            z = 2 * x * (qCos(r + 2) / r -
                         qSin(r + 2) / r); // the B field strength of dipole radiation (modulo physical constants)
            colorMap->data()->setCell(xIndex, yIndex, z);
        }
    }

    // add a color scale:
    QCPColorScale *colorScale = new QCPColorScale(customPlot);
    customPlot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(
            QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("Magnetic Field Strength");

    // set the color gradient of the color map to one of the presets:
    colorMap->setGradient(QCPColorGradient::gpPolar);
    // we could have also created a QCPColorGradient instance and added own colors to
    // the gradient, see the documentation of QCPColorGradient for what's possible.

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMap->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
    customPlot->axisRect()->setMarginGroup(QCP::msBottom | QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom | QCP::msTop, marginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    customPlot->rescaleAxes();
    customPlot->setGeometry(400, 250, 800, 800);
    customPlot->yAxis->setScaleRatio(customPlot->xAxis, 1.0);
    customPlot->xAxis->setRange(-4, 4);
    customPlot->yAxis->setRange(-4, 4);

    customPlot->replot();
    //    customPlot->show();
}

QPixmap customPlotToQPixMap(QCustomPlot *customPlot, QPdfWriter *qPdfWriter)
{
    int plotWidth = customPlot->viewport().width();
    int plotHeight = customPlot->viewport().height();
    double scale = (double) qPdfWriter->width() / (double) plotWidth;
    //    std::cout << "customPlot->viewport().width() "<< plotWidth << std::endl;
    //    std::cout << "CcustomPlot->viewport().height() "<< plotHeight << std::endl;
    //    std::cout << "Customplot to QPixMap scale : "<< scale << std::endl;
    QPixmap colorMapResultQPixelMap = customPlot->toPixmap(plotWidth, plotHeight, scale * .8);
    return colorMapResultQPixelMap;
}

void drawHeaderReport(QCPPainter *resultsQCPPainter, const QString &patientName, const QString &patientMRN,
                      const int &pageNumber, const QString &planName, const QString &trial)
{
    QPen penx00;
    penx00.setColor(QColor(0, 100, 190));
    penx00.setWidth(12);
    QPen penBlack;
    penBlack.setColor(QColor(0, 0, 0));
    penBlack.setWidth(12);
    QPen penRED;
    penRED.setColor(QColor(255, 0, 0));
    penRED.setWidth(8);
    resultsQCPPainter->setFont(QFont("Courier New", 16, QFont::DemiBold));
    resultsQCPPainter->setPen(penBlack);
    QPoint pt(4125, -150);
    mydrawText(*resultsQCPPainter, pt, Qt::AlignCenter, "Robustness Analysis");

    resultsQCPPainter->setFont(QFont("Courier New", 12));
    resultsQCPPainter->setPen(penx00);
    resultsQCPPainter->drawText(150, 200, "Date & Time :");
    resultsQCPPainter->drawText(150, 400, "Patient Name:");
    resultsQCPPainter->drawText(150, 600, "Patient MRN :");
    resultsQCPPainter->drawText(150, 800, "Plan :");
    resultsQCPPainter->drawText(150, 1000, "Trial :");

    resultsQCPPainter->setPen(penBlack);
    resultsQCPPainter->drawText(1750, 200, QString(nowInString2().c_str()));
    resultsQCPPainter->drawText(1750, 400, patientName);
    resultsQCPPainter->drawText(1750, 600, patientMRN);
    resultsQCPPainter->drawText(1750, 800, planName);
    resultsQCPPainter->drawText(1750, 1000, trial);

    resultsQCPPainter->setPen(penRED);
    resultsQCPPainter->drawText(6800, 200, QString("Page %1").arg(pageNumber));
    resultsQCPPainter->setPen(penBlack);
    resultsQCPPainter->drawText(5550, 400, QString("University Of Virginia"));
    resultsQCPPainter->drawText(5755, 600, QString("Radiation Oncology"));

    resultsQCPPainter->setPen(penx00);
    resultsQCPPainter->drawLine(QLine(150, 1050, 8250, 1050));
}

void MainWindow::testPrintReport(RobustnessAnalyzer *robustnessAnalyzer,
                                 PinnacleFiles *myPinnacleFiles,
                                 const QString &resultFolder,
                                 const QString &trialName,
                                 const QString &PlanName)
{


    QString reportPaths = resultFolder;
    QString patientMRN = myPinnacleFiles->Patient.MedicalRecordNumber.c_str();
    QString patientName = (myPinnacleFiles->Patient.FirstName + ' ' + myPinnacleFiles->Patient.LastName).c_str();
    QString planName = PlanName;
    QString TrialName = trialName;

    //    QStringList requestedROINamesQStringList;
    //    QString folderPathToSaveResults;
    //    QString totalDoseHeaderFilePath;
    //    QString totalDoseImageFilePath;
    //    QString totalDoseImageHeaderFilePath;
    float sysTranSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysTranErrorSigma.x;
    float sysTranSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysTranErrorSigma.y;
    float sysTranSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysTranErrorSigma.z;
    float randomTranSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->randTranErrorSigma.x;
    float randomTranSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->randTranErrorSigma.y;;
    float randomTranSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->randTranErrorSigma.z;;
    float sysRotSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysRotErrorSigma.x;
    float sysRotSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysRotErrorSigma.y;
    float sysRotSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->sysRotErrorSigma.z;
    float randomRotSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->randRotErrorSigma.x;
    float randomRotSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->randRotErrorSigma.y;
    float randomRotSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->randRotErrorSigma.z;
    float intraFractionTranSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x;
    float intraFractionTranSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y;
    float intraFractionTranSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z;
    float intraFractionRotSigmaX = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x;
    float intraFractionRotSigmaY = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y;
    float intraFractionRotSigmaZ = robustnessAnalyzer->modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z;

    int numFrac = robustnessAnalyzer->getNumFractions();
    int numSimulation = robustnessAnalyzer->getNumTrials();


    QVector<QRgb> colorTable;
    for (int i = 0; i < 256; i++)
    {
        colorTable.push_back(qRgb(i, i, i));
    }

    QString directoryName = QString("%1/%2").arg(reportPaths).arg("Reports");
    if (!QDir(directoryName).exists())
    {
        QDir().mkdir(directoryName);
    }

    QString sOutputFileName = QString("%1/%2_%3.pdf").arg(directoryName).arg(patientMRN).arg(patientName);

    QPdfWriter qPdfWriter(sOutputFileName);
    qPdfWriter.setPageSize(QPagedPaintDevice::A4);
    qPdfWriter.setPageOrientation(QPageLayout::Portrait);
    qPdfWriter.setPageMargins(QMargins(15, 15, 15, 15), QPageLayout::Millimeter);


    QFileInfo reportQFileInfo(sOutputFileName);
    if (reportQFileInfo.exists() && reportQFileInfo.isFile()) //file was printed automatically
    {
        qDebug() << ("Attention, report replaced existing file.", QColor("red"));
    }

    QCPPainter resultsQCPPainter(&qPdfWriter);
    resultsQCPPainter.setRenderHint(QPainter::Antialiasing, true);
    QPen penx00;
    penx00.setColor(QColor(0, 100, 190));
    penx00.setWidth(12);
    QPen penBlack;
    penBlack.setColor(QColor(0, 0, 0));
    penBlack.setWidth(12);
    QPen penGRN;
    penGRN.setColor(QColor(0, 153, 0));
    penGRN.setWidth(12);
    QPen penYEL;
    penYEL.setColor(QColor(204, 204, 0));
    penYEL.setWidth(12);
    QPen penRED;
    penRED.setColor(QColor(255, 0, 0));
    penRED.setWidth(12);
    QPen penMissedRad;
    penMissedRad.setColor(QColor(140, 10, 70));
    penMissedRad.setWidth(12);
    QPen penExcessRad;
    penExcessRad.setColor(QColor(10, 70, 140));
    penExcessRad.setWidth(12);
    resultsQCPPainter.setPen(penx00);
    // page limits are --> each line extends (-700,9250)  first and third elemet in QLine
    //                    and page height extend from -700 to 13400 --> each mm --> 47.47
    // resultsQPainter.drawLine(QLine(-700,-700,9250,13400));


    // 0-  Title  and  1- draw Header
    int pageNumber = 1;
    drawHeaderReport(&resultsQCPPainter, patientName, patientMRN, pageNumber, planName, TrialName);


    int lineStep = 200;
    int currentLine = 1200;

    // 2 - simulation parameter summary
    resultsQCPPainter.setFont(QFont("Courier New", 12));
    resultsQCPPainter.drawText(150, currentLine, QString("Number of Fractions:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(2550, currentLine, QString("%1").arg(numFrac));


    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("Number of Virtual Treatment Simulations:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(4950, currentLine, QString("%1").arg(numSimulation));


    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of Systematic Translation Error (cm) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(6230, currentLine,
                               QString("[%1,%2,%3]").arg(sysTranSigmaX).arg(sysTranSigmaY).arg(sysTranSigmaZ));

    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of Random Translation Error (cm) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(5750, currentLine,
                               QString("[%1,%2,%3]").arg(randomTranSigmaX).arg(randomTranSigmaY).arg(randomTranSigmaZ));

    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of intraFx Translation Error (cm) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(6000, currentLine,
                               QString("[%1,%2,%3]").arg(intraFractionTranSigmaX).arg(intraFractionTranSigmaY).arg(
                                       intraFractionTranSigmaZ));


    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of Systematic Rotation Error (Deg) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(6100, currentLine,
                               QString("[%1,%2,%3]").arg(sysRotSigmaX).arg(sysRotSigmaY).arg(sysRotSigmaZ));


    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of Random Rotation Error (Deg) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(5700, currentLine,
                               QString("[%1,%2,%3]").arg(randomRotSigmaX).arg(randomRotSigmaY).arg(randomRotSigmaZ));


    currentLine = currentLine + lineStep;
    resultsQCPPainter.setPen(penx00);
    resultsQCPPainter.drawText(150, currentLine, QString("STD of intraFx Rotation Error (Deg) [LR,PA,SI]:"));
    resultsQCPPainter.setPen(penBlack);
    resultsQCPPainter.drawText(5900, currentLine,
                               QString("[%1,%2,%3]").arg(intraFractionRotSigmaX).arg(intraFractionRotSigmaY).arg(
                                       intraFractionRotSigmaZ));


    resultsQCPPainter.setMode(QCPPainter::pmVectorized);
    resultsQCPPainter.setMode(QCPPainter::pmNoCaching);
    //      resultsQCPPainter.setMode(QCPPainter::pmNonCosmetic); // comment this out if you want cosmetic thin lines (always 1 pixel thick independent of pdf zoom level)

    // 3) plot PDVHs
    robustnessAnalyzer->plotPDVHSNew();
    QPixmap colorMapResultQPixelMap = customPlotToQPixMap(robustnessAnalyzer->customPlotPDvhs, &qPdfWriter);
    currentLine = currentLine + lineStep;
    resultsQCPPainter.drawPixmap(800, currentLine, colorMapResultQPixelMap.width(), colorMapResultQPixelMap.height(),
                                 colorMapResultQPixelMap);
    delete (robustnessAnalyzer->customPlotPDvhs);



    // 4) plot confidence intervals
    currentLine = currentLine + lineStep + colorMapResultQPixelMap.height();
    robustnessAnalyzer->plotDVHsConfInterval();
    colorMapResultQPixelMap = customPlotToQPixMap(robustnessAnalyzer->customPlotDvhsConfIntervals, &qPdfWriter);
    resultsQCPPainter.drawPixmap(800, currentLine, colorMapResultQPixelMap.width(), colorMapResultQPixelMap.height(),
                                 colorMapResultQPixelMap);
    robustnessAnalyzer->customPlotDvhsConfIntervals->close();
    delete (robustnessAnalyzer->customPlotDvhsConfIntervals);


    //Now plot DCHs for each ROIs
    for (int iRoi = 0; iRoi < myPinnacleFiles->roisPropVector.size(); ++iRoi)
    {
        qPdfWriter.newPage();
        pageNumber = pageNumber + 1;
        drawHeaderReport(&resultsQCPPainter, patientName, patientMRN, pageNumber, planName, TrialName);
        currentLine = 1200;
        currentLine = currentLine + lineStep;
        robustnessAnalyzer->plotDCH(myPinnacleFiles->roisPropVector[iRoi].name);
        colorMapResultQPixelMap = customPlotToQPixMap(robustnessAnalyzer->customPlotDch, &qPdfWriter);
        resultsQCPPainter.drawPixmap(800, currentLine, colorMapResultQPixelMap.width(),
                                     colorMapResultQPixelMap.height(), colorMapResultQPixelMap);
        delete (robustnessAnalyzer->customPlotDch);
    }



    //      resultsQCPPainter.setPen(penx00);
    //      resultsQCPPainter.drawText(150,currentLine,QString("STD of Random Rotation Error (Deg) [LR,PA,SI]:"));





    resultsQCPPainter.end();
}


void MainWindow::robustnessAnalyzerPinnacle(QString patientPath,
                                            QString planDotRoiPath,
                                            QString planDotPinnaclePath,
                                            const std::vector<std::string> &desiredRoiList,
                                            QString totalDoseHeaderFilePath,
                                            QString totalDoseImageFilePath,
                                            QString totalDoseImageHeaderFilePath,
                                            QString resultFolder,
                                            float sysTranSigmaX,
                                            float sysTranSigmaY,
                                            float sysTranSigmaZ,
                                            float randomTranSigmaX,
                                            float randomTranSigmaY,
                                            float randomTranSigmaZ,
                                            float sysRotSigmaX,
                                            float sysRotSigmaY,
                                            float sysRotSigmaZ,
                                            float randomRotSigmaX,
                                            float randomRotSigmaY,
                                            float randomRotSigmaZ,
                                            float intraFxTranSigmaX,
                                            float intraFxTranSigmaY,
                                            float intraFxTranSigmaZ,
                                            float intraFxRotSigmaX,
                                            float intraFxRotSigmaY,
                                            float intraFxRotSigmaZ,
                                            int numFrac,
                                            int numSimulation,
                                            int numSubFrac,
                                            QString trialName,
                                            QString planName,
                                            bool debug)
{
    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setVebose(debug); // if debug is on verbosity will be on as well

    // 0) read plan.Pinnacle file and extract StartWithDicom flag
    QFile planDotPinnacleQFile(planDotPinnaclePath);
    bool flagStartWithDICOMFound = false;
    bool IsStartWithDICOM = false;
    if (planDotPinnacleQFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&planDotPinnacleQFile);

        while (!in.atEnd())
        {
            QString line = in.readLine();
            QRegularExpression re("^StartWithDICOM = (\\d);$");
            QRegularExpressionMatch match = re.match(line);
            if (match.hasMatch())
            {
                QString matched = match.captured(1);
                IsStartWithDICOM = static_cast<bool>(matched.toInt());
                flagStartWithDICOMFound = true;
            }
        }
        planDotPinnacleQFile.close();
    }
    else
    {
        std::cout << "ERROR:  in reading plan.Pinnacle file" << std::endl;
        return;
    }
    if (!flagStartWithDICOMFound)
    {
        std::cout << "ERROR:  StartWithDICOM not found in plan.Pinnacle file" << std::endl;
        return;
    }


    myPinnacleFile.setIsStartWithDicom(IsStartWithDICOM);


    // 1) read a patinet file and its imagesSets
    myPinnacleFile.setPatientFilePath(patientPath.toStdString());


    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
        return;
    }
    if (debug)
    {
        for (int iImageSet = 0; iImageSet < myPinnacleFile.Patient.ImageSetList.size(); ++iImageSet)
        {
            writeVectorToNrrdFile(resultFolder.toStdString() + "/ImageSetPixelData_" +
                                  std::to_string(myPinnacleFile.Patient.PatientID) +
                                  "ImageSet" + myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".nrrd",
                                  myPinnacleFile.Patient.ImageSetList[iImageSet],
                                  myPinnacleFile.Patient.ImageSetList[iImageSet].volumeDataUint16);
        }

    }

    // 2) read ROIs for plan.roi
    myPinnacleFile.roisPropVector.clear();
    myPinnacleFile.setRoiFilePath(planDotRoiPath.toStdString());
    if (OKFlag != myPinnacleFile.readRoisProperties())
    {
        std::cout << "ERROR: in reading Roi properties" << std::endl;
        return;
    }

    // Loading data for an specific ROI with a given name
    vector<int> allroiIndices = generateIntRange(0, 1, myPinnacleFile.roisPropVector.size() - 1);


    vector<int> desiredRoiIndicesInRoisPropVector;
    for (int iRoi = 0; iRoi < desiredRoiList.size(); ++iRoi)
    {
        string roiNameToLoadData = desiredRoiList[iRoi];
        auto itRoi = find_if(myPinnacleFile.roisPropVector.begin(), myPinnacleFile.roisPropVector.end(),
                             [&roiNameToLoadData](const roi_prop &rp)
                             { return 0 == rp.name.compare(roiNameToLoadData); });
        int roiIndexInRoisPropVector = distance(myPinnacleFile.roisPropVector.begin(), itRoi);
        desiredRoiIndicesInRoisPropVector.push_back(roiIndexInRoisPropVector);
        if (roiIndexInRoisPropVector >= myPinnacleFile.roisPropVector.size())
        {
            cout << "No ROI in plan.roi file with name= " << roiNameToLoadData << ",  ignoring this ROI."
                 << std::endl;
            continue;
        }
    }

    sort(desiredRoiIndicesInRoisPropVector.begin(), desiredRoiIndicesInRoisPropVector.end());
    vector<int> roiIndicesToBeExcluded;
    // delete desired desiredRoiIndicesInRoisPropVector elements from allroiIndices and put the result in roiIndicesToBeExcluded
    set_difference(allroiIndices.begin(), allroiIndices.end(), desiredRoiIndicesInRoisPropVector.begin(),
                   desiredRoiIndicesInRoisPropVector.end(),
                   inserter(roiIndicesToBeExcluded, roiIndicesToBeExcluded.begin()));

    //  now erase  roiIndicesToBeExcluded from myPinnacleFile.roisPropVector
    myPinnacleFile.roisPropVector.erase(ToggleIndices(myPinnacleFile.roisPropVector, std::begin(roiIndicesToBeExcluded),
                                                      end(roiIndicesToBeExcluded)),
                                        myPinnacleFile.roisPropVector.end());


    roiIndicesToBeExcluded.clear();
    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {

        if (OKFlag != myPinnacleFile.readRoiDataForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }
        // if roi data is empty put it in the exclude list and continue
        if (myPinnacleFile.roisPropVector[iRoi].n_curves == 0)
        {

            roiIndicesToBeExcluded.push_back(iRoi);
            cout << "ROI " << myPinnacleFile.roisPropVector[iRoi].name << "is going to be excluded!" << std::endl;
            continue;
        }
    }
    // delete the ROIs that needs to be excluded
    myPinnacleFile.roisPropVector.erase(ToggleIndices(myPinnacleFile.roisPropVector, std::begin(roiIndicesToBeExcluded),
                                                      end(roiIndicesToBeExcluded)),
                                        myPinnacleFile.roisPropVector.end());

    // 6) Reading a dose file
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    rtDosePinnTotalDose.setDoseHeaderFilePath(totalDoseHeaderFilePath.toStdString());
    rtDosePinnTotalDose.setDoseImageFilePath(totalDoseImageFilePath.toStdString());
    rtDosePinnTotalDose.setDoseImageHeaderFilePath(totalDoseImageHeaderFilePath.toStdString());

    if (OKFlag != rtDosePinnTotalDose.getDoseArrayFromPinnacleDoseAndHeaderFiles())
    {
        std::cout << "ERROR:  in running PinnacleFiles::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles" << std::endl;
        return;
    }

    if (debug)
    {
        writePinnacleDoseToNrrdFile(
                resultFolder.toStdString() + "/TotalDoseArray_" + std::to_string(myPinnacleFile.Patient.PatientID) +
                "_Plan_" + ".nrrd", rtDosePinnTotalDose);
    }

    robustAnalyzer(myPinnacleFile, rtDosePinnTotalDose, resultFolder,
                   sysTranSigmaX, sysTranSigmaY, sysTranSigmaZ,
                   randomTranSigmaX, randomTranSigmaY, randomTranSigmaZ,
                   sysRotSigmaX, sysRotSigmaY, sysRotSigmaZ,
                   randomRotSigmaX, randomRotSigmaY, randomRotSigmaZ,
                   intraFxTranSigmaX, intraFxTranSigmaY, intraFxTranSigmaZ,
                   intraFxRotSigmaX, intraFxRotSigmaY, intraFxRotSigmaZ,
                   numFrac, numSimulation, numSubFrac,
                   trialName, planName, debug);

}

void MainWindow::robustAnalyzer(PinnacleFiles &myPinnacleFile, RtDosePinn &rtDosePinnTotalDose, const QString &resultFolder,
                                float sysTranSigmaX, float sysTranSigmaY, float sysTranSigmaZ,
                                float randomTranSigmaX, float randomTranSigmaY, float randomTranSigmaZ,
                                float sysRotSigmaX, float sysRotSigmaY, float sysRotSigmaZ,
                                float randomRotSigmaX, float randomRotSigmaY, float randomRotSigmaZ,
                                float intraFxTranSigmaX, float intraFxTranSigmaY, float intraFxTranSigmaZ,
                                float intraFxRotSigmaX, float intraFxRotSigmaY, float intraFxRotSigmaZ,
                                int numFrac, int numSimulation, int numSubFrac,
                                const QString &trialName, const QString &planName, bool debug)
{
    cout << "\nroiPropVector has " << myPinnacleFile.roisPropVector.size() << " members" << std::endl;

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if (OKFlag != myPinnacleFile.createBitmapForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (OKFlag != myPinnacleFile.calculateCenterOfMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            return;
        }

        if (debug)
        {
            string RoiBitmapNameToSave = resultFolder.toStdString() + '/' +
                                              myPinnacleFile.roisPropVector[iRoi].name +
                                              "ImageSet" + myPinnacleFile.roisPropVector[iRoi].volume_name +
                                              "binary-label.nrrd";
            cout << " Saving binary mask at --> " << RoiBitmapNameToSave << std::endl;
            if (OKFlag !=
                myPinnacleFile.saveBitmapForGivenRoiName(RoiBitmapNameToSave, myPinnacleFile.roisPropVector[iRoi].name))
            {
                return;
            }


            string partialVolVecNameToSave = resultFolder.toStdString() + '/' +
                                                  myPinnacleFile.roisPropVector[iRoi].name +
                                                  "ImageSet" + myPinnacleFile.roisPropVector[iRoi].volume_name +
                                                  "partialVol-label.nrrd";
            cout << " Saving partial volume--> " << RoiBitmapNameToSave << std::endl;

            if (OKFlag != myPinnacleFile.savePartialVolumeOFBitmapForGivenRoiName(partialVolVecNameToSave,
                                                                                  myPinnacleFile.roisPropVector[iRoi].name))
            {
                return;
            }
        }

    }

    // 2.5) calculated voxel mass --> TODO: DMH will be optinal in next version for this version I need them for QA purpose therefore mass needs to be calculated
    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if (OKFlag != myPinnacleFile.createBitmapVoxelMassForGivenRoiName(myPinnacleFile.roisPropVector[iRoi].name))
        {
            cout << "ERROR:  in  running createBitmapVoxelMassForGivenRoiName for ROI: "
                      << myPinnacleFile.roisPropVector[iRoi].name << std::endl;

        }
        if (verbose)
        {
            cout << "Roi[" << myPinnacleFile.roisPropVector[iRoi].name << "].mass = "
                      << myPinnacleFile.roisPropVector[iRoi].mass << std::endl;
        }
    }

    //2.75) identify target volume and normal tissues automatically

    if (OKFlag != myPinnacleFile.deduceRoiTypeFromName())
    {
        cout << "ERROR: This is not what we expected" << std::endl;
        return;
    }


    // 3) Setting up uncertainty model
    cout << "... Instantiating ModelData ... " << std::endl;
    ModelData *modelData = new ModelData(cout,
                                         myPinnacleFile.roisPropVector); // // It automatically creates GUMMODELMap
    string ROINameForCenterOfRotation;
    bool atLeastOneTargetBool = false;

    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        if (myPinnacleFile.roisPropVector[iRoi].IsTargetVolume)
        {
            ROINameForCenterOfRotation = myPinnacleFile.roisPropVector[iRoi].name;
            atLeastOneTargetBool = true;
        }
    }

    // if there is no target we choose the first ROI name for COM
    if (!atLeastOneTargetBool)
    {
        ROINameForCenterOfRotation = myPinnacleFile.roisPropVector[0].name;
    }


    // 4) setting up rigid body motion
    setRigidBodyMotion(modelData, ROINameForCenterOfRotation,
                       sysTranSigmaX, sysTranSigmaY, sysTranSigmaZ,
                       randomTranSigmaX, randomTranSigmaY, randomTranSigmaZ,
                       sysRotSigmaX, sysRotSigmaY, sysRotSigmaZ,
                       randomRotSigmaX, randomRotSigmaY, randomRotSigmaZ,
                       numSubFrac,
                       intraFxTranSigmaX, intraFxTranSigmaY, intraFxTranSigmaZ,
                       intraFxRotSigmaX, intraFxRotSigmaY, intraFxRotSigmaZ);



    // 5) setting up Rotation and transformation of individual ROI
    RotationAndTranslationModel rotTransModel = RotationAndTranslationModel(cout);
    rotTransModel.sysRotErrorSigma.x = 0;
    rotTransModel.sysRotErrorSigma.y = 0;
    rotTransModel.sysRotErrorSigma.z = 0;
    rotTransModel.sysTranErrorSigma.x = 0;
    rotTransModel.sysTranErrorSigma.y = 0;
    rotTransModel.sysTranErrorSigma.z = 0;
    rotTransModel.rotCenter.RoiIDForRotationAroundCOM = ROINameForCenterOfRotation;
    rotTransModel.rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    std::map<std::string, boost::tuple<RotationAndTranslationModel,DeformationModel,DelineationModel>>::iterator iter = modelData->gumMap.find(
            ROINameForCenterOfRotation);
    if (iter != modelData->gumMap.end())
    {
        boost::get<0>(iter->second) = rotTransModel;
    }
    else
    {
        cout << "ERROR/Warning rotTranModel for " << ROINameForCenterOfRotation
                  << " is not created, since it was not in the gumap" << std::endl;

    }

    // 7) assign the dose to the pinnacle patient
    myPinnacleFile.setRtDosePinn(&rtDosePinnTotalDose);

    // 8) Initialze RobustAnalyzer
    // Calculation engine is instantiated here !
    RobustnessAnalyzer *robustnessAnalyzer = new RobustnessAnalyzer(cout);
    robustnessAnalyzer->setPinnacleFlag(true);
    robustnessAnalyzer->setModelData(modelData);
    robustnessAnalyzer->setPinnacleFiles(&myPinnacleFile);
    robustnessAnalyzer->setNumDoseBins(350);
    robustnessAnalyzer->setNumTrials(numSimulation);
    robustnessAnalyzer->setGPUUsage(true);
    robustnessAnalyzer->setResultFolderPath(resultFolder.toStdString());
    // intraFx uncertainty parameters
    robustnessAnalyzer->IsIntraFxRandomGaussian = false; // intra

    if (numSubFrac == 0)
    {
        robustnessAnalyzer->setNumSubFraction(
                1); // this is done becasue  to use only one function to generate transformations
    }
    else
    {
        robustnessAnalyzer->setNumSubFraction(numSubFrac);

    }

    int totalSubFXs = numFrac * robustnessAnalyzer->getNumSubFractions();
    robustnessAnalyzer->setNumFractions(totalSubFXs);

    runRobustnessAnalyzer(robustnessAnalyzer);

    writeResults(resultFolder, myPinnacleFile, robustnessAnalyzer);


    //10. Creating Report Document for This analysis
    testPrintReport(robustnessAnalyzer, &myPinnacleFile, resultFolder, trialName, planName);


    delete (modelData);
    delete (robustnessAnalyzer);
    cout << "Done !" << std::endl;
}

void MainWindow::runRobustnessAnalyzer(RobustnessAnalyzer *robustnessAnalyzer) const
{
    if (!robustnessAnalyzer->generateTransformationsParamsWithIntraFractionPinnacle())
    {
        throw " Error in generateTransformationsParams .";
        //return;
    }

    //     std::cout << "Patient" << std::to_string(patientNum) << " transformations has been created in robustnessAnalyzer  for dose  " <<  rtDose.doseCalcImageSet <<  " plan and Scan_" + std::to_string(scanFolderNumber) + " at " + nowInString() << std::endl;
//if gpu device is chosen for computation
    if (!robustnessAnalyzer->calculateAllDVHSonGpuForPinnaclePatient()) // in this version it calculated both DVHs and DMHs both
    {

        throw " Error in Calculation PDVHs on GPU(s).";
        //return;
    }

    if (!robustnessAnalyzer->calculateDchs())
    {
        throw " Error in Calculation Dchs.";
        //return;
    }

    cout << " all Dchs has been calculated in robustnessAnalyzer " << std::endl;
    cout << "Initialize Confidence Level for PDVHs based on the ROI's' type " << std::endl;
    if (!robustnessAnalyzer->calculatePDVHsConfLevelInitialValues())
    {
        throw " Error in Calculation PDVHs.";
    }
}

void MainWindow::writeResults(const QString &resultFolder, const PinnacleFiles &myPinnacleFile,
                              RobustnessAnalyzer *robustnessAnalyzer) const
{
    cout << "Writing DCHs to file... " << std::endl;
    if (!robustnessAnalyzer->writeAllDchsToFile())
    {
        throw "ERORR --> in writing DCHs to file";
    }

    cout << "calculateDVHsMeanAndVarianceMedianPercentiles... " << std::endl;
    if (!robustnessAnalyzer->calculateDVHsMeanAndVarianceMedianPercentiles())
    {
        throw "ERORR --> in calculateDVHsMeanAndVarianceMedianPercentiles  ";
    }

    cout << "Writing Planed Dvhs to file... " << std::endl;
    if (!robustnessAnalyzer->WritePlannedDVHS())
    {
        throw "ERORR --> in writing DCHs to file";
    }

    cout << "Writing diff DVHs to file... " << std::endl;
    if (!robustnessAnalyzer->WriteDiffDvhsForTrials())
    {
        throw "ERORR --> in writing diffDVHs to file";
    }


    writeVectorToBinaryFileQT(
            resultFolder.toStdString() + "/DoseBinCenters_Patient_" + to_string(myPinnacleFile.Patient.PatientID) +
            "_Plan_" + myPinnacleFile.rtDosePinn->doseCalcImageSet + ".bin", robustnessAnalyzer->doseBinCenters);

    for (int iRoi = 0; iRoi < robustnessAnalyzer->pinnacleFiles->roisPropVector.size(); ++iRoi)
    {
        writeVectorToBinaryFileQT(
                resultFolder.toStdString() + "/cDMH_" + to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                robustnessAnalyzer->cDMHForTrial[iRoi]);
        writeVectorToBinaryFileQT(
                resultFolder.toStdString() + "/diffDMH_" + to_string(myPinnacleFile.Patient.PatientID) + "_Plan_" +
                myPinnacleFile.rtDosePinn->doseCalcImageSet + "_roi_" +
                robustnessAnalyzer->pinnacleFiles->roisPropVector[iRoi].name + ".bin",
                robustnessAnalyzer->diffDMHForTrial[iRoi]);

    }
}


void MainWindow::robustnessAnalyzerDICOM(QString dicomPath,
                                         QString dicomStructurePath,
                                         QString dicomDosePath,
                                         const std::vector<std::string> &desiredRoiList,
                                         QString resultFolder,
                                         float sysTranSigmaX,
                                         float sysTranSigmaY,
                                         float sysTranSigmaZ,
                                         float randomTranSigmaX,
                                         float randomTranSigmaY,
                                         float randomTranSigmaZ,
                                         float sysRotSigmaX,
                                         float sysRotSigmaY,
                                         float sysRotSigmaZ,
                                         float randomRotSigmaX,
                                         float randomRotSigmaY,
                                         float randomRotSigmaZ,
                                         float intraFxTranSigmaX,
                                         float intraFxTranSigmaY,
                                         float intraFxTranSigmaZ,
                                         float intraFxRotSigmaX,
                                         float intraFxRotSigmaY,
                                         float intraFxRotSigmaZ,
                                         int numFrac,
                                         int numSimulation,
                                         int numSubFrac,
                                         QString trialName,
                                         QString planName,
                                         bool debug)
{
    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setVebose(debug); // if debug is on verbosity will be on as well

    // Check if this a dicom folder
    DicomFilesProp dicomPropFiles(logstream);
    dicomPropFiles.setDicomFolderPath(dicomPath.toStdString());
    if (!dicomPropFiles.parseFolder())
    {
        logstream << "Error in Parsing the folder one or more dicom files have problems" << std::endl;
        return;
    }
    std::vector<string> ctFilePathList;
    string structureFile;
    string doseFile;
    const auto study = dicomPropFiles.PatientsVec[0].StudyVec[0];
    for (auto series : study.SeriesVec)
    {
        if (series.ModalityID.compare("CT") == 0)
        {
            ctFilePathList = series.FilePathVec;
        }
        if (series.ModalityID.compare("RTSTRUCT") == 0)
        {
            structureFile = series.FilePathVec[0];
        }
        if (series.ModalityID.compare("RTDOSE") == 0)
        {
            doseFile = series.FilePathVec[0];
        }
    }
    cout << study.SeriesVec[0].ModalityID.c_str() << endl;
    cout << ctFilePathList[0] << endl;
    cout << structureFile << endl;
    cout << doseFile << endl;

    myPinnacleFile.Patient.PatientID = stoi(dicomPropFiles.PatientsVec[0].PatientID);
    myPinnacleFile.Patient.PatientPath = "from DICOM";
//    myPinnacleFile.Patient.LastName
//    myPinnacleFile.Patient.FirstName;
//    myPinnacleFile.Patient.MiddleName;
//    myPinnacleFile.Patient.MedicalRecordNumber;
//    myPinnacleFile.Patient.EncounterNumber;
//    myPinnacleFile.Patient.PrimaryPhysician;
//    myPinnacleFile.Patient.AttendingPhysician;
//    myPinnacleFile.Patient.ReferringPhysician;
//    myPinnacleFile.Patient.RadiationOncologist;
//    myPinnacleFile.Patient.Oncologist;
//    myPinnacleFile.Patient.Radiologist;
//    myPinnacleFile.Patient.Prescription;
//    myPinnacleFile.Patient.Disease;
//    myPinnacleFile.Patient.Diagnosis;
//    myPinnacleFile.Patient.Comment;
//    myPinnacleFile.Patient.NextUniquePlanID;
//    myPinnacleFile.Patient.NextUniqueImageSetID;
//    myPinnacleFile.Patient.Gender;
//    myPinnacleFile.Patient.DateOfBirth;

    std::vector<CTImage> ctImageSet = std::vector<CTImage>(ctFilePathList.size());
    for (int iCT = 0; iCT < ctFilePathList.size(); ++iCT)
    {
        ctImageSet[iCT].setCTDicomFilePath(ctFilePathList[iCT]);
        ctImageSet[iCT].parseCTDicomfile();
    }
    // reordering by z value
    auto cmp = [](const CTImage &a, const CTImage &b)
    {
        return a.sliceZvalue < b.sliceZvalue;
    };
    std::sort(ctImageSet.begin(), ctImageSet.end(), cmp);
    cout << endl;

    // DICOM images to Pinnacle images
    {
        ImageSet_type imageSet;
        imageSet.isStartWithDICOM = true;
        imageSet.PatientID = myPinnacleFile.Patient.PatientID;
        imageSet.NumberOfImages = ctImageSet.size();

        auto &imageHeader = imageSet.imageHeader;
        imageHeader.db_name = ctImageSet[0].StudyID;
        imageHeader.x_dim = ctImageSet[0].Columns;
        imageHeader.y_dim = ctImageSet[0].Rows;
        imageHeader.z_dim = ctImageSet.size();
        imageHeader.x_pixdim = ctImageSet[0].PixelSpacing[0] / 10;
        imageHeader.y_pixdim = ctImageSet[0].PixelSpacing[1] / 10;
        imageHeader.z_pixdim =
                (ctImageSet.back().sliceZvalue - ctImageSet.front().sliceZvalue) / (ctImageSet.size() - 1) / 10;
        imageHeader.x_start_dicom = ctImageSet[0].ImagePositionPatient[0] / 10;
        imageHeader.y_start_dicom = -ctImageSet[0].ImagePositionPatient[1] / 10
                                    - (imageHeader.y_dim - 1) * imageHeader.y_pixdim;
        imageHeader.x_start = imageHeader.x_start_dicom;
        imageHeader.y_start = imageHeader.y_start_dicom + 2 * ctImageSet[0].ImagePositionPatient[1] / 10;
        imageHeader.z_start = -ctImageSet[0].ImagePositionPatient[2] / 10
                              - (imageHeader.z_dim - 1) * imageHeader.z_pixdim;
        imageSet.isImageHeaderLoaded = true;

        imageSet.volumeDataUint16.clear();
        for (int iCT = ctFilePathList.size() - 1; iCT >= 0; --iCT)
        {
            auto volumeData = ctImageSet[iCT].pixelDataVectorUint16;

            std::copy(volumeData.begin(), volumeData.end(),
                      std::back_inserter(imageSet.volumeDataUint16));
        }

        imageSet.volumeXGridDataVectorInCm = std::vector<float>(imageHeader.x_dim);
        imageSet.volumeYGridDataVectorInCm = std::vector<float>(imageHeader.y_dim);
        imageSet.volumeZGridDataVectorInCm = std::vector<float>(imageHeader.z_dim);

        for (int ix = 0; ix < imageSet.imageHeader.x_dim; ++ix)
        {
            imageSet.volumeXGridDataVectorInCm[ix] = imageHeader.x_start_dicom + ix * imageHeader.x_pixdim;
        }
        for (int iy = 0; iy < imageSet.imageHeader.y_dim; ++iy)
        {
            imageSet.volumeYGridDataVectorInCm[iy] = imageHeader.y_start_dicom +
                                                     (imageHeader.y_dim - iy - 1.0) * imageHeader.y_pixdim;
        }
        for (int iz = 0; iz < imageSet.imageHeader.z_dim; ++iz)
        {
            imageSet.volumeZGridDataVectorInCm[iz] = imageHeader.z_start + iz * imageHeader.z_pixdim;
        }
        imageSet.isVolumeDataLoaded = true;

        myPinnacleFile.Patient.ImageSetList.push_back(imageSet);
    }

    if (debug)
    {
        for (int iImageSet = 0; iImageSet < myPinnacleFile.Patient.ImageSetList.size(); ++iImageSet)
        {
            writeVectorToNrrdFile(resultFolder.toStdString() + "/ImageSetPixelData_" +
                                  std::to_string(myPinnacleFile.Patient.PatientID) +
                                  "ImageSet" + myPinnacleFile.Patient.ImageSetList[iImageSet].ImageName + ".nrrd",
                                  myPinnacleFile.Patient.ImageSetList[iImageSet],
                                  myPinnacleFile.Patient.ImageSetList[iImageSet].volumeDataUint16);
        }

    }

    // 2) read ROIs for plan.roi
    RtStructure2 rtStructure = RtStructure2();
    rtStructure.associateCTDicomFile = ctFilePathList[0].c_str();
    rtStructure.setCTImageSet(&ctImageSet);

    structureFile = dicomStructurePath.toStdString();
    rtStructure.rtStructFileName = structureFile.c_str();

    if (!rtStructure.constructRoiNumIDMap())
    {
        logstream << "Error in ROIName Numuber map between StructureSetROISeq and RoiContourSeq in DICOM file"
                  << std::endl;
        return;

    }

    if (!rtStructure.ReadAllRoisProp())
    {
        logstream << " ERORR in reading ROIs basic properties." << std::endl;
        return;
    }

    std::vector<std::string> selectedRoiVec = desiredRoiList;
    std::vector<int> selectedRoiIsTargetVolumeFlagVec;
    for (int iRoi = 0; iRoi < selectedRoiVec.size(); ++iRoi)
    {
        if (selectedRoiVec[iRoi].find("TV") >= 0)
            selectedRoiIsTargetVolumeFlagVec.push_back(1);
        else
            selectedRoiIsTargetVolumeFlagVec.push_back(0);
    }

    std::vector<double> selectedRoiPointCloudResolutionVec = std::vector<double>(selectedRoiVec.size(), 1.5);

    if (!rtStructure.verifyAndAddRoiNameListForComputation(selectedRoiVec, selectedRoiPointCloudResolutionVec,
                                                           selectedRoiIsTargetVolumeFlagVec)) // verify selectedRoiVec
    {
        logstream
                << " Check input XML file for the request ROI name. Or check RT structre path and make sure it is pointing to the right RT structure DICOM file."
                << std::endl;
        return;
    }

    if (!rtStructure.createROIModel())
    {
        logstream << "Error in creating ROI model of rtStructure" << std::endl;
        return;
    }

    // DICOM-RS to Pinnacle roi
    myPinnacleFile.roisPropVector.clear();
    for (auto roi : rtStructure.roiVector)
    {
        roi_prop rp;

        rp.name = roi.RoiName;
        rp.DisplayColor = roi.DisplayColor;

        rp.boundingCuboid.xMin = roi.boundingBox.minX / 10;
        rp.boundingCuboid.xMax = roi.boundingBox.maxX / 10;
        rp.boundingCuboid.yMin = -roi.boundingBox.maxY / 10;
        rp.boundingCuboid.yMax = -roi.boundingBox.minY / 10;
        rp.boundingCuboid.zMin = (-roi.boundingBox.maxZ - (ctImageSet.size() - 1) * ctImageSet[0].PixelSpacing[2]) / 10;
        rp.boundingCuboid.zMax = (-roi.boundingBox.minZ - (ctImageSet.size() - 1) * ctImageSet[0].PixelSpacing[2]) / 10;

        rp.curveVector.clear();
        for (auto roi_curve : roi.curveVector)
        {
            auto rp_curve = roi_curve;
            rp_curve.minX = roi_curve.minX / 10;
            rp_curve.maxX = roi_curve.maxX / 10;
            rp_curve.minY = -roi_curve.maxY / 10;
            rp_curve.maxY = -roi_curve.minY / 10;

            for (auto &rc_point : rp_curve.point)
            {
                rc_point.x = rc_point.x / 10;
                rc_point.y = -rc_point.y / 10;
                rc_point.z = (-rc_point.z - (ctImageSet.size() - 1) * ctImageSet[0].PixelSpacing[2]) / 10;
            }
            rp.curveVector.push_back(rp_curve);
            rp.curveZLocations.push_back(rp_curve.point[0].z);
        }
        rp.volume_name = ctImageSet[0].StudyID;

        myPinnacleFile.roisPropVector.push_back(rp);
    }

    // 6) Reading a dose file
    RtDosePinn rtDosePinnTotalDose = RtDosePinn();
    readDicomDoseToPinnacleDose(dicomDosePath.toStdString(), rtDosePinnTotalDose);
    if (debug)
    {
        writePinnacleDoseToNrrdFile(
                resultFolder.toStdString() + "/TotalDoseArray_" + std::to_string(myPinnacleFile.Patient.PatientID) +
                "_Plan_" + ".nrrd", rtDosePinnTotalDose);
    }

    robustAnalyzer(myPinnacleFile, rtDosePinnTotalDose, resultFolder,
                   sysTranSigmaX, sysTranSigmaY, sysTranSigmaZ,
                   randomTranSigmaX, randomTranSigmaY, randomTranSigmaZ,
                   sysRotSigmaX, sysRotSigmaY, sysRotSigmaZ,
                   randomRotSigmaX, randomRotSigmaY, randomRotSigmaZ,
                   intraFxTranSigmaX, intraFxTranSigmaY, intraFxTranSigmaZ,
                   intraFxRotSigmaX, intraFxRotSigmaY, intraFxRotSigmaZ,
                   numFrac, numSimulation, numSubFrac,
                   trialName, planName, debug);
}


void MainWindow::setIntraFracMotionError(const ModelData *modelData, int numSubFrac,
                                         float intraFxTranSigmaX, float intraFxTranSigmaY, float intraFxTranSigmaZ,
                                         float intraFxRotSigmaX, float intraFxRotSigmaY, float intraFxRotSigmaZ) const
{
    if (numSubFrac == 0)
    {
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x = 0;
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y = 0;
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z = 0;

        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x = 0;
        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y = 0;
        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z = 0;

    }
    else
    {
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.x = intraFxTranSigmaX;
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.y = intraFxTranSigmaY;
        modelData->rigidBodyMotionModel->intraFractionTranErrorSigma.z = intraFxTranSigmaZ;

        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.x = intraFxRotSigmaX;
        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.y = intraFxRotSigmaY;
        modelData->rigidBodyMotionModel->intraFractionRotErrorSigma.z = intraFxRotSigmaZ;
    }
}

void
MainWindow::setRigidBodyMotion(ModelData *modelData, const string &RoiToRotateAroundCOMString,
                               float sysTranSigmaX, float sysTranSigmaY, float sysTranSigmaZ,
                               float sysRotSigmaX, float sysRotSigmaY, float sysRotSigmaZ,
                               float randomTranSigmaX, float randomTranSigmaY, float randomTranSigmaZ,
                               float randomRotSigmaX, float randomRotSigmaY, float randomRotSigmaZ,
                               int numSubFrac,
                               float intraFxTranSigmaX, float intraFxTranSigmaY, float intraFxTranSigmaZ,
                               float intraFxRotSigmaX, float intraFxRotSigmaY, float intraFxRotSigmaZ) const
{
    modelData->rigidBodyMotionModel = new RigidBodyMotionModel(cout);
    modelData->rigidBodyMotionModel->DoseConvFlag = false;
    modelData->rigidBodyMotionModel->rotCenter.RotationCenterType = RotationCenter::ROICenterOfMass;
    modelData->rigidBodyMotionModel->rotCenter.RoiIDForRotationAroundCOM = RoiToRotateAroundCOMString;
    modelData->rigidBodyMotionExist = true;

    modelData->rigidBodyMotionModel->sysTranErrorSigma.x = sysTranSigmaX;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.y = sysTranSigmaY;
    modelData->rigidBodyMotionModel->sysTranErrorSigma.z = sysTranSigmaZ;

    modelData->rigidBodyMotionModel->randTranErrorSigma.x = randomTranSigmaX;
    modelData->rigidBodyMotionModel->randTranErrorSigma.y = randomTranSigmaY;
    modelData->rigidBodyMotionModel->randTranErrorSigma.z = randomTranSigmaZ;

    modelData->rigidBodyMotionModel->sysRotErrorSigma.x = sysRotSigmaX;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.y = sysRotSigmaY;
    modelData->rigidBodyMotionModel->sysRotErrorSigma.z = sysRotSigmaZ;

    modelData->rigidBodyMotionModel->randRotErrorSigma.x = randomRotSigmaX;
    modelData->rigidBodyMotionModel->randRotErrorSigma.y = randomRotSigmaY;
    modelData->rigidBodyMotionModel->randRotErrorSigma.z = randomRotSigmaZ;

    setIntraFracMotionError(modelData, numSubFrac,
                            intraFxTranSigmaX, intraFxTranSigmaY, intraFxTranSigmaZ,
                            intraFxRotSigmaX, intraFxRotSigmaY, intraFxRotSigmaZ);
}


void MainWindow::listRoisForGivenPatient(QString patientPath, QString planDotRoiPath)
{
    PinnacleFiles myPinnacleFile;
    myPinnacleFile.setPatientFilePath(patientPath.toStdString());

    //myPinnacleFile.setVebose(true);
    if (myPinnacleFile.readPatientFileAndImageSets() != 0)
    {
        std::cout << "ERROR:  in reading patient file" << std::endl;
    }


    myPinnacleFile.setRoiFilePath(planDotRoiPath.toStdString());
    myPinnacleFile.readRoisProperties();

    std::cout << "\nroiPropVector has " << myPinnacleFile.roisPropVector.size() << " members" << std::endl;
    for (int iRoi = 0; iRoi < myPinnacleFile.roisPropVector.size(); ++iRoi)
    {
        std::cout << std::setw(10) << "Roi[" << std::to_string(iRoi) << "] = "
                  << myPinnacleFile.roisPropVector[iRoi].name << std::setw(20) << "ImageSet ->  "
                  << myPinnacleFile.roisPropVector[iRoi].volume_name << std::endl;
    }
}

void MainWindow::setInitialParamsForApplication()
{
    //1) initial all pointers

    //2) initial application default paramaters from setting
    QSettings settings("UVA", "RTRA");
    patientCurrentFolderQString = settings.value("patientCurrentFolderQString", QDir::currentPath()).toString();

    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaXSysTrans = settings.value("sigmaXSysTrans",tr(".9")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaYSysTrans = settings.value("sigmaYSysTrans",tr("2.7")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaZSysTrans = settings.value("sigmaZSysTrans",tr("1.7")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaXSysRot = settings.value("sigmaXSysRot",tr(".3")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaYSysRot = settings.value("sigmaYSysRot",tr(".3")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaZSysRot = settings.value("sigmaZSysRot",tr(".3")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaXRandomTrans = settings.value("sigmaXRandomTrans",tr("0.91")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaYRandomTrans = settings.value("sigmaYRandomTrans",tr("2.71")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaZRandomTrans = settings.value("sigmaZRandomTrans",tr("1.71")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaXRandomRot = settings.value("sigmaXRandomRot",tr(".1")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaYRandomRot = settings.value("sigmaYRandomRot",tr(".1")).toDouble();
    //    rigidBodyUncertaintyDefaultParamsStruct.sigmaZRandomRot = settings.value("sigmaZRandomRot",tr(".1")).toDouble();

    //    simulationDefaultParamsStruct.numTrials = settings.value("numTrials",tr("1000")).toInt();
    //    simulationDefaultParamsStruct.numDoseBins = settings.value("numDoseBins",tr("300")).toInt();
    //    simulationDefaultParamsStruct.numFractions = settings.value("numFractions",tr("30")).toInt();
    //    simulationDefaultParamsStruct.useGPU = settings.value("useGPU",tr("1")).toBool();






}

bool MainWindow::specifyPatientFolder()
{
    std::string class_member = "MainGuiWindow::specifyPatientFolder: ";
    QSettings settings(tr("UVA"), tr("RTRA"));
    QFileDialog qFileDialog(this);
    int titleBarHeight = this->style()->pixelMetric(QStyle::PM_TitleBarHeight);
    int titleBarMargin = this->style()->pixelMetric(QStyle::PM_ToolBarItemMargin);
    // TODO: it used to work on PlanRobustnessAnalysisQT
    //    qFileDialog.move(QApplication::desktop()->screen()->rect().center().x() - qFileDialog.rect().center().x(),this->rect().height()+2*(titleBarHeight+titleBarMargin));
    QString dir = qFileDialog.getExistingDirectory(this, tr("Open Directory"),
                                                   patientCurrentFolderQString,
                                                   QFileDialog::ShowDirsOnly
                                                   | QFileDialog::DontResolveSymlinks);
    if (dir.toStdString().compare("") == 0)
    {
        logstream << class_member << " Open patient folder has canceled!" << std::endl;
        return Failed_RTRA;
    }
    settings.setValue("patientCurrentFolderQString", dir);
    patientCurrentFolderQString = dir;
    return OK_RTRA;
}
