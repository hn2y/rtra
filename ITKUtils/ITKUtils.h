#if defined(_MSC_VER)
#pragma warning ( disable : 4786 )
#endif

#include <itkImage.h>
#include <itkCommand.h>

#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkGDCMImageIO.h>
#include <itkRegionOfInterestImageFilter.h>
#include <itkFlipImageFilter.h>

#include <itkImageRegionIterator.h>
#include <itkImageRegionIteratorWithIndex.h>

#include <itkBinaryBallStructuringElement.h>
#include <itkBinaryErodeImageFilter.h>
#include <itkBinaryDilateImageFilter.h>

// std
#include <iostream>
#include <fstream>
#include <limits>
#include <string>
#include <csignal>

#define ITK_UTILS

using namespace std;

// Definition of Data type
using InputPixelType = float;
using InternalPixelType = float;
using OutputPixelType = float;

using LabelPixelType = short;
using MaskPixelType = short;

const unsigned int Dimension = 3;
const unsigned int OutputDimension = 2;
const MaskPixelType maskValue = 1;

using InputImageType = itk::Image<InputPixelType, Dimension>;
using InternalImageType = itk::Image<InternalPixelType, Dimension>;
using OutputImageType = itk::Image<OutputPixelType, OutputDimension>;
using InputImage2DType = itk::Image<InputPixelType, Dimension - 1>;

using LabelImageType = itk::Image<LabelPixelType, Dimension>;
using MaskImageType = itk::Image<MaskPixelType, Dimension>;
using MaskImage2DType = itk::Image<MaskPixelType, Dimension - 1>;


// Definition of Properties
using SpacingType = InputImageType::SpacingType;
using OriginType = InputImageType::PointType;
using RegionType = InputImageType::RegionType;
using SizeType = InputImageType::SizeType;
using IndexType = InputImageType::IndexType;
using DirectionType = InputImageType::DirectionType;


//Definition of Morphological operation sturcture elements
using StructuringElementType = itk::BinaryBallStructuringElement<MaskPixelType, Dimension>;
using DilateFilterType = itk::BinaryDilateImageFilter<MaskImageType, MaskImageType, StructuringElementType>;
using ErodeFilterType = itk::BinaryErodeImageFilter<MaskImageType, MaskImageType, StructuringElementType>;


template<typename TImageType>
typename TImageType::Pointer ReadImageFile(string fileName)
{
    using ImageReaderType = itk::ImageFileReader<TImageType>;

    typename ImageReaderType::Pointer ImageReader = ImageReaderType::New();
    ImageReader->SetFileName(fileName);
    ImageReader->Update();

    itk::SmartPointer<TImageType> Image = ImageReader->GetOutput();

    return Image;
}


template<typename TImageType>
void WriteImageFile(const itk::SmartPointer<TImageType> outputImage, string fileName)
{
    ////////To write Output images /////////////////////
    using ImageWriterType = itk::ImageFileWriter<TImageType>;

    typename ImageWriterType::Pointer OutImageWriter = ImageWriterType::New();
    OutImageWriter->SetUseCompression(true);
    OutImageWriter->SetInput(outputImage);
    OutImageWriter->SetFileName(fileName);

    try
    {
        OutImageWriter->Update();
    }
    catch (itk::ExceptionObject &excp)
    {
        std::cerr << "Exception thrown while writing the series " << std::endl;
        std::cerr << excp << std::endl;
        //return EXIT_FAILURE;
    }
}


template<typename TImageType>
typename TImageType::Pointer
ApplyRoi(typename TImageType::Pointer inputImage, itk::ImageRegion<Dimension> inputRegion)
{
    typename TImageType::Pointer roiImage;

    using RoiFilterType = itk::RegionOfInterestImageFilter<TImageType, TImageType>;
    typename RoiFilterType::Pointer RoiFilter = RoiFilterType::New();
    RoiFilter->SetRegionOfInterest(inputRegion);

    RoiFilter->SetInput(inputImage);
    RoiFilter->Update();

    roiImage = RoiFilter->GetOutput();

    return roiImage;
}

MaskImageType::Pointer unionMasks(vector<MaskImageType::Pointer> inputMaskImages);

MaskImageType::Pointer GetMaskImage(LabelImageType::Pointer);

MaskImageType::Pointer GetMaskImage(LabelImageType::Pointer, LabelPixelType);

MaskImageType::RegionType RoiIndexToRegion(MaskImageType::IndexType, MaskImageType::IndexType);

void RegionToIndex(MaskImageType::RegionType, MaskImageType::IndexType &, MaskImageType::IndexType &);

MaskImageType::RegionType GetRoi(MaskImageType::Pointer);

void ExpandRoi(MaskImageType::Pointer, MaskImageType::RegionType &, const double objectSize = 10);

void BoundingCheck(MaskImageType::Pointer, InputImageType::Pointer, MaskImageType::RegionType &,
                   InputImageType::RegionType &);

void BoundingCheck(MaskImageType::Pointer, LabelImageType::Pointer, MaskImageType::RegionType &,
                   InputImageType::RegionType &);

MaskImageType::RegionType ReadROI(string);

void WriteROI(MaskImageType::RegionType, string);

// To print the progress
class ShowProgressObject
{
public:
    ShowProgressObject(itk::ProcessObject *o)
    {
        m_Process = o;
    }

    void ShowProgress()
    {
        std::cout << "\rProgress: "
                  << static_cast<unsigned int>( 100.0 * m_Process->GetProgress()) << "%";
    }

    itk::ProcessObject::Pointer m_Process;
};


// ITK and Pinnacle data bridge
#include "PinnacleFiles.h"

template<typename TImageType>
void GetPinnaclePhysicalSpace(typename TImageType::RegionType region,
                              typename TImageType::SpacingType spacing,
                              typename TImageType::PointType origin,
                              int &x_dim, int &y_dim, int &z_dim,
                              float &x_pixdim, float &y_pixdim, float &z_pixdim,
                              float &x_start_dicom, float &y_start_dicom, float &z_start)
{
    // compute Pinnacle physical space information from DICOM coordinate (LPS)
    auto size = region.GetSize();

    x_dim = size[0];
    y_dim = size[1];
    z_dim = size[2];

    x_start_dicom = origin[0] / 10;
    y_start_dicom = (-origin[1] - (size[1] - 1) * spacing[1]) / 10;
    z_start = (-origin[2] - (size[2] - 1) * spacing[2]) / 10;

    x_pixdim = spacing[0] / 10;
    y_pixdim = spacing[1] / 10;
    z_pixdim = spacing[2] / 10;
}


template<typename TImageType>
void GetPinnaclePhysicalSpace(typename TImageType::RegionType region,
                              typename TImageType::SpacingType spacing,
                              typename TImageType::PointType origin,
                              ImageSet_type &imageSet)
{
    auto imageHeader = imageSet.imageHeader;
    imageSet.isStartWithDICOM = true;
    GetPinnaclePhysicalSpace<TImageType>(region, spacing, origin,
                                         imageHeader.x_dim, imageHeader.y_dim, imageHeader.z_dim,
                                         imageHeader.x_pixdim, imageHeader.y_pixdim, imageHeader.z_pixdim,
                                         imageHeader.x_start_dicom, imageHeader.y_start_dicom, imageHeader.z_start);
    imageHeader.x_start = imageHeader.x_start_dicom;
    imageHeader.y_start = 2*origin[1]/10 + imageHeader.y_start_dicom;
    imageSet.imageHeader = imageHeader;
}

template<typename TImageType>
void GetPinnaclePhysicalSpace(typename TImageType::RegionType region,
                              typename TImageType::SpacingType spacing,
                              typename TImageType::PointType origin,
                              RtDosePinn &rtDosePinn)
{

    auto imageHeader = rtDosePinn.imageHeader;
    GetPinnaclePhysicalSpace<TImageType>(region, spacing, origin,
                                         imageHeader.DoseGridDimensionX,
                                         imageHeader.DoseGridDimensionY,
                                         imageHeader.DoseGridDimensionZ,
                                         imageHeader.DoseGridVoxelSizeX,
                                         imageHeader.DoseGridVoxelSizeY,
                                         imageHeader.DoseGridVoxelSizeZ,
                                         imageHeader.DoseGridOriginX,
                                         imageHeader.DoseGridOriginY,
                                         imageHeader.DoseGridOriginZ);
    rtDosePinn.imageHeader = imageHeader;
}

template<typename TImageType>
void GetPhysicalSpace(int x_dim, int y_dim, int z_dim,
                      float x_pixdim, float y_pixdim, float z_pixdim,
                      float x_start, float y_start, float z_start,
                      typename TImageType::RegionType &region,
                      typename TImageType::SpacingType &spacing,
                      typename TImageType::PointType &origin)
{
    // compute physical space information in DICOM coordinate (LPS)
    typename TImageType::IndexType start;
    start.Fill(0);

    typename TImageType::SizeType size;
    size[0] = x_dim;
    size[1] = y_dim;
    size[2] = z_dim;

    region.SetIndex(start);
    region.SetSize(size);

    spacing[0] = x_pixdim * 10;
    spacing[1] = y_pixdim * 10;
    spacing[2] = z_pixdim * 10;

    origin[0] = x_start * 10;
    origin[1] = -(y_start * 10 + (size[1] - 1) * spacing[1]);
    origin[2] = -(z_start * 10 + (size[2] - 1) * spacing[2]);
}

template<typename TImageType>
void GetPhysicalSpace(ImageSet_type imageSet,
                      typename TImageType::RegionType &region,
                      typename TImageType::SpacingType &spacing,
                      typename TImageType::PointType &origin)
{
    auto imageHeader = imageSet.imageHeader;
    if (imageSet.isStartWithDICOM)
    {
        GetPhysicalSpace<TImageType>(imageHeader.x_dim, imageHeader.y_dim, imageHeader.z_dim,
                                     imageHeader.x_pixdim, imageHeader.y_pixdim, imageHeader.z_pixdim,
                                     imageHeader.x_start_dicom, imageHeader.y_start_dicom, imageHeader.z_start,
                                     region, spacing, origin);
    }
    else
    {
        GetPhysicalSpace<TImageType>(imageHeader.x_dim, imageHeader.y_dim, imageHeader.z_dim,
                                     imageHeader.x_pixdim, imageHeader.y_pixdim, imageHeader.z_pixdim,
                                     imageHeader.x_start, imageHeader.y_start, imageHeader.z_start,
                                     region, spacing, origin);
    }
}

template<typename TImageType>
void GetPhysicalSpace(RtDosePinn rtDosePinn,
                      typename TImageType::RegionType &region,
                      typename TImageType::SpacingType &spacing,
                      typename TImageType::PointType &origin)
{

    auto imageHeader = rtDosePinn.imageHeader;
    GetPhysicalSpace<TImageType>(imageHeader.DoseGridDimensionX,
                                 imageHeader.DoseGridDimensionY,
                                 imageHeader.DoseGridDimensionZ,
                                 imageHeader.DoseGridVoxelSizeX,
                                 imageHeader.DoseGridVoxelSizeY,
                                 imageHeader.DoseGridVoxelSizeZ,
                                 imageHeader.DoseGridOriginX,
                                 imageHeader.DoseGridOriginY,
                                 imageHeader.DoseGridOriginZ,
                                 region, spacing, origin);
}

template<typename TImageType>
typename TImageType::Pointer
GetItkImage(typename TImageType::RegionType &region,
            typename TImageType::SpacingType &spacing,
            typename TImageType::PointType &origin,
            vector<typename TImageType::PixelType> volumeData)
{
    auto outputImage = TImageType::New();
    auto direction = outputImage->GetDirection();

    // allocate outputImage first
    outputImage->SetOrigin(origin);
    outputImage->SetSpacing(spacing);
    outputImage->SetRegions(region);
    outputImage->Allocate();
    outputImage->FillBuffer(0);

    // write volume data to itkImage
    itk::ImageRegionIterator<TImageType> out(outputImage, region);
    typename vector<typename TImageType::PixelType>::iterator it = volumeData.begin();

    for (out.GoToBegin();
         it != volumeData.end();
         ++it, ++out)
    {
        if (*it) out.Set(*it);
    }

    // Z-direction data flip
    using FlipFilterType = itk::FlipImageFilter<TImageType>;
    typename FlipFilterType::Pointer flipper = FlipFilterType::New();
    typename FlipFilterType::FlipAxesArrayType flipAxes;
    flipAxes[0] = false;
    flipAxes[1] = false;
    flipAxes[2] = true;
    flipper->SetInput(outputImage);
    flipper->FlipAboutOriginOff();
    flipper->SetFlipAxes(flipAxes);
    flipper->Update();

    outputImage = flipper->GetOutput();
    outputImage->SetOrigin(origin);
    outputImage->SetDirection(direction);

    return outputImage;
}

template<typename TImageType>
typename TImageType::Pointer
GetItkImage(ImageSet_type imageSet, vector<typename TImageType::PixelType> volumeData)
{
    typename TImageType::RegionType region;
    typename TImageType::SpacingType spacing;
    typename TImageType::PointType origin;
    GetPhysicalSpace<TImageType>(imageSet, region, spacing, origin);
    auto outputImage = GetItkImage<TImageType>(region, spacing, origin, volumeData);

    return outputImage;
}

template<typename TImageType>
vector<typename TImageType::PixelType>
GetPinnacleImage(ImageSet_type &imageSet, typename TImageType::Pointer inputImage)
{
    // allocate inputImage first
    auto inputOrigin = inputImage->GetOrigin();
    auto inputSpacing = inputImage->GetSpacing();
    auto inputRegion = inputImage->GetLargestPossibleRegion();
    auto inputSize = inputRegion.GetSize();
    auto inputDirection = inputImage->GetDirection();

    // Z-direction data flip
    using FlipFilterType = itk::FlipImageFilter<TImageType>;
    typename FlipFilterType::Pointer flipper = FlipFilterType::New();
    typename FlipFilterType::FlipAxesArrayType flipAxes;
    flipAxes[0] = false;
    flipAxes[1] = false;
    flipAxes[2] = true;
    flipper->SetInput(inputImage);
    flipper->FlipAboutOriginOff();
    flipper->SetFlipAxes(flipAxes);
    flipper->Update();

    inputImage = flipper->GetOutput();
    auto direction = inputImage->GetDirection();
    inputImage->SetDirection(inputDirection);

    GetPinnaclePhysicalSpace<TImageType>(inputRegion, inputSpacing, inputOrigin, imageSet);

    // write itkImage to volume data
    vector<typename TImageType::PixelType> volumeData;
    itk::ImageRegionIterator<TImageType> out(inputImage, inputRegion);
    for (out.GoToBegin(); !out.IsAtEnd(); ++out)
    {
        volumeData.push_back(out.Value());
    }

    return volumeData;
}

template<typename TImageType>
vector<typename TImageType::PixelType>
GetPinnacleDoseImage(RtDosePinn &rtDosePinn, typename TImageType::Pointer inputImage, bool isTomo = false)
{
    // allocate inputImage first
    auto inputOrigin = inputImage->GetOrigin();
    auto inputSpacing = inputImage->GetSpacing();
    auto inputRegion = inputImage->GetLargestPossibleRegion();
    auto inputSize = inputRegion.GetSize();
    auto inputDirection = inputImage->GetDirection();

    if(!isTomo)
    {
        // Z-direction data flip
        using FlipFilterType = itk::FlipImageFilter<TImageType>;
        typename FlipFilterType::Pointer flipper = FlipFilterType::New();
        typename FlipFilterType::FlipAxesArrayType flipAxes;
        flipAxes[0] = false;
        flipAxes[1] = false;
        flipAxes[2] = true;
        flipper->SetInput(inputImage);
        flipper->FlipAboutOriginOff();
        flipper->SetFlipAxes(flipAxes);
        flipper->Update();

        inputImage = flipper->GetOutput();
        auto direction = inputImage->GetDirection();
        inputImage->SetDirection(inputDirection);
    }
    else
    {
        auto size = inputRegion.GetSize();
        inputOrigin[2] = inputOrigin[2] - (size[2] - 1) * inputSpacing[2];
    }

    GetPinnaclePhysicalSpace<TImageType>(inputRegion, inputSpacing, inputOrigin, rtDosePinn);

    // write itkImage to volume data
    vector<typename TImageType::PixelType> volumeData;
    itk::ImageRegionIterator<TImageType> out(inputImage, inputRegion);
    for (out.GoToBegin(); !out.IsAtEnd(); ++out)
    {
        volumeData.push_back(out.Value());
    }

    return volumeData;
}

template<typename T>
void writeVectorToNrrdFile(string filePath, ImageSet_type imageSet, vector<T> &inputVector)
{
    using OutputImageType = itk::Image<T, Dimension>;
    auto outputImage = GetItkImage<OutputImageType>(imageSet, inputVector);

    WriteImageFile<OutputImageType>(outputImage, filePath);
}

template<typename T>
void readNrrdFileToVector(string filePath, ImageSet_type imageSet, vector<T> &outputVector)
{
    using InputImageType = itk::Image<T, Dimension>;
    auto inputImage = ReadImageFile<InputImageType>(filePath);

    outputVector = GetPinnacleImage(imageSet, inputImage);
}

void writePinnacleDoseToNrrdFile(string filePath, RtDosePinn rtDosePinn);

void readNrrdFileToPinnacleDose(string filePath, RtDosePinn &rtDosePinn);

void readDicomDoseToPinnacleDose(string filePath, RtDosePinn &rtDosePinn);
