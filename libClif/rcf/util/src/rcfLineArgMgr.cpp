
#include "rcfLineArgMgr.hpp"
#include <string.h>
#include <stdio.h>
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include <iostream>

using namespace std;


void rcfLineArgMgr::basicInit()
{
   nargs = 0;
   flags = NULL;
   data = NULL;
}

rcfLineArgMgr::rcfLineArgMgr(int iArgc, char *ppcArgv[])
{
   nargs = iArgc - 1;
   nargs /= 2;
   flags = NULL;
   data = NULL;

   if( nargs > 0 )
   {
      //allocate memory 
      flags = new char* [nargs];
      if(NULL == flags)
      {
         fprintf(stderr,"ERROR: rcfLineArgMgr::rcfLineArgMgr failure to allocate memory for the flags array, of size %d \n",nargs);
         return;
      }
      else memset(flags,0,nargs*sizeof(char*));

      data = new char* [nargs];
      if(NULL == data)
      {
         fprintf(stderr,"ERROR: rcfLineArgMgr::rcfLineArgMgr failure to allocate memory for the data array, of size %d \n",nargs);
         return;
      }
      else memset(data,0,nargs*sizeof(char*));
   }
   else
   {
      fprintf(stderr,"ERROR: rcfLineArgMgr::rcfLineArgMgr no arguments to process. Value of iArgc %d \n",iArgc);
      return;
   }

   int length;
   int index = 1;
   // memory allocated, process the line
    for(int indext=0; indext<nargs; indext++) 
    {
       length = strlen(ppcArgv[index]);
       flags[indext] = new char[length+1];
       strcpy(flags[indext],ppcArgv[index]);
       index++;
       length = strlen(ppcArgv[index]);
       data[indext] = new char[length+1];
       strcpy(data[indext],ppcArgv[index]);
       index++;
       
    }

    // done
   return;
}


rcfLineArgMgr::~rcfLineArgMgr()
{
   if( nargs > 0 )
   {
      for(int i=0; i<nargs; i++)
      {
         if(NULL != flags[i])
         {
             delete[] flags[i];
             flags[i] = NULL;
         }
         if(NULL != data[i])
         {
             delete[] data[i];
             data[i] = NULL;
         }
      }
      if(NULL != flags)
      {
          delete[] flags;
          flags = NULL;
      }
      if(NULL != data)
      {
          delete[] data;
          data = NULL;
      }
   }
}

int rcfLineArgMgr::getMessage(char *flag, int *usr_data)
{
   for(int i=0; i<nargs; i++)
   {
      if(0 == strcmp(flag,flags[i]))
      {
         sscanf(data[i],"%d",usr_data);
         return(CF_OK);
      }
   }
   return(CF_FAIL);
}

int rcfLineArgMgr::getMessage(char *flag, float *usr_data)
{
   for(int i=0; i<nargs; i++)
   {
      if(0 == strcmp(flag,flags[i]))
      {
         sscanf(data[i],"%f",usr_data);
         return(CF_OK);
      }
   }
   return(CF_FAIL);
}

int rcfLineArgMgr::getMessage(char *flag, char** usr_data)
{
   for(int i=0; i<nargs; i++)
   {
      if(0 == strcmp(flag,flags[i]))
      {
         *usr_data = data[i]; 
         return(CF_OK);
      }
   }
   return(CF_FAIL);
}

int rcfLineArgMgr::getNumberOfTokens()
{
   return(nargs);
}

bool rcfLineArgMgr::IsValid()
{
   if(nargs>0)return(true);
   else return(false);
}
