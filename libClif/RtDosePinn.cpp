#include "RtDosePinn.h"
#include "someHelpers.h"

// TODO: this should be written differently , for now to use RTDose in dicom parser, HN did this fast and dirty.
RtDosePinn::RtDosePinn()
{
    header = Header(); // reset header
    imageHeader = ImageHeader(); // reset ImageHeader
    doseHeaderFilePath.clear();
    doseImageFilePath.clear();
    doseImageHeaderFilePath.clear();
}


int RtDosePinn::getDoseArrayFromPinnacleDoseAndHeaderFiles()
{
    //TODO:: dont forget to read some other fields from the header file
    //TODO: define two  structs for doseHeaderFilePath and DoseImageHeaderFile and make them cleans
    std::vector<std::string> headerLines;
    std::string str;
    std::ifstream file(doseHeaderFilePath.c_str());

    if (file.is_open())
    {


        while (!file.eof())
        {
            std::getline(file, str);
            str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());


            // Process str
            std::size_t indexStart  = str.find("bytes_pix =");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+11;

                std::string bytes_pixString= str.substr (start,str.length()-start-1);
                header.bytes_pix =std::stoi(bytes_pixString);
                continue;
            }


            indexStart  = str.find("x_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string x_dimNumString=  str.substr (start,str.length()-start-1);
                header.x_dim =std::stoi(x_dimNumString);
                // doseImageColumns=x_dim;

                continue;
            }

            indexStart  = str.find("y_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string y_dimNumString=  str.substr (start,str.length()-start-1);
                header.y_dim =std::stoi(y_dimNumString);
                //doseImageRows=y_dim;
                continue;
            }

            indexStart  = str.find("z_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string z_dimNumString=  str.substr (start,str.length()-start-1);
                header.z_dim =std::stoi(z_dimNumString);
                //  doseImageNumOfFrames=z_dim;
                continue;
            }

            //        x_pixdim = 0.300000;
            //            y_pixdim = 0.300000;
            //            z_pixdim = 0.300000;

            indexStart  = str.find("x_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string x_pixdimString=  str.substr (start,str.length()-start-1);
                header.x_pixdim =std::stof(x_pixdimString);
                //DoseImagePixelSpacing[0]=x_pixdim;
                continue;

            }

            indexStart  = str.find("y_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string y_pixdimString=  str.substr (start,str.length()-start-1);
                header.y_pixdim =std::stof(y_pixdimString);
                // DoseImagePixelSpacing[1]=y_pixdim;
                continue;

            }

            indexStart  = str.find("z_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string z_pixdimString=  str.substr (start,str.length()-start-1);
                header.z_pixdim =std::stof(z_pixdimString);
                // DoseImageSpacingBetweenSlices=z_pixdim;
                continue;

            }

            indexStart  = str.find("x_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string x_startString=  str.substr (start,str.length()-start-1);
                header.x_start =std::stof(x_startString);
                continue;

            }

            indexStart  = str.find("y_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string y_startString=  str.substr (start,str.length()-start-1);
                header.y_start =std::stof(y_startString);
                continue;

            }

            indexStart  = str.find("z_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string z_startString=  str.substr (start,str.length()-start-1);
                header.z_start =std::stof(z_startString);
                continue;

            }






            indexStart  = str.find("X_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string X_offsetString=  str.substr (start,str.length()-start-1);
                header.X_offset =std::stof(X_offsetString);



                continue;

            }

            indexStart  = str.find("Y_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string Y_offsetString=  str.substr (start,str.length()-start-1);
                header.Y_offset =std::stof(Y_offsetString);
                continue;

            }
            indexStart  = str.find("x_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string x_start_dicomString=  str.substr (start,str.length()-start-1);
                header.x_start_dicom =std::stof(x_start_dicomString);
                continue;

            }

            indexStart  = str.find("y_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string y_start_dicomString=  str.substr (start,str.length()-start-1);
                header.y_start_dicom =std::stof(y_start_dicomString);

                continue;

            }



            //headerFilePath.push_back(str);

        }
    } // is_open
    else
    {
        std::cout << "Error in opening dose header file: forget to set or do not exist in the path  " << std::endl;
        return FAIL;
    }
    std::ifstream doseImageHeaderFile(doseImageHeaderFilePath.c_str());
    if (doseImageHeaderFile.is_open())
    {


        while (!doseImageHeaderFile.eof())
        {
            std::getline(doseImageHeaderFile, str);
            str.erase(std::remove(str.begin(), str.end(), '\r'), str.end());

            // Process str
            std::size_t indexStart  = str.find("DoseGrid.VoxelSize.X =");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+22;

                std::string DoseGridVoxelSizeXString= str.substr (start,str.length()-start-1);
                imageHeader.DoseGridVoxelSizeX =std::stof(DoseGridVoxelSizeXString);
                //                DoseImagePixelSpacing[0]=DoseGridVoxelSizeX;
                continue;
            }


            indexStart  = str.find("DoseGrid.VoxelSize.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+22;
                std::string DoseGridVoxelSizeYString=  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridVoxelSizeY = std::stof(DoseGridVoxelSizeYString);
                //                DoseImagePixelSpacing[1]=DoseGridVoxelSizeY;
                continue;
            }

            indexStart  = str.find("DoseGrid.VoxelSize.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+22;
                std::string DoseGridVoxelSizeZString=  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridVoxelSizeZ = std::stof(DoseGridVoxelSizeZString);;
                //                DoseImageSpacingBetweenSlices=DoseGridVoxelSizeZ;
                continue;
            }


            indexStart  = str.find("DoseGrid.Dimension.X =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionXString=  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridDimensionX =std::stoi(DoseGridDimensionXString);
                //                doseImageColumns=DoseGridDimensionX;


                continue;
            }

            indexStart  = str.find("DoseGrid.Dimension.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionYString=  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridDimensionY =std::stoi(DoseGridDimensionYString);
                //                doseImageRows=DoseGridDimensionY ;
                continue;
            }

            indexStart  = str.find("DoseGrid.Dimension.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionZString=  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridDimensionZ =std::stoi(DoseGridDimensionZString);
                //                doseImageNumOfFrames=DoseGridDimensionZ;
                continue;
            }


            //        x_pixdim = 0.300000;
            //            y_pixdim = 0.300000;
            //            z_pixdim = 0.300000;

            indexStart  = str.find("DoseGrid.Origin.X =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginXString =  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridOriginX =std::stof(DoseGridOriginXString);

                continue;

            }

            indexStart  = str.find("DoseGrid.Origin.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginYString =  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridOriginY =std::stof(DoseGridOriginYString);
                continue;

            }

            indexStart  = str.find("DoseGrid.Origin.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginZString =  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridOriginZ =std::stof(DoseGridOriginZString);
                continue;

            }

            indexStart  = str.find("DoseGrid.BinaryOrder.Endian =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+30;
                std::string DoseGridBinaryOrderEndianString =  str.substr (start,str.length()-start-1);
                imageHeader.DoseGridBinaryOrderEndian =std::stoi(DoseGridBinaryOrderEndianString);
                continue;

            }



            //headerFilePath.push_back(str);

        }
    } // is_open
    else
    {
        std::cout << "Error in opening dose image  header file" << std::endl;
        return FAIL;

    }



//
//    double doseImagePositionPatient_X = header.x_start_dicom + imageHeader.DoseGridOriginX*10 ;
//    double doseImagePositionPatient_Y = -header.y_start_dicom -imageHeader.DoseGridOriginY*10 - (imageHeader.DoseGridDimensionY-1)*imageHeader.DoseGridVoxelSizeY;
//    double doseImagePositionPatient_Z  = -(imageHeader.DoseGridOriginZ*10+(imageHeader.DoseGridDimensionZ -1)*imageHeader.DoseGridVoxelSizeZ);


    //size_t expectedSizeVolumeInBytes = imageSet.imageHeader.bytes_pix*imageSet.imageHeader.x_dim*imageSet.imageHeader.y_dim*imageSet.imageHeader.z_dim;
    size_t expectedSizeVolume = imageHeader.DoseGridDimensionX*imageHeader.DoseGridDimensionY*imageHeader.DoseGridDimensionZ;

    doseArray = std::vector<float> (expectedSizeVolume,0);
//    readLitteEndianBinaryFileToStdDoubleVector(doseImageFilePath, doseArray);
    if (!readBinayFileQt(doseImageFilePath,static_cast<DataByteOrder>(imageHeader.DoseGridBinaryOrderEndian),expectedSizeVolume,doseArray))
    {
        std::cout << "ERROR: when reading file " << doseImageFilePath << std::endl;
        return FAIL;
    }


    // Centi Gray to Gray
    std::transform(doseArray.begin(), doseArray.end(), doseArray.begin(),std::bind1st(std::multiplies<double>(),.01));// Centi Gray to Gray

    updateDoseGridValues();

    return OKFlag;
}

void RtDosePinn::updateDoseGridValues()
{
    size_t expectedSizeVolume = imageHeader.DoseGridDimensionX*imageHeader.DoseGridDimensionY*imageHeader.DoseGridDimensionZ;

    auto maxDoseIt = std::max_element(doseArray.begin(), doseArray.end());
    maxDose = *maxDoseIt+.001;
    std::cout << "Max Dose = " << *maxDoseIt << std::endl;



    doseGridXvalues = std::vector<float>(imageHeader.DoseGridDimensionX, 0);
    for (int iXplane= 0; iXplane < imageHeader.DoseGridDimensionX; ++iXplane)
    {
        doseGridXvalues[iXplane] = imageHeader.DoseGridOriginX+ static_cast<float>(iXplane)*imageHeader.DoseGridVoxelSizeX;
    }


    doseGridYvalues = std::vector<float>(imageHeader.DoseGridDimensionY, 0);
    for (int iYplane= 0; iYplane < imageHeader.DoseGridDimensionY; ++iYplane)
    {
        doseGridYvalues[iYplane] = imageHeader.DoseGridOriginY+ (static_cast<float>(imageHeader.DoseGridDimensionY) -static_cast<float>(iYplane)-1.0)*imageHeader.DoseGridVoxelSizeY;
    }

    doseGridZvalues = std::vector<float>(imageHeader.DoseGridDimensionZ, 0);
    for (int iZplane= 0; iZplane < imageHeader.DoseGridDimensionZ; ++iZplane)
    {
        doseGridZvalues[iZplane] = imageHeader.DoseGridOriginZ+ static_cast<float>(iZplane)*imageHeader.DoseGridVoxelSizeZ;
    }

    std::vector <int> SubX;
    std::vector <int> SubY;
    std::vector <int> SubZ;

    doseArrayCoordinatesX= std::vector<float>(expectedSizeVolume,0);
    doseArrayCoordinatesY= std::vector<float>(expectedSizeVolume,0);
    doseArrayCoordinatesZ= std::vector<float>(expectedSizeVolume,0);


//    xyzMeshGrid(doseGridXvalues,doseGridYvalues , doseGridZvalues, doseArrayCoordinatesX, doseArrayCoordinatesY, doseArrayCoordinatesZ);


    auto indicesColMajor= generateIntRange(0,1,expectedSizeVolume);
    ind2SubForColMajorTensor(indicesColMajor,imageHeader.DoseGridDimensionX,imageHeader.DoseGridDimensionY,imageHeader.DoseGridDimensionZ,SubX,SubY,SubZ);
    for (int iDosePoint = 0; iDosePoint < expectedSizeVolume; ++iDosePoint)
    {
        doseArrayCoordinatesX[iDosePoint]= doseGridXvalues[SubX[iDosePoint]];
        doseArrayCoordinatesY[iDosePoint]= doseGridYvalues[SubY[iDosePoint]];
        doseArrayCoordinatesZ[iDosePoint]= doseGridZvalues[SubZ[iDosePoint]];
    }


    auto result = std::max_element(doseArray.begin(), doseArray.end());  // get the max dose to verify the
    auto indexMax = std::distance(doseArray.begin(), result);
    std::cout <<  "max element at: " << indexMax << '\n';
    std::cout <<  "max dose  = " << doseArray[indexMax] <<std::endl;
    std::cout <<  "max dose happens at: x =" << doseArrayCoordinatesX[indexMax] << "  y= " << doseArrayCoordinatesY[indexMax] << " z =" << doseArrayCoordinatesZ[indexMax] << std::endl;

}

void RtDosePinn::setDoseHeaderFilePath(std::string _doseHeaderFilePath)
{
    doseHeaderFilePath= _doseHeaderFilePath;
}

void RtDosePinn::setDoseImageFilePath(std::string _doseImageFilePath)
{
    doseImageFilePath = _doseImageFilePath;
}

void RtDosePinn::setDoseImageHeaderFilePath(std::string _doseImageHeaderFilePath)
{
    //  The contend of a sample Dose header -->     autoPlan7Beam00.Dose.binary.img.header
    //    // Autocreated by saveDoseWithHeader.Script
    //    DoseGrid.units = "Dose";
    //    DoseGrid.weight= 1.000;
    //    DoseGrid.VoxelSize.X =   0.300;
    //    DoseGrid.VoxelSize.Y =   0.300;
    //    DoseGrid.VoxelSize.Z =   0.300;
    //    DoseGrid.Dimension.X =   127;
    //    DoseGrid.Dimension.Y =    95;
    //    DoseGrid.Dimension.Z =    49;
    //    DoseGrid.Origin.X =   4.500;
    //    DoseGrid.Origin.Y =  12.000;
    //    DoseGrid.Origin.Z =   5.400;
    //    DoseGrid.BinaryOrder.Endian =  0;

    doseImageHeaderFilePath = _doseImageHeaderFilePath;
}

float RtDosePinn::getMaxDose()
{
    return maxDose;
}
