#include "DicomFilesProp.h"

#include <QDir>
#include <QFileInfo>
#include <QString>

struct stat info;

DicomFilesProp::DicomFilesProp() : logstream(std::cout)
{

    verbose = false;
    if (verbose)
    {
        logstream << "DICOM Folder reader has been constructued with cout as ostream" << std::endl;
    }
}

DicomFilesProp::DicomFilesProp(std::ostream &stream) : logstream(stream)
{
    verbose = false;
    if (verbose)
    {
        logstream << "DICOM Folder reader has been constructued with user defined output stream " << std::endl;
    }
}

void DicomFilesProp::setDicomFolderPath(std::string _dicomFolderPath)
{
    dicomFolderPath = _dicomFolderPath;
}

bool DicomFilesProp::parseFolder()
{
    std::string class_member = "DicomFilesProp::parseFolder:";

    if (dicomFolderPath.empty())
    {
        logstream << class_member << "The dicom folder has not been set!" << std::endl;
        return false;
    }

    /* ****************************************************************************************************************************************/
    /* ************************************************ Check if dicomFolderPath is a folder ************************************************ */
    /* ****************************************************************************************************************************************/

    if (stat(dicomFolderPath.c_str(), &info) != 0)
    {
        logstream << class_member << "cannot access : " << dicomFolderPath << std::endl;
        return false;
    }


    else if (info.st_mode & S_IFDIR)  // S_ISDIR() doesn't exist on my windows
    {
        if (verbose)
        {
            logstream << class_member << dicomFolderPath << " ... ok . " << std::endl;
        }
    }
    else
    {
        logstream << class_member << dicomFolderPath << " is not a Folder/Directory. " << std::endl;
        return false;
    }


    /* ****************************************************************************************************************************************/
    /* **************************************** Read all the dicom files in dicomFolderPath****************************************************/
    /* ****************************************************************************************************************************************/
    // TODO : rewrite the following without QT related headers
    QDir qDirDcmFolderPath(dicomFolderPath.c_str());
    qDirDcmFolderPath.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList filesInsdeDCMFolderPath = qDirDcmFolderPath.entryList();
    filesInsdeDCMFolderPath.sort();
    QList<bool> isDcmFileFlagList;

    //    if (filesInsdeDCMFolderPath.size()<3)
    //    {
    //        logstream << class_member<<  "ERROR : At least 3 files ( CT, RT Dose, RT Structure) are needed for robustness analysis " << std::endl;
    //        return false;
    //    }
    //    else
    //    {

    for (int iFile = 0; iFile < filesInsdeDCMFolderPath.size(); ++iFile)
    {
        // logstream << "File" << iFile << ": " << filesInsdeDCMFolderPath.at(iFile).toStdString() << std::endl;




        FILE *f = NULL;
        OFBool ok = OFFalse;
        char signature[4];
        std::string fullPath = dicomFolderPath + "/" + filesInsdeDCMFolderPath.at(iFile).toStdString();
        f = fopen(fullPath.c_str(), "rb");
        if (f == 0)
        {
            ok = OFFalse;
            logstream << class_member << "ERROR : Could not open file:" << fullPath << std::endl;


        }
        else
        {
            if ((fseek(f, DCM_PreambleLen, SEEK_SET) < 0) || (fread(signature, 1, DCM_MagicLen, f) != DCM_MagicLen))
            {
                ok = OFFalse;
            }
            else if (strncmp(signature, DCM_Magic, DCM_MagicLen) != 0)
            {
                ok = OFFalse;
            }
            else
            {
                /* looks ok */
                ok = OFTrue;
            }
            fclose(f);

        }

        if (ok)
        {
            if (verbose)
            {
                logstream << "yes: " << filesInsdeDCMFolderPath.at(iFile).toStdString() << OFendl;
            }
            isDcmFileFlagList.append(true);
            dicomFilesVec.push_back(fullPath);

        }
        else
        {
            //                DcmFileFormat fileformat;
            //                OFCondition status = fileformat.loadFile(filesInsdeDCMFolderPath.at(iFile).toStdString().c_str());
            //                if (status.good())
            //                {
            //                    logstream << "yes: " << filesInsdeDCMFolderPath.at(iFile).toStdString() << OFendl;
            //                    isDcmFileFlagList.append(true);
            //                    dicomFilesVec.push_back(fullPath);
            //                }
            //                else
            //                {

            logstream << "not a DICOM File: " << filesInsdeDCMFolderPath.at(iFile).toStdString() << OFendl;
            isDcmFileFlagList.append(false);
            //                }
        }
    }
    //    }


    /* ****************************************************************************************************************************************/
    /* **************************************** Read  the patient and study IDs for all the input files*************************************************/
    /* ****************************************************************************************************************************************/
    for (int iDicomFilePath = 0; iDicomFilePath < dicomFilesVec.size(); ++iDicomFilePath)
    {
        DcmFileFormat fileformat;
        OFCondition status = fileformat.loadFile(dicomFilesVec[iDicomFilePath].c_str());
        int patientIndexInPatientVec = -1;
        int studyIndexInStudyVec = -1;
        int seriesIndexInSeriesVec = -1;
        if (status.good())
        {
            // Read Patient ID
            OFString patientID;
            if (fileformat.getDataset()->findAndGetOFString(DCM_PatientID, patientID).good())
            {
                if (verbose)
                {

                    logstream << "Patient's ID : " << patientID << std::endl;
                }
                std::string pID = patientID.c_str();
                patientsIDVec.push_back(pID);

                // Find the patient in the patient list
                if (PatientsVec.size() > 0)
                {
                    auto pred = [pID](const DicomPatient &item)
                    {
                        return item.PatientID.compare(pID) == 0;
                    };

                    auto it = std::find_if(std::begin(PatientsVec), std::end(PatientsVec), pred);

                    if (it != std::end(PatientsVec)) // if the patientID exist in PatientVec we
                    {
                        patientIndexInPatientVec = it - PatientsVec.begin();
                        if (verbose)
                        {

                            logstream << "Patient ID Exists: " << patientIndexInPatientVec << std::endl;
                        }

                    }
                    else
                    {
                        DicomPatient P1;
                        P1.PatientID = pID;
                        PatientsVec.push_back(P1);
                        patientIndexInPatientVec = PatientsVec.size() - 1;

                    }

                }
                else // first patient
                {
                    DicomPatient P1;
                    P1.PatientID = pID;
                    PatientsVec.push_back(P1);
                    patientIndexInPatientVec = 0;

                }


            }
            else
            {
                logstream << "ERROR: cannot access Patient's ID!" << std::endl;
                return false;
            }

            // Read study ID
            OFString studyId;
            if (fileformat.getDataset()->findAndGetOFString(DCM_StudyID, studyId).good())
            {
                if (verbose)
                {

                    logstream << "Study ID : " << studyId.c_str() << std::endl;
                }
                std::string studyIdTemp = studyId.c_str();
                studyIDVec.push_back(studyIdTemp);

                // Check if the Study ID exists in the PatientVec[patientIndexInPatientVec].StudyVec

                auto pred = [studyIdTemp](const DicomStudy &item)
                {
                    return item.StudyID.compare(studyIdTemp) == 0;
                };

                auto it = std::find_if(std::begin(PatientsVec[patientIndexInPatientVec].StudyVec),
                                       std::end(PatientsVec[patientIndexInPatientVec].StudyVec), pred);

                if (it !=
                    std::end(PatientsVec[patientIndexInPatientVec].StudyVec)) // if the patientID exist in PatientVec we
                {
                    studyIndexInStudyVec = it - PatientsVec[patientIndexInPatientVec].StudyVec.begin();
                    if (verbose)
                    {

                        logstream << "Study ID Exists: " << studyIndexInStudyVec << std::endl;
                    }

                }
                else
                {
                    DicomStudy S1;
                    S1.PatientID = PatientsVec[patientIndexInPatientVec].PatientID;
                    S1.StudyID = studyIdTemp;
                    PatientsVec[patientIndexInPatientVec].StudyVec.push_back(S1);
                    studyIndexInStudyVec = PatientsVec[patientIndexInPatientVec].StudyVec.size() - 1;

                }

            }
            else
            {
                logstream << "ERROR: cannot access Study ID" << std::endl;
                return false;

            }


            // Read DICOM File SeriesNumber
            OFString SeriesNumberOFString;
            if (fileformat.getDataset()->findAndGetOFString(DCM_SeriesNumber, SeriesNumberOFString).good())
            {
                if (verbose)
                {

                    logstream << "SeriesNumber : " << SeriesNumberOFString.c_str() << std::endl;
                }
                std::string seriesNumTemp = SeriesNumberOFString.c_str();
                SeriesNumberVec.push_back(seriesNumTemp);

                // check for existing Series in PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec
                auto pred = [seriesNumTemp](const DicomSeries &item)
                {
                    return item.SeriesNumber.compare(seriesNumTemp) == 0;
                };

                auto it = std::find_if(
                        std::begin(PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec),
                        std::end(PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec), pred);

                if (it != std::end(
                        PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec)) // if the patientID exist in PatientVec we
                {
                    seriesIndexInSeriesVec =
                            it - PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec.begin();
                    if (verbose)
                    {

                        logstream << "Series Number  Exists: " << seriesIndexInSeriesVec << std::endl;
                    }

                }
                else
                {
                    DicomSeries tempSeries;
                    tempSeries.PatientID = PatientsVec[patientIndexInPatientVec].PatientID;
                    tempSeries.StudyID = PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].StudyID;
                    tempSeries.SeriesNumber = seriesNumTemp;
                    PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec.push_back(
                            tempSeries);
                    seriesIndexInSeriesVec =
                            PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec.size() - 1;

                }

            }
            else
            {
                logstream << "ERROR: cannot access Series Number" << std::endl;
                return false;

            }


            // Read DICOM File modality
            OFString modalityOFString;
            if (fileformat.getDataset()->findAndGetOFString(DCM_Modality, modalityOFString).good())
            {
                if (verbose)
                {

                    logstream << "Modality : " << modalityOFString.c_str() << std::endl;
                }
                std::string modalityTemp = modalityOFString.c_str();
                modalityVec.push_back(modalityTemp);

                PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec[seriesIndexInSeriesVec].ModalityID = modalityTemp;
            }
            else
            {
                logstream << "ERROR: cannot access modality" << std::endl;
                return false;

            }


            // Add dcm file paths to the PatientsVec[i].StudyVec[j].SeriesVec[k].FilePathVec
            PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec[seriesIndexInSeriesVec].FilePathVec.push_back(
                    dicomFilesVec[iDicomFilePath]);

            QFileInfo fileInfo(dicomFilesVec[iDicomFilePath].c_str());
            QString name = fileInfo.fileName();

            PatientsVec[patientIndexInPatientVec].StudyVec[studyIndexInStudyVec].SeriesVec[seriesIndexInSeriesVec].FileNameVec.push_back(
                    name.toStdString());



            // Read DICOM File SOPInstanceUID
            OFString sOPInstanceUidOFString;
            if (fileformat.getDataset()->findAndGetOFString(DCM_SOPInstanceUID, sOPInstanceUidOFString).good())
            {
                if (verbose)
                {
                    logstream << "SOPInstanceUID : " << sOPInstanceUidOFString.c_str() << std::endl;
                }
                std::string sopInstanceUidTemp = sOPInstanceUidOFString.c_str();
                SOPInstanceUID.push_back(sopInstanceUidTemp);


            }
            else
            {
                logstream << "ERROR: cannot access SOPInstanceUID" << std::endl;
                return false;
            }



            // Read DICOM File SOPClassUID
            OFString SOPClassUidOFString;
            if (fileformat.getDataset()->findAndGetOFString(DCM_SOPClassUID, SOPClassUidOFString).good())
            {
                if (verbose)
                {
                    logstream << "SOPClassUID : " << SOPClassUidOFString.c_str() << std::endl;
                }
                std::string SOPClassUidTemp = SOPClassUidOFString.c_str();
                SOPClassUID.push_back(SOPClassUidTemp);


            }
            else
            {
                logstream << "ERROR: cannot access SOPClassUID" << std::endl;
                return false;

            }


            // Read DICOM File StudyInstanceUID
            OFString StudyInstanceUidOFString;
            if (fileformat.getDataset()->findAndGetOFString(DCM_StudyInstanceUID, StudyInstanceUidOFString).good())
            {
                if (verbose)
                {
                    logstream << "StudyInstanceUID : " << StudyInstanceUidOFString.c_str() << std::endl;
                }
                std::string StudyInstanceUidTemp = StudyInstanceUidOFString.c_str();
                StudyInstanceUID.push_back(StudyInstanceUidTemp);


            }
            else
            {
                logstream << "ERROR: cannot access StudyInstanceUID" << std::endl;
                return false;

            }


            //  logstream <<  "modalityOFString compared with CT :" << modalityOFString.compare("CT") << std::endl;
            // logstream <<  "modalityOFString compared with RTSTRUCT :" << modalityOFString.compare("RTSTRUCT") << std::endl;
            // logstream <<  "modalityOFString compared with 'RTDOSE' :" << modalityOFString.compare("RTDOSE") << std::endl;


            // FrameOfReferenceUID in RTSTRUCT is found inside the ReferencedFrameOfReferenceSequence
            // ReferencedFrameOfReferenceSequence[0].FrameOfReferenceUID
            if (modalityOFString.compare("RTSTRUCT") == 0)
            {
                if (verbose)
                {
                    logstream << "Read FrameOfReferenceUID For Structure : " << std::endl;
                }
                // Read DICOM File FrameOfReferenceUID
                DRTStructureSetIOD rtstructIOD;
                //DcmFileFormat fileFormatStructure;
                //const char * rtStructFileName;
                //OFFilename rtStructOFFileName(rtStructFileName);
                //statusRTStructureSetIOD = fileFormatStructure.loadFile(rtStructOFFileName);
                //OFCondition statusRTStructureSetIOD =  fileFormatStructure.loadFile(rtStructOFFileName);
                DRTReferencedFrameOfReferenceSequence refFrameofRefSequence;
                DRTReferencedFrameOfReferenceSequence::Item *refFrameofRefSequenceItem;
                OFCondition statusRTStructureSetIOD = rtstructIOD.read(*fileformat.getDataset());
                OFString FrameOfReferenceUIDOFString;

                if (statusRTStructureSetIOD.good())
                {
                    refFrameofRefSequence = rtstructIOD.getReferencedFrameOfReferenceSequence();
                    Uint16 num_items = refFrameofRefSequence.getNumberOfItems();

                    refFrameofRefSequence.getItem(0, refFrameofRefSequenceItem);
                    refFrameofRefSequenceItem->getFrameOfReferenceUID(FrameOfReferenceUIDOFString);
                    if (verbose)
                    {

                        logstream << "FrameOfReferenceUID : " << FrameOfReferenceUIDOFString.c_str() << std::endl;
                    }
                    std::string FrameOfReferenceUidTemp = FrameOfReferenceUIDOFString.c_str();
                    FrameOfReferenceUID.push_back(FrameOfReferenceUidTemp);


                }

                //                //ReferencedFrameOfReferenceSequence.Item_1.FrameOfReferenceUID
                //                if (fileformat.getDataset()->findAndGetOFString(DCM_ReferencedFrameOfReferenceSequence, FrameOfReferenceUIDOFString).good())
                //                {
                //                    logstream <<  "FrameOfReferenceUID : " << FrameOfReferenceUIDOFString.c_str() << std::endl;
                //                    std::string FrameOfReferenceUidTemp= FrameOfReferenceUIDOFString.c_str();
                //                    FrameOfReferenceUID.push_back(FrameOfReferenceUidTemp);

                //                }
                //                else
                //                {
                //                    logstream  <<  "ERROR: cannot access FrameOfReferenceUID" << std::endl;
                //                }

            }

            if (modalityOFString.compare("RTDOSE") == 0 | modalityOFString.compare("CT") == 0)
            {
                // Read DICOM File FrameOfReferenceUID
                OFString FrameOfReferenceUIDOFString;
                if (fileformat.getDataset()->findAndGetOFString(DCM_FrameOfReferenceUID,
                                                                FrameOfReferenceUIDOFString).good())
                {
                    if (verbose)
                    {

                        logstream << "FrameOfReferenceUID : " << FrameOfReferenceUIDOFString.c_str() << std::endl;
                    }
                    std::string FrameOfReferenceUidTemp = FrameOfReferenceUIDOFString.c_str();
                    FrameOfReferenceUID.push_back(FrameOfReferenceUidTemp);

                }
                else
                {
                    logstream << "ERROR: cannot access FrameOfReferenceUID" << std::endl;
                    return false;

                }
            }


        }
        else
        {
            logstream << "Error: cannot read DICOM file (" << status.text() << ")" << std::endl;
            return false;
        }
    }



    /* ****************************************************************************************************************************************/
    /* **************************************** Read  the patient IDs for all the input files*************************************************/
    /* ****************************************************************************************************************************************/
    //
//    for (int iPatinet = 0; iPatinet < PatientsVec.size(); ++iPatinet)
//    {
//     PatientsVec[iPatinet].

//    }


    return true;

}

void DicomFilesProp::printParsedFolderInfo()
{


    // if Patient file exist then this is a pinncle patient folder otherwise, we need to check if there is any dicom file
    //    if not, we notify user

    for (DicomPatient &patient : this->PatientsVec)
    {
        logstream << "PatientID = " << patient.PatientID << std::endl;
        for (DicomStudy &study :patient.StudyVec)
        {
            logstream << "\t StudyID = " << study.StudyID << std::endl;
            for (DicomSeries &series :study.SeriesVec)
            {
                logstream << "\t \t  SeriesNumber = " << series.SeriesNumber << std::endl;
                logstream << "\t \t  ModalityID = " << series.ModalityID << std::endl;
                for (std::string &fileName : series.FileNameVec)
                {
                    logstream << "\t \t \t fileName = " << fileName << std::endl;
                }
            }

        }

    }
}






