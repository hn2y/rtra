/**
 *  @file    PinnacleFiles.cpp
 *  @author  Hamidreza Nourzadeh
 *  @date    10/09/2016
 *  @version 1.0
 *
 *  @brief This code is esstially a wrapper for modified/extended version of some functions in JVS libpin.
 *
 *
 *  @section DESCRIPTION
 *
 *
 */
#include "PinnacleFiles.h"
#include "someHelpers.h"
#include <QColor>

#include "ITKUtils.h"
//inline bool fileExists(const std::string& name) {
//    if (FILE *file = fopen(name.c_str(), "r")) {
//        fclose(file);
//        return true;
//    } else {
//        return false;
//    }
//}



PinnacleFiles::PinnacleFiles()
{
    numRois = 0;
    patientFilePath.clear();
    planDotTrailFilePath.clear();
    planDotRoiFilePath.clear();
    Patient.PatientID = -1; // Used to check if the patient is loaded
    Patient.isStartWithDICOM = false;
    roisPropVector.clear();
    rtDosePinn = NULL;
    verbose = false;
}

PinnacleFiles::~PinnacleFiles()
{


}

//TODO : get stream as an input
void PinnacleFiles::usage()
{
    std::cout << "\n usage: TestIt( string nameOfPlan.TrialFile,string nameOfTrialToUseInTesting) " << std::endl;

    return;
}

void PinnacleFiles::setVebose(bool _verbose)
{
    verbose = _verbose;
}

int PinnacleFiles::closestNumberIndexInVectorLessThanOrEqualToValue(const std::vector<float> &vec, float value)
{


    auto it = std::min_element(vec.begin(), vec.end(), [=](float x, float y)
    {
        return (std::abs(x - value) <= std::abs(y - value)) & (x <= value);
    });


    if (it == vec.end())
    { return -1; }

    return std::distance(vec.begin(), it);
}

int PinnacleFiles::closestNumberIndexInVectorGreaterThanOrEqualToValue(const std::vector<float> &vec, float value)
{

    auto it = std::min_element(vec.begin(), vec.end(), [=](float x, float y)
    {
        return (std::abs(x - value) <= std::abs(y - value)) & (x >= value);
    });


    return std::distance(vec.begin(), it);
}


//TODO: rewrite it properly
int PinnacleFiles::ReadTrialList(std::string nameOfTrialToUseInTesting)
{
    //    int debugLevel=0;
    //    //    if( OK != get_option("-d",&argc,argv,&debugLevel)) {
    //    //      std::cout <<"\n ERROR: getting -d command line option" << std::endl;
    //    //      usage(); exit(FAIL);
    //    //    }


    //    //    char filename[MAX_STR_LEN]; filename[0]=0;
    //    //    if( OK != get_option("-i",&argc,argv,filename) || 0 == filename[0]) {
    //    //      printf("\n ERROR: getting -i command line option");
    //    //      usage(); exit(FAIL);
    //    //    }
    //    //char myTrialName[MAX_STR_LEN]; myTrialName[0]=0;


    //    const char *filename = planDotTrailFilePath.c_str();

    //    const char *myTrialName = nameOfTrialToUseInTesting.c_str();

    //    //   if( OK != get_option("-t",&argc,argv,myTrialName) || 0 == myTrialName[0] ) {
    //    //      printf("\n ERROR: getting -t command line option");
    //    //      usage(); exit(FAIL);
    //    //    }
    //    //    // Open file
    //    FILE *istrm = fopen(filename,"r");
    //    if(istrm == NULL){
    //        printf("\n ERROR: Opening File %s\n", filename); return(FAIL);
    //    }

    //    if (OclifObjectTypePlanDotTrail==NULL)
    //    {
    //        OclifObjectTypePlanDotTrail = new(clifObjectType);
    //    }

    //    if(OclifObjectTypePlanDotTrail == NULL){
    //        printf("\n ERROR: Allocating Memory for clifObjectType\n"); return(FAIL);
    //    }

    //    // Use this syntax if you want to read in all Trials
    //    if( readClifFile(istrm, OclifObjectTypePlanDotTrail,"TrialList") != OK){
    //        printf("\n ERROR: Reading clif from file %s\n", filename); return(FAIL);
    //    }
    //    fclose(istrm);
    //    //    if(verbose)
    //    //    {
    //    //        dumpClifStructure(OclifObjectTypePlanDotTrail);
    //    //    }

    //    {
    //        char PinnVersion[MAX_STR_LEN];
    //        char commandString[MAX_STR_LEN];
    //        sprintf(commandString,"%s.ObjectVersion.WriteVersion",myTrialName);
    //        if(readStringClifStructure(OclifObjectTypePlanDotTrail, commandString,PinnVersion)!= OK ){
    //            printf("\n ERROR: Getting Pinnacle Version from TrialList\n"); return(FAIL);
    //        }
    //        printf("\n Pinnacle Version is %s\n", PinnVersion);
    //    }
    //    char *Value=NULL;
    //    // access clif values from the structure
    //    char commandString[MAX_STR_LEN];
    //    sprintf(commandString,"%s.PrescriptionList.Prescription.Name",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString ,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.PrescriptionList.Prescription_1.Name",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.PrescriptionList.Prescription_1.Method",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString, &Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.PrescriptionList.#\"#0\".Method",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);

    //    printf("\n -----------Number of objects in the Beam list----------");
    //    // Borrowed from libpin/readBeams.cc
    //    {
    //        printf("\n\t Method 1: Access the BeamList clif and count the beams like in readBeams.cc");
    //        clifObjectType *BeamList;
    //        sprintf(commandString,"%s.BeamList",myTrialName);
    //        if(getClifAddress(OclifObjectTypePlanDotTrail,commandString, &BeamList)!= OK){
    //            printf("\n ERROR: Getting Address for BeamList"); return(FAIL);
    //        }
    //        if(checkIfObjectList(BeamList) != OK){
    //            printf("\n ERROR: Invalid Object List"); return(FAIL);
    //        }
    //        int nBeams = BeamList->nObjects;
    //        printf("\n\t\t nBeams = %d",nBeams);
    //    }
    //    printf("\n\t Method 2: Using getClifNObjects");
    //    sprintf(commandString,"%s.BeamList",myTrialName);
    //    printf ("\n\t\t nBeams = %d",getClifNObjects(OclifObjectTypePlanDotTrail,commandString));
    //    printf("\n\t Method 3: Using .Count");
    //    sprintf(commandString,"%s.BeamList.Count",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("\n\t\t%s = %s\n...\n",commandString, Value);

    //    //
    //    sprintf(commandString,"%s.BeamList.#\"#0\".Name",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.BeamList.#\"#0\".CPManager.CPManagerObject.NumberOfControlPoints",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    //
    //    sprintf(commandString,"%s.BeamList.#\"#0\".CPManager.CPManagerObject.ControlPointList.#0.Gantry",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.BeamList.#\"#0\".CPManager.CPManagerObject.ControlPointList.#0.Collimator",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    sprintf(commandString,"%s.BeamList.#\"#0\".CPManager.CPManagerObject.ControlPointList.#0.LeftJawPosition",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);



    //    /*
    //    float *Array;
    //    int nArray=0;
    //    printf("\n Going To Read The Array\n");
    //    if(readClifArrayStructure(OclifObjectTypePlanDotTrail, "Trial.BeamList.#\"#0\".MLCLeafPositions.RawData.Points",&nArray,&Array)!=OK){
    //      printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("\n %d Values found in Array", nArray);
    //    for(int i=0; i< nArray; i++) printf("\n %d %f",i, Array[i]);

    //    */
    //    // free the structure
    //    if(freeClifStructure(OclifObjectTypePlanDotTrail) != OK){

    //        printf("\n ERROR: Freeing memory for clif object");
    //    }
    //    delete(OclifObjectTypePlanDotTrail);
    //    OclifObjectTypePlanDotTrail=NULL;

    //    /* Done Dealing with All Trials */
    //    /* ------------------------------------------------------------------------ */
    //    /* Use this format if you want to read in a single trial */
    //    istrm = fopen(filename,"r");
    //    if(istrm == NULL){
    //        printf("\n ERROR: Opening File %s\n", filename); return(FAIL);
    //    }

    //    if (OclifObjectTypePlanDotTrail==NULL)
    //    {
    //        OclifObjectTypePlanDotTrail = new(clifObjectType);
    //    }
    //    if(OclifObjectTypePlanDotTrail == NULL){
    //        printf("\n ERROR:  Allocating Memory for clifObjectType\n"); return(FAIL);
    //    }

    //    if( readSpecificClif(istrm, OclifObjectTypePlanDotTrail, "Trial", myTrialName) != OK){
    //        printf("\n ERROR: Reading clif from file %s\n", filename); return(FAIL);
    //    }
    //    fclose(istrm);
    //    printf("\n-------------\n");
    //    if(verbose) dumpClifStructure(OclifObjectTypePlanDotTrail);
    //    printf("\n-------------\n");

    //    Value=NULL;
    //    // access clif Values from the structure
    //    sprintf(commandString,"PrescriptionList.Prescription.Name",myTrialName);
    //    if(readClifStructure(OclifObjectTypePlanDotTrail, commandString,&Value)!=OK){
    //        printf("\n ERROR: Reading Clif Structure\n"); return(FAIL);
    //    }
    //    printf("%s = %s\n...\n",commandString, Value);
    //    // free the structure
    //    if(freeClifStructure(OclifObjectTypePlanDotTrail) != OK){
    //        printf("\n ERROR: Freeing memory for clif object");
    //    }
    //    delete(OclifObjectTypePlanDotTrail);

    //    printf("\n Normal Termination\n");
    return (OKFlag);

}


int PinnacleFiles::readPatientFileAndImageSets()
{
    const char *patientFilePathChar = patientFilePath.c_str();
    FILE *istrm = fopen(patientFilePathChar, "r");
    if (istrm == NULL)
    {
        // printf("\n ERROR: Opening File %s\n", patientFilePathChar); return(FAIL);
        std::cout << " ERROR: Opening File  : " << patientFilePathChar << std::endl;
        return (FAIL);
    } else
    {
        std::cout << " Opening File ... OK " << std::endl;
    }


    int returnValue = read_pinnacle_patient_full_prop((char *) patientFilePathChar, Patient);

    if (OKFlag != returnValue)
    {
        fprintf(stderr, "PinnacleFiles::readPatientFile. Return: %d  ", returnValue);
        return (FAIL);
    }
    if (verbose)
    {
        for (int iImageSet = 0; iImageSet < Patient.ImageSetList.size(); ++iImageSet)
        {
            std::cout << "ImageSet " << std::to_string(iImageSet) << " -------> " << "  ImageSetID = "
                      << std::to_string(Patient.ImageSetList[iImageSet].ImageSetID) << std::endl;
            std::cout << "  \t -------> " << "  NumberOfImages = "
                      << std::to_string(Patient.ImageSetList[iImageSet].NumberOfImages) << std::endl;
            std::cout << "  \t -------> " << "  ImageName = " << Patient.ImageSetList[iImageSet].ImageName << std::endl;
            std::cout << "  \t -------> " << "  NameFromScanner = " << Patient.ImageSetList[iImageSet].NameFromScanner
                      << std::endl;
        }

        for (int iPlan = 0; iPlan < Patient.PlanList.size(); ++iPlan)
        {
            std::cout << "PlanID = " << std::to_string(Patient.PlanList[iPlan].PlanID) << std::endl;

        }
    }


    return (OKFlag);

}

void PinnacleFiles::setIsStartWithDicom(bool _IsStartWithDicom)
{
    Patient.isStartWithDICOM = _IsStartWithDicom;
}


int PinnacleFiles::getRoiNamesVector(std::vector<std::string> &roiNamesVector)
{

    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }
    numRois = roisPropVector.size();
    roiNamesVector.clear();

    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        roiNamesVector.push_back(roisPropVector[iRoi].name);
        if (verbose)
        {
            std::cout << "roiNamesVector[" << std::to_string(iRoi) << "] = " << roiNamesVector[iRoi] << std::endl;
        }
    }
    return OKFlag;
}


int PinnacleFiles::readRoisProperties()
{
    const char *planDotRoiFilePathChar = planDotRoiFilePath.c_str();

    int returnValue = scan_pinnacle_roi_full_prop((char *) planDotRoiFilePathChar, roisPropVector, &numRois, 999);

    if (OKFlag != returnValue)
    {
        fprintf(stderr,
                "PinnacleFiles::getRoisProperties to scan rois, or empty set detected. Return: %d  nfound: %d\n",
                returnValue, numRois);
        return (FAIL);
    } else if (0 == numRois)
    {
        fprintf(stderr, "WARNING:PinnacleFiles::getRoisProperties did not detect any ROIs\n");
    }


    for (int iRoi = 0; iRoi < numRois; ++iRoi)
    {
        //Fill in Display COLOR   TODO: Consider moving it to scan_pinnacle_roi_full_prop without QColor
        const QString colorNameQString = QString(roisPropVector[iRoi].color.c_str());
        QColor color(colorNameQString);
        roisPropVector[iRoi].DisplayColor = std::vector<unsigned int>(3, 0);
        roisPropVector[iRoi].DisplayColor[0] = color.red();
        roisPropVector[iRoi].DisplayColor[1] = color.green();
        roisPropVector[iRoi].DisplayColor[2] = color.blue();


        if (verbose)
        {
            //            std::cout << "roiNamesVector[" << std::to_string(iRoi) << "] = " << roiNamesVector[iRoi] << std::endl;
            std::cout << "roisPropVector[" << std::to_string(iRoi) << "].name = " << roisPropVector[iRoi].name
                      << std::endl;
            std::cout << "\t \t \t .volume_name = " << roisPropVector[iRoi].volume_name << std::endl;
            std::cout << "\t \t \t .stats_volume_name = " << roisPropVector[iRoi].stats_volume_name << std::endl;
            std::cout << "\t \t \t .author = " << roisPropVector[iRoi].author << std::endl;
            std::cout << "\t \t \t .organ_name = " << roisPropVector[iRoi].organ_name << std::endl;
            std::cout << "\t \t \t .flags = " << roisPropVector[iRoi].flags << std::endl;
            std::cout << "\t \t \t .roiinterpretedtype = " << roisPropVector[iRoi].roiinterpretedtype << std::endl;
            std::cout << "\t \t \t .color = " << roisPropVector[iRoi].color << std::endl;
            std::cout << "\t \t \t .curve_min_area = " << roisPropVector[iRoi].curve_min_area << std::endl;
            std::cout << "\t \t \t .curve_overlap_min = " << roisPropVector[iRoi].curve_overlap_min << std::endl;
            std::cout << "\t \t \t .lower = " << roisPropVector[iRoi].lower << std::endl;
            std::cout << "\t \t \t .upper = " << roisPropVector[iRoi].upper << std::endl;
            std::cout << "\t \t \t .radius = " << roisPropVector[iRoi].radius << std::endl;
            std::cout << "\t \t \t .density = " << roisPropVector[iRoi].density << std::endl;
            std::cout << "\t \t \t .density_units = " << roisPropVector[iRoi].density_units << std::endl;
            std::cout << "\t \t \t .override_data = " << roisPropVector[iRoi].override_data << std::endl;
            std::cout << "\t \t \t .override_order = " << roisPropVector[iRoi].override_order << std::endl;
            std::cout << "\t \t \t .override_material = " << roisPropVector[iRoi].override_material << std::endl;
            std::cout << "\t \t \t .material = " << roisPropVector[iRoi].material << std::endl;
            std::cout << "\t \t \t .invert_density_loading = " << roisPropVector[iRoi].invert_density_loading
                      << std::endl;
            std::cout << "\t \t \t .volume = " << roisPropVector[iRoi].volume << std::endl;
            std::cout << "\t \t \t .pixel_min = " << roisPropVector[iRoi].pixel_min << std::endl;
            std::cout << "\t \t \t .pixel_max = " << roisPropVector[iRoi].pixel_max << std::endl;
            std::cout << "\t \t \t .pixel_mean = " << roisPropVector[iRoi].pixel_mean << std::endl;
            std::cout << "\t \t \t .pixel_std = " << roisPropVector[iRoi].pixel_std << std::endl;
            std::cout << "\t \t \t .bBEVDRROutline = " << roisPropVector[iRoi].bBEVDRROutline << std::endl;
            std::cout << "\t \t \t .display_on_other_vols = " << roisPropVector[iRoi].display_on_other_vols
                      << std::endl;
            std::cout << "\t \t \t .is_linked = " << roisPropVector[iRoi].is_linked << std::endl;
            std::cout << "\t \t \t .auto_update_contours = " << roisPropVector[iRoi].auto_update_contours << std::endl;
            std::cout << "\t \t \t .UID = " << roisPropVector[iRoi].UID << std::endl;
            std::cout << "\t \t \t .stoppingpower = " << roisPropVector[iRoi].stoppingpower << std::endl;
            std::cout << "\t \t \t .ctnumber = " << roisPropVector[iRoi].ctnumber << std::endl;
            std::cout << "\t \t \t .is_created_by_atlasseg = " << roisPropVector[iRoi].is_created_by_atlasseg
                      << std::endl;
            std::cout << "\t \t \t .is_warped = " << roisPropVector[iRoi].is_warped << std::endl;
            std::cout << "\t \t \t .n_curves = " << roisPropVector[iRoi].n_curves << std::endl;

        }
    }

    return OKFlag;
}


int PinnacleFiles::readRoiDataForGivenRoiName(std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }

    if (OKFlag !=
        read_pinnacle_roi_data_for_given_roiName((char *) planDotRoiFilePath.c_str(), ROIName, roisPropVector))
    {
        return FAIL;
    }

    return OKFlag;
}


int PinnacleFiles::readRoiDataForAllRoisOnGivenImageSetName(std::string ImageSetName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForAllRoisOnGivenImageSetName -->  roisPropVector size is 0"
                  << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }
    char *stringToFind = (char *) ImageSetName.c_str();
    std::vector<roi_prop> matchingProps;
    std::copy_if(roisPropVector.begin(), roisPropVector.end(), std::back_inserter(matchingProps),
                 [&stringToFind](const roi_prop &rp)
                 { return 0 == strcmp(rp.volume_name.c_str(), stringToFind); });

    for (int iMatchingRoi = 0; iMatchingRoi < matchingProps.size(); ++iMatchingRoi)
    {
        std::cout << matchingProps[iMatchingRoi].name << std::endl;
        if (OKFlag != readRoiDataForGivenRoiName(matchingProps[iMatchingRoi].name))
        {
            return FAIL;
        }
    }


    return OKFlag;
}

int PinnacleFiles::deduceRoiTypeFromName()
{

    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::decudeRoiTypeFromName -->  roisPropVector size is 0" << std::endl;
        return FAIL;
    }
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        roisPropVector[iRoi].IsTargetVolume = false;
        std::size_t foundPTV = roisPropVector[iRoi].name.find("PTV");
        std::size_t foundCTV = roisPropVector[iRoi].name.find("CTV");
        std::size_t foundGTV = roisPropVector[iRoi].name.find("GTV");
        std::size_t foundMinus1 = roisPropVector[iRoi].name.find("-");
        std::size_t foundMinus2 = roisPropVector[iRoi].name.find("Minus");
        if ((foundPTV != std::string::npos) | (foundGTV != std::string::npos) | (foundCTV != std::string::npos))
        {
            // either GTV or PTV is found  make sure there is no minus in the name
            if ((foundMinus1 == std::string::npos) | (foundMinus2 == std::string::npos))
            {
                roisPropVector[iRoi].IsTargetVolume = true;
            }
        }
    }
    return OKFlag;
}

int PinnacleFiles::calculateCenterOfMassForGivenRoiName(std::string ROIName)
{

    std::string class_member = "PinnacleFiles::calculateCOM:";

    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }

    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }

    if (verbose)
    {
        std::cout << class_member << " ROI's Center of Mass (COM) is calculated based on binary Mask" << std::endl;
    }
    if (!roisPropVector[indexRoi].isBitmapComputed)
    {
        std::cout << "ERROR: Bitmap is not computed for this roi: " << ROIName << std::endl;
        return FAIL;
    }


    if (roisPropVector[indexRoi].binaryBitmapPointCoordinatesX.size() == 0 |
        roisPropVector[indexRoi].binaryBitmapPointCoordinatesY.size() == 0 |
        roisPropVector[indexRoi].binaryBitmapPointCoordinatesZ.size() == 0)
    {
        std::cout << class_member << " ERROR --> inPolygonPoints vectors are empty. Forgot to run generatePointCloud?"
                  << std::endl;
        return false;
    }

    roisPropVector[indexRoi].RoiCOM.x = 0;
    roisPropVector[indexRoi].RoiCOM.y = 0;
    roisPropVector[indexRoi].RoiCOM.z = 0;
    for (int iPts = 0; iPts < roisPropVector[indexRoi].binaryBitmapPointCoordinatesX.size(); ++iPts)
    {
        roisPropVector[indexRoi].RoiCOM.x =
                roisPropVector[indexRoi].RoiCOM.x + roisPropVector[indexRoi].binaryBitmapPointCoordinatesX[iPts];
        roisPropVector[indexRoi].RoiCOM.y =
                roisPropVector[indexRoi].RoiCOM.y + roisPropVector[indexRoi].binaryBitmapPointCoordinatesY[iPts];
        roisPropVector[indexRoi].RoiCOM.z =
                roisPropVector[indexRoi].RoiCOM.z + roisPropVector[indexRoi].binaryBitmapPointCoordinatesZ[iPts];
    }
    roisPropVector[indexRoi].RoiCOM.x =
            roisPropVector[indexRoi].RoiCOM.x / roisPropVector[indexRoi].binaryBitmapPointCoordinatesX.size();
    roisPropVector[indexRoi].RoiCOM.y =
            roisPropVector[indexRoi].RoiCOM.y / roisPropVector[indexRoi].binaryBitmapPointCoordinatesY.size();
    roisPropVector[indexRoi].RoiCOM.z =
            roisPropVector[indexRoi].RoiCOM.z / roisPropVector[indexRoi].binaryBitmapPointCoordinatesZ.size();
    //


    return OKFlag;
}

std::vector<Point_2d_float> PinnacleFiles::xyMeshGrid(std::vector<float> &xGridPts, std::vector<float> &yGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    Point_2d_float P;
    P.x = 0.0;
    P.y = 0.0;

    std::vector<Point_2d_float> outputPoint(sizeX * sizeY, P);
    for (int i = 0; i < sizeX; i++)
    {
        for (int j = 0; j < sizeY; j++)
        {
            //X[i*sizeY + j] = xGridPts[i];
            outputPoint[i * sizeY + j].x = xGridPts[i];
            //Y[i*sizeY + j] = yGridPts[j];
            outputPoint[i * sizeY + j].y = yGridPts[j];
        }
    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;
}


template<typename Func>
std::vector<unsigned int> PinnacleFiles::findSatisfiyingIndices(std::vector<unsigned int> &v, Func f)
{
    std::vector<unsigned int> results;

    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end())
    {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

template<typename Func>
std::vector<int> PinnacleFiles::findSatisfiyingIndices(std::vector<int> &v, Func f)
{
    std::vector<int> results;

    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end())
    {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

template<typename Func>
std::vector<int> PinnacleFiles::findSatisfiyingIndices(std::vector<float> &v, Func f)
{
    std::vector<int> results;

    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end())
    {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

template<typename Func>
std::vector<int> PinnacleFiles::findSatisfiyingIndices(std::vector<double> &v, Func f)
{
    std::vector<int> results;
    auto it = std::find_if(v.begin(), v.end(), f);
    while (it != v.end())
    {
        results.emplace_back(std::distance(std::begin(v), it));
        it = std::find_if(std::next(it), std::end(v), f);
    }
    return results;
}

std::vector<Point_3d_float>
PinnacleFiles::xyzMeshGrid(std::vector<float> &xGridPts, std::vector<float> &yGridPts, std::vector<float> &zGridPts)
{
    auto sizeX = xGridPts.size();
    auto sizeY = yGridPts.size();
    auto sizeZ = zGridPts.size();
    //std::vector<double> X(sizeX*sizeY, 0.0);
    //std::vector<double> Y(sizeX*sizeY, 0.0);
    Point_3d_float P;
    P.x = 0.0;
    P.y = 0.0;
    P.z = 0.0;


    std::vector<Point_3d_float> outputPoint(sizeX * sizeY * sizeZ, P);
    for (int k = 0; k < sizeZ; k++)
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                //X[i*sizeY + j] = xGridPts[i];
                outputPoint[k * sizeX * sizeY + i * sizeY + j].x = xGridPts[i];
                //Y[i*sizeY + j] = yGridPts[j];
                outputPoint[k * sizeX * sizeY + i * sizeY + j].y = yGridPts[j];
                //Z[i*sizeY + j] = ZGridPts[k];
                outputPoint[k * sizeX * sizeY + i * sizeY + j].z = zGridPts[k];
            }
        }

    }
    //return make_pair(X,Y);
    // http://www.cplusplus.com/reference/utility/make_pair/
    return outputPoint;

}

int PinnacleFiles::createBitmapForGivenRoiName(std::string ROIName)
{

    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }


    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }

    if (-1 == Patient.PatientID)
    {
        std::cout << "ERROR: Patient File is not loaded: " << std::endl;
        return FAIL;
    }

    if (Patient.ImageSetList.size() == 0)
    {
        std::cout << "ERROR: No ImageSet found in Patient(PatientID,Name,LastName) " << Patient.PatientID << ","
                  << Patient.FirstName << "," << Patient.LastName << ")" << std::endl;
        return FAIL;
    }


    int indexImageSet = -1;
    for (int iImageSet = 0; iImageSet < Patient.ImageSetList.size(); ++iImageSet)
    {
        if (roisPropVector[indexRoi].volume_name.compare(Patient.ImageSetList[iImageSet].imageHeader.db_name) == 0)
        {
            indexImageSet = iImageSet;
            break;
        }

    }

    if (-1 == indexImageSet)
    {
        std::cout << "ERROR: ImageSet name in ROI:" << roisPropVector[indexRoi].name
                  << " do not match the ImageSet names in Patient:" << Patient.PatientID << std::endl;
        return FAIL;
    }
    // update corresponding imageSet index
    roisPropVector[indexRoi].indexImageSet = indexImageSet;


    // 2) Check if the ImageData (volumeData) is loaded if not attempt to load it

    if (!Patient.ImageSetList[indexImageSet].isVolumeDataLoaded)
    {
        std::cout << "ImageSet volume data is not loaded attempting to load...." << std::endl;
        if (OKFlag != read_pinnacle_volume(Patient.ImageSetList[indexImageSet]))
        {
            std::cout << "ERROR in reading data volume for  imageset " << indexImageSet << std::endl;
            return FAIL;
        }
    }

    // 3) initialize the ROI binary mask with a array of size CT image set
    roisPropVector[indexRoi].binaryBitmap = std::vector<bool>(Patient.ImageSetList[indexImageSet].imageHeader.x_dim *
                                                              Patient.ImageSetList[indexImageSet].imageHeader.y_dim *
                                                              Patient.ImageSetList[indexImageSet].imageHeader.z_dim);

    // 4) for a given  image plane form CT imageSet
    //   a- check if CT z-values are inside roi bounding box, if not skip the binary volume with the same z-values
    //     these  z values which contain ROI's  z values --> volumeZGridDataVectorInCm[indexStartZ:indexEndZ]
    // TODO : zvalues are not matching for a test patient
    int indexStartZ = closestNumberIndexInVectorLessThanOrEqualToValue(
            Patient.ImageSetList[indexImageSet].volumeZGridDataVectorInCm,
            roisPropVector[indexRoi].boundingCuboid.zMin + 0.002);  //0.001 to ignore small differences
    int indexEndZ = closestNumberIndexInVectorGreaterThanOrEqualToValue(
            Patient.ImageSetList[indexImageSet].volumeZGridDataVectorInCm,
            roisPropVector[indexRoi].boundingCuboid.zMax - 0.002); //0.001 to ignore small differences
    if (indexStartZ > indexEndZ)
    {
        std::cout
                << "ERROR: The z-values are not monotonically increasing! Check this volume this should not normally happen for a Pinnacle Volume"
                << std::endl;
        return FAIL;
    }


    roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.clear();
    roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.clear();
    roisPropVector[indexRoi].binaryBitmapPointIndicesX.clear();
    roisPropVector[indexRoi].binaryBitmapPointIndicesY.clear();
    roisPropVector[indexRoi].binaryBitmapPointIndicesZ.clear();
    roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.clear();

    for (int iZ = indexStartZ; iZ < indexEndZ + 1; ++iZ)
    {

        // find ROI's curves indices at this  z-value
        float CtZvalue = Patient.ImageSetList[indexImageSet].volumeZGridDataVectorInCm[iZ];
        std::vector<int> matchingCurveIndices = findSatisfiyingIndices(roisPropVector[indexRoi].curveZLocations,
                                                                       [CtZvalue](float curveZvalues)
                                                                       {
                                                                           return (std::abs(curveZvalues - CtZvalue) <=
                                                                                   0.003);
                                                                       }); // Notice a small error between z-values is acceptable


        if (0 == matchingCurveIndices.size())
        {
            if (verbose)
            {
                std::cout << "Could not found a curve  at this "
                          << Patient.ImageSetList[indexImageSet].volumeZGridDataVectorInCm[iZ]
                          << " binary bitmap z-location " << std::endl;
            }
            continue;
        }


        for (unsigned int iC = 0; iC < matchingCurveIndices.size(); ++iC)
        {

            // For each ROI curve -->  make a meshgrid within roi_curve_type.maxX , roi_curve_type.minX , roi_curve_type.maxY , roi_curve_type.minY
            //    int indexStartX =closestNumberIndexInVectorLessThanOrEqualToValue(Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm,roisPropVector[indexRoi].boundingCuboid.xMin);
            //    int indexEndX =closestNumberIndexInVectorGreaterThanOrEqualToValue(Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm,roisPropVector[indexRoi].boundingCuboid.xMax);

            int indexStartXForCurve = closestNumberIndexInVectorLessThanOrEqualToValue(
                    Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm,
                    roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].minX);
            int indexEndXForCurve = closestNumberIndexInVectorGreaterThanOrEqualToValue(
                    Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm,
                    roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].maxX);


            int indexEndYForCurve = closestNumberIndexInVectorLessThanOrEqualToValue(
                    Patient.ImageSetList[indexImageSet].volumeYGridDataVectorInCm,
                    roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].minY);
            int indexStartYForCurve = closestNumberIndexInVectorGreaterThanOrEqualToValue(
                    Patient.ImageSetList[indexImageSet].volumeYGridDataVectorInCm,
                    roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].maxY);

            std::vector<float> xGridValsForCurve;
            std::vector<float> yGridValsForCurve;
            for (int indX = indexStartXForCurve; indX < indexEndXForCurve + 1; ++indX)
            {
                xGridValsForCurve.push_back(Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm[indX]);
            }
            if ((indexStartYForCurve > indexEndYForCurve) & iC == 0)
            {
                std::cout << "indexStartYForCurve< indexEndYForCurve " << " for: " << roisPropVector[indexRoi].name
                          << std::endl;
                return FAIL;
            }
            for (int indY = indexStartYForCurve; indY < indexEndYForCurve + 1; ++indY)
            {
                yGridValsForCurve.push_back(Patient.ImageSetList[indexImageSet].volumeYGridDataVectorInCm[indY]);
            }


            std::vector<Point_2d_float> gridPointsToInOutCheck = xyMeshGrid(xGridValsForCurve, yGridValsForCurve);
            // indexgridPointsToInOutCheck = iSmallGridSubX*yGridValsForCurve.size() + iSmallGridSubY ,where iSmallGridSubX=0:(xGridValsForCurve.size()-1) , iSmallGridSubY=0:(yGridValsForCurve.size()-1)
            // SubXInBinaryVolume = iSmallGridSubX + indexStartXForCurve;
            // SubYInBinaryVolume = iSmallGridSubY + indexStartYForCurve;
            // SubZInBinaryVolume = iZ
            std::vector<int> indicesOfVoxelCentersInsidePolygon = inPolygonIndices(gridPointsToInOutCheck,
                                                                                   roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);

            // Now consider 4 diffrent corner voxel points for each voxel
            // Need a factor of 0.1 to convert to cm
            // left = -ImageSet_type.imageHeader.x_pixdim/20;
            // Right= +ImageSet_type.imageHeader.x_pixdim/20;
            // Up = +ImageSet_type.imageHeader.y_pixdim/20;
            // Low= -ImageSet_type.imageHeader.y_pixdim/20;

            std::vector<Point_2d_float> upperLeftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> upperRightCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> lowerLeftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> lowerRightCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> upperCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> lowerCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> leftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<Point_2d_float> rightCornerPoints(gridPointsToInOutCheck.size());


            for (int iPt = 0; iPt < gridPointsToInOutCheck.size(); ++iPt)
            {
                upperLeftCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x - Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                upperLeftCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y + Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                upperRightCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x + Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                upperRightCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y + Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                lowerLeftCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x - Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                lowerLeftCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y - Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                lowerRightCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x + Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                lowerRightCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y - Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                upperCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x;
                upperCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y + Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                lowerCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x;
                lowerCornerPoints[iPt].y =
                        gridPointsToInOutCheck[iPt].y - Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim / 2;

                leftCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x - Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                leftCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y;

                rightCornerPoints[iPt].x =
                        gridPointsToInOutCheck[iPt].x + Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim / 2;
                rightCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y;


            }

            std::vector<int> indicesOfUpperLeftCornersInsidePolygon = inPolygonIndices(upperLeftCornerPoints,
                                                                                       roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfUpperRigthCornersInsidePolygon = inPolygonIndices(upperRightCornerPoints,
                                                                                        roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerLeftCornersInsidePolygon = inPolygonIndices(lowerLeftCornerPoints,
                                                                                       roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerRightCornersInsidePolygon = inPolygonIndices(lowerRightCornerPoints,
                                                                                        roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);

            std::vector<int> indicesOfUpperCornersInsidePolygon = inPolygonIndices(upperCornerPoints,
                                                                                   roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerCornersInsidePolygon = inPolygonIndices(lowerCornerPoints,
                                                                                   roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLeftCornersInsidePolygon = inPolygonIndices(leftCornerPoints,
                                                                                  roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfRightCornersInsidePolygon = inPolygonIndices(rightCornerPoints,
                                                                                   roisPropVector[indexRoi].curveVector[matchingCurveIndices[iC]].point);



            // now union of all indices for center voxel and corners give the voxel points inside
            std::vector<unsigned int> allIndicesAppendedVector;
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(), indicesOfVoxelCentersInsidePolygon.begin(),
                                            indicesOfVoxelCentersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),
                                            indicesOfUpperLeftCornersInsidePolygon.begin(),
                                            indicesOfUpperLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),
                                            indicesOfUpperRigthCornersInsidePolygon.begin(),
                                            indicesOfUpperRigthCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),
                                            indicesOfLowerLeftCornersInsidePolygon.begin(),
                                            indicesOfLowerLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),
                                            indicesOfLowerRightCornersInsidePolygon.begin(),
                                            indicesOfLowerRightCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(), indicesOfUpperCornersInsidePolygon.begin(),
                                            indicesOfUpperCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(), indicesOfLowerCornersInsidePolygon.begin(),
                                            indicesOfLowerCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(), indicesOfLeftCornersInsidePolygon.begin(),
                                            indicesOfLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(), indicesOfRightCornersInsidePolygon.begin(),
                                            indicesOfRightCornersInsidePolygon.end());


            //std::vector<int> indicesOfVoxelsInsidePolygon(unionOfAllset.begin(), unionOfAllset.end());

            // A map to store indexOfVoxel => freq
            std::map<unsigned int, unsigned int> indicesOfVoxelsInsidePolygonAndFrequency = histogram(
                    allIndicesAppendedVector);


            int SubXInBinaryVolume;
            int SubYInBinaryVolume;

            // calculate the corresponding 3d indices in binary volume and add a one
            // for (int iPtsInside = 0; iPtsInside < indicesOfVoxelsInsidePolygonAndFrequency.size(); ++iPtsInside)
            for (const auto &indexFreqPair : indicesOfVoxelsInsidePolygonAndFrequency)
            {

                // At least three points should be inside the polygon to be considered in
                // this produced the least error wrt pinnacle (less than 0.7% in volume for bladder(dice=.999) , and 2% for sigmoid (dice 0.98))
                if (indexFreqPair.second < 2)
                {
                    continue;
                }


                int iSmallGridSubX = indexFreqPair.first / yGridValsForCurve.size();
                int iSmallGridSubY = indexFreqPair.first % yGridValsForCurve.size();

                SubXInBinaryVolume = iSmallGridSubX + indexStartXForCurve;
                SubYInBinaryVolume = iSmallGridSubY + indexStartYForCurve;

                int index3DColMajor = iZ * Patient.ImageSetList[indexImageSet].imageHeader.x_dim *
                                      Patient.ImageSetList[indexImageSet].imageHeader.y_dim +
                                      SubYInBinaryVolume * Patient.ImageSetList[indexImageSet].imageHeader.x_dim +
                                      SubXInBinaryVolume;

                roisPropVector[indexRoi].binaryBitmapPointIndicesX.push_back(SubXInBinaryVolume);
                roisPropVector[indexRoi].binaryBitmapPointIndicesY.push_back(SubYInBinaryVolume);
                roisPropVector[indexRoi].binaryBitmapPointIndicesZ.push_back(iZ);
                roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.push_back(index3DColMajor);
                roisPropVector[indexRoi].binaryBitmap[index3DColMajor] =
                        roisPropVector[indexRoi].binaryBitmap[index3DColMajor] ^
                        true; // XOR true to find inside or outside

                // Condition for partial volume computation for each voxel based on 9 points
                // 1,2-->0
                //3 --> 1/8
                //4 -->1/4
                //5--> 1/4+1/8
                //6 -->0.5
                //7 -->0.5 +1/8
                //8--> 0.5 +1/4
                // 9 --> 1
                switch (indexFreqPair.second)
                {
                    case 2 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.25); // 0.125
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(2);
                        break;
                    case 3 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.125
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(3);
                        break;
                    case 4 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.250
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(4);
                        break;
                    case 5 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.375
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(5);
                        break;
                    case 6 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.625);  // 0.5
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(6);
                        break;
                    case 7 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(0.75); // 0.625
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(7);
                        break;
                    case 8 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(1.0); // 0.75
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(8);
                        break;
                    case 9 :
                        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.push_back(1.0);
                        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(9);
                        break;
                }

            }


        } // iC
    } // iZ


    // delete even number of index3DColMajor in indicesNonzerosElemInBinaryBitmap,  also delete the partial volumeBinaryBitmapVoxels associated with them
    // TODO: needs to be written faster --> so new method is written but it is buggy now for windows (new method to be fixed in future)
    bool flagOldMethod = false;

    if (flagOldMethod)
    {
        std::map<unsigned int, unsigned int> indicesOfVoxelsInsideRoiAndFrequency = histogram(
                roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap);
        std::vector<unsigned int> indicesToBeDeleted;
        for (const auto &indexFreqPair : indicesOfVoxelsInsideRoiAndFrequency)
        {
            if (indexFreqPair.second % 2 == 0)
            {
                int indexToBeDelete = indexFreqPair.first;
                std::vector<unsigned int> indicesToBeDeletedForThisPair = findSatisfiyingIndices(
                        roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap,
                        [indexToBeDelete](unsigned int indexValue)
                        { return indexValue == indexToBeDelete; });

                indicesToBeDeleted.insert(indicesToBeDeleted.end(), indicesToBeDeletedForThisPair.begin(),
                                          indicesToBeDeletedForThisPair.end());

            }
        }

        for (int iToBeDel = 0; iToBeDel < indicesToBeDeleted.size(); ++iToBeDel)
        {
            roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.erase(
                    roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin() + indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.erase(
                    roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.begin() + indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.erase(
                    roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.begin() +
                    indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].binaryBitmapPointIndicesX.erase(
                    roisPropVector[indexRoi].binaryBitmapPointIndicesX.begin() + indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].binaryBitmapPointIndicesY.erase(
                    roisPropVector[indexRoi].binaryBitmapPointIndicesY.begin() + indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].binaryBitmapPointIndicesZ.erase(
                    roisPropVector[indexRoi].binaryBitmapPointIndicesZ.begin() + indicesToBeDeleted[iToBeDel]);
        }

    } else // new method
    {

        std::map<unsigned int, freqAndConsInd> indicesOfVoxelsInsideRoiWithFrequencyAndReconstructIndices = histogramWithReconstructionIndices(
                roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap);
        std::vector<unsigned int> indicesToBeDeleted;
        //    int indexToBeDelete;
        for (const auto &indexAndFreqConsIndPair : indicesOfVoxelsInsideRoiWithFrequencyAndReconstructIndices)
        {
            if (indexAndFreqConsIndPair.second.freq % 2 == 0)
            {
                //           indexToBeDelete= indexAndFreqConsIndPair.first;
                //             std::vector<unsigned int> indicesTobeDeletedForThisVoxelIndex= findSatisfiyingIndices(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap,[indexToBeDelete](unsigned int indexValue){return indexValue ==indexToBeDelete;});
                //            indicesToBeDeleted.insert(indicesToBeDeleted.end(),indicesTobeDeletedForThisVoxelIndex.begin(),indicesTobeDeletedForThisVoxelIndex.end());
                indicesToBeDeleted.insert(indicesToBeDeleted.end(), indexAndFreqConsIndPair.second.constIndices.begin(),
                                          indexAndFreqConsIndPair.second.constIndices.end());
            }
        }
        for (unsigned int iToBeDel = 0; iToBeDel < indicesToBeDeleted.size(); ++iToBeDel)
        {
            //            roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.erase(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin()+indicesToBeDeleted[iToBeDel]);
            //            roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.erase(roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.begin()+indicesToBeDeleted[iToBeDel]);
            //            roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.erase(roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.begin()+indicesToBeDeleted[iToBeDel]);
            roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap[indicesToBeDeleted[iToBeDel]] = std::numeric_limits<unsigned int>::max();
            roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels[indicesToBeDeleted[iToBeDel]] = -5.0;
            roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap[indicesToBeDeleted[iToBeDel]] = 0;
            roisPropVector[indexRoi].binaryBitmapPointIndicesX[indicesToBeDeleted[iToBeDel]] = -5;
            roisPropVector[indexRoi].binaryBitmapPointIndicesY[indicesToBeDeleted[iToBeDel]] = -5;
            roisPropVector[indexRoi].binaryBitmapPointIndicesZ[indicesToBeDeleted[iToBeDel]] = -5;
        }

        roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.erase(
                std::remove(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin(),
                            roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.end(),
                            std::numeric_limits<unsigned int>::max()),
                roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.end());
        roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.erase(
                std::remove(roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.begin(),
                            roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.end(),
                            -5.0), roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.end());

        roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.erase(
                std::remove(roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.begin(),
                            roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.end(),
                            0), roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.end());

        roisPropVector[indexRoi].binaryBitmapPointIndicesX.erase(
                std::remove(roisPropVector[indexRoi].binaryBitmapPointIndicesX.begin(),
                            roisPropVector[indexRoi].binaryBitmapPointIndicesX.end(),
                            -5), roisPropVector[indexRoi].binaryBitmapPointIndicesX.end());
        roisPropVector[indexRoi].binaryBitmapPointIndicesY.erase(
                std::remove(roisPropVector[indexRoi].binaryBitmapPointIndicesY.begin(),
                            roisPropVector[indexRoi].binaryBitmapPointIndicesY.end(),
                            -5), roisPropVector[indexRoi].binaryBitmapPointIndicesY.end());
        roisPropVector[indexRoi].binaryBitmapPointIndicesZ.erase(
                std::remove(roisPropVector[indexRoi].binaryBitmapPointIndicesZ.begin(),
                            roisPropVector[indexRoi].binaryBitmapPointIndicesZ.end(),
                            -5), roisPropVector[indexRoi].binaryBitmapPointIndicesZ.end());
    }
    /**************************************************************/


    // calculate volume
    float sumOfPartialVolums = 0;
    for (float pVol : roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels)
    {
        sumOfPartialVolums += pVol;
    }

    if (verbose)
    {
        std::cout << "\n \n" << std::endl;
        std::cout << "Number of element of partialVolumeOfBinaryBitmapVoxels: "
                  << std::to_string(roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.size()) << std::endl;
        std::cout << "Number of elements of numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap: " << std::to_string(
                roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.size()) << std::endl;
        std::cout << "Number of elements inside indicesNonzerosElemInBinaryBitmap: "
                  << std::to_string(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.size()) << std::endl;
        std::cout << "Partial Volume sum: " << std::to_string(sumOfPartialVolums) << std::endl;
        std::cout << "\n \n" << std::endl;
    }
    roisPropVector[indexRoi].volume = sumOfPartialVolums * Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim *
                                      Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim *
                                      Patient.ImageSetList[indexImageSet].imageHeader.z_pixdim;
    std::cout << "Estimated Volume for " << roisPropVector[indexRoi].name << ": " << roisPropVector[indexRoi].volume
              << std::endl;

    // now calculate the coordinates of bitmap points form indices
    roisPropVector[indexRoi].binaryBitmapPointCoordinatesX.clear();
    roisPropVector[indexRoi].binaryBitmapPointCoordinatesY.clear();
    roisPropVector[indexRoi].binaryBitmapPointCoordinatesZ.clear();

    for (int iPoint = 0; iPoint < roisPropVector[indexRoi].binaryBitmapPointIndicesX.size(); ++iPoint)
    {
        roisPropVector[indexRoi].binaryBitmapPointCoordinatesX.push_back(
                Patient.ImageSetList[indexImageSet].volumeXGridDataVectorInCm[roisPropVector[indexRoi].binaryBitmapPointIndicesX[iPoint]]);
        roisPropVector[indexRoi].binaryBitmapPointCoordinatesY.push_back(
                Patient.ImageSetList[indexImageSet].volumeYGridDataVectorInCm[roisPropVector[indexRoi].binaryBitmapPointIndicesY[iPoint]]);
        roisPropVector[indexRoi].binaryBitmapPointCoordinatesZ.push_back(
                Patient.ImageSetList[indexImageSet].volumeZGridDataVectorInCm[roisPropVector[indexRoi].binaryBitmapPointIndicesZ[iPoint]]);
    }
    roisPropVector[indexRoi].isBitmapComputed = true;


    return OKFlag;
}

int PinnacleFiles::createBitmapVoxelMassForGivenRoiName(std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::createBitmapVoxelMassForGivenRoiName -->  roisPropVector size is 0"
                  << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }

    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }

    if (-1 == Patient.PatientID)
    {
        std::cout << "ERROR: Patient File is not loaded: " << std::endl;
        return FAIL;
    }

    if (Patient.ImageSetList.size() == 0)
    {
        std::cout << "ERROR: No ImageSet found in Patient(PatientID,Name,LastName) " << Patient.PatientID << ","
                  << Patient.FirstName << "," << Patient.LastName << ")" << std::endl;
        return FAIL;
    }

    int indexImageSet = -1;
    for (int iImageSet = 0; iImageSet < Patient.ImageSetList.size(); ++iImageSet)
    {
        if (roisPropVector[indexRoi].volume_name.compare(Patient.ImageSetList[iImageSet].imageHeader.db_name) == 0)
        {
            indexImageSet = iImageSet;
            break;
        }

    }

    if (-1 == indexImageSet)
    {
        std::cout << "ERROR: imageset name in ROI:" << roisPropVector[indexRoi].name
                  << " do not match the imageset names in Patient:" << Patient.PatientID << std::endl;
        return FAIL;
    }
    // update corresponding imageSet index
    roisPropVector[indexRoi].indexImageSet = indexImageSet;

    // 2) Check if the ImageData (volumeData) is loaded if not attempt to load it

    if (!Patient.ImageSetList[indexImageSet].isVolumeDataLoaded)
    {
        std::cout << "ImageSet volume data is not loaded attempting to load...." << std::endl;
        if (OKFlag != read_pinnacle_volume(Patient.ImageSetList[indexImageSet]))
        {
            std::cout << "ERROR in reading data volume for  imageset " << indexImageSet << std::endl;
            return FAIL;
        }
    }

    // 3) check if the Bitmap is computed

    if (!roisPropVector[indexRoi].isBitmapComputed)
    {
        std::cout << "ERROR: bitmap is not computed in ROI:" << roisPropVector[indexRoi].name
                  << " do not match the imageset names in Patient:" << Patient.PatientID << std::endl;
        return FAIL;
    }


    //4) now create the massOfBinaryBitmapVoxels array
    //    need CT information to calc this we have the indices calculated
    roisPropVector[indexRoi].massOfBinaryBitmapVoxels.clear();
    roisPropVector[indexRoi].massOfBinaryBitmapVoxels = std::vector<float>(
            roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.size(), 0.0);
    float volumeOneVoxel = Patient.ImageSetList[indexImageSet].imageHeader.x_pixdim *
                           Patient.ImageSetList[indexImageSet].imageHeader.y_pixdim *
                           Patient.ImageSetList[indexImageSet].imageHeader.z_pixdim;
    float mass = 0;
    for (int iVoxel = 0; iVoxel < roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.size(); ++iVoxel)
    {
        roisPropVector[indexRoi].massOfBinaryBitmapVoxels[iVoxel] =
                roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels[iVoxel] * volumeOneVoxel *
                static_cast<float>(Patient.ImageSetList[indexImageSet].volumeDataUint16[roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap[iVoxel]]) /
                1000.0;
        mass = mass + roisPropVector[indexRoi].massOfBinaryBitmapVoxels[iVoxel];
    }
    roisPropVector[indexRoi].mass = mass;

    roisPropVector[indexRoi].isMassComputed = true;


    return OKFlag;
}


// http://stackoverflow.com/questions/21682716/create-a-histogram-using-c-with-map-unordered-map-the-default-value-for-a-non
template<typename T>
std::map<T, unsigned int> PinnacleFiles::histogram(std::vector<T> &a)
{
    std::map<T, unsigned int> hist;
    for (auto &x : a)
    {
        hist[x]++;
    }
    return hist;
}

template<typename T>
std::map<T, PinnacleFiles::freqAndConsInd> PinnacleFiles::histogramWithReconstructionIndices(std::vector<T> &a)
{
    std::map<T, freqAndConsInd> histWithReconIndices;
    for (int index = 0; index < a.size(); ++index)
    {
        histWithReconIndices[a[index]].freq++;
        histWithReconIndices[a[index]].constIndices.push_back(index);
    }

    return histWithReconIndices;


}


int PinnacleFiles::createBitmapForAllRoisOnGivenImageSetName(std::string ImageSetName)
{
    return OKFlag;
}

//TODO: fix portable binary writing
int PinnacleFiles::saveBitmapForGivenRoiName(std::string bitMapFileName, std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }


    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }


    if (fileExists(bitMapFileName))
    {
        std::cout << "Warnning: bitMap file " << bitMapFileName << " exists, and is being overwritten  " << std::endl;
    }

#ifndef ITK_UTILS
    std::ofstream outputFileStream(bitMapFileName.c_str(), std::ios::out | std::ofstream::binary);
    if (outputFileStream.good())
    {
        std::copy(roisPropVector[indexRoi].binaryBitmap.begin(), roisPropVector[indexRoi].binaryBitmap.end(), std::ostreambuf_iterator<char>(outputFileStream));
    }
    else
    {
        std::cout << "ERROR: bitMap file  " <<  bitMapFileName  << std::endl;
        outputFileStream.close();
        return FAIL;
    }
    outputFileStream.close();
#else
    // nrrd file writing
    int indexImageSet = roisPropVector[indexRoi].indexImageSet;
    std::vector<char> volumeData;
    std::copy(roisPropVector[indexRoi].binaryBitmap.begin(), roisPropVector[indexRoi].binaryBitmap.end(),
              std::back_inserter(volumeData));

    writeVectorToNrrdFile(bitMapFileName, Patient.ImageSetList[indexImageSet], volumeData);
#endif

    return OKFlag;
}

int PinnacleFiles::savePartialVolumeOFBitmapForGivenRoiName(std::string partialVolFileName, std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }


    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }


    if (fileExists(partialVolFileName))
    {
        std::cout << "Warnning: paritial volume file " << partialVolFileName << " exists, and is being overwritten  "
                  << std::endl;
    }

#ifndef ITK_UTILS
    std::ofstream outputFileStream(partialVolFileName.c_str(), std::ios::out | std::ofstream::binary);

    if (outputFileStream.good())
    {

        //     std::copy(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin(), roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.end(), std::ostreambuf_iterator<char>(outputFileStream));
        outputFileStream.write(
                reinterpret_cast<const char *>(&roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels[0]),
                roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.size() * sizeof(float));

        outputFileStream.close();
    } else
    {
        std::cout << "ERROR: writing paritial volume " << partialVolFileName << std::endl;
        outputFileStream.close();
        return FAIL;
    }
#else
    // nrrd file writing
    int indexImageSet = roisPropVector[indexRoi].indexImageSet;
    std::vector<float> volumeData;
    std::copy(roisPropVector[indexRoi].binaryBitmap.begin(), roisPropVector[indexRoi].binaryBitmap.end(),
              std::back_inserter(volumeData));
    auto pvol = roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.begin();
    auto idx = roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin();
    for(;pvol != roisPropVector[indexRoi].partialVolumeOfBinaryBitmapVoxels.end(); ++pvol, ++idx)
    {
        volumeData[*idx] = *pvol;
    }

    writeVectorToNrrdFile(partialVolFileName, Patient.ImageSetList[indexImageSet], volumeData);
#endif
    
    return OKFlag;
}

int PinnacleFiles::saveIndicesNonzerosElemInBinaryBitmapForGivenRoiName(std::string indicesVecFileName,
                                                                        std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }


    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }


    if (fileExists(indicesVecFileName))
    {
        std::cout << "Warnning: Indices vector file " << indicesVecFileName << "exists, and is being overwritten  "
                  << std::endl;
    }

    std::ofstream outputFileStream(indicesVecFileName.c_str(), std::ios::out | std::ofstream::binary);
    if (outputFileStream.good())
    {

        //     std::copy(roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.begin(), roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.end(), std::ostreambuf_iterator<char>(outputFileStream));
        outputFileStream.write(
                reinterpret_cast<const char *>(&roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap[0]),
                roisPropVector[indexRoi].indicesNonzerosElemInBinaryBitmap.size() * sizeof(unsigned int));

        outputFileStream.close();
    } else
    {
        std::cout << "ERROR: writing indices vector file " << indicesVecFileName << std::endl;
        outputFileStream.close();
        return FAIL;
    }


    return OKFlag;
}

int PinnacleFiles::savenumCornerInsidePolygonForNonzeroPointsOfBinaryBitmapForGivenRoiName(
        std::string numConrnersVecFileName, std::string ROIName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::readRoiDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }


    //1) find the requested ROI index in the roisPropVector and extract the corresponding imageSet name
    int indexRoi = -1;
    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(ROIName) == 0)
        {
            indexRoi = iRoi;
            break;
        }
    }

    if (-1 == indexRoi)
    {
        std::cout << "ERROR: Could not found the requested ROI: " << ROIName << std::endl;
        return FAIL;
    }


    if (fileExists(numConrnersVecFileName))
    {
        std::cout << "Warnning: number-of-corners-inside-mask file " << numConrnersVecFileName
                  << "exists, and is being overwritten  " << std::endl;
    }

    std::ofstream outputFileStream(numConrnersVecFileName.c_str(), std::ios::out | std::ofstream::binary);


    if (outputFileStream.good())
    {

        outputFileStream.write(
                reinterpret_cast<const char *>(&roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap[0]),
                roisPropVector[indexRoi].numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.size() *
                sizeof(unsigned int short));

        outputFileStream.close();
    } else
    {
        std::cout << "ERROR: writing number-of-corners-inside-mask file " << numConrnersVecFileName << std::endl;
        outputFileStream.close();
        return FAIL;
    }


    return OKFlag;
}


std::vector<int>
PinnacleFiles::inPolygonIndices(const std::vector<Point_3d_float> &pVec, const std::vector<Point_3d_float> &polygon)
{
    int nvert = polygon.size();
    std::vector<double> vertx(nvert, 0);
    std::vector<double> verty(nvert, 0);

    for (int iVert = 0; iVert < nvert; iVert++)
    {
        vertx[iVert] = polygon[iVert].x;
        verty[iVert] = polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert, vertx, verty, pVec[i].x, pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
    return inPolyIndices;

}

// function ignores the z coordinates of polygon
std::vector<int>
PinnacleFiles::inPolygonIndices(const std::vector<Point_2d_float> &pVec, const std::vector<Point_3d_float> &polygon)
{
    int nvert = polygon.size();
    std::vector<double> vertx(nvert, 0);
    std::vector<double> verty(nvert, 0);

    for (int iVert = 0; iVert < nvert; iVert++)
    {
        vertx[iVert] = polygon[iVert].x;
        verty[iVert] = polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert, vertx, verty, pVec[i].x, pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}

std::vector<int>
PinnacleFiles::inPolygonIndices(const std::vector<Point_2d_float> &pVec, const std::vector<Point_2d_float> &polygon)
{
    int nvert = polygon.size();
    std::vector<double> vertx(nvert, 0);
    std::vector<double> verty(nvert, 0);

    for (int iVert = 0; iVert < nvert; iVert++)
    {
        vertx[iVert] = polygon[iVert].x;
        verty[iVert] = polygon[iVert].y;
    }



    // initialize the output vector
    std::vector<int> inPolyIndices(0);
    for (int i = 0; i < pVec.size(); i++)
    {
        // pVec[i], polygon
        if (pnpoly(nvert, vertx, verty, pVec[i].x, pVec[i].y))
        {
            inPolyIndices.push_back(i);
        }
    }
    return inPolyIndices;
}

bool
PinnacleFiles::pnpoly(int nvert, std::vector<double> &vertx, std::vector<double> &verty, double testx, double testy)
{
    int i, j;
    bool c = false;
    for (i = 0, j = nvert - 1; i < nvert; j = i++)
    {
        if (((verty[i] > testy) != (verty[j] > testy)) &&
            (testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
            c = !c;
    }
    return c;
}


int PinnacleFiles::printRoisDataForGivenRoiName(std::string roiName)
{
    if (0 == roisPropVector.size())
    {
        std::cout << "ERROR: PinnacleFiles::printRoisDataForGivenRoiName -->  roisPropVector size is 0" << std::endl;
        if (OKFlag != readRoisProperties())
        {
            return FAIL;
        }
    }

    int roiIndexInVector = -1;

    for (int iRoi = 0; iRoi < roisPropVector.size(); ++iRoi)
    {
        if (roisPropVector[iRoi].name.compare(roiName) == 0)
        {
            roiIndexInVector = iRoi;
            break;
        }
    }
    if (roiIndexInVector == -1)
    {
        std::cout << "ERROR:printRoisDataForGivenRoiName --> roiName could not be found in input roi_full_prop?"
                  << std::endl;
        return FAIL;
    }


    if (verbose)
    {
        std::cout << "Reading data for ROI  --> " << roisPropVector[roiIndexInVector].name << std::endl;

    }
    std::cout << "\t Number of curve is  = " << std::to_string(roisPropVector[roiIndexInVector].n_curves) << std::endl;
    std::cout << "\t Volume is  = " << std::to_string(roisPropVector[roiIndexInVector].volume) << std::endl;
    std::cout << "\t volume_name is  = " << roisPropVector[roiIndexInVector].volume_name << std::endl;
    std::cout << "\t color is  = " << roisPropVector[roiIndexInVector].color << std::endl;
    std::cout << "\t stats_volume_name is  = " << roisPropVector[roiIndexInVector].stats_volume_name << std::endl;

    for (int iCurve = 0; iCurve < roisPropVector[roiIndexInVector].n_curves; ++iCurve)
    {
        std::cout << "\t \t Number of pts in curve " << std::to_string(iCurve) << " is "
                  << std::to_string(roisPropVector[roiIndexInVector].curveVector[iCurve].npts) << std::endl;
        std::cout << "\t \t Area for  curve " << std::to_string(iCurve) << " is "
                  << std::to_string(roisPropVector[roiIndexInVector].curveVector[iCurve].area) << std::endl;

        for (int iPts = 0; iPts < roisPropVector[roiIndexInVector].curveVector[iCurve].npts; ++iPts)
        {
            std::cout << "\t \t \t point[" << std::to_string(iPts) << "] coordinate -->  (x,y,z) = (" <<
                      std::to_string(roisPropVector[roiIndexInVector].curveVector[iCurve].point[iPts].x) << "," <<
                      std::to_string(roisPropVector[roiIndexInVector].curveVector[iCurve].point[iPts].y) << "," <<
                      std::to_string(roisPropVector[roiIndexInVector].curveVector[iCurve].point[iPts].z) << std::endl;
        }

    }

    return OKFlag;
}


void PinnacleFiles::setRtDosePinn(RtDosePinn *_rtDosePinn)
{
    rtDosePinn = _rtDosePinn;
}


void PinnacleFiles::setPatientFilePath(std::string _patientFilePath)
{

    patientFilePath = _patientFilePath;
}

void PinnacleFiles::setTrialFilePath(std::string _planDotTrailFilePath)
{

    planDotTrailFilePath = _planDotTrailFilePath;
}

void PinnacleFiles::setRoiFilePath(std::string _planDotRoiFilePath)
{
    planDotRoiFilePath = _planDotRoiFilePath;
}






