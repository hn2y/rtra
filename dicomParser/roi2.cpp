#include "roi2.h"



ROI2::ROI2():logstream(std::cout)
{
    rtstructIODPointer= NULL;
    DisplayColor=std::vector<unsigned int>(3);
    rtStructFileName=0;
    rtstructIODPointer=0;
    ctImageSet=NULL;
    IsTargetVolume=false;
    ItemNumberInRoiContourSequence =-1;
}

ROI2::ROI2(std::ostream &stream):logstream(stream)
{
    rtstructIODPointer= NULL;
    DisplayColor=std::vector<unsigned int>(3);
    rtStructFileName=0;
    rtstructIODPointer=0;
    ctImageSet=NULL;
    IsTargetVolume=false;
    ItemNumberInRoiContourSequence=-1;
}

ROI2::~ROI2()
{

}




bool ROI2::createBitmap()
{
    // read dicom image information from the ctImageSet to construct the binary map
    std::string class_member = "ROI2::createBitmap:";

    logstream << class_member<< "Number of CT Image in Image set:" << ctImageSet->size() << std::endl;
    if (NULL == ctImageSet)
    {
        logstream << class_member <<"ctImageSet has not been set! " << std::endl;

        return false;

    }

    imageSetSlicesZValuesInMM.clear();

    for (int iCT = 0; iCT < ctImageSet->size(); ++iCT)
    {
        imageSetSlicesZValuesInMM.push_back(ctImageSet->at(iCT).sliceZvalue);


        if (0 == iCT)
        {
            ImageSetYdim =ctImageSet->at(iCT).Rows;
            ImageSetXdim = ctImageSet->at(iCT).Columns;
            // fill the ImageSetGrid data // TODO: check for consistency
            ImageSetXGridValuesInMM= ctImageSet->at(iCT).gridXvalues;
            ImageSetYGridValuesInMM = ctImageSet->at(iCT).gridYvalues;

        }

    }
    ImageSetZdim = imageSetSlicesZValuesInMM.size();

    // 3) initialize the ROI binary mask with a array of size CT image set
    binaryBitmap = std::vector<bool>(ImageSetYdim*ImageSetXdim*ImageSetZdim);



    // 4) for a given  image plane form CT imageSet
    //   a- check if CT z-values are inside roi bounding box, if not skip the binary volume with the same z-values
    //     these  z values which contain ROI's  z values --> volumeZGridDataVectorInCm[indexStartZ:indexEndZ]
    int indexStartZ = closestNumberIndexInVectorLessThanOrEqualToValue(imageSetSlicesZValuesInMM,boundingBox.minZ + 0.002);  //0.001 to ignore small differences
    int indexEndZ = closestNumberIndexInVectorGreaterThanOrEqualToValue(imageSetSlicesZValuesInMM,boundingBox.maxZ - 0.002); //0.001 to ignore small differences
    if(indexStartZ > indexEndZ)
    {
        logstream << class_member<< "ERROR: The z-values are not monotonically increasing! Check this volume this should not normally happen for a DICOM Volume"  << std::endl;
        return false;
    }


    partialVolumeOfBinaryBitmapVoxels.clear();
    indicesNonzerosElemInBinaryBitmap.clear();
    numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.clear();

    float x_PixelDimMM = ctImageSet->at(0).PixelSpacing[0];
    float y_PixelDimMM = ctImageSet->at(0).PixelSpacing[1];
    float z_PixelDimMM = fabs(imageSetSlicesZValuesInMM[1]-imageSetSlicesZValuesInMM[0]);


    // 5) going through each slice one by one to
    for (int iZ = indexStartZ; iZ < indexEndZ+1; ++iZ)
    {

        // find ROI's curves indices at this  z-value
        float CtZvalue = imageSetSlicesZValuesInMM[iZ];
        std::vector<int> matchingCurveIndices = findSatisfiyingIndices(curveZlocations,[CtZvalue](float curveZvalues){return (abs(curveZvalues - CtZvalue)<=0.001) ;}); // Notice a small error between z-values is acceptable

        if (0 == matchingCurveIndices.size())
        {
            std::cout << "Could not found a curve  at this "<< imageSetSlicesZValuesInMM[iZ] <<" binary bitmap z-location "  << std::endl;
            continue;
        }


        for (unsigned int iC= 0; iC < matchingCurveIndices.size(); ++iC)
        {

            // For each ROI curve -->  make a meshgrid within roi_curve_type.maxX , roi_curve_type.minX , roi_curve_type.maxY , roi_curve_type.minY
            int indexStartXForCurve =closestNumberIndexInVectorLessThanOrEqualToValue(ImageSetXGridValuesInMM,curveVector[matchingCurveIndices[iC]].minX);
            int indexEndXForCurve =closestNumberIndexInVectorGreaterThanOrEqualToValue(ImageSetXGridValuesInMM,curveVector[matchingCurveIndices[iC]].maxX);


            // in HFS  indexEndYForCurve< indexStartYForCurve
            int indexEndYForCurve =closestNumberIndexInVectorLessThanOrEqualToValue(ImageSetYGridValuesInMM,curveVector[matchingCurveIndices[iC]].minY);
            int indexStartYForCurve =closestNumberIndexInVectorGreaterThanOrEqualToValue(ImageSetYGridValuesInMM,curveVector[matchingCurveIndices[iC]].maxY);


            std::vector<float> xGridValsForCurve(ImageSetXGridValuesInMM.begin()+indexStartXForCurve, ImageSetXGridValuesInMM.begin()+indexEndXForCurve+1);
            std::vector<float> yGridValsForCurve(ImageSetYGridValuesInMM.begin()+indexEndYForCurve, ImageSetYGridValuesInMM.begin()+indexStartYForCurve+1);

            std::vector<PointFloat> gridPointsToInOutCheck = xyMeshGrid(xGridValsForCurve,yGridValsForCurve);
            // indexgridPointsToInOutCheck = iSmallGridSubX*yGridValsForCurve.size() + iSmallGridSubY ,where iSmallGridSubX=0:(xGridValsForCurve.size()-1) , iSmallGridSubY=0:(yGridValsForCurve.size()-1)
            // SubXInBinaryVolume = iSmallGridSubX + indexStartXForCurve;
            // SubYInBinaryVolume = iSmallGridSubY + indexStartYForCurve;
            // SubZInBinaryVolume = iZ
            std::vector<int> indicesOfVoxelCentersInsidePolygon= inPolygonIndices(gridPointsToInOutCheck,curveVector[matchingCurveIndices[iC]].point);

            // Now consider 4 diffrent corner voxel points for each voxel
            // Need a factor of 0.1 to convert to cm
            // left = -ImageSet_type.imageHeader.x_pixdim/20;
            // Right= +ImageSet_type.imageHeader.x_pixdim/20;
            // Up = +ImageSet_type.imageHeader.y_pixdim/20;
            // Low= -ImageSet_type.imageHeader.y_pixdim/20;

            std::vector<PointFloat> upperLeftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> upperRightCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> lowerLeftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> lowerRightCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> upperCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> lowerCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> leftCornerPoints(gridPointsToInOutCheck.size());
            std::vector<PointFloat> rightCornerPoints(gridPointsToInOutCheck.size());



            for (int iPt = 0; iPt < gridPointsToInOutCheck.size(); ++iPt)
            {


                upperLeftCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x-x_PixelDimMM/2.0;
                upperLeftCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y+y_PixelDimMM/2.0;

                upperRightCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x+x_PixelDimMM/2.0;
                upperRightCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y+y_PixelDimMM/2.0;

                lowerLeftCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x-x_PixelDimMM/2.0;
                lowerLeftCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y-y_PixelDimMM/2.0;

                lowerRightCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x+x_PixelDimMM/2.0;
                lowerRightCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y-y_PixelDimMM/2.0;

                upperCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x;
                upperCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y+y_PixelDimMM/2.0;

                lowerCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x;
                lowerCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y-y_PixelDimMM/2.0;

                leftCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x-x_PixelDimMM/2.0;
                leftCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y;

                rightCornerPoints[iPt].x = gridPointsToInOutCheck[iPt].x+x_PixelDimMM/2.0;
                rightCornerPoints[iPt].y = gridPointsToInOutCheck[iPt].y;


            }

            std::vector<int> indicesOfUpperLeftCornersInsidePolygon= inPolygonIndices(upperLeftCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfUpperRigthCornersInsidePolygon= inPolygonIndices(upperRightCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerLeftCornersInsidePolygon= inPolygonIndices(lowerLeftCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerRightCornersInsidePolygon= inPolygonIndices(lowerRightCornerPoints,curveVector[matchingCurveIndices[iC]].point);

            std::vector<int> indicesOfUpperCornersInsidePolygon= inPolygonIndices(upperCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLowerCornersInsidePolygon= inPolygonIndices(lowerCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfLeftCornersInsidePolygon= inPolygonIndices(leftCornerPoints,curveVector[matchingCurveIndices[iC]].point);
            std::vector<int> indicesOfRightCornersInsidePolygon= inPolygonIndices(rightCornerPoints,curveVector[matchingCurveIndices[iC]].point);



            // now union of all indices for center voxel and corners give the voxel points inside
            std::vector<unsigned int> allIndicesAppendedVector;
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfVoxelCentersInsidePolygon.begin(),indicesOfVoxelCentersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfUpperLeftCornersInsidePolygon.begin(),indicesOfUpperLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfUpperRigthCornersInsidePolygon.begin(),indicesOfUpperRigthCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfLowerLeftCornersInsidePolygon.begin(),indicesOfLowerLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfLowerRightCornersInsidePolygon.begin(),indicesOfLowerRightCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfUpperCornersInsidePolygon.begin(),indicesOfUpperCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfLowerCornersInsidePolygon.begin(),indicesOfLowerCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfLeftCornersInsidePolygon.begin(),indicesOfLeftCornersInsidePolygon.end());
            allIndicesAppendedVector.insert(allIndicesAppendedVector.end(),indicesOfRightCornersInsidePolygon.begin(),indicesOfRightCornersInsidePolygon.end());


            //std::vector<int> indicesOfVoxelsInsidePolygon(unionOfAllset.begin(), unionOfAllset.end());

            // A map to store indexOfVoxel => freq
            std::map <unsigned int, unsigned int> indicesOfVoxelsInsidePolygonAndFrequency=histogram(allIndicesAppendedVector);



            int SubXInBinaryVolume;
            int SubYInBinaryVolume;

            // calculate the corresponding 3d indices in binary volume and add a one
            // for (int iPtsInside = 0; iPtsInside < indicesOfVoxelsInsidePolygonAndFrequency.size(); ++iPtsInside)
            for ( const auto &indexFreqPair : indicesOfVoxelsInsidePolygonAndFrequency )
            {

                // At least three points should be inside the polygon to be considered in
                // this produced the least error wrt pinnacle (less than 0.7% in volume for bladder(dice=.999) , and 2% for sigmoid (dice 0.98))
                if (indexFreqPair.second <2)
                {
                    continue;
                }


                int iSmallGridSubX = indexFreqPair.first/yGridValsForCurve.size();
                int iSmallGridSubY = indexFreqPair.first%yGridValsForCurve.size();

                SubXInBinaryVolume = iSmallGridSubX+indexStartXForCurve;
                SubYInBinaryVolume = iSmallGridSubY+indexStartYForCurve;

                int index3DColMajor =  iZ*ImageSetXdim*ImageSetYdim+ SubYInBinaryVolume*ImageSetXdim+ SubXInBinaryVolume;

                indicesNonzerosElemInBinaryBitmap.push_back(index3DColMajor);
                binaryBitmap[index3DColMajor]= binaryBitmap[index3DColMajor] ^ true; // XOR true to find inside or outside

                // Condition for partial volume computation for each voxel based on 9 points
                // 1,2-->0
                //3 --> 1/8
                //4 -->1/4
                //5--> 1/4+1/8
                //6 -->0.5
                //7 -->0.5 +1/8
                //8--> 0.5 +1/4
                // 9 --> 1
                switch(indexFreqPair.second)
                {
                case 2 : partialVolumeOfBinaryBitmapVoxels.push_back(0.25); // 0.125
                         numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(2);
                    break;
                case 3 : partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.125
                        numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(3);
                    break;
                case 4 : partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.250
                        numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(4);
                    break;
                case 5 : partialVolumeOfBinaryBitmapVoxels.push_back(0.5); // 0.375
                        numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(5);
                    break;
                case 6 : partialVolumeOfBinaryBitmapVoxels.push_back(0.625);  // 0.5
                         numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(6);
                    break;
                case 7 : partialVolumeOfBinaryBitmapVoxels.push_back(0.75); // 0.625
                         numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(7);
                    break;
                case 8 : partialVolumeOfBinaryBitmapVoxels.push_back(1.0); // 0.75
                        numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(8);
                    break;
                case 9 : partialVolumeOfBinaryBitmapVoxels.push_back(1.0);
                         numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap.push_back(9);
                    break;
                }

            }


        } // iC


    } // iZ




    return true;
}



bool ROI2::readCurveData()
{
    std::string class_member = "ROI2::readCurveData:";

    if (NULL== rtstructIODPointer)
    {
        logstream << class_member <<"rtstructIODPointer has not been set! " << std::endl;

        return false;
    }

    if (-1 == ItemNumberInRoiContourSequence)
    {
        logstream << class_member <<"ItemNumberInRoiContourSequence has not been set! " << std::endl;
        return false;
    }

    DRTROIContourSequence roiContourSequence = rtstructIODPointer->getROIContourSequence();
    DRTROIContourSequence::Item* roiContourSequenceItem;
    roiContourSequence.getItem(ItemNumberInRoiContourSequence,roiContourSequenceItem);

    DRTContourSequence contourSeq = roiContourSequenceItem->getContourSequence();
    DRTContourSequence::Item * contourItem;
    OFVector<Float64> contourData;

    unsigned long int  contNumItem = contourSeq.getNumberOfItems();
    n_curves = contNumItem;
    OFCondition contourDataLoadStatus;



    logstream << class_member << RoiName <<  " -> Contour  sequence size = " << n_curves << std::endl;
    curveVector = std::vector<roi_curve_type>(contNumItem);

    for (unsigned long int iContour = 0; iContour < n_curves; iContour++)
    {
        contourSeq.getItem(iContour, contourItem);

        contourDataLoadStatus = contourItem->getContourData(contourData);
        logstream << class_member << " Contour number " << iContour << " :" << std::endl;
        if (contourDataLoadStatus.good())
        {
            // 	cout << "Contour data load status is good" << endl;
            logstream << class_member <<  RoiName << " ----> Contour data size for contour " << iContour << "  = " << contourData.size() << std::endl;

            logstream << class_member << RoiName << " ----> Contour z value is " << contourData.back() << std::endl;
            curveZlocations.push_back(contourData.back());

            curveVector[iContour].point = std::vector<Point3Float>(contourData.size()/3);
            int iPoint =0;
            curveVector[iContour].npts = contourData.size()/3;

            #ifdef  _WIN32 || defined _WIN64
                        double maxX=-(std::numeric_limits<double>::max)();
                        double maxY=-(std::numeric_limits<double>::max)();
                        double minX=(std::numeric_limits<double>::max)();
                        double minY=(std::numeric_limits<double>::max)();
            #elif __linux__
                        double maxX=-std::numeric_limits<double>::max();
                        double maxY=-std::numeric_limits<double>::max();
                        double minX=std::numeric_limits<double>::max();
                        double minY=std::numeric_limits<double>::max();
            #else
            #error "unknown platform"
            #endif


            for (auto p = contourData.begin(); p != contourData.end(); p = p + 3)
            {

                curveVector[iContour].point[iPoint].x = (*p);
                curveVector[iContour].point[iPoint].y = (*(p + 1));
                curveVector[iContour].point[iPoint].z = (*(p + 2));

                curveVector[iContour].maxX =std::max(curveVector[iContour].point[iPoint].x, curveVector[iContour].maxX);
                curveVector[iContour].maxY =std::max(curveVector[iContour].point[iPoint].y, curveVector[iContour].maxY);
                curveVector[iContour].minX =std::min(curveVector[iContour].point[iPoint].x, curveVector[iContour].minX);
                curveVector[iContour].minY =std::min(curveVector[iContour].point[iPoint].y, curveVector[iContour].minY);

                boundingBox.maxX = std::max(*p, boundingBox.maxX);
                boundingBox.maxY = std::max(*(p + 1), boundingBox.maxY);
                boundingBox.maxZ = std::max(*(p + 2), boundingBox.maxZ);
                boundingBox.minX = std::min(*p, boundingBox.minX);
                boundingBox.minY = std::min(*(p + 1), boundingBox.minY);
                boundingBox.minZ = std::min(*(p + 2), boundingBox.minZ);
                iPoint++;
            }
        }
        else
        {
            logstream << class_member  << "          " << "ERORR --> Failed to read  contour data " << std::endl;
            return false;
        }
    } // end for iContour


    return true;
}

bool ROI2::setCTImageSet(std::vector<CTImage> *_ctImageSet)
{
    ctImageSet = _ctImageSet;
    return true;
}




int ROI2::closestNumberIndexInVectorLessThanOrEqualToValue(const std::vector<float> &vec, float value)
{


    auto it = std::min_element(vec.begin(), vec.end(), [=] (float x, float y)
    {
        return (abs(x - value) <= abs(y - value)) & (x<=value);
    });


    if (it == vec.end()) { return -1; }

    return std::distance(vec.begin(), it);
}

int ROI2::closestNumberIndexInVectorGreaterThanOrEqualToValue(const std::vector<float> &vec, float value)
{

    auto it = std::min_element(vec.begin(), vec.end(), [=] (float x, float y)
    {
        return (abs(x - value) <= abs(y - value)) & (x>=value);
    });


    return std::distance(vec.begin(), it);
}


// http://stackoverflow.com/questions/21682716/create-a-histogram-using-c-with-map-unordered-map-the-default-value-for-a-non
template <typename T>
std::map<T, unsigned int> ROI2::histogram(std::vector<T> &a){
    std::map<T, unsigned int> hist;
    for (auto &x : a){
        hist[x]++;
    }
    return hist;
}
