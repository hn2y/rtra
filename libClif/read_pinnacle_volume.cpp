/**
 *  @file    read_pinnacle_volume.cpp
 *  @author  Hamidreza Nourzadeh
 *  @date    10/09/2016
 *  @version 1.0
 *
 *  @brief This function loads volume into ImageSet_type.volumeRawData
 *
 *  @section DESCRIPTION
 *
 *
 */
//
#include "case_info.h"
#include "someHelpers.h"

int read_pinnacle_volume(ImageSet_type& imageSet)
{
    if (!imageSet.isImageHeaderLoaded)
    {
        std::cout << "ImageHeader is not loaded for imageSet" << std::endl;
        return FAIL;
    }

    if (imageSet.isVolumeDataLoaded)
    {
        std::cout << "Volume Raw Data is already loaded. Reloading it again:" << std::endl;
    }

    //    size_t expectedSizeVolumeInBytes = imageSet.imageHeader.bytes_pix*imageSet.imageHeader.x_dim*imageSet.imageHeader.y_dim*imageSet.imageHeader.z_dim;
    size_t expectedSizeVolume = imageSet.imageHeader.x_dim*imageSet.imageHeader.y_dim*imageSet.imageHeader.z_dim;
    // TODO: We should cast image data pixles to specific format for instance float.
    if (!readBinayFileQt(imageSet.dataFilePath,static_cast<DataByteOrder>(imageSet.imageHeader.byte_order),expectedSizeVolume,imageSet.volumeDataUint16))
    {
        std::cout << "ERROR: when reading file " << imageSet.dataFilePath << std::endl;
        return FAIL;
    }



    imageSet.volumeXGridDataVectorInCm = std::vector<float>(imageSet.imageHeader.x_dim);
    imageSet.volumeYGridDataVectorInCm = std::vector<float>(imageSet.imageHeader.y_dim);
    imageSet.volumeZGridDataVectorInCm = std::vector<float>(imageSet.imageHeader.z_dim);

    // XgridInWOrldCoordinate =  (floatIndexCenterOfVoxelsInImageCoordX-0.5)*VoxelSizeX  = ((1:x_dim)-0.5)*x_pixdim
    // YgridInWOrldCoordinate =  (floatIndexCenterOfVoxelsInImageCoordY-y_dim+0.5)*-VoxelSizeY  = replace floatIndexCenterOfVoxelsInImageCoordY with ((1:y_dim)-0.5)
    // ZgridInWOrldCoordinate =  (floatIndexCenterOfVoxelsInImageCoordZ-0.5)*VoxelSizeZ  = ((1:x_dim)-0.5)*x_pixdim

    for (int ix = 0; ix < imageSet.imageHeader.x_dim; ++ix) {
        if (imageSet.isStartWithDICOM)
        {
            imageSet.volumeXGridDataVectorInCm[ix]= (imageSet.imageHeader.x_start_dicom + (static_cast<float>(ix))*imageSet.imageHeader.x_pixdim);
        }
        else
        {
            imageSet.volumeXGridDataVectorInCm[ix]= (imageSet.imageHeader.x_start + (static_cast<float>(ix))*imageSet.imageHeader.x_pixdim);
        }
    }

    for (int iy = 0; iy < imageSet.imageHeader.y_dim; ++iy) {
        if (imageSet.isStartWithDICOM)
        {
            imageSet.volumeYGridDataVectorInCm[iy]= (imageSet.imageHeader.y_start_dicom + (static_cast<float>(imageSet.imageHeader.y_dim) -static_cast<float>(iy)-1.0)*imageSet.imageHeader.y_pixdim);
        }
        else
        {
            imageSet.volumeYGridDataVectorInCm[iy]= (imageSet.imageHeader.y_start + (static_cast<float>(imageSet.imageHeader.y_dim) -static_cast<float>(iy)-1.0)*imageSet.imageHeader.y_pixdim);
        }
    }
    for (int iz = 0; iz < imageSet.imageHeader.z_dim; ++iz) {
        imageSet.volumeZGridDataVectorInCm[iz]= (imageSet.imageHeader.z_start + (static_cast<float>(iz))*imageSet.imageHeader.z_pixdim);
    }


    //    auto it = max_element(std::begin(imageSet.volumeDataUint16), std::end(imageSet.volumeDataUint16));
    //    auto it2 = min_element(std::begin(imageSet.volumeDataUint16), std::end(imageSet.volumeDataUint16));
    //    qDebug() << "max(volumeDataUint16) = "  << *it ;
    //    qDebug() << "min(volumeDataUint16) = "  << *it2 ;

    imageSet.isVolumeDataLoaded=true;
    return OKFlag;
}
