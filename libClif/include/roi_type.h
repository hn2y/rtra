/** \file
    \author JVS, PJK

    roi_type.h

    This header file defines the structures used to read/write Pinnacle ROI's
    ROI's (regions of interest) are also known as contours and structures, and are used in
    the treatment planning program (e.g. Pinnacle) to identify regions of which some quantity
    (e.g. dose) is of interest
**/
/*
   Copyright 2000-2006 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* roi_type.h 

   Created: Jan 25, 1999: JVS:
   
   Modification History:
      Dec 26, 2000: PJK: Added name,volume and com,area to roi type and roi curve type
      Jan 6, 2000: JVS: name[MAX_STR_LEN] changed to name{MAX_ROI_NAME_LEN] since MAX_STR_LEN not defined
      July 25, 2002: PJK: Added write_pinnacle_roi
      August 29, 2002: PJK/RG: added volume_name to roi
      January 22, 2003: PJK: Added color to roi
      February 14,2005: KW: Added a new structure roi_mesh_type to roi_type. 
      This reads in the new varables roi: vertices, triangles, for 
      surface_mesh and  mean_mesh for Pinnacle 7.7 
      Nov 15, 2006: JVS: Start commenting using dooxygen
      Feb 24, 2011: JVS: Merge in additions from RCF December 14, 2007: CY: Added a new structure vtk_mesh_type
      May 16, 2012: JVS: remove roi_name from call to write_pinnacle_roi since not used
      Oct 2, 2016: HN : Added a function named scan_pinnacle_roi_full_prop which fills a new struct called roi_prop that contains all properties
                        listed in the roi file except data(i.e. curve and surface_mesh). This helps to filter rois based on properties.
      Oct 17, 2016 : HN : Got rid of pointer and used STL container to avoid memory leakage, also add some new members like roi_type


*/

#ifndef ROI_TYPE_H_INCLUDED
#define ROI_TYPE_H_INCLUDED

#ifndef MAX_ROI_NAME_LEN
#define MAX_ROI_NAME_LEN 200  ///< The maximum length of a string
#endif                        

#include "typedefs.h"    ///< Include the basic types
#include <vector>
typedef struct {
    int npts;             ///< Number of points on a curve // HN not need this anymore since we are using vector
    std::vector<Point_3d_float> point; ///< Pointer to the actual points
    Point_2d_float com;    ///< Unknown, added by PJK
    float area;          ///< Unknown, added by PJK
    float maxX;         //HN
    float minX;         //HN
    float maxY;         //HN
    float minY;         //HN
} roi_curve_type;          ///< A "curve" or contour of an ROI on a single slice

typedef struct {
  int n_vertices;
  int n_triangles;
  std::vector<Point_3d_int> triangle;
  std::vector<Point_3d_float> vertex;
} roi_mesh_type;           ///< The mesh-type of a Pinnacle ROI

typedef struct {
    float xMin;
    float xMax;
    float yMin;
    float yMax;
    float zMin;
    float zMax;
} bounding_cuboid_type;

// HN
typedef struct {
    std::string name;
    std::string volume_name;
    std::string stats_volume_name;
    std::string author;
    std::string organ_name;
    int flags;
    std::string roiinterpretedtype;
    std::string color;

//    int box_size;
//    int line_2d_width;
//    int line_3d_width;
//    int paint_brush_radius;
//    bool paint_allow_curve_closing;
    float curve_min_area;
    float curve_overlap_min;
    int lower;
    int upper;
    float radius;
    float density;
    std::string density_units;
    int override_data; // bool ?
    int override_order; // bool ?
    int override_material ; // bool ?
    std::string material;
    int invert_density_loading;
    float volume;
    int pixel_min;
    int pixel_max ;
    float pixel_mean;
    float pixel_std;
    int bBEVDRROutline; // bool?
    int display_on_other_vols; // bool ?
    int is_linked ; // bool ?
    int auto_update_contours; // bool
    std::string UID;
    float stoppingpower; // float?
    int ctnumber ;
    int is_created_by_atlasseg; // bool ?
    int is_warped; // bool ?
    int n_curves;         ///< The number of contours
    std::vector<roi_curve_type> curveVector; ///< The contours
    std::vector<roi_mesh_type> meshVector;   ///< For the "mesh" format
    // Custom members which are not in the header
    float mass;
    int indexImageSet;
    std::vector<bool> binaryBitmap;
    std::vector<float> binaryBitmapPointCoordinatesX;
    std::vector<float> binaryBitmapPointCoordinatesY;
    std::vector<float> binaryBitmapPointCoordinatesZ;
    std::vector<short int> binaryBitmapPointIndicesX;
    std::vector<short int> binaryBitmapPointIndicesY;
    std::vector<short int> binaryBitmapPointIndicesZ;
    std::vector<unsigned int> indicesNonzerosElemInBinaryBitmap; // column major
    std::vector<float> partialVolumeOfBinaryBitmapVoxels;
    std::vector<float> massOfBinaryBitmapVoxels;

    std::vector<unsigned int short> numCornerInsidePolygonForNonzeroPointsOfBinaryBitmap;
    bool bVoxelBitStartState;
    std::vector<unsigned int> voxelBitStateChange;
    std::vector<float> curveZLocations;
    bool isDataLoaded;
    bool isBitmapComputed;
    bool isMassComputed;
    bounding_cuboid_type boundingCuboid;
    bool IsTargetVolume;
    std::vector<unsigned int> DisplayColor;
    Point_3d_float RoiCOM;
} roi_prop;



typedef struct {
    int n_curves;         ///< The number of contours
    std::vector<roi_curve_type> curve; ///< The contours
    std::vector<roi_mesh_type> mesh;   ///< For the "mesh" format
   char name[MAX_ROI_NAME_LEN];
   char volume_name[MAX_ROI_NAME_LEN];
   char stats_volume_name[MAX_ROI_NAME_LEN];
   char color[MAX_ROI_NAME_LEN];
   float volume;
} roi_type;   ///< general type for storing roi information

typedef struct {
   int numberOfPoints;
   Point_3d_longint pointIndex;
}vtk_polygon_type;

typedef struct {
  int numberOfPoints;
  int numberOfPolygons;
  Point_3d_float* points;
  vtk_polygon_type* polygons;
}vtk_mesh_type;


int read_pinnacle_3d_points(FILE *fp, int npoints, Point_3d_float *point);
int read_pinnacle_3d_vertices(FILE *fp, int npoints, Point_3d_float *point);
int read_pinnacle_3d_triangles(FILE *fp, int npoints, Point_3d_int *triangle);
int read_pinnacle_roi(char *pbeam_fname_stem,
                      char *roi_name,
                      roi_type *roi);
int scan_pinnacle_roi_full_prop(char *pRoiFileName,
                              std::vector<roi_prop> &roi_full_prop,
                             int *nfound,
                             int max_roi);
//This method failed
//int read_pinnacle_roi_full_prop(char *RoiFilename,std::vector<roi_prop2> &roi_full_prop,int max_roi);

int read_pinnacle_roi_data_for_given_roiName(char *pRoiFileName,std::string roiName,std::vector<roi_prop> &roi_full_prop );

int scan_pinnacle_rois(char *pbeam_fname_stem,
                      char **roi_list, int *nfound,
                      int roi_max);


int sortROICurvesByZCoordinate(roi_type *roi);

int write_pinnacle_roi(char *pbeam_fname_stem,
		       //                      char *roi_name,
                      roi_type *roi);
int write_pinnacle_roi(char *pbeam_fname_stem,int mesh_on,
		       //                      char *roi_name,
                      roi_type *roi);

int writePinnacleRoi(char *pbeam_fname_stem,int mesh_on,
                      char *roi_name,
                      roi_type *roi);
int readVTKSurfaceMesh(char *vtkFileName, vtk_mesh_type *vtkMesh);

int writeVTKSurfaceMesh(char *vtkFileName, vtk_mesh_type *vtkMesh);

#endif
/* ***************************************************************************************************** */
