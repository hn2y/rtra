// CUDA-C includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <iostream>
#include "math_constants.h"


extern "C"
void negateAll(float *vectorToNegate, int vecSize);

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		//fprintf(stdout, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}


__global__ void negate_cuda(float *vectorToNegate, int vecSize)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx >= vecSize)
	{
		return;
	}

	float x = vectorToNegate[idx];

	vectorToNegate[idx] = -x;
}


void negateAll(float *vectorToNegate, int vecSize) {

	//  number of kenel to lanuch 
	int n_threads_per_block = 1 << 10;  // 1024 threads per block // TODO: note: optimal value is harware dependent
	int n_blocks = ceil((double)vecSize / (double)n_threads_per_block);
	std::cout << "number blocks used in negate.cu = " << n_blocks << std::endl;
	std::cout << "threads per blocks  used in negate.cu= " << n_threads_per_block << std::endl;

	negate_cuda << <n_blocks, n_threads_per_block >> >(vectorToNegate,vecSize);

	gpuErrchk(cudaPeekAtLastError());
	gpuErrchk(cudaDeviceSynchronize());
}
