
/*
 * Header Name: rcfGraphUtilMgr.cpp
 * Author: Mirek Fatyga
 * Date: .....
 */
/**
    \class rcfGraphUtilMgr
    @ingroup util
    
    This class holds functions related to operations on DVFs. 

    History:
       January 18, 2008: CY added class WarpVolumeTrilinear. This method
                            is using trilinear interpolation to warp the
                            data structure cfVolume
       January 24, 2008: CY added class WarpVolumeNearestNeighbor
       February 03, 2008: CY added getTargetIndex which will return the targetIndex.
                             This is the index of the lower-left corner of the 
                             voxel which contains targetPoint

*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "rcfGraphUtilMgr.hpp"
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"

#define LERP(a,l,h)	((l)+(((h)-(l))*(a))) // from GraphicsGems.h

float rcfGraphUtilMgr::errorMessage(int x0, int y0, int z0, int xsize, int ysize, int zsize)
{
  printf("\nERROR: Array (%d, %d, %d) out of bounds (%d, %d, %d) in trilerp_ c.cc. \nEXITING\n", x0, y0, z0, xsize, ysize, zsize);   
  return CF_FAIL;
}

int rcfGraphUtilMgr::trilinear(cfPoint3D<float> *p, float *d, int xsize, int ysize, int zsize, float *dxyz)
{
#   define DENS(X, Y, Z) d[(X)+xsize*((Y)+ysize*(Z))]

    int        x0, y0, z0,
               x1, y1, z1;
    float       *dp,
               fx, fy, fz,
               d000, d001, d010, d011,
               d100, d101, d110, d111,
               dx00, dx01, dx10, dx11,
               dxy0, dxy1;

    x0 = (int)floor(p->x); fx = p->x - x0;
    y0 = (int)floor(p->y); fy = p->y - y0;
    z0 = (int)floor(p->z); fz = p->z - z0;

    x1 = x0 + 1;
    y1 = y0 + 1;
    z1 = z0 + 1;

    if (x0 >= 0 && x1 <= xsize &&
        y0 >= 0 && y1 <= ysize &&
        z0 >= 0 && z1 <= zsize)
    {
        dp = &DENS(x0, y0, z0);
        d000 = dp[0];
        d100 = dp[1];
        dp += xsize;
        d010 = dp[0];
        d110 = dp[1];
        dp += xsize*ysize;
        d011 = dp[0];
        d111 = dp[1];
        dp -= xsize;
        d001 = dp[0];
        d101 = dp[1];
    }
    else
    {
#       define INRANGE(X, Y, Z) \
                  ((X) >= 0 && (X) <= xsize && \
                   (Y) >= 0 && (Y) <= ysize && \
                   (Z) >= 0 && (Z) <= zsize)

        d000 = INRANGE(x0, y0, z0) ? DENS(x0, y0, z0) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d001 = INRANGE(x0, y0, z1) ? DENS(x0, y0, z1) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d010 = INRANGE(x0, y1, z0) ? DENS(x0, y1, z0) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d011 = INRANGE(x0, y1, z1) ? DENS(x0, y1, z1) : errorMessage(x0, y0, z0, xsize, ysize, zsize);

        d100 = INRANGE(x1, y0, z0) ? DENS(x1, y0, z0) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d101 = INRANGE(x1, y0, z1) ? DENS(x1, y0, z1) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d110 = INRANGE(x1, y1, z0) ? DENS(x1, y1, z0) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
        d111 = INRANGE(x1, y1, z1) ? DENS(x1, y1, z1) : errorMessage(x0, y0, z0, xsize, ysize, zsize);
    }
    dx00 = LERP(fx, d000, d100);
    dx01 = LERP(fx, d001, d101);
    dx10 = LERP(fx, d010, d110);
    dx11 = LERP(fx, d011, d111);

    dxy0 = LERP(fy, dx00, dx10);
    dxy1 = LERP(fy, dx01, dx11);

    *dxyz = LERP(fz, dxy0, dxy1);

    return(CF_OK);
}
