/* Copyright 2105: University of Virginia
   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* Modification History:
   May 6, 2015: JVS: Created
   Dec 5, 2015: JVS: Added planDir

*/

#ifndef classBasedPinnacle_h_included
#define classBasedPinnacle_h_included

#include <string>
using namespace std;
#include "read_clif.h"
#include "typedefs.h"

class planInfoClass {
 public:
  planInfoClass(){};
  ~planInfoClass(){};
  bool readPinnaclePlanInfo();
  bool readPinnaclePlanInfo(const char *);
  string PatientName;
  string Institution;
  string PlanName;
  string PatientID;
  string LastName;
  string FirstName;
  string MiddleName;
  string MedicalRecordNumber;
  string PlanPath;
  string PrimaryImageType;
  void setPlanDir(const char *newPlanDir){planDir = newPlanDir;}
 private:
  string planDir;
};


class mlcApertureClass {
 public:
  mlcApertureClass(){
    nLeafPairs=0;
    leaf = NULL;
  };
  ~mlcApertureClass(){
    if(NULL != leaf) free(leaf);
  };
  int nLeafPairs;
  Point_2d_float *leaf;
};


class controlPointClass {
 public:
  controlPointClass();
  controlPointClass(clifObjectType *cpSegment);
  ~controlPointClass();
  float Gantry;
  float Couch;
  float Collimator;
  float LeftJawPosition;
  float RightJawPosition;
  float TopJawPosition;
  float BottomJawPosition;
  float Weight;
  int WeightLocked;
  float PercentOfArc;
  float DoseRate;
  float DeliveryTime; 
  mlcApertureClass *mlc;
  bool readCPSegment(clifObjectType *cpSegment);
  bool readCPMLCPositions(clifObjectType *cpMLCPositions);
};

class cpManagerClass {
 public:
  cpManagerClass();
  cpManagerClass(clifObjectType *cpManager);
  ~cpManagerClass();
  bool readPinnacleCPManager(clifObjectType *cpManager);
 
  int IsGantryStartStopLocked;
  int IsCouchStartStopLocked;
  int IsCollimatorStartStopLocked;
  int IsLeftRightIndependentLocked;
  int IsLeftRightIndependent;
  int IsTopBottomIndependent;
  int NumberOfControlPoints;
  int getNumberOfControlPoints(){ return NumberOfControlPoints;};
  controlPointClass *CP;
};

class beamClass {
 public:
  beamClass();
  beamClass(clifObjectType *);
  ~beamClass(){
  };
  bool readPinnacleBeam(clifObjectType *);
  bool exportMLCForBeam(planInfoClass PlanInfo, string trialName);
  string Name;
  string IsocenterName;
  string MachineNameAndVersion;
  string MachineName;
  string MachineVersion;
  string Modality;
  string MachineEnergyName;
  string SetBeamType;
  int UseMLC;
  //  int mlcDefined;
  float Weight;
  cpManagerClass CPManager;
  // MonitorUnitInfoClass *MonitorUnitInfo;
};

class trialClass {
 public:
  trialClass(){
    Beam=NULL;
    nBeams=0;
  };
  ~trialClass(){
    //if(NULL != Beam) delete[] Beam; // will leak like crazy w/o delete, but I am getting seg faults on my deletes...
  };
  string Name;
  beamClass *Beam;
  int nBeams;
};
class planClass {
 public:
  planClass(){
    Trial=NULL;
    nTrials=0;
    planDir=".";
    PlanInfo.setPlanDir(planDir.c_str());
  };
  ~planClass(){
    //if( NULL != Trial) delete[] Trial; // will leak like crazy w/o delete, but I am getting seg faults on my deletes...
  };
  bool readSpecificTrial(const char *);
  void setPlanDir(const char *newPlanDir){
    planDir = newPlanDir;
    PlanInfo.setPlanDir(planDir.c_str());
  };
  //
  trialClass *Trial;
  int nTrials;
  planInfoClass PlanInfo;
 private:
  string planDir;
};
#endif
