/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* ct_routines.cc
   routines to deal with CT image
   Modification History:
      April 26, 2000: JVS: Cut routines from image_contour.cc
      June 30, 2000: JVS:  Check for non-equispace images...
                           just bomb for now if they are found....
      Jan 24, 2002: PJK/SSV Problems with read_ct_image. Because opening as r not rb?
      Jan 28, 2002: PJK/SSV added write_ct_header and write_ct_image
      Mar 14, 2002: PJK adding write read_ct_imageInfo
      July 25, 2002: PJK added read_ct_imageInf
      Sept 30, 2004: PJK/KW added byte_order to read/write header
      Feb 10, 2005: JVS: Add readCTImageAndHeader
                         Add checking of byte order and swabbing if differs from hardware byte order
                         Modify in int read_ct_image so that it calls ITYPE * read_ct_image
                         to minimize replicate coding...
      Feb 11, 2005: Got rid of incompatible new's and replaced by calloc's
      June 12, 2007: JVS: Change checkForNonEquispacedImage so used % of voxel tolerance....Set to 5%
      Nov 5, 2007: JVS: get rid of myerrno
      Dec 27, 2007: JVS: change open_file to fopen for ImageInfo file so not give false ERROR message when the file does not exist.... (could have used fileExists)
      Feb 8, 2008: JVS: Add maxMin defintions....
      June 4, 2008: JVS: Updates for Sun CC
      June 26, 2008: JVS: clean up output messages
      Mar 17, 2010: JVS: Add prototype for  reverse_short_byte_order
      April 29, 2011: JVS: x_start_dicom and y_start_dicom are read and used if Version = 9.0 or greater
      June 2, 2011: JVS: Add explicit reading of Version, x_start_dicom, and y_start_dicom
                         NOTE: use of x_start_dicom and y_start_dicom are NOT based on Version = 9.0 and greater, 
                         but based on Pinnacle Parameter StartWithDICOM, which is an active Pinnacle variable, 
                         and is listed in the file plan.Pinnacle
                         Change code to use value from that file ----
      March 3, 2012: JVS: Add in reading of db_name;
      May 26, 2013: JVS: Make failure to read in Version a warning.
      May 5, 2015: JVS: add <cmath> for yak solaris compile

*/
#include <stdio.h>
#include <stdlib.h> // calloc and free
#include <string.h> // for strcmp
#include <math.h> // for fabs
#include <cmath>
using namespace std;
#include "utilities.h"
#include "option.h"
#include "string_util.h"
#include "typedefs.h"
#include "translation.h"
#include "ct_routines.h"
#include "read_clif.h"
#include "readWriteEndianFile.h" // For reverse_short_byte_order
////
/// Feb 8, 2008: JVS: Unknown why now need to define MINMAX ???
/* #define max and min */
//#if !defined( __MINMAX_DEFINED)
#define __MINMAX_DEFINED
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define __max       max
#define __min       min
//#endif /*__MINMAX_DEFINED */

/* *********************************************************************** */
typedef struct imageZType{
   int   Slice;
   float Z;
}imageZTypeStruct;
#define MAX_DIFF 0.0001 // difference between numbers to be called the same
/* *********************************************************************** */
int reportImageStatistics(image_header_type *imageHeader, ITYPE *image)
{
  printf("\n Image nx, ny, nz: %d, %d, %d", imageHeader->x_dim, imageHeader->y_dim, imageHeader->z_dim);
  // printf("\n sizeof(ITYPE) = %d", sizeof(ITYPE));
  // printf("\n image[0] = %d, image[1000] = %d", (int) image[0], (int) image[1000]);
  int nVoxels= imageHeader->x_dim*imageHeader->y_dim*imageHeader->z_dim;
  ITYPE minImage=image[0];
  ITYPE maxImage=image[0];
  for(int iVoxel=0; iVoxel<nVoxels; iVoxel++)
  {
    minImage = min(minImage,image[iVoxel]);
    maxImage = max(maxImage,image[iVoxel]);
  }
  printf ("\n Image min = %d\n Image max = %d", (int) minImage, (int) maxImage);
  //  cout << "\n Image min = " << minImage ;
  // cout << "\n Image max = " << maxImage ;
  return(OK);
}
/* *********************************************************************** */
int checkForNonEquispacedImage(char *imageFilename) 
{  // Use this later as prototype for actually reading/using the image sectionZ's 
  // printf("\n Checking of NonEquispaced Images\n");
  FILE *imageStrm;
  char fullFileName[MAX_STR_LEN];
  sprintf(fullFileName,"%s.ImageInfo",imageFilename);
  // imageStrm = open_file(imageFilename,".ImageInfo","r");
  imageStrm = fopen(fullFileName,"r");
  if(NULL == imageStrm )  {
    printf ("\n WARNING: %s does not exist, using default image spacing",fullFileName);
  } 
  else // found ImageInfo file so read it
  {
     /* read in the ClifObjects */
     clifObjectType *ImageInfoList = new(clifObjectType); // allocate memory
     // read in the Clif
     if( readClifFile(imageStrm, ImageInfoList,"ImageInfoList") != OK)
     {
        printf("\n ERROR: Reading clif from file %s\n", imageFilename); return(FAIL);
     }
     // printf("\n Detected %d Objects",ImageInfoList->nObjects);
     // Feb 11, 2005: g++ complains about variable sized array with "new"
     // imageZType *imageInfo = new( imageZType [ImageInfoList->nObjects] );
     // use "calloc", which I am more familiar with
     imageZType *imageInfo = (imageZType *) calloc(ImageInfoList->nObjects, sizeof(imageZType) );
     if(NULL == imageInfo) {
       printf("\n ERROR: Allocating memory for imageInfo"); return(FAIL);
     }
     // Parse out the Values into a structure
     for(int iObject=0; iObject<ImageInfoList->nObjects;iObject++)
     {
        char sliceName[MAX_STR_LEN];
        sprintf(sliceName,"#\"#%d\".SliceNumber",iObject);
        char zName[MAX_STR_LEN];
        sprintf(zName,"#\"#%d\".TablePosition",iObject);
        if( readIntClifStructure  (ImageInfoList, sliceName, &imageInfo[iObject].Slice) != OK  ||
            readFloatClifStructure(ImageInfoList, zName,     &imageInfo[iObject].Z ) != OK  )
	{
           printf("\n ERROR: Reading object %d", iObject);
           return(FAIL);
        }
        // printf("\n Found Slice %d at %f", imageInfo[iObject].Slice,imageInfo[iObject].Z);
     }
     int nSlices = ImageInfoList->nObjects;
     delete ( ImageInfoList );
     fclose(imageStrm);

     /* ************************* end of the read routine ************************** */
     /* Check for equispaced slices */
     // float Max_Allowed_Diff=1e-4;
     float SliceMissingDiff=0.1f;
     float maxSliceTolerance=0.05f;
     // float averageSliceSpacing =  imageInfo[1].Z - imageInfo[0].Z;
     float averageSliceSpacing = (float) (fabs(imageInfo[nSlices-1].Z - imageInfo[0].Z) / ((float) (nSlices-1)));
#ifdef DEBUG
     printf ("\n Average image slice spacing is %f", averageSliceSpacing);
#endif
     // printf ("\n\tslice[0] = %f", imageInfo[0].Z);
     // printf ("\n\tslice[%d] = %f", nSlices, imageInfo[nSlices-1].Z);
     for(int iSlice=1; iSlice<nSlices; iSlice++)
     {
       float idealSlicePosition=imageInfo[0].Z + ((float) (iSlice))*averageSliceSpacing;
       if ( fabs(idealSlicePosition -  imageInfo[iSlice].Z)/averageSliceSpacing > maxSliceTolerance) 
       {
           printf("\n ERROR: Non-uniform CT image spacing detected at slice %d", iSlice);
           printf("\n\t Expected Slice Position of %f", idealSlicePosition);
           printf("\n\t Actual Slice Position of %f", imageInfo[iSlice].Z);
           printf("\n\t Differance = %f or %f%% of slice spacing", idealSlicePosition - imageInfo[iSlice].Z, 100.0* fabs(idealSlicePosition -  imageInfo[iSlice].Z)/averageSliceSpacing);
           printf("\n\t Tolerance is %f%% of a slice", 100.0*maxSliceTolerance);
           printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
           printf("\n\t Slice %d, Z = %f",iSlice-1,imageInfo[iSlice-1].Z);
           printf("\n\t Slice Spacing = %g",  (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z));
           if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - averageSliceSpacing ) > SliceMissingDiff )
             printf("\n\tThere is probably a slice missing");
           return(FAIL);
       }
       /*
         // printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
        float currentSliceSpacing=imageInfo[nSlices-1].Z - imageInfo[0].Z;
        // if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - averageSliceSpacing ) > Max_Allowed_Diff )
        if( fabs(1.0-currentSliceSpacing/averageSliceSpacing ) > 0.025 )
	{
           printf("\n ERROR: Non-uniform CT image spacing detected at slice %d", iSlice);
           printf("\n\t Expect Spacing of %f", averageSliceSpacing);
           printf("\n\t Found  Spacing of %f", (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z));
           printf("\n\t Differance found of %g",  (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z) - averageSliceSpacing);
           printf("\n\t Maximum Allowed Differance is %g", Max_Allowed_Diff);
           printf("\n\t Slice %d, Z = %f",iSlice,imageInfo[iSlice].Z);
           printf("\n\t Slice %d, Z = %f",iSlice-1,imageInfo[iSlice-1].Z);
           if( fabs( (imageInfo[iSlice].Z - imageInfo[iSlice-1].Z ) - averageSliceSpacing ) > SliceMissingDiff )
             printf("\n\tThere is probably a slice missing");
           return(FAIL);
        }
       */
     }
     free(imageInfo);
  }
  return(OK);
}
/* *********************************************************************** */
int readCTImageAndHeader(char *imageFileName, image_header_type **imageHeader, ITYPE **image)
{
  // Reads in header and the image
  // Pointers to pointers of image_header and image 
  // see convertPinnacleCTToDensities.cc for example usage
  image_header_type *imageHeaderLocal = (image_header_type *) calloc(1, sizeof(image_header_type));
  if(NULL==imageHeaderLocal)
  {
    printf("\n ERROR: allocation image header\n"); return(FAIL);
  }

  if(OK !=read_image_header(imageFileName, imageHeaderLocal))
  {
    printf("\n ERROR: reading image header %s\n",imageFileName); return(FAIL);
  }
  printf("\n Done reading header\n");
  /* read in ct-image */
  ITYPE *imageLocal;
  imageLocal = read_ct_image(imageFileName, imageHeaderLocal);
  if(NULL == imageLocal)
  {
    printf("\n ERROR: reading in ct image %s\n",imageFileName); return(FAIL);
  }
  *image = imageLocal;
  *imageHeader = imageHeaderLocal;
  return(OK);
}
/* *********************************************************************** */
int read_ct_image(char *image_fname, image_header_type *img_header, ITYPE *image)
{
  image = read_ct_image(image_fname, img_header);
  if(NULL == image) {
    printf("\n ERROR: Reading CT image %s", image_fname); return(FAIL);
  }
  return(OK);
}
/* ************************************************************************* */
// JVS: int read_ct_image changed to above since otherwise have replicate functions
int old_read_ct_image_dont_use_me(char *image_fname, image_header_type *img_header, ITYPE *image)
{
  /* Check for non-eqs image */
  if(checkForNonEquispacedImage(image_fname) != OK)
  {
     printf("\n ERROR: Non-equispaced Image Detected in read_ct_image");
     return(FAIL);
  }
  /* Open the image stream */
  FILE *image_strm;
  image_strm = open_file(image_fname,".img","r");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.img \n",image_fname);return(FAIL);
  }

  /* allocate memory for the image */
  size_t image_size = img_header->x_dim*img_header->y_dim*img_header->z_dim;

  image = (ITYPE *)calloc(image_size,sizeof(ITYPE));
  if(image == NULL){
     eprintf("\n ERROR: Cannot allocate memory for image\n");return(FAIL);
  }
#ifdef DEBUG_CT
  printf("\n Reading in the CT image %s", image_fname);
  printf("\n Currently at byte %ld",ftell(image_strm));
#endif
  size_t sread = fread(image,sizeof(ITYPE),image_size,image_strm);
  /* close the data stream */
  fclose(image_strm);

#ifdef DEBUG_CT
   printf("\n");
   for(int k=0; k < img_header->z_dim; k++) /* Loop over EACH image */
   {
     // fill matrix
     for(int j=0; j < img_header->y_dim; j++)
     {
       for(int i=0; i < img_header->x_dim; i++)
	 {
           if ((k==0) && (j%100 == 0) && (i%100 == 0) )
	     {
	       printf("%d %d %d %d %d\t",image[i + img_header->x_dim * ( j + k * img_header->y_dim)],i + img_header->x_dim * ( j + k * img_header->y_dim), i, j, k);
	     }
	 }
       if ( (k==0) && (j%100 == 0) )
	 {
	   printf("\n");
	 }
     }
   }
#endif
#ifdef DEBUG_CT
  printf("\n %u floats read from %s.img",sread, image_fname);
  printf(" (%u bytes )",sread*sizeof(ITYPE));
#endif
  if(sread != image_size) return(FAIL);
  return(OK);
}
/* *********************************************************************** */
ITYPE *read_ct_image(char *image_fname, image_header_type *img_header)
{
  /* Check for non-eqs image */
  if(checkForNonEquispacedImage(image_fname) != OK)
  {
     printf("\n ERROR: Non-equispaced Image Detected in read_ct_image");
     return(NULL);
  }
  /* Open the image stream */
  FILE *image_strm;
  image_strm = open_file(image_fname,".img","r");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.img \n",image_fname);return(NULL);
  }

  /* allocate memory for the image */
  size_t image_size = img_header->x_dim*img_header->y_dim*img_header->z_dim;

  ITYPE *image = (ITYPE *)calloc(image_size,sizeof(ITYPE));
  if(image == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(NULL);
  }
#ifdef DEBUG_CT
  printf("\n Reading in the CT image %s", image_fname);
#endif
  size_t sread = fread(image,sizeof(ITYPE),image_size,image_strm);
  /* close the data stream */
  fclose(image_strm);
#ifdef DEBUG_CT
  printf("\n %u floats read from %s.img",sread, image_fname);
  printf(" (%u bytes )",sread*sizeof(ITYPE));
#endif
  if(sread != image_size) return(NULL);

  // Check byte order of the machine and compare it with the byte order of the image,
  // swab if necessary
  // byte_order = 1, then BIG_ENDIAN
  // byte_order = 0, then LITTLE_ENDIAN
  //  img_header->byte_order 
  int swabFlag=0;
  switch( check_byte_order() ) 
  {
     case BIG_ENDIAN:
       printf("\n Current machine is Big Endian");
       if(0 == img_header->byte_order) {
	 printf("\n CT Image Header indicates byte order 0, must SWAB");
         swabFlag=1;
       }
       break;
     case LITTLE_ENDIAN:
       printf("\n Current machine is Little Endian");
      if(1 == img_header->byte_order) {
	 printf("\n CT Image Header indicates byte order 1, must SWAB");
         swabFlag=1;
       }
       break;
     default:
       printf("\n ERROR: indeterminate byte order");
       return(NULL);
  }
  //  reportImageStatistics(img_header, image);
  // swab if required
  if(swabFlag){
    printf("\n Swabbing CT data on input to match current hardware");
    for(unsigned iIndex=0; iIndex<image_size; iIndex++){
      image[iIndex] = reverse_short_byte_order(image[iIndex]);
    }
    // reportImageStatistics(img_header, image);
  }
#ifdef INVERT_IMAGE
  /* Invert the order of the y lines in the image */
  // for(int ib=0; ib<5; ib++) putchar(7);
  printf ("\n ************ CT image is being inverted **********\n");
  ITYPE *iimage = (ITYPE *)calloc(image_size,sizeof(ITYPE));
  if(iimage == NULL){
     eprintf("\n ERROR: Cannot alloate memory for image\n");return(NULL);
  }
  int icnt = 0;
  for(int k=0; k < img_header->z_dim; k++) /* Loop over EACH image */
     for(int j = img_header->y_dim-1; j >= 0; j--) /* Loop Over Each row */
        for(int i=0; i < img_header->x_dim; i++)
        {
           int index = i + img_header->x_dim * ( j + k * img_header->y_dim);
           iimage[icnt++] = image[index];
	}
  free(image);
  return(iimage);
#else
  return(image);
#endif
}
/* *********************************************************************** */
int check_ct_image(image_header_type *head, ITYPE *image)
{  /* checks parameters in CT image */
   unsigned int nimage = head->x_dim*head->y_dim*head->z_dim;
   ITYPE maxpixel = image[0];
   ITYPE minpixel = image[0];
   for(unsigned int i=0; i<nimage; i++)
   {
      maxpixel = max(maxpixel, image[i]);
      minpixel = min(minpixel, image[i]);
   }
   if(head->max_pix != maxpixel)
   {
      printf("\n WARNING: Maximum pixel in image header (%f) is incorrect", head->max_pix);
      head->max_pix = maxpixel;
      printf("\n\t Resetting to correct value of %f", head->max_pix);
   }
   if(head->min_pix != minpixel)
   {
      printf("\n WARNING: Minimum pixel in image header (%f) is incorrect", head->min_pix);
      head->min_pix = minpixel;
      printf("\n\t Resetting to correct value of %f", head->min_pix);
   }
#ifdef DEBUG_CT
   printf("In check CT image\n");
   for(int k=0; k < head->z_dim; k++) /* Loop over EACH image */
   {
     // fill matrix
     for(int j=0; j < head->y_dim; j++)
     {
       for(int i=0; i < head->x_dim; i++)
	 {
           if ((k==0) && (j%100 == 0) && (i%100 == 0) )
	     {
	       printf("%d %d %d %d %d\t",image[i + head->x_dim * ( j + k * head->y_dim)],i + head->x_dim * ( j + k * head->y_dim), i, j, k);
	     }
	 }
       if ( (k==0) && (j%100 == 0) )
	 {
	   printf("\n");
	 }
     }
   }
#endif
   return(OK);
}
/* *********************************************************************** */
int read_image_header(char *image_fname, image_header_type *img_header)
{
  //  printf("\n ---- read_image_header-----");
  /* open image header file */
  FILE *image_header_strm;
  image_header_strm = open_file(image_fname,".header","r");
  if(image_header_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.header \n",image_fname);return(FAIL);
  }
  /* get  dimensions */
  // added byte order 30 Sept 2004 PJK/KW
  img_header->byte_order = (int) get_value_in_file(image_header_strm,"byte_order =");
  img_header->x_dim = (int) get_value_in_file(image_header_strm,"x_dim =");
  img_header->y_dim = (int) get_value_in_file(image_header_strm,"y_dim =");
  img_header->z_dim = (int) get_value_in_file(image_header_strm,"z_dim =");
  img_header->max_pix = get_value_in_file(image_header_strm, "vol_max =");
  img_header->min_pix = get_value_in_file(image_header_strm, "vol_min =");
  img_header->range_low  = 0;   // temp, set for now, compute when more than 1 img type
  img_header->range_high = 4095;// temp, set for now, 
  img_header->x_pixdim = get_value_in_file(image_header_strm,"x_pixdim =");
  img_header->y_pixdim = get_value_in_file(image_header_strm,"y_pixdim =");
  img_header->z_pixdim = get_value_in_file(image_header_strm,"z_pixdim =");
  img_header->x_start = (float) get_value_in_file(image_header_strm,"x_start =");
  img_header->y_start = (float) get_value_in_file(image_header_strm,"y_start =");
  img_header->z_start = (float) get_value_in_file(image_header_strm,"z_start =");
  if(checkStringError() == FAIL) return(FAIL); // check for error set while reading
  if(OK != string_after(image_header_strm, "db_name :",img_header->db_name)) {
    printf("\n ERROR: read_image_header: parsing db_name "); 
    printf("\n \t db_name string not found in header for file %s",image_fname); 
    resetStringError();
    return(FAIL);
  }
  // printf("\n ---- img_header->db_name = %s in file %s", img_header->db_name, image_fname);
  // get Version Number
  //
  {
    strcpy(img_header->version,"Unknown"); // 5/26/2013: JVS: set default
    // Starting in Pinnacle Version 8.1?
    if(OK != string_after(image_header_strm, "Version :",img_header->version)) {
      printf("\n WARNING: read_image_header: parsing Version ");  // 5/26/2013: JVS: not have an error if not have version.
      printf("\n \t Version string not found in header for file %s",image_fname); 
      resetStringError();
      // return(FAIL); // 5/26/2013: JVS: not an error if not have version (e.g. VA)
    }
    img_header->x_start_dicom = (float) get_value_in_file(image_header_strm,"x_start_dicom =");
    img_header->y_start_dicom = (float) get_value_in_file(image_header_strm,"y_start_dicom =");
  }
  if(checkStringError() == FAIL) {
    printf("\n ERROR: string read error found while reading %s", image_fname);
    return(FAIL); // check for error set while reading
  }
#ifdef INVERT_CT_HEADER
  printf("\n *********************Inverted CT header ****************");
#ifdef DEBUG_CT
  printf("\n Original:");
  printf("\n    dim:  x %d y %d z %d", img_header->x_dim,img_header->y_dim,img_header->z_dim);
  printf("\n pixdim:  x %f y %f z %f", img_header->x_pixdim,img_header->y_pixdim,img_header->z_pixdim);
  printf("\n  start:  x %f y %f z %f", img_header->x_start,img_header->y_start,img_header->z_start);
  printf("\n Final:");
#endif
  img_header->y_start = img_header->y_start+(img_header->y_dim-1)*img_header->y_pixdim;
  img_header->y_pixdim*=-1; // reverse the size of the voxel
#endif

#ifdef DEBUG_CT
  printf("\n    dim:  x %d y %d z %d", img_header->x_dim,img_header->y_dim,img_header->z_dim);
  printf("\n pixdim:  x %f y %f z %f", img_header->x_pixdim,img_header->y_pixdim,img_header->z_pixdim);
  printf("\n  start:  x %f y %f z %f", img_header->x_start,img_header->y_start,img_header->z_start);
#endif
  fclose(image_header_strm);
  return(OK);
}
/* *********************************************************************** */
/* Determine CT coordinate transforms to place pixel the first pixel at 0,0,0 */
int determine_ct_translation(image_header_type *img_header, translation_type *t)
{
    /* In general, a point goes through the translation
       new_coordinate = old_coordinate * slope + intercept.
       Thus, for an image that is only shifted, slope = 1 and intercept = -shift...
       i.e, if the original CT data has x_start = -0.5, then must add +0.5 to all x
       coordinates to have the image map properly....
       new_coordinate = old_coordinate*1 + intercept 
       The Y coordinate is more complex... this is because Y is top-down in Pinnacle...
       

*/
   t->x.slope     =  1.0;
   t->x.intercept =  -img_header->x_start;
   t->z.slope     =  1.0;
   t->z.intercept =  -img_header->z_start; 
   // t->z.intercept+=   img_header->z_pixdim; // further offset...just so never < 0
   t->y.slope     = -1.0;
   t->y.intercept =  img_header->y_start + ((float) (img_header->y_dim - 1)) * img_header->y_pixdim;
#ifdef DEBUG_CT
   printf("\n Translation is:");
   printf("\n\t\t X: m=%f b=%f", t->x.slope, t->x.intercept);
   printf("\n\t\t Y: m=%f b=%f", t->y.slope, t->y.intercept);
   printf("\n\t\t Z: m=%f b=%f", t->z.slope, t->z.intercept);
#endif
   return(OK);
}
/* ************************************************************************* */
int write_image_header(char *image_fname, image_header_type *img_header)
{
  /* open image header file */
  FILE *image_header_strm;
  image_header_strm = open_file(image_fname,".header","w");
  if(image_header_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.header \n",image_fname);return(FAIL);
  }
  printf("\nWARNING- not all header components are read in");
  fprintf(image_header_strm,"\tbyte_order = %d;\n",img_header->byte_order);
  fprintf(image_header_strm,"\tread_conversion = \"\";\n");
  fprintf(image_header_strm,"\twrite_conversion = \"\";\n");
  fprintf(image_header_strm,"\tt_dim = 0;\n");
  fprintf(image_header_strm,"\tx_dim = %d;\n\ty_dim = %d;\n\tz_dim = %d;\n",img_header->x_dim,img_header->y_dim,img_header->z_dim);
  fprintf(image_header_strm,"\tdatatype = 1;\n");
  fprintf(image_header_strm,"\tbitpix = 16;\n");
  fprintf(image_header_strm,"\tbytes_pix = 2;\n");
  fprintf(image_header_strm,"\tvol_max = %f;\n",img_header->max_pix);
  fprintf(image_header_strm,"\tvol_min = %f;\n",img_header->min_pix);
  fprintf(image_header_strm,"\tt_pixdim = 0.000000;\n");
  fprintf(image_header_strm,"\tx_pixdim = %f;\n\ty_pixdim = %f;\n\tz_pixdim = %f;\n",img_header->x_pixdim,img_header->y_pixdim,img_header->z_pixdim);
  fprintf(image_header_strm,"\tt_start = 0.000000;\n");
  fprintf(image_header_strm,"\tx_start = %f;\n\ty_start = %f;\n\tz_start = %f;\n",img_header->x_start,img_header->y_start,img_header->z_start);
  fprintf(image_header_strm,"\tz_time = 0.000000;\n");
  fprintf(image_header_strm,"\tdim_units :\n");
  fprintf(image_header_strm,"\tvoxel_type :\n");
  fprintf(image_header_strm,"\tid = 0;\n");
  fprintf(image_header_strm,"\tvis_only = 0;\n");
  fprintf(image_header_strm,"\tdata_type :\n");
  fprintf(image_header_strm,"\tvol_type :\n");
  fprintf(image_header_strm,"\tdb_name : %s\n",img_header->db_name);
  fprintf(image_header_strm,"\tmedical_record :\n");
  fprintf(image_header_strm,"\toriginator :\n");
  fprintf(image_header_strm,"\tdate : 020202\n");
  fprintf(image_header_strm,"\tscanner_id :\n");
  fprintf(image_header_strm,"\tpatient_position : HFS\n");
  fprintf(image_header_strm,"\torientation = 0;\n");
  fprintf(image_header_strm,"\tscan_acquisition = 0;\n");
  fprintf(image_header_strm,"\tcomment :\n");
  fprintf(image_header_strm,"\tfname_format :\n");
  fprintf(image_header_strm,"\tfname_index_start = 0;\n");
  fprintf(image_header_strm,"\tfname_index_delta = 0;\n");
  fprintf(image_header_strm,"\tbinary_header_size = 0;\n");
  fprintf(image_header_strm,"\tVersion : %s;\n",img_header->version);
  fprintf(image_header_strm,"\tx_start_dicom = %f;\n\ty_start_dicom = %f;\n",img_header->x_start_dicom,img_header->y_start_dicom);

    
  fclose(image_header_strm);
  return(OK);
}
/* *********************************************************************** */
int write_ct_image(char *image_fname, image_header_type *img_header, 
ITYPE *image)
{
  /* open image header file */
  FILE *image_strm;
  image_strm = open_file(image_fname,".img","wb");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.header \n",image_fname);return(FAIL);
  }

  size_t image_size = img_header->x_dim*img_header->y_dim*img_header->z_dim;

  fwrite(image,sizeof(ITYPE),image_size,image_strm);
  /* close the data stream */
  fclose(image_strm);
  return(OK);
}
/* *********************************************************************** */
int write_ct_imageInfo(char *image_fname, image_header_type *img_header)
{
  /* open image header file */
  FILE *image_strm;
  image_strm = open_file(image_fname,".ImageInfo","w");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening output file %s.ImageInfo \n",image_fname);return(FAIL);
  }

  for (int k =0;k<img_header->z_dim;k++)
  {
    fprintf(image_strm,"ImageInfo ={\n  TablePosition =   %5.2f;\n  SliceNumber = %d;\n  SeriesUID = \"\";\n  StudyInstanceUID = \"\";\n  FrameUID = \"\";\n  ClassUID = \"\";\n  InstanceUID = \"\";\n};\n",img_header->zSlice[k],img_header->zSliceNumber[k]);
  }

  /* close the data stream */
  fclose(image_strm);
  return(OK);
}
/* *********************************************************************** */
int read_ct_imageInfo(char *image_fname, image_header_type *img_header)
{
  /* open image header file */
  FILE *image_strm;
  image_strm = open_file(image_fname,".ImageInfo","r");
  if(image_strm == NULL)
  {
    eprintf("\n ERROR: opening input file %s.ImageInfo \n",image_fname);return(FAIL);
  }

  for (int k =0;k<img_header->z_dim;k++)
  {
    img_header->zSlice[k] = (float) get_value_in_file(image_strm,"TablePosition =");
    img_header->zSliceNumber[k] = (int) get_value_in_file(image_strm,"SliceNumber =");
  }

  /* close the data stream */
  fclose(image_strm);
  return(OK);
}
/* *********************************************************************** */
