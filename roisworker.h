#ifndef ROISWORKER_H
#define ROISWORKER_H

#include <QObject>
#include "Common/PatientInfo.h"

class ROIsWorker : public QObject
{
    Q_OBJECT

    //Patient *activePatient =NULL;

    bool readPinnacleStructure(QString sFileName);
    bool readDcmStructure(QString sFileName);


public:
    explicit ROIsWorker(QObject *parent = 0);
    ~ROIsWorker();

signals:

public slots:
};

#endif // ROISWORKER_H
