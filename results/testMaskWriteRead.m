close all force

close all force
clear variables
cTImageInfo=readImageHeader('CTVolumePatientAImageSetNKIVCU^PA^S00^I0002.bin.header');



fid=fopen('CTVolumePatientAImageSetNKIVCU^PA^S00^I0002.bin.img');
CTImageVector= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'uint16','l');%x_dim*y_dim*z_dim
% CTImageVector=CTImageVector(end:-1:1);

CTImageVolume=reshape(CTImageVector,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]);
fclose(fid);
% SliceBrowser(CTImageVolume)

% figure
fid=fopen('ROIBladderPatientAVolumeNKIVCU^PA^S00^I0002.binary.mask');
% fid=fopen('ROISigmoid_00PatientAVolumeNKIVCU^PA^S00^I0002.binary.mask');
roiMaskVector= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'int8','l');%x_dim*y_dim*z_dim
%  roiMaskVector=roiMaskVector(end:-1:1);
roiBinMaskVol=reshape(roiMaskVector,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]);
max(roiMaskVector)
min(roiMaskVector)
fclose(fid);
% SliceBrowser(roiBinMaskVol)



% Bitmap created by software
% figure
fid=fopen('ROIN_BladderImageSetNKIVCU^PA^S00^I0002.binary.mask');
% fid=fopen('ROI_Sigmoid_00ImageSetNKIVCU^PA^S00^I0002.binary.mask');
roiMaskVector2= fread(fid,cTImageInfo.x_dim*cTImageInfo.y_dim*cTImageInfo.z_dim,'int8','l');%x_dim*y_dim*z_dim
%  roiMaskVector=roiMaskVector(end:-1:1);
roiBinMaskVol2=reshape(roiMaskVector2,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]);
max(roiMaskVector2) 
min(roiMaskVector2)
fclose(fid);
% SliceBrowser(roiBinMaskVol2-roiBinMaskVol)
VolDiff= roiBinMaskVol2-roiBinMaskVol;
 maskToKeep=roiMaskVector2 | roiMaskVector;
 intersection=roiMaskVector2 & roiMaskVector;
 
% dice = 2*sum(intersection==1)/(sum(roiMaskVector2==1)+sum(roiMaskVector==1));
dice =@(roiMaskVec1, roiMaskVec2) 2*sum((roiMaskVec2 & roiMaskVec1)==1)/(sum(roiMaskVec1==1)+sum(roiMaskVec2==1));
figure
hist(VolDiff(maskToKeep==1),100)
sum(roiMaskVector2==1)/sum(roiMaskVector==1)
oneVoxelVolume= cTImageInfo.x_pixdim*cTImageInfo.y_pixdim*cTImageInfo.z_pixdim;

disp(['Pinnacle Volume = ', num2str( oneVoxelVolume*sum(roiMaskVector==1))])
disp(['Our calculated Volume = ', num2str( oneVoxelVolume*sum(roiMaskVector2==1))])
disp(['Dice= ', num2str( dice(roiMaskVector2,roiMaskVector))])

%% Test python generated roi
% these two are substantially different make sure that you have the right
% masks
dice =@(roiMaskVec1, roiMaskVec2) 2*sum((roiMaskVec2 & roiMaskVec1)==1)/(sum(roiMaskVec1==1)+sum(roiMaskVec2==1));

cTImageInfo=readImageHeader('CTVolumePatientSImageSetNKIVCU^PS^S00^I0002.bin.header');


%S = dir('ROIOuterPatientSVolumeNKIVCU^PS^S00^I0002.binary.mask');
S = dir('ROIFEMUR_LPatientSVolumeNKIVCU^PS^S02^I0002.binary.mask');
fid=fopen(S.name);
roiMaskVectorTest= fread(fid,S.bytes,'int8','l');%x_dim*y_dim*z_dim
fclose(fid)


% S = dir('PartialVolumeROIOuterPatientSVolumeNKIVCU^PS^S00^I0002.bin');
S = dir('PartialVolumeROIFEMUR_LPatientSVolumeNKIVCU^PS^S02^I0002.bin');
fid=fopen(S.name);
partialVol= fread(fid,S.bytes,'double','l');%x_dim*y_dim*z_dim
fclose(fid)



% S = dir('NonZerosIndicesROIOuterPatientSVolumeNKIVCU^PS^S00^I0002.bin');
S = dir('NonZerosIndicesROIFEMUR_LPatientSVolumeNKIVCU^PS^S02^I0002.bin');
fid=fopen(S.name);
nonZeroIndices= fread(fid,S.bytes,'int64','l');%x_dim*y_dim*z_dim
fclose(fid)

% nonZeroIndices(find(abs(partialVol-0.5)<1e-4))
S = dir('OurROIOuterPatientSVolumeNKIVCU^PS^S00^I0002.binary.mask');
fid=fopen(S.name);
ourRoiMaskVectorTest= fread(fid,S.bytes,'int8','l');%x_dim*y_dim*z_dim
fclose(fid)


%Calculated roi with our software
% S = dir('ROI_OuterImageSetNKIVCU^PS^S00^I0002.binary.mask');
S = dir('ROI_FEMUR_LImageSetNKIVCU^PS^S02^I0002.binary.mask');
fid=fopen(S.name);
roiMaskVector_Calc= fread(fid,S.bytes,'int8','l');%x_dim*y_dim*z_dim
fclose(fid)


% S = dir('ROI_OuterImageSetNKIVCU^PS^S00^I0002.indices.bin');
S = dir('ROI_FEMUR_LImageSetNKIVCU^PS^S02^I0002.indices.bin');
fid=fopen(S.name);
nonZeroIndices_Calc= fread(fid,S.bytes/4,'uint32','l');%x_dim*y_dim*z_dim
fclose(fid)


% S = dir('ROI_OuterImageSetNKIVCU^PS^S00^I0002.numCornerInside.bin');
S = dir('ROI_FEMUR_LImageSetNKIVCU^PS^S02^I0002.numCornerInside.bin');
fid=fopen(S.name);
numCornersInside_Calc= fread(fid,S.bytes/2,'uint16','l');%x_dim*y_dim*z_dim
fclose(fid)


% S = dir('ROI_OuterImageSetNKIVCU^PS^S00^I0002.partialVol.bin');
S = dir('ROI_FEMUR_LImageSetNKIVCU^PS^S02^I0002.partialVol.bin');
fid=fopen(S.name);
partialVol_Calc= fread(fid,S.bytes/4,'single','l');%x_dim*y_dim*z_dim
fclose(fid)
oneVoxelVolume= cTImageInfo.x_pixdim*cTImageInfo.y_pixdim*cTImageInfo.z_pixdim;

disp(['Pinnacle Volume = ', num2str( oneVoxelVolume*sum(roiMaskVectorTest==1))])
disp(['Our calculated Volume = ', num2str( oneVoxelVolume*sum(roiMaskVector_Calc==1))])
disp(['Dice= ', num2str( dice(roiMaskVectorTest,roiMaskVector_Calc))])
setdiff(find(roiMaskVectorTest),find(roiMaskVector_Calc))
% comperession 
temp=[0;find(diff(roiMaskVector_Calc)~=0)];

%%
indicesWithSpecifiedCode = nonZeroIndices_Calc(find(numCornersInside_Calc==7));
partVolVecWithSpecifiedCode = [];
for ind = 1: length(indicesWithSpecifiedCode )
   partVolVecWithSpecifiedCode= [partVolVecWithSpecifiedCode,partialVol(find( nonZeroIndices==indicesWithSpecifiedCode (ind)))];
end
find(abs(partVolVecWithSpecifiedCode-0.5)<1e-4)
find(abs(partVolVecWithSpecifiedCode-1)<1e-4)
hist(partVolVecWithSpecifiedCode)
%%
%%SliceBrowser(reshape(roiMaskVectorTest,[cTImageInfo.x_dim,cTImageInfo.y_dim,cTImageInfo.z_dim]))
% SliceBrowser(reshape(ourRoiMaskVectorTest,[512,512,S.bytes/512/512]))
% 
% plot(ourRoiMaskVectorTest-roiMaskVectorTest)