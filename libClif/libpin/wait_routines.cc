/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/* wait_routines.cc
   Routines to wait for completion of child processes

   Modification History:
      Feb 2, 1999: JVS: Broken off of run_routines and MCVMC
      Dec 20, 1999: wait.h is in sys/wait.h. and sys/types.h
                    needed for pid_t
      Sept 5, 2000: JVS: Add fflush command
      May 26, 2013: JVS: Add myProc to the errors/warnings.
                         returns nFailedProcesses
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>

#include "utilities.h"
#include "wait_routines.h"
/* **************************************************************************** */
int wait_for_child(int pid)
{
   int wait_status;   /* wait for the child process to terminate */
   int rpid;
   char *myProc="wait_for_child";
   do{
      rpid = waitpid(pid, &wait_status,0);
      if(rpid == -1 && errno != ECHILD){
         printf("\n ERROR: %s: waiting for child",myProc);exit(-1);
      }
   }while(rpid != pid && errno != ECHILD );           
   if(! WIFEXITED(wait_status) && errno != ECHILD ) /* Return Error if child has error exit status */
   {
      printf("\n ERROR: %s: Child %d Exited with status %d",myProc, rpid, WEXITSTATUS(wait_status));
      printf("\t WIFEXITED = %d", WIFEXITED(wait_status));
      perror("wait_for_child");
      return(FAIL);
   }
   if (errno == ECHILD){
      printf("\n WARNING: %s: Child Process %d does not exist", myProc, pid);
      return(FAIL);
   }
   if (wait_status != 0){
       printf("\n WARNING: %s: PID %d returned with NON-ZERO value (%d)", myProc, rpid, wait_status);
       return(FAIL);
   }
   return(OK);
}
/* ***************************************************************************************** */
int wait_for_process_completion(int n_pids, pid_t *pid_array)
{
  int wait_status;
  int n_wait = 0;
  int return_status = OK;
  int nFailedProcesses = 0;

  bool outputMessages = false;
  
  char *myProc="wait_for_process_completion";
  do{
    n_wait = 0;
    if(outputMessages) printf("\n %s Waiting for Child processes, n_pids = %d",myProc, n_pids);
    // list the pid's still running 
    for(int i=0; i < n_pids; i++){
      if(pid_array[i] != 0 ){ 
	if(outputMessages) printf(" %ld",(long) pid_array[i]); 
	n_wait++;
      }
    }
    if(outputMessages) { printf(" to finish \n");
    fflush(stdout);fflush(stderr);}
     
    if(n_wait){
      int rpid = wait(&wait_status);
      if(rpid == -1 && errno != ECHILD){
	printf("\n ERROR: %s:waiting for child processes to finish",myProc);
	exit(-1);
      }
      if(rpid != -1){
	int ibm=0;
	while(rpid != pid_array[ibm] && 
              ibm++ < n_pids );
	if(ibm >= n_pids){
              printf("\n ERROR: %s: PID %d Not found", myProc, rpid);
              printf("\n Available PID's:");
              for(int i=0; i<n_pids; i++) 
		printf("\t %d",pid_array[i]);
	}
	else{
	  if(wait_status != 0) {
	    printf("\n ERROR: %s: Child Process %d (array_id %d) returned NONZERO value (%d)", myProc,
		   rpid, ibm, wait_status);
	    return_status = FAIL;
	    nFailedProcesses++;
	  }
	  else {
	    if(outputMessages) 
	      printf("\n %s: Child Process %d returned (array_id %d)", myProc, rpid,ibm);
	    else
	      printf("!");
	  }
	  pid_array[ibm] = 0; /* reset the pid_array  value */
	}
      }
    }
  }while(errno != ECHILD && n_wait );
  if(nFailedProcesses) return(nFailedProcesses);
  return(return_status);
}
/* ************************************************************************************ */
