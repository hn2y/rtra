/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* read_modifiers.cc
   Reads beam modifiers from Pinnacle
   Modification History:
      Nov. 4, 1999: JVS: Created
      April 24, 2000: JVS: Allow EXPOSE EXPOSE apertures (these are invalid apertures) needed
                           for IMRT program so can use aperture outline
      October 11, 2000: JVS: readPinnacleCompensator
      Nov 10, 2005: Make compatible with .so
   Nov 5, 2007: JVS: eliminate myerrno, replace with checkClifError()
   Nov 27, 2007: JVS: Add const char * to get rid of compiler warning
   June 4, 2008: JVS: Updates for sun CC complier
   June 10, 2008: JVS: Update to use readBigEndianPinnacleDoseFile
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h> /* for fabs */
#include "string_util.h"
#include "utilities.h"
#include "typedefs.h"
#include "defined_values.h"
#include "apertures.h"
#include "beams.h"
#include "read_clif.h"
#include "modifiers.h"
#include "lib.h" /* for definition of TRUE */
#include "myPinnacle.h"
#include "myPinnComm.h"

/* ****************************************************************************** */
int read_pinnacle_modifiers(FILE *fp, int *n_apertures, aperture_type **ap)
{
   // reads in the pinnacle beam modifiers, returns FAIL if at End of File
   // returns END_OF_CONTEXT if reads to end of ModifierList
   // first line is the type of modifier 
   // a "modifier" is considered to be an aperture

   char input_string[MAX_STR_LEN];
   aperture_type *aperture = NULL; // local copy of the aperture
   *n_apertures = 0;

   while(clif_get_to_line(fp,"BeamModifier ={") ==OK ) /* do as long as modifiers specified */
   {
         // printf("\n\t Modifier %d: %s",pbeam->n_apertures, input_string);
         // allocate memory for the aperture
         if(*n_apertures == 0)
            aperture = (aperture_type *)calloc(1,sizeof(aperture_type));
         else
         {
#ifdef DEBUG_READ_BEAMS
            printf("\n Reallocating Memory for %d apertures (plus first)",*n_apertures);
#endif
            aperture = (aperture_type *)realloc( 
                                   (aperture_type *) aperture, 
                                   (*n_apertures+1)*sizeof(aperture_type));
         }
         if(aperture == NULL)
         {
            eprintf("\n ERROR: allocating memory for aperture %d",*n_apertures);
            return(FAIL);
         }

#ifdef DEBUG_READ_BEAMS
            printf("\n Have done %d apertures",*n_apertures);
#endif
         if(read_pinnacle_aperture(fp,&aperture[*n_apertures])!=OK)
         {
            eprintf("\n ERROR: Reading aperture %d", *n_apertures+1);
            return(FAIL);
         }
         *n_apertures=*n_apertures+1;
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing %s structure in file ",input_string);
            return(FAIL);
         }
         if(checkClifError() != OK) return(FAIL);
   }
   resetClifError(); /* clear errno set when fails to find another BeamModifier */
   *ap = aperture;
   return(OK);
}
/* ************************************************************************************ */
int read_pinnacle_apertures(int ibeam, int *n_apertures, aperture_type **ap)
{ /* Read aperture information from the calling pinnacle session */
   
   // save the modifier list to a file
   char tmp_file[MAX_STR_LEN];
   tmpnam(tmp_file);
   char command[MAX_STR_LEN];
   //sprintf(command,"TrialList.Current.BeamList.#%d.ModifierList.#%d.Save = %s\n",ibeam, iblock+1, tmp_file);
   sprintf(command,"TrialList.Current.BeamList.#%d.ModifierList.Save = %s\n",ibeam, tmp_file);
   int iStatus = iPinnComm_IssueRootCommand(command); 
   if(!iStatus){fprintf(stderr,"\nERROR: Executing Command %s",command); return(FAIL);} 

   // open this file and read the modifiers out of it....
   FILE *istrm = fopen(tmp_file,"r");
   if(istrm == NULL)
   {
      eprintf("\n ERROR: Opening file %s", tmp_file); return(FAIL);
   }
   if(read_pinnacle_modifiers(istrm, n_apertures, ap) != OK)
   {
      eprintf("\n ERROR: Reading Pinnacle Modifiers");
      return(FAIL);
   }
   /* remove the temp file created above */
   if(remove(tmp_file) == -1)
   {
      eprintf("\n ERROR: Cannot remove temp file %s", tmp_file); return(FAIL);
   }
   return(OK);
}
/* ************************************************************************************* */
int read_pinnacle_aperture(FILE *fp, aperture_type *ap)
{  // read in aperture from pinnacle (from file)
    ap->valid = 0;
    ap->n_contours = 0; // initialize to zero contours in the aperture 
   // figure out if aperture is a "hole" or not
   char OutsideMode[MAX_STR_LEN];
   char InsideMode[MAX_STR_LEN];
   // printf("\n Determining Aperture Mode:");
   if(clif_string_read(fp,"InsideMode =",InsideMode)!=OK)
     { eprintf("\n ERROR: Reading InsideMode"); return(FAIL);}
   if(clif_string_read(fp,"OutsideMode =",OutsideMode)!=OK)
     { eprintf("\n ERROR: Reading OutsideMode"); return(FAIL);}
   // printf("\t Inside %s, Outside %s",InsideMode,OutsideMode);
   if( (strcmp(InsideMode,"Expose") == 0) &&
        (strcmp(OutsideMode,"Block") == 0)){ ap->hole = 1; ap->valid = 1; }
   else if( (strcmp(OutsideMode,"Expose") == 0) &&
             (strcmp(InsideMode,"Block") == 0)){ ap->hole = 0; ap->valid = 1;} 
   else if( (strcmp(OutsideMode,"As Is") == 0) &&
             (strcmp(InsideMode,"As Is") == 0)){ ap->hole = 2; ap->valid = 0;}
   else if( (strcmp(OutsideMode,"Expose") == 0) &&
             (strcmp(InsideMode,"Expose") == 0)){ ap->hole = 2; ap->valid = 0;}
   else
   {
      eprintf("\n ERROR: InsideMode = %s, OutsideMode = %s invalid",InsideMode,OutsideMode);
      return(FAIL);
   }
   // get to level that describes the aperture
#ifdef DEBUG_READ_BEAMS
   printf("\n   Reading Contour List for Aperture (n_contours = %d)",ap->n_contours);
#endif
   if(read_pinnacle_contour_list(fp, &ap->n_contours, &ap->contour)!=OK)
   {
      eprintf("\n ERROR: reading pinnacle contour list");
      return(FAIL);
   }
   /* convert default pinnacle float vertices to int vertices required by the pb program */
   /* 
   ap->units = 1.0000000000e-3;  
   if( convert_fvertices_to_int(ap->num_vertices, ap->units, ap->fvertices, &ap->vertices) != OK)
   {
      eprintf("\n ERROR: converting pinnacle floating point contour to int contour");
      return(FAIL);
   }
   */
   /*   ap->use_for_dose_computations = TRUE;
    printf("\n **** OBJECT CODE BEING ASSIGNED TO 1 *****");
   ap->object_code = 1;
   */
   return(OK);
}
/* ****************************************************************************************** */
#ifdef LINUX
int read_pinComm_aperture(int ibeam, int iblock, aperture_type *ap)
{
  ap->valid=0;
  printf("\n Attempting to read beam %d, block %d",ibeam,iblock);
  printf("\n ERROR: read_pinnComm_aperture cannot be executed from LINUX");
  return(FAIL);
}
#else
int read_pinComm_aperture(int ibeam, int iblock, aperture_type *ap)
{ /* Read aperture information from the calling pinnacle session */
    /* Routine is NOT finished yet....JVS: Nov 3, 1999 */
   char pcMessage[256];
   int iStatus;
   void *pvAperture;

   sprintf(pcMessage, "TrialList.Current.BeamList.#%d.ModifierList.#%d.Address", ibeam, iblock);
   iStatus = iPinnComm_QueryObject(NULL, pcMessage, &pvAperture); 
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: query object: %s\n", pcMessage);
      return(FAIL);
   }
   /* Read Strings from the Aperture */
   char acBuffer[PINN_COMM_STRING_LEN];
   {
    char qString[]="Name";
    iStatus = iPinnComm_QueryString(pvAperture, qString, acBuffer);
    if (!iStatus) {
      fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
    }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   printf("\n Aperture Name is %s", acBuffer);
   char OutsideMode[MAX_STR_LEN];
   char InsideMode[MAX_STR_LEN];

   { char qString[]="InsideMode";
     iStatus = iPinnComm_QueryString(pvAperture, qString, acBuffer);
     if (!iStatus) {
        fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
     }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   strcpy(InsideMode,acBuffer);
   {char qString[]="OutsideMode";
   iStatus = iPinnComm_QueryString(pvAperture, qString, acBuffer);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
   }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   strcpy(OutsideMode,acBuffer);
   // printf("\t Inside %s, Outside %s",InsideMode,OutsideMode);
   if( (strcmp(InsideMode,"Expose") == 0) &&
        (strcmp(OutsideMode,"Block") == 0)){ ap->hole = 1; ap->valid = 1; }
   else if( (strcmp(OutsideMode,"Expose") == 0) &&
             (strcmp(InsideMode,"Block") == 0)){ ap->hole = 0; ap->valid = 1;} 
   else if( (strcmp(OutsideMode,"As Is") == 0) &&
             (strcmp(InsideMode,"As Is") == 0)){ ap->hole = 2; ap->valid = 0;}
   else
   {
      eprintf("\n ERROR: InsideMode = %s, OutsideMode = %s invalid",InsideMode,OutsideMode);
      return(FAIL);
   }
   printf("\n Inside Mode = %s, Outside Mode = %s", InsideMode, OutsideMode);
   return(OK);
}
#endif
/* ***************************************************************************** */
int read_pinnacle_compensator(int ibeam, compensator_type *c)
{
  printf("\n \7\7\7 WARNING: Reading Compensator for CURRENT trial");
  printf("\n Re-code to use readPinnacleCompensator()");

  return(readPinnacleCompensator("Current",ibeam,c));
}
/* **************************************************************************** */
#ifdef LINUX
int readPinnacleCompensator(const char *TrialName, int ibeam, compensator_type *c)
{
  c->IsValid = 0;  
  printf("\n ERROR: readPinnacleCompensator cannot be executed from LINUX  for trial %s, beam %d",TrialName,ibeam);
  return(FAIL);
}
#else
int readPinnacleCompensator(const char *TrialName, int ibeam, compensator_type *c)
{ /* send the compensator data to the calling pinnacle session */
   char pcMessage[256];
   int iStatus;
   int itmp;

   /*
   void *pvBeam;
   sprintf(pcMessage, "TrialList.Current.BeamList.#%d.Address", ibeam);
   iStatus = iPinnComm_QueryObject(NULL, pcMessage, &pvBeam); 
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: query object: %s\n", pcMessage);
      return(FAIL);
   }
   // get the name of the beam
  char acBuffer[PINN_COMM_STRING_LEN];
        iStatus = iPinnComm_QueryString(pvBeam, "Name", acBuffer);
        if (!iStatus) 
        {
           fprintf(stderr, "Error query beam Name.\n");
	   // return (FAIL);
           return(-1);
        }
   printf("\n Beam Name is %s", acBuffer); */
   void *pvCompensator;
   sprintf(pcMessage, "TrialList.%s.BeamList.#%d.Compensator.Address", TrialName,ibeam);
   iStatus = iPinnComm_QueryObject(NULL, pcMessage, &pvCompensator); 
   if (!iStatus) {
      fprintf(stderr, "\n ERROR: query object: %s\n", pcMessage);
      return(FAIL);
   }
   // printf("\n Have Compensator address");
   /* Read Strings from the Compensator */
   char acBuffer[PINN_COMM_STRING_LEN];
   { 
     char qString[] = "Name";
     iStatus = iPinnComm_QueryString(pvCompensator, qString, acBuffer);
     if (!iStatus) {
       fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
     }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   strcpy(c->Name, acBuffer);
   {char qString[] = "Type";
   iStatus = iPinnComm_QueryString(pvCompensator, qString, acBuffer);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
   }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   strcpy(c->Type,acBuffer);
   {char qString[] = "FillOutsideMode";
   iStatus = iPinnComm_QueryString(pvCompensator, qString, acBuffer);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryString \n");return (FAIL);
   }
   }
   if(strlen(acBuffer) > MAX_OBJ_NAME_LEN) acBuffer[MAX_OBJ_NAME_LEN-1]='\0';
   strcpy(c->FillOutsideMode,acBuffer);
   /* Read various Values for the compensator */
   /* get Current Value of "isValid" */
   itmp = -100;
   {char qString[] = "IsValid";
   iStatus = iPinnComm_QueryInt(pvCompensator, qString, &itmp);
   if (!iStatus) {
      fprintf(stderr, "\nERROR: QueryInt\n");
      return (FAIL);
   }
   }
   // NOTE: IsValid always seems to return 0 !!! 
   // printf ("\n Currently, Compensator IsValid = %d", itmp);
   c->IsValid = itmp;
   /* Get values of other parameters */
   float ifloat;
   {char qString[] = "Width";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "\nERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->Width = ifloat;
   {char qString[] = "Height";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "\nERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->Height = ifloat;
   {char qString[] = "Resolution";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "\nERROR: QueryFloat \n");return (FAIL);
   }
   }

   c->Resolution = ifloat;
   c->nx = (int) (c->Width/c->Resolution)  +1;
   c->ny = (int) (c->Height/c->Resolution) +1;
   {char qString[] = "Density";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "\nERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->Density = ifloat;

   {char qString[] = "OutputFactor";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->OutputFactor = ifloat;
   {char qString[] = "Density";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->Density = ifloat;

   {char qString[] = "SourceToCompensatorDistance";
   iStatus = iPinnComm_QueryFloat(pvCompensator, qString, &ifloat);
   if (!iStatus) {
      fprintf(stderr, "ERROR: QueryFloat \n");return (FAIL);
   }
   }
   c->SourceToCompensatorDistance = ifloat;

#ifdef DEBUG_COMPENSATOR
   printf("\n Compensator Parameters:");
   printf("\n\t Height = %f", c->Height);
   printf("\n\t Width  = %f", c->Width);
   printf("\n\t Density = %f", c->Density);
   printf("\n\t Resolution = %f", c->Resolution);
   printf("\n\t Output Factor = %f", c->OutputFactor);
   printf("\n\t Source to Compensator Distance = %f", c->SourceToCompensatorDistance);
   printf("\n\t IsValid = %d", c->IsValid);
#endif

  /* Allocate Memory for the Compensator thickness array*/
   /*   NOT required, memory will be allocated in read_dose_file
   c->thickness = (float *) calloc(c->nx*c->ny, sizeof(float));
  if(c->thickness == NULL)
  {
     eprintf("\n ERROR: Allocating Memory for Compensator");
     return(FAIL);
     } */
/* write Pinnacle Compensator out to a temporary file */
   char tmp_file[MAX_STR_LEN];
   tmpnam(tmp_file);
#ifdef DEBUG
   printf("\n Writing Pinnacle Beam to file %s", tmp_file);
#endif

   char command[MAX_STR_LEN];
   sprintf(command,"TrialList.%s.BeamList.#%d.Save = %s\n",TrialName,ibeam,tmp_file);
   iStatus = iPinnComm_IssueRootCommand(command); 
   if(!iStatus){fprintf(stderr,"\nERROR: Executing Command %s",command); return(FAIL);} 

   char compfile[MAX_STR_LEN];
   sprintf(compfile,"%s.binary.001", tmp_file);
   /* read in the file */
   if(readBigEndianPinnacleDoseFile(compfile, c->nx*c->ny, &c->thickness) != OK)
   {
      eprintf("\n ERROR: readBigEndianPinnacleDoseFile: Reading Pinnacle Compensator File %s", compfile);return(FAIL);
   }
   /* clean up tmp files created */
   if(remove(tmp_file) == -1)
   {
      eprintf("\n ERROR: Cannot remove temp file %s", tmp_file); return(FAIL);
   }
   for(int i=0; i<3; i++)
   {
      sprintf(compfile,"%s.binary.%03d", tmp_file, i);
      if(remove(compfile) == -1)
      {
         eprintf("\n ERROR: Cannot remove temp file %s", compfile); return(FAIL);
      }
   }
   
   return(OK);
}
#endif
/* ****************************************************************************************** */
/* ********************************************************************************* */
int read_pinnacle_compensator(FILE *fp, beam_type *pbeam)
{  
   if(clif_get_to_line(fp,"Compensator ={")!=OK)
   {
      eprintf("\n ERROR: finding Compensator in file"); return(FAIL);
   }
   /*
   if(clif_string_read(fp,"Name =",pbeam->compensator_name)!=OK)
   { 
      eprintf("\n ERROR: Reading Compensator Type"); return(FAIL);
   }
   */
   int isvalid = (int) clif_get_value(fp,"IsValid =");
      if(checkClifError() !=OK) {eprintf("\n ERROR: reading IsValid value"); return(FAIL);}
   // printf("\n Reading Pinnacle Compensator (valid = %d)",isvalid);
   if(isvalid == 0)
   {
      pbeam->intensity_modulator_defined = 0;
   }
   else
   {
      pbeam->intensity_modulator_defined = 1;

      pbeam->comp = (compensator_type *)calloc(1,sizeof(compensator_type));
      if(pbeam->comp == NULL)
      {
         eprintf("\n ERROR: allocating memory for compensator"); 
         return(FAIL);
      }
      pbeam->comp->SourceToCompensatorDistance = clif_get_value(fp,"SourceToCompensatorDistance =");
      pbeam->comp->Width= clif_get_value(fp,"Width =");
      pbeam->comp->Height= clif_get_value(fp,"Height =");
      pbeam->comp->Resolution= clif_get_value(fp,"Resolution =");
      pbeam->comp->CompensatorHangsDown= (int)clif_get_value(fp,"CompensatorHangsDown =");
      pbeam->comp->OutputFactor= clif_get_value(fp,"OutputFactor =");
      pbeam->comp->Density= clif_get_value(fp,"Density =");
      if(checkClifError() !=OK)
      { 
         eprintf("\n ERROR: reading compensator values"); 
         return(FAIL);
      }
      // Need to get Interplation
      pbeam->comp->Interpolation = (int) clif_get_value(fp,"ResampleUsingLinearInterpolation =");
      if(clif_string_read(fp,"Type =",pbeam->comp->Type)!=OK)           
         { eprintf("\n ERROR: Reading compensator type"); return(FAIL);}
      char tmp_str[MAX_OBJ_NAME_LEN];
      if(clif_string_read(fp,"Thickness =",tmp_str)!=OK)          
         { eprintf("\n ERROR: Reading compensator  Fname"); return(FAIL);}
      // read thickness from file
      // create file name
      if(strncmp(tmp_str,"XDR:",4) != 0)   // check for binary file specification, first 4 char 
      {
         eprintf("\n ERROR: %s is invalid specifier for compensator file name",tmp_str);
         return(FAIL);
      }
      int id_num = 0;
      if(sscanf(tmp_str+4,"%d",&id_num) != 1)
      {
         eprintf("\n ERROR: Cannot get compensator file ID number from %s", tmp_str);
         return(FAIL);
      }
      sprintf(pbeam->comp->Fname,"%s.binary.%03d",pbeam->fname,id_num);
#ifdef DEBUG
      printf("\n Reading in compensator file %s", pbeam->comp->Fname);
#endif
      pbeam->comp->nx = (int) (pbeam->comp->Width/pbeam->comp->Resolution)  +1;
      pbeam->comp->ny = (int) (pbeam->comp->Height/pbeam->comp->Resolution) +1;
      pbeam->comp->thickness = read_binary(pbeam->comp->Fname, pbeam->comp->nx*pbeam->comp->ny);
      if(pbeam->comp->thickness == NULL)
      {
         eprintf("\n ERROR: reading compensator thickness");
         return(FAIL);
      }
      // can read compensator from Pinnacle File or from the intensity.nml file
      // for reading from intensity.nml file, use qwu code
      // eprintf("\n ERROR: Compensators are not currently supported");
      //  return(FAIL);
   }
   // close out compensator context
   if(clif_get_to_line(fp,"};")!=OK)
   {
      eprintf("\n ERROR: Closing compensator in file ");return(FAIL);
   }
   return(OK);
}
/* ***************************************************************** */
int read_pinnacle_pbeam_modifiers(FILE *fp, beam_type *pbeam)
{
   // reads in the pinnacle beam modifiers, returns FAIL if at End of File
   // returns END_OF_CONTEXT if reads to end of ModifierList
   // first line is the type of modifier 
   // a "modifier" is considered to be an aperture

   char input_string[MAX_STR_LEN];

   while(clif_get_to_line(fp,"BeamModifier ={") ==OK ) /* do as long as modifiers specified */
   {
         // printf("\n\t Modifier %d: %s",pbeam->n_apertures, input_string);
         // allocate memory for the aperture
         if(pbeam->n_apertures == 0)
            pbeam->aperture = (aperture_type *)calloc(1,sizeof(aperture_type));
         else
         {
#ifdef DEBUG_READ_BEAMS
            printf("\n Reallocating Memory for %d apertures (plus first)",pbeam->n_apertures);
#endif
            pbeam->aperture = (aperture_type *)realloc( 
                                   (aperture_type *) pbeam->aperture, 
                                   (pbeam->n_apertures+1)*sizeof(aperture_type));
         }
         if(pbeam->aperture == NULL)
         {
            eprintf("\n ERROR: allocating memory for aperture %d",pbeam->n_apertures);
            return(FAIL);
         }
#ifdef DEBUG_READ_BEAMS
            printf("\n Have done %d apertures",pbeam->n_apertures);
#endif
         if(read_pinnacle_aperture(fp,&pbeam->aperture[pbeam->n_apertures])!=OK)
         {
            eprintf("\n ERROR: Reading aperture %d", pbeam->n_apertures+1);
            return(FAIL);
         }
         pbeam->n_apertures=pbeam->n_apertures+1;
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing %s structure in file ",input_string);
            return(FAIL);
         }
         if(checkClifError() != OK) return(FAIL);
   }
   resetClifError(); /* clear errno set when fails to find another BeamModifier */

   // then has BlockingMaskPixelSize...TrayFactor,CutoutFactor...
   if(pbeam->n_apertures)
   {
      pbeam->aperture[0].transmission = clif_get_value(fp,"BlockAndTrayFactor =");
         if(checkClifError() !=OK) {eprintf("\n ERROR: reading BlockAndTrayFactor"); return(FAIL);}
      pbeam->aperture[0].hole_transmission = clif_get_value(fp,"TrayFactor =");
         if(checkClifError() !=OK) {eprintf("\n ERROR: reading TrayFactor"); return(FAIL);}
   }
   // the BlockAndTrayFactor and TrayFactor are the same for ALL of the apertures
   for(int iap=1;iap<pbeam->n_apertures; iap++)
   {
      pbeam->aperture[iap].transmission =pbeam->aperture[0].transmission;
      pbeam->aperture[iap].hole_transmission =pbeam->aperture[0].hole_transmission;
   }
   return(OK);
}
/* ************************************************************************************ */
