#include "cfClif.hpp"
#include "cfClifElement.hpp"
#include "cfClifArray.hpp"
#include "cfClifWriter.hpp"
#include "cfTypedefs.hpp"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

const char TAB = '\t';

cfClifWriter::cfClifWriter()
{
    path = "";
    filename = "";
    clif = NULL;
    stackDepth = 0;
}

cfClifWriter::cfClifWriter( cfClif* clifToSave )
{
    path = "";
    filename = "";
    clif = clifToSave;
    stackDepth = 0;
}

cfClifWriter::~cfClifWriter()
{
    // nothing to delete
}

int cfClifWriter::WriteClif()
{
    if( 0 == path.length() || 0 == filename.length() )
    {
        cout << "ERORR: cfClifWriter:WriteClif: path and filename are not both set" << endl;
        return CF_FAIL;
    }
    
    string fullPath = path + "/" + filename;   
    int stackDepth = 0;
    outputClif.open(fullPath.c_str());

    if( !outputClif.is_open() )
    {
        cerr << "ERROR:cfClifWriter::WriteClif(): Could not open file " << fullPath
                << " to write" << endl;
        return CF_FAIL;
    }
    else if( NULL == clif )
    {
        outputClif.close();
        cerr << "ERROR:cfClifWriter::WriteClif(): clif is NULL" << endl;
        return CF_FAIL;
    }
    else
    {
        WriteClifRecursive(clif, &stackDepth);
    }
    
    outputClif.close();
    return CF_OK;
}

void cfClifWriter::WriteClifRecursive( cfClif* currentClif, int* stackDepth )
{    
    for( unsigned int i = 0; i < currentClif->data.size(); i++ )
    {
        outputClif << InsertTabs(*stackDepth) << currentClif->data[i]->GetHandle() << " = " << currentClif->data[i]->GetValue(false) << ";" << endl;
    }

    for( unsigned int i = 0; i < currentClif->arrays.size(); i++ )
    {
        WriteArray( currentClif, stackDepth, i );
    }

    (*stackDepth)++;
    for( unsigned int i = 0; i < currentClif->clifList.size(); i++ )
    {
        outputClif << InsertTabs(*stackDepth) << currentClif->clifList[i]->handle << " ={" << endl;       
        WriteClifRecursive( currentClif->clifList[i], stackDepth );
        outputClif << InsertTabs(*stackDepth) << "};" << endl;
    }
    (*stackDepth)--;
}

void cfClifWriter::WriteArray( cfClif* currentClif, int* stackDepth, int arrayIndex )
{
    outputClif << InsertTabs(*stackDepth) << currentClif->arrays[arrayIndex]->GetHandle() << "[] ={" << endl;
    for( unsigned int i = 0; i < currentClif->arrays[arrayIndex]->arrayElements.size(); i++ )
    {
        outputClif << TAB << InsertTabs(*stackDepth) << currentClif->arrays[arrayIndex]->GetElement(i);
        // add comma only if its not the last element
        if( i < currentClif->arrays[arrayIndex]->arrayElements.size() - 1)
        {
            outputClif << ",";
        }
        outputClif << endl;
    }
    outputClif << InsertTabs(*stackDepth) << "};" << endl;
}

string cfClifWriter::InsertTabs( int level )
{
    string tabs = "";
    for( int i = 0; i < level; i++ )
    {
        tabs += TAB;
    }
    return tabs;
}
