/** \class cfCoordAxis
 * \brief Represent a coordinate axis
 *
 * \author Baoshe Zhang
 */

#ifndef CF_COORDAXIS_HPP
#define CF_COORDAXIS_HPP

#include "cfTypedefs.hpp"

template <class C> class cfCoordAxis
{
public:
	/** \brief constructor
	 *
	 * use setCoord to set coordinate points and use setUnit to the unit of the coordinate aixs
	 */
	cfCoordAxis(): _nElement(0), _unit("a.u."), _isUniform(false) { }

	/** \brief constructor
	 *
	 * \param coord a data array representing the coordinate points
	 * \param nElement the number of the coordinate points on this axis
	 */
	cfCoordAxis(const C* coord, const int nElement): _nElement(0), _unit("a.u.")
	{
            setCoord(coord, nElement);
	}

	/** \brief constructor
	 *
	 * \param coord a data array representing the coordinate points
	 * \param nElement the number of the coordinate points on this axis
	 * \param unit the unit of the axis
	 */
	cfCoordAxis(const C* coord, const int nElement, const std::string unit): _nElement(0)
	{
            setCoord(coord, nElement);
            setUnit(unit);
	}

	/** \brief constructor for uniform cases
	 *
	 * \param origin the origin of the coordinate axis
	 * \param step the interval between two adjacent coordinate points.
	 * \param nElement the number of the coordinate points on this axis
	 */
	cfCoordAxis(const C origin, const C step, const int nElement): _nElement(0), _unit("a.u.")
	{
            setCoord(origin, step, nElement);
	}

	/** \brief constructor for uniform cases
	 *
	 * \param origin the origin of the coordinate axis
	 * \param step the interval between two adjacent coordinate points.
	 * \param nElement the number of the coordinate points on this axis
	 * \param unit the unit of the axis
	 */
	cfCoordAxis(const C origin, const C step, const int nElement, const std::string unit): _nElement(0)
	{
            setCoord(origin, step, nElement);
            setUnit(unit);
	}


	/** \brief copy constructor (deep)
	 *
	 * \param o the coordinate axis is copied to the current coordinate axis
	 */
	cfCoordAxis(const cfCoordAxis<C>& o): _nElement(0)
	{
            *this = o;
	}

	/** \brief destructor (deep)
	 */
	~cfCoordAxis()
	{
            if(isValid())
            {
                delete[] _coord;
                _coord = NULL;
            }

            _nElement = 0;
            _unit = "a.u.";
	}

	/** \brief operator =
	 *
	 * \param o the coordinate axis is assigned to the current axis
	 */
	void operator=(const cfCoordAxis<C>& o)
	{
            setCoord(o.getCoordPtr(), o.getNDim());
            _isUniform = o.isUniform();
            setUnit(o.getUnit());
            setLabel(o.getLabel());
	}

	/** \brief operator +=
	 *
	 * \param displacement the displacement of the coordinate axis
	 */
	void operator+=(const double displacement)
	{
            C *coordPtr = getCoordPtr();
            for(int i=0; i<getNDim(); i++)
            {
                    *(coordPtr+i) = *(coordPtr+i) + displacement;
            }
	}

	/** \brief operator +=
	 *
	 * \param scale the scale factor of the coordinate axis
	 */
	void operator*=(const double scale)
	{
            C *coordPtr = getCoordPtr();
            for(int i=0; i<getNDim(); i++)
            {
                    *(coordPtr+i) = *(coordPtr+i) * scale;
            }
	}

	/** \brief operator ==
	 *
	 * \param o the coordinate axis is compared to the current axis
	 * \return true if equal; otherwise, false.
	 */
	bool operator==(const cfCoordAxis<C>& o) const
	{
            if(o.isValid() && this->isValid())
            {
                if(o.getNDim() == this->getNDim())
                {
                    double interval = (double)(_coord[_nElement-1]-_coord[0])/double(_nElement-1);
                    double precisionControl = (double)(std::numeric_limits<float>::epsilon() * 4.0);
                    for(int i=0; i<o.getNDim(); i++)
                    {
                        C *coordPtr = o.getCoordPtr();
                        double t = (coordPtr[i] - _coord[i])/interval;
                        if(t < 0)
                        {
                            t = -t;
                        }
                        if(t > precisionControl)
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
	}

	/** \brief set the coordinate points of the axis
	 *
	 * \param coord a data array representing the coordinate points
	 * \param nElement the number of the coordinate points on this axis
	 */
	bool setCoord(const C* coord, const int nElement)
	{
            int i;

            if(nElement <= 0)  // check if nElement is valid
            {
                std::cerr << "Error: The number of coordinate points is invalid(cfCoordAxis::setCoord)!!!" << std::endl;
                return false;
            }

            for(i=0; i<nElement-1; i++)
            {
                if(coord[i+1] <= coord[i])  // check if coord is valid
                {
                    std::cerr << "Error: The coordinate points are invalid(cfCoordAxis::setCoord)!!!" << std::endl;
                    return false;
                }
            }

            // if memeory has been allocated to _coord, delete it for recycling purpose
            if(isValid())
            {
                delete[] _coord;
                _coord = NULL;
                _nElement = 0;
            }

            _nElement = nElement;          
            try
            {
                _coord = new C[nElement];
                int i;
                for(i=0; i<nElement; i++)
                {
                    *(_coord+i) = *(coord+i);
                }
            }
            catch(std::bad_alloc&)
            {
                std::cerr << "Error: Memory Allocation for CoordAxis(cfCoordAxis::setCoord)!!!" << std::endl;
                _nElement = 0;
                return false;
            }

            _isUniform = false;
	    return true;
	}

	/** \brief set the coordinate axis
	 *
	 * \param origin the origin of the coordinate axis
	 * \param step the interval between two adjacent coordinate points.
	 * \param nElement the number of the coordinate points on this axis
	 */
	bool setCoord(const C origin, const C step, const int nElement)
	{
            if(nElement <= 0)  // check if nElement is valid
            {
                std::cerr << "Error: The number of coordinate points is invalid(cfCoordAxis::setCoord)!!!" << std::endl;
                return false;
            }

            if(step <= 0) //check if step is valid
            {
                std::cerr << "Error: The coordinate points are invalid(step<0)(cfCoordAxis::setCoord)!!!" << std::endl;
                return false;
            }

            if(isValid()) // if the coordinate system has been set, delete it
            {
                delete[] _coord;
                _coord = NULL;
                _nElement = 0;
            }

            _nElement = nElement;
            try
            {
                _coord = new C[nElement];
                int i;
                for(i=0; i<nElement; i++)
                {
                    *(_coord+i) = origin + i * step;
                }
            }
            catch(std::bad_alloc&)
            {
                std::cerr << "Error: Memory Allocation for CoordAxis(cfCoordAxis::setCoord)!!!" << std::endl;
                _nElement = 0;
                return false;
            }

            _isUniform = true;
	    return true;
	}

	/** \brief get the pointer to the data array of the coordinate axis
	 *
	 * \return the pointer to the data array of the coordinate axis
	 */
	C* getCoordPtr() const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::getCoordPtr)!!!" << std::endl;
                return NULL;
            }
            else
            {
                return _coord;
            }
	}

	/** \brief get the values of the coordinate axis
	 *
	 * \param coord store the values of the coordinate axis
	 */
	void getCoord(C* coord) const
	{
            int i;

            if(!isValid())
            {
                    std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::getCoord)!!!" << std::endl;
                    coord = NULL;
                    return;
            }

            for(i=0; i<_nElement; i++)
            {
                *(coord+i) = *(_coord+i);
            }
	}

	/** \brief set the unit of the coordinate axis
	 *
	 * \param unit a string representing the axis unit, such as, "cm"
	 */
	void setUnit(const std::string unit)
	{
            _unit = unit;
	}

	/** \brief set the label of the coordinate axis
	 *
	 * \param label a string representing the axis unit, such as, "cm"
	 */
	void setLabel(const std::string label)
	{
            _label = label;
	}

	/** \brief get the number of the points on the coordinate axis
	 *
	 * \return the point number
	 */
	int getNDim() const
	{
            return _nElement;
	}

	/** \brief get the value of the first coordinate point
	 *
	 * \return the value of the first coordinate point
	 */
	C getFirstCoord() const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::getFirstCoord)!!!" << std::endl;
                C retValue = C();
                return retValue;
            }

            return _coord[0];
	}

	/** \brief get the value of the last coordinate point
	 *
	 * \return the value of the last coordinate point
	 */
	C getLastCoord() const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::getLastCoord)!!!" << std::endl;
                C retValue = C();
                return retValue;
            }
            return _coord[_nElement-1];
	}

	/** \brief get the value of a coordinate point indexed by index
	 *
	 * \param index the index specifying a coordinate point
	 * \return the value of the last coordinate point.
	 */
	C getCoordAt(const int index) const
	{
            if(!isInRange(index))
            {
                std::cerr <<"Error: Index is out of range (CoordAxis::getCoordAt)!!!" << std::endl;
                C retValue = C();
                return retValue;
            }
            return _coord[index];
	}

	/** \brief set the value of a coordinate point indexed by index
	 *
	 * \param index the index specifying a coordinate point
	 * \return the value of the last coordinate point.
	 */
	void setCoordAt(const int index, C coord) const
	{
            if(!isInRange(index))
            {
                    std::cerr <<"Error: Index is out of range (CoordAxis::setCoordAt)!!!" << std::endl;
                    return;
            }
            _coord[index] = coord;
	}

	/** \brief get the nearest index (on the left) of a coordinate axis in current
	 * coordinate axis
	 *
	 * \param coord the coordinate axis
	 * \return the index coordinate axis.
	 */

	cfCoordAxis<int> getIndex(const cfCoordAxis<C> coord) const
	{
		int coordSize = coord.getNDim();
		int* indexList = new int[coordSize];
		for(int i=0; i<coordSize; i++)
		{
			indexList[i] = getIndex(coord.getCoordAt(i));
		}
		return cfCoordAxis<int>(indexList, coordSize);
	}

	/** \brief get the nearest index (on the left) of a point
	 *
	 * \param point a point
	 * \return the index of the point on the coordinate axis. if the point is not on the axis, return -1.
	 */
	int getIndex(const C point) const
	{
            if(!isInAxis(point)) // if the pointer is on the axis, return -1
            {
                return -1;              
            }

            // binary search
            int first, last, index;
            first = 0;
            last = _nElement - 1;
            while(last - first > 1)
            {
                index = (first + last)/2;
                if(point > _coord[index])
                {
                    first = index;  // repeat search in top half
                }
                else
                {
                    last = index;   // repeat search in bottom half
                }
            }
            return first;
	}

	/** \brief shift the coordinate axis
	 *
	 * \param value the shift value
	 */
	bool shift(const C value) const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::shift)!!!" << std::endl;
                return false;
            }

            for(int i=0; i<_nElement; i++)
            {
                *(_coord+i) = (*(_coord+i) + value);
            }

            return true;
	}

	/** \brief get the unit of the coordinate axis
	 *
	 * \return the unit of the coordinate axis.
	 */
	std::string getUnit() const
	{
            return _unit;
	}

	/** \brief get the label of the coordinate axis
	 *
	 * \return the label of the coordinate axis.
	 */
	std::string getLabel() const
	{
            return _label;
	}

	/** \brief check if a point is inside the coordinate axis
	 *
	 * \param point a point
	 * \return true, if a point is on the axis; otherwise, false
	 */
	bool isInAxis(const C point) const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::isInAxis)!!!" << std::endl;
                return false;
            }
                      
            if( (point-getFirstCoord())*(point-getLastCoord()) > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

            //FIXME (FS) - should the if statement look like this?
            /*if( point >= getFirstCoord() && point <= getLastCoord() )
            {
                return true;
            }
            else
            {
                return false;
            }**/
	}

	/** \brief check if an index is inside the coordinate axis
	 *
	 * \param index the index
	 * \return true, if an index is inside the coordinate axis; otherwise, false
	 */
	bool isInRange(const int index) const
	{
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::isInRange)!!!" << std::endl;
                return false;
            }
            if(index<0 || index>_nElement-1)
            {
                return false;
            }
            else
            {
                return true;
            }
	}

	/** \brief check if the coordinate system is set properly
	 *
	 * \return true, if the coordinate system is set properly; otherwise, false
	 */
	bool isValid() const
	{
            return (_nElement > 0);
	}

	/** \brief check if the coordinate system is evenly sampled
	 *
	 * \return true, if uniform; otherwise, false
	 */
	bool isUniform() const
	{
            int i;
            if(!isValid())
            {
                std::cerr << "Error: The coordinate axis is not valid(cfCoordAxis::isUniform)!!!" << std::endl;
                return false;
            }

            if(_nElement < 3)
            {
                return true;
            }

            double interval = (double)(_coord[_nElement-1]-_coord[0])/double(_nElement-1);

            double precisionControl = (double)(std::numeric_limits<float>::epsilon() * 4.0);
            for(i=0; i<_nElement-1; i++)
            {
                double t = (_coord[i+1] - _coord[i])/interval - 1;
                if(t < 0)
                {
                    t = -t;
                }
                if(t > precisionControl)
                {
                    return false;
                }
            }
            return true;
	}

private:
	C* _coord;         // 1-d data array containing coordinate points
	int _nElement;     // _nElement of the coordinate points
	std::string _unit; // the unit of this coordinate axis.
	std::string _label; // the axis label
	bool _isUniform;   // if the axis is evenly sampled.

}; // cfCoordAxis

/** \brief operator << for cfCoordAxis
 *
 * \param output the output stream
 * \param c the coordinate axis
 * \return the output stream
 *
 * \author Baoshe Zhang
 */
template<class C>
std::ostream& operator<<(std::ostream& output, const cfCoordAxis<C>& c)
{
    if(!c.isValid())
    {
        std::cerr << "Warning: The coordinate axis is not valid!!!" << std::endl;
        return output;
    }
    output << "Axis(unit:" << c.getUnit() << ")" << std::endl;
    int i, num = c.getNDim();
    for(i=0; i<num; i++)
    {
        output << "(" << i << ")" << c.getCoordAt(i) << " ";
    }
    return output;
}

#endif

/** \class cfCoord3d
 * \brief Represent a 3-D a rectilinear coordinate system.
 *
 * \author Baoshe Zhang
 */
#ifndef CF_COORD3D_HPP
#define CF_COORD3D_HPP

template<class C>
class cfCoord3d
{
public:
	/** \brief constructor
	 */
	cfCoord3d() { }

	/** \brief constructor
	 *
	 * \param x axis-x
	 * \param y axis-y
	 * \param z axis-z
	 */
	cfCoord3d(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
	{
            setCoordAxis(x, y, z);
	}

	/** \brief copy constructor (deep)
	 *
	 * \param o cfCoord3d coordinate system
	 */
	cfCoord3d(const cfCoord3d<C>& o)
	{
            *this = o;
	}

	/** \brief operator =
	 *
	 * \param o a cfCoord3d coordinate system
	 */
	void operator=(const cfCoord3d<C>& o)
	{
            _x = o.getCoordAxisX();
            _y = o.getCoordAxisY();
            _z = o.getCoordAxisZ();
            if(_x.getLabel().empty())
            {
                _x.setLabel("x");
            }
            if(_y.getLabel().empty())
            {
                _y.setLabel("y");
            }
            if(_z.getLabel().empty())
            {
                _z.setLabel("z");
            }
	}

	/** \brief operator ==
	 *
	 * \param o a cfCoord3d coordinate system to be compared
	 * \return true if equal; otherwise, false
	 */
	bool operator==(const cfCoord3d<C>& o) const
	{
            return ((_x == o.getCoordAxisX()) &&
                            (_y == o.getCoordAxisY()) &&
                            (_z == o.getCoordAxisZ()));
	}

	/** \brief set the coordinate system
	 *
	 * \param x axis-x
	 * \param y axis-y
	 * \param z axis-z
	 */
	void setCoordAxis(const cfCoordAxis<C>& x, const cfCoordAxis<C>& y, const cfCoordAxis<C>& z)
	{
            if(!(x.isValid() && y.isValid() && z.isValid()))
            {
                    std::cerr << "Error: The input coordinate axes are not valid(cfCoord3d::setCoordAxis)!" << std::endl;
                    return;
            }
            _x = x;
            if(_x.getLabel().empty())
            {
                _x.setLabel("x");
            }
            _y = y;
            if(_y.getLabel().empty())
            {
                _y.setLabel("y");
            }
            _z = z;
            if(_z.getLabel().empty())
            {
                _z.setLabel("z");
            }
	}

	/** \brief get x- coordinate axis
	 *
	 * \return axis-x
	 */
	const cfCoordAxis<C>& getCoordAxisX() const
	{
            return _x;
	}

	/** \brief get y- coordinate axis
	 *
	 * \return axis-y
	 */
	const cfCoordAxis<C>& getCoordAxisY() const
	{
            return _y;
	}

	/** \brief get z- coordinate axis
	 *
	 * \return axis-z
	 */
	const cfCoordAxis<C>& getCoordAxisZ() const
	{
            return _z;
	}


	/** \brief check if the coordinate system is valid
	 *
	 * \return true, valid; false, not valid
	 */
	bool isValid() const
	{
            return (_x.isValid() && _y.isValid() && _z.isValid());
	}

	/** \brief check if a point is inside the coordinate system
	 *
	 * \param point a 3-D point
	 * \return true, if inside the coordinate system; false, if outside
	 */
	bool isInBox(const cfPoint3D<C>& point) const
	{
            return (_x.isInAxis(point.x) && _y.isInAxis(point.y) && _z.isInAxis(point.z));
	}

	/** \brief check if an index is inside the coordinate system
	 *
	 * \param index a 3-D index
	 * \return true, if inside the coordinate system; false, if outside
	 */
	bool isInRange(const cfPoint3D<int>& index) const
	{
            return (_x.isInRange(index.x) && _y.isInRange(index.y) && _z.isInRange(index.z));
	}

	/** \brief check if this 3-d coordinate system is uniform (evenly sampled)
	 *
	 * \return true, if uniform; otherwise, false
	 */
	bool isUniform() const
	{
            return (_x.isUniform() && _y.isUniform() && _z.isUniform());
	}


	/** \brief get the dimension of the coordinate system
	 *
	 * \return the dimension of the current coordinate system
	 */
	cfPoint3D<int> getNDim() const
	{
            cfPoint3D<int> dim;
            dim.x = _x.getNDim();
            dim.y = _y.getNDim();
            dim.z = _z.getNDim();
            return dim;
	}

	/** \brief get the total number of grid points on the the coordinate system
	 *
	 * \return the number of the grid points on the current coordinate system
	 */
	int getNumPoint() const
	{
            cfPoint3D<int> dim;
            dim.x = _x.getNDim();
            dim.y = _y.getNDim();
            dim.z = _z.getNDim();
            return dim.x * dim.y * dim.z;
	}


	/** \brief get the coordinate point according to the index
	 *
	 * \param index the index
	 * \param coord the coordinate point corresponding to index
	 * \return true, if the index is in range; otherwise, false
	 */
	bool getCoordAt(const cfPoint3D<int>& index, cfPoint3D<C>& coord) const
	{
            if(isInRange(index))
            {
                coord.x = _x.getCoordAt(index.x);
                coord.y = _y.getCoordAt(index.y);
                coord.z = _z.getCoordAt(index.z);
                return true;
            }
            return false;
	}

	/** \brief set the coordinate point according to the index
	 *
	 * \param index the index
	 * \param coord the coordinate point corresponding to index
	 */
	void setCoordAt(const cfPoint3D<int>& index, cfPoint3D<C>& coord) const
	{
            if(isInRange(index))
            {
                _x.setCoordAt(index.x, coord.x);
                _y.setCoordAt(index.y, coord.y);
                _z.setCoordAt(index.z, coord.z);
            }
	}

	/** \brief shift the coordinate axis
	 *
	 * \param value the shift value
	 */
	void shift(const cfPoint3D<C> value) const
	{
            _x.shift(value.x);
            _y.shift(value.y);
            _z.shift(value.z);
	}


	/** \brief get the index of a 3-D box
	 *
	 * \param coord the coordinate
	 * \param index the index (actually, the index of the nearest left-bottom grid point)
	 * \return true if the point is in the coordinate system; otherwise, false
	 */
	cfCoord3d<int> getCoordIndex(const cfCoord3d<C>& coord3d) const
	{
            cfCoordAxis<int> x_index = getIndex(coord3d.getCoordAxisX());
            cfCoordAxis<int> y_index = getIndex(coord3d.getCoordAxisY());
            cfCoordAxis<int> z_index = getIndex(coord3d.getCoordAxisZ());
            return cfCoord3d<int>(x_index, y_index, z_index);
	}

	/** \brief get the index of a point
	 *
	 * \param coord the coordinate
	 * \param index the index (actually, the index of the nearest left-bottom grid point)
	 * \return true if the point is in the coordinate system; otherwise, false
	 */
	bool getCoordIndex(const cfPoint3D<C>& coord, cfPoint3D<int>& index) const
	{
            if(!isInBox(coord))
            {
//			std::cerr << "Warning: The point is out of range(cfCoord3d::getCoordIndex)!!!" << std::endl;
                return false;
            }
            index.x = _x.getIndex(coord.x);
            index.y = _y.getIndex(coord.y);
            index.z = _z.getIndex(coord.z);
            return true;
	}

private:
	cfCoordAxis<C> _x;  // axis-x
	cfCoordAxis<C> _y;  // axis-y
	cfCoordAxis<C> _z;  // axis-z
}; //cfCoord3d

/** \brief operator << for cfCoord3d
 *
 * \param output the output stream
 * \param coord the 3-D coordinate system
 * \return the output stream
 *
 * \author Baoshe Zhang
 */
template<class C>
std::ostream& operator<<(std::ostream& output, const cfCoord3d<C>& coord)
{
    output << "X: " << coord.getCoordAxisX() << std::endl;
    output << "Y: " << coord.getCoordAxisY() << std::endl;
    output << "Z: " << coord.getCoordAxisZ() << std::endl;
    return output;
}

#endif
