#include <fstream> // ifstream
#include "PinnImageSetHeader.h"

inline void removeBackslashR(std::string& mystring)
{
    if (!mystring.empty() && mystring[mystring.size() - 1] == '\r')
        mystring.erase(mystring.size() - 1);
    return;
}

int readPinnacleImageSetHeader(std::string imageHeaderFilePath, ImageHeader_type &imageHeader)
{
    // // TODO: since the ImageSetHeader file in not actually a Clif file we use simple reader to parse it

    //    FILE *fp = fopen(imageHeaderFilePath.c_str(),"r");
    //    if(fp==NULL)
    //    {
    //        std::cout << " ERROR: opening imageHeader  file " <<  imageHeaderFilePath <<std::endl;
    //        return(FAIL);
    //    }

    //    clifObjectType imageHeaderClif = clifObjectType{};
    //    // read the entire clif object in....
    //    if ( OK != readClifFile(fp,&imageHeaderClif,"ImageHeaderClif")) {
    //        std::cout << "ERROR: PinnImageSetHeader imageHeaderClif" << std::endl; return FAIL;
    //    }
    //    fclose(fp);


    //     if (            !readStringClifStructure(&imageHeaderClif,"db_name :",imageHeader.db_name) )
    //     {
    //         std::cout << "ERROR: PinnImageSetHeader: reading image header " << std::endl;
    //     }

    //     dumpClifStructure(&imageHeaderClif);
    //    // Parse out the object of interest....
    //    // Notice we are not reading all the variables in the image header
    //    if( OK != readIntClifStructure(&imageHeaderClif,"x_dim",&imageHeader.x_dim) ||
    //            OK != readIntClifStructure(&imageHeaderClif,"y_dim",&imageHeader.y_dim) ||
    //            OK != readIntClifStructure(&imageHeaderClif,"z_dim",&imageHeader.z_dim) ||
    //            OK != readIntClifStructure(&imageHeaderClif,"bytes_pix",&imageHeader.bytes_pix) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"x_pixdim",&imageHeader.x_pixdim) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"y_pixdim",&imageHeader.y_pixdim) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"z_pixdim",&imageHeader.z_pixdim) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"x_start",&imageHeader.x_start) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"y_start",&imageHeader.y_start) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"z_start",&imageHeader.z_start) ||
    //            OK != readFloatClifStructure(&imageHeaderClif,"y_start_dicom",&imageHeader.y_start_dicom)
    //            )
    //    {
    //        std::cout << "ERROR: PinnImageSetHeader: reading image header " << std::endl;
    //        return FAIL;

    //    }
    //// // clif reader fails in the following tags since there is no ; at the end
    ////       OK != readFloatClifStructure(&imageHeaderClif,"x_start_dicom",&imageHeader.x_start_dicom) ||
    ////    !readStringClifStructure(&imageHeaderClif,"db_name",imageHeader.db_name) ||
    ////    !readStringClifStructure(&imageHeaderClif,"patient_id",imageHeader.patient_id) ||
    ////    !readStringClifStructure(&imageHeaderClif,"patient_position",imageHeader.patient_position) ||
    ////    !readStringClifStructure(&imageHeaderClif,"Version",imageHeader.Version) ||
    ////    !readStringClifStructure(&imageHeaderClif,"date",imageHeader.date) ||

    std::ifstream file(imageHeaderFilePath.c_str());
    std::string str;

    if (file.is_open())
    {
        while (!file.eof())
        {
            std::getline(file, str);

            std::size_t indexStart  = str.find("db_name :");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+10;
                imageHeader.db_name= str.substr (start,str.length()-start);
                removeBackslashR(imageHeader.db_name);
                continue;
            }

            indexStart  = str.find("patient_id :");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+13;
                imageHeader.patient_id= str.substr (start,str.length()-start);
                removeBackslashR(imageHeader.patient_id);
                continue;
            }

            indexStart  = str.find("patient_position :");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+19;
                imageHeader.patient_position= str.substr (start,str.length()-start);
                continue;
            }

            indexStart  = str.find("Version :");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+10;
                imageHeader.Version= str.substr (start,str.length()-start);
                removeBackslashR(imageHeader.Version);
                continue;
            }



            indexStart  = str.find("date :");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+7;
                imageHeader.date= str.substr (start,str.length()-start);
                removeBackslashR(imageHeader.date);
                continue;
            }


            // Process str
            indexStart  = str.find("bytes_pix =");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+11;

                std::string bytes_pixString= str.substr (start,str.length()-start-1);
                imageHeader.bytes_pix =std::stoi(bytes_pixString);
                continue;
            }


            indexStart  = str.find("x_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string x_dimNumString=  str.substr (start,str.length()-start-1);
                imageHeader.x_dim =std::stoi(x_dimNumString);
                // doseImageColumns=x_dim;

                continue;
            }

            indexStart  = str.find("y_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string y_dimNumString=  str.substr (start,str.length()-start-1);
                imageHeader.y_dim =std::stoi(y_dimNumString);
                //doseImageRows=y_dim;
                continue;
            }

            indexStart  = str.find("z_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string z_dimNumString=  str.substr (start,str.length()-start-1);
                imageHeader.z_dim =std::stoi(z_dimNumString);
                //  doseImageNumOfFrames=z_dim;
                continue;
            }

            //        x_pixdim = 0.300000;
            //            y_pixdim = 0.300000;
            //            z_pixdim = 0.300000;

            indexStart  = str.find("x_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string x_pixdimString=  str.substr (start,str.length()-start-1);
                imageHeader.x_pixdim =std::stof(x_pixdimString);
                //DoseImagePixelSpacing[0]=x_pixdim;
                continue;

            }

            indexStart  = str.find("y_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string y_pixdimString=  str.substr (start,str.length()-start-1);
                imageHeader.y_pixdim =std::stof(y_pixdimString);
                // DoseImagePixelSpacing[1]=y_pixdim;
                continue;

            }

            indexStart  = str.find("z_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string z_pixdimString=  str.substr (start,str.length()-start-1);
                imageHeader.z_pixdim =std::stof(z_pixdimString);
                // DoseImageSpacingBetweenSlices=z_pixdim;
                continue;

            }

            indexStart  = str.find("x_start =");
            std::size_t index2Start = str.find("fname_index_start ="); // to make sure

            if ((indexStart!=std::string::npos) & !(index2Start!=std::string::npos))
            {
                std::size_t start = indexStart+10;
                std::string x_startString=  str.substr (start,str.length()-start-1);
                imageHeader.x_start =std::stof(x_startString);
                //   doseImagePositionPatient[0]=x_start;
                continue;

            }

            indexStart  = str.find("y_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string y_startString=  str.substr (start,str.length()-start-1);
                imageHeader.y_start =std::stof(y_startString);
                // doseImagePositionPatient[1]=y_start;
                continue;

            }

            indexStart  = str.find("z_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string z_startString=  str.substr (start,str.length()-start-1);
                imageHeader.z_start =std::stof(z_startString);
                //   doseImagePositionPatient[2]=z_start;
                continue;

            }



            indexStart  = str.find("X_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string X_offsetString=  str.substr (start,str.length()-start-1);
                imageHeader.X_offset =std::stof(X_offsetString);



                continue;

            }

            indexStart  = str.find("Y_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string Y_offsetString=  str.substr (start,str.length()-start-1);
                imageHeader.Y_offset =std::stof(Y_offsetString);
                continue;

            }
            indexStart  = str.find("x_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string x_start_dicomString=  str.substr (start,str.length()-start-1);
                imageHeader.x_start_dicom =std::stof(x_start_dicomString);
                continue;

            }

            indexStart  = str.find("y_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string y_start_dicomString=  str.substr (start,str.length()-start-1);
                imageHeader.y_start_dicom =std::stof(y_start_dicomString);

                continue;

            }



            //headerFilePath.push_back(str);

        }
    } // is_open
    else
    {
        std::cout << "Error in opening header file:"  << imageHeaderFilePath << std::endl;
        return FAIL;
    }
    // sometime header infomation regarding y_start_dicom is corr.imageHeader.y_start_dicom
//    std::cout << " -2*imageHeader.y_pixdim*(static_cast<float>(imageHeader.y_dim)-1.0)= " <<  std::to_string(-2*imageHeader.y_dim*(static_cast<float>(imageHeader.y_dim)-1.0));
  //  imageHeader.y_start_dicom = -2*imageHeader.y_pixdim*(static_cast<float>(imageHeader.y_dim)-1.0)-imageHeader.y_start;
   // imageHeader.x_start_dicom = imageHeader.x_start;
    return OKFlag;
}
