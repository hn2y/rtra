#include "cfClifArray.hpp"
#include <vector>
#include <iostream>
#include <string>
#include <cstdlib>
#include <sstream>

using namespace std;

cfClifArray::cfClifArray()
{
    
}

cfClifArray::~cfClifArray()
{
    
}

void cfClifArray::AddElement( string element )
{
    arrayElements.push_back( element );
}

void cfClifArray::AddElement( float element )
{
    stringstream ss;
    ss << element;
    AddElement(ss.str());
}

void cfClifArray::SetHandle( std::string arrayHandle )
{
    handle = arrayHandle;
}

bool cfClifArray::GetPoints( int** points )
{
    int* tempPoints = new int[arrayElements.size()];
   
    for( unsigned int i = 0; i < arrayElements.size(); i++ )
    {
        tempPoints[i] = atoi((char*)(arrayElements[i]).c_str());
    }

    *points = tempPoints;

    return true;
}

int cfClifArray::GetPoints( float* points )
{
    for( unsigned int i = 0; i < arrayElements.size(); i++ )
    {
        points[i] = atof((char*)(arrayElements[i]).c_str());
    }

    return arrayElements.size();
}
