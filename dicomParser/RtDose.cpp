#include "RtDose.h"

RtDose::RtDose()
{
    rtDicomDoseFile=0;

    associateCTDicomFile=0;
    doseArray = std::vector<double>(0);
}


bool RtDose::getDoseArray()
{

    if (!rtDicomDoseFile)
    {
        std::cout << "Dose file has not been set yet." << std::endl;
        return false;
    }


    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile(rtDicomDoseFile);
    // DRTDoseIOD rtdose;
    //    std::string tempString(rtDoseFile);
    //    if (!fileExists(tempString))
    //    {
    //        std::cout << tempString + "file doesnot exist" << std::endl;
    //    }


    if (status.good())
    {

        DcmObject *dset = &fileformat;
        fileformat.loadAllDataIntoMemory();
        dset = fileformat.getDataset();
        DcmDataset *d = fileformat.getDataset();



        OFString summationtype;
        d->findAndGetOFString(DCM_DoseSummationType, summationtype); // TODO : Check if the summation type is not PLAN

        OFString bA;
        d->findAndGetOFString(DCM_BitsAllocated, bA); //  DCM_BitsAllocated-->  Bits Allocated(0028, 0100) : Number of bits allocated for each pixel sample
        int numBits = 0;
        if (bA.compare("32") == 0)
        {
            numBits = 32;
            std::cout << "Number of bits allocated for each pixel data for this dose data is 32 bit " << std::endl;
            // PixelData is  a array of 16 bit numbers in which the 4 bytes
            //of 2 consecutive elements represent a single 32 bit point.
        }
        else if (bA.compare("16") == 0)
        {
            numBits = 16;
            std::cout << "Number of bits allocated for each pixel data for this dose data is 16 bit " << std::endl;
        }
        else
        {
            std::cout << "Error: Bits Allocated  for each dose pixel has to be set to either 16 or 32." << std::endl;
            return false;
        }

        OFString gridscaling;
        d->findAndGetOFString(DCM_DoseGridScaling, gridscaling); // DCM_DoseGridScaling // Dose Grid Scaling (3004,000E) -->  Scaling factor that when multiplied by the dose grid data found in Pixel Data (7FE0,0010) attribute of the Image Pixel Module, yields grid doses in the dose units as specified by Dose Units (3004,0002).		Required if Pixel Data(7FE0, 0010) is present.
        Float32 gridscale = OFStandard::atof(gridscaling.c_str());

        //// get some properties of the RT Dose Object
        Uint16 rows, columns, frames;
        //Uint16 &rows_ref = rows;
        //Uint16 &columns_ref = columns;
        d->findAndGetUint16(DCM_Rows, rows);
        d->findAndGetUint16(DCM_Columns, columns);
        OFString nrframes;
        d->findAndGetOFString(DCM_NumberOfFrames, nrframes);
        frames = atoi(nrframes.c_str());






        const Uint16* pixelData16 = NULL;
        unsigned long count = 0;
        //d->findAndGetUint16Array(DCM_PixelData, pixelData16); 	// Pixel Data (7FE0,0010)
        // from CERR ()  Conversion of this data into meaningful values is the responsibility of the calling function.



        OFString doseUnitOFString;
        d->findAndGetOFString(DCM_DoseUnits, doseUnitOFString);		 // Dose Units (3004,0002) -->  Units used to describe dose. --> Enumerated Values:	GY = Gray,	RELATIVE = dose relative to reference value specified in DVH Normalization Dose	Value(3004, 0042)

        double doseUnitNormalizationFactor = 1;
        if (doseUnitOFString.compare("GY") == 0)
        {
            std::cout << " Dose Unit is GY" << std::endl;
        }
        else if (doseUnitOFString.compare("RELATIVE") == 0)
        {
            std::cout << " Dose Unit is RELATIVE " << std::endl;
            //
            //OFString doseNFOFString;
            //d->findAndGetOFString(DCM_DVHNormalizationDoseValue, doseNFOFString); // TODO : This part has to be tested .
            OFString doseNFOFString;
            status = d->findAndGetOFString(DCM_DVHNormalizationDoseValue, doseNFOFString); // TODO : This part has to be tested .
            if (status.bad())
            {
                std::cout << " there is no DCM_DVHNormalizationDoseValue element, so dose unit normalization factor will be 1" << std::endl;
            }

        }
        doseArray.resize(frames*rows*columns, 0);
        int indexDoseRead = 0;
        int indexDoseWrite = 0;
        if (d->findAndGetUint16Array(DCM_PixelData, pixelData16, &count).good())
        {
            for (int k = 0; k < frames; k++){
                for (int j = 0; j < columns; j++){
                    for (int i = 0; i < rows; i++){
                        // dose(k, i, j) = pixelData16[k*rows*columns + i*columns + j];
                        // see Dicom part 3, page 507, (3004,000E)
                        //dose_gray(k, i, j) = static_cast<Float32>(pixelData16[k*rows*columns + i*columns + j]) * gridscale;
                        //if ((i == 0) && (j == 0) && (k == 0))
                        //{
                        //	indexDose = 0;
                        //}
                        //else
                        //{
                        //	indexDose++;
                        //}
                        indexDoseRead = k*rows*columns + i*columns + j; // row major , dose data is saved in raw major order
                        indexDoseWrite = k*rows*columns + j*rows + i; // col major, we save the dose in col major order

                        if (numBits == 16)
                        {

                            auto doseVal = static_cast<Float32>(pixelData16[indexDoseRead]) * gridscale;
                            doseArray[indexDoseWrite] = doseVal;
                        }
                        else if (numBits == 32)
                        {
                            // http://stackoverflow.com/questions/18104489/convert-two-uint16-values-to-one-uint32-value-consider-least-and-most-significan
                            // first two bytes are most significant one
                            int locationOfFirstTwoBytes = 2 * (indexDoseRead);
                            int locationOf2ndTwoBytes = locationOfFirstTwoBytes + 1;
                            Uint16 leastSignificantWord = pixelData16[locationOf2ndTwoBytes];
                            Uint16 mostSignificantWord = pixelData16[locationOfFirstTwoBytes];
                            Uint32 result = (leastSignificantWord << 16) + mostSignificantWord;
                            auto doseVal = static_cast<Float32>(result)* gridscale;
                            doseArray[indexDoseWrite] = doseVal;

                        }

                    }
                }
            }
        }

        // 		// (0020,0037)	>Image Orientation (Patient) // TODO : handle orientation in the patient

        // TODO: We might need to handle the following situations for a RTDOSE FILE
        // populate_planC_dose_field
        /*
                %Permute dimensions x and y.
                if ~mread
                dose3 = permute(dose3, [2 1 3]);
                end
                dataS = dose3;

                imgOri = dcm2ml_Element(dcmobj.get(hex2dec('00200037')));
                if (imgOri(1) == -1)
                dataS = flipdim(dataS, 2);
                end

                if (imgOri(5) == -1)
                dataS = flipdim(dataS, 1);
                end

                if isequal(pPos, 'HFP') || isequal(pPos, 'FFP')
                % dataS = flipdim(dataS, 2);
                dataS = flipdim(dataS, 1); %APA change
                end*/



        //status = rtdose.read(*fileformat.getDataset());
        //if (status.good())
        //{
        //	auto pData= rtdose.getPixelData();



        //
        //	//OFString patientsID;
        //	//if (fileformat.getDataset()->findAndGetOFString(DCM_PatientID, patientsID).good())
        //	//{
        //	//	std::cout << "Patient's Name: " << patientsID << std::endl;
        //	//}
        //	//else
        //	//	cerr << "Error: cannot access Patient's Name!" << std::endl;

        //	// rtdose.getPixelData()
        //	// DcmPixelData pData;
        //
        //
        //
        //	// DCM_DoseUnits // Dose Units (3004,0002) -->  Units used to describe dose. --> Enumerated Values: GY,	Gray,		RELATIVE
        //	// DCM_DoseReferencePointCoordinates
        //	// DCM_DoseSummationType 		// Dose Summation Type (3004,000A) --> has to be PLAN
        //

        //	//


        //}
        //else
        //{
        //	cerr << "Error: reading Dataset of DRTDoseIOD  (rtdose)" << status.text() << ")" << std::endl;
        //}
    }
    else{
        std::cout << "Error: cannot read Dose DICOM file (" << status.text() << ")" << std::endl;
        return false;

    }




    auto maxDoseIt = std::max_element(doseArray.begin(), doseArray.end());
    maxDose = *maxDoseIt+.001;
    return true;
}

bool RtDose::getdoseArrayUsingDCMRT() // TODO : need to figure out a way to make it faster, this is recommened way of parsing a RTDOSE DICOM files.
{

    if (!rtDicomDoseFile)
    {
        std::cout << "Dose file has not been set yet." << std::endl;
        return false;
    }

    doseArray = std::vector<double>(0);

    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile(rtDicomDoseFile);
    DRTDoseIOD rtdose;


    if (status.good())
    {
        status = rtdose.read(*fileformat.getDataset());

        Uint16 imageFrameOrigin;
        status = rtdose.getImageFrameOrigin(imageFrameOrigin);

        if (status.good())
        {
            std::cout << "status is good" << std::endl;
        }
        /*
        DcmObject *dset = &fileformat;
        fileformat.loadAllDataIntoMemory();
        dset = fileformat.getDataset();
        DcmDataset *d = fileformat.getDataset();
        DRTDoseIOD *dcmrt_dose = new DRTDoseIOD();
        DcmItem &ditem = dynamic_cast<DcmItem&>(*dset);
        dcmrt_dose->read(ditem);*/


    }
    else
    {
        std::cout<< "Loading falied" << std::endl;
        return false;
    }

    return true;

}

bool RtDose::getDoseArrayFromPinnacleTotalDoseAndHeaderFiles(std::string headerFilePath, std::string doseImageHeaderFilePath, std::string totalDoseBinaryFilePath)
{
    //TODO:: dont forget to read some other fields from the header file

    std::vector<std::string> headerLines;

    std::string str;
    int bytes_pix;
    int x_dim; // doseImageColumns
    int y_dim; // doseImageRows
    int z_dim; //
    float x_pixdim;
    float y_pixdim;
    float z_pixdim;
    float x_start;
    float y_start;
    float z_start;
    float X_offset; //
    float Y_offset; //  = 0.000000;
    float x_start_dicom;
    float y_start_dicom;
    std::ifstream file(headerFilePath.c_str());

    if (file.is_open())
    {


        while (!file.eof())
        {
            std::getline(file, str);

            // Process str
            std::size_t indexStart  = str.find("bytes_pix =");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+11;

                std::string bytes_pixString= str.substr (start,str.length()-start-1);
                bytes_pix =std::stoi(bytes_pixString);
                continue;
            }


            indexStart  = str.find("x_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string x_dimNumString=  str.substr (start,str.length()-start-1);
                x_dim =std::stoi(x_dimNumString);
               // doseImageColumns=x_dim;

                continue;
            }

            indexStart  = str.find("y_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string y_dimNumString=  str.substr (start,str.length()-start-1);
                y_dim =std::stoi(y_dimNumString);
                //doseImageRows=y_dim;
                continue;
            }

            indexStart  = str.find("z_dim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+8;
                std::string z_dimNumString=  str.substr (start,str.length()-start-1);
                z_dim =std::stoi(z_dimNumString);
              //  doseImageNumOfFrames=z_dim;
                continue;
            }

            //        x_pixdim = 0.300000;
            //            y_pixdim = 0.300000;
            //            z_pixdim = 0.300000;

            indexStart  = str.find("x_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string x_pixdimString=  str.substr (start,str.length()-start-1);
                x_pixdim =std::stof(x_pixdimString)*10;
                //DoseImagePixelSpacing[0]=x_pixdim;
                continue;

            }

            indexStart  = str.find("y_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string y_pixdimString=  str.substr (start,str.length()-start-1);
                y_pixdim =std::stof(y_pixdimString)*10;
               // DoseImagePixelSpacing[1]=y_pixdim;
                continue;

            }

            indexStart  = str.find("z_pixdim =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+11;
                std::string z_pixdimString=  str.substr (start,str.length()-start-1);
                z_pixdim =std::stof(z_pixdimString)*10;
               // DoseImageSpacingBetweenSlices=z_pixdim;
                continue;

            }

            indexStart  = str.find("x_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string x_startString=  str.substr (start,str.length()-start-1);
                x_start =std::stof(x_startString)*10;
              //   doseImagePositionPatient[0]=x_start;
                continue;

            }

            indexStart  = str.find("y_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string y_startString=  str.substr (start,str.length()-start-1);
                y_start =std::stof(y_startString)*10;
               // doseImagePositionPatient[1]=y_start;
                continue;

            }

            indexStart  = str.find("z_start =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string z_startString=  str.substr (start,str.length()-start-1);
                z_start =std::stof(z_startString)*10;
             //   doseImagePositionPatient[2]=z_start;
                continue;

            }






            indexStart  = str.find("X_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string X_offsetString=  str.substr (start,str.length()-start-1);
                X_offset =std::stof(X_offsetString)*10;



                continue;

            }

            indexStart  = str.find("Y_offset =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+10;
                std::string Y_offsetString=  str.substr (start,str.length()-start-1);
                Y_offset =std::stof(Y_offsetString)*10;
                continue;

            }
            indexStart  = str.find("x_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string x_start_dicomString=  str.substr (start,str.length()-start-1);
                x_start_dicom =std::stof(x_start_dicomString)*10;
                continue;

            }

            indexStart  = str.find("y_start_dicom =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+15;
                std::string y_start_dicomString=  str.substr (start,str.length()-start-1);
                y_start_dicom =std::stof(y_start_dicomString)*10;

                continue;

            }



            //headerFilePath.push_back(str);

        }
    } // is_open
    else
    {
        std::cout << "Error in opening header file" << std::endl;
    }
float DoseGridVoxelSizeX;
float DoseGridVoxelSizeY;
float DoseGridVoxelSizeZ;
float DoseGridDimensionX;
float DoseGridDimensionY;
float DoseGridDimensionZ;
float DoseGridOriginX;
float DoseGridOriginY;
float DoseGridOriginZ;





    std::ifstream doseImageHeaderFile(doseImageHeaderFilePath.c_str());
    if (doseImageHeaderFile.is_open())
    {


        while (!doseImageHeaderFile.eof())
        {
            std::getline(doseImageHeaderFile, str);

            // Process str
            std::size_t indexStart  = str.find("DoseGrid.VoxelSize.X =");
            if (indexStart!=std::string::npos)
            {

                std::size_t start = indexStart+22;

                std::string DoseGridVoxelSizeXString= str.substr (start,str.length()-start-1);
                DoseGridVoxelSizeX =std::stof(DoseGridVoxelSizeXString);
                DoseImagePixelSpacing[0]=DoseGridVoxelSizeX*10;
                continue;
            }


            indexStart  = str.find("DoseGrid.VoxelSize.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+22;
                std::string DoseGridVoxelSizeYString=  str.substr (start,str.length()-start-1);
                DoseGridVoxelSizeY = std::stof(DoseGridVoxelSizeYString);
                DoseImagePixelSpacing[1]=DoseGridVoxelSizeY*10;
                continue;
            }

            indexStart  = str.find("DoseGrid.VoxelSize.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+22;
                std::string DoseGridVoxelSizeZString=  str.substr (start,str.length()-start-1);
                DoseGridVoxelSizeZ = std::stof(DoseGridVoxelSizeZString);;
                DoseImageSpacingBetweenSlices=DoseGridVoxelSizeZ*10;
                continue;
            }


            indexStart  = str.find("DoseGrid.Dimension.X =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionXString=  str.substr (start,str.length()-start-1);
                DoseGridDimensionX =std::stoi(DoseGridDimensionXString);
                doseImageColumns=DoseGridDimensionX;


                continue;
            }

            indexStart  = str.find("DoseGrid.Dimension.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionYString=  str.substr (start,str.length()-start-1);
                DoseGridDimensionY =std::stoi(DoseGridDimensionYString);
                doseImageRows=DoseGridDimensionY ;
                continue;
            }

            indexStart  = str.find("DoseGrid.Dimension.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+23;
                std::string DoseGridDimensionZString=  str.substr (start,str.length()-start-1);
                DoseGridDimensionZ =std::stoi(DoseGridDimensionZString);
                doseImageNumOfFrames=DoseGridDimensionZ;
                continue;
            }


            //        x_pixdim = 0.300000;
            //            y_pixdim = 0.300000;
            //            z_pixdim = 0.300000;

            indexStart  = str.find("DoseGrid.Origin.X =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginXString =  str.substr (start,str.length()-start-1);
                DoseGridOriginX =std::stof(DoseGridOriginXString);

                continue;

            }

            indexStart  = str.find("DoseGrid.Origin.Y =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginYString =  str.substr (start,str.length()-start-1);
                DoseGridOriginY =std::stof(DoseGridOriginYString);
                continue;

            }

            indexStart  = str.find("DoseGrid.Origin.Z =");
            if (indexStart!=std::string::npos)
            {
                std::size_t start = indexStart+20;
                std::string DoseGridOriginZString =  str.substr (start,str.length()-start-1);
                DoseGridOriginZ =std::stof(DoseGridOriginZString);
                continue;

            }



            //headerFilePath.push_back(str);

        }
    } // is_open
    else
    {
        std::cout << "Error in opening dose image  header file" << std::endl;
    }




    //
    //y_start_dicom =-2*(CTImageInfo.y_dim-1)*CTImageInfo.y_pixdim; // we get these information from CT image files
    // y_start_dicom= -942.16087; // TODO: to fix this in the original file
    y_start_dicom = -2*(y_dim-1)*y_pixdim;
    //DoseImagePixelSpacingY= (-y_start_dicom-DoseGrid.Origin.Y - (DoseGrid.Dimension.Y-1)*DoseGrid.VoxelSize.Y)*10;
    doseImagePositionPatient[0] = x_start_dicom + DoseGridOriginX*10 ;
    doseImagePositionPatient[1] = -y_start_dicom -DoseGridOriginY*10 - (doseImageRows-1)*DoseImagePixelSpacing[1];
    doseImagePositionPatient[2] = -(DoseGridOriginZ*10+(DoseGridDimensionZ -1)*DoseImageSpacingBetweenSlices);

    coord1OFFirstPoint = doseImagePositionPatient[0];
    coord2OFFirstPoint = -doseImagePositionPatient[1]; // since it is HFS

    doseArray = std::vector<double> (DoseGridDimensionX*DoseGridDimensionY*DoseGridDimensionZ,0);
    auto doseArrayTemp =  std::vector<double> (DoseGridDimensionX*DoseGridDimensionY*DoseGridDimensionZ,0);

    readLitteEndianBinaryFileToStdDoubleVector(totalDoseBinaryFilePath, doseArrayTemp);

    // Centi Gray to Gray
    std::transform(doseArrayTemp.begin(), doseArrayTemp.end(), doseArrayTemp.begin(),std::bind1st(std::multiplies<double>(),.01));// Centi Gray to Gray

    auto maxDoseIt = std::max_element(doseArrayTemp.begin(), doseArrayTemp.end());
    maxDose = *maxDoseIt+.001;




    verticalGridIntervalDoseImage = -abs(DoseImagePixelSpacing[0]);
    horizontalGridIntervalDoseImage = abs(DoseImagePixelSpacing[1]);

    doseGridXvalues = std::vector<double>(doseImageColumns, 0);
    // doseGridXvalues = coord1OFFirstPoint: horizontalGridIntervalDoseImage : (doseImageColumns - 1)*horizontalGridIntervalDoseImage + coord1OFFirstPoint;
    for (int i = 0; i < doseImageColumns; i++)
    {
        doseGridXvalues[i] = coord1OFFirstPoint + i*(horizontalGridIntervalDoseImage);
    }
    // doseGridXvalues = coord2OFFirstPoint: verticalGridIntervalDoseImage : (doseImageRows - 1)*verticalGridIntervalDoseImage + coord2OFFirstPoint;
    doseGridYvalues = std::vector<double>(doseImageRows, 0);
    for (int j = 0; j < doseImageRows; j++)
    {
        doseGridYvalues[j] = coord2OFFirstPoint + j*(verticalGridIntervalDoseImage);
    }

    doseGridZvalues = std::vector<double>(doseImageNumOfFrames, 0);

    for (int iZplane= 0; iZplane < doseImageNumOfFrames; ++iZplane)
    {
        //doseGridZvalues[iZplane] = doseGridZvalues[iZplane] + doseImagePositionPatient[2];
        doseGridZvalues[iZplane] = -DoseImageSpacingBetweenSlices *iZplane -  doseImagePositionPatient[2]; // minus sign for DoseImageSpacingBetweenSlices *iZplan term is   to match to cerr  /Pinnacle convention

    }
    xyzMeshGrid(doseGridXvalues, doseGridYvalues, doseGridZvalues, doseArrayCoordinatesX, doseArrayCoordinatesY, doseArrayCoordinatesZ);


        std::reverse(doseArrayTemp.begin(),doseArrayTemp.end());

        std::vector <int> indTotalDoseArrayVecAfterReversing(doseArray.size(),0);
        std::iota(indTotalDoseArrayVecAfterReversing.begin(), indTotalDoseArrayVecAfterReversing.end(), 0);
        //std::vector <int> indTotalDoseArrayVecAfterReversing;
        //indTotalDoseArrayVecAfterReversing.push_back(2000);
        std::vector <int> SubX;
        std::vector <int> SubY;
        std::vector <int> SubZ;
        ind2SubForColMajorTensor(indTotalDoseArrayVecAfterReversing,DoseGridDimensionX,DoseGridDimensionY,DoseGridDimensionZ,SubX,SubY,SubZ);

        std::vector <int> DicomSubX(doseArray.size(),0);
        std::vector <int> DicomSubY(doseArray.size(),0);
        for (int i = 0; i < doseArray.size(); ++i)
        {
            DicomSubX[i] = DoseGridDimensionX-1-SubX[i];
            DicomSubY[i] = DoseGridDimensionY-1-SubY[i];
        }


        std::vector <int> indDoseArrayVecDICOM(doseArray.size(),0);
        //std::vector <int> indDoseArrayVecDICOM;
        //Sub2IndForColMajorTensor(indDoseArrayVecDICOM,DoseGridDimensionY,DoseGridDimensionX,DoseGridDimensionZ,SubX,SubY,SubZ);
        Sub2IndForColMajorTensor(indDoseArrayVecDICOM,DoseGridDimensionY,DoseGridDimensionX,DoseGridDimensionZ,DicomSubY,DicomSubX,SubZ);
        for (int i = 0; i < doseArray.size(); ++i)
        {
            // doseArray[i] =   doseArrayTemp[i];
            doseArray[indDoseArrayVecDICOM[i]] =   doseArrayTemp[indTotalDoseArrayVecAfterReversing[i]];
        }

    //  indVec.push_back(73);
    //    SubX.push_back(4);
    //    SubY.push_back(2);
    //    SubZ.push_back(1);
    //    Sub2IndForColMajorTensor(indVec,10,5,6,SubX,SubY,SubZ);
    //    Sub2IndForRowMajorTensor(indVec,10,5,6,SubX,SubY,SubZ);
    //     ind2SubForRowMajorTensor(indVec,10,5,6,SubX,SubY,SubZ);







    auto result = std::max_element(doseArray.begin(), doseArray.end());  // get the max dose to verify the
    auto indexMax = std::distance(doseArray.begin(), result);
    std::cout <<  "max element at: " << indexMax << '\n';
    std::cout <<  "max dose  = " << doseArray[indexMax] <<std::endl;
    std::cout <<  "max dose happens at: x =" << doseArrayCoordinatesX[indexMax] << "  y= " << doseArrayCoordinatesY[indexMax] << " z =" << doseArrayCoordinatesZ[indexMax] << std::endl;


    return true;
}

bool RtDose::getDoseArrayCoordiantes()
{

    // http://svn.openscenegraph.org/osg/OpenSceneGraph/trunk/src/osgPlugins/dicom/ReaderWriterDICOM.cpp
    //auto output = std::vector<PointInPolygon::Point3>(0, PointInPolygon::Point3());
    if (!rtDicomDoseFile)
    {
        std::cout << "Dose file has not been set yet." << std::endl;
        return false;
    }

    // populate_planC_dose_field
    //DcmFileFormat rtDoseDCMTKfileformat;
    OFCondition status = rtDoseDCMTKfileformat.loadFile(rtDicomDoseFile);

    if (status.good())
    {

        DcmObject *dset = &rtDoseDCMTKfileformat;
        rtDoseDCMTKfileformat.loadAllDataIntoMemory();
        dset = rtDoseDCMTKfileformat.getDataset();
        DcmDataset *d = rtDoseDCMTKfileformat.getDataset();

        // patient position
        double doseImagePositionPatient[3] = { 0.0, 0.0, 0.0 };
        for (int i = 0; i < 3; ++i)
        {
            if (d->findAndGetFloat64(DCM_ImagePositionPatient, doseImagePositionPatient[i], i).good()) // (00200032)
            {
                std::cout << "Read DOSE's DCM_ImagePositionPatient[" << i << "], " << doseImagePositionPatient[i] << std::endl;
            }
            else
            {
                std::cout << "ERROR in reading DOSE's DCM_ImagePositionPatient[" << i << "]" << std::endl;
                return false;
            }
        }

        //Pixel Spacing
        double pixelSpacingx;
        if (d->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingx, 0).good())// (0028,0030)
        {
            DoseImagePixelSpacing[0] = pixelSpacingx;
        }
        else
        {
            std::cout << "ERROR in reading DOSE's DCM_PixelSpacing X " << std::endl;
            return false;
        }

        double pixelSpacingy;
        if (d->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingy, 1).good())// (0028,0030)
        {
            DoseImagePixelSpacing[1] = pixelSpacingy;
        }
        else
        {
            std::cout << "ERROR in reading DCM_PixelSpacing Y " << std::endl;
            return false;
        }



        if (d->findAndGetUint16(DCM_Columns, doseImageColumns).good()) // (0028,0011)
        {

            std::cout << "doseImage number  of columns = " << doseImageColumns << std::endl;
        }
        else
        {
            std::cout << "ERROR in reading DCM_Columns" << std::endl;
        }




        for (int i = 0; i < 6; i++)
        {
            double value = 0.0;
            if (d->findAndGetFloat64(DCM_ImageOrientationPatient, value, i).good()) // (0020,0037) image orientation
            {
                DoseImageOrientation[i] = value;
            }
            else
            {
                std::cout << "Error in parsing dose image orientation " << std::endl;
                return false;
            }

        }

        if (DoseImageOrientation[0] == -1) // TODO: I am not sure why this is done reference populate_planC_dose_field
        {
            coord1OFFirstPoint = doseImagePositionPatient[0] - abs(pixelSpacingy)*(doseImageColumns - 1);
        }
        else
        {
            coord1OFFirstPoint = doseImagePositionPatient[0];
        }




        // https://www.dabsoft.ch/dicom/3/C.8.8.3.2/ Grid Frame Offset Vector GridFrameOffsetVector
        //a.If Grid Frame Offset Vector(3004, 000C) is present and its first element is zero, this attribute contains an array of n elements indicating the plane location of the data in the right - handed image coordinate system, relative to the position of the first dose plane transmitted, i.e., the point at which the Image Position(Patient) (0020, 0032) attribute is defined, with positive offsets in the direction of the cross product of the row and column directions.
        //	b.If Grid Frame Offset Vector(3004, 000C) is present, its first element is equal to the third element of Image Position(Patient) (0020, 0032), and Image Orientation(Patient) (0020, 0037) has the value(1, 0, 0, 0, 1, 0), then Grid Frame Offset Vector contains an array of n elements indicating the plane location(patient z coordinate) of the data in the patient coordinate system.
        //	In future implementations, use of option a) is strongly recommended.

        //double value = 0.0;
        //if (d->findAndGetFloat64(DCM_SpacingBetweenSlices, value).good())
        //{
        //	std::cout<< "DCM_SpacingBetweenSlices for DOSE image is  = " << value << std::endl;
        //	DoseImageSpacingBetweenSlices = value;
        //}
        //else
        //{
        //	std::cout << " error happend in reading DoseImageSpacingBetweenSlices" << std::endl;
        //}
        // NumberOfFrames

        if (!getCTXoffset())
        {

            std::cout << "Error in loading CT data offset" << std::endl;
            return false;
        }

        // TODO:
        if (patientPosition.compare("HFS") == 0)
        {
            coord1OFFirstPoint = coord1OFFirstPoint;

        }
        else if (patientPosition.compare("HFP") == 0)
        {
            coord1OFFirstPoint = -coord1OFFirstPoint;
            coord1OFFirstPoint = 2 * xOffsetCT - coord1OFFirstPoint;


            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFS") == 0)
        {
            coord1OFFirstPoint = coord1OFFirstPoint;

        }

        else if (patientPosition.compare("FFP") == 0)
        {
            coord1OFFirstPoint = -coord1OFFirstPoint;
            coord1OFFirstPoint = 2 * xOffsetCT - coord1OFFirstPoint;
            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            coord1OFFirstPoint = coord1OFFirstPoint;
            std::cout << " Patient position is " << patientPosition << " which is not supported using defalut position (HFS)" << std::endl;
        }

        //	if isstr(pPos)
        //		switch upper(pPos)
        //		case 'HFS'
        //		dataS = dataS;
        //case 'HFP'
        //	%dataS = -dataS; %APA commented
        //	dataS = -dataS;
        //	dataS = 2 * xOffsetCT - dataS;
        //	case 'FFS'
        //		%dataS = -dataS;
        //		dataS = dataS; %APA change
        //			case 'FFP'
        //			%dataS = dataS;
        //		dataS = -dataS;
        //		dataS = 2 * xOffsetCT - dataS;
        //		end
        //	else
        //	dataS = dataS; % default to HFS
        //	end


        if (!getCTYoffset())
        {
            std::cout << "Error in loading CT data offset" << std::endl;
            return false;
        }

        if (d->findAndGetUint16(DCM_Rows, doseImageRows).good()) // (0028,0011)
        {

            std::cout << "doseImage number  of Rows = " << doseImageRows << std::endl;
        }
        else
        {
            std::cout << "Error in reading DCM_Rows" << std::endl;
            return false;
        }


        if (DoseImageOrientation[1] == -1) // TODO: I am not sure why this is done reference populate_planC_dose_field
        {
            coord2OFFirstPoint = doseImagePositionPatient[1] + abs(pixelSpacingx)*(doseImageRows - 1);
        }
        else if ((DoseImageOrientation[1] == 0) && (patientPosition.compare("FFP") == 0)) //  flip is necessary to display couch at the bottom.How anout HFP ?
        {
            coord2OFFirstPoint = doseImagePositionPatient[1] + +abs(pixelSpacingx)*(doseImageRows - 1);
        }
        else
        {
            coord2OFFirstPoint = doseImagePositionPatient[1];
        }

        if (patientPosition.compare("HFS") == 0)
        {
            coord2OFFirstPoint = -coord2OFFirstPoint;
        }
        else if (patientPosition.compare("HFP") == 0)
        {
            coord2OFFirstPoint = coord2OFFirstPoint;


            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;

        }
        else if (patientPosition.compare("FFS") == 0)
        {
            coord2OFFirstPoint = -coord2OFFirstPoint;

            std::cout << " Patient position other than FFS, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFP") == 0)
        {
            coord2OFFirstPoint = -coord2OFFirstPoint;

            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            coord2OFFirstPoint = -coord2OFFirstPoint;
            std::cout << " Patient position is " << patientPosition << " which is not supported using defalut position (HFS)" << std::endl;

        }


        /*	if (imgOri(2) == -1)
                    dataS = iPP(2) + (abs(pixspac(1)) * (nRows - 1));
                    dataS = dataS / 10;
                    elseif(imgOri(2) == 0) && strcmpi(pPos, 'FFP') % flip is necessary to display couch at the bottom.How anout HFP ?
                    dataS = iPP(2) + (abs(pixspac(1)) * (nRows - 1));
                    dataS = dataS / 10;
                    else
                    dataS = iPP(2) / 10;
                    end

                    if isstr(pPos)
                    switch upper(pPos)
                    case 'HFS'
                    dataS = -dataS;
                    case 'HFP'
                    dataS = dataS;
                    case 'FFS'
                    dataS = -dataS;
                    case 'FFP'
                    dataS = dataS;
                    end
                    else
                    dataS = -dataS; % default to HFS
                    end*/


        verticalGridIntervalDoseImage = -std::abs(DoseImagePixelSpacing[0]);
        horizontalGridIntervalDoseImage = std::fabs(DoseImagePixelSpacing[1]);

        doseGridXvalues = std::vector<double>(doseImageColumns, 0);
        // doseGridXvalues = coord1OFFirstPoint: horizontalGridIntervalDoseImage : (doseImageColumns - 1)*horizontalGridIntervalDoseImage + coord1OFFirstPoint;
        for (int i = 0; i < doseImageColumns; i++)
        {
            doseGridXvalues[i] = coord1OFFirstPoint + i*(horizontalGridIntervalDoseImage);
        }
        // doseGridXvalues = coord2OFFirstPoint: verticalGridIntervalDoseImage : (doseImageRows - 1)*verticalGridIntervalDoseImage + coord2OFFirstPoint;
        doseGridYvalues = std::vector<double>(doseImageRows, 0);
        for (int j = 0; j < doseImageRows; j++)
        {
            doseGridYvalues[j] = coord2OFFirstPoint + j*(verticalGridIntervalDoseImage);
        }



        OFString numofFrames;
        if (d->findAndGetOFString(DCM_NumberOfFrames, numofFrames).good()) // (0028,0008)
        {
            std::cout << "Number of Frames in dose matrix (DCM_NumberOfFrames): " << numofFrames << std::endl;
            doseImageNumOfFrames = atoi(numofFrames.c_str());
        }
        else
        {
            std::cout << "Error in reading dose  DCM_NumberOfFrames" << std::endl;
            return false;

        }

        doseGridZvalues = std::vector<double>(doseImageNumOfFrames, 0);

        //  ftp://medical.nema.org/medical/dicom/final/cp697_ft.doc
        // %Frame Increment Pointer
        // 	fIP = dcm2ml_Element(dcmobj.get(hex2dec('00280009')));
        OFString doseFIP;

        // numberMultiFrameImages
        if (d->findAndGetOFString(DCM_FrameIncrementPointer, doseFIP).good()) // (0028,0009)
        {
            bool IsRelativeGridFrame;
            std::cout << "doseImage DCM_FrameIncrementPointer = " << doseFIP << std::endl;
            if (doseFIP.compare("(3004,000c)") == 0) // ftp://medical.nema.org/medical/dicom/final/cp434_ft.pdf or http://dicom.nema.org/medical/dicom/current/output/chtml/part03/sect_C.8.8.3.2.html
            {

                std::cout << "			 multi-frame pixel data are present and Frame Increment Pointer(0028, 0009) points to Grid Frame Offset Vector(3004, 000C)" << std::endl;
                double value;
                std::cout << "Reading DOSE's DCM_GridFrameOffsetVector" << std::endl;
                for (int i = 0; i < doseImageNumOfFrames; ++i)
                {

                    if (d->findAndGetFloat64(DCM_GridFrameOffsetVector, value, i).good()) // (00200032)
                    {
                        // std::cout << "Read DOSE's DCM_GridFrameOffsetVector[" << i << "], " << value << std::endl;
                        /*
                                 if ((imgOri(1) == -1) || (imgOri(5) == -1)) && ~isequal(pPos, 'HFP')
                                 gFOV = -gFOV;
                                 end*/
                        if (((DoseImageOrientation[0] == -1) || (DoseImageOrientation[4] == -1)) && (patientPosition.compare("HFP") != 0))
                        {
                            doseGridZvalues[i] = -value;
                        }
                        else
                        {
                            doseGridZvalues[i] = value;
                        }

                        if (i == 0)
                        {
                            if (doseGridZvalues[i] == 0)
                            {
                                std::cout << "Relatieve Dose Grid Frame" << std::endl;
                                IsRelativeGridFrame = true;
                            }
                            else
                            {
                                std::cout << "Absolute Dose Grid Frame" << std::endl;
                                IsRelativeGridFrame = false;
                            }
                        }

                        if (IsRelativeGridFrame) // Relative Grid Frame
                        {

                            doseGridZvalues[i] = doseGridZvalues[i] + doseImagePositionPatient[2];
                        }

                        doseGridZvalues[i] = -doseGridZvalues[i]; // we negate  this to match the coordinate to CERR coordinte system

                    }
                    else
                    {
                        std::cout << "ERROR in reading DOSE's DCM_GridFrameOffsetVector[" << i << "]" << std::endl;
                        return false;
                    }

                }
            }
            else // TODO :  See how to handle this
            {
                std::cout << "We are not supporting dose grid other than dose grid frame offset vector" << std::endl;
            }
        }
        else
        {
            std::cout << "ERROR in reading dose  DCM_FrameIncrementPointer" << std::endl;
            return false;

        }
    } // check DCM file status




    //doseArrayCoordinates = xyzMeshGrid(doseGridXvalues, doseGridYvalues, doseGridZvalues);
    xyzMeshGrid(doseGridXvalues, doseGridYvalues, doseGridZvalues, doseArrayCoordinatesX, doseArrayCoordinatesY, doseArrayCoordinatesZ);
    return true;
    //return output;
}



bool RtDose::getCTXoffset()
{

    if (!associateCTDicomFile)
    {
        std::cout << "Associated CT Dicom file has not been set yet." << std::endl;
        return false;
    }

    DcmFileFormat fileformat1;
    OFCondition status1 = fileformat1.loadFile(associateCTDicomFile);


    if (status1.good())
    {
        OFString pPos;
        //OFString patientPosition;
        if (fileformat1.getDataset()->findAndGetOFString(DCM_PatientPosition, pPos).good())
        {

            std::cout << "Patient's Pos: " << pPos << std::endl;
        }
        else
        {
            std::cout << "Error: cannot access patient position!" << std::endl;
            return false;
        }
        if (pPos.size() > 0)
        {
            std::cout << "Patient's Pos: " << pPos << std::endl;
            OFString pPosHFS = "HFS";

            std::cout << pPos.compare(pPosHFS) << std::endl;
            if (pPos.compare("HFS") == 0)				// HFS --> +x, -y, -z
            {
                patientPosition = "HFS";
            }
            else if (pPos.compare("HFP") == 0)		// "HFP" --> +x, +y, -z
            {
                patientPosition = "HFP";

            }
            else if (pPos.compare("FFS") == 0)		// FFS --> +x, -y, -z
            {
                patientPosition = "FFS";

            }
            else if (pPos.compare("FFP") == 0)		// FFP --> -x, +y, -z
            {
                patientPosition = "FFP";
            }

        }
        else
        {
            std::cout << "Patient's Pos is not in the dcm file provided !" << std::endl;
            return false;

        }

        for (int i = 0; i < 6; i++)
        {
            double value = 0.0;
            if (fileformat1.getDataset()->findAndGetFloat64(DCM_ImageOrientationPatient, value, i).good()) // (0020,0037) image orientation
            {
                cTImageOrientation[i] = value;
            }
            else
            {
                std::cout << "Error in parsing CT image orientation " << std::endl;
                return false;

            }

        }

        // patient position // TODO: Consider supporting Multiframe NM image. if there is no DCM_IMagePostionPatient
        double cTImagePositionPatient[3] = { 0.0, 0.0, 0.0 };
        for (int i = 0; i < 3; ++i)
        {
            if (fileformat1.getDataset()->findAndGetFloat64(DCM_ImagePositionPatient, cTImagePositionPatient[i], i).good()) // (00200032)
            {
                std::cout << "Read CT's DCM_ImagePositionPatient[" << i << "], " << cTImagePositionPatient[i] << std::endl;
            }
            else
            {
                std::cout << "NO CT's DCM_ImagePositionPatient" << std::endl;
                return false;

            }
        }





        //Pixel Spacing
        double pixelSpacingx;
        if (fileformat1.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingx, 0).good())// (0028,0030)
        {
            cTImagePixelSpacing[0] = pixelSpacingx;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing X in CT ? " << std::endl;
            return false;

        }

        double pixelSpacingy;
        if (fileformat1.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingy, 1).good())// (0028,0030)
        {
            cTImagePixelSpacing[1] = pixelSpacingy;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing Y in CT " << std::endl;
            return false;

        }




        if (fileformat1.getDataset()->findAndGetUint16(DCM_Columns, cTImageColumns).good()) // (0028,0011)
        {

            std::cout << "CT image number  of columns = " << cTImageColumns << std::endl;
        }
        else
        {
            std::cout << "Error in reading CT image tag DCM_Columns" << std::endl;
            return false;

        }

        if (abs(cTImageOrientation[0] - 1) < 1e-5)
        {
            xOffsetCT = cTImagePositionPatient[0] + (pixelSpacingy * (cTImageColumns - 1) / 2);

        }
        else if (abs(cTImageOrientation[0] + 1) < 1e-5)
        {

            xOffsetCT = cTImagePositionPatient[0] - (pixelSpacingy* (cTImageColumns - 1) / 2);

        }
        else
        {
            xOffsetCT = cTImagePositionPatient[0];
        }



        if (patientPosition.compare("HFS") == 0)
        {
            xOffsetCT = xOffsetCT;
        }
        else if (patientPosition.compare("HFP") == 0)
        {
            xOffsetCT = -xOffsetCT;
            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFS") == 0)
        {
            xOffsetCT = xOffsetCT;
            std::cout << " Patient position other than FFS, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFP") == 0)
        {
            xOffsetCT = -xOffsetCT;
            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            xOffsetCT = xOffsetCT;
            std::cout << " Patient position is " << patientPosition << " which is not supported using defalut position (HFS)" << std::endl;
        }

        std::cout << "xoffsetCT = " << xOffsetCT << std::endl;
    }
    else

    {
        std::cout << "Error: cannot read the CT dicom file " << std::endl;

        return false;
    }

    return true;
    //%Image Position(Patient)
    //	imgpos = dcm2ml_Element(dcmobj.get(hex2dec('00200032')));

    //imgOri = dcm2ml_Element(dcmobj.get(hex2dec('00200037')));

    //if isempty(imgpos)
    //	% Multiframe NM image.
    //	detectorInfoSequence = dcm2ml_Element(dcmobj.get(hex2dec('00540022')));
    //imgpos = detectorInfoSequence.Item_1.ImagePositionPatient;
    //imgOri = detectorInfoSequence.Item_1.ImageOrientationPatient;
    //end

    //	%Pixel Spacing
    //	pixspac = dcm2ml_Element(dcmobj.get(hex2dec('00280030')));

    //%Columns
    //	nCols = dcm2ml_Element(dcmobj.get(hex2dec('00280011')));

    //if (imgOri(1) - 1) ^ 2 < 1e-5
    //	xOffsetCT = imgpos(1) + (pixspac(2) * (nCols - 1) / 2);
    //elseif(imgOri(1) + 1) ^ 2 < 1e-5
    //	xOffsetCT = imgpos(1) - (pixspac(2) * (nCols - 1) / 2);
    //else
    //	% by Deshan Yang, 3 / 2 / 2010
    //	xOffsetCT = imgpos(1);
    //end

    //%Convert from DICOM mm to CERR cm.
    //	switch upper(pPos)
    //	case 'HFS'
    //	dataS = xOffsetCT / 10;
    //case 'HFP'
    //	dataS = -xOffsetCT / 10;
    //	case 'FFS'
    //		dataS = xOffsetCT / 10;
    //		case 'FFP'
    //			dataS = -xOffsetCT / 10;
    //			end

}


bool RtDose::getCTYoffset()
{
    if (!associateCTDicomFile)
    {
        std::cout << "Associated CT Dicom file has not been set yet." << std::endl;
        return false;
    }

    DcmFileFormat fileformat1;
    OFCondition status1 = fileformat1.loadFile(associateCTDicomFile);
    //OFCondition status1 = fileformat1.loadFile(RTPLAN_FILENAME);

    if (status1.good())
    {
        OFString pPos;
        //OFString patientPosition;
        if (fileformat1.getDataset()->findAndGetOFString(DCM_PatientPosition, pPos).good())
        {

            std::cout << "Patient's Pos: " << pPos << std::endl;
        }
        else
        {
            std::cout << "Error: cannot access patient position!" << std::endl;
            return false;
        }
        if (pPos.size() > 0)
        {
            std::cout << "Patient's Pos: " << pPos << std::endl;
            OFString pPosHFS = "HFS";

            std::cout << pPos.compare(pPosHFS) << std::endl;
            if (pPos.compare("HFS") == 0)				// HFS --> +x, -y, -z
            {
                patientPosition = "HFS";
            }
            else if (pPos.compare("HFP") == 0)		// "HFP" --> +x, +y, -z
            {
                patientPosition = "HFP";

            }
            else if (pPos.compare("FFS") == 0)		// FFS --> +x, -y, -z
            {
                patientPosition = "FFS";

            }
            else if (pPos.compare("FFP") == 0)		// FFP --> -x, +y, -z
            {
                patientPosition = "FFP";
            }

        }
        else
        {
            std::cout << "Patient's Pos is not in the dcm file provided !" << std::endl;
            return false;
        }


        for (int i = 0; i < 6; i++)
        {
            double value = 0.0;
            if (fileformat1.getDataset()->findAndGetFloat64(DCM_ImageOrientationPatient, value, i).good()) // (0020,0037) image orientation
            {
                cTImageOrientation[i] = value;
            }
            else
            {
                std::cout << "Error in parsing CT image orientation " << std::endl;
                return false;
            }

        }

        // patient position // TODO: Consider supporting Multiframe NM image. if there is no DCM_IMagePostionPatient
        double cTImagePositionPatient[3] = { 0.0, 0.0, 0.0 };
        for (int i = 0; i < 3; ++i)
        {
            if (fileformat1.getDataset()->findAndGetFloat64(DCM_ImagePositionPatient, cTImagePositionPatient[i], i).good()) // (00200032)
            {
                std::cout << "Read CT's DCM_ImagePositionPatient[" << i << "], " << cTImagePositionPatient[i] << std::endl;
            }
            else
            {
                std::cout << "Error : NO CT's DCM_ImagePositionPatient" << std::endl;
                return false;
            }
        }





        //Pixel Spacing
        double pixelSpacingx;
        if (fileformat1.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingx, 0).good())// (0028,0030)
        {
            cTImagePixelSpacing[0] = pixelSpacingx;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing X in CT ? " << std::endl;
            return false;

        }

        double pixelSpacingy;
        if (fileformat1.getDataset()->findAndGetFloat64(DCM_PixelSpacing, pixelSpacingy, 1).good())// (0028,0030)
        {
            cTImagePixelSpacing[1] = pixelSpacingy;
        }
        else
        {
            std::cout << "Error in reading DCM_PixelSpacing Y in CT " << std::endl;
            return false;

        }




        if (fileformat1.getDataset()->findAndGetUint16(DCM_Rows, cTImageRows).good()) // (0028,0011)
        {

            std::cout << "CT image number  of Rows = " << cTImageRows << std::endl;
        }
        else
        {
            std::cout << "Error in reading CT image tag DCM_Rows" << std::endl;
            return false;
        }

        if (abs(cTImageOrientation[4] - 1) < 1e-5)
        {
            yOffsetCT = cTImagePositionPatient[1] + (cTImagePixelSpacing[0] * (cTImageRows - 1) / 2);

        }
        else if (abs(cTImageOrientation[4] + 1) < 1e-5)
        {

            yOffsetCT = cTImagePositionPatient[1] - (cTImagePixelSpacing[0] * (cTImageRows - 1) / 2);

        }
        else
        {
            yOffsetCT = cTImagePositionPatient[1];
        }



        if (patientPosition.compare("HFS") == 0)
        {
            yOffsetCT = -yOffsetCT;
        }
        else if (patientPosition.compare("HFP") == 0)
        {
            yOffsetCT = yOffsetCT;
            std::cout << " Patient position other than HFP, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFS") == 0)
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position other than FFS, it is supported but not tested extensively" << std::endl;
        }
        else if (patientPosition.compare("FFP") == 0)
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position other than FFP, it is supported but not tested extensively" << std::endl;
        }
        else
        {
            yOffsetCT = -yOffsetCT;
            std::cout << " Patient position is " << patientPosition << " which is not supported using defalut position (HFS)" << std::endl;
        }
        std::cout << "yoffsetCT = " << yOffsetCT << std::endl;


    }
    else
    {
        std::cout << "Error: cannot read the CT dicom file " << std::endl;
        return false;
    }
    return true;

}

