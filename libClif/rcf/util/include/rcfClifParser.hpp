// Author: M.Fatyga
// Date: 03/14/2006
// This class provides utilities for
// parsing text files in Clif format
// Clif format is used by the Pinnacle TPS
// in its database. 

#ifndef __rcfClifParser
#define __rcfClifParser

#include "cfGenDefines.h" // data types below are defined here
#include "cf_read_clif.h"
#include <stdio.h>
#include <vector>
#include <string>

#define MAX_STR_LEN 1024

/**
    \class rcfClifParser
    @ingroup UTIL
*/

class rcfClifParser
{

   protected:

      clifObjectType myclif;
      int clif_ready;
      int open_for_write;
      char *func;
      int myerrno;
      int debug_flag;

      //working buffers
      clifObjectType *wrk_clif;
      int sel_level;
      FILE *fid_out;

      //search support
      int object_found;
      int search_level;

      //branch mark control
      clifObjectType *mark_clif;

      //iterator support
      bool iterator;
      clifObjectType *top_clif;
      std::vector<rcfClifParser *> iterators;
      std::vector<rcfClifParser *> block_iterators;
      std::vector<rcfClifParser *> unhandled_block_iterators;


   public:

      // default constructor should be used primarily if
      // you are constructing the clif programmatically
      rcfClifParser();
      // read clif from file, and assign it a handle
      rcfClifParser(char *file, char *handle);
      // read clif from file, do not assign a handle to top
      // level clif
      rcfClifParser(char *file);
      // destructor
     ~rcfClifParser();

      // Build and operate on Clif tree read from file

      // building and operating on the top level clif

      // read clif from file. If the handle is NULL, the
      // top cliff handle will not be assigned. This has
      // a consequence when the clif is written out. It will
      // not be bracketed at the top level. If the handle is
      // provided, top cliff will be assigned a handle, and
      // the top cliff will be bracketed with handle written
      // out. NOTE: you do not need to use this function if
      // you already constructed the object with Clif file name 
      // (see constructor list).
      int readClifFromFile(char *file,char *handle);


      // check if the clif is ready for operations
      // after file read
      int isClifReady();


      // get info about the top clif level
      int getClifLevel(int *nlevel);
      int getNumberOfClifObjects(int *nclifs);
      int getNumberOfArrayObjects(int *narrays);
      int getNumberOfDataObjects(int *ndata);


      // open the file for output
      // calling this function repeatedly
      // closes currently opened file unconditionally
      int setFileOutput(char *name);
      void closeFileOutput();

      // dump clif. If the output file is set, clif will
      // be written into the output file. Otherwise, stderr
      int dumpClif();


      // finding and selecting clifs in a tree
      // NOTE: because of the potential complexity
      // of the clif tree, this object requires you 
      // to select an individual clif you will operate
      // on. Once selected, you can get info on and
      // access to data within this cliff. Functions
      // below give you several modes of clif selection.

      // search the tree for a clif and select it

      // NOTE: the first clif with usrHandle will be selected
      // when found. If there are multiple clifs with the same
      // handle, the first one will be selected
      int findAndSelectClif(const char *usrHandle, int *found);

      //if the first clif you find is not to your liking, and you
      // think there are more with the same Handle, use this function
      // and set the skip level to the desired level. For example,
      // if skip=1, the first clif found will be skipped, and the
      // second one will be searched for.
      int findAndSelectClifWithSkip(const char *usrHandle, int *found,int skip);

      // find clif in a branch attached to the currently
      // selected clif. This allows you to drill into 
      // the clif tree.
      int iterateAndSelectClif(const char *usrHandle, int *found);

      
      // select n-th clif in the branch directly attached
      // to currently selected clif.
      int selectClifObject(int nClif);

      // re-select the top clif
      void resetClifObjectSelection();

      // mark selected clif for easy return
      void markCurrentClif();
      int selectMarkedClif();


      // services for selected clif
      // NOTE: When the object is created, top clif
      // is automatically selected. 

      // dump selected clif only
      // NOTE: same rules as with full clif.
      // If the output file was selected, the
      // output goes there. Otherwise, the output
      // goes to stderr.
      // NOTE: the output file remains open for
      // write until the object is destroyed, or
      // file is explicitely closed. If you call this
      // function repeatedly, file will be appended.
      int dumpSelectedClif();
      
      clifObjectType* getClifStructure();

      // access data in selected clif
      char *getHandleOfSelectedClif(int *verify);
      int getNumberOfClifObjectsInSelectedClif(int *nobjects);
      int getNumberOfArrayObjectsInSelectedClif(int *narrays);
      int getNumberOfDataObjectsInSelectedClif(int *ndata);
      char *getHandleOfArrayInSelectedClif(int narray,int *verify);
      float *getArrayBufferInSelectedClif(int narray,int *length,int *verify);
      float *getArrayBufferInSelectedClif(const char* usrHandle,int *length,int *verify);
      char *getHandleOfDataObjectInSelectedClif(int ndata,int *verify);
      char *getDataBufferInSelectedClif(int ndata,int *verify);
      int getDataBufferInSelectedClif(int ndata,int *out,int *verify);
      int getDataBufferInSelectedClif(int ndata,float *out,int *verify);
      char *getDataBufferInSelectedClif(const char* usrHandle,int *verify);
      int getDataBufferInSelectedClif(const char* usrHandle,int *out,int *verify);
      int getDataBufferInSelectedClif(const char* usrHandle,unsigned long *out,int *verify);
      int getDataBufferInSelectedClif(const char* usrHandle,float *out,int *verify);
      // the following function performs comparison of data value with template
      // taking data value to lower string before comparison. Thus user can construct
      // case insensitive comparison with template string.
      bool downstringCompareDataBufferInSelectedClif(const char* usrHandle, const char* utempl, bool *verify);
      bool downstringCompare(std::string usr1, std::string usr2);

      // ITERATORS: generate rcfClifParser object which is limited to
      // a sub-clif of a parent clif.
      // spawn a single iterator
      rcfClifParser* getIteratorOnSelectedClif(int *verify);
      bool isThisAnIterator();

      // create internally, and give access to, an array of iterators
      // which are directly subordinate to the currently selected clif.
      // The same function can be called on iterators. These functions
      // can be used to traverse trees, without changing the selection
      // state of the parent.
      // NOTE: For the first two functions, iterators will be deleted next time
      // the function is called, or when parent is destroyed. 
      // These functions are meant for immediate, in-situ
      // information retrieval (call and use). The --Unhandled-- functions
      // create iterators that are detached from this object (they are still iterators,
      // parent must be alive). User is responsible for deleting unhandled iterators after use,
      // else memory leak.
      int createSubordinateIteratorBlockOnSelectedClif(int *nobjects);
      rcfClifParser* getIteratorFromBlock(unsigned int index);
      int createUnhandledSubordinateIteratorBlockOnSelectedClif(int *nobjects);
      rcfClifParser* getUnhandledIteratorFromBlock(unsigned int index);

      // NOTE: all iterators are owned by the parent, get automatically
      // destroyed when the parent gets destroyed.

      // Build a clif programmatically
      // NOTE: Reading Clif from file and then modifying it programmatically
      // are currently incompatible modes. Selecting Top clif to write
      // checks if the object is "clean", and allows you to start
      // writng to the object.
      int selectTopClifToWrite(char *usrHandle);
      int selectTopClifToWrite();

      // add data objects to selected clif
      int addClifArrayToSelectedClif(int nobjs);
      int addDataObjectArrayToSelectedClif(int nobjs);
      int addArrayObjectArrayToSelectedClif(int nobjs);

      // If you add an array of clifs and you want
      // start modifying one of the new clifs, select
      // it first, using selectClifObject(int nobj)

      // actually set data in created data objects
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, char *Data,char *usr_delim);
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, char *Data);
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, int Data);
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, float Data, int precision = 0);
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, bool Data);
      int setDataObjectInSelectedClif(int nobj, char *usrHandle, unsigned long Data);
      int setArrayObjectInSelectedClif(int nobj, char *usrHandle, float *buffer, int length);
      // set handle in the clif you just selected
      int setHandleInSelectedClif(char *usrHandle);


      // debugging
      void setDebug();
      void setDebugSearch();

   private:

      // private functions written for this object

      void basicInit();
      void buildParser(char *file, char *handle);
      void reportError(const char*);
      void reportError(const char*, char*);
      void reportWarning(const char*);
      int searchForClif(clifObjectType *clif, const char *usrHandle, int *found,int skip);
      int searchForDataObjectInClif(struct clifObjectType *clif, const char *usrHandle, int *found,clifDataType **out);
      int searchForArrayObjectInClif(struct clifObjectType *clif, const char *usrHandle, int *found,clifArrayType **out);
      int addObjectArrayToSelectedClif(int nobjs,int select);
      int setDataObjectInSelectedClifSelect(int nobj, char *usrHandle, char *Data,int select,char *usr_delim);




      // legacy code functions wrapped by this object.
      // this code was developed by J.V.Siebers at Virginia Commonwealth University

      int dumpClifStructure(clifObjectType *clif);
      int freeClifStructure(clifObjectType *clif); // free's all of the memory allocated by a clif struture
      /* Routines used for Reading a Value from a Clif Structure */
      int readClifStructure(clifObjectType *clif, char *Name, char **Value);
      int readClifArrayStructure(clifObjectType *clif, char *messageHandle, int *nArray, float **Array);
      /* Routines used for Reading Clif From a File */
      int readClifStringName(char *inString, char *delimiter, char **outString);
      int readClifStringValue(char *inString, char *delimiter, char **outString,int *isquote);
      int readClifArray(FILE *istrm,clifArrayType *Array);
      int readClifLevel(FILE *istrm, clifObjectType *clif);
      int readClifData(FILE *istrm, clifObjectType *clif);
      int readClif(FILE *istrm, clifObjectType *clif, char *clifName);
      int readClifFile(FILE *istrm, clifObjectType *clif, char *clifName);
      int readSpecificClif(FILE *istrm, clifObjectType *clif, char *clifHandle, char *clifName);
      int getMatchingClifAddress(clifObjectType *clif, char *handleToMatch, char *valueToMatch, clifObjectType **matchingObject);
      int getClifAddress(clifObjectType *Clif, char *messageHandle, clifObjectType **Object);
      int getClifObjectNumber(int nObjects,  clifObjectType *Object, char *objectHandle, int *iObject);
      int findClifObjectNumber(int nObjects,  clifObjectType *Object, char *objectHandle, int *iObject);
      int readStringClifStructure(clifObjectType *clif, char *messageHandle, char *Value);// read preallocted string
      int readIntClifStructure(clifObjectType *clif, char *messageHandle, int *Value);
      int readFloatClifStructure(clifObjectType *clif, char *messageHandle, float *Value);
      int checkIfObjectList(clifObjectType *Object);
      int checkIfObjectList(clifObjectType *Object, int nObjects);
      int checkForClifObject(clifObjectType *clif, char *messageHandle);
      int parseClifObjectNumber(int nObjects, clifObjectType *Object, char *objectHandle, int *iObject);
      int readClifDataObject(int *nDataObjects, clifDataType **Data, char *string, char *delim);
      int processClifData(FILE *istrm, clifObjectType *clif, char *string);
      char* strdup(char *originalString);
      int checkForStoreString(char *input_string);
      char* strnset(char *s, int ch, size_t n);




      int clif_check_if_line(FILE *istrm, char *string, char *ostring);
      int clif_string_after(FILE *istrm, char *string, char *str);
      float clif_get_value(FILE *istrm, char *string);
      int clif_get_to_line(FILE *istrm, char *string);
      int clif_string_read(FILE *fp, char *string, char *ostring);
      char *clif_fgets(char *string, int Max_Len, FILE *fp);
      char *clifFgets(char *string, int Max_Len, FILE *fp);
      int fget_cstring(char *string, int Max_Str_Len, FILE *fspec);

};

#endif
