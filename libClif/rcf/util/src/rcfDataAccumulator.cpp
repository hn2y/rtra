
#include "rcfDirectoryMgr.hpp"
#include "rcfDataAccumulator.hpp"
#include <string>
#include <string.h>
#include <stdio.h>
#include "cfGenDefines.h"
#include "cfTypedefs.hpp"
#include <iostream>
#include <sys/stat.h>
#include <stdlib.h>

using namespace std;


void rcfDataAccumulator::basicInit()
{
   request = NULL;
   logid = NULL;
   errlogid = stderr;
}

rcfDataAccumulator::rcfDataAccumulator()
{
   basicInit();
}

rcfDataAccumulator::~rcfDataAccumulator()
{
   reInitList();
   if(NULL != request)
   {
       delete request;
       request = NULL;
   }
   if(NULL != logid)
   {
         time_t mytime = time(NULL);
         fprintf(logid,"\n LAST SESSION CLOSED AT: %s\n\n",ctime(&mytime));
      fclose(logid);
   }
   if(stderr != errlogid)
   {
         time_t mytime = time(NULL);
         fprintf(errlogid,"\n LAST SESSION CLOSED AT: %s\n\n",ctime(&mytime));
      fclose(errlogid);
   }
}

int rcfDataAccumulator::setSystemLog(rcfClifParser *parser)
{
   func = (char*)"rcfDataAccumulator::setSystemLog";
   int ndata;
   int rtn,verify;
   // sanity checks
   if(NULL == parser)
   {
      reportError("NULL pointer passed in lieu of parser");
      return(CF_OK);
   }

   // verify there is a systemlog and set it
   rtn = parser->getNumberOfDataObjects(&ndata);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of data objects in top clif");
      return(CF_FAIL);
   }
   if( ndata <= 0 )
   {
      // this is fine, log is not mandatory
      return(CF_OK);
   }

   char *cbuffer;
   int ifound = 0;
   std::string fname;

   char *ebuffer;
   int ifounderr = 0;
   std::string errfname;

   for( int i=0; i<ndata; i++)
   {
      cbuffer = request->getHandleOfDataObjectInSelectedClif(i,&verify);
      if( (NULL != cbuffer) && (1 == verify) )
      {
         if( 0 == strcmp(cbuffer,"systemlog"))
         {
            cbuffer = request->getDataBufferInSelectedClif(i,&verify);
            if( (NULL == cbuffer) || (0 == verify) )
            {
               reportError("Failure to get data associated with systemlog tag");
               return(CF_FAIL);
            }
            fname = std::string(cbuffer);
            ifound = 1;
         }
         if( 0 == strcmp(cbuffer,"errorlog"))
         {
            ebuffer = request->getDataBufferInSelectedClif(i,&verify);
            if( (NULL == ebuffer) || (0 == verify) )
            {
               reportError("Failure to get data associated with errorlog tag");
               return(CF_FAIL);
            }
            errfname = std::string(ebuffer);
            ifounderr = 1;
         }
      }
   }
   if(1 == ifound)
   {
      logid = fopen(fname.c_str(),"a");
      if(NULL == logid)
      {
         reportError("Failure to open logfile. The logfile directory must exist. This object is not permitted to create it");
         fprintf(errlogid,"file name: %s\n",fname.c_str());
         return(CF_FAIL);
      }
      else
      {
         time_t mytime = time(NULL);
         fprintf(logid,"\n FILE OPENED FOR ACCESS: %s\n\n",ctime(&mytime));
         fflush(logid);
      }
   }
   if(1 == ifounderr)
   {
      errlogid = fopen(errfname.c_str(),"a");
      if(NULL == errlogid)
      {
         reportError("Failure to open error logfile. The logfile directory must exist. This object is not permitted to create it");
         fprintf(errlogid,"file name: %s\n",errfname.c_str());
         return(CF_FAIL);
      }
      else
      {
         time_t mytime = time(NULL);
         fprintf(errlogid,"\n FILE OPENED FOR ACCESS: %s\n\n",ctime(&mytime));
         fflush(errlogid);
      }
   }
   return(CF_OK);
}

int rcfDataAccumulator::setRoot(rcfClifParser *parser)
{
   int rtn,verify;
   int ndata;

   func = (char*)"rcfDataAccumulator::setRoot";
   if(!(parser->isClifReady()))
   {
      reportError("parser is not valid");
      return(CF_FAIL);
   }

   // verify there is a root and set it
   rtn = parser->getNumberOfDataObjects(&ndata);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of data objects in top clif");
      return(CF_FAIL);
   }
   if( ndata <= 0 )
   {
      reportError("Root directory for patient accumulation must be defined");
      return(CF_FAIL);
   }

   char *cbuffer;
   int ifound = 0;

   for( int i=0; i<ndata; i++)
   {
      cbuffer = request->getHandleOfDataObjectInSelectedClif(i,&verify);
      if( (NULL != cbuffer) && (1 == verify) )
      {
         if( 0 == strcmp(cbuffer,"root"))
         {
            cbuffer = request->getDataBufferInSelectedClif(i,&verify);
            if( (NULL == cbuffer) || (0 == verify) )
            {
               reportError("Failure to get data associated with root tag");
               return(CF_FAIL);
            }
            root = std::string(cbuffer);
            ifound = 1;
            break;
         }
      }
   }
   if(1 == ifound)
   {
      // verify that root exists
      rcfDirectoryMgr tmgr;
      rtn = tmgr.loadLine((char*)(root.c_str()));
      if(CF_OK != rtn)
      {
         reportError("Failure to load root directory line");
         return(CF_FAIL);
      }
      // test if root exists
      rtn = tmgr.testLine(&verify);
      if(CF_OK != rtn)
      {
         reportError("Failure to test root directory line");
         return(CF_FAIL);
      }
      if(0 == verify)
      {
         reportError("Root directory must exist, this object is not allowed to create it");
         fprintf(errlogid,"Failed to find: %s\n",root.c_str());
         root = std::string("");
         return(CF_FAIL);
      }
      return(CF_OK);
   }
   else
   {
      reportError("root directory not found. It must be defined, and it must exist");
      return(CF_FAIL);
   }
   return(CF_FAIL);
}

int rcfDataAccumulator::processRequest(char *file)
{
   func = (char*)"rcfDataAccumulator::processRequest";
   int rtn,verify;
   int nlists; 
   
   request = new rcfClifParser(file);
   if(NULL == request)
   {
      reportError("Failure to create the request parser");
      return(CF_FAIL);
   }
   if(!(request->isClifReady()))
   {
      reportError("processing request is not valid at construction");
      delete request;
      request = NULL;
      return(CF_FAIL);
   }

   rtn = setRoot(request);
   if(CF_OK != rtn)
   {
      reportError("Failure to set root. Root directory must be defined, and it must exist");
      return(CF_FAIL);
   }

   rtn = setSystemLog(request);
   if(CF_OK != rtn)
   {
      // ok to return failure, system log may not have been defined
      // issue warning and go ahead
      reportWarning("System log not defined, operations will not be logged");
   }
   

   rtn = request->getNumberOfClifObjects(&nlists);
   if( (CF_OK != rtn) || (nlists <= 0) )
   {
      reportError("Failure to obtain patient lists");
      return(CF_FAIL);
   }
   for(int i=0; i<nlists; i++)
   {
      request->resetClifObjectSelection();
      rtn = request->selectClifObject(i);
      if(CF_OK != rtn)
      {
         reportError("Failure to select clif object");
         fprintf(errlogid,"at index: %d\n",i);
         return(CF_FAIL);
      }
      // check if this is a legal patient list
      char *lhandle = NULL;
      lhandle = request->getHandleOfSelectedClif(&verify);
      if( (NULL == lhandle) || (0 == verify) )
      {
         // issue warning and move on
         reportWarning("Failure to obtain handle of presumed patient list, skipping");
         fprintf(errlogid,"at index: %d\n",i);
         continue; // go to the next item
      }
      if( !(0 == strcmp(lhandle,"patient_list")) )
      {
         // issue warning and move on
         reportWarning("Failure to verify handle of presumed patient list, skipping");
         fprintf(errlogid,"observed handle: %s at index: %d\n",lhandle,i);
         continue; // go to the next item
      }
      rtn = processPatientList(request);
      if(CF_OK != rtn)
      {
         reportError("Failure to process patient list");
         fprintf(errlogid,"at index: %d\n",i);
      }

   } // end of loop over patient lists
   return(CF_OK);
}

void rcfDataAccumulator::reInitList()
{
   func = (char*)"rcfDataAccumulator::reInitList";
   for(unsigned int i=0; i<tgtdir.size(); i++)
   {
      if(NULL != tgtdir[i])
      {
          delete tgtdir[i];
          tgtdir[i] = NULL;
      }
   }
   tgtdir.clear();
   for(unsigned int i=0; i<srcdir.size(); i++)
   {
      if(NULL != srcdir[i])
      {
          delete srcdir[i];
          srcdir[i] = NULL;
      }
   }
   srcdir.clear();
   for(unsigned int i=0; i<corr_tgt.size(); i++)
   {
      if(NULL != corr_tgt[i])
      {
          delete corr_tgt[i];
          corr_tgt[i] = NULL;
      }
   }
   corr_tgt.clear();
   for(unsigned int i=0; i<corr_src.size(); i++)
   {
      if(NULL != corr_src[i])          
      {
          delete corr_src[i];
          corr_src[i] = NULL;
      }
   }
   corr_src.clear();
   for(unsigned int i=0; i<comm_src.size(); i++)
   {
      if(NULL != comm_src[i])
      {
          delete comm_src[i];
          comm_src[i] = NULL;
      }
   }
   comm_src.clear();
   for(unsigned int i=0; i<comm_tgt.size(); i++)
   {
      if(NULL != comm_tgt[i])
      {
          delete comm_tgt[i];
          comm_tgt[i] = NULL;
      }
   }
   comm_tgt.clear();
   for(unsigned int i=0; i<patient_list.size(); i++)
   {
      if(NULL != patient_list[i])
      {
          delete patient_list[i];
          patient_list[i] = NULL;
      }
   }
   patient_list.clear();
   
}

int rcfDataAccumulator::processPatient(unsigned int index)
{
   func = (char*)"rcfDataAccumulator::processPatient";

   // sanity check
   if( index >= patient_list.size() )
   {
      reportError("Illegal index");
      fprintf(errlogid,"Index observed: %d  Minimum Allowed: 0, Maximum Allowed: %ld\n",index,patient_list.size()-1);
      return(CF_FAIL);
   }

   if(0 == root.size())
   {
      reportError("The root directory must be defined, and must exist");
      return(CF_FAIL);
   }

   std::string src;
   std::string tgt;

   // process correlated data
   if( corr_src.size() != corr_tgt.size() )
   {
      reportError("Inconsistence in the size of correlated data queues");
      return(CF_FAIL);
   }
   
   rcfDirectoryMgr *srcmgr = NULL;
   rcfDirectoryMgr *tgtmgr = NULL;
   int rtn,verify;
   for(unsigned int i=0; i<corr_src.size(); i++)
   {
      src = *(corr_src[i]) + std::string("/") + *(patient_list[index]);
      tgt = root + std::string("/") + *(patient_list[index]) + std::string("/") + *(corr_tgt[i]);
      srcmgr = new rcfDirectoryMgr;
      if(NULL == srcmgr)
      {
         reportError("Failure to create source manager");
         return(CF_FAIL);
      }
      else 
      {
         if(NULL != logid)
         {
            fprintf(logid,"\n Processing for Patient: %s\n",(*(patient_list[index])).c_str());
         }
         rtn = srcmgr->loadLine((char*)(src.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load source line");
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
         // source must exist
         rtn = srcmgr->testLine(&verify);
         if( (CF_OK != rtn) || (0 == verify) )
         {
            reportError("Source Directory Must Exist");
            fprintf(errlogid,"missing source directory: %s\n",src.c_str());
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
      }
      tgtmgr = new rcfDirectoryMgr;
      if(NULL == tgtmgr)
      {
         reportError("Failure to create target manager");
         delete srcmgr;
         srcmgr = NULL;
         return(CF_FAIL);
      }
      else 
      {
         rtn = tgtmgr->loadLine((char*)(tgt.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load target line");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         // check if target exists, create it if you can 
         rtn = tgtmgr->testLine(&verify);
         if( (CF_OK != rtn) )
         {
            reportError("Failure to test for the existence of target directory");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         if(0 == verify)
         {
            // attempt to create
            rtn = tgtmgr->createLine(&verify); 
            if( (CF_OK != rtn) || (0 == verify) )
            {
               reportError("Failure to create target directory");
               delete srcmgr;
               srcmgr = NULL;
               delete tgtmgr;
               tgtmgr = NULL;
               return(CF_FAIL);
            }
         }
      }
      // all is well, accumulate
      if(NULL != logid)tgtmgr->setSystemLog(logid);
      rtn = tgtmgr->accumulate(*srcmgr);
      if( CF_OK == rtn )
      {
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
      }
      else
      {
         reportError("Failure to synchronize source and target directory");
         cout<<"would have synchronized: \t"<<src<<"\t to \t"<<tgt<<"\n";
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
         return(CF_FAIL);
      }
   }

   // process common data
   if( comm_src.size() != comm_tgt.size() )
   {
      reportError("Inconsistence in the size of common data queues");
      return(CF_FAIL);
   }
   for(unsigned int i=0; i<comm_src.size(); i++)
   {
      src = *(comm_src[i]); 
      tgt = root + std::string("/") + *(patient_list[index]) + std::string("/") + *(comm_tgt[i]);
      srcmgr = new rcfDirectoryMgr;
      if(NULL == srcmgr)
      {
         reportError("Failure to create source manager");
         return(CF_FAIL);
      }
      else 
      {
         rtn = srcmgr->loadLine((char*)(src.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load source line");
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
         // source must exist
         rtn = srcmgr->testLine(&verify);
         if( (CF_OK != rtn) || (0 == verify) )
         {
            reportError("Source Directory Must Exist");
            fprintf(errlogid,"missing directory: %s\n",src.c_str());
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
      }
      tgtmgr = new rcfDirectoryMgr;
      if(NULL == tgtmgr)
      {
         reportError("Failure to create target manager");
         delete srcmgr;
         srcmgr = NULL;
         return(CF_FAIL);
      }
      else 
      {
         rtn = tgtmgr->loadLine((char*)(tgt.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load target line");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         // check if target exists, create it if you can 
         rtn = tgtmgr->testLine(&verify);
         if( (CF_OK != rtn) )
         {
            reportError("Failure to test for the existence of target directory");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         if(0 == verify)
         {
            // attempt to create
            rtn = tgtmgr->createLine(&verify); 
            if( (CF_OK != rtn) || (0 == verify) )
            {
               reportError("Failure to create target directory");
               delete srcmgr;
               srcmgr = NULL;
               delete tgtmgr;
               tgtmgr = NULL;
               return(CF_FAIL);
            }
         }
      }
      // all is well, accumulate
      rtn = tgtmgr->accumulate(*srcmgr);
      if( CF_OK == rtn )
      {
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
      }
      else
      {
         reportError("Failure to synchronize source and target directory");
         cout<<"would have synchronized: \t"<<src<<"\t to \t"<<tgt<<"\n";
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
         return(CF_FAIL);
      }
   }

   // process drop data
   if( drop_src.size() != drop_tgt.size() )
   {
      reportError("Inconsistence in the size of drop data queues");
      return(CF_FAIL);
   }
   for(unsigned int i=0; i<drop_src.size(); i++)
   {
      src = *(drop_src[i]) + std::string("/") + *(patient_list[index]);
      tgt = *(drop_tgt[i]); 
      srcmgr = new rcfDirectoryMgr;
      if(NULL == srcmgr)
      {
         reportError("Failure to create source manager");
         return(CF_FAIL);
      }
      else 
      {
         rtn = srcmgr->loadLine((char*)(src.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load source line");
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
         // source must exist
         rtn = srcmgr->testLine(&verify);
         if( (CF_OK != rtn) || (0 == verify) )
         {
            reportError("Source Directory Must Exist");
            fprintf(errlogid,"missing directory: %s\n",src.c_str());
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
      }
      tgtmgr = new rcfDirectoryMgr;
      if(NULL == tgtmgr)
      {
         reportError("Failure to create target manager");
         delete srcmgr;
         srcmgr = NULL;
         return(CF_FAIL);
      }
      else 
      {
         rtn = tgtmgr->loadLine((char*)(tgt.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load target line");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         // check if target exists, create it if you can 
         rtn = tgtmgr->testLine(&verify);
         if( (CF_OK != rtn) )
         {
            reportError("Failure to test for the existence of target directory");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         if(0 == verify)
         {
            // attempt to create
            rtn = tgtmgr->createLine(&verify); 
            if( (CF_OK != rtn) || (0 == verify) )
            {
               reportError("Failure to create target directory");
               delete srcmgr;
               srcmgr = NULL;
               delete tgtmgr;
               tgtmgr = NULL;
               return(CF_FAIL);
            }
         }
      }
      // all is well, dump data 
      if(NULL != logid)tgtmgr->setSystemLog(logid);
      rtn = tgtmgr->dropData(*srcmgr);
      if( CF_OK == rtn )
      {
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
      }
      else
      {
         reportError("Failure to drop data into target directory from source directory ");
         cout<<"source: \t"<<src<<"\t target: \t"<<tgt<<"\n";
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
         return(CF_FAIL);
      }
   }
   // process filtered file data
   if( file_src.size() != file_tgt.size() )
   {
      reportError("Inconsistence in the size of file data queues");
      return(CF_FAIL);
   }
   for(unsigned int i=0; i<file_src.size(); i++)
   {
      src = *(file_src[i]) + std::string("/");
      tgt = root + std::string("/") + *(patient_list[index]) + std::string("/") + *(file_tgt[i]);
      srcmgr = new rcfDirectoryMgr;
      if(NULL == srcmgr)
      {
         reportError("Failure to create source manager");
         return(CF_FAIL);
      }
      else 
      {
         rtn = srcmgr->loadLine((char*)(src.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load source line");
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
         // source must exist
         rtn = srcmgr->testLine(&verify);
         if( (CF_OK != rtn) || (0 == verify) )
         {
            reportError("Source Directory Must Exist");
            fprintf(errlogid,"missing directory: %s\n",src.c_str());
            delete srcmgr;
            srcmgr = NULL;
            return(CF_FAIL);
         }
      }
      tgtmgr = new rcfDirectoryMgr;
      if(NULL == tgtmgr)
      {
         reportError("Failure to create target manager");
         delete srcmgr;
         srcmgr = NULL;
         return(CF_FAIL);
      }
      else 
      {
         rtn = tgtmgr->loadLine((char*)(tgt.c_str()));
         if(CF_OK != rtn)
         {
            reportError("Failure to load target line");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         // check if target exists, create it if you can 
         rtn = tgtmgr->testLine(&verify);
         if( (CF_OK != rtn) )
         {
            reportError("Failure to test for the existence of target directory");
            delete srcmgr;
            srcmgr = NULL;
            delete tgtmgr;
            tgtmgr = NULL;
            return(CF_FAIL);
         }
         if(0 == verify)
         {
            // attempt to create
            rtn = tgtmgr->createLine(&verify); 
            if( (CF_OK != rtn) || (0 == verify) )
            {
               reportError("Failure to create target directory");
               delete srcmgr;
               srcmgr = NULL;
               delete tgtmgr;
               tgtmgr = NULL;
               return(CF_FAIL);
            }
         }
      }
      //filter data
      int ndirs,nfiles;
      rtn = srcmgr->statLine(&ndirs,&nfiles);
      if(CF_OK != rtn)
      {
         reportError("Failure to stat source directory");
         return(CF_FAIL);
      }
      rtn = srcmgr->filterFilesBySubstring(*(patient_list[index]),&nfiles);
      if(CF_OK != rtn)
      {
         reportError("Failure to filter source directory");
         return(CF_FAIL);
      }
      rtn = tgtmgr->statLine(&ndirs,&nfiles);
      if(CF_OK != rtn)
      {
         reportError("Failure to stat target directory");
         return(CF_FAIL);
      }
      rtn = tgtmgr->filterFilesBySubstring(*(patient_list[index]),&nfiles);
      if(CF_OK != rtn)
      {
         reportError("Failure to filter target directory");
         return(CF_FAIL);
      }
      // all is well, dump data 
      if(NULL != logid)tgtmgr->setSystemLog(logid);
      rtn = tgtmgr->accumulateFilteredFiles(*srcmgr);
      if( CF_OK == rtn )
      {
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
      }
      else
      {
         reportError("Failure to accumulate filtered files into target directory from source directory ");
         cout<<"source: \t"<<src<<"\t target: \t"<<tgt<<"\n";
         delete srcmgr;
         srcmgr = NULL;
         delete tgtmgr;
         tgtmgr = NULL;
         return(CF_FAIL);
      }
   }

   return(CF_OK);
}

int rcfDataAccumulator::processPatientList(rcfClifParser *parser)
{
   func = (char*)"rcfDataAccumulator::processPatientList";
   int rtn,verify;
   bool patient_failed = false;

   // sanity checks
   if(!(parser->isClifReady()))
   {
      reportError("processing parser is not valid");
      return(CF_FAIL);
   }


   reInitList();

   //build patient list
   rtn = buildPatientList(parser);
   if(CF_OK != rtn)
   {
      reportError("Failure to build patient list");
      return(CF_FAIL);
   }

   // create lists of correlated data
   parser->markCurrentClif();

   rtn = parser->iterateAndSelectClif("correlated_data",&verify);
   if((CF_OK != rtn) || (0 == verify) )
   {
      reportWarning("Failure to find correlated data list"); 
   }
   else
   {
      rtn = buildCorrelatedDataList(parser);
      if(CF_OK != rtn)
      {
         reportError("Failure to build correlated data list");
         return(CF_FAIL);
      }
   }

   parser->selectMarkedClif();
   rtn = parser->iterateAndSelectClif("common_data",&verify);
   if((CF_OK != rtn) || (0 == verify))
   {
      // benign failure, do nothing
   }
   else
   {
      rtn = buildCommonDataList(parser);
      if(CF_OK != rtn)
      {
         reportError("Failure to build common data list");
         return(CF_FAIL);
      }
   }

   parser->selectMarkedClif();
   rtn = parser->iterateAndSelectClif("drop_data",&verify);
   if((CF_OK != rtn) || (0 == verify))
   {
      // benign failure, do nothing
   }
   else
   {
      rtn = buildDropDataList(parser);
      if(CF_OK != rtn)
      {
         reportError("Failure to build drop data list");
         return(CF_FAIL);
      }
   }

   parser->selectMarkedClif();
   rtn = parser->iterateAndSelectClif("filtered_file_data",&verify);
   if((CF_OK != rtn) || (0 == verify))
   {
      // benign failure, do nothing
   }
   else
   {
      rtn = buildFileDataList(parser);
      if(CF_OK != rtn)
      {
         reportError("Failure to build file data list");
         return(CF_FAIL);
      }
   }



   // loop over patients 
   for(unsigned int i=0; i<patient_list.size(); i++)
   {
     rtn = processPatient(i); 
     if(CF_OK != rtn)
     {
        reportError("Failure to process Patient");
        fprintf(errlogid,"Patient index: %d\n",i);
        // do not exit, just report error.
        patient_failed = true;
     }
   }
  

   if(patient_failed)return(CF_FAIL);
   else return(CF_OK);
}

int rcfDataAccumulator::buildPatientList(rcfClifParser *parser)
{
   func = (char*)"rcfDataAccumulator::buildPatientList";
   int rtn,verify;
   int npatients;

   // sanity checks
   if(!(parser->isClifReady()))
   {
      reportError("processing parser is not valid");
      return(CF_FAIL);
   }

   // get number of patients
   rtn = parser->getNumberOfDataObjectsInSelectedClif(&npatients);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of patients");
      return(CF_FAIL);
   }
   if( npatients <= 0 )
   {
      reportError("No patients to process");
      return(CF_FAIL);
   }

   char *handle = NULL;
   char *data = NULL;
   std::string *shandle = NULL;
   std::string *sdata = NULL;

   for(int i=0; i<npatients; i++)
   {
      handle = parser->getHandleOfDataObjectInSelectedClif(i,&verify);
      if( (NULL == handle) || (0 == verify) )
      {
         reportError("Failure to get Handle of data object");
         return(CF_FAIL);
      }
      shandle = new std::string(handle);
      if(NULL == shandle)
      {
         reportError("Failure to create Handle string");
         return(CF_FAIL);
      }

      // not a patient
      if(0 != shandle->compare("patient"))
      {
         delete shandle;
         shandle = NULL;
         continue;
      }
      else
      {
          delete shandle;
          shandle = NULL;
      }


      data = parser->getDataBufferInSelectedClif(i,&verify);
      if( (NULL == data) || (0 == verify) )
      {
         reportError("Failure to get Handle of data object");
         return(CF_FAIL);
      }
      sdata = new std::string(data);
      if(NULL == sdata)
      {
         reportError("Failure to create data string");
         return(CF_FAIL);
      }
      patient_list.push_back(sdata);
   }
   return(CF_OK);
}

int rcfDataAccumulator::buildDataListSelect(rcfClifParser *parser,int select)
{
   func = (char*)"rcfDataAccumulator::buildDataListSelect";
   int rtn,verify;
   int nlines;

   // sanity checks
   if(!(parser->isClifReady()))
   {
      reportError("processing parser is not valid");
      return(CF_FAIL);
   }

   switch(select)
   {
      case 0:
      case 1:
      case 2:
      case 3:
         break;
      default:
         reportError("Invalid Selector");
         fprintf(errlogid,"Selector value: %d\n",select);
         return(CF_FAIL);
         break;
   }

   rtn = parser->getNumberOfDataObjectsInSelectedClif(&nlines);
   if(CF_OK != rtn)
   {
      reportError("Failure to get number of lines");
      return(CF_FAIL);
   }

   char *handle = NULL;
   char *data = NULL;
   std::string *shandle = NULL;
   std::string *sdata = NULL;

   for(int i=0; i<nlines; i++)
   {
      handle = parser->getHandleOfDataObjectInSelectedClif(i,&verify);
      if( (NULL == handle) || (0 == verify) )
      {
         reportError("Failure to get Handle of data object");
         return(CF_FAIL);
      }
      shandle = new std::string(handle);
      if(NULL == shandle)
      {
         reportError("Failure to create Handle string");
         return(CF_FAIL);
      }
      data = parser->getDataBufferInSelectedClif(i,&verify);
      if( (NULL == data) || (0 == verify) )
      {
         reportError("Failure to get Handle of data object");
         return(CF_FAIL);
      }
      sdata = new std::string(data);
      if(NULL == sdata)
      {
         reportError("Failure to create data string");
         return(CF_FAIL);
      }
      switch(select)
      {
         case 0:
            corr_tgt.push_back(shandle);
            corr_src.push_back(sdata);
            break;
         case 1:
            comm_tgt.push_back(shandle);
            comm_src.push_back(sdata);
            break;
         case 2:
            drop_tgt.push_back(shandle);
            drop_src.push_back(sdata);
            break;
         case 3:
            file_tgt.push_back(shandle);
            file_src.push_back(sdata);
            break;
         default:
            reportError("Invalid Selector");
            fprintf(errlogid,"Selector value: %d\n",select);
            return(CF_FAIL);
            break;
      }
   }

   return(CF_OK);

}

int rcfDataAccumulator::buildCorrelatedDataList(rcfClifParser *parser)
{
   return(buildDataListSelect(parser,0));
}
int rcfDataAccumulator::buildCommonDataList(rcfClifParser *parser)
{
   return(buildDataListSelect(parser,1));
}
int rcfDataAccumulator::buildDropDataList(rcfClifParser *parser)
{
   return(buildDataListSelect(parser,2));
}
int rcfDataAccumulator::buildFileDataList(rcfClifParser *parser)
{
   return(buildDataListSelect(parser,3));
}

void rcfDataAccumulator::reportError(const char *string)
{
   if(NULL == string)
   {
      if(NULL == func)
      {
         fprintf(errlogid,"ERROR: Anonymous Function called error report with empty data string  \n");
      }
      else
      {
         fprintf(errlogid,"ERROR: %s  called error report with empty data string \n",func);
      }
   }

   if(NULL == func)
   {
      fprintf(errlogid,"ERROR: Anonymous Function reports  %s \n",string);
   }
   else
   {
      fprintf(errlogid,"ERROR: %s  %s \n",func,string);
   }
}

void rcfDataAccumulator::reportWarning(const char *string)
{
   if(NULL == string)
   {
      if(NULL == func)
      {
         fprintf(errlogid,"WARNING: Anonymous Function called error report with empty data string  \n");
      }
      else
      {
         fprintf(errlogid,"WARNING: %s  called error report with empty data string \n",func);
      }
   }

   if(NULL == func)
   {
      fprintf(errlogid,"WARNING: Anonymous Function reports  %s \n",string);
   }
   else
   {
      fprintf(errlogid,"WARNING: %s  %s \n",func,string);
   }
}
