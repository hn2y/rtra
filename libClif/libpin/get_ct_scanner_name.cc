/** \file 
 simple tag auto added to force generation of simple doxygen comments
**/
/*
   Copyright 2000-2003 Virginia Commonwealth University


   Advisory:
1. The authors make no claim of accuracy of the information in these files or the 
   results derived from use of these files.
2. You are not allowed to re-distribute these files or the information contained within.
3. This methods and information contained in these files was interpreted by the authors 
   from various sources.  It is the users sole responsibility to verify the accuracy 
   of these files.  
4. If you find a error within these files, we ask that you to contact the authors 
   and the distributor of these files.
5. We ask that you acknowledge the source of these files in publications that use results
   derived from the input 

   Please contact us if you have any questions 
*/
/* get_ct_scanner_name.cc
   Reads in Scanner name either interactively or non-interactively 
   Created: 18 Jan, 2000: PJK
   Modification History:
     April 29, 2006: JVS: Pinnacle V7.9, puts CT data in "PatientRepresentation" object...
                          Also, get rid of "current" in the read_pinn_scanner script
                          To do this, I used the readClif object.....
      June 26, 2008: JVS: clean up output messages
*********************************************************************************** */
#include <stdlib.h>      // for calloc
#include <stdio.h>
#include <string.h>      // for strcat and strcpy
#include "string_util.h" // for get_value
#include "utilities.h"   // for error routines
#include "typedefs.h"
//#include "dose_region.h" // for definition of dose regions structure and function prototypes
#include "read_clif.h"
#include "get_ct_scanner.h"

#include "myPinnacle.h"
#include "myPinnComm.h"
/* ********************************************************************************* */


int read_pinn_scanner(char *plan_fname, char *trial_name, char *CtToDensityName, long myPinnWindowID)
{
  if(myPinnWindowID > 0 )  // active pinnacle session
  {
   // Get ct to density conversion
   char stmp[PINN_COMM_STRING_LEN];   
   char command[PINN_COMM_STRING_LEN];
   int iStatus = 1;
   sprintf(command,"TrialList.%s.CtToDensityName",trial_name);
   // iStatus = iPinnComm_QueryString(NULL, "TrialList.Current.CtToDensityName", stmp);
   iStatus = iPinnComm_QueryString(NULL, command, stmp);
   if (!iStatus) {
      fprintf(stderr, "ERROR: (Active) Getting CT scanner type\n");
      return (FAIL);
   }
   strcpy(CtToDensityName,stmp);
#ifdef DEBUG
   printf("\n(Int) CT table is %s",CtToDensityName);
#endif
  }
  else // non-active pinnacle session
  {
   if(read_pinn_scanner(plan_fname, trial_name, CtToDensityName) != OK)
   {
      eprintf("\n ERROR: (Non-Active) Getting CT scanner type"); 
      return(FAIL);
   }
  }
  return(OK);
}
/* ********************************************************************************* */
int read_pinn_scanner(char *pbeamFnameStem, char *TrialName, char *CtToDensityName)
{  // (alot like readPinnaclePatientBeams)
    // reads in the CT Scanner from Pinnacle
   // append .Trial to the name to read in the .Trial information
   char pbeamFilename[MAX_STR_LEN];
   strcpy(pbeamFilename,pbeamFnameStem);  // put first part on name
   strcat(pbeamFilename,".Trial");          // append .Trial to the name  
   /* Open The File */
   FILE *istrm = fopen(pbeamFilename,"r");
   if(istrm==NULL)
   {
      printf("\n ERROR: opening patient beam file %s", pbeamFilename);return(FAIL);
   }
   clifObjectType *TrialList = new(clifObjectType);
   if(TrialList == NULL)
   {
      printf("\n ERROR: Allocating Memory for clifObjectType\n"); return(FAIL);
   }
   if( readSpecificClif(istrm, TrialList, "Trial", TrialName) != OK)
   {
      printf("\n ERROR: Reading clif from file %s\n", pbeamFilename); return(FAIL);
   }
   fclose(istrm); // close the input stream
   /* Check if CT object exists.... */
   if( OK == checkForClifObject(TrialList,"PatientRepresentation") ) 
   {
     // Version 7.9 and higher
      if(OK != readStringClifStructure(TrialList, "PatientRepresentation.CtToDensityName",CtToDensityName)  )
      {
	printf("\n ERROR: Cannot read CtToDensityName from PatientRepresentation"); return(FAIL);
      }
   } else {
     // version < 7.9
      if(OK != readStringClifStructure(TrialList, "CtToDensityName",CtToDensityName)  )
      {
	printf("\n ERROR: Cannot read CtToDensityName from Trial"); return(FAIL);
      }
   }  
#ifdef DEBUG
   printf("\n CtToDensityName = %s", CtToDensityName);
#endif
   return(OK);
}
/* ********************************************************************************* */
int read_pinn_scannerOLD(char *fname_stem, char *Trial_Name, char *CtToDensityName)
{
   char pbeam_fname[MAX_STR_LEN];
   strcpy(pbeam_fname,fname_stem);  // put first part on name
   strcat(pbeam_fname,".Trial");    // append .Trial to the name  
   // open dose region file
   FILE *fp = fopen(pbeam_fname,"r");
   if(fp==NULL)
   {
      eprintf("\n ERROR: opening dose region file %s", pbeam_fname);
      return(FAIL);
   }
   int beam_found = 0;
   // get to the desired trial
   do{
      if(clif_get_to_line(fp,"Trial ={")!=OK)
      {
         eprintf("\n ERROR: finding Trial in file %s",pbeam_fname);
         return(FAIL);
      }
      char Current_Trial_Name[MAX_STR_LEN];
      // get and report Trial Name
      if(clif_string_read(fp,"Name =",Current_Trial_Name) != OK)  
         return(FAIL); 
      /* check if current trial matches */
      if(strcmp(Current_Trial_Name,Trial_Name) == 0) beam_found = 1;
      if(!beam_found)
      {  /* Close the Trial cliff */
         if(clif_get_to_line(fp,"};")!=OK)
         {
            eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
            return(FAIL);
         }
      }
   }while(!beam_found);
#ifdef DEBUG
   printf("\n Reading In CT scanner for Trial %s", Trial_Name);
#endif
   if(clif_string_read(fp,"CtToDensityName = ",CtToDensityName) != OK)
   {
     eprintf("\n ERROR: Reading in CT to Dens Name in read_pinn_scanner");
     return(FAIL);
   }
#ifdef DEBUG
   printf("\n (Non-int) CT to density name is %s...",CtToDensityName);
#endif
   // currently, read in only 3-D dose grid from Pinnacle
   // close context of Trial structure
   if(clif_get_to_line(fp,"};")!=OK)
   {
      eprintf("\n ERROR: Closing Trial structure in file %s",pbeam_fname);
      return(FAIL);
   }
   // close dose region file
   fclose(fp);
   return(OK);
} 
/* ********************************************************************************* */
